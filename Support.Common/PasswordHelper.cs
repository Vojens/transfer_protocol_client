﻿using System;

namespace Support.Common
{
    /// <summary>
    /// PasswordHelper
    /// </summary>
    public static class PasswordHelper
    {
        /// <summary>
        /// Generates the random password.
        /// </summary>
        /// <param name="passwordLength">Length of the password.</param>
        /// <returns></returns>
        public static string GenerateRandomPassword(int passwordLength)
        {
            string allowedChars = "abcdefghjkmnpqrtuvwxyzABCDEFGHJKMNPQRTUVWXYZ2346789";
            char[] chars = new char[passwordLength];
            Random rd = new Random();

            for (int i = 0; i < passwordLength; i++)
            {
                chars[i] = allowedChars[rd.Next(0, allowedChars.Length)];
            }

            return new string(chars);
        }
    }
}
