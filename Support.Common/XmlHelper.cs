 

using System;
using System.Collections.Generic;
using System.Text;
using System.Xml.Linq;
using System.Xml;
using System.Xml.Xsl;
using System.Net;
using System.IO;
using System.Xml.Serialization;
using System.Security;
using Support.Common.Logging;
using System.Text.RegularExpressions;

namespace Support.Common
{
    public class XmlHelper
    {
        //  Has passed UnitTest.
        /// <summary>
        /// Gets differences between two xml strings.
        /// </summary>
        /// <param name="xml1">1st xml string.</param>
        /// <param name="xml2">2nd xml string.</param>
        /// <returns>Differences message, empty string if two xmls are similar.</returns>
        public static string GetDiff(string xml1, string xml2)
        {
            if (string.IsNullOrEmpty(xml1) || string.IsNullOrEmpty(xml2))
            {
                throw new XmlHelperException("One of the string is null or an empty string.");
            }
            else
            {
                try
                {
                    XElement el1 = XElement.Parse(xml1);
                    XElement el2 = XElement.Parse(xml2);
                    string diffMessage = string.Empty;

                    if (el1.Value != el2.Value)
                    {
                        List<string> differents;
                        List<int> lineNumbers;
                        long diffIndex = StringHelper.GetDiffIndex(el1.ToString(), el2.ToString(), out differents
                            , out lineNumbers);
                        if (diffIndex != -1)
                        {
                            diffMessage = string.Format("{0} in line {2} and {1} in line {3}", differents[0], differents[1]
                                , lineNumbers[0], lineNumbers[1]);
                        }
                    }

                    return diffMessage;
                }
                catch  (XmlException)
                {
                    throw new XmlHelperException("Can not parse one of the xml string.");
                }
        }
        }

        private static void removeIgnoredTags(List<string> ignoredTags, ref string xml)
        {
            foreach (string ignoreTag in ignoredTags)
            {
                string pattern = "<" + ignoreTag + @">.+</" + ignoreTag + ">";
                xml = Regex.Replace(xml, pattern, string.Empty);
            }
        }

        public static string GetDiff(string xml1, string xml2, List<string> ignoredTags)
        {
            if (string.IsNullOrEmpty(xml1) || string.IsNullOrEmpty(xml2))
            {
                throw new XmlHelperException("One of the string is null or an empty string.");
            }
            else
            {
                try
                {
                    //  Remove all exception tags from both of XML file.
                    removeIgnoredTags(ignoredTags, ref xml1);
                    removeIgnoredTags(ignoredTags, ref xml2);

                    XElement el1 = XElement.Parse(xml1);
                    XElement el2 = XElement.Parse(xml2);

                    string diffMessage = string.Empty;

                    if (el1.Value != el2.Value)
                    {
                        List<string> differents;
                        List<int> lineNumbers;
                        long diffIndex = StringHelper.GetDiffIndex(el1.ToString(), el2.ToString(), out differents
                            , out lineNumbers);
                        if (diffIndex != -1)
                        {
                            diffMessage = string.Format("{0} in line {2} and {1} in line {3}", differents[0], differents[1]
                                , lineNumbers[0], lineNumbers[1]);
                        }
                    }

                    return diffMessage;
                }
                catch (XmlException)
                {
                    throw new XmlHelperException("Can not parse one of the xml string.");
                }
            }
        }

        //  Has passed UnitTest.
        /// <summary>
        /// Transforms an XML with specified XSLT into an HTML page.
        /// </summary>
        /// <param name="xmlFullFilePath">Path to the XML file.</param>
        /// <param name="xsltFullFilePath">Path to the XSLT file.</param>
        /// <param name="htmlFullFilePath">Path/save as HTML file.</param>
        public static void Transform(string xmlFullFilePath, string xsltFullFilePath, string htmlFullFilePath)
        {
            if (string.IsNullOrEmpty(htmlFullFilePath))
            {
                throw new XmlHelperException("Transform into HTML has failed.");
            }

            try
            {
                IOHelper.ValidateFullFilePath(xmlFullFilePath);
                IOHelper.ValidateFullFilePath(xsltFullFilePath);

                // Load the style sheet.
                XslCompiledTransform xslt = new XslCompiledTransform();
                xslt.Load(xsltFullFilePath);

                // Execute the transform and output the results to a file.
                xslt.Transform(xmlFullFilePath, htmlFullFilePath);
            }
            catch (IOHelperException)
            {
                throw new XmlHelperException("Transform into HTML has failed.");
            }
            catch (XsltException)
            {
                throw new XmlHelperException("Transform into HTML has failed.");
            }
            catch (WebException)
            {
                throw new XmlHelperException("Transform into HTML has failed.");
            }
            catch (UriFormatException)
            {
                throw new XmlHelperException("Transform into HTML has failed.");
            }
            catch (XmlException)
            {
                throw new XmlHelperException("Transform into HTML has failed.");
            }
        }

        //  Has passed UnitTest.
        /// <summary>
        /// Deserialize XML File into T object.
        /// </summary>
        /// <param name="xmlFilePath">XML file path.</param>
        /// <returns>T object.</returns>
        public static T Deserialize<T>(string xmlFullFilePath)
        {
            return Deserialize<T>(xmlFullFilePath, true);
        }

        public static T Deserialize<T>(string xmlFullFilePath, bool isValidatePath)
        {
            return Deserialize<T>(xmlFullFilePath, isValidatePath, new Type[0]);
        }

        public static T Deserialize<T>(string xmlFullFilePath, bool isValidatePath, Type[] extraTypes)
        {
            if (isValidatePath)
            {
                IOHelper.ValidateFullFilePath(xmlFullFilePath);
                if (string.IsNullOrEmpty(IOHelper.GetFileContent(xmlFullFilePath).Trim()))
                {
                    throw new XmlHelperException("File is empty");
                }
            }

            XmlSerializer serializer;

            if (extraTypes.Length <= 0)
            {
                serializer = new XmlSerializer(typeof(T));
            }
            else
            {
                serializer = new XmlSerializer(typeof(T), extraTypes);
            }

            try
            {
                using (StreamReader reader = new StreamReader(xmlFullFilePath))
                {                   
                    return (T)serializer.Deserialize(reader);
                }
            }
            catch (IOHelperException ioHelpEx)
            {
                ExceptionHandlerLogger.Write(ioHelpEx.Message);
                throw new XmlHelperException("Deserialize process is failed.");
            }
            catch (IOException ioEx)
            {
                ExceptionHandlerLogger.Write(ioEx.Message);
                throw new XmlHelperException("Deserialize process is failed.");
            }
            catch (InvalidOperationException invOpEx)
            {
                ExceptionHandlerLogger.Write(invOpEx.Message);
                throw new XmlHelperException("Deserialize process is failed.");
            }
        }

        /// <summary>
        /// Deserialize xml string into a T object.
        /// </summary>
        /// <typeparam name="T"></typeparam>
        /// <param name="xml"></param>
        /// <returns></returns>
        public static T DeserializeString<T>(string xml)
        {
            if (string.IsNullOrEmpty(xml))
            {
                throw new XmlHelperException("Deserialize process is failed.");
            }

            try
            {
                using (StringReader reader = new StringReader(xml))
                {
                    XmlSerializer serializer = new XmlSerializer(typeof(T));
                    return (T)serializer.Deserialize(reader);
                }
            }
            catch (InvalidOperationException invOpEx)
            {
                ExceptionHandlerLogger.Write(invOpEx.Message);
                throw new XmlHelperException("Deserialize process is failed.");
            }
        }


        /// <summary>
        /// Serializes object into XML and save it in a directory path.
        /// </summary>
        /// <typeparam name="T">Type of object to serialize.</typeparam>
        /// <param name="objToSerialize">Object to serialize.</param>
        /// <param name="xmlFullFilePath">XML file name target of serialization.</param>
        public static void Serialize<T>(T objToSerialize, string xmlFullFilePath)
        {
            Serialize<T>(objToSerialize, xmlFullFilePath, new Type[0]);
        }

        
        /// <summary>
        /// Serializes object into XML and save it in a directory path.
        /// </summary>
        /// <typeparam name="T">Type of object to serialize.</typeparam>
        /// <param name="objToSerialize">Object to serialize.</param>
        /// <param name="xmlFullFilePath">XML file name target of serialization.</param>
        /// <param name="extraTypes">A System.Type array of additional object types to serialize.</param>
        public static void Serialize<T>(T objToSerialize, string xmlFullFilePath, Type[] extraTypes)
        {
            try
            {
                FileInfo fInfo = new FileInfo(xmlFullFilePath);
                if (!Directory.Exists(fInfo.DirectoryName))
                    Directory.CreateDirectory(fInfo.DirectoryName);

                //  Validate the full file path, if not exist then the path will be created automatically and return true.
                if (IOHelper.IsValidFullFilePath(xmlFullFilePath, true))
                {
                    if (objToSerialize == null)
                    {
                        throw new XmlHelperException("Serialize process is failed.");
                    }

                    XmlSerializer serializer;

                    if (extraTypes.Length <= 0)
                    {
                        serializer = new XmlSerializer(typeof(T));
                    }
                    else
                    {
                        serializer = new XmlSerializer(typeof(T), extraTypes);
                    }

                    try
                    {
                        using (StreamWriter writer = new StreamWriter(xmlFullFilePath))
                        {
                            serializer.Serialize(writer, objToSerialize);
                        }
                    }
                    catch (SecurityException secEx)
                    {
                        ExceptionHandlerLogger.Write(secEx.Message);
                        throw new XmlHelperException("Serialize process is failed.");
                    }
                    catch (UnauthorizedAccessException unEx)
                    {
                        ExceptionHandlerLogger.Write(unEx.Message);
                        throw new XmlHelperException("Serialize process is failed.");
                    }
                    catch (PathTooLongException plEx)
                    {
                        ExceptionHandlerLogger.Write(plEx.Message);
                        throw new XmlHelperException("Serialize process is failed.");
                    }
                    catch (IOException ioEx)
                    {
                        ExceptionHandlerLogger.Write(ioEx.Message);
                        throw new XmlHelperException("Serialize process is failed.");
                    }
                    catch (Exception ex)
                    {
                        ExceptionHandlerLogger.Write(ex.Message);
                        throw new XmlHelperException("Serialize process is failed.");
                    }
                }
                else
                {
                    throw new XmlHelperException("Serialize process is failed.");
                }
            }
            catch 
            {
                throw new XmlHelperException("Serialize process is failed.");
            }
        }

        /// <summary>
        /// Serialize an object into a string in XML format.
        /// </summary>
        /// <typeparam name="T"></typeparam>
        /// <param name="objToSerialize"></param>
        /// <returns></returns>
        public static string Serialize<T>(T objToSerialize, string prefix, string namespaces)
        {
            if (objToSerialize == null)
                {
                    throw new XmlHelperException("Serialize process is failed.");
                }

                //Create our own namespaces for the output
                XmlSerializerNamespaces ns = new XmlSerializerNamespaces();

                //Add an empty namespace and empty value
                ns.Add(prefix, namespaces);

                XmlSerializer serializer = new XmlSerializer(typeof(T));

                try
                {
                    using (StringWriter writer = new StringWriter())
                    {
                        serializer.Serialize(writer, objToSerialize, ns);
                        return writer.ToString();
                    }
                }
                catch (Exception ex)
                {
                    ExceptionHandlerLogger.Write(ex.Message);
                    throw new XmlHelperException("Serialize process is failed.");
                }
        }

        /// <summary>
        /// Serialize an object into a string in XML format.
        /// </summary>
        /// <typeparam name="T"></typeparam>
        /// <param name="objToSerialize"></param>
        /// <returns></returns>
        public static string Serialize<T>(T objToSerialize)
        {
            if (objToSerialize == null)
            {
                throw new XmlHelperException("Serialize process is failed.");
            }

            XmlSerializer serializer = new XmlSerializer(typeof(T));

            try
            {
                //using (StringWriter writer = new StringWriter())
                StringBuilder sbWriter = new StringBuilder();
                using (StringWriterWithEncoding writer = new StringWriterWithEncoding(sbWriter, Encoding.UTF8))
                {
                    serializer.Serialize(writer, objToSerialize);
                    return sbWriter.ToString();
                }
            }
            catch (Exception ex)
            {
                ExceptionHandlerLogger.Write(ex.Message);
                throw new XmlHelperException("Serialize process is failed.");
            }
        }

        public class StringWriterWithEncoding : StringWriter
        {
            Encoding encoding; 
            public StringWriterWithEncoding(StringBuilder builder, Encoding encoding) : base(builder) 
            { this.encoding = encoding; }
            public override Encoding Encoding 
            { get { return encoding; } }
        }

        public static string EscapeXml(string s)
        {
            string xml = s;
            if (!string.IsNullOrEmpty(xml))
            {
                // replace literal values with entities
                xml = xml.Replace("&", "&amp;");
                xml = xml.Replace("<", "&lt;");
                xml = xml.Replace(">", "&gt;");
                xml = xml.Replace("\"", "&quot;");
                xml = xml.Replace("'", "&apos;");
            }
            return xml;
        }

        public static string EscapeXmlNotQuot(string s)
        {
            string xml = s;
            if (!string.IsNullOrEmpty(xml))
            {
                // replace literal values with entities
                xml = xml.Replace("&", "&amp;");
                xml = xml.Replace("<", "&lt;");
                xml = xml.Replace(">", "&gt;");
                xml = xml.Replace("\"", "&quot;");
                //xml = xml.Replace("'", "&apos;");
            }
            return xml;
        }


        public static string UnescapeXml(string s)
        {
            string unxml = s;
            if (!string.IsNullOrEmpty(unxml))
            {
                // replace entities with literal values
                unxml = unxml.Replace("&apos;", "'");
                unxml = unxml.Replace("&quot;", "\"");
                unxml = unxml.Replace("&gt;", ">");
                unxml = unxml.Replace("&lt;", "<");
                unxml = unxml.Replace("&amp;", "&");
            }
            return unxml;
        }

        public static string WritterFormatedXml(string xml)
        {
            string retStr;
            StringWriter formatedXml = new StringWriter();
            try
            {
                XmlTextReader reader = new XmlTextReader(new StringReader(xml));
                reader.WhitespaceHandling = WhitespaceHandling.None;
                XmlTextWriter writer = new XmlTextWriter(formatedXml);
                writer.Formatting = Formatting.Indented;
                writer.Indentation = 3;
                writer.IndentChar = ' ';
                
                writer.WriteNode(reader, false);
                writer.Flush();
                retStr = formatedXml.ToString();

                writer.Close();
                reader.Close();
            }
            catch
            {
                return xml;
            }
            finally
            {
                if (formatedXml != null)
                    formatedXml.Close();
            }

            return retStr;
        }
    }
}
