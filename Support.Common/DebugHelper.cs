﻿ 

using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Configuration;

namespace Support.Common
{
    public class DebugHelper
    {
        private List<string> _debugs = new List<string>();
        private object _debugLock = new object();

        public int entryLimit = 1000;

        public void Write(string text)
        {
            lock (_debugLock)
            {
                if (entryLimit > 0 && _debugs.Count >= entryLimit) _debugs.RemoveAt(0);

                _debugs.Add("[" + DateTime.Now.ToString() + "] " + text);
            }
        }

        public string ToString()
        {
            var debugBuilder = new StringBuilder();
            
            debugBuilder.AppendLine(SystemInformation.GetEnvironmentInformation());

            lock (_debugLock)
            {
                foreach (string text in _debugs)
                {
                    debugBuilder.AppendLine(text);
                    debugBuilder.AppendLine();
                }
            }
            //if (result != string.Empty) result = result.Substring(0, result.Length - 4);

            return debugBuilder.ToString();
        }

        public void Clear()
        {
            _debugs.Clear();
        }

    }
}
