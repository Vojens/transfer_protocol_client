﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;

namespace Support.Common
{
    public class TimeSpanHelper
    {
        /// <summary>
        /// This method helps adding capability to add total hour using "HH" in timespan
        /// Normally, Microsoft only provides "hh" which on 24 hour onward will display as Days instead of keeping it in hour section
        /// </summary>
        /// <param name="input"></param>
        /// <param name="format"></param>
        /// <returns></returns>
        public static string ConvertTimeSpanToString(TimeSpan input, string format)
        {
            if (String.IsNullOrEmpty(format)) return String.Empty;
            if (format.Contains("HH"))
            {
                var resultBuilder = new StringBuilder();

                //Find Position
                var position = format.IndexOf("HH", StringComparison.CurrentCulture);
                
                //Process Head
                var head = format.Substring(0, position);
                var headResult = String.IsNullOrEmpty(head) ? String.Empty : input.ToString(head);
                resultBuilder.Append(headResult);

                //Process "HH" Segment
                var hours = input.Days*24 + input.Hours;
                resultBuilder.Append(hours);

                var tail = format.Substring(position + 2);
                ConvertTimeSpanToString(input, tail,resultBuilder); //Process Tail
                

                var result = resultBuilder.ToString();
                return result;
            }
            else
            {
                return input.ToString(format);
            }
        }

        private static void ConvertTimeSpanToString(TimeSpan input, string format,StringBuilder resultBuilder)
        {
            if (String.IsNullOrEmpty(format)) return;
            if (format.Contains("HH"))
            {
                var position = format.IndexOf("HH", StringComparison.CurrentCulture);
                var head = format.Substring(0, position);
                var tail = format.Substring(position + 2);
                var headResult = String.IsNullOrEmpty(head) ? String.Empty : input.ToString(head);

                var hours = input.Days * 24 + input.Hours;
                var tailResult = ConvertTimeSpanToString(input, tail);
                resultBuilder.Append(headResult);
                resultBuilder.Append(hours);
                resultBuilder.Append(tailResult);
            }
            else
            {
                resultBuilder.Append(input.ToString(format));
            }
        }
    }
}
