 

using System;
using System.Collections.Generic;
using System.Text;

namespace Support.Common
{
    public class ArrayHelper
    {
        /// <summary>
        /// Return array contains separator to split an object.
        /// </summary>
        /// <typeparam name="T"></typeparam>
        /// <param name="separator"></param>
        /// <returns></returns>
        public static T[] GetSeparator<T>(T separator)
        {
            return new T[] { separator };
        }

        public static string[] Split(string targetToSplit, char separator, int max)
        {
            char[] separators = new char[] { separator };

            if (max > 0)
            {
                return targetToSplit.Split(separators, max);
            }
            else
            {
                return targetToSplit.Split(separators);
            }
        }
    }
}
