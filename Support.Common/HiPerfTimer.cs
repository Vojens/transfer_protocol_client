 

using System;
	using System.Runtime.InteropServices;
	using System.ComponentModel;
	using System.Threading;

namespace Support.Common
	{
		public class HiPerfTimer
		{
			[DllImport("Kernel32.dll")]
			private static extern bool QueryPerformanceCounter(out long lpPerformanceCount);

			[DllImport("Kernel32.dll")] 
			private static extern bool QueryPerformanceFrequency(out long lpFrequency);

			private long startTime, stopTime;
			private long freq;

            private bool running;

			// Constructor
			public HiPerfTimer()
			{
				startTime = 0;
				stopTime  = 0;
                running = false;

				if (QueryPerformanceFrequency(out freq) == false)
				{
					// high-performance counter not supported
					throw new Win32Exception();
				}
			}

			// Start the timer
			public void Start()
			{
				// lets do the waiting threads there work
				Thread.Sleep(0);
                running = true;
				QueryPerformanceCounter(out startTime);

			}


            // Stop the timer
			public void Stop()
			{
                if (running)
                {
                    QueryPerformanceCounter(out stopTime);
                    running = false;
                }
			}

			// Returns the duration of the timer (in seconds)
			public double Duration
			{
				get
				{
					QueryPerformanceCounter(out stopTime);
					return (double)(stopTime - startTime) / (double) freq;
				}
			}

			public double FinalDuration
			{
				get
				{
					return (double)(stopTime - startTime) / (double) freq;
				}
			}
		}
	}

