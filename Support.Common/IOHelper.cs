 

using System;
using System.Collections.Generic;
using System.IO;
using System.Security;
using System.Security.Cryptography;
using System.Text;
using System.Text.RegularExpressions;
using System.Threading;
using Support.Common.Logging;

namespace Support.Common
{
    public static class IOHelper
    {
        public static bool SaveFile(this Stream inputStream, string fullFileName, ref long lastBufferIndex, long streamLength
            , FileStream targetStream)
        {
            bool isSucceed = true;

            try
            {
                //Read from the input stream in 4K chunks and save to output stream.
                const int bufferLen = 4096;
                byte[] buffer = new byte[bufferLen];
                int count = 0;
                while ((count = inputStream.Read(buffer, 0, bufferLen)) > 0)
                {
                    targetStream.Write(buffer, 0, count);
                    lastBufferIndex += count;
                }
            }
            catch (IOException ioEx)
            {
                isSucceed = false;
                Console.WriteLine("Error: {0}", ioEx.Message);
            }
            finally
            {
                if (isSucceed)
                {
                    inputStream.Close();
                }
            }

            return isSucceed;
        }

        public static void RenameFile(string fromFileName, string toFileName)
        {
            if (!IsValidFullFilePath(fromFileName, false))
            {
                throw new IOHelperException("Invalid source file name.");
            }

            try
            {
                if (IsValidFullFilePath(toFileName, false))
                {
                    File.Delete(toFileName);
                }

                File.Move(fromFileName, toFileName);
                File.Delete(fromFileName);
            }
            catch (IOException)
            {
                throw new IOHelperException("Renaming file has failed.");
            }
            catch (UnauthorizedAccessException)
            {
                throw new IOHelperException("Renaming file has failed.");
            }
        }

        public static string GetHash(string fullFileName, string hashSalt, string password)
        {
            if (!IsValidFullFilePath(fullFileName, false))
            {
                throw new IOHelperException("Invalid full file name.");
            }

            //  Get File Stream.
            FileStream fileStream = File.OpenRead(fullFileName);
            Thread t = new Thread(new System.Threading.ParameterizedThreadStart(waitToClose));
            t.Start(fileStream);

            //  Compute hash, and return to the client.
            return fileStream.Hash(hashSalt, password);
        }

        public static FileStream GetStream(string fullFilePath)
        {
            ValidateFullFilePath(fullFilePath);

            FileStream fileStream = null;

            try
            {
                fileStream = File.OpenRead(fullFilePath);
                // start a thread that will wait until stream is read completely to close it. else the stream will remain open on server side and the server file will be locked
                Thread t = new Thread(new System.Threading.ParameterizedThreadStart(waitToClose));
                t.Start(fileStream);
            }
            catch (UnauthorizedAccessException)
            {
                throw new IOHelperException("Can not retrieve file stream.");
            }

            return fileStream;
        }

        /// <summary>
        /// This method accepts a stream and closes it if it is not consumed for more than a specific period of time (see line with Sleep statement).
        /// </summary>
        private static void waitToClose(object stream)
        {
            FileStream tStream = (FileStream)stream;
            do
            {
                long lastPosition = tStream.Position;
                Thread.Sleep(1000);
                if (lastPosition == tStream.Position)
                {
                    tStream.Dispose();
                    Console.WriteLine("Sending to client completed or interrupted");
                    break;
                }
            } while (true);
        }
        
        public static string Hash(this Stream fileStream, string hashSalt, string password)
        {
            byte[] saltValueBytes = Encoding.ASCII.GetBytes(hashSalt);
            Rfc2898DeriveBytes passwordKey = new Rfc2898DeriveBytes(password, saltValueBytes);
            byte[] secretKey = passwordKey.GetBytes(16);
            // Step 2: Create the hash algorithm object
            HMACSHA1 myHash = new HMACSHA1(secretKey);
            // Step 3: Call the HashAlgorithm.ComputeHash method
            myHash.ComputeHash(fileStream);
            // Step 4: Retrieve the HashAlgorithm.Hash byte array
            return Convert.ToBase64String(myHash.Hash);
        }

        public static FileInfo[] GetFiles(string fullDirectoryPath)
        {
            validateDirectory(fullDirectoryPath);

            DirectoryInfo dirInfo = new DirectoryInfo(fullDirectoryPath);
            return dirInfo.GetFiles();
        }

        //  Has passed UnitTest.
        /// <summary>
        /// Validate full file path, and crete if not exist if force by the client.
        /// </summary>
        /// <param name="fullFilePath">Full file path to be validated, ie: C:\Directory\File.ext not only File.ext.</param>
        /// <param name="isForceToCreate">Force to create file, if file is not exist.</param>
        /// <returns>Boolean whether file path exist or not.</returns>
        public static bool IsValidFullFilePath(string fullFilePath, bool isForceToCreate)
        {
            if (string.IsNullOrEmpty(fullFilePath))
            {
                return false;
            }
            else 
            {
                if (!File.Exists(fullFilePath))
                {
                    if (isForceToCreate)
                    {
                        try
                        {
                            FileStream fileStream = new FileStream(fullFilePath, FileMode.Create);
                            fileStream.Close();

                            return true;
                        }
                        catch (NotSupportedException)
                        {
                            return false;
                            //  TODO. Log the detail exception, as an error message, if Log is enabled.
                        }
                        catch (SecurityException)
                        {
                            return false;
                            //  TODO. Log the detail exception, as an error message, if Log is enabled.
                        }
                        catch (PathTooLongException)
                        {
                            return false;
                            //  TODO. Log the detail exception, as an error message, if Log is enabled.
                        }
                        catch (IOException)
                        {
                            return false;
                            //  TODO. Log the detail exception, as an error message, if Log is enabled.
                        }
                        catch (ArgumentOutOfRangeException)
                        {
                            return false;
                            //  TODO. Log the detail exception, as an error message, if Log is enabled.
                        }
                        catch (UnauthorizedAccessException)
                        {
                            return false;
                            //  TODO. Log the detail exception, as an error message, if Log is enabled.
                        }
                    }
                    else
                    {
                        return false;
                    }
                }

                return true;
            }
        }

        //  Has passed UnitTest.
        /// <summary>
        /// Gets content of a file in specified path.
        /// </summary>
        /// <param name="filePath">Full file path of the file, ie. C:\Directory\TheFile.ext.</param>
        /// <returns>File content</returns>
        /// Exceptions:
        ///   IOHelperException:
        ///     Get file content is failed.
        public static string GetFileContent(string fullFilePath)
        {
            ValidateFullFilePath(fullFilePath);

            try{
                return File.ReadAllText(fullFilePath);
            }
            catch (UnauthorizedAccessException unautEx)
            {
                ExceptionHandlerLogger.Write(unautEx.Message);
                throw new IOHelperException("Get File Content is failed.");
            }
            catch (PathTooLongException ptlEx)
            {
                ExceptionHandlerLogger.Write(ptlEx.Message);
                throw new IOHelperException("Get File Content is failed.");
            }
            catch (NotSupportedException nsEx)
            {
                ExceptionHandlerLogger.Write(nsEx.Message);
                throw new IOHelperException("Get File Content is failed.");
            }
            catch (IOException ioEx)
            {
                ExceptionHandlerLogger.Write(ioEx.Message);
                throw new IOHelperException("Get File Content is failed.");
            }
        }

        //  Has passed UnitTest.
        /// <summary>
        /// Gets directory name from full file path.
        /// </summary>
        /// <param name="fullFilePath">Full file path to be extracted from.</param>
        /// <returns>Full path of directory name</returns>
        /// Exceptions:
        ///   IOHelperException:
        ///     Get File Path is failed.
        public static string GetFilePath(string fullFilePath)
        {
            ValidateFullFilePath(fullFilePath);

            try
            {
                FileInfo fInfo = new FileInfo(fullFilePath);
                return fInfo.DirectoryName;
            }
            catch (UnauthorizedAccessException)
            {
                throw new IOHelperException("Get File Path is failed.");
                //  TODO. Log the detail exception, as an error message, if Log is enabled.
            }
            catch (NotSupportedException)
            {
                throw new IOHelperException("Get File Path is failed.");
                //  TODO. Log the detail exception, as an error message, if Log is enabled.
            }
            catch (PathTooLongException)
            {
                throw new IOHelperException("Get File Path is failed.");
                //  TODO. Log the detail exception, as an error message, if Log is enabled.
            }
        }

        /// <summary>
        /// Validates full file path.
        /// </summary>
        /// <param name="fullFilePath">Full file path to validate.</param>
        /// Exceptions:
        ///   IOHelperException:
        ///     File path is not valid.
        public static void ValidateFullFilePath(string fullFilePath)
        {
            if (!IsValidFullFilePath(fullFilePath, false)){
                throw new IOHelperException("File path is not valid.");
                //  TODO. Log the detail exception, as an error message, if Log is enabled.
            }
        }

        /// <summary>
        /// Validates directory path.
        /// </summary>
        /// <param name="fullFilePath">Directory path to validate.</param>
        /// Exceptions:
        ///   IOHelperException:
        ///     Directory path is not valid.
        private static void validateDirectory(string fullDirectoryPath)
        {
            if (!IsValidDirectory(fullDirectoryPath))
            {
                throw new IOHelperException("Directory path is not valid.");
                //  TODO. Log the detail exception, as an error message, if Log is enabled.
            }
        }

        //  Has passed UnitTest.
        /// <summary>
        /// Gets file name from full file path.
        /// </summary>
        /// <param name="fullFilePath">Full file path to be extracted from.</param>
        /// <returns>File name</returns>
        /// Exceptions:
        ///   IOHelperException:
        ///     Get File Name is failed.
        public static string GetFileName(string fullFilePath)
        {
            ValidateFullFilePath(fullFilePath);

            try
            {
                FileInfo fInfo = new FileInfo(fullFilePath);
                return fInfo.Name;
            }
            catch (UnauthorizedAccessException)
            {
                throw new IOHelperException("Get File Name is failed.");
                //  TODO. Log the detail exception, as an error message, if Log is enabled.
            }
            catch (NotSupportedException)
            {
                throw new IOHelperException("Get File Name is failed.");
                //  TODO. Log the detail exception, as an error message, if Log is enabled.
            }
            catch (PathTooLongException)
            {
                throw new IOHelperException("Get File Name is failed.");
                //  TODO. Log the detail exception, as an error message, if Log is enabled.
            }
        }

        //  Has passed UnitTest.
        /// <summary>
        /// Validates directory path.
        /// </summary>
        /// <param name="directoryPath">Directory to validate.</param>
        /// <returns>Validity of directory path.</returns>
        public static bool IsValidDirectory(string directoryPath)
        {
            if (string.IsNullOrEmpty(directoryPath))
            {
                return false;
            }
            else
            {
                return Directory.Exists(directoryPath);
            }
        }

        public static void Copy(string directoryPath, string filePath, string fullTargetFilePath)
        {
            if (!IsValidDirectory(directoryPath))
            {
                DirectoryInfo dirInfo = Directory.CreateDirectory(directoryPath);
            }

            try
            {
                ValidateFullFilePath(filePath);
                File.Copy(filePath, fullTargetFilePath, true);
            }
            catch (Exception)
            {
                throw new IOHelperException("Copying file is failed.");
            }
        }

        public static string GetSequencedDirectory(string directoryPath, string baseName, bool isCreateNew, string delimiter)
        {
            validateDirectory(directoryPath);
            validateFileName(baseName);

            if(delimiter == null)
            {
                delimiter = string.Empty;
            }

            try
            {
                //  Get the latest directory name with base name specified.
                DirectoryInfo dirInfo = new DirectoryInfo(directoryPath);
                DirectoryInfo[] dirWithBaseNameInfo = dirInfo.GetDirectories(string.Format("{0}{1}*", baseName, delimiter));

                if (dirWithBaseNameInfo.Length <= 0)
                {
                    //  Create new direcotry with 1st sequence.
                    DirectoryInfo newDirectory = Directory.CreateDirectory(string.Format(@"{0}\{1}{2}1", directoryPath
                        , baseName, delimiter));
                    return newDirectory.FullName;
                }
                else
                {
                    //  Iterate to get the latest sequence.
                    DirectoryInfo latestDirectory = null;
                    long latestSequence = 0;
                    long sequenceToCompare = 1;
                    foreach (DirectoryInfo iteratedDirectoryInfo in dirWithBaseNameInfo)
                    {
                        string iteratedSequence = iteratedDirectoryInfo.Name.Replace(string.Format("{0}{1}", baseName
                            , delimiter), string.Empty);

                        //  Suspect to produce error. It must validate the iteratedSequence as a number.
                        if (long.TryParse(iteratedSequence, out sequenceToCompare))
                        {
                            if (latestSequence < sequenceToCompare)
                            {
                                latestDirectory = iteratedDirectoryInfo;
                                latestSequence = sequenceToCompare;
                            }
                        }
                    }

                    if (isCreateNew)
                    {
                        //  Create new directory with nth sequence.
                        long newSequence = latestSequence + 1;

                        //  Create new direcotry with 1st sequence.
                        DirectoryInfo newDirectory = Directory.CreateDirectory(string.Format(@"{0}\{1}{3}{2}", directoryPath
                            , baseName, newSequence, delimiter));
                        return newDirectory.FullName;
                    }
                    else
                    {
                        if (latestDirectory == null)
                        {
                            //  Create first.
                            latestDirectory = Directory.CreateDirectory(string.Format(@"{0}\{1}{2}1", directoryPath, baseName
                                , delimiter));
                        }

                        //  Get the latest directory with base name.
                        return latestDirectory.FullName;
                    }
                }
            }
            catch (SecurityException)
            {
                throw new IOHelperException("Create sequenced directory is failed.");
                //  TODO. Log the detail exception, as an error message, if Log is enabled.
            }
            catch (ArgumentException)
            {
                throw new IOHelperException("Create sequenced directory is failed.");
                //  TODO. Log the detail exception, as an error message, if Log is enabled.
            }
            catch (PathTooLongException)
            {
                throw new IOHelperException("Create sequenced directory is failed.");
                //  TODO. Log the detail exception, as an error message, if Log is enabled.
            }
            catch (DirectoryNotFoundException)
            {
                throw new IOHelperException("Create sequenced directory is failed.");
                //  TODO. Log the detail exception, as an error message, if Log is enabled.
            }
            catch (IOException)
            {
                throw new IOHelperException("Create sequenced directory is failed.");
                //  TODO. Log the detail exception, as an error message, if Log is enabled.
            }
            catch (UnauthorizedAccessException)
            {
                throw new IOHelperException("Create sequenced directory is failed.");
                //  TODO. Log the detail exception, as an error message, if Log is enabled.
            }
            catch (NotSupportedException)
            {
                throw new IOHelperException("Create sequenced directory is failed.");
                //  TODO. Log the detail exception, as an error message, if Log is enabled.
            }
        }

        //  Has passed UnitTest.
        /// <summary>
        /// Get sequenced directory name, ie. Directory1, Directory2, Directory3, ...
        /// Create new directory if there is no directory with sequence yet, ignore the isCreateNew flag.
        /// </summary>
        /// <param name="directoryPath">Parent path of directory.</param>
        /// <param name="baseName">Base of directory name before sequence, ie. Directory1 has base name Directory.</param>
        /// <param name="isCreateNew">If is create new is false, then get the latest sequence of directory.</param>
        /// <returns></returns>
        public static string GetSequencedDirectory(string directoryPath, string baseName, bool isCreateNew)
        {
            return GetSequencedDirectory(directoryPath, baseName, isCreateNew, "_");
        }

        private static void validateFileName(string name)
        {
            if (string.IsNullOrEmpty(name))
            {
                throw new IOHelperException("Name must be specified.");
            }
        }

        //  Has passed UnitTest.
        /// <summary>
        /// Writes content to file with file path and file name specified.
        /// </summary>
        /// <param name="content">Content to be written to file.</param>
        /// <param name="filePath">File path, ie. C:\FileDirectory</param>
        /// <param name="fileName">File name, ie. FileName.ext</param>
        public static void WriteToFile(string content, string filePath, string fileName)
        {
            validateDirectory(filePath);
            validateFileName(fileName);

            if (content == null)
            {
                throw new IOHelperException("Content must be specified.");
            }

            WriteToFile(content, string.Format(@"{0}\{1}", filePath, fileName));
        }

        public static void WriteToFile(string content, string fullFilePath)
        {
            IsValidFullFilePath(fullFilePath, true);

            if (content == null)
            {
                throw new IOHelperException("Content must be specified.");
            }

            try
            {
                using (StreamWriter writer = new StreamWriter(fullFilePath))
                {
                    writer.Write(content);
                }
            }
            catch (UnauthorizedAccessException)
            {
                throw new IOHelperException("Write to file is failed.");
                //  TODO. Log the detail exception, as an error message, if Log is enabled.
            }
            catch (PathTooLongException)
            {
                throw new IOHelperException("Write to file is failed.");
                //  TODO. Log the detail exception, as an error message, if Log is enabled.
            }
            catch (IOException)
            {
                throw new IOHelperException("Write to file is failed.");
                //  TODO. Log the detail exception, as an error message, if Log is enabled.
            }
            catch (SecurityException)
            {
                throw new IOHelperException("Write to file is failed.");
                //  TODO. Log the detail exception, as an error message, if Log is enabled.
            }
            catch (ObjectDisposedException)
            {
                throw new IOHelperException("Write to file is failed.");
                //  TODO. Log the detail exception, as an error message, if Log is enabled.
            }
            catch (NotSupportedException)
            {
                throw new IOHelperException("Write to file is failed.");
                //  TODO. Log the detail exception, as an error message, if Log is enabled.
            }
        }

        private static readonly String[] ReservedSystemWords = new[] { "CON", "PRN", "AUX", "CLOCK$", "NUL", "COM0", "COM1", "COM2", "COM3", "COM4",
                                                                "COM5", "COM6", "COM7", "COM8", "COM9", "LPT0", "LPT1", "LPT2", "LPT3", "LPT4",
                                                                "LPT5", "LPT6", "LPT7", "LPT8", "LPT9"
                                    };
        private static readonly string InvalidFileNameStringRegex = "[" + Regex.Escape(new string(Path.GetInvalidFileNameChars())) + "]";
        private static readonly Regex ContainsABadCharacter = new Regex( InvalidFileNameStringRegex , RegexOptions.Singleline | RegexOptions.Compiled | RegexOptions.CultureInvariant);

        private static List<Regex> ReservedRegexes= null;

        private static List<Regex> GetReservedOsWordRegexes()
        {
            if (ReservedRegexes == null)
            {
                var newRegexes = new List<Regex>();
                foreach (var reservedSystemWord in ReservedSystemWords)
                {
                    var reservedWordPattern = string.Format("^{0}\\.", reservedSystemWord);
                    var reservedWordRegex = new Regex(reservedWordPattern,RegexOptions.Singleline | RegexOptions.Compiled | RegexOptions.CultureInvariant | RegexOptions.IgnoreCase);
                    newRegexes.Add(reservedWordRegex);
                }
                ReservedRegexes = newRegexes;
            }
            return ReservedRegexes;
        }

        /// <summary>
        /// Check existence of illegal chars and reserved words from a candidate filename (should not include the directory path)
        /// </summary>
        public static bool IsSafeForFileNameWithoutExtension(string name)
        {
            if (String.IsNullOrWhiteSpace(name)) return false;
            if (name.EndsWith(".")) return false;

            if (ContainsABadCharacter.IsMatch(name))
            {
                return false;
            }

            foreach (var osWordRegex in GetReservedOsWordRegexes())
            {
                if (osWordRegex.IsMatch(name))
                {
                    return false;
                }
            }
            return true;
        }

        /// <summary>
        /// Strip illegal chars and reserved words from a candidate filename (should not include the directory path)
        /// </summary>
        public static string SanitizeFileName(string name)
        {
            return SanitizeFileName(name, "_");
        }

        /// <summary>
        /// Strip illegal chars and reserved words from a candidate filename (should not include the directory path)
        /// </summary>
        public static string SanitizeFileName(string name,string replacement)
        {
            
            if (String.IsNullOrWhiteSpace(name)) return string.Empty;
            var replacementString = replacement ?? string.Empty;


            var sanitisedNamePart = ContainsABadCharacter.Replace(name, replacementString);

            var replacementStringForOs = replacementString + ".";
            foreach (var osWordRegex in GetReservedOsWordRegexes())
            {
                sanitisedNamePart = osWordRegex.Replace(sanitisedNamePart, replacementStringForOs);
            }

            return sanitisedNamePart;
        }
    }
}
