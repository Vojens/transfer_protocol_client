﻿using System;
using System.Collections.Generic;
using System.Linq;

namespace Support.Common
{
    public class DictionaryHelper
    {
        [Flags]
        public enum ComparisonLevel
        {
            None = 0,
            Keys = 2,
            Values = 4,
            All = 15
        }

        /// <summary>
        /// Checks if the dictionaries key-value pairs are the same
        /// For non-basic data-types(key or value), ensure the IComparable is implemented
        /// </summary>
        /// <param name="dictA"></param>
        /// <param name="dictB"></param>
        /// <param name="mismatchedElems">If function returns false, contains the first unmatched key-value pair</param>
        /// <param name="cmpLevel">Enum Flag allows to change the level of comparison, by default = ComparisonLevel.All = will check matching keys and values</param>
        public static bool AreEqual<TDictKey, TDictValuesType, TDictValuesInnerType>(IDictionary<TDictKey, TDictValuesType> dictA, IDictionary<TDictKey, TDictValuesType> dictB, out Dictionary<TDictKey, TDictValuesType> mismatchedElems, ComparisonLevel cmpLevel = ComparisonLevel.All) where TDictValuesType : IEnumerable<TDictValuesInnerType>
        {
            mismatchedElems = new Dictionary<TDictKey, TDictValuesType>();
            foreach (var kvp in dictA)
            {
                TDictValuesType dictBSet;
                if (dictB.TryGetValue(kvp.Key, out dictBSet))
                {
                    if ((cmpLevel & ComparisonLevel.Values) == ComparisonLevel.Values)
                    {
                        var diffrentialSet = kvp.Value.Except(dictBSet);
                        if (diffrentialSet.Count() > 0)
                        {
                            mismatchedElems.Add(kvp.Key, (TDictValuesType)diffrentialSet);
                            return false;
                        }
                    }
                }
                else
                {
                    if ((cmpLevel & ComparisonLevel.Keys) == ComparisonLevel.Keys)
                    {
                        mismatchedElems.Add(kvp.Key, kvp.Value);
                        return false;
                    }
                }
            }
            return true;
        }


        /// <summary>
        /// Checks if the dictionaries key-value pairs are the same
        /// <para>For non-basic data-types(key or value), ensure the IComparable is implemented</para>
        /// </summary>
        /// <param name="dictA"></param>
        /// <param name="dictB"></param>
        /// <param name="cmpLevel">Enum Flag allows to change the level of comparison, by default = :All = will check matching keys and values</param>
        public static bool AreEqual<TDictKey, TDictValuesType, TDictValuesInnerType>(IDictionary<TDictKey, TDictValuesType> dictA, IDictionary<TDictKey, TDictValuesType> dictB, ComparisonLevel cmpLevel = ComparisonLevel.All) where TDictValuesType : IEnumerable<TDictValuesInnerType>
        {
            foreach (var kvp in dictA)
            {
                TDictValuesType dictBSet;
                if (dictB.TryGetValue(kvp.Key, out dictBSet))
                {
                    if ((cmpLevel & ComparisonLevel.Values) == ComparisonLevel.Values)
                    {
                        var diffrentialSet = kvp.Value.Except(dictBSet);
                        if (diffrentialSet.Count() > 0)
                        {
                            return false;
                        }
                    }
                }
                else
                {
                    if ((cmpLevel & ComparisonLevel.Keys) == ComparisonLevel.Keys)
                    {
                        return false;
                    }
                }
            }
            return true;
        }

        /// <summary>
        /// Checks if the dictionaries key-value pairs are the same
        /// <para>For non-basic data-types(key or value), ensure the IComparable is implemented</para>
        /// </summary>
        /// <param name="dictA"></param>
        /// <param name="dictB"></param>
        /// <param name="mismatchedElems">If function returns false, contains the first unmatched key-value pair</param>
        /// <param name="cmpLevel">Enum Flag allows to change the level of comparison, by default = :All = will check matching keys and values</param>
        public static bool AreEqual<TDictKey, TDictValues>(ref IDictionary<TDictKey, IEnumerable<TDictValues>> dictA, ref IDictionary<TDictKey, IEnumerable<TDictValues>> dictB, out IEnumerable<TDictValues> mismatchedElems, ComparisonLevel cmpLevel = ComparisonLevel.All)
        {
            mismatchedElems = default(IEnumerable<TDictValues>);
            foreach (var kvp in dictA)
            {
                IEnumerable<TDictValues> dictBSet;
                if (dictB.TryGetValue(kvp.Key, out dictBSet))
                {
                    if ((cmpLevel & ComparisonLevel.Values) == ComparisonLevel.Values)
                    {
                        mismatchedElems = kvp.Value.Except(dictBSet);
                        if (mismatchedElems.Count() > 0)
                        {
                            return false;
                        }
                    }
                }
                else
                {
                    if ((cmpLevel & ComparisonLevel.Keys) == ComparisonLevel.Keys)
                    {
                        mismatchedElems = kvp.Value.Except(kvp.Value);
                        return false;
                    }
                }
            }
            return true;
        }

        /// <summary>
        /// Checks if the dictionaries key-value pairs are the same
        /// <para>For non-basic data-types(key or value), ensure the IComparable is implemented</para>
        /// </summary>
        /// <param name="dictA"></param>
        /// <param name="dictB"></param>
        /// <param name="cmpLevel">Enum Flag allows to change the level of comparison, by default = :All = will check matching keys and values</param>
        public static bool AreEqual<TDictKey, TDictValues>(ref IDictionary<TDictKey, IEnumerable<TDictValues>> dictA, ref IDictionary<TDictKey, IEnumerable<TDictValues>> dictB, ComparisonLevel cmpLevel = ComparisonLevel.All)
        {
            foreach (var kvp in dictA)
            {
                IEnumerable<TDictValues> dictBSet;
                if (dictB.TryGetValue(kvp.Key, out dictBSet))
                {
                    if ((cmpLevel & ComparisonLevel.Values) == ComparisonLevel.Values)
                    {
                        if (kvp.Value.Except(dictBSet).Count() > 0)
                        {
                            return false;
                        }
                    }
                }
                else
                {
                    if ((cmpLevel & ComparisonLevel.Keys) == ComparisonLevel.Keys)
                    {
                        return false;
                    }
                }
            }
            return true;
        }
    }
}
