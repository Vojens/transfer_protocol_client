﻿ 

using System;
using System.IO;
using System.IO.Compression;
using System.Text;
using System.Text.RegularExpressions;

namespace Support.Common.Compression
{
    public class CompressionHelper
    {
        public static bool IsBase64String(string s)
        {
            s = s.Trim();

            bool result = (s.Length % 4 == 0) && Regex.IsMatch(s, @"^[a-zA-Z0-9\+/]*={0,3}$", RegexOptions.None);
            // the above result if true may not be perfect always, hence try to decrypt and check for exceptions in Convert.FromBase64String
            return result;
        }
        
        public static string Decompress(string text)
        {
            byte[] bSource = Convert.FromBase64String(text);

            using (var inStream = new MemoryStream(bSource))
            using (var gzip = new GZipStream(inStream, CompressionMode.Decompress))
            using (var outStream = new MemoryStream())
            {
                gzip.CopyTo(outStream);
                return Encoding.UTF8.GetString(outStream.ToArray());
            }
        }

        public static string Compress(string text)
        {
            byte[] data = Encoding.UTF8.GetBytes(text);
            using (var input = new MemoryStream(data))
            using (var output = new MemoryStream())
            {
                using (var gzip = new GZipStream(output,
                    CompressionMode.Compress, true))
                {
                    input.CopyTo(gzip);
                }

                return Convert.ToBase64String(output.ToArray());
            }
        }
    }
}
