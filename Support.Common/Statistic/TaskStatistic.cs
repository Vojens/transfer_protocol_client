﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;

namespace Support.Common.Statistic
{
    /// <summary>
    /// Class For helping record of task satistic
    /// Author: Aristo
    /// </summary>
    public class TaskStatistic
    {
        public TaskStatistic()
        {
            SubTasks = new List<TaskStatistic>();
            Indentation = "...";
        }

        /// <summary>
        /// Name of the Task
        /// </summary>
        public string Name { get; set; }

        private TaskStates _state = TaskStates.Initial;
        public TaskStates State
        {
            get { return _state; }
            protected set { _state = value; }
        }

        protected DateTime CurrentStartTime;
        /// <summary>
        /// Reset timing and start
        /// </summary>
        public void StartTiming()
        {
            if(State == TaskStates.Initial)
            {
                CurrentStartTime = DateTime.Now;
                State = TaskStates.Running;
            }
            else if (State == TaskStates.Stopped)
            {
                CurrentStartTime = DateTime.Now;
                State = TaskStates.Running;
            }
            else
            {
                throw new InvalidOperationException("Before StartTiming, current state must be Initial or stopped.");
            }
        }

        /// <summary>
        /// Pause timing of main task, current Subtask is also paused
        /// </summary>
        public void PauseTiming()
        {
            if(State == TaskStates.Running)
            {
                PauseCurrentSubTask();
                TotalTime = TotalTime.Add(DateTime.Now - CurrentStartTime);
                State = TaskStates.Paused;
            }
            else if(State == TaskStates.Paused)
            {
                PauseCurrentSubTask();
            }
            else
            {
                throw new InvalidOperationException("Before PauseTiming, current state must be Running or Paused.");
            }
            
        }

        /// <summary>
        /// Resume tiing of main task, Subtask needs to be resumed separately
        /// </summary>
        public void ResumeTiming()
        {
            if (State == TaskStates.Paused)
            {
                CurrentStartTime = DateTime.Now;
                State = TaskStates.Running;
            }
            else if (State == TaskStates.Running)
            {
                //Just Ignore
            }
            else
            {
                throw new InvalidOperationException("Before ResumeTiming, current state must be Paused. Running is acceptable but just ignored.");
            }
        }

        /// <summary>
        /// Stop timing, will also stop current subtask
        /// </summary>
        public void StopTiming()
        {
            if(State == TaskStates.Running)
            {
                StopCurrentSubTask();
                TotalTime = TotalTime.Add(DateTime.Now - CurrentStartTime);
                State = TaskStates.Stopped;
            }
            else if (State == TaskStates.Paused)
            {
                StopCurrentSubTask();
                State = TaskStates.Stopped;
            }
            else if (State == TaskStates.Stopped)
            {
                StopCurrentSubTask();
            }
            else if(State ==TaskStates.Initial)
            {
                StopCurrentSubTask();
                State = TaskStates.Stopped;
            }
            else
            {
                throw new InvalidOperationException("Unable to stop timing");
            }
        }

        /// <summary>
        /// Reset Timing and clear all subtasks
        /// </summary>
        public void ResetTiming()
        {
            if(State != TaskStates.Stopped)
            {
                StopTiming();
            }

            TotalTime = new TimeSpan(0);
            CurrentSubTask = null;
            SubTasks.Clear();
            State = TaskStates.Initial;
        }

        /// <summary>
        /// Time of the Task
        /// </summary>
        public TimeSpan TotalTime { get; protected set; }

        /// <summary>
        /// List of Subtasks with time information
        /// </summary>
        public List<TaskStatistic> SubTasks { get; protected set; }

        /// <summary>
        /// Currently started subtask, can be null when there is no subtask
        /// </summary>
        public TaskStatistic CurrentSubTask { get; protected set; }

        /// <summary>
        /// Create new subtask and start it;
        /// </summary>
        /// <param name="name">Name for the new SubTask</param>
        public void StartSubTask(String name)
        {
            var newSubTask = new TaskStatistic() {Name = name};
            SubTasks.Add(newSubTask);
            CurrentSubTask = newSubTask;
            newSubTask.StartTiming();
        }

        /// <summary>
        /// Stop currently assigned sub Task
        /// </summary>
        public void StopCurrentSubTask()
        {
            if (CurrentSubTask != null) CurrentSubTask.StopTiming();
        }

        /// <summary>
        /// Stop currently assigned sub Task and  Create new subtask and start it;
        /// </summary>
        /// <param name="name">Name for the new SubTask</param>
        public void StopCurrentSubTaskAndStartAnotherSubtask(String name)
        {
            StopCurrentSubTask();
            StartSubTask(name);
        }

        /// <summary>
        /// Pause currently assigned sub Task
        /// </summary>
        public void PauseCurrentSubTask()
        {
            if (CurrentSubTask != null) CurrentSubTask.StopTiming();
        }

        public override string ToString()
        {
            var statBuilder = new StringBuilder();
            return ToString(0, statBuilder).ToString();
        }

        public StringBuilder ToString(int level, StringBuilder statBuilder)
        {
            if (statBuilder == null) statBuilder = new StringBuilder();

            //Add local information
            string indentationString = CreateIndentationString(level, Indentation);
            statBuilder.Append(indentationString).Append(Name).Append(" : ").AppendLine(TotalTime.TotalSeconds.ToString());

            var raisedLevel = level+1;
            if(SubTasks != null && SubTasks.Count > 0)
            {
                foreach (var taskTimeSpan in SubTasks)
                {
                    taskTimeSpan.ToString(raisedLevel, statBuilder);
                }
            }

            return statBuilder;
        }

        public string Indentation { get; set; }

        /// <summary>
        /// Create Indentation based on level and the indentation string
        /// </summary>
        /// <param name="level">desired level</param>
        /// <param name="singleIndentationString">Single indentation string per level</param>
        /// <returns>Indentation string</returns>
        public static string CreateIndentationString(int level,string singleIndentationString)
        {
            string indentationString;
            if (!String.IsNullOrEmpty(singleIndentationString))
            {
                var indentationBuilder = new StringBuilder();
                for (int i = 0; i < level; i++)
                {
                    indentationBuilder.Append(singleIndentationString);
                }
                indentationString = indentationBuilder.ToString();
            }
            else
            {
                indentationString = String.Empty;
            }
            return indentationString;
        }
    }

    public enum TaskStates
    {
        Stopped, Running, Paused, Initial
    }
}
