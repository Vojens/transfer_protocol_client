﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Net.NetworkInformation;

namespace Support.Common
{
    public class SystemInformation
    {
        public SystemInformation()
        {
            OperatingSystemInfo = GetOSInfo();
            OperatingSystemType = GetOSType();
            OperatingSystemPlatform = GetOSArchitecture().ToString();
            NetVersionInfo = GetNetVersion();
            OtherInformations = new List<string>();
        }

        public string OperatingSystemInfo { get; private set; }
        public string OperatingSystemType { get; private set; }
        public string OperatingSystemPlatform { get; private set; }
        public string NetVersionInfo { get; private set; }

        public List<string> OtherInformations { get; private set; }

        public static string GetEnvironmentInformation()
        {
            var environmentTb = new StringBuilder();
            try
            {
                if (Environment.Is64BitProcess) environmentTb.AppendLine("Application is running in 64 Bit Mode");
                else environmentTb.AppendLine("Application is running in 32 Bit Mode");
                
                environmentTb.AppendLine();

                environmentTb.Append("Machine Name: ").AppendLine(Environment.MachineName);
                environmentTb.Append("Operating System : ").Append(GetOSInfo()).Append(" [").Append(Environment.OSVersion.ToString()).Append("]").AppendLine();
                environmentTb.Append("Dot Net version : ").AppendLine(GetNetVersion());
                environmentTb.Append("CPU : ").AppendLine(Environment.ProcessorCount.ToString());
                environmentTb.AppendLine();

                environmentTb.Append("Working Set : ").AppendLine(Environment.WorkingSet.ToString());
                environmentTb.Append("System Page Size : ").AppendLine(Environment.SystemPageSize.ToString());
                environmentTb.Append("Current Path : ").AppendLine(Environment.CurrentDirectory);
                environmentTb.AppendLine("_______________");

                NetworkInterface[] networks = NetworkInterface.GetAllNetworkInterfaces();
                if(networks != null)
                {
                    environmentTb.AppendLine("Network Interface");
                    foreach (var networkInterface in networks)
                    {
                        environmentTb.Append("==");
                        environmentTb.Append(networkInterface.Name);
                        environmentTb.Append(",");
                        environmentTb.Append(networkInterface.Description);
                        environmentTb.AppendLine("==");
                        environmentTb.Append("Type: ");
                        environmentTb.Append(networkInterface.NetworkInterfaceType.ToString());
                        environmentTb.AppendLine();
                        environmentTb.Append("Status: ");
                        environmentTb.Append(networkInterface.OperationalStatus.ToString());
                        environmentTb.AppendLine();
                        var ipProperties = networkInterface.GetIPProperties();
                        if (ipProperties != null)
                        {
                            environmentTb.Append("DNS");
                            environmentTb.Append(" Enabled=").Append(ipProperties.IsDnsEnabled.ToString());
                            environmentTb.Append(" Dynamic=").AppendLine(ipProperties.IsDynamicDnsEnabled.ToString());
                            if (!string.IsNullOrEmpty(ipProperties.DnsSuffix))
                            {
                                environmentTb.Append(" Suffix=").Append(ipProperties.DnsSuffix.ToString());
                            }
                            
                            var dnsAddresses = ipProperties.DnsAddresses;
                            if (dnsAddresses != null && dnsAddresses.Count > 0)
                            {
                                environmentTb.Append("DNS Addr=");
                                bool first = true;
                                foreach (var dnsAddress in dnsAddresses)
                                {
                                    if (!first) environmentTb.Append("  >>  ");
                                    environmentTb.Append(dnsAddress.ToString());
                                    first = false;
                                }
                                environmentTb.AppendLine();
                            }
                            

                            var dhcpAddresses = ipProperties.DhcpServerAddresses;
                            if (dhcpAddresses != null && dhcpAddresses.Count > 0)
                            {
                                bool first = true;
                                environmentTb.Append("DHCP Addr=");
                                foreach (var dhcpAddress in dhcpAddresses)
                                {
                                    if (!first) environmentTb.Append("  >>  ");
                                    environmentTb.Append(dhcpAddress.ToString());
                                    first = false;
                                }
                                environmentTb.AppendLine();
                            }

                            IPv4InterfaceProperties ip4info = null;
                            try
                            {
                                ip4info = ipProperties.GetIPv4Properties();
                            }
                            catch (NetworkInformationException)
                            {
                                //Just ignore
                            }
                            
                            if(ip4info != null)
                            {
                                environmentTb.Append("IP4 ");
                                environmentTb.Append(" DHCP=").Append(ip4info.IsDhcpEnabled.ToString());
                                environmentTb.Append(" Forwarding=").Append(ip4info.IsForwardingEnabled.ToString());
                                environmentTb.Append(" WINS=").Append(ip4info.UsesWins.ToString());
                                environmentTb.Append(" Private Addressing Enabled=").Append(ip4info.IsAutomaticPrivateAddressingEnabled.ToString());
                                environmentTb.Append(" Active=").Append(ip4info.IsAutomaticPrivateAddressingActive.ToString());
                                environmentTb.Append(" Index=").Append(ip4info.Index);
                                environmentTb.Append(" MTU=").Append(ip4info.Mtu);
                                environmentTb.AppendLine();
                            }
                            IPv6InterfaceProperties ip6info = null;
                            try
                            {
                                ip6info = ipProperties.GetIPv6Properties();
                            }
                            catch (NetworkInformationException)
                            {
                                //Just ignore
                            }

                            if (ip6info != null)
                            {
                                environmentTb.Append("IP6 ");
                                environmentTb.Append(" Index=").Append(ip6info.Index);
                                environmentTb.Append(" MTU=").Append(ip6info.Mtu);
                                environmentTb.AppendLine();
                            }

                            var unicastAddrs = ipProperties.UnicastAddresses;
                            GetUnicastInformation(environmentTb, unicastAddrs);
                            var anycastAddrs = ipProperties.AnycastAddresses;
                            GetIpAddressInformation(environmentTb, anycastAddrs,"AnyCast");
                            var multicastAddrs = ipProperties.MulticastAddresses;
                            GetIpAddressInformation(environmentTb, multicastAddrs, "MultiCast");
                            var gatewayAddrs = ipProperties.GatewayAddresses;
                            GetIpAddressInformation(environmentTb, gatewayAddrs, "Gateway");
                            //gatewayAddrs.
                        }
                        environmentTb.AppendLine();
                    }
                }
                environmentTb.AppendLine("_______________");
            }
            catch (Exception e)
            {
                environmentTb.AppendLine("Fail to obtain full environment");
                environmentTb.AppendLine(e.ToString());
            }

            return environmentTb.ToString();
        }

        /// <summary>
        /// Create Unicast Information from UnicastIPAddressInformationCollection
        /// </summary>
        /// <param name="environmentTb">The string builder to be used, if null, it will be created</param>
        /// <param name="unicastAddrs">The collection of UnicastIPAddress</param>
        private static StringBuilder GetUnicastInformation(StringBuilder environmentTb, UnicastIPAddressInformationCollection unicastAddrs)
        {
            if(environmentTb== null) environmentTb = new StringBuilder();

            //Add Information
            if (unicastAddrs != null && unicastAddrs.Count > 0)
            {
                environmentTb.AppendLine("===Unicast===");
                foreach (var entry in unicastAddrs)
                {
                    var ip6StringAdded = false;
                    if(entry.Address.IsIPv6LinkLocal)
                    {
                        if(!ip6StringAdded)
                        {
                            environmentTb.Append("IP6");
                            ip6StringAdded = true;
                        }
                        environmentTb.Append(" LinkLocal");
                    }
                    if (entry.Address.IsIPv6Multicast)
                    {
                        if(!ip6StringAdded)
                        {
                            environmentTb.Append("IP6");
                            ip6StringAdded = true;
                        }
                        environmentTb.Append(" Multicast");
                    }
                    if (entry.Address.IsIPv6SiteLocal)
                    {
                        if(!ip6StringAdded)
                        {
                            environmentTb.Append("IP6");
                            ip6StringAdded = true;
                        }
                        environmentTb.Append(" SiteLocal");
                    }
                    if (entry.Address.IsIPv6Teredo)
                    {
                        if(!ip6StringAdded)
                        {
                            environmentTb.Append("IP6");
                            ip6StringAdded = true;
                        }
                        environmentTb.Append(" SiteLocal");
                    }

                    if(!ip6StringAdded)
                    {
                        environmentTb.Append("IP4");
                    }

                    environmentTb.Append(", ");
                    environmentTb.Append(entry.Address.ToString());
                                    
                    if(!ip6StringAdded)
                    {
                        environmentTb.Append(", Mask:");
                        if (entry.IPv4Mask != null)
                        {
                            environmentTb.Append(entry.IPv4Mask.ToString());
                        }
                        else
                        {
                            environmentTb.Append("NULL");
                        }
                    }
                    environmentTb.AppendLine();
                }
                //environmentTb.AppendLine("===End of Unicast===");
            }
            return environmentTb;
        }

        /// <summary>
        /// Create Unicast Information from UnicastIPAddressInformationCollection
        /// </summary>
        /// <param name="environmentTb">The string builder to be used, if null, it will be created</param>
        /// <param name="ipAddrs">The collection of UnicastIPAddress</param>
        private static StringBuilder GetIpAddressInformation(StringBuilder environmentTb, IPAddressInformationCollection ipAddrs, string segmentTitle)
        {
            if (environmentTb == null) environmentTb = new StringBuilder();

            //Add Information
            if (ipAddrs != null && ipAddrs.Count > 0)
            {
                environmentTb.Append("===");
                environmentTb.Append(segmentTitle);
                environmentTb.AppendLine("===");
                foreach (var entry in ipAddrs)
                {
                    var ip6StringAdded = false;
                    if (entry.Address.IsIPv6LinkLocal)
                    {
                        if (!ip6StringAdded)
                        {
                            environmentTb.Append("IP6");
                            ip6StringAdded = true;
                        }
                        environmentTb.Append(" LinkLocal");
                    }
                    if (entry.Address.IsIPv6Multicast)
                    {
                        if (!ip6StringAdded)
                        {
                            environmentTb.Append("IP6");
                            ip6StringAdded = true;
                        }
                        environmentTb.Append(" Multicast");
                    }
                    if (entry.Address.IsIPv6SiteLocal)
                    {
                        if (!ip6StringAdded)
                        {
                            environmentTb.Append("IP6");
                            ip6StringAdded = true;
                        }
                        environmentTb.Append(" SiteLocal");
                    }
                    if (entry.Address.IsIPv6Teredo)
                    {
                        if (!ip6StringAdded)
                        {
                            environmentTb.Append("IP6");
                            ip6StringAdded = true;
                        }
                        environmentTb.Append(" SiteLocal");
                    }

                    if (!ip6StringAdded)
                    {
                        environmentTb.Append("IP4");
                    }

                    environmentTb.Append(", ");
                    environmentTb.Append(entry.Address.ToString());
                    environmentTb.AppendLine();
                }
                environmentTb.Append("===End of ");
                environmentTb.Append(segmentTitle);
                environmentTb.AppendLine("===");
            }
            return environmentTb;
        }

        /// <summary>
        /// Create Unicast Information from UnicastIPAddressInformationCollection
        /// </summary>
        /// <param name="environmentTb">The string builder to be used, if null, it will be created</param>
        /// <param name="ipAddrs">The collection of UnicastIPAddress</param>
        private static StringBuilder GetIpAddressInformation(StringBuilder environmentTb, MulticastIPAddressInformationCollection ipAddrs, string segmentTitle)
        {
            if (environmentTb == null) environmentTb = new StringBuilder();

            //Add Information
            if (ipAddrs != null && ipAddrs.Count > 0)
            {
                environmentTb.Append("===");
                environmentTb.Append(segmentTitle);
                environmentTb.AppendLine("===");
                foreach (var entry in ipAddrs)
                {
                    var ip6StringAdded = false;
                    if (entry.Address.IsIPv6LinkLocal)
                    {
                        if (!ip6StringAdded)
                        {
                            environmentTb.Append("IP6");
                            ip6StringAdded = true;
                        }
                        environmentTb.Append(" LinkLocal");
                    }
                    if (entry.Address.IsIPv6Multicast)
                    {
                        if (!ip6StringAdded)
                        {
                            environmentTb.Append("IP6");
                            ip6StringAdded = true;
                        }
                        environmentTb.Append(" Multicast");
                    }
                    if (entry.Address.IsIPv6SiteLocal)
                    {
                        if (!ip6StringAdded)
                        {
                            environmentTb.Append("IP6");
                            ip6StringAdded = true;
                        }
                        environmentTb.Append(" SiteLocal");
                    }
                    if (entry.Address.IsIPv6Teredo)
                    {
                        if (!ip6StringAdded)
                        {
                            environmentTb.Append("IP6");
                            ip6StringAdded = true;
                        }
                        environmentTb.Append(" SiteLocal");
                    }

                    if (!ip6StringAdded)
                    {
                        environmentTb.Append("IP4");
                    }
                    
                    environmentTb.Append(", ");
                    environmentTb.Append(entry.Address.ToString());
                    environmentTb.AppendLine();
                }
                //environmentTb.Append("===End of ");
                //environmentTb.Append(segmentTitle);
                //environmentTb.AppendLine("===");
            }
            return environmentTb;
        }

        /// <summary>
        /// Create Unicast Information from UnicastIPAddressInformationCollection
        /// </summary>
        /// <param name="environmentTb">The string builder to be used, if null, it will be created</param>
        /// <param name="ipAddrs">The collection of UnicastIPAddress</param>
        private static StringBuilder GetIpAddressInformation(StringBuilder environmentTb, GatewayIPAddressInformationCollection ipAddrs, string segmentTitle)
        {
            if (environmentTb == null) environmentTb = new StringBuilder();

            //Add Information
            if (ipAddrs != null && ipAddrs.Count > 0)
            {
                environmentTb.Append("===");
                environmentTb.Append(segmentTitle);
                environmentTb.AppendLine("===");
                foreach (var entry in ipAddrs)
                {
                    var ip6StringAdded = false;
                    if (entry.Address.IsIPv6LinkLocal)
                    {
                        if (!ip6StringAdded)
                        {
                            environmentTb.Append("IP6");
                            ip6StringAdded = true;
                        }
                        environmentTb.Append(" LinkLocal");
                    }
                    if (entry.Address.IsIPv6Multicast)
                    {
                        if (!ip6StringAdded)
                        {
                            environmentTb.Append("IP6");
                            ip6StringAdded = true;
                        }
                        environmentTb.Append(" Multicast");
                    }
                    if (entry.Address.IsIPv6SiteLocal)
                    {
                        if (!ip6StringAdded)
                        {
                            environmentTb.Append("IP6");
                            ip6StringAdded = true;
                        }
                        environmentTb.Append(" SiteLocal");
                    }
                    if (entry.Address.IsIPv6Teredo)
                    {
                        if (!ip6StringAdded)
                        {
                            environmentTb.Append("IP6");
                            ip6StringAdded = true;
                        }
                        environmentTb.Append(" SiteLocal");
                    }

                    if (!ip6StringAdded)
                    {
                        environmentTb.Append("IP4");
                    }
                    
                    environmentTb.Append(", ");
                    environmentTb.Append(entry.Address.ToString());
                    environmentTb.AppendLine();
                }
                //environmentTb.Append("===End of ");
                //environmentTb.Append(segmentTitle);
                //environmentTb.AppendLine("===");
            }
            return environmentTb;
        }

        private static string GetNetVersion()
        {
            return Environment.Version.ToString();
        }

        private static int GetOSArchitecture()
        {
            return (Environment.Is64BitOperatingSystem ? 64 : 32);
        }

        private static string GetOSType()
        {
            //Get Operating system information.
            OperatingSystem os = Environment.OSVersion;
            //Get version information about the os.
            Version vs = os.Version;

            //Variable to hold our return value
            string operatingSystem = "";

            if (os.Platform == PlatformID.Win32Windows)
            {
                //This is a pre-NT version of Windows
                switch (vs.Minor)
                {
                    case 0:
                        operatingSystem = OSType.Win95;
                        break;
                    case 10:
                        if (vs.Revision.ToString() == "2222A")
                            operatingSystem = OSType.Win98SE;
                        else
                            operatingSystem = OSType.Win98;
                        break;
                    case 90:
                        operatingSystem = OSType.WinMe;
                        break;
                    default:
                        break;
                }
            }
            else if (os.Platform == PlatformID.Win32NT)
            {
                switch (vs.Major)
                {
                    case 3:
                        operatingSystem = OSType.WinNT351;
                        break;
                    case 4:
                        operatingSystem = OSType.WinNT40;
                        break;
                    case 5:
                        if (vs.Minor == 0)
                            operatingSystem = OSType.Win2000;
                        else
                            operatingSystem = OSType.WinXP;
                        break;
                    case 6:
                        if (vs.Minor == 0)
                            operatingSystem = OSType.WinVista;
                        else
                            operatingSystem = OSType.Win7;
                        break;
                    default:
                        break;
                }
            }

            return operatingSystem;
        }

        private static string GetOSInfo()
        {
            //Get Operating system information.
            OperatingSystem os = Environment.OSVersion;
            //Get version information about the os.
            Version vs = os.Version;

            //Variable to hold our return value
            string operatingSystem = GetOSType();

            //Make sure we actually got something in our OS check
            //We don't want to just return " Service Pack 2" or " 32-bit"
            //That information is useless without the OS version.
            if (operatingSystem != "")
            {
                //Got something.  Let's prepend "Windows" and get more info.
                operatingSystem = "Windows " + operatingSystem;
                //See if there's a service pack installed.
                if (os.ServicePack != "")
                {
                    //Append it to the OS name.  i.e. "Windows XP Service Pack 3"
                    operatingSystem += " " + os.ServicePack;
                }
                //Append the OS architecture.  i.e. "Windows XP Service Pack 3 32-bit"
                operatingSystem += " " + GetOSArchitecture().ToString() + "-bit";
            }
            //Return the information we've gathered.
            return operatingSystem;
        }
    }

    public class OSType
    {
        public const string Win95 = "95";
        public const string Win98SE = "98SE";
        public const string Win98 = "98";
        public const string WinMe = "Me";
        public const string WinNT351 = "NT 3.51";
        public const string WinNT40 = "NT 4.0";
        public const string Win2000 = "2000";
        public const string WinXP = "XP";
        public const string WinVista = "Vista";
        public const string Win7 = "7";
    }

}
