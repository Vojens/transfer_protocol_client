 

using System;
using System.Collections.Generic;
using System.IO;
using System.Linq;
using System.Text;
using System.Xml;

namespace Support.Common.Xml
{
    public class XmlUtilities
    {
        /// <summary>
        /// Get the path of the node from the root to self
        /// </summary>
        /// <param name="node"></param>
        /// <returns></returns>
        public static string GetNodePath(XmlNode node)
        {            
            string path = "";
            while (node.ParentNode.NodeType != XmlNodeType.Document)
            {
                if (node.NodeType != XmlNodeType.Text)
                {
                    path = @"\" + (string.IsNullOrEmpty(node.Prefix) ? string.Empty : node.Prefix+":") + node.Name + path;
                }
                node = node.ParentNode;
            }
            return path;
        }

        /// <summary>
        /// Will point to the root node (XmlNode) of the xml input
        /// </summary>
        /// <param name="xml"></param>
        /// <returns></returns>
        public static XmlNode GetFirstContentXmlNodeFromXml(ref string xml)
        {
            //create setting for reader tp parse through query
            XmlReaderSettings setting = new XmlReaderSettings();
            setting.ConformanceLevel = ConformanceLevel.Document;
            setting.IgnoreComments = true;
            setting.IgnoreWhitespace = true;
            setting.IgnoreProcessingInstructions = true;

            StringReader sr = new StringReader(xml);
            XmlReader xmlReader = XmlReader.Create(sr, setting);
            xmlReader.MoveToContent();

            XmlDocument xmlQuery = new XmlDocument();
            return xmlQuery.ReadNode(xmlReader);
        }

        /// <summary>
        /// Will point to the root node (XmlNode) of the xml input
        /// </summary>
        /// <param name="xml"></param>
        /// <returns></returns>
        public static XmlNode GetFirstContentXmlNodeFromXml(string xml)
        {
            //create setting for reader tp parse through query
            XmlReaderSettings setting = new XmlReaderSettings();
            setting.ConformanceLevel = ConformanceLevel.Document;
            setting.IgnoreComments = true;
            setting.IgnoreWhitespace = true;
            setting.IgnoreProcessingInstructions = true;

            StringReader sr = new StringReader(xml);
            XmlReader xmlReader = XmlReader.Create(sr, setting);
            xmlReader.MoveToContent();

            XmlDocument xmlQuery = new XmlDocument();
            return xmlQuery.ReadNode(xmlReader);
        }

        public static XmlNode MergeXmlDataNodeIntoTemplateNode(XmlNode templateNode, XmlNode dataNode, bool swapWrite = false, bool supressWarnings = false)
        {
            if(templateNode.Name.Equals(dataNode.Name))
            {
                if(!string.IsNullOrEmpty(dataNode.Value))
                {
                    templateNode.Value = dataNode.Value;
                }

                if (templateNode.Attributes != null)
                {
                    if (dataNode.Attributes != null)
                    {
                        foreach (XmlAttribute dataNodeAttrib in dataNode.Attributes)
                        {
                            if (!string.IsNullOrEmpty(dataNodeAttrib.Value))
                            {
                                if (templateNode.Attributes[dataNodeAttrib.Name] != null)
                                {
                                    templateNode.Attributes[dataNodeAttrib.Name].Value = dataNodeAttrib.Value;
                                }
                            }
                        }
                    }
                    else
                    {
                        if (!supressWarnings)
                        {
                            throw new Exception(
                                "Data has attributes that the template missed out, please revise the template.");
                        }
                    }
                }

                if (templateNode.HasChildNodes)
                {
                    foreach (XmlNode childInTemplate in templateNode.ChildNodes)
                    {
                        if (dataNode.HasChildNodes)
                        {
                            var childInData =
                                dataNode.ChildNodes.Cast<XmlNode>().FirstOrDefault(
                                    dataChild => dataChild.Name.Equals(childInTemplate.Name));
                            if (childInData != null)
                            {
                                MergeXmlDataNodeIntoTemplateNode(childInTemplate, childInData);
                            }
                        }
                    }
                }
            }
            else
            {
                if (templateNode.ParentNode != null)
                {
                    templateNode.ParentNode.AppendChild(dataNode);
                }
            }
            return templateNode;
        }
    }
}
