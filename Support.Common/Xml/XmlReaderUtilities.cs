 

using System;
using System.Collections.Generic;
using System.Text;
using System.Xml;

namespace Support.Common.Xml
{
    public class XmlReaderUtilities
    {
        /// <summary>
        /// This piece of code will read through until EndElement
        /// Used when we want to read an element which has child nodes, and skip it to the end without needing to traverse subchild
        /// </summary>
        /// <param name="reader"></param>
        public static void ReadBlockToEndElement(XmlReader xr)
        {
            if (xr.NodeType != XmlNodeType.Element)
            {
                throw new Exception("ReadBlock can only be used on XmlNodeType.Element");
            }
            if (xr.IsEmptyElement)
            {
                return;
            }
            string elName = xr.Name;
            while (!(xr.NodeType == XmlNodeType.EndElement && xr.Name == elName))
            {
                xr.Read();
            }
        }

        public static string GetElementContent(XmlReader xr)
        {
            if (xr.Read())
            {
                if (xr.NodeType == XmlNodeType.Text)
                {
                    return xr.Value;
                }
            }
            return String.Empty;
        }
    }
}
