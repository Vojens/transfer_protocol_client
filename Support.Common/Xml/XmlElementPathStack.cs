 

using System;
using System.Collections.Generic;
using System.Xml;
using System.Text;
using Support.Common.Exceptions;

namespace Support.Common.Xml
{
    /// <summary>
    /// Represent the Path during traversal of an XML Tree
    /// Stores previous paths in a FILO Stack which is pushed/poped
    /// </summary>
    public class XmlElementPathStack
    {
        int pathStartDepth = 1;
        public XmlElementPathStack(int StartingDepth)
        {
            pathStartDepth = StartingDepth;
        }

        private string currentElName = "";

        /// <summary>
        /// The full element path to the element reader currently in
        /// </summary>
        public string FullElementPath
        {
            get
            {
                string val = String.Empty;
                foreach (string x in path)
                {
                    val += x;
                }

                return val + currentElName;
            }
        }
        private List<string> path = new List<string>();

        /// <summary>
        /// Refresh the current ReaderPath
        /// </summary>
        public void Refresh(int depth, string name)
        {
            //Store from depth == 1, do not store the last to avoid add n remove
            try
            {
                if (depth == 1)
                {
                    path.Clear();
                    //path.Add(name);
                }
                else if (depth < pathStartDepth)
                {
                    //Do Nothing
                }
                else
                {
                    //When the nextNode is @ the same depth as the last one
                    if (depth == path.Count + pathStartDepth)
                    {
                        // remove the previous node as we encountered 
                        //if (path.Count > 0)
                        //{
                        //    int lastIndex = path.Count - 1;
                        //    path.RemoveAt(lastIndex);
                        //}
                    }
                    //When the nextNode is deeper than the last one
                    else if (depth > path.Count + pathStartDepth)
                    {
                        path.Add(currentElName);
                    }
                    //When the nextNode is lower than the last one
                    else if (depth < path.Count + pathStartDepth)
                    {
                        int lastIndex = path.Count - 1;
                        path.RemoveAt(lastIndex);
                    }
                }
                currentElName = "\\" + name;
            }
            catch (Exception ex)
            {
                throw new XmlReaderPathException(ex.Message);
            }
        }
    }
}
