 

using System;
using System.Collections.Generic;
using System.Text;
using System.Text.RegularExpressions;
using System.Xml;
using System.IO;

namespace Support.Common.Xml
{
    public class XmlCleaner
    {
        /// <summary>
        /// Cleans the string from XmlDeclaration and Namespace
        /// </summary>
        /// <param name="xml"></param>
        /// <returns></returns>
        public static void Clean(ref string xml)
        {
            using (MemoryStream ms = new MemoryStream())
            {
                MemoryStream ms2 = new MemoryStream(Encoding.Unicode.GetBytes(xml));
                XmlReader reader = XmlReader.Create(ms2);
                reader.MoveToContent();

                XmlTextWriterCleaner writer = new XmlTextWriterCleaner(ms, Encoding.Unicode);
                writer.Namespaces = false;
                writer.WriteNode(reader, true);
                writer.Flush();

                using (StreamReader sr = new StreamReader(ms))
                {
                    ms.Seek(0, SeekOrigin.Begin);
                    xml = sr.ReadToEnd();
                }
            }
        }

        public static void CleanOra(ref string xml)
        {
            using (MemoryStream ms = new MemoryStream())
            {
                
                MemoryStream ms2 = null;
                XmlReader reader;
                XmlTextWriterCleaner writer;
                try
                {
                    ms2 = new MemoryStream(Encoding.UTF8.GetBytes(xml));
                    reader = XmlReader.Create(ms2);
                    reader.MoveToContent();
                    writer = new XmlTextWriterCleaner(ms, Encoding.UTF8);
                }
                catch
                {
                    ms2 = new MemoryStream(Encoding.Unicode.GetBytes(xml));
                    reader = XmlReader.Create(ms2);
                    reader.MoveToContent();
                    writer = new XmlTextWriterCleaner(ms, Encoding.Unicode);
                }

                writer.Namespaces = false;
                writer.WriteNode(reader, true);
                writer.Flush();

                using (StreamReader sr = new StreamReader(ms))
                {
                    ms.Seek(0, SeekOrigin.Begin);
                    xml = sr.ReadToEnd();
                }
            }


        }

        /// <summary>
        /// Cleans the string from XmlDeclaration and Namespace
        /// </summary>
        /// <param name="xml"></param>
        /// <returns></returns>
        public static void CleanWithFormatted(ref string xml)
        {

            using (MemoryStream ms = new MemoryStream())
            {

                MemoryStream ms2 = null;
                XmlReader reader;
                XmlTextWriterCleaner writer;
                try
                {
                    ms2 = new MemoryStream(Encoding.UTF8.GetBytes(xml));
                    reader = XmlReader.Create(ms2);
                    reader.MoveToContent();
                    writer = new XmlTextWriterCleaner(ms, Encoding.UTF8);
                }
                catch
                {
                    ms2 = new MemoryStream(Encoding.Unicode.GetBytes(xml));
                    reader = XmlReader.Create(ms2);
                    reader.MoveToContent();
                    writer = new XmlTextWriterCleaner(ms, Encoding.Unicode);
                }

                writer.Formatting = Formatting.Indented;
                writer.Indentation = 2;
                writer.Namespaces = false;
                writer.WriteNode(reader, true);

                writer.Flush();

                using (StreamReader sr = new StreamReader(ms))
                {
                    ms.Seek(0, SeekOrigin.Begin);
                    xml = sr.ReadToEnd();
                }
            }

        }


        public static void CleanDeclaration(ref string xml)
        {
            string encoding = "<?xml version=\"1.0\" encoding=\"utf-8\"?>";
            xml = xml.Replace(encoding, String.Empty);
        }

        /// <summary>
        /// Returns string after Striping all xml namespace declarations from the Xml string input
        /// </summary>
        /// <param name="xml"></param>
        /// <returns></returns>
        public static string StripXmlNameSpaces(string xml)
        {
            const string strXMLPattern = @"xmlns(:\w+)?=""([^""]+)""|xsi(:\w+)?=""([^""]+)"""; // from maulidas code
            return Regex.Replace(xml, strXMLPattern, String.Empty);
        }
    }

    /// <summary>
    /// This class is used for Serialize without Declaration and xmlns
    /// </summary>
    public class XmlTextWriterCleaner : XmlTextWriter
    {
        bool _skip = false;

        public XmlTextWriterCleaner(TextWriter w)
            : base(w)
        {

        }
        public XmlTextWriterCleaner(Stream stream, Encoding encoding)
            : base(stream, encoding)
        {
        }

        public override void WriteStartDocument()
        {
            //Do Nothing
        }
        public override void WriteStartElement(string prefix, string localName, string ns)
        {
            base.WriteStartElement(null, localName, null);
        }
        public override void WriteStartAttribute(string prefix, string localName, string ns)
        {
            //xmlns:xsd && xmlns:xsi is to be omitted
            bool namespaceExtras = (prefix == "xmlns" && (localName == "xsd" || localName == "xsi"));
            if (namespaceExtras || localName == "xmlns" || localName == "version")
            {
                _skip = true;
                return;
            }
            base.WriteStartAttribute(null, localName, null);
        }
        public override void WriteEndAttribute()
        {
            if (_skip)
            {
                // Reset the flag, so we keep writing.
                _skip = false;
                return;
            }
            base.WriteEndAttribute();
        }
        public override void WriteString(string text)
        {
            if (_skip) return;
            base.WriteString(text);
        }
    }
}
