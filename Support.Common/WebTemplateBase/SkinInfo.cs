﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Xml.Serialization;

namespace Support.Common.Template
{
    [XmlRoot("SkinInfo")]
    public class SkinInfo
    {
        [XmlElement("CompanyName")]
        public string CompanyName { get; set; }

        [XmlElement("FullProductName")]
        public string FullProductName { get; set; }

        [XmlElement("FullProductNameBrowserTitle")]
        public string FullProductNameBrowserTitle { get; set; }

        [XmlElement("ShortProductName")]
        public string ShortProductName { get; set; }

        [XmlElement("ContactUrl")]
        public string ContactUrl { get; set; }

        [XmlIgnore]
        public string RegisteredMark { get; set; }
    }
}
