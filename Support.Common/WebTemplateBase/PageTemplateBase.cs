﻿using System;
using System.Web.UI;

namespace Support.Common.Template
{
    public class PageTemplateBase : UserControl
    {
        private PageBodyBase _pageBody = null;

        public virtual NamingPlaceHolder InnerContents
        {
            get { return null; }
        }

        public PageBodyBase PageBody
        {
            get { return _pageBody; }
            set { _pageBody = value; }
        }

        protected string ParseUrl(string value)
        {
            string url = value;
            if (string.Equals(url, "#", StringComparison.Ordinal) && Request.Url != null && !string.IsNullOrEmpty(Request.Url.AbsoluteUri))
            {
                url = Request.Url.AbsoluteUri + "#";
            }
            return url;
        }
    }
}