﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Web;
using System.Web.UI;

namespace Support.Common.Template
{
    public class PageBodyBase : Control, INamingContainer
    {
        private List<Control> _controls = new List<Control>();
        private string _templatePath = string.Empty;
        private bool _isTemplateLoaded = false;
        private bool _isChildControlsCreated = false;

        public PageTemplateBase Template { get; set; }

        public string TemplatePath
        {
            get { return _templatePath; }
            set
            {
                if (_isTemplateLoaded) throw new HttpException("The page template cannot be set more than once.");

                _templatePath = value;
                _isTemplateLoaded = true;
                if (Page != null) CreateChildControls();
            }
        }

        protected override void CreateChildControls()
        {
            if (!_isTemplateLoaded) return;
            if (_isChildControlsCreated) return;

            Template = Page.LoadControl(_templatePath) as PageTemplateBase;
            Template.ID = "_PageTemplate";
            Template.PageBody = this;

            foreach (object item in _controls)
            {
                Control child = item as Control;
                if (child != null) Template.InnerContents.Controls.Add(child);
            }

            Controls.Add(Template);

            _isChildControlsCreated = true;
        }

        protected override void AddParsedSubObject(object obj)
        {
            if (obj is Control) _controls.Add(obj as Control);
        }
    }
}