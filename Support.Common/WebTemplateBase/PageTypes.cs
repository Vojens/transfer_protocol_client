﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Web;

namespace Support.Common.Template
{
    public enum PageTypes
    {
        Login,
        BrowserCompatibility,
        Download,
        TermsCondition,
        DefaultIDP,
        Help,
        Thankyou,
        FileLink,
        ChangePassword,
        Message
    }
}