﻿ 

using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Reflection;
using System.Threading;

namespace Support.Common.Threading
{
    public class AsyncHelper
    {
        class TargetInfo
        {
            internal TargetInfo(Delegate d, object[] args)
            {
                Target = d;
                Args = args;
            }

            internal readonly Delegate Target;
            internal readonly object[] Args;
        }

        private static WaitCallback dynamicInvokeShim = new WaitCallback(DynamicInvokeShim);

        public static void FireAndForget(Delegate d, params object[] args)
        {
            ThreadPool.QueueUserWorkItem(dynamicInvokeShim, new TargetInfo(d, args));
        }

        static void DynamicInvokeShim(object o)
        {
            TargetInfo ti = (TargetInfo)o;
            ti.Target.DynamicInvoke(ti.Args);
        }

        /// <summary>
        /// Call asynchronous method that has BeginXXX / EndXXX pattern
        /// <see cref="http://marcgravell.blogspot.com/2009/02/async-without-pain.html"/>
        /// </summary>
        /// <typeparam name="T">result type</typeparam>
        /// <param name="begin">Asynchronous begin method that has signature IAsyncResult BeginXXX(AsyncCallback callback, object obj)</param>
        /// <param name="end">Asynchronous end method that has signature T EndXXX(IAsyncResul ar)</param>
        /// <param name="callback"></param>
        public static void RunAsync<T>(
            Func<AsyncCallback, object, IAsyncResult> begin,
            Func<IAsyncResult, T> end,
            Action<Func<T>> callback)
        {
            begin(ar =>
            {
                T result;
                try
                {
                    result = end(ar); // ensure end called
                    callback(() => result);
                }
                catch (Exception ex)
                {
                    callback(() => { throw ex; });
                }
            }, null);
        }
    }
}
