 

using System;
using System.Collections.Generic;
using System.Text;
using System.IO;
using System.Runtime.Serialization.Formatters.Binary;

namespace Support.Common.Clone
{
    public abstract class SerializeClone : ICloneable
    {
        public object Clone()
        {
            MemoryStream s = new MemoryStream();
            BinaryFormatter f = new BinaryFormatter();

            f.Serialize(s, this);
            s.Position = 0;
            object clone = f.Deserialize(s);
            return clone;
        }
    }
}
