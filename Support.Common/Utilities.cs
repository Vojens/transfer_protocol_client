using System;
using System.Collections.Generic;
using System.Text;
using System.Net.NetworkInformation;
using System.Net;
using System.Runtime.InteropServices;

namespace Support.Common
{
    /// <summary>
    /// Contains common operatations when working with Assembly
    /// </summary>
    public class AssemblyUtilities
    {
        /// <summary>
        /// Gets the directory where the assembly resides
        /// Guarentees its the source of the assemble(where it is on disk)
        /// </summary>
        /// <returns>The physical path to assembly directory</returns>
        public static string GetDirectory()
        {
            return System.IO.Path.GetDirectoryName(System.Reflection.Assembly.GetCallingAssembly().GetName().CodeBase);
        }
    }

    public class NetworkUtilities
    {

        public static string GetLocalIp()
        {
            string localIp = string.Empty;
            string ethernetIp = string.Empty;
            string wirelessIp = string.Empty;

            NetworkInterface[] networks = NetworkInterface.GetAllNetworkInterfaces();
            foreach (NetworkInterface network in networks)
            {

                //if ((network.NetworkInterfaceType.ToString().Contains("Ethernet")) && !network.Name.Contains("Virtual"))
                if ((network.OperationalStatus == OperationalStatus.Up) &&
                     (network.NetworkInterfaceType.ToString().ToLower().Contains("ethernet") || network.NetworkInterfaceType.ToString().ToLower().Contains("wireless")) &&
                     !(network.Name.ToLower().Contains("virtual") || network.Description.ToLower().Contains("virtual")))
                {
                    foreach (UnicastIPAddressInformation entry in network.GetIPProperties().UnicastAddresses)
                    {
                        if (!entry.Address.IsIPv6LinkLocal && !entry.Address.IsIPv6Multicast && !entry.Address.IsIPv6SiteLocal)
                        {
                            //localIp = entry.Address.ToString();
                            if (network.NetworkInterfaceType.ToString().ToLower().Contains("ethernet"))
                                ethernetIp = entry.Address.ToString();
                            else
                                wirelessIp = entry.Address.ToString();
                        }
                    }
                }
                //if (!String.IsNullOrEmpty(localIp))
                //    break;
            }

            if (string.IsNullOrEmpty(ethernetIp))
                localIp = wirelessIp;
            else
                localIp = ethernetIp;


            if (String.IsNullOrEmpty(localIp))
                localIp = System.Net.Dns.GetHostName();

            return localIp;
        }

        public static string GetClientIP()
        {
            string strHostName = Dns.GetHostName();

            IPHostEntry ipEntry = Dns.GetHostEntry(strHostName);
            IPAddress[] address = ipEntry.AddressList;

            return address[0].ToString();

        }
    }

    public class SQLGuidUtil
    {

        [DllImport("rpcrt4.dll", SetLastError = true)]

        static extern int UuidCreateSequential(out Guid guid);

        

        public static Guid NewSequentialId()
        {
            try
            {
                //const RPC_S_OK = 0;
                Guid guid;

                int result = UuidCreateSequential(out guid);

                if (result != 0)
                    return Guid.NewGuid();
                else
                {
                    var s = guid.ToByteArray();
                    var t = new byte[16];
                    t[3] = s[0];
                    t[2] = s[1];
                    t[1] = s[2];
                    t[0] = s[3];
                    t[5] = s[4];
                    t[4] = s[5];
                    t[7] = s[6];
                    t[6] = s[7];
                    t[8] = s[8];
                    t[9] = s[9];
                    t[10] = s[10];
                    t[11] = s[11];
                    t[12] = s[12];
                    t[13] = s[13];
                    t[14] = s[14];
                    t[15] = s[15];
                    return new Guid(t);
                }
            }
            catch
            {
                return Guid.NewGuid();
            }
            
            
        }

    }
}
