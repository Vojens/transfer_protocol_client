 

using System;
using System.Collections.Generic;
using System.Text;
using System.Threading;

namespace Support.Common
{
    public class ThreadHelper
    {
        public static void CreateThread(ThreadStart delegatedMethod, bool isJoin)
        {
            try
            {
                Thread methodThread = GetThread(delegatedMethod);
                if (isJoin)
                {
                    methodThread.Join();
                }
            }
            catch (ThreadHelperException theEx)
            {
                throw theEx;
            }
            catch (ThreadStateException tsEx)
            {
                throw new ThreadHelperException("CreateThread", tsEx.Message);
            }
            catch (ThreadInterruptedException tiEx)
            {
                throw new ThreadHelperException("CreateThread", tiEx.Message);
            }
        }

        public static Thread GetThread(ThreadStart delegatedMethod, string name)
        {
            try
            {
                Thread methodThread = GetThread(delegatedMethod);
                methodThread.Name = name;

                return methodThread;
            }
            catch (ThreadHelperException thEx)
            {
                throw thEx;
            }
        }

        public static Thread GetThread(ThreadStart delegatedMethod)
        {
            if (delegatedMethod == null)
            {
                throw new ThreadHelperException("GetThread", "Can not create thread, delegate method is null.");
            }

                Thread methodThread = new Thread(delegatedMethod);

                try
                {
                    methodThread.Start();
                    return methodThread;
                }
                catch (ThreadStateException tsEx)
                {
                    throw new ThreadHelperException("GetThread", tsEx.Message);
                }
                catch (OutOfMemoryException oomEx)
                {
                    throw new ThreadHelperException("GetThread", oomEx.Message);
                }
        }

        public static Thread PrepareThread(ThreadStart delegatedMethod)
        {
            if (delegatedMethod == null)
            {
                throw new ThreadHelperException("PrepareThread", "Can not prepare thread, delegate method is null.");
            }

            Thread methodThread = new Thread(delegatedMethod);
            return methodThread;
        }
    }
}
