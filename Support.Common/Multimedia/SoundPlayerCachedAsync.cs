﻿using System;
using System.Collections.Generic;
using System.Text;
using System.Runtime.InteropServices;
using System.IO;

namespace Support.Common.Multimedia
{
    public class SoundPlayerCachedAsync:IDisposable
    {
        [DllImport("winmm.dll", SetLastError = true)]
        public static extern bool PlaySound(byte[] ptrToSound,
           System.UIntPtr hmod, uint fdwSound);

        [DllImport("winmm.dll", SetLastError = true)]
        public static extern bool PlaySound(IntPtr ptrToSound,
           System.UIntPtr hmod, uint fdwSound);

        [DllImport("winmm.dll", SetLastError = true)]
        public static extern void Stop(IntPtr handle);

        private Dictionary<string, Byte[]> _soundCache;
        private Dictionary<string, GCHandle> _soundHandlesCache;

        public SoundPlayerCachedAsync()
        {
            _soundCache = new Dictionary<string, byte[]>();
            _soundHandlesCache = new Dictionary<string, GCHandle>();
        }


        public bool Play(string soundKey)
        {
            return Play(soundKey, SoundFlags.SND_MEMORY |
                                  SoundFlags.SND_ASYNC);
        }

        public bool PlayLooping(string soundKey)
        {
            return Play(soundKey, SoundFlags.SND_MEMORY |
                                  SoundFlags.SND_ASYNC | SoundFlags.SND_LOOP);
        }

        public void Stop(string soundKey)
        {
            GCHandle gcHandle;
            if (!String.IsNullOrEmpty(soundKey) && _soundHandlesCache.TryGetValue(soundKey, out gcHandle))
            {
                Stop(gcHandle.AddrOfPinnedObject());
            }
            else
            {
                Stop(IntPtr.Zero);
            }
        }

        public void StopAll()
        {
            PlaySound((byte[])null, UIntPtr.Zero, (uint)SoundFlags.SND_PURGE);
        }

        /// <summary>
        /// Play specified sound by the soundKey
        /// </summary>
        /// <param name="soundKey"></param>
        /// <param name="flags">Soundflags</param>
        /// <returns>False if unable to find the key, true otherwise</returns>
        public bool Play(string soundKey, SoundFlags flags)
        {
            //Or with Async and memory
            flags |= SoundFlags.SND_ASYNC;
            flags |= SoundFlags.SND_MEMORY;

            GCHandle gcHandle;
            if(!String.IsNullOrEmpty(soundKey) && _soundHandlesCache.TryGetValue(soundKey, out gcHandle))
            {
                PlaySound(gcHandle.AddrOfPinnedObject(),(UIntPtr)0, (uint)flags);
                return true;
            }
            else
            {
                PlaySound((byte[])null, (UIntPtr)0, (uint)flags);
                return false;
            }

        }

        /// <summary>
        /// Load sound stream with specified key
        /// </summary>
        /// <param name="soundKey">SoundKey for Discovering sound</param>
        /// <param name="stream">The stream of the sound</param>
        public void LoadSound(string soundKey, Stream stream)
        {
            if (String.IsNullOrEmpty(soundKey)) throw new ArgumentException("Sound Key for SoundPlayerCachedAsync registration may not be null!");
            if (stream == null) throw new ArgumentException("SoundPlayerCachedAsync can not register null stream!");
            var length = stream.Length;
            
            byte[] bytesToPlay = new byte[length];
            stream.Read(bytesToPlay, 0, (int)length);
            LoadSound(soundKey, bytesToPlay);
        }

        /// <summary>
        ///  Load sound bytes with specified key
        /// </summary>
        /// <param name="soundKey">SoundKey for Discovering sound</param>
        /// <param name="soundBytes">The bytes of the sound</param>
        public void LoadSound(string soundKey,byte[] soundBytes)
        {
            if(String.IsNullOrEmpty(soundKey)) throw new ArgumentException("Sound Key for SoundPlayerCachedAsync registration may not be null!");
            if(soundBytes == null) throw new ArgumentException("SoundPlayerCachedAsync can not register null soundBytes!");
            var length = soundBytes.LongLength;
            if(length < 1) throw new ArgumentException("SoundPlayerCachedAsync can not register empty soundBytes!");

            //Copy
            byte[] soundByteCopy = new byte[length];
            var newGcHandle = GCHandle.Alloc(soundByteCopy, GCHandleType.Pinned); //Pinning
            Array.Copy(soundBytes, soundByteCopy, length);

            //Register
            _soundCache.Add(soundKey, soundByteCopy);
            _soundHandlesCache.Add(soundKey, newGcHandle);
        }


        /// <summary>
        /// Dispose this object
        /// </summary>
        public void Dispose()
        {
            //Stop
            PlaySound((byte[])null, (UIntPtr)0, (uint)0);

            //Dispose every handle
            foreach (var handle in _soundHandlesCache.Values)
            {
                if(handle != null) handle.Free();
            }
            _soundHandlesCache.Clear();
            _soundHandlesCache = null;

        }
    }

    [Flags]
    public enum SoundFlags : int
    {
        SND_SYNC        = 0x0000,            // play synchronously (default)
        SND_ASYNC       = 0x0001,        // play asynchronously
        SND_NODEFAULT   = 0x0002,        // silence (!default) if sound not found
        SND_MEMORY      = 0x0004,        // pszSound points to a memory file
        SND_LOOP        = 0x0008,            // loop the sound until next sndPlaySound
        SND_NOSTOP      = 0x0010,        // don't stop any currently playing sound
        SND_PURGE       = 0x0040,        // purge non-static
        SND_APPLICATION = 0x0080,        // look for application-specific association
        SND_NOWAIT      = 0x00002000,        // don't wait if the driver is busy
        SND_ALIAS       = 0x00010000,        // name is a registry alias
        SND_ALIAS_ID    = 0x00110000,        // alias is a predefined id
        SND_FILENAME    = 0x00020000,        // name is file name
        SND_RESOURCE    = 0x00040004,     // name is resource name or atom 
        
        
    }
}
