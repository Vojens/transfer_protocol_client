﻿ 

using System.IO;
using System.Reflection;

namespace Support.Common
{
    public class EmbeddedResourceExtractor
    {
        /// <summary>
        /// Extracts a resources such as an html file or aspx page to the webroot directory
        /// and returns the filepath.
        /// </summary>
        /// <param name="resourceName">Name of the resource.</param>
        /// <param name="destinationFileName">Name of the destination file.</param>
        /// <returns></returns>
        public static string ExtractResourceToFile(string resourceName, string destinationFileName)
        {

            //NOTE: if you decide to drop this class into a separate assembly (for example, 
            //into a unit test helper assembly to make it more reusable), 
            //call Assembly.GetCallingAssembly() instead.
            Assembly a = Assembly.GetCallingAssembly();
            string[] list = a.GetManifestResourceNames();
            using (Stream stream = a.GetManifestResourceStream(resourceName))
            {
                using (StreamWriter outfile = File.CreateText(destinationFileName))
                {
                    using (StreamReader infile = new StreamReader(stream))
                    {
                        outfile.Write(infile.ReadToEnd());
                    }
                }
            }
            return destinationFileName;
        }

        private static string extractResourceToString(Assembly a, string resourceName)
        {
            string resourceString;

            using (Stream stream = a.GetManifestResourceStream(resourceName))
            {
                using (StringWriter outString = new StringWriter())
                {
                    using (StreamReader infile = new StreamReader(stream))
                    {
                        outString.Write(infile.ReadToEnd());
                    }
                    resourceString = outString.ToString();
                }
            }
            return resourceString;
        }

        public static string ExtractResourceToString(string resourceName)
        {
            return extractResourceToString(Assembly.GetCallingAssembly(), resourceName);
        }

        public static string ExtractResourceToString(string resourceName, string assemblyName)
        {
            return extractResourceToString(Assembly.Load(assemblyName), resourceName);
        }
    }
}
