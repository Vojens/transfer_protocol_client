﻿ 

using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Runtime.InteropServices;

namespace Support.Common
{
    public class DEPHandler
    {
        [DllImport("Kernel32.dll", CharSet = CharSet.Unicode, SetLastError = true, ThrowOnUnmappableChar = true)]
        public static extern bool GetModuleHandleEx(UInt32 dwFlags,string lpModuleName,out IntPtr phModule);

        [DllImport("Kernel32.dll", CharSet = CharSet.Unicode, SetLastError = true, ThrowOnUnmappableChar = true)]
        public static extern IntPtr GetProcAddress(IntPtr hModule, string lpProcName);

        public delegate bool SetProcessDEPPolicyDelegate(uint dwDEPPolicy);

        public bool SetDEPPolicy(uint dwDEPPolicy)
        {
            IntPtr hMod = IntPtr.Zero;
            if (GetModuleHandleEx(0, "Kernel32.dll", out hMod) == false) return false;

            IntPtr ipSetProcessDEPPolicy = GetProcAddress(hMod, "SetProcessDEPPolicy");
            if (ipSetProcessDEPPolicy == IntPtr.Zero) return false;

            SetProcessDEPPolicyDelegate setProcessDEPPolicy =
                (SetProcessDEPPolicyDelegate)Marshal.GetDelegateForFunctionPointer(ipSetProcessDEPPolicy, typeof(SetProcessDEPPolicyDelegate));
            
            return setProcessDEPPolicy(dwDEPPolicy);
        }

        public bool SetDEPPolicyEnableDEPAndDisallowATLThunks()
        {
            return SetDEPPolicy(DEPHandlerConstants.PROCESS_DEP_ENABLE |
                DEPHandlerConstants.PROCESS_DEP_DISABLE_ATL_THUNK_EMULATION);
        }
    }

    public class DEPHandlerConstants
    {
        public const uint PROCESS_DEP_ENABLE = 0x00000001;
        public const uint PROCESS_DEP_DISABLE_ATL_THUNK_EMULATION = 0x00000002;
    }
}