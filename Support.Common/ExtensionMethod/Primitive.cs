﻿ 

using System;
using System.Collections.Generic;
using System.Text;
using System.Xml.Linq;

namespace Support.Common.ExtensionMethod
{
    public static class Primitive
    {
        public static string ToStringBytes(this Guid g)
        {
            StringBuilder sb = new StringBuilder();
            byte[] bytes = g.ToByteArray();
            foreach (byte b in bytes)
            {
                sb.Append(b.ToString("X2"));
            }

            return sb.ToString();
        }

        public static string[] XElementsToStrings(this IEnumerable<XElement> xElements)
        {
            List<string> strings = new List<string>();

            foreach (string xElement in xElements)
            {
                strings.Add(xElement);
            }

            return strings.ToArray();
        }


    }
}
