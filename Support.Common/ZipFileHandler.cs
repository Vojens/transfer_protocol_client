﻿using System.IO;
using ICSharpCode.SharpZipLib.Core;
using ICSharpCode.SharpZipLib.Zip;
using System.Collections.Generic;

namespace Support.Common
{
    public class ZipFileHandler
    {
        private string _zipFileName;
        private long _maxBufferSize;

        public ZipFileHandler(string zipFileName)
        {
            this._zipFileName = zipFileName;
            this._maxBufferSize = 4096;
        }

        public ZipFileHandler(string zipFileName, long maxBufferSize)
        {
            this._zipFileName = zipFileName;
            this._maxBufferSize = maxBufferSize;
        }

        /// <summary>
        /// Extract the zipped file to destination path
        /// </summary>
        /// <param name="destinationPath">Path where the file will be extracted</param>
        public void ExtractTo(string destinationPath)
        {
            ZipFile zipFile = new ZipFile(this._zipFileName);
            try
            {
                foreach (ZipEntry entry in zipFile)
                {
                    string destinationFile = string.Empty;
                    string entryName = entry.Name.Replace("/", "\\");

                    byte[] buffer = new byte[4096];
                    Stream zipStream = zipFile.GetInputStream(entry);

                    if (entry.IsDirectory)
                    {
                        string dir = destinationPath + "\\" + entry.Name.Replace("/", "\\");
                        Directory.CreateDirectory(dir);
                    }
                    else
                    {
                        destinationFile = destinationPath + entryName;
                        string dir = Path.GetDirectoryName(destinationFile);
                        if (dir.Length > 0)
                            Directory.CreateDirectory(dir);
                        using (FileStream streamWriter = File.Create(destinationFile))
                        {
                            StreamUtils.Copy(zipStream, streamWriter, buffer);
                        }
                    }

                }
            }
            catch (ZipException zipEx)
            {
                throw zipEx;
            }
            finally
            {
                zipFile.Close();
            }
        }

        public List<string> GetDirectories()
        {
            List<string> listOfDirectory = new List<string>();

            ZipFile zipFile = new ZipFile(this._zipFileName);
            
            foreach (ZipEntry zipEntry in zipFile)
            {
                if (zipEntry.IsDirectory)
                {
                    listOfDirectory.Add(zipEntry.Name);
                }
            }

            zipFile.Close();

            return listOfDirectory;
        }
    }
}
