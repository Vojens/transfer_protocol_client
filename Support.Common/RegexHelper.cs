 

using System;
using System.Collections.Generic;
using System.Text.RegularExpressions;
using Support.Common.Logging;

namespace Support.Common
{
    public static class RegexHelper
    {
        /// <summary>
        /// Extract input string with specific format defined in pattern as a replacement form.
        /// i.e: Extract(@"^__(?<recordNumber>\d+)?__", "${recordNumber}", "__02__") will return "02".
        /// </summary>
        /// <param name="pattern">Specific format that input must fulfilled.</param>
        /// <param name="replacement">Form from the specific pattern format that expect to retrieve.</param>
        /// <param name="input">String that will be extracted.</param>
        /// <returns></returns>
        public static string Extract(string pattern, string replacement, string input)
        {
            if (string.IsNullOrEmpty(pattern))
            {
                throw new RegexHelperException("Pattern can not be empty.");
            }

            if (string.IsNullOrEmpty(replacement))
            {
                throw new RegexHelperException("Replacement can not be empty.");
            }

            if (string.IsNullOrEmpty(input))
            {
                throw new RegexHelperException("Input can not be empty.");
            }

            try
            {
                Regex r = new Regex(pattern, RegexOptions.Compiled);
                return r.Match(input).Result(replacement);
            }
            catch (ArgumentOutOfRangeException argOoRangeEx)
            {
                ExceptionHandlerLogger.Write(argOoRangeEx.Message);
                throw new RegexHelperException("Regular expression extraction is failed.");
            }
            catch (ArgumentException argEx)
            {
                ExceptionHandlerLogger.Write(argEx.Message);
                throw new RegexHelperException("Regular expression extraction is failed.");
            }
            catch (NotSupportedException notSupEx)
            {
                ExceptionHandlerLogger.Write(notSupEx.Message);
                throw new RegexHelperException("Regular expression extraction is failed.");
            }
        }

        internal static string Extract(string m_fileNamePattern, object fileName)
        {
            throw new Exception("The method or operation is not implemented.");
        }
    }
}
