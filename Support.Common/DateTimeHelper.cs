 

using System;
using System.Collections.Generic;
using System.Diagnostics.CodeAnalysis;
using System.Globalization;
using System.Text;

namespace Support.Common
{
    public class DateTimeHelper
    {
        /// <summary>    
        /// Converts a DateTime to an ISO 8601 datetime string with timezone indicator    
        /// See http://en.wikipedia.org/wiki/ISO_8601#Time_zones for info    
        /// </summary>    
        /// <param name="dateTimeToConvert">The DateTime to be converted</param>
        public static string DateTimeString(DateTime dateTimeToConvert) 
        { 
            TimeSpan timeSpan = TimeZone.CurrentTimeZone.GetUtcOffset(dateTimeToConvert); 
            if (timeSpan.TotalMilliseconds == 0)            
                dateTimeToConvert = dateTimeToConvert.ToUniversalTime(); 
            return dateTimeToConvert.ToString("o"); 
        }

        /// <summary>
        /// Add time to date.
        /// </summary>
        /// <param name="timesInString">Time in format in HH:MM, i.e.: 02:00</param>
        /// <param name="dt">Datetime as the target for the time to be added.</param>
        /// <returns></returns>
        public static DateTime AddTime(string timesInString, DateTime dt)
        {
            double[] times = new double[2];
            times[0] = Double.Parse(timesInString.Substring(0, 2));
            times[1] = Double.Parse(timesInString.Substring(2, 2));
            return Parser.SetKindToUtc(dt.AddHours(times[0]).AddMinutes(times[1]));
        }

        public static string GetDatetimeFormatFromShortAlias(char shortAliasCharacter, ref DateTimeFormatInfo dateTimeFormatInfoOutput)
        {
            switch (shortAliasCharacter)
            {
                case 'D':
                    {
                        return dateTimeFormatInfoOutput.LongDatePattern;
                    }
                case 'F':
                    {
                        return dateTimeFormatInfoOutput.FullDateTimePattern;
                    }
                case 'G':
                    {
                        return string.Concat(dateTimeFormatInfoOutput.ShortDatePattern, " ",
                                             dateTimeFormatInfoOutput.LongTimePattern);
                    }
                case 'M':
                    {
                        return dateTimeFormatInfoOutput.MonthDayPattern;
                    }
                case 'O':
                    {
                        dateTimeFormatInfoOutput = DateTimeFormatInfo.InvariantInfo;
                        return "yyyy'-'MM'-'dd'T'HH':'mm':'ss.fffffffK";
                    }
                case 'R':
                    {
                        dateTimeFormatInfoOutput = DateTimeFormatInfo.InvariantInfo;
                        return dateTimeFormatInfoOutput.RFC1123Pattern;
                    }
                case 'T':
                    {
                        return dateTimeFormatInfoOutput.LongTimePattern;
                    }
                case 'U':
                    {
                        dateTimeFormatInfoOutput = DateTimeFormatInfo.InvariantInfo;
                        return dateTimeFormatInfoOutput.FullDateTimePattern;
                    }
                case 'Y':
                    {
                        return dateTimeFormatInfoOutput.YearMonthPattern;
                    }
                case 'd':
                    {
                        return dateTimeFormatInfoOutput.ShortDatePattern;
                    }
                case 'f':
                    {
                        return string.Concat(dateTimeFormatInfoOutput.LongDatePattern, " ",
                                             dateTimeFormatInfoOutput.ShortTimePattern);
                    }
                case 'g':
                    {
                        return string.Concat(dateTimeFormatInfoOutput.ShortDatePattern, " ",
                                             dateTimeFormatInfoOutput.ShortTimePattern);
                    }
                case 'm':
                    {
                        return dateTimeFormatInfoOutput.MonthDayPattern;
                    }
                case 'o':
                    {
                        dateTimeFormatInfoOutput = DateTimeFormatInfo.InvariantInfo;
                        return "yyyy'-'MM'-'dd'T'HH':'mm':'ss.fffffffK";
                    }
                case 'r':
                    {
                        dateTimeFormatInfoOutput = DateTimeFormatInfo.InvariantInfo;
                        return dateTimeFormatInfoOutput.RFC1123Pattern;
                    }
                case 's':
                    {
                        dateTimeFormatInfoOutput = DateTimeFormatInfo.InvariantInfo;
                        return dateTimeFormatInfoOutput.SortableDateTimePattern;
                    }
                case 't':
                    {
                        return dateTimeFormatInfoOutput.ShortTimePattern;
                    }
                case 'u':
                    {
                        dateTimeFormatInfoOutput = DateTimeFormatInfo.InvariantInfo;
                        return dateTimeFormatInfoOutput.UniversalSortableDateTimePattern;
                    }
                case 'y':
                    {
                        return dateTimeFormatInfoOutput.YearMonthPattern;
                    }
                default:
                    {
                        throw new FormatException("Invalid format alias character specified");
                    }
            }
            
        }

        /// <summary>
        /// Convert a datestring using  Nullable DateTime to specified string Format using Current Culture
        /// </summary>
        /// <param name="dTim">Input DateTime</param>
        /// <param name="stringFormat">String Format of the output string</param>
        /// <returns>
        /// Null if dTim has no value.
        /// Otherwise, a datetime representation in the string Format
        /// </returns>
        public static string ConvertFromNullableDateTimeToCurrentCultureString(DateTime? dTim, string stringFormat)
        {
            if (!dTim.HasValue)
            {
                return null;
            }
            var value = dTim.Value;
            return value.ToString(stringFormat, CultureInfo.CurrentCulture);
        }

        public static DateTime? ConvertFromCurrentCultureStringToNullableDateTime(string inputDatetimeString, string inputFormat)
        {
            if (string.IsNullOrEmpty(inputDatetimeString))
            {
                return null;
            }
            var formats = new string[] { inputFormat, "g", "G", "f", "F", "d", "D", "t", "T", "o", "O", "r", "R", "m", "M", "y", "Y" };
            return DateTime.ParseExact(inputDatetimeString, formats, CultureInfo.CurrentCulture, DateTimeStyles.AllowWhiteSpaces);
        }
    }
}
