﻿ 

using System;
using System.Collections.Generic;
using System.Text;
using System.Collections.Specialized;

namespace Support.Common
{
    public static class ConfigHelper
    {
        public static bool IsKeyExist(this NameValueCollection configSetting, string key)
        {
            return (configSetting.HasKeys() && !string.IsNullOrEmpty(configSetting.Get(key)));
        }
    }
}
