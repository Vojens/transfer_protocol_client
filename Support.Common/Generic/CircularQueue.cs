﻿ 

using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;

namespace Support.Common.Generic
{
    public class CircularQueue<T>
    {
        private T[] items;

        private int firstItemIndex = -1;
        private int lastItemIndex = -1;
        private int max;

        public CircularQueue(int max)
        {
            this.max = max;
            items = new T[max];
        }

        public void Enqueue(T data)
        {
            if (firstItemIndex == -1)
            {
                firstItemIndex = 0;
            }

            lastItemIndex++;
            if (lastItemIndex == max - 1)
            {
                lastItemIndex = 0;
                firstItemIndex++;
            }
            if (lastItemIndex < firstItemIndex)
            {
                firstItemIndex++;
            }
            if (firstItemIndex == max - 1)
            {
                firstItemIndex = 0;
            }

            items[lastItemIndex] = data;
        }

        public T Dequeue()
        {
            T item = LastItem;
            if (!item.Equals(default(T)))
            {
                lastItemIndex--;
                firstItemIndex--;
            }
            return item;
        }


        public T FirstItem
        {
            get { return firstItemIndex >= 0 ? items[firstItemIndex] : default(T); }
        }

        public T LastItem
        {
            get { return lastItemIndex >= 0 ? items[lastItemIndex] : default(T); }
        }

        public int Count
        {
            get
            {
                if (lastItemIndex == -1) return 0;
                if (lastItemIndex >= firstItemIndex) return lastItemIndex - firstItemIndex + 1;
                return max;
            }
        }

        public TResult Sum<TResult>(Func<T, TResult> accumulator)
        {
            TResult res = default(TResult);
            foreach (T item in items)
            {
                res = accumulator(item);
            }
            return res;
        }

    }
}
