﻿ 

using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Reflection;

namespace Support.Common.Generic
{
    public class AssemblyInfo
    {
        /// <summary>
        /// Gets the name of the assembly.
        /// </summary>
        /// <value>The name of the assembly.</value>
        public static String AssemblyName<T>()
        {
            return typeof(T).Assembly.GetName().Name.ToString();
        }

        /// <summary>
        /// Gets the full name of the assembly.
        /// </summary>
        /// <value>The full name of the assembly.</value>
        public static String AssemblyFullName<T>()
        {
            return typeof(T).Assembly.GetName().FullName.ToString();
        }

        /// <summary>
        /// Gets the code base.
        /// </summary>
        /// <value>The code base.</value>
        public static String CodeBase<T>()
        {
            return typeof(T).Assembly.CodeBase;
        }

        /// <summary>
        /// Gets the copyright.
        /// </summary>
        /// <value>The copyright.</value>
        public static String Copyright<T>()
        {
                Type att = typeof(AssemblyCopyrightAttribute);
                object[] r = typeof(T).Assembly.GetCustomAttributes(att, false);
                AssemblyCopyrightAttribute copyattr = (AssemblyCopyrightAttribute)r[0];
                return copyattr.Copyright;
        }

        /// <summary>
        /// Gets the company.
        /// </summary>
        /// <value>The company.</value>
        public static String Company<T>()
        {
                Type att = typeof(AssemblyCompanyAttribute);
                object[] r = typeof(T).Assembly.GetCustomAttributes(att, false);
                AssemblyCompanyAttribute compattr = (AssemblyCompanyAttribute)r[0];
                return compattr.Company;
        }

        /// <summary>
        /// Gets the description.
        /// </summary>
        /// <value>The description.</value>
        public static String Description<T>()
        {
                Type att = typeof(AssemblyDescriptionAttribute);
                object[] r = typeof(T).Assembly.GetCustomAttributes(att, false);
                AssemblyDescriptionAttribute descattr = (AssemblyDescriptionAttribute)r[0];
                return descattr.Description;
        }

        /// <summary>
        /// Gets the product.
        /// </summary>
        /// <value>The product.</value>
        public static String Product<T>()
        {
                Type att = typeof(AssemblyProductAttribute);
                object[] r = typeof(T).Assembly.GetCustomAttributes(att, false);
                AssemblyProductAttribute prodattr = (AssemblyProductAttribute)r[0];
                return prodattr.Product;
        }

        /// <summary>
        /// Gets the title.
        /// </summary>
        /// <value>The title.</value>
        public static String Title<T>()
        {
                Type att = typeof(AssemblyTitleAttribute);
                object[] r = typeof(T).Assembly.GetCustomAttributes(att, false);
                AssemblyTitleAttribute titleattr = (AssemblyTitleAttribute)r[0];
                return titleattr.Title;
        }

        /// <summary>
        /// Gets the version.
        /// </summary>
        /// <value>The version.</value>
        public static String Version<T>()
        {
                return typeof(T).Assembly.GetName().Version.ToString();
        }
    }
}
