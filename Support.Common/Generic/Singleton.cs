﻿ 

using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;

namespace Support.Common.Generic
{
    /// <summary>
    /// Generic Singleton instance creator 
    /// </summary>
    /// <typeparam name="T">Class that has to have default constructor</typeparam>
    public sealed class Singleton<T> where T : new()
    {   
        class SingletonCreator
        {
            static SingletonCreator() { }

            internal static readonly T instance = new T();
        }

        Singleton() { }
        public static T Instance
        {
            get
            {
                return SingletonCreator.instance;
            }
        }
    }
}
