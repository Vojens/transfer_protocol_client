﻿ 

using System.Reflection;
using System.IO;
using System;
using Support.Common.Exceptions;

namespace Support.Common
{
    public static class AssemblyLoader
    {
        public static object LoadAssembly(string assemblyName, string typeName)
        {
            if (string.IsNullOrEmpty(assemblyName) && string.IsNullOrEmpty(typeName))
            {
                throw new AssemblyLoaderException("Assembly or type name is empty", LibraryReturnCode.LIB_LoadAssemblyFailed);
            }

            typeName = string.Format("{0}.{1}", assemblyName, typeName);

            try
            {
                Assembly asm = Assembly.Load(assemblyName);
                return asm.CreateInstance(typeName);
            }
            catch (FileNotFoundException fnfEx)
            {
                throw new AssemblyLoaderException("Assembly file is not found", LibraryReturnCode.LIB_LoadAssemblyFailed
                    , fnfEx);
            }
            catch (FileLoadException flEx)
            {
                throw new AssemblyLoaderException("Load assembly file is failed", LibraryReturnCode.LIB_LoadAssemblyFailed
                    , flEx);
            }
            catch (BadImageFormatException bifEx)
            {
                throw new AssemblyLoaderException("Assembly is not valid", LibraryReturnCode.LIB_LoadAssemblyFailed, bifEx);
            }
            catch (MissingMethodException mmEx)
            {
                throw new AssemblyLoaderException("No matching constructor was found"
                    , LibraryReturnCode.LIB_LoadAssemblyFailed, mmEx);
            }
            catch (Exception ex)
            {
                throw new AssemblyLoaderException("Load assembly file is failed", LibraryReturnCode.LIB_LoadAssemblyFailed, ex);
            }
        }

        public static object LoadAssembly(string fullTypeName)
        {
            string[] assemblyAndType = fullTypeName.Split(new char[] { ',' });
            return LoadAssembly(assemblyAndType[0].Trim(), assemblyAndType[1].Trim());
        }
    }
}
