﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;

namespace Support.Common.Comparison
{
    public class Int32Comparer : IComparer<Int32>
    {
        private SortOrders _sortOrder = SortOrders.Default;
        private bool _isFrozen = false;

        public SortOrders SortOrder
        {
            get { return _sortOrder; }
            set
            {
                if (!IsFrozen)
                {
                    _sortOrder = value;    
                }
            }
        }

        public int Compare(int a, int b)
        {
            if (SortOrders.Ascending == SortOrder)
            {
                if (a > b)
                    return 1;
                if (a < b)
                    return -1;
                else
                    return 0; // equal    
            }
            else if (SortOrders.Descending == SortOrder)
            {
                if (a > b)
                    return -1; 
                if (a < b)
                    return 1; 
                else
                    return 0; 
            }
            else
            {
                return a.CompareTo(b);
            }
        }

        public bool IsFrozen
        {
            private set { _isFrozen = value; }
            get { return _isFrozen; }
        }

        public void Freeze()
        {
            IsFrozen = true;
        }

        public static Int32Comparer DescendingComparer = null;
        public static Int32Comparer GetDescendingComparer()
        {
            if (DescendingComparer == null)
            {
                var comparerObject = new Int32Comparer() {SortOrder = SortOrders.Descending};
                comparerObject.Freeze();
                DescendingComparer = comparerObject;
            }
            return DescendingComparer;
        }
    }

    public enum SortOrders
    {
        Ascending,Descending, Default
    }
}
