﻿using System;

namespace Support.Common
{
    /// <summary>
    /// ShortGuid
    /// </summary>
    public static class ShortGuid
    {
        /// <summary>
        /// Shortens the specified GUID.
        /// </summary>
        /// <param name="guid">The GUID.</param>
        /// <returns></returns>
        public static string Shorten(this Guid guid)
        {
            string encoded = Convert.ToBase64String(guid.ToByteArray());
            encoded = encoded.Replace("/", "_").Replace("+", "-");
            return encoded.Substring(0, 22);
        }

        /// <summary>
        /// News the short GUID.
        /// </summary>
        /// <returns></returns>
        public static string NewShortGuid()
        {
            return Guid.NewGuid().Shorten();
        }
    }
}
