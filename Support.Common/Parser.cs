﻿ 

using System;
using System.Collections.Generic;
using System.Globalization;
using System.Linq;
using System.Text;
using System.Reflection;
using System.Xml.Serialization;

namespace Support.Common
{
    /// <summary>
    /// General Parser to provide consistent parsing
    /// </summary>
    public static class Parser
    {
        /// <summary>
        /// Return universal datetime from string value
        /// </summary>
        /// <param name="value"></param>
        /// <returns></returns>
        public static DateTime ParseDateTime(string value)
        {
            DateTime result;
            // Need to handle 24:00 format. since DateTime.Parse only support 00:00 to 23:59
            if (value.Contains("T24:"))
            {
                value = value.Replace("T24:", "T00:");
                result = DateTime.Parse(value, enUSCulture).ToUniversalTime();
                result = result.AddDays(1);
            }
            else
            {
                result = DateTime.Parse(value,enUSCulture).ToUniversalTime();
            }

            return result;
        }

        /// <summary>
        /// Return offset based datetime from string value
        /// </summary>
        /// <param name="value"></param>
        /// <param name="offsetedDateTime"></param>
        /// <returns>If offset specified in string or not</returns>
        public static bool ParseDateTimeOffset(string value, out DateTimeOffset offsetedDateTime)
        {
            // Need to handle 24:00 format. since DateTime.Parse only support 00:00 to 23:59
            value = value.Replace("T24:", "T00:");

            // assume UTC only if an offset is not provided
            offsetedDateTime = DateTimeOffset.Parse(value, enUSCulture, DateTimeStyles.AssumeUniversal);

            if (value.Contains("T24:"))
            {
                offsetedDateTime = offsetedDateTime.AddDays(1);
            }

            switch (DateTime.Parse(value, enUSCulture).Kind)
            {
                case DateTimeKind.Unspecified:
                    return false;
                default:
                    if (value.EndsWith("Z"))
                        return false;
                    else
                        return true;
            }
        }

        /// <summary>
        /// Return universal datetime from string valuew
        /// </summary>
        /// <param name="value"></param>
        /// <returns></returns>
        public static DateTime ParseDateTimeToLocalTime(string value)
        {
            return DateTime.Parse(value,enUSCulture).ToUniversalTime().ToLocalTime();
        }

        /// <summary>
        /// Set DateTime Kind from Unspecified to Utc
        /// Used when obtaining DateTime database value
        /// </summary>
        /// <param name="value"></param>
        /// <returns></returns>
        public static DateTime SetKindToUtc(object value)
        {
            return SetKindToUtc((DateTime)value);
        }
        /// <summary>
        /// Set DateTime Kind from Unspecified to Utc
        /// Used when obtaining DateTime database value
        /// </summary>
        /// <param name="value"></param>
        /// <returns></returns>
        public static DateTime SetKindToUtc(DateTime value)
        {
            switch (value.Kind)
            {
                case DateTimeKind.Unspecified:
                    return DateTime.SpecifyKind(value, DateTimeKind.Utc);
                case DateTimeKind.Local:
                default:
                    return value.ToUniversalTime();
            }

        }

        /// <summary>
        /// Return universal datetime from DateTime object
        /// </summary>
        /// <param name="value"></param>
        /// <returns></returns>
        public static DateTime DateTimeToUtc(DateTime value)
        {
            return value.ToUniversalTime();
        }

        /// <summary>
        /// Return integer
        /// </summary>
        /// <param name="value"></param>
        /// <returns></returns>
        public static int ParseInt(string value)
        {
            return Int32.Parse(value);
        }

        /// <summary>
        /// Return short
        /// </summary>
        /// <param name="value"></param>
        /// <returns></returns>
        public static Int16 ParseShort(string value)
        {
            return short.Parse(value);
        }

        private static CultureInfo enUSCulture = new CultureInfo("en-US",false);
        /// <summary>
        /// Return double 
        /// </summary>
        /// <param name="value"></param>
        /// <returns></returns>
        public static double ParseDouble(string value)
        {
            return double.Parse(value, NumberStyles.Any, enUSCulture);
            //return double.Parse(value);
        }

        /// <summary>
        /// Return decimal 
        /// </summary>
        /// <param name="value"></param>
        /// <returns></returns>
        public static decimal ParseDecimal(string value)
        {
            return decimal.Parse(value,NumberStyles.Any,enUSCulture);
            //return decimal.Parse(value);
        }

        /// <summary>
        /// Return boolean from string
        /// </summary>
        /// <param name="value"></param>
        /// <returns></returns>
        public static bool ParseBoolean(string value)
        {
            return Convert.ToBoolean(value);
        }

        /// <summary>
        /// Return boolean from int
        /// </summary>
        /// <param name="value"></param>
        /// <returns></returns>
        public static bool ParseBoolean(int value)
        {
            return Convert.ToBoolean(value);
        }

        /// <summary>
        /// Return guid
        /// </summary>
        /// <param name="value"></param>
        /// <returns></returns>
        public static Guid ParseGuid(string value)
        {
            return new Guid(value);
        }

        public static string ToString<T>(T value) where T : IFormattable
        {            
            return value.ToString(null, enUSCulture);
        }

        /// <summary>
        /// parse string to enum,if enum not exist,return defaultEnum
        /// </summary>
        /// <typeparam name="T">type of enum</typeparam>
        /// <param name="value">string to be parsed to enum</param>
        /// <param name="defaultEnum">default enum,which is to be returned if no matching enum found</param>
        /// <returns>enum</returns>
        public static T ParseEnumFromString<T>(string value, T defaultEnum)
        {
            try
            {
                if (!string.IsNullOrEmpty(value))
                {
                    foreach (FieldInfo fi in typeof(T).GetFields())
                    {
                        object[] attrs = fi.GetCustomAttributes(typeof(XmlEnumAttribute), false);
                        if ((attrs.Length > 0 && ((XmlEnumAttribute)attrs[0]).Name == value) || fi.Name == value)
                        {
                            return (T)Enum.Parse(typeof(T), fi.Name, true);
                        }
                    }
                }

            }
            catch
            {
            }
            //match not found
            return defaultEnum;

        }
    }
}
