﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Windows.Threading;

namespace Support.Common
{
    public class DotNetUtil
    {
        static bool _isInProcessCollectGc = false;
        static readonly object IsInProcessCollectGcSync = new object();

        /// <summary>
        /// Cleaning Garbage Collector without waiting for finalizer
        /// </summary>
        public static void CleanGarbageCollectorWithoutWait()
        {
            lock (IsInProcessCollectGcSync)
            {
                if (_isInProcessCollectGc)
                    return;

                _isInProcessCollectGc = true;
                GC.Collect();
                _isInProcessCollectGc = false;

            }
        }

        /// <summary>
        /// Cleaning Garbage Collector and wait until it finishes
        /// </summary>
        public static void CleanGarbageCollector()
        {
            lock (IsInProcessCollectGcSync)
            {
                if (_isInProcessCollectGc)
                    return;

                _isInProcessCollectGc = true;
                GC.Collect();
                GC.WaitForPendingFinalizers();
                _isInProcessCollectGc = false;
                
            }
        }

        /// <summary>
        /// Delayed Cleaning Garbage Collector (GC) with selected Dispatcher
        /// or if not specified, the current dispatcher. This uses (DispatcherPriority.Background)
        /// as thread priority to ensure that other dispathers queue already done.
        /// 
        /// May ensure inconsistencies if there is other dispatcher using Background priority
        /// </summary>
        /// <param name="dispatcherObject">The Dispatcher object used to clean the GC, 
        /// if null, the current dispatcher will be used.
        /// </param>
        /// <param name="waitForFinalizer">Whether the cleaning process need to wait until finalizer complete</param>
        public static void DelayedCleanGarbageCollector(Dispatcher dispatcherObject,bool waitForFinalizer)
        {
            //Get the dispatcher
            var usedDispatcherObject = dispatcherObject ?? Dispatcher.CurrentDispatcher;

            //Selected Action
            var cleanAction = waitForFinalizer ? new Action(CleanGarbageCollector) : new Action(CleanGarbageCollectorWithoutWait);

            //Garbage Collection
            usedDispatcherObject.BeginInvoke(DispatcherPriority.Background,cleanAction);
        }

    }
}
