 

using System;
using System.Collections.Generic;
using System.Text;

namespace Support.Common
{
    public class StringHelper
    {
        private static int getFirstIndexOfLine(int startFromIndex, byte[] bytesOfStrings)
        {
            int firstIndexOfLine = 0;

            for (int i = startFromIndex; i >= 0; i--)
            {
                if ((char)bytesOfStrings[i] == '\r' || (char)bytesOfStrings[i] == '\n')
                {
                    firstIndexOfLine = i + 1;
                    break;
                }
            }

            return firstIndexOfLine;
        }

        private static int getLastIndexOfLine(int startFromIndex, byte[] bytesOfStrings)
        {
            int lastIndexOfLine = bytesOfStrings.Length;

            for (int i = startFromIndex; i < bytesOfStrings.Length; i++)
            {
                if ((char)bytesOfStrings[i] == '\n' || (char)bytesOfStrings[i] == '\r')
                {
                    lastIndexOfLine = i;
                    break;
                }
            }

            return lastIndexOfLine;
        }

        //  Has passed UnitTest.
        /// <summary>
        /// Gets different from two strings.
        /// </summary>
        /// <param name="s1">1st string.</param>
        /// <param name="s2">2nd string.</param>
        /// <param name="differents">Text which differentiate those two strings.</param>
        /// <param name="lineNumbers">Line number of that different.</param>
        /// <returns>Starting index of the difference.</returns>
        public static long GetDiffIndex(string s1, string s2, out List<string> differents, out List<int> lineNumbers)
        {
            if (string.IsNullOrEmpty(s1) || string.IsNullOrEmpty(s2))
            {
                throw new StringHelperException("One of the string is null or empty string.");
            }
            else
            {
                differents = new List<string>(2);
                lineNumbers = new List<int>(2);

                byte[] s1Bytes = Encoding.ASCII.GetBytes(s1);
                byte[] s2Bytes = Encoding.ASCII.GetBytes(s2);

                //  Set the shortest string as the maximum length to iterate.
                int shortestLength = (s1Bytes.Length < s2Bytes.Length) ? s1Bytes.Length : s2Bytes.Length;
                int differentIndex = -1;
                int lineNumber1 = 0;
                int lineNumber2 = 0;

                for (int i = 0; i < shortestLength; i++)
                {
                    if ((char)s1Bytes[i] == '\n')
                    {
                        lineNumber1++;
                    }

                    if ((char)s2Bytes[i] == '\n')
                    {
                        lineNumber2++;
                    }

                    if (s1Bytes[i] != s2Bytes[i])
                    {
                        differentIndex = i;
                        break;
                    }
                }

                if (differentIndex != -1)
                {
                    //  Populate a line of string from each bytes.
                    int s1StartIndex = getFirstIndexOfLine(differentIndex, s1Bytes);
                    int s1LastIndex = getLastIndexOfLine(differentIndex, s1Bytes);

                    if (s1LastIndex < s1StartIndex)
                    {
                        s1LastIndex = s1StartIndex;
                    }
                    
                    differents.Add(Encoding.ASCII.GetString(s1Bytes, s1StartIndex, s1LastIndex - s1StartIndex));
                    lineNumbers.Add(lineNumber1+1);

                    int s2StartIndex = getFirstIndexOfLine(differentIndex, s2Bytes);
                    int s2LastIndex = getLastIndexOfLine(differentIndex, s2Bytes);

                    if (s2LastIndex < s2StartIndex)
                    {
                        s2LastIndex = s2StartIndex;
                    }

                    differents.Add(Encoding.ASCII.GetString(s2Bytes, s2StartIndex, s2LastIndex - s2StartIndex));
                    lineNumbers.Add(lineNumber2+1);
                }

                return differentIndex;
            }
        }
    }
}
