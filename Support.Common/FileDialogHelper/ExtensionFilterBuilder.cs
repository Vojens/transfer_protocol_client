﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;

namespace Support.Common.FileDialogHelper
{
    public class ExtensionFilterBuilder
    {
        /// <summary>
        /// Obtain File Dialog Filter String for specified FileExtensionInfo
        /// </summary>
        /// <param name="fileextensionList"></param>
        /// <returns></returns>
        public static string GetExtensionFilterString(List<FileExtensionInfo> fileextensionList)
        {
            if (fileextensionList == null) return string.Empty;
            var filterBuilder = new StringBuilder();
            foreach (var fileExtensionInfo in fileextensionList)
            {
                fileExtensionInfo.ToFilterString(filterBuilder);
            }

            return filterBuilder.ToString();
        }
    }

    public class FileExtensionInfo
    {
        private List<string> _extensions = new List<string>();
        public List<string> Extensions
        {
            get { return _extensions; }
            set { _extensions = value; }
        }

        public string DisplayName { get; set; }

        /// <summary>
        /// Create a filter builder
        /// </summary>
        /// <returns></returns>
        public String ToFilterString()
        {
            var filterBuilder = new StringBuilder();
            return ToFilterString(filterBuilder).ToString();
        }

        public StringBuilder ToFilterString(StringBuilder filterBuilder)
        {
            //If string builder is null return
            if (filterBuilder == null) throw new ArgumentNullException("filterBuilder");
            
            //if not empty string add separator
            if(filterBuilder.Length > 0)filterBuilder.Append("|");

            //Add Display
            filterBuilder.Append(DisplayName).Append(" (");
            bool firstElement = true;
            foreach (var extension in Extensions)
            {
                if (!firstElement) filterBuilder.Append(";");
                filterBuilder.Append("*.").Append(extension);
                firstElement = false;
            }
            filterBuilder.Append(")");

            //Add Extension itself
            filterBuilder.Append("|");
            firstElement = true;
            foreach (var extension in Extensions)
            {
                if (!firstElement) filterBuilder.Append(";");
                filterBuilder.Append("*.").Append(extension);
                firstElement = false;
            }


            return filterBuilder;
        }
    }

    public class PdfExtensionInfo : FileExtensionInfo
    {
        public PdfExtensionInfo()
        {
            DisplayName ="PDF Documents";
            Extensions.Add("pdf");
        }
    }

    public class XpsExtensionInfo : FileExtensionInfo
    {
        public XpsExtensionInfo()
        {
            DisplayName = "XPS Documents";
            Extensions.Add("xps");
        }
    }

    public class ImageFilesExtensionInfo : FileExtensionInfo
    {
        public ImageFilesExtensionInfo()
        {
            DisplayName = "Image Files";
            Extensions.Add("BMP");
            Extensions.Add("JPG");
            Extensions.Add("PNG");
        }
    }

    public class CsvExtensionInfo : FileExtensionInfo
    {
        public CsvExtensionInfo()
        {
            DisplayName = "Comma Separated Value Documents";
            Extensions.Add("csv");
        }
    }

    public class XlsExtensionInfo : FileExtensionInfo
    {
        public XlsExtensionInfo()
        {
            DisplayName = "Excel Workbook Binary File Format";
            Extensions.Add("xls");
        }
    }

    public class XlsxExtensionInfo : FileExtensionInfo
    {
        public XlsxExtensionInfo()
        {
            DisplayName = "Excel Workbook Office Open XML File Format";
            Extensions.Add("xlsx");
        }
    }

    public class AllFilesExtensionInfo : FileExtensionInfo
    {
        public AllFilesExtensionInfo()
        {
            DisplayName = "All Files";
            Extensions.Add("*");
        }
    }
}
