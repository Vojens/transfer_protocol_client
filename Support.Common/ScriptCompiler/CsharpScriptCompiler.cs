﻿using System;
using System.IO;
using System.Reflection;
using System.CodeDom.Compiler;
using Microsoft.CSharp;



namespace Support.Common.ScriptCompiler
{
    public class CsharpScriptCompiler
    {

#if(DEBUG)
        private static bool _compileHasBeenRunOnce = true;
#endif

        public static CompilerResults Compile(bool isVerifyMode, string strStatement)
        {
            CodeDomProvider codeProvider = new CSharpCodeProvider();
            // get all the compiler parameters

            // ReSharper disable UseObjectOrCollectionInitializer
            var compilerParams = new CompilerParameters();
            // ReSharper restore UseObjectOrCollectionInitializer

            compilerParams.GenerateExecutable = false;
#if(DEBUG)
            // Allow result assembly debugging
            compilerParams.CompilerOptions = "/target:library";
            compilerParams.IncludeDebugInformation = true;
            compilerParams.GenerateInMemory = false;
            var localApplicationData = Environment.GetFolderPath(Environment.SpecialFolder.LocalApplicationData);
            var tempPath = localApplicationData + @"\Temp";
            if (!isVerifyMode && _compileHasBeenRunOnce)
            {
                _compileHasBeenRunOnce = false;
                // Clenup test folder
                foreach (string strFileDel in Directory.GetFiles(tempPath))
                {
                    try
                    {
                        File.Delete(strFileDel);
                    }
                    catch (IOException) { }
                    catch (UnauthorizedAccessException) { }
                }
            }
            compilerParams.TempFiles = new TempFileCollection(tempPath, true);
#else
      compilerParams.GenerateInMemory = true;
      compilerParams.CompilerOptions = "/target:library /optimize";
      compilerParams.IncludeDebugInformation = false;
#endif
            compilerParams.ReferencedAssemblies.Add("mscorlib.dll");
            compilerParams.ReferencedAssemblies.Add("System.dll");
            if (!isVerifyMode)
            {
                // Append current assembly to referenced assembly list
                // Append calculation common assembly
                compilerParams.ReferencedAssemblies.Add(Assembly.GetExecutingAssembly().Location);
            }
            CompilerResults results = codeProvider.CompileAssemblyFromSource(compilerParams, strStatement);
            return results;
        }
    }
}
