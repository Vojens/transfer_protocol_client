﻿using System;
using System.CodeDom.Compiler;
using System.Collections.Generic;
using System.IO;

namespace Support.Common.ScriptCompiler
{
    public class CsharpScriptModel
    {
        public CsharpScriptModel()
        {
            ReferencedAssemblies = new List<string> {"mscorlib.dll", "System.dll"};
            ScriptMode = ScriptModes.BasedOnBuild;
            SessionGuid = Guid.NewGuid();

            string localApplicationData = Environment.GetFolderPath(Environment.SpecialFolder.LocalApplicationData) ;
            TemporaryFolderPath = localApplicationData + @"\Console\TempScripts\" + SessionGuid;
        }

        public Guid SessionGuid { get; set; }

        public string TemporaryFolderPath { get; set; }

        /// <summary>
        /// Clean Current Temporary Folder
        /// </summary>
        /// <returns>Exception encountered while deleting files</returns>
        public List<Exception> CleanTemporaryFolder()
        {
            // Cleanup test folder
            var listException = new List<Exception>();
            try
            {
                foreach (string strFileDel in Directory.GetFiles(TemporaryFolderPath))
                {
                    try
                    {
                        File.Delete(strFileDel);
                    }
                    catch (Exception e)
                    {
                        listException.Add(e);
                    }
                }

                Directory.Delete(TemporaryFolderPath);
            }
            catch (Exception e)
            {
                listException.Add(e);
            }
            return listException;
        }

        /// <summary>
        /// The script modes
        /// Based Build is decided when the assembly is built
        /// Debug is debug mode
        /// Release is release mode
        /// </summary>
        public ScriptModes ScriptMode { get; set; }

        /// <summary>
        /// List of referenced Assebly. 
        /// By default it will include "mscorlib.dll" and "System.dll"
        /// </summary>
        public List<string> ReferencedAssemblies { get; internal set; }

        public CompilerParameters GetCompileParameters()
        {
            var compilerParams = new CompilerParameters {GenerateExecutable = false};

            bool isDebugMode;
            if (ScriptMode == ScriptModes.Debug)
            {
                isDebugMode = true;
            }
            else if (ScriptMode == ScriptModes.BasedOnBuild)
            {
#if(DEBUG)
                isDebugMode = true;
#else
                isDebugMode = false;
#endif
            }
            else
            {
                isDebugMode = false;
            }
            
            //Append Debug Information as needed
            if (isDebugMode)
            {
                // Allow result assembly debugging
                compilerParams.CompilerOptions = "/target:library";
                compilerParams.IncludeDebugInformation = true;
                compilerParams.GenerateInMemory = false;
                compilerParams.TempFiles = new TempFileCollection(TemporaryFolderPath, true);
            }
            else
            {
                compilerParams.GenerateInMemory = true;
                compilerParams.CompilerOptions = "/target:library /optimize";
                compilerParams.IncludeDebugInformation = false;
            }

            return compilerParams;
        }
    }

    /// <summary>
    /// The script modes
    /// Based Build is decided when the assembly is built
    /// Debug is debug mode
    /// Release is release mode
    /// </summary>
    public enum ScriptModes { BasedOnBuild, Debug, Release }
}
