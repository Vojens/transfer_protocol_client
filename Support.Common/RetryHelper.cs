﻿ 

using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading;
using System.Collections;

namespace Support.Common
{
    public class RetryHelper
    {
        public event EventHandler<RetryEventArgs> WaitingToRetry;
        public event EventHandler<RetryEventArgs> Retrying;

        private int maxRetries;
        private int retryWaitMilis;
        private volatile bool exit = false;
        private bool increasingBackoff = false;
        private AutoResetEvent timeout = new AutoResetEvent(false);

        private Type[] exceptionToRetry = new Type[0];
        private Type[] exceptionToIgnore = new Type[0];
        
        /// <summary>
        /// Constructor
        /// </summary>
        /// <param name="maxRetries">maximum number of retry</param>
        /// <param name="retryWaitMilliSeconds">time to wait before retry - in miliseconds</param>
        public RetryHelper(int maxRetries, int retryWaitMilliSeconds)
        {
            this.maxRetries = maxRetries;
            this.retryWaitMilis = retryWaitMilliSeconds;
        }

        /// <summary>
        /// Constructor
        /// </summary>
        /// <param name="maxRetries">maximum number of retry</param>
        /// <param name="retryWaitMilliSeconds">time to wait before retry - in miliseconds</param>
        /// <param name="increasingBackoff">if set to true the retry wait time will be increasing
        /// retry #1: 1 * retryWaitMiliSeconds
        /// retry #n: n * retryWaitMiliSeconds
        /// </param>
        public RetryHelper(int maxRetries, int retryWaitMilliSeconds
            , bool increasingBackoff)
            : this(maxRetries, retryWaitMilliSeconds)
        {
            this.increasingBackoff = increasingBackoff;
        }

        public event EventHandler<ExtraRetryExceptinionHandledArgs> ExtraExceptionHandled;

        public RetryHelper Any(params Type[] exceptions)
        {
            if (exceptions == null)
            {
                return this;
            } 
            validateParams(exceptions);

            RetryHelper newHelper = new RetryHelper(maxRetries, retryWaitMilis, increasingBackoff);
            newHelper.exceptionToRetry = exceptions;
            return newHelper;
        }

        public RetryHelper Except(params Type[] exceptions)
        {
            if (exceptions == null)
            {
                return this;
            } 
            validateParams(exceptions);

            RetryHelper newHelper = new RetryHelper(maxRetries, retryWaitMilis, increasingBackoff);
            newHelper.exceptionToIgnore = exceptions;
            return newHelper;
        }

        private void validateParams(Type[] exceptions)
        {
            foreach (Type t in exceptions)
            {
                if (!typeof(System.Exception).IsAssignableFrom(t))
                {
                    throw new ArgumentException("Invalid parameter: " + t.ToString() + ". Expecting array of exception type.");
                }
            }
        }

        /// <summary>
        /// Execute action, retry up to maximum retries when System.Exception happen
        /// </summary>
        /// <param name="action">action to be retried</param>
        public void Execute(Action action)
        {
            int tries = 0;
            bool success = false;
            exit = false;
            while (!success)
            {
                if (exit)
                {
                    break;
                }
                try
                {
                    tries++;
                    action.Invoke();
                    success = true;
                }
                catch (System.Exception ex)
                {
                    if (exceptionHandled(tries, ex))
                    {
                        continue;
                    }
                   throw;
                }
            }
        }

        /// <summary>
        /// Execure function func, retry when error occured
        /// </summary>
        /// <typeparam name="TResult"></typeparam>
        /// <param name="func"></param>
        /// <param name="customErrorMessage"></param>
        /// <returns></returns>
        public TResult Execute<TResult>(Func<TResult> func)
        {
            int tries = 0;
            exit = false;
            while (true)
            {
                if (exit)
                {
                    break;
                }
                try
                {
                    tries++;
                    return func();
                }
                catch (System.Exception ex)
                {
                    if (exceptionHandled(tries, ex))
                    {
                        continue;
                    }
                    throw;
                }
            }
            return default(TResult);
        }

        /// <summary>
        /// Cancel auto retry
        /// </summary>
        public void Exit()
        {
            exit = true;
            timeout.Set();
        }
        
        private bool exceptionHandled(int tries, Exception ex)
        {
            if (inIgoreList(ex))
            {
                return false;
            }
            
            //  The method to add exceptions to retry is available but not yet implemented.
            //  Below is the implementation. If the exceptions to retry is specified then do retry only for those exceptions.
            if (exceptionToRetry.Length > 0 && !inRetryList(ex))
            {
                return false;
            }

            //  Other object can do extra checking for exception being retried and decide whether to continue retry the exception or not.
            if (ExtraExceptionHandled != null)
            {
                ExtraRetryExceptinionHandledArgs e = new ExtraRetryExceptinionHandledArgs(ex);
                e.CancelRetry = false;
                ExtraExceptionHandled(this, e);
                if (e.CancelRetry)
                {
                    return false;
                }
            }
            
            int retry = tries - 1;
            if (retry == maxRetries)
            {
                return false;
            }
            if (WaitingToRetry != null)
            {
                RetryEventArgs retryArgs = new RetryEventArgs(ex.Message, ex, tries);
                WaitingToRetry(this, retryArgs);

                if (retryArgs.StopRetry)
                {
                    return false;
                }
            }
            int backOffTime = increasingBackoff ? CalculateLogarithmicBackOffTime(retry, retryWaitMilis) : retryWaitMilis;
            timeout.WaitOne(backOffTime);
            if (Retrying != null)
            {
                Retrying(this, new RetryEventArgs("Retrying... #" + tries, null, tries));
            }
            return true;
        }

        int CalculateLogarithmicBackOffTime(int iteration, int milisecond)
        {
            //-- (60 seconds * (iteration - 1)) + (Seconds * iteration)
            int seconds = 60 * (iteration - 1);
            return TimeSpan.FromSeconds(seconds).Milliseconds + (milisecond * iteration);
        }



        private bool inIgoreList(Exception ex)
        {
            foreach (Type t in exceptionToIgnore)
            {
                if (t == ex.GetType() || 
                    (ex.InnerException != null && t.IsAssignableFrom(ex.InnerException.GetType())) //Test to check inner exception (see defect #239)
                    )
                {
                    return true;
                }
            }
            return false;
        }

        private bool inRetryList(Exception ex)
        {
            foreach (Type t in exceptionToRetry)
            {
                if (t == ex.GetType() ||
                    (ex.InnerException != null && t.IsAssignableFrom(ex.InnerException.GetType())) //Test to check inner exception (see defect #239)
                    )
                {
                    return true;
                }
            }
            return false;
        }
    }

    public class ExtraRetryExceptinionHandledArgs : EventArgs
    {
        public ExtraRetryExceptinionHandledArgs(Exception ex)
        {
            this.Error = ex;
        }
        public bool CancelRetry { get; set; }
        public Exception Error { get; private set; }
    }

    public class RetryEventArgs : EventArgs
    {
        public RetryEventArgs(string message, Exception error, int retryCounter)
        {
            Message = message;
            RetryCounter = retryCounter;
            Error = error;
            StopRetry = false;
        }
        public int RetryCounter { get; private set; }
        public string Message { get; private set; }
        public Exception Error { get; private set; }
        public bool StopRetry { get; set; }
    }
}
