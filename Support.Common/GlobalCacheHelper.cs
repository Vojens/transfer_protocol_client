﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Timers;

namespace Support.Common
{
    public class GlobalCacheHelper
    {
        private class GlobalCacheTimer : Timer
        {
            public string CacheKey { get; set; }
        }

        static Dictionary<string, object> globalCacheHandler = new Dictionary<string, object>();
        static object _objSync = new object();
        public static void AddToCache<T>(string key, T data)
        {
            AddToCache<T>(key, data, 0);
        }
        public static void AddToCache<T>(string key, T data, double expireInterval)
        {
            if (IsExist(key)) return;

            lock (_objSync)
            {
                globalCacheHandler.Add(key, data);
            }
            
            if (expireInterval > 0)
            {
                GlobalCacheTimer t = new GlobalCacheTimer();
                t.CacheKey = key;
                t.Interval = expireInterval;
                t.AutoReset = false;
                t.Elapsed += new ElapsedEventHandler(WhenExpire_Elapsed);
                t.Start();
            }
        }
        static void WhenExpire_Elapsed(object sender, ElapsedEventArgs e)
        {
            GlobalCacheTimer t = sender as GlobalCacheTimer;
            Remove(t.CacheKey);
            t.Dispose();
        }
        
        public static int Size
        {
            get
            {
                lock (_objSync)
                {
                    return globalCacheHandler.Count;
                }
            }
        }
        public static T GetData<T>(string key)
        {
            lock (_objSync)
            {
                return (T)globalCacheHandler[key];
            }
        }
        public static T GetDataOrDefault<T>(string key)
        {
            lock (_objSync)
            {
                if (!IsExist(key)) return default(T);

                return (T)globalCacheHandler[key];
            }
        }
        public static void Reset()
        {
            lock (_objSync)
            {
                globalCacheHandler = null;
                globalCacheHandler = new Dictionary<string, object>();
            }
        }
        public static bool IsExist(string key)
        {
            lock (_objSync)
            {
                return globalCacheHandler.ContainsKey(key);
            }
        }
        public static void Remove(string key)
        {
            lock (_objSync)
            {
                if (globalCacheHandler.ContainsKey(key))
                {
                    globalCacheHandler.Remove(key);
                }                
            }
        }
    }
}
