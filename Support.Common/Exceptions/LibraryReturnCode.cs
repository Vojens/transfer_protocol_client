﻿ 

using System.Collections.Generic;
using Support.Common.Exceptions;

namespace Support.Common.Exceptions
{
    /// <summary>
    /// Return Code using by any PowerStore specific code.
    /// </summary>
    public enum LibraryReturnCode
    {
        //10.300 Group - Custom errors, just to give enough space if the Default ReturnCode is expanded by WITSML Committe. Previously we want to use 1.000 above as Custom
        LIB_LoadAssemblyFailed = -10300,
    }

    public class LibraryReturnCodeMessage : IReturnCodeMessage
    {
        class Nested
        {
            // Explicit static constructor to tell C# compiler
            // not to mark type as beforefieldinit
            static Nested()
            {
            }

            internal static readonly LibraryReturnCodeMessage instance = new LibraryReturnCodeMessage();
        }

        public static IReturnCodeMessage Instance
        {
            get
            {
                return Nested.instance;
            }

        }

        protected static Dictionary<short, string> m_messageList;

        LibraryReturnCodeMessage()
        {
            m_messageList = new Dictionary<short, string>();

            m_messageList.Add(-10300, "Load assembly error: Load assembly failed.");
        }

        public Dictionary<short, string> MessageList
        {
            get { return m_messageList; }
        }
    }
}
