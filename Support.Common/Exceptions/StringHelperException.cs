 

using System;
using System.Collections.Generic;
using System.Text;

namespace Support.Common
{
    public class StringHelperException : ApplicationException
    {
        public StringHelperException(string errorMessage)
            : base(errorMessage)
        {
        }
    }

}
