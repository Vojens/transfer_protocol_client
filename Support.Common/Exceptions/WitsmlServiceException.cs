﻿ 

using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;

namespace Support.Common.Exceptions
{
    public class WitsmlServiceException : WitsmlException
    {
        public WitsmlServiceException(string message, WitsmlReturnCode code)
            : base(message, code)
        {
        }
    }

    public class UnhandledServiceException : System.Exception
    {
        public UnhandledServiceException(string message)
            : base(message)
        {
        }
    }
}
