 

using System;
using System.Collections.Generic;
using System.Text;

namespace Support.Common
{
    public class ThreadHelperException:ApplicationException
    {
        public ThreadHelperException(string message)
            : base(message)
        {
        }

        public ThreadHelperException(string format, params object[] args)
            : base(string.Format(format, args))
        {
        }

        public ThreadHelperException(string methodName, string message)
            : this("{0}: {1}", methodName, message)
        {
        }
    }
}
