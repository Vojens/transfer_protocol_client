 

using System;
using System.Collections.Generic;
using System.Text;

namespace Support.Common
{
    public class FolderWatcherException: ApplicationException
    {
        public FolderWatcherException(string message)
            : base(message)
        {
        }
    }
}
