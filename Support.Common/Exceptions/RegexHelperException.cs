 

using System;
using System.Collections.Generic;
using System.Text;

namespace Support.Common
{
    public class RegexHelperException: ApplicationException
    {
        public RegexHelperException(string message)
            : base(message)
        {
        }
    }
}
