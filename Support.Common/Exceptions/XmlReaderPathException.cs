 

using System;
using System.Collections.Generic;
using System.Text;

namespace Support.Common.Exceptions
{
    public class XmlReaderPathException : ApplicationException
    {
        public XmlReaderPathException(string message) : base(message)
        {            
        }
    }
}
