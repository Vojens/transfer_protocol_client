﻿ 

using System;
using Support.Common.Exceptions;

namespace Support.Common.Exceptions
{
    public class AssemblyLoaderException : WitsmlException
    {
        public AssemblyLoaderException(string message, LibraryReturnCode code)
            : base(message, (short)code, LibraryReturnCodeMessage.Instance)
        {
        }
        public AssemblyLoaderException(string message, LibraryReturnCode code, Exception innerException)
            : base(message, (short)code, innerException, LibraryReturnCodeMessage.Instance)
        {
        }
        public AssemblyLoaderException(string message, LibraryReturnCode code, Exception innerException, bool returnMsg)
            : base(message, (short)code, innerException, LibraryReturnCodeMessage.Instance, returnMsg)
        {
        }

        public AssemblyLoaderException(string message, LibraryReturnCode code, bool returnMsg)
            : base(message, (short)code, LibraryReturnCodeMessage.Instance, returnMsg)
        {
        }
    }
}
