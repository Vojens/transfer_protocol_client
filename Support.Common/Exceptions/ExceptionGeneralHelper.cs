﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading;

namespace Support.Common.Exceptions
{
    public class ExceptionGeneralHelper
    {
        /// <summary>
        /// Check Whether specified exception is critical stop exception which can crash applications
        /// </summary>
        /// <param name="exception">Input exception</param>
        /// <returns>
        /// True, if the exception is either: <br/>
        /// OutOfMemoryException<br/>
        /// AppDomainUnloadedException<br/>
        /// BadImageFormatException<br/>
        /// CannotUnloadAppDomainException<br/>
        /// InvalidProgramException<br/>
        /// ThreadAbortException<br/>
        /// </returns>
        public static bool IsCriticalStopException(Exception exception)
        {
            if (exception is OutOfMemoryException 
                || exception is AppDomainUnloadedException 
                || exception is BadImageFormatException 
                || exception is CannotUnloadAppDomainException 
                || exception is InvalidProgramException
                || exception is ThreadAbortException)
            {
                return true;
            }
            return false;
        }
    }
}
