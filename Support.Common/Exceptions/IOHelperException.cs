 

using System;
using System.Collections.Generic;
using System.Text;

namespace Support.Common
{
    public class IOHelperException : ApplicationException
    {
        public IOHelperException(string errorMessage)
            : base(errorMessage)
        {
        }
    }
}
