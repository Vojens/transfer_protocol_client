 

using System;
using System.Collections.Generic;
using System.Text;

namespace Support.Common
{
    public class XmlHelperException : ApplicationException
    {
        public XmlHelperException(string errorMessage)
            : base(errorMessage)
        {
        }
    }
}
