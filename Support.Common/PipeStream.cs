﻿ 

using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.IO;
using System.Threading;

namespace Support.Common
{
    public class PipeStream : Stream
    {
        private long length = 0;
        protected Queue<byte> backendStorage = new Queue<byte>();
        private AutoResetEvent waitHandle = new AutoResetEvent(false);

        public override bool CanRead
        {
            get { return true; }
        }

        public override bool CanSeek
        {
            get { return false; }
        }

        public override bool CanWrite
        {
            get { return true; }
        }

        public override bool CanTimeout
        {
            get { return true; }
        }

        public override void Flush()
        {
            //do nothing
        }

        public override long Length
        {
            get { return length; }
        }

        public override long Position
        {
            get;
            set;
        }

        public override int Read(byte[] buffer, int offset, int count)
        {
            for (int i = 0; i < count; i++)
            {
                if (backendStorage.Count == 0)
                {
                    waitHandle.WaitOne(ReadTimeout);
                    if (backendStorage.Count == 0)
                    {
                        return i;
                    }
                }
                buffer[offset + i] = backendStorage.Dequeue();
            }
            return count;
        }

        public override long Seek(long offset, SeekOrigin origin)
        {
            throw new InvalidOperationException("Stream does not support seek");
        }

        public override void SetLength(long value)
        {
            length = value;
        }

        public override void Write(byte[] buffer, int offset, int count)
        {
            for (int i = 0; i < count; i++)
            {
                backendStorage.Enqueue(buffer[offset + i]);
                waitHandle.Set();
            }
        }

        public override void Close()
        {
            waitHandle.Set();
            backendStorage.Clear();
        }
    }
}
