﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Data;
using System.IO;

namespace Support.Common
{
    /// <summary>
    /// Class to write data to a csv file
    /// </summary>
    public class CsvWriter : IDisposable
    {

        private readonly CsvFile _csvFile = new CsvFile();

        public CsvFile CsvFile
        {
            get { return _csvFile; }
        }

        string _separatorCharacter = ",";
        /// <summary>
        /// character will be used to separate values in csv. default a comma.
        /// </summary>
        public string SeparatorCharacter
        {
            get { return _separatorCharacter; }
            set { _separatorCharacter = value; }
        }

        string _endRecordCharacter = Environment.NewLine;
        /// <summary>
        /// character will be used to end a record in csv. default a Environment.NewLine. 
        /// Environment.NewLine varies on os, in windows it wil be CR & LF, in mono, might be LF only.
        /// </summary>
        public string EndRecordCharacter
        {
            get { return _endRecordCharacter; }
            set { _endRecordCharacter = value; }
        }

        string _replaceCRLFCharacter = " ";
        /// <summary>
        /// character will be used to replace CR & LF. default a space.
        /// </summary>
        public string ReplaceCRLFCharacter
        {
            get { return _replaceCRLFCharacter; }
            set { _replaceCRLFCharacter = value; }
        }

        bool _replaceCRLF = false;
        /// <summary>
        /// when enabled, replace line feed with character specified in ReplaceCRLFCharacter on the field value of csv
        /// </summary>
        public bool ReplaceCRLF
        {
            get { return _replaceCRLF; }
            set { _replaceCRLF = value; }
        }



        /// <summary>
        /// Writes csv content to a file
        /// </summary>
        /// <param name="csvFile">CsvFile</param>
        /// <param name="filePath">File path</param>
        public void WriteCsv(string filePath)
        {
            WriteCsv(filePath, null);
        }

        /// <summary>
        /// Writes csv content to a file
        /// </summary>
        /// <param name="csvFile">CsvFile</param>
        /// <param name="filePath">File path</param>
        /// <param name="encoding">Encoding</param>
        public void WriteCsv(string filePath, Encoding encoding)
        {
            if (File.Exists(filePath))
                File.Delete(filePath);

            using (StreamWriter writer = new StreamWriter(filePath, false, encoding ?? Encoding.Default))
            {
                WriteToStream(_csvFile, writer);
                writer.Flush();
                writer.Close();
            }
        }

        /// <summary>
        /// Writes csv content to a string
        /// </summary>
        /// <param name="csvFile">CsvFile</param>
        /// <param name="encoding">Encoding</param>
        /// <returns>Csv content in a string</returns>
        public string WriteCsv(Encoding encoding)
        {
            string content = string.Empty;

            using (MemoryStream memoryStream = new MemoryStream())
            {
                using (StreamWriter writer = new StreamWriter(memoryStream, encoding ?? Encoding.Default))
                {
                    WriteToStream(_csvFile, writer);
                    writer.Flush();
                    memoryStream.Position = 0;

                    using (StreamReader reader = new StreamReader(memoryStream, encoding ?? Encoding.Default))
                    {
                        content = reader.ReadToEnd();
                        writer.Close();
                        reader.Close();
                        memoryStream.Close();
                    }
                }
            }

            return content;
        }

        /// <summary>
        /// Writes the Csv File
        /// </summary>
        /// <param name="csvFile">CsvFile</param>
        /// <param name="writer">TextWriter</param>
        private void WriteToStream(CsvFile csvFile, TextWriter writer)
        {
            if (csvFile.HasHeader)
                WriteRecord(csvFile.Headers, writer);

            csvFile.Records.ForEach(record => WriteRecord(record.Fields, writer));
        }

        /// <summary>
        /// Writes the record to the underlying stream
        /// </summary>
        /// <param name="fields">Fields</param>
        /// <param name="writer">TextWriter</param>
        private void WriteRecord(IList<string> fields, TextWriter writer)
        {
            for (int i = 0; i < fields.Count; i++)
            {
                bool quotesRequired = fields[i].Contains(_separatorCharacter);
                bool escapeQuotes = fields[i].Contains("\"");
                string fieldValue = (escapeQuotes ? fields[i].Replace("\"", "\"\"") : fields[i]);

                if (_replaceCRLF && (fieldValue.Contains("\r") || fieldValue.Contains("\n")))
                {
                    quotesRequired = true;
                    fieldValue = fieldValue.Replace("\r\n", _replaceCRLFCharacter);
                    fieldValue = fieldValue.Replace("\r", _replaceCRLFCharacter);
                    fieldValue = fieldValue.Replace("\n", _replaceCRLFCharacter);
                }

                writer.Write(string.Format("{0}{1}{0}{2}",
                    (quotesRequired || escapeQuotes ? "\"" : string.Empty),
                    fieldValue,
                    (i < (fields.Count - 1) ? _separatorCharacter : string.Empty)));
            }

            writer.WriteLine();
        }

        /// <summary>
        /// Disposes of all unmanaged resources
        /// </summary>
        public void Dispose()
        {
            _csvFile.Headers.Clear();
            _csvFile.Records.Clear();
        }

      

    }
}
