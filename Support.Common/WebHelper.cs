﻿ 

using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.ServiceModel;
using System.Collections.Specialized;
using System.Globalization;
using System.Runtime;
using System.Net;
using System.IO;
using System.Configuration;

namespace Support.Common
{
    public static class WebHelper
    {
        public static string ResolveUrl(OperationContext context, string originalUrl)
        {
            Uri launchUrl = OperationContext.Current.IncomingMessageHeaders.To;
            return ResolveUrl(launchUrl, originalUrl);
        }

        public static string ResolveUrl(string url, string siteOfOrigin, string cappName, string pvappName)
        {
            return ResolveUrl(url, siteOfOrigin, cappName, pvappName, string.Empty);
        }

        public static string ResolveUrl(string url, string siteOfOrigin, string cappName, string pvappName, string pvSrvSite)
        {
            url = url.Replace("http://site.origin", siteOfOrigin);
            url = url.Replace("capp.name/", cappName);
            url = url.Replace("pvapp.name/", pvappName);
            if (!string.IsNullOrEmpty(pvSrvSite))
            {
                if (url.Contains("net.tcp"))
                {
                    url = url.Replace("net.tcp://pvsrv.site", pvSrvSite);
                    url = url.Replace("net.tcp://site.origin", pvSrvSite);
                }
                else
                {
                    url = url.Replace("http://pvsrv.site", pvSrvSite);
                }
            }

            return url;
        }

        /// <summary>
        /// Get the resolved Url string from original configuration and currently used address
        /// </summary>
        /// <param name="uri">Actual Uri coming from real HttpContext/OperationContext</param>
        /// <param name="url">url string in configuration</param>
        /// <returns></returns>
        public static string ResolveUrl(Uri uri, string url)
        {
            string siteofOrigin = uri.GetLeftPart(UriPartial.Authority);
            string appName = uri.Segments[1];

            string cappName = string.Empty;
            string pvappName = string.Empty;

            appName = appName.ToLower();
            if (appName.Contains("console"))
            {
                cappName = appName;
                pvappName = appName.Replace("console", "square");
            }
            else if (appName.Contains("square"))
            {
                pvappName = appName;
                cappName = appName.Replace("square", "console");
            }
            //Need to find a better way to resolve instance
            else if (appName.Contains("drtdh"))
            {
                pvappName = appName;
                cappName = appName.Replace("drtdh", "drtdv");
            }
            else if (appName.Contains("drtdv"))
            {
                cappName = appName;
                pvappName = appName.Replace("drtdv", "drtdh");
            }
            else
            {
                cappName = appName;
                pvappName = appName;
            }

            string pvSrvSite = string.Empty;
            try
            {

                pvSrvSite = ConfigurationManager.AppSettings["PvSrvSite"];
                if (pvSrvSite == null) pvSrvSite = "http://site.origin";
            }
            catch (ConfigurationException) { pvSrvSite = "http://site.origin"; }
            if (pvSrvSite.ToLower() == "http://site.origin") pvSrvSite = siteofOrigin;
            string originScheme = uri.Scheme;
            string pvScheme = new Uri(pvSrvSite).Scheme;

            if (url.Contains("net.tcp"))
            {
                pvSrvSite = pvSrvSite.Replace(pvScheme, "net.tcp");          
            }
            else
            {
                pvSrvSite = pvSrvSite.Replace(pvScheme, originScheme);
            }

            return ResolveUrl(url, siteofOrigin, cappName, pvappName, pvSrvSite);
        }

        public static string ConsoleInstanceName(Uri uri)
        {
            string appName = uri.Segments[1];
            string cappName = string.Empty;

            appName = appName.ToLower();            
            if (appName.Contains("console"))
            {
                cappName = appName;
            }
            else if (appName.Contains("square"))
            {
                cappName = appName.Replace("square", "console");
            }
            else if (appName.Contains("drtdv"))
            {
                cappName = appName;
            }
            else if (appName.Contains("drtdh"))
            {
                cappName = appName.Replace("drtdh", "drtdv");
            }
            else
            {
                cappName = appName;
            }            
            return cappName;
        }

        public static NameValueCollection ParseQueryString(string query)
        {
            return ParseQueryString(query, Encoding.UTF8);
        }

        public static NameValueCollection ParseQueryString(string query, Encoding encoding)
        {
            if (query == null)
            {
                throw new ArgumentNullException("query");
            }
            if (encoding == null)
            {
                throw new ArgumentNullException("encoding");
            }
            if ((query.Length > 0) && (query[0] == '?'))
            {
                query = query.Substring(1);
            }
            return new HttpValueCollection(query, false, true, encoding);
        }

        public static string UrlDecode(string str)
        {
            if (str == null)
            {
                return null;
            }
            return UrlDecode(str, Encoding.UTF8);
        }

        public static string UrlDecode(string str, Encoding e)
        {
            return new HttpEncoder().UrlDecode(str, e);
        }

        public static string UrlEncode(string str)
        {
            if (str == null)
            {
                return null;
            }
            return UrlEncode(str, Encoding.UTF8);
        }

        public static string UrlEncode(string str, Encoding e)
        {
            if (str == null)
            {
                return null;
            }
            return Encoding.ASCII.GetString(UrlEncodeToBytes(str, e));
        }

        public static byte[] UrlEncodeToBytes(string str, Encoding e)
        {
            if (str == null)
            {
                return null;
            }
            byte[] bytes = e.GetBytes(str);
            return new HttpEncoder().UrlEncode(bytes, 0, bytes.Length, false);
        }

        public static string HtmlDecode(string s)
        {
            return new HttpEncoder().HtmlDecode(s);
        }

        public static string HtmlEncode(string s)
        {
            return new HttpEncoder().HtmlEncode(s);
        }
    }

    internal class HttpEncoder
    {
        private class UrlDecoder
        {
            // Fields
            private int _bufferSize;
            private byte[] _byteBuffer;
            private char[] _charBuffer;
            private Encoding _encoding;
            private int _numBytes;
            private int _numChars;

            // Methods
            internal UrlDecoder(int bufferSize, Encoding encoding)
            {
                this._bufferSize = bufferSize;
                this._encoding = encoding;
                this._charBuffer = new char[bufferSize];
            }

            internal void AddByte(byte b)
            {
                if (this._byteBuffer == null)
                {
                    this._byteBuffer = new byte[this._bufferSize];
                }
                this._byteBuffer[this._numBytes++] = b;
            }

            internal void AddChar(char ch)
            {
                if (this._numBytes > 0)
                {
                    this.FlushBytes();
                }
                this._charBuffer[this._numChars++] = ch;
            }

            private void FlushBytes()
            {
                if (this._numBytes > 0)
                {
                    this._numChars += this._encoding.GetChars(this._byteBuffer, 0, this._numBytes, this._charBuffer, this._numChars);
                    this._numBytes = 0;
                }
            }

            internal string GetString()
            {
                if (this._numBytes > 0)
                {
                    this.FlushBytes();
                }
                if (this._numChars > 0)
                {
                    return new string(this._charBuffer, 0, this._numChars);
                }
                return string.Empty;
            }
        }

        internal string UrlDecode(string value, Encoding encoding)
        {
            if (value == null)
            {
                return null;
            }
            int length = value.Length;
            UrlDecoder decoder = new UrlDecoder(length, encoding);
            for (int i = 0; i < length; i++)
            {
                char ch = value[i];
                if (ch == '+')
                {
                    ch = ' ';
                }
                else if ((ch == '%') && (i < (length - 2)))
                {
                    if ((value[i + 1] == 'u') && (i < (length - 5)))
                    {
                        int num3 = HttpEncoderUtility.HexToInt(value[i + 2]);
                        int num4 = HttpEncoderUtility.HexToInt(value[i + 3]);
                        int num5 = HttpEncoderUtility.HexToInt(value[i + 4]);
                        int num6 = HttpEncoderUtility.HexToInt(value[i + 5]);
                        if (((num3 < 0) || (num4 < 0)) || ((num5 < 0) || (num6 < 0)))
                        {
                            goto Label_010B;
                        }
                        ch = (char)((((num3 << 12) | (num4 << 8)) | (num5 << 4)) | num6);
                        i += 5;
                        decoder.AddChar(ch);
                        continue;
                    }
                    int num7 = HttpEncoderUtility.HexToInt(value[i + 1]);
                    int num8 = HttpEncoderUtility.HexToInt(value[i + 2]);
                    if ((num7 >= 0) && (num8 >= 0))
                    {
                        byte b = (byte)((num7 << 4) | num8);
                        i += 2;
                        decoder.AddByte(b);
                        continue;
                    }
                }
            Label_010B:
                if ((ch & 0xff80) == 0)
                {
                    decoder.AddByte((byte)ch);
                }
                else
                {
                    decoder.AddChar(ch);
                }
            }
            return decoder.GetString();
        }

        internal byte[] UrlEncode(byte[] bytes, int offset, int count, bool alwaysCreateNewReturnValue)
        {
            byte[] buffer = this.UrlEncode(bytes, offset, count);
            if ((alwaysCreateNewReturnValue && (buffer != null)) && (buffer == bytes))
            {
                return (byte[])buffer.Clone();
            }
            return buffer;
        }

        protected internal virtual byte[] UrlEncode(byte[] bytes, int offset, int count)
        {
            if (!ValidateUrlEncodingParameters(bytes, offset, count))
            {
                return null;
            }
            int num = 0;
            int num2 = 0;
            for (int i = 0; i < count; i++)
            {
                char ch = (char)bytes[offset + i];
                if (ch == ' ')
                {
                    num++;
                }
                else if (!HttpEncoderUtility.IsUrlSafeChar(ch))
                {
                    num2++;
                }
            }
            if ((num == 0) && (num2 == 0))
            {
                return bytes;
            }
            byte[] buffer = new byte[count + (num2 * 2)];
            int num4 = 0;
            for (int j = 0; j < count; j++)
            {
                byte num6 = bytes[offset + j];
                char ch2 = (char)num6;
                if (HttpEncoderUtility.IsUrlSafeChar(ch2))
                {
                    buffer[num4++] = num6;
                }
                else if (ch2 == ' ')
                {
                    buffer[num4++] = 0x2b;
                }
                else
                {
                    buffer[num4++] = 0x25;
                    buffer[num4++] = (byte)HttpEncoderUtility.IntToHex((num6 >> 4) & 15);
                    buffer[num4++] = (byte)HttpEncoderUtility.IntToHex(num6 & 15);
                }
            }
            return buffer;
        }

        private static bool ValidateUrlEncodingParameters(byte[] bytes, int offset, int count)
        {
            if ((bytes == null) && (count == 0))
            {
                return false;
            }
            if (bytes == null)
            {
                throw new ArgumentNullException("bytes");
            }
            if ((offset < 0) || (offset > bytes.Length))
            {
                throw new ArgumentOutOfRangeException("offset");
            }
            if ((count < 0) || ((offset + count) > bytes.Length))
            {
                throw new ArgumentOutOfRangeException("count");
            }
            return true;
        }

        internal string HtmlDecode(string value)
        {
            if (string.IsNullOrEmpty(value))
            {
                return value;
            }
            return WebUtility.HtmlDecode(value);
        }

        internal string HtmlEncode(string value)
        {
            if (string.IsNullOrEmpty(value))
            {
                return value;
            }
            return WebUtility.HtmlEncode(value);
        }
    }

    internal static class HttpEncoderUtility
    {
        // Methods
        public static int HexToInt(char h)
        {
            if ((h >= '0') && (h <= '9'))
            {
                return (h - '0');
            }
            if ((h >= 'a') && (h <= 'f'))
            {
                return ((h - 'a') + 10);
            }
            if ((h >= 'A') && (h <= 'F'))
            {
                return ((h - 'A') + 10);
            }
            return -1;
        }

        public static char IntToHex(int n)
        {
            if (n <= 9)
            {
                return (char)(n + 0x30);
            }
            return (char)((n - 10) + 0x61);
        }

        public static bool IsUrlSafeChar(char ch)
        {
            if ((((ch >= 'a') && (ch <= 'z')) || ((ch >= 'A') && (ch <= 'Z'))) || ((ch >= '0') && (ch <= '9')))
            {
                return true;
            }
            switch (ch)
            {
                case '(':
                case ')':
                case '*':
                case '-':
                case '.':
                case '_':
                case '!':
                    return true;
            }
            return false;
        }

        internal static string UrlEncodeSpaces(string str)
        {
            if ((str != null) && (str.IndexOf(' ') >= 0))
            {
                str = str.Replace(" ", "%20");
            }
            return str;
        }
    }

    [Serializable]
    internal class HttpValueCollection : NameValueCollection
    {
        internal HttpValueCollection(string str, bool readOnly, bool urlencoded, Encoding encoding)
            : base(StringComparer.OrdinalIgnoreCase)
        {
            if (!string.IsNullOrEmpty(str))
            {
                this.FillFromString(str, urlencoded, encoding);
            }
            base.IsReadOnly = readOnly;
        }

        internal void FillFromString(string s, bool urlencoded, Encoding encoding)
        {
            int num = (s != null) ? s.Length : 0;
            for (int i = 0; i < num; i++)
            {
                int startIndex = i;
                int num4 = -1;
                while (i < num)
                {
                    char ch = s[i];
                    if (ch == '=')
                    {
                        if (num4 < 0)
                        {
                            num4 = i;
                        }
                    }
                    else if (ch == '&')
                    {
                        break;
                    }
                    i++;
                }
                string str = null;
                string str2 = null;
                if (num4 >= 0)
                {
                    str = s.Substring(startIndex, num4 - startIndex);
                    str2 = s.Substring(num4 + 1, (i - num4) - 1);
                }
                else
                {
                    str2 = s.Substring(startIndex, i - startIndex);
                }
                if (urlencoded)
                {
                    base.Add(WebHelper.UrlDecode(str, encoding), WebHelper.UrlDecode(str2, encoding));
                }
                else
                {
                    base.Add(str, str2);
                }
                if ((i == (num - 1)) && (s[i] == '&'))
                {
                    base.Add(null, string.Empty);
                }
            }
        }
    }
}
