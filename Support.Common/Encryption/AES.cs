﻿ 

using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Security.Cryptography;
using System.IO;

namespace Support.Common.Encryption
{
    public class AES
    {
        public enum KeySize { Bits128 = 128, Bits192 = 192, Bits256 = 256};

        static string initVector = "P3B2c43def5dL1nk"; // must be 16 bytes
        static string shaltStr = "Sa1tV@lue";

        byte[] initVectorBytes = Encoding.ASCII.GetBytes(initVector);

        public AES()
        {

        }

        public byte[] Encrypt(byte[] data, string passPhrase, AES.KeySize keysize)
        {
            if (string.IsNullOrEmpty(passPhrase))
                passPhrase = "P3tr0L1nk";
            MemoryStream msEncrypt = null;
            CryptoStream csEncrypt = null;

            byte[] encrypted = null;

            PasswordDeriveBytes password = new PasswordDeriveBytes(
                                passPhrase,
                                Encoding.ASCII.GetBytes(shaltStr),
                                "SHA1",
                                2);
            byte[] keyBytes = password.GetBytes((int)keysize / 8);

            AesManaged AesManagedAlg = new AesManaged();
            AesManagedAlg.Key = keyBytes;
            AesManagedAlg.IV = initVectorBytes;

            ICryptoTransform encryptor = AesManagedAlg.CreateEncryptor(AesManagedAlg.Key, AesManagedAlg.IV);

            msEncrypt = new MemoryStream();
            csEncrypt = new CryptoStream(msEncrypt, encryptor, CryptoStreamMode.Write);

            csEncrypt.Write(data, 0, data.Length);
            csEncrypt.FlushFinalBlock();

            encrypted = msEncrypt.ToArray();
            return encrypted;
        }

        public byte[] Dencrypt(byte[] chiperData, string passPhrase, AES.KeySize keysize)
        {
            if (string.IsNullOrEmpty(passPhrase))
                passPhrase = "P3tr0L1nk";
            MemoryStream msEncrypt = null;
            CryptoStream csEncrypt = null;

            byte[] plain = new byte[chiperData.Length];

            PasswordDeriveBytes password = new PasswordDeriveBytes(
                                passPhrase,
                                Encoding.ASCII.GetBytes(shaltStr),
                                "SHA1",
                                2);

            byte[] keyBytes = password.GetBytes((int)keysize / 8);

            AesManaged AesManagedAlg = new AesManaged();
            AesManagedAlg.Key = keyBytes;
            AesManagedAlg.IV = initVectorBytes;

            ICryptoTransform dencryptor = AesManagedAlg.CreateDecryptor(AesManagedAlg.Key, AesManagedAlg.IV);

            msEncrypt = new MemoryStream(chiperData);
            csEncrypt = new CryptoStream(msEncrypt, dencryptor, CryptoStreamMode.Read);

            int byteData = csEncrypt.Read(plain, 0, plain.Length);

            byte[] actual = new byte[byteData];
            Array.Copy(plain, actual, byteData);

            return actual;
        }
    }
}
