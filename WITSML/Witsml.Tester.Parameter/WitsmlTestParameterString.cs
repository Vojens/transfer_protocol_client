using System;
using System.Collections.Generic;
using System.Text;
using Support.Common;
using System.Diagnostics;

namespace Witsml.Tester.Parameter
{
    public class WitsmlTestParameterString: IParameterString
    {
        private string m_suiteFilePath = string.Empty;

        public string  SuiteFilePath
        {
            get { return m_suiteFilePath ; }
            set { m_suiteFilePath = value; }
        }

        private string m_fixtureFilePath = string.Empty;

        /// <summary>
        /// Stores full file path, ie.C:\Folder\FileName.Ext, not only FileName.Ext
        /// </summary>
        public string FixtureFilePath
        {
            get { return m_fixtureFilePath ; }
            set { m_fixtureFilePath = value; }
        }

        private string m_url = string.Empty;

        public string Url
        {
            get { return m_url ; }
            set { m_url = value; }
        }

        private string m_user = string.Empty;

        public string User
        {
            get { return m_user; }
            set { m_user = value; }
        }

        private string m_password = string.Empty;

        public string Password
        {
            get { return m_password; }
            set { m_password = value; }
        }

        private bool m_isIgnoreResponseTime = false;

        public bool IgnoreResponseTime
        {
            get { return m_isIgnoreResponseTime; }
            set { m_isIgnoreResponseTime = value; }
        }

        public override string ToString()
        {
            throw new Exception("The method or operation is not implemented.");
        }

        public void Validate()
        {
            validate(m_suiteFilePath, m_fixtureFilePath, m_url, false);
        }

        private void validate(string suiteFilePath, string fixtureFilePath, string url, bool isThrowWithHelp)
        {
            if (string.IsNullOrEmpty(suiteFilePath) && string.IsNullOrEmpty(fixtureFilePath))
            {
                throwException("Suite or Fixture file path is null.", isThrowWithHelp);
            }
            else
            {
                try
                {
                    if (!string.IsNullOrEmpty(fixtureFilePath))
                    {
                        IOHelper.ValidateFullFilePath(fixtureFilePath);
                    }
                    else
                    {
                        IOHelper.ValidateFullFilePath(suiteFilePath);
                    }
                }
                catch (IOHelperException)
                {
                    throwException("Suite or fixture file path is not valid.", isThrowWithHelp);
                }
            }

            if (string.IsNullOrEmpty(url))
            {
                throwException("Url is null.", isThrowWithHelp);
            }
        }

        //  Has passed UnitTest.
        /// <summary>
        /// Constructs WitsmlTestParameterString, throws error if (suite or fixture) and url are not specified.
        /// </summary>
        /// <param name="suiteFilePath">Path to the suite file.</param>
        /// <param name="fixtureFilePath">Path to the fixture file.</param>
        /// <param name="url">Url target of WITSML Web service will be tested.</param>
        /// <param name="user">User id for the web service.</param>
        /// <param name="password">Password for the web service.</param>
        /// <param name="isIgnoreResponseTime">Test will ignore response time if this variable is set as true
        /// , if test is longer than response time, it won't be failed.</param>
        public WitsmlTestParameterString(string suiteFilePath, string fixtureFilePath, string url, string user, string password
            , bool isIgnoreResponseTime):this(suiteFilePath, fixtureFilePath, url, user, password, isIgnoreResponseTime, false)
        {
        }

        public WitsmlTestParameterString(string suiteFilePath, string fixtureFilePath, string url, string user, string password
            , bool isIgnoreResponseTime, bool isThrowWithException)
        {
            validate(suiteFilePath, fixtureFilePath, url, isThrowWithException);

            m_suiteFilePath = suiteFilePath;
            m_fixtureFilePath = fixtureFilePath;

            m_url = url;
            m_user = user;
            m_password = password;
            m_isIgnoreResponseTime = isIgnoreResponseTime;
        }

        /// <summary>
        /// Throws exception with Help message to client.
        /// </summary>
        /// <param name="message"></param>
        private static void throwException(string message, bool isThrowWithHelp)
        {
            if (isThrowWithHelp)
            {
                StringBuilder sbErr = new StringBuilder(message).Append("\n\n").Append(m_sbHelp);
                throw new WitsmlTestParameterException(sbErr.ToString());
            }
            else
            {
                throw new WitsmlTestParameterException(message);
            }
        }

        //  Has passed UnitTest.
        /// <summary>
        /// Parses argument and construct an instance of this class.
        /// </summary>
        /// <param name="args">Argument conatins parameter to construct this class.</param>
        /// <returns>An instance of this class.</returns>
        public static WitsmlTestParameterString Parse(string[] args)
        {
            if (args == null || args.Length == 0)
            {
                throwException("Invalid arguments", true);
            }

            if (args.Length == 1 && args[0] != "?")
            {
                throwException("Invalid arguments", true);
            }

            string suiteFilePath = string.Empty;
            string fixtureFilePath = string.Empty;
            string url = string.Empty;

            string user = string.Empty;
            string password = string.Empty;
            bool isIgnoreResponseTime = false;

            try
            {

                //  Itearate through all args, and parse it to get value for each option.
                foreach (string arg in args)
                {
                    if (!string.IsNullOrEmpty(arg))
                    {
                        string[] option = arg.Split(new char[] { ':' }, 2);
                        if (option.Length > 1)
                        {
                            switch (option[0])
                            {
                                case "/s":
                                    suiteFilePath = option[1];
                                    break;

                                case "/f":
                                    fixtureFilePath = option[1];
                                    break;

                                case "/url":
                                    url = option[1];
                                    break;

                                case "/user":
                                    user = option[1];
                                    break;

                                case "/pwd":
                                    password = option[1];
                                    break;

                            }
                        }
                        else
                        {
                            switch (option[0])
                            {
                                case "/time":
                                    isIgnoreResponseTime = true;
                                    break;
                            }
                        }
                    }
                }
            }
            catch (Exception ex)
            {
                //  Only called by client uses arguments, i.e: Console app.
                throwException(ex.Message, true);
            }

            return new WitsmlTestParameterString(suiteFilePath, fixtureFilePath, url, user, password, isIgnoreResponseTime
                , true);
        }

        private static StringBuilder m_sbHelp = new StringBuilder("WITSML Test Tool\n\nWITSMLTester.exe -\n")
                .Append("\tUtility to test functionality of a WITSML data store against a set of known good answers.\n\n")
                .Append("WITSMLTester.exe {options}\n\n")
                .Append("\t- OPTIONS -\n\n")
                .Append("/?\n")
                .Append("\tProduce this help.\n\n")
                .Append("/s:<file>\n")
                .Append("\tTestSuite to use.\n\n")
                .Append("/f:<file>\n")
                .Append("\tSpecific Fixture file to run.\n")
                .Append("\tIgnores the fixtures specified in the TestSuite, and only runs this one TestFixture.\n\n")
                .Append("/url:<url>\n")
                .Append("\tURL to test.\n\n")
                .Append("/user:<user>\n")
                .Append("\tUsername to present to the server.\n\n")
                .Append("/pwd:<password>\n")
                .Append("\tPassword to present to the server.\n\n")
                .Append("/time\n")
                .Append("\tIgnore Expected Response times.\n")
                .Append("\tRecord call response times, but don't fail the test if times exceeed the expectedMaxResponseTime.");

        public static string Help
        {
            get{
                return m_sbHelp.ToString();
            }
        }

        public static string HELP_ARG = "/?";
    }
}
