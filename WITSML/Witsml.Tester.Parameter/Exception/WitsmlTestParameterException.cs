using System;
using System.Collections.Generic;
using System.Text;

namespace Witsml.Tester
{
    public class WitsmlTestParameterException:Exception
    {
        public WitsmlTestParameterException(string message)
            : base(message)
        {
        }
    }
}
