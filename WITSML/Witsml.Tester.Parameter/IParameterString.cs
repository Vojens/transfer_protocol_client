using System;
using System.Collections.Generic;
using System.Text;

namespace Witsml.Tester.Parameter
{
    public interface IParameterString
    {
        string ToString();
    }
}
