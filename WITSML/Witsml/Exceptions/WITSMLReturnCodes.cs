﻿ 

using System;
using System.Collections.Generic;
using Support.Common.Exceptions;

namespace Witsml.Common.Exceptions
{
    /// <summary>
    /// Return Code for WITSML Store specific operation.
    /// </summary>
    public enum WITSMLReturnCode
    {
        // Success !
        Success = 1,

        unknown = 0,
        
        //100 Group - Parameter errors
        ParamMissingObjectType = -101,
        ParamMissingXML = -102,
        ParamMissingQuery = -103,
        ParamMissingServerName = -104,
        ParamMissingPublisherName = -105,
        ParamMissingSubscriberName = -106,
        ParamMissingRTDTypeName = -107,
        ParamMissingRealTimeData = -108,
        ParamMissingXmlSchema = -109,
        ParamInvalidOption = -110,
        ParamMissingSublocation = -111,
        ParamMissingSubSecure = -112,
        ParamUnknownCause = -199,

        //200 Group - Persistant Store errors
        StoreDuplicateKey = -201,
        StoreCouldNotParseXML = -202,
        StoreCouldNotMapXML = -203,
        StoreNoMatchingData = -204,
        StoreQueryResultToLarge = -205,
        StoreUnknownStoreError = -299,

        //300 Group - XMLSchema errors
        XSDParseError = -301,
        XSDInvalidContent = -302,
        XSDNoBaseAttribute = -303,
        XSDNestingError = -304,
        XSDUnrecognizedType = -305,
        XSDCircularReference = -306,
        XSDUnknownCause = -399,

        //400 Group - WITSML Library errors
        WITSMLDocumentError = -401,
        WITSMLEmptyDocument = -402,
        WITSMLParseError = -403,
        WITSMLObjectTypeNotSupported = -404,
        WITSMLInvalidParentUID = -405,
        WITSMLMnemonicExists = -406,
        WITSMLMnemonicNotSpecified = -407,
        WITSMLUidContainsSpace = -408,
        WITSMLObjectExistOnXMLFile = -409,
        WITSMLUnkownCause = -499,

        //900 Group - program logic/exception errors
        InternalLoadError = -901,
        InternalObjectNotSupported = -902,
        InternalError = -999

    }

    /// <summary>
    /// TODO. Make it singleton.
    /// </summary>
    public sealed class WITSMLReturnCodeMessage : IReturnCodeMessage
    {
        class Nested
        {
            // Explicit static constructor to tell C# compiler
            // not to mark type as beforefieldinit
            static Nested()
            {
            }

            internal static readonly WITSMLReturnCodeMessage instance = new WITSMLReturnCodeMessage();
        }

        public static IReturnCodeMessage Instance
        {
            get
            {
                return Nested.instance;
            }
        }

        private static Dictionary<short, string> m_messageList;
        
        private WITSMLReturnCodeMessage()
        {
            m_messageList = new Dictionary<short, string>();

            m_messageList.Add(1, "Operation completed Successfully");
            m_messageList.Add(0, "Program Error: unknown cause");

            // -1nn Parameter errors
            m_messageList.Add(-101, "Parameter error: Invalid/missing WITSML object type");
            m_messageList.Add(-102, "Parameter error: Invalid/missing XML");
            m_messageList.Add(-103, "Parameter error: Invalid/missing selection criteria");
            m_messageList.Add(-104, "Parameter error: Invalid/missing server name");
            m_messageList.Add(-105, "Parameter error: Invalid/missing publisher name");
            m_messageList.Add(-106, "Parameter error: Invalid/missing subscriber name");
            m_messageList.Add(-107, "Parameter error: Invalid/missing real-time data type name");
            m_messageList.Add(-108, "Parameter error: Invalid/missing real-time data");
            m_messageList.Add(-109, "Parameter error: Invalid/missing XML Schema (XSD) path/filename");
            m_messageList.Add(-110, "Parameter error: Invalid option");
            m_messageList.Add(-111, "Parameter error: Invalid/missing subscriber process location");
            m_messageList.Add(-112, "Parameter error: Invalid/missing subscriber secure indicator");
            m_messageList.Add(-199, "Parameter error: unknown cause");

            // -2nn Persistent Store errors
            m_messageList.Add(-201, "Persistent Store error: Duplicate key");
            m_messageList.Add(-202, "Persistent Store error: Could not parse XML");
            m_messageList.Add(-203, "Persistent Store error: Could not map XML");
            m_messageList.Add(-204, "Persistent Store error: No data matched selection criteria");
            m_messageList.Add(-205, "Persistent Store error: The query results are too large");
            m_messageList.Add(-299, "Persistent Store error: unknown cause");

            // -3nn XML Schema errors
            m_messageList.Add(-301, "XML Schema error: Error while parsing the specified XML Schema (XSD) file");
            m_messageList.Add(-302, "XML Schema error: specified XML Schema (XSD) has invalid content");
            m_messageList.Add(-303, "XML Schema error: no base attribute present below data type definition element");
            m_messageList.Add(-304, "XML Schema error: nesting of data types exceeds maximum - check for circular reference in definitions");
            m_messageList.Add(-305, "XML Schema error: unrecognized XML Schema data type");
            m_messageList.Add(-306, "XML Schema error: circular reference (loop) in included schemas");
            m_messageList.Add(-399, "XML Schema error: unknown cause");

            // -4nn WITSML Library errors
            m_messageList.Add(-401, "WITSML Decode error: Error while reading xml file");
            m_messageList.Add(-402, "WITSML Decode error: Empty Document");
            m_messageList.Add(-403, "WITSML Decode error: Error while parsing XML value");
            m_messageList.Add(-404, "WITSML Decode error: Object is not supported");
            m_messageList.Add(-405, "WITSML Decode error: Parent Id is invalid");
            m_messageList.Add(-406, "WITSML Decode error: Object with same mnemonic exists");
            m_messageList.Add(-407, "WITSML Decode error: Mnemonic is not specified");
            m_messageList.Add(-408, "WITSML Decode error: Uid contain space");
            m_messageList.Add(-409, "WITSML Decode error: Object with same key already exists");
            m_messageList.Add(-499, "WITSML Decode error: unknown cause");

             // -9nn Program errors
            m_messageList.Add(-901, "Program Error: problem loading internal program or component");
            m_messageList.Add(-902, "Program Error: XML Schema data type is not presently supported by the WITSML API");
            m_messageList.Add(-999, "Program Error: unknown cause");

            // internal PV error 
            m_messageList.Add(-10001, "Access to Store Denied check your Credentials");
            m_messageList.Add(-10002, "Cascade Delete attempted! (All child objects should be deleted before the parent is deleted)");
            m_messageList.Add(-10003, "Store Error: Parent Object Not Found");
            m_messageList.Add(-10004, "Store Error: Missing / Incomplete Subscription");
            m_messageList.Add(-10005, "Store Error: Invalid Identifier");
            m_messageList.Add(-10006, "Store Error: Invalid Query");
            m_messageList.Add(-10007, "Store Error: Parent folder does not exist yet");
            m_messageList.Add(-10008, "Store Error: Both mdTop and mdBottom are required in a GeologyInterval");
            m_messageList.Add(-10009, "Operation Unauthorized");
            m_messageList.Add(-10010, "Store Error: Invalid OptionsIn");

            m_messageList.Add(-10100, "Store Database error");
            m_messageList.Add(-10101, "Store Database error");
            m_messageList.Add(-10102, "Store Database error");
            m_messageList.Add(-10103, "Store Database error");
            m_messageList.Add(-10104, "Store Database error");
            
        }

        public Dictionary<short, string> MessageList
        {
            get { return m_messageList; }
        }
    }
}
