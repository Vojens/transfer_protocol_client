﻿using System;
using Support.Common.Exceptions;

namespace Witsml.Common.Exceptions
{
    public class StoreException : WitsmlException
    {
        public StoreException(string message, WITSMLReturnCode code)
            : base(message, (short)code, WITSMLReturnCodeMessage.Instance)
        {

        }
        public StoreException(string message, WITSMLReturnCode code, bool isReturnMsg)
            : base(message, (short)code, WITSMLReturnCodeMessage.Instance, isReturnMsg)
        {
        }
        public StoreException(string message, WITSMLReturnCode code, Exception innerException)
            : base(message, (short)code, innerException, WITSMLReturnCodeMessage.Instance)
        {
        }
    }
}
