 

using System;
using Support.Common.Exceptions;
using Witsml.Common.Exceptions;

namespace Witsml.Exceptions
{
    public class UnsupportedObjectType : WitsmlException
    {
        public UnsupportedObjectType(string message, WITSMLReturnCode code)
            : base(message, (short)code, WITSMLReturnCodeMessage.Instance)
        {
           
        }
        public UnsupportedObjectType(string message, WITSMLReturnCode code, bool isReturnMsg)
            : base(message, (short)code, WITSMLReturnCodeMessage.Instance, isReturnMsg)
        {
        }
        public UnsupportedObjectType(string message, WITSMLReturnCode code, Exception innerException)
            : base(message, (short)code, innerException, WITSMLReturnCodeMessage.Instance)
        {
        }
    }
}
