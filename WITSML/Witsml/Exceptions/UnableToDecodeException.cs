 

using System;
using Support.Common.Exceptions;
using Witsml.Common.Exceptions;

namespace Witsml.Exceptions
{
    public class UnableToDecodeException : WitsmlException
    {
        public UnableToDecodeException(string message, WITSMLReturnCode code)
            : base(message, (short)code, WITSMLReturnCodeMessage.Instance)
        {
        }
        public UnableToDecodeException(string message, WITSMLReturnCode code, bool isReturnMsg)
            : base(message, (short)code, WITSMLReturnCodeMessage.Instance, isReturnMsg)
        {
        }
        public UnableToDecodeException(string message, WITSMLReturnCode code, Exception innerException)
            : base(message, (short)code, innerException, WITSMLReturnCodeMessage.Instance)
        {
        }
    }
}
