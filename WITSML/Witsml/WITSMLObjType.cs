 

using System;
using System.Collections.Generic;
using System.Text;

namespace Witsml
{
    //TODO:  Document these properties / functions etc
    public class WITSMLObjType
    {
        private string m_objectTypeName;  // Root object name, i.e. 'log'
        private string m_pluralTypeName;  // Root Pluralizer containter object, i.e. 'logs'
        private Guid m_objectTypeID;      // GUID which is internal represenation of this type, never changes.  Allows consistency with database etc
        private Guid m_parentObjectTypeID;      // GUID which is internal represenation of this type, never changes.  Allows consistency with database etc
        private List<string> m_uidOrder;  // List containing the order and names of the UID fields for this object, i.e.  uidWell, uidWellbore
        private List<string> m_nameOrder; // List containing the order and names of the 'name{item}' fields for this object,  i.e.  nameWell, nameWellbore
        private WITSMLType witsmlType;//whether it is witsml object or custom object(folder,document)

        public WITSMLObjType()
        {
            m_uidOrder = new List<string>();
            m_nameOrder = new List<string>();
        }

        public WITSMLObjType(string objectTypeName, string pluralTypeName, Guid objectTypeID, Guid parentObjectTypeID, string[] uidOrder, string[] nameOrder)
            : this()
        {
            this.m_objectTypeName = objectTypeName;
            this.m_pluralTypeName = pluralTypeName;
            this.m_objectTypeID = objectTypeID;
            this.m_parentObjectTypeID = parentObjectTypeID;

            // Add the UID's for object path
            foreach (string uid in uidOrder)
            {
                this.m_uidOrder.Add(uid);
            }

            // Add the Names for Object Path
            foreach (string name in nameOrder)
            {
                this.m_nameOrder.Add(name);
            }


            this.witsmlType = WITSMLType.Witsml;
        }

        public WITSMLObjType(string objectTypeName, string pluralTypeName, Guid objectTypeID, Guid parentObjectTypeID, string[] uidOrder, string[] nameOrder, WITSMLType type)
            : this(objectTypeName, pluralTypeName, objectTypeID, parentObjectTypeID, uidOrder, nameOrder)
        {
            this.witsmlType = type;
        }

        public string ObjectTypeName
        {
            get { return this.m_objectTypeName; }
        }

        public string PluralTypeName
        {
            get { return this.m_pluralTypeName; }
        }

        public Guid ObjectTypeID
        {
            get { return this.m_objectTypeID; }
        }

        public Guid ParentObjectTypeID
        {
            get { return this.m_parentObjectTypeID; }
        }


        public List<string> UidOrder
        {
            get { return this.m_uidOrder; }
        }

        public List<string> NameOrder
        {
            get { return this.m_nameOrder; }
        }

        public WITSMLType WitsmlType
        {
            get { return this.witsmlType; }
        }

        public override string ToString()
        {
            return m_objectTypeName;
        }
    }

    public enum WITSMLType
    {
        Witsml,
    }
}
