 

using System;
using System.Collections.Generic;
using System.IO;
using System.Text;
using System.Xml;
using System.Xml.Serialization;
using System.Linq;
using Witsml;
using Witsml.Common.Exceptions;
using Witsml.Config;
using Witsml.Custom;
using Witsml.Exceptions;
using Witsml.Objects1311;
using Witsml.ObjectStore;
using Witsml.Utility;
using XmlSerializerFactory = System.Xml.Serialization.XmlSerializerFactory;

namespace Witsml
{
    public abstract class WITSMLObject
    {
        [XmlIgnore()]
        public WITSMLVersion version = WITSMLVersion.V131;

        protected WITSMLObject() { }

        // Check if a UID string is valid (i.e. not null or empty, and length <= 64 (max length of a UID string)
        protected bool IsValidUid(string uid)
        {
            bool result = false;
            if (!string.IsNullOrEmpty(uid))
            {
                if (uid.Length <= 64)
                {
                    result = true;
                }
            }

            return result;
        }

        //Decode singular object
        public abstract void Decode(ref string xmlIn);

        /// <summary>
        /// Serialize object into xml string,can be overriden
        /// </summary>
        /// <returns>result of serialization</returns>
        public virtual string Serialize()
        {
            return this.Serialize(null, SerializerType.Null);
        }
        public virtual string SerializeClean()
        {
            return this.SerializeClean(null, SerializerType.Null);
        }
        public virtual string SerializeNoDeclaration()
        {
            return this.SerializeNoDeclaration(null, SerializerType.Null);
        }

        /// <summary>
        /// Serialize object with override
        /// </summary>
        /// <param name="xmlAttributeOverrides"></param>
        /// <returns>result of serialization</returns>
        protected string Serialize(XmlAttributeOverrides xmlAttributeOverrides, SerializerType key)
        {
            XmlSerializer serializer;
            if (xmlAttributeOverrides == null)
            {
                //Generic serialization, memory managed correctly
                serializer = new XmlSerializer(this.GetType());
            }
            else
            {
                //Custom serialization with overrides, Factory used to prevent serializer undisposed
                serializer = XmlSerializerFactory.Create(this.GetType(), xmlAttributeOverrides, key);
            }

            return Serialize(serializer);
        }
        protected string Serialize(XmlSerializer serializer)
        {
            //string returned need to be in utf-8
            XmlWriterSettings settings = new XmlWriterSettings();
            settings.Encoding = System.Text.Encoding.UTF8;
            using (MemoryStream ms = new MemoryStream())
            {
                using (XmlWriter writer = XmlWriter.Create(ms, settings))
                {
                    serializer.Serialize(writer, this);
                }
                ms.Seek(0, System.IO.SeekOrigin.Begin);
                using (StreamReader sr = new StreamReader(ms))
                {
                     return sr.ReadToEnd();
                }
            }
        }

        /// <summary>
        /// Same as Serialize but using XmlTextWriterCleaner(No Declaration and no xmlns)
        /// </summary>
        /// <param name="xmlAttributeOverrides"></param>
        /// <returns></returns>
        protected string SerializeClean(XmlAttributeOverrides xmlAttributeOverrides, SerializerType key)
        {
            XmlSerializer serializer;
            if (xmlAttributeOverrides == null)
                //generic serialization
                serializer = new XmlSerializer(this.GetType());
            else
                //custom serialization with override
                serializer = XmlSerializerFactory.Create(this.GetType(), xmlAttributeOverrides, key);

            return SerializeClean(serializer);
        }
        protected string SerializeClean(XmlSerializer serializer)
        {
            using (MemoryStream ms = new MemoryStream())
            {
                using (XmlWriterCleaner writer = new XmlWriterCleaner(ms))
                {
                    serializer.Serialize(writer, this);
                }
                ms.Seek(0, System.IO.SeekOrigin.Begin);
                using (StreamReader sr = new StreamReader(ms))
                {
                    return sr.ReadToEnd();
                }
            }
        }

        /// <summary>
        /// Serialize with no XML Declaration
        /// </summary>
        /// <param name="xmlAttributeOverrides"></param>
        /// <returns></returns>
        protected string SerializeNoDeclaration(XmlAttributeOverrides xmlAttributeOverrides, SerializerType key)
        {
            XmlSerializer serializer;
            if (xmlAttributeOverrides == null)
                //generic serialization
                serializer = new XmlSerializer(this.GetType());
            else
                //custom serialization with override
                serializer = XmlSerializerFactory.Create(this.GetType(), xmlAttributeOverrides, key);
            //string returned need to be in utf-8
            XmlWriterSettings settings = new XmlWriterSettings();
            settings.OmitXmlDeclaration = true; // Remove the <?xml version="1.0" encoding="utf-8"?>
            settings.Encoding = System.Text.Encoding.UTF8;
            using (MemoryStream ms = new MemoryStream())
            {
                using (XmlWriter writer = XmlWriter.Create(ms, settings))
                {
                    serializer.Serialize(writer, this);
                }
                ms.Seek(0, System.IO.SeekOrigin.Begin);
                using (StreamReader sr = new StreamReader(ms))
                {
                    return sr.ReadToEnd();
                }
            }
        }
        /// <summary>
        /// Return singular Object from xml string using XML deserialization
        /// </summary>
        /// <typeparam name="T">Type of IHandleXML</typeparam>
        /// <param name="xmlIn">xml string</param>
        /// <returns>new witsmlDocument</returns>
        public static T Deserialize<T>(ref string xmlIn) where T : WITSMLObject
        {
            XmlSerializer serializer = new XmlSerializer(typeof(T));
            using (StringReader reader = new StringReader(xmlIn))
            {
                T tObj = (T)serializer.Deserialize(reader);
                return tObj;
            }
        }

        /// <summary>
        /// Check for existence of recurring object
        /// </summary>
        /// <typeparam name="T">recurring object type</typeparam>
        /// <param name="uid">uid of recurring object</param>
        /// <param name="dictionaryToMatch">additional information to match about the recurring object</param>
        /// <returns>object from DB that match</returns>
        protected virtual T CheckForExistence<T>(string uid, Dictionary<string, string> dictionaryToMatch) where T : WITSMLSubElement, new()
        {
            //1.Check Type and Get xmlstring From DB
            //2.Deserialize/Decode using xmlstring
            //3.Return object from step 2
            //Do nothing here as it will be overriden on store

            //Do nothing here as it will be overriden on store
            return default(T);
        }

        /// <summary>
        /// Do Add or Update to Database for recurring object
        /// </summary>
        /// <typeparam name="T">type of object to add/update</typeparam>
        /// <param name="obj">the object to add or update in db</param>
        protected virtual void AddUdpdate<T>(T obj, OperationType operationType) where T : WITSMLSubElement
        {
            //to be overriden
        }

        // Public Properties / accessors for 'generic' fields in every object.

        public abstract string ValidateGetPath(string newUid);   // Throws and exception if full Path is not Valid.  Accepts a default value for the UID field, which it will use if the object dosn't HAVE a uid value.
        public abstract string GetParentPath();                  // Throws an exception if the full path is not valid.
        public abstract string Get_uid();
        public abstract string Get_uidWell();
        public abstract string Get_uidWellbore();
        public abstract string Get_name();
        public abstract string Get_nameWell();
        public abstract string Get_nameWellbore();
        public abstract ICommonData Get_commonData();
        public abstract ICustomData Get_customData();

        [XmlIgnore]
        public virtual cs_square square
        {
            get
            {
                ICustomData customData = this.Get_customData();
                if (customData == null)
                {
                    return null;
                }
                else
                {
                    if (customData.square == null)
                    {
                        customData.square = new cs_square();
                    }
                    return customData.square;
                }
            }
            set
            {
                ICustomData customData = this.Get_customData();
                if (customData != null)
                {
                    customData.square = value;
                }
            }
        }

        public static cs_square Decode_square(ref string xmlIn)
        {
            cs_square square = null;
            using (StringReader stringReader = new StringReader(xmlIn))
            {
                XmlReaderSettings setting = new XmlReaderSettings();
                setting.ConformanceLevel = ConformanceLevel.Document;
                setting.IgnoreComments = true;
                setting.IgnoreWhitespace = true;
                setting.IgnoreProcessingInstructions = true;
                using (XmlReader xmlReader = XmlReader.Create(stringReader, setting))
                {
                    xmlReader.MoveToContent();
                    while (!xmlReader.EOF)
                    {
                        if (xmlReader.Name == "customData" && xmlReader.NodeType == XmlNodeType.Element)
                        {
                            using (XmlReader reader = xmlReader.ReadSubtree())
                            {
                                reader.MoveToContent();
                                while (!reader.EOF)
                                {
                                    if (reader.Name == "square")
                                    {
                                        square = new cs_square();
                                        square.HandleXML(reader, true);

                                    }
                                    reader.Read();
                                }
                            }

                        }
                        xmlReader.Read();
                    }
                }
            }
            return square;
        }

        public static WITSMLObject GetObjectByType(WITSMLObjType type, string version)
        {
            try
            {
                switch (version)
                {
                    case "1.3.1.1":
                        return GetObject1311ByType(type);
                    case "1.4.0.0":
                        return GetObject1400ByType(type);
                    default:
                        return GetObject1311ByType(type);
                }
            }
            catch (UnsupportedObjectType ex)
            {
                throw (ex);
            }

            catch (Exception ex)
            {
                //TODO: log the error
                throw new UnableToDecodeException("Unable to create WITSML Object from objectType, EX:" + ex.Message, WITSMLReturnCode.WITSMLUnkownCause);
            }
        }

        public static WITSMLObject GetObject1311ByType(WITSMLObjType type)
        {
            try
            {
                //Generic object checking
                if (GenericObjectConfig.Instance != null)
                {
                    if (GenericObjectConfig.Instance.IsObjectTypeRegistered(type.ObjectTypeName))
                    {
                        return new obj_generic(type.ObjectTypeName);
                    }
                }

                switch (type.ObjectTypeName)
                {
                    //generic case
                    case "attachment":
                        return new obj_attachment();
                    case "well":
                        return new obj_well();
                    case "wellbore":
                        return new obj_wellbore();
                    case "rig":
                        return new obj_rig();
                    case "opsReport":
                        return new obj_opsReport();
                    case "formationMarker":
                        return new obj_formationMarker();
                    case "bhaRun":
                        return new obj_bhaRun();
                    case "tubular":
                        return new obj_tubular();
                    case "wbGeometry":
                        return new obj_wbGeometry();
                    case "target":
                        return new obj_target();
                    case "surveyProgram":
                        return new obj_surveyProgram();
                    case "sidewallCore":
                        return new obj_sidewallCore();
                    case "risk":
                        return new obj_risk();
                    case "message":
                        return new obj_message();
                    case "fluidsReport":
                        return new obj_fluidsReport();
                    case "convCore":
                        return new obj_convCore();
                    case "cementJob":
                        return new obj_cementJob();
                    case "dtsInstalledSystem":
                        return new obj_dtsInstalledSystem();
                    case "dtsMeasurement":
                        return new obj_dtsMeasurement();
                    //special
                    case "trajectory":
                        return new obj_trajectory();
                    case "log":
                        return new obj_log();
                    case "mudLog":
                        return new obj_mudLog();
                    case "wellLog":
                        return new obj_wellLog();
                    case "realtime":
                        return new obj_realtime();
                    case "changeLog":
                        return new obj_changeLog();                        
                    default:
                        throw new UnsupportedObjectType("Type Unknown:" + type.ObjectTypeName, WITSMLReturnCode.WITSMLObjectTypeNotSupported);
                }
            }
            catch (UnsupportedObjectType ex)
            {
                throw (ex);
            }

            catch (Exception ex)
            {
                //TODO: log the error
                throw new UnableToDecodeException("Unable to create WITSML Object from objectType, EX:" + ex.Message, WITSMLReturnCode.WITSMLUnkownCause);
            }
        }

        public static WITSMLObject GetObject1400ByType(WITSMLObjType type)
        {
            try
            {
                switch (type.ObjectTypeName)
                {
                    default:
                        throw new UnsupportedObjectType("Type Unknown:" + type.ObjectTypeName, WITSMLReturnCode.WITSMLObjectTypeNotSupported);
                }
            }
            catch (UnsupportedObjectType ex)
            {
                throw (ex);
            }

            catch (Exception ex)
            {
                //TODO: log the error
                throw new UnableToDecodeException("Unable to create WITSML Object from objectType, EX:" + ex.Message, WITSMLReturnCode.WITSMLUnkownCause);
            }
        }
    }
}
