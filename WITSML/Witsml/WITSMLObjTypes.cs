 

using System;
using System.Collections.Generic;
using System.Text;
using System.Xml;
using System.IO;
using Witsml.Config;

namespace Witsml
{
    /// <summary>
    /// A static dictionary of all the WITSML Object Types, and information for each one
    /// </summary>
    public static class WITSMLObjTypes
    {
        private static Dictionary<string, WITSMLObjType> objectTypeList;
        private static Dictionary<string, WITSMLObjType> objectPluralList;

        static WITSMLObjTypes()
        {
            objectTypeList = new Dictionary<string, WITSMLObjType>();
            objectPluralList = new Dictionary<string, WITSMLObjType>();

            objectTypeList.Add("attachment", new WITSMLObjType(
            "attachment",
            "attachments",
            new Guid("{542e231f-b9a4-43e7-9137-c10501535fba}"),
            new Guid("{ceeec8f0-f111-4477-8229-009f34f7142f}"), // Parent ID, which is ID of a WellBore
            new string[] { "uidWell", "uidWellbore", "uid" },
            new string[] { "nameWell", "nameWellbore", "name" }
            ));

            objectTypeList.Add("bhaRun", new WITSMLObjType(
                "bhaRun",
                "bhaRuns",
                new Guid("{d9b25b62-ce0c-46fc-b709-603dd97ff5ca}"),
                new Guid("{ceeec8f0-f111-4477-8229-009f34f7142f}"),    // Parent ID, which is ID of a WellBore
                new string[] { "uidWell", "uidWellbore", "uid" },
                new string[] { "nameWell", "nameWellbore", "name" }
                ));

            objectTypeList.Add("cementJob", new WITSMLObjType(
                "cementJob",
                "cementJobs",
                new Guid("{e138e0b4-5a54-476f-8cf3-70bdae26bfaf}"),
                new Guid("{ceeec8f0-f111-4477-8229-009f34f7142f}"),    // Parent ID, which is ID of a WellBore
                new string[] { "uidWell", "uidWellbore", "uid" },
                new string[] { "nameWell", "nameWellbore", "name" }
                ));

            objectTypeList.Add("convCore", new WITSMLObjType(
                "convCore",
                "convCores",
                new Guid("{701b6471-581e-4146-9aec-e8da5c136c80}"),
                new Guid("{ceeec8f0-f111-4477-8229-009f34f7142f}"),    // Parent ID, which is ID of a WellBore
                new string[] { "uidWell", "uidWellbore", "uid" },
                new string[] { "nameWell", "nameWellbore", "name" }
                ));

            objectTypeList.Add("fluidsReport", new WITSMLObjType(
                "fluidsReport",
                "fluidsReports",
                new Guid("{86d8e28d-a68b-4a75-add4-a01bdda14bc2}"),
                new Guid("{ceeec8f0-f111-4477-8229-009f34f7142f}"),    // Parent ID, which is ID of a WellBore
                new string[] { "uidWell", "uidWellbore", "uid" },
                new string[] { "nameWell", "nameWellbore", "name" }
                ));

            objectTypeList.Add("formationMarker", new WITSMLObjType(
                "formationMarker",
                "formationMarkers",
                new Guid("{2c52740c-4fa7-4a89-acea-36a28d987e71}"),
                new Guid("{ceeec8f0-f111-4477-8229-009f34f7142f}"),    // Parent ID, which is ID of a WellBore
                new string[] { "uidWell", "uidWellbore", "uid" },
                new string[] { "nameWell", "nameWellbore", "name" }
                ));

            objectTypeList.Add("log", new WITSMLObjType(
                "log",
                "logs",
                new Guid("{f06a0473-90cb-4341-b58c-7814e9713720}"),
                new Guid("{ceeec8f0-f111-4477-8229-009f34f7142f}"),    // Parent ID, which is ID of a WellBore
                new string[] { "uidWell", "uidWellbore", "uid" },
                new string[] { "nameWell", "nameWellbore", "name" }
                ));

            objectTypeList.Add("message", new WITSMLObjType(
                "message",
                "messages",
                new Guid("{4f6aef74-3bda-4530-af81-8af34179d3b7}"),
                new Guid("{ceeec8f0-f111-4477-8229-009f34f7142f}"),    // Parent ID, which is ID of a WellBore
                new string[] { "uidWell", "uidWellbore", "uid" },
                new string[] { "nameWell", "nameWellbore", "name" }
                ));

            objectTypeList.Add("mudLog", new WITSMLObjType(
                "mudLog",
                "mudLogs",
                new Guid("{aa9150e8-a7d1-469e-bb61-d3b4dcbe8bc5}"),
                new Guid("{ceeec8f0-f111-4477-8229-009f34f7142f}"),    // Parent ID, which is ID of a WellBore
                new string[] { "uidWell", "uidWellbore", "uid" },
                new string[] { "nameWell", "nameWellbore", "name" }
                ));

            objectTypeList.Add("opsReport", new WITSMLObjType(
                "opsReport",
                "opsReports",
                new Guid("{23877330-d044-4ee3-bc63-81829b04d81e}"),
                new Guid("{ceeec8f0-f111-4477-8229-009f34f7142f}"),    // Parent ID, which is ID of a WellBore
                new string[] { "uidWell", "uidWellbore", "uid" },
                new string[] { "nameWell", "nameWellbore", "name" }
                ));

            objectTypeList.Add("realtime", new WITSMLObjType(
                "realtime",
                "realtimes",
                new Guid("{1cfc15d6-4a44-4869-bedc-3d4ad6f12930}"),
                new Guid("{ceeec8f0-f111-4477-8229-009f34f7142f}"),    // Parent ID, which is ID of a WellBore
                new string[] { "uidWell", "uidWellbore", "idSub" },
                new string[] { "nameWell", "nameWellbore", "idSub" }
                ));

            objectTypeList.Add("rig", new WITSMLObjType(
                "rig",
                "rigs",
                new Guid("{3c8034d6-9aff-4234-84b9-d119cb0b911c}"),
                new Guid("{ceeec8f0-f111-4477-8229-009f34f7142f}"),    // Parent ID, which is ID of a WellBore
                new string[] { "uidWell", "uidWellbore", "uid" },
                new string[] { "nameWell", "nameWellbore", "name" }
                ));

            objectTypeList.Add("risk", new WITSMLObjType(
                "risk",
                "risks",
                new Guid("{0eee0e90-44b9-4105-be4e-cf2677b8120d}"),
                new Guid("{ceeec8f0-f111-4477-8229-009f34f7142f}"),    // Parent ID, which is ID of a WellBore
                new string[] { "uidWell", "uidWellbore", "uid" },
                new string[] { "nameWell", "nameWellbore", "name" }
                ));

            objectTypeList.Add("sidewallCore", new WITSMLObjType(
                "sidewallCore",
                "sidewallCores",
                new Guid("{9f9f90f7-4db2-4579-abe1-41049c538fc3}"),
                new Guid("{ceeec8f0-f111-4477-8229-009f34f7142f}"),    // Parent ID, which is ID of a WellBore
                new string[] { "uidWell", "uidWellbore", "uid" },
                new string[] { "nameWell", "nameWellbore", "name" }
                ));

            objectTypeList.Add("surveyProgram", new WITSMLObjType(
                "surveyProgram",
                "surveyPrograms",
                new Guid("{1aa92299-f824-4c57-8bd0-dcf9da11388a}"),
                new Guid("{ceeec8f0-f111-4477-8229-009f34f7142f}"),    // Parent ID, which is ID of a WellBore
                new string[] { "uidWell", "uidWellbore", "uid" },
                new string[] { "nameWell", "nameWellbore", "name" }
                ));

            objectTypeList.Add("target", new WITSMLObjType(
                "target",
                "targets",
                new Guid("{376e5c1b-4ce9-4469-b2f0-18bd60bcc18d}"),
                new Guid("{ceeec8f0-f111-4477-8229-009f34f7142f}"),    // Parent ID, which is ID of a WellBore
                new string[] { "uidWell", "uidWellbore", "uid" },
                new string[] { "nameWell", "nameWellbore", "name" }
                ));

            objectTypeList.Add("trajectory", new WITSMLObjType(
                "trajectory",
                "trajectorys",
                new Guid("{870156fa-eddb-4701-8eef-79f2b7b576e6}"),
                new Guid("{ceeec8f0-f111-4477-8229-009f34f7142f}"),    // Parent ID, which is ID of a WellBore
                new string[] { "uidWell", "uidWellbore", "uid" },
                new string[] { "nameWell", "nameWellbore", "name" }
                ));

            objectTypeList.Add("trajectoryStation", new WITSMLObjType(
                "trajectoryStation",
                "trajectoryStations",
                new Guid("{6af5434f-82c0-49f5-83e1-5053a27fc966}"),
                new Guid("{ceeec8f0-f111-4477-8229-009f34f7142f}"),    // Parent ID, which is ID of a WellBore
                new string[] { "uidWell", "uidWellbore", "uidTrajectory", "uid" },
                new string[] { "nameWell", "nameWellbore", "nameTrajectory", "name" }));

            objectTypeList.Add("tubular", new WITSMLObjType(
                "tubular",
                "tubulars",
                new Guid("{753f9234-45aa-428d-9426-0d319db04402}"),
                new Guid("{ceeec8f0-f111-4477-8229-009f34f7142f}"),    // Parent ID, which is ID of a WellBore
                new string[] { "uidWell", "uidWellbore", "uid" },
                new string[] { "nameWell", "nameWellbore", "name" }
                ));

            objectTypeList.Add("wbGeometry", new WITSMLObjType(
                "wbGeometry",
                "wbGeometrys",
                new Guid("{4329663f-d583-48c9-bc1c-fcf539d4281c}"),
                new Guid("{ceeec8f0-f111-4477-8229-009f34f7142f}"),    // Parent ID, which is ID of a WellBore
                new string[] { "uidWell", "uidWellbore", "uid" },
                new string[] { "nameWell", "nameWellbore", "name" }
                ));

            objectTypeList.Add("well", new WITSMLObjType(
                "well",
                "wells",
                new Guid("{fa01c8f5-5b96-4749-8cd9-e9ecbd83b8bc}"),
                System.Guid.Empty,                                  // Parent ID, which for a well is Empty (there is NO parent).
                new string[] { "uid" },
                new string[] { "name" }));

            objectTypeList.Add("wellbore", new WITSMLObjType(
                "wellbore",
                "wellbores",
                new Guid("{ceeec8f0-f111-4477-8229-009f34f7142f}"),
                new Guid("{fa01c8f5-5b96-4749-8cd9-e9ecbd83b8bc}"),    // Parent ID, which is ID of a Well
                new string[] { "uidWell", "uid" },
                new string[] { "nameWell", "name" }));

            objectTypeList.Add("wellLog", new WITSMLObjType(
                "wellLog",
                "wellLogs",
                new Guid("{4a596f07-fbea-440c-8ce0-7e51be3e592c}"),
                new Guid("{ceeec8f0-f111-4477-8229-009f34f7142f}"),    // Parent ID, which is ID of a WellBore
                new string[] { "uidWell", "uidWellbore", "uid" },
                new string[] { "nameWell", "nameWellbore", "name" }
                ));

            objectTypeList.Add("dtsMeasurement", new WITSMLObjType(
                "dtsMeasurement",
                "dtsMeasurements",
                new Guid("{7c6bb914-6324-4293-bd42-39eddd8321f4}"),
                new Guid("{ceeec8f0-f111-4477-8229-009f34f7142f}"),    // Parent ID, which is ID of a WellBore
                new string[] { "uidWell", "uidWellbore", "uid" },
                new string[] { "nameWell", "nameWellbore", "name" }
                ));

            objectTypeList.Add("dtsInstalledSystem", new WITSMLObjType(
                "dtsInstalledSystem",
                "dtsInstalledSystems",
                new Guid("{a672f305-37f4-46f2-a79f-2a26292efac1}"),
                new Guid("{ceeec8f0-f111-4477-8229-009f34f7142f}"),    // Parent ID, which is ID of a WellBore
                new string[] { "uidWell", "uidWellbore", "uid" },
                new string[] { "nameWell", "nameWellbore", "name" }
                ));

            objectTypeList.Add("changeLog", new WITSMLObjType(
                "changeLog",
                "changeLogs",
                new Guid("{D76E1EAF-7713-4415-B5B4-D90C511E6DFF}"),
                Guid.Empty,    // Parent ID, which is ID of a WellBore
                new string[] { "uidWell", "uidWellbore", "uidObject" },
                new string[] { "nameWell", "nameWellbore", "nameObject" }
                ));

            objectTypeList.Add("WITSMLComposite", new WITSMLObjType(
                "WITSMLComposite",
                "WITSMLComposite",
                new Guid("{72D4C7F8-D4A8-405f-98EE-956BA2FDE33F}"),
                Guid.Empty, //Top level object since information is passed from top to bottom
                new string[] { "uidWell", "uidWellbore", "uidObject" },
                new string[] { "nameWell", "nameWellbore", "nameObject" }
                ));

            if (GenericObjectConfig.Instance != null)
            {
                foreach (GenericObjectInfo objInfo in GenericObjectConfig.Instance.Object)
                {
                    objectTypeList.Add(objInfo.objectTypeName, new WITSMLObjType(
                     objInfo.objectTypeName,
                     objInfo.pluralTypeName,
                     objInfo.objectTypeID,
                     new Guid("{ceeec8f0-f111-4477-8229-009f34f7142f}"),    // Parent ID, which is ID of a WellBore
                     new string[] { "uidWell", "uidWellbore", "uid" },
                     new string[] { "nameWell", "nameWellbore", "name" }
                     ));
                }
            }

            // Create The Plural to Singular Look up table, from the objectTypeList.
            foreach (KeyValuePair<string, WITSMLObjType> kvp in objectTypeList)
            {
                objectPluralList.Add(kvp.Value.PluralTypeName, kvp.Value);
            }
        }

        //Enumerator to access collection
        public static Dictionary<string, WITSMLObjType>.Enumerator TypeEnumerators()
        {
            return objectTypeList.GetEnumerator();
        }



        public static WITSMLObjType GetTypeByName(string objectName)
        {
            return objectTypeList[objectName];
        }

        // Parses an XML Fragment and attempts to derive the object type from it..
        // Works for both plural and Singluar, and finds the first one, which theoretically should be the first row of the object (but not always)
        // Only scans as far as it needs to to get the first type, using a forward only reader
        public static WITSMLObjType ParseObjectType(ref string xmlFragment)
        {
            WITSMLObjType objType = null;

            try
            {
                if (!string.IsNullOrEmpty(xmlFragment))
                {
                    StringReader sr = new StringReader(xmlFragment);
                    XmlReader xr = XmlReader.Create(sr);

                    while (xr.Read())
                    {
                        // Plurals
                        if ((xr.NodeType == XmlNodeType.Element) && (objectPluralList.ContainsKey(xr.Name)))
                        {
                            objType = objectPluralList[xr.Name];
                            break;
                        }

                        // Singulars
                        if ((xr.NodeType == XmlNodeType.Element) && (objectTypeList.ContainsKey(xr.Name)))
                        {
                            objType = objectTypeList[xr.Name];
                            break;
                        }
                    }
                }
                return objType;
            }
            catch (Exception)
            {
                //TODO: Add logging here of the exception
                return null;
            }
        }
    }
}


