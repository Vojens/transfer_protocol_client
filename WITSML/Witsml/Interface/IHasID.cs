 

using System;
using System.Collections.Generic;
using System.Text;

namespace Witsml
{
    //Summary: Interface to provide contract with generic function
    //See StaticHelper
    public interface IHasID
    {
        //Summary:
        //  return uid of the object
        string UID { get;}
    }
}
