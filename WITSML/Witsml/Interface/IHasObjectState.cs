﻿ 

using System;
using System.Collections.Generic;
using System.Text;

namespace Witsml
{
    //Summary: Interface to provide contract with generic function
    //See StaticHelper
    public interface IHasObjectState
    {
        //Summary:
        //  return object state of the object
        ObjectState State { get; set; }
    }
}
