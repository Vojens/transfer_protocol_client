 

using System.Xml;

namespace Witsml
{
    //Summary:
    //          Define a method to Handle XML reader for Witsml Object
    public interface IHandleXML
    {
        //Summary:
        //      Handle XML string provided by XMLReader
        void HandleXML(XmlReader xmlReader,bool IsNew);
    }
}
