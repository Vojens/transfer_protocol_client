﻿ 

using System;
using System.Collections.Generic;
using System.Text;

namespace Witsml
{
    //Summary: Interface to provide contract with generic function
    //See StaticHelper
    public interface IHasGUID
    {
        //Summary:
        //  return uid of the object
        Guid ID { get; }
    }
}
