﻿ 

using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;

namespace Witsml
{
    public interface ICommonData
    {
        DateTime DTimCreation { set; }
        bool DTimCreationSpecified { set; }
        DateTime DTimLastChange { set; }
        bool DTimLastChangeSpecified { set; }
    }
}
