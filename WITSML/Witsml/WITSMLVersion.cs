﻿ 

using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Xml.Serialization;
namespace Witsml
{
    public enum WITSMLVersion
    {
        [XmlEnum("")]
        Unknown = 0,
        [XmlEnum("1.2.0.0")]
        V120 = 1,
        [XmlEnum("1.3.1.1")]
        V131 = 2,
        [XmlEnum("1.4.0.0")]
        V140 = 3,
        [XmlEnum("1.2.0.0(PRODML)")]
        V120P = 4
    }

    public class WITSMLVersionParser
    { 
        
    }
}
