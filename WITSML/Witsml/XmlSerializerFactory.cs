﻿ 

using System;
using System.Collections;
using System.Collections.Generic;
using System.Xml.Serialization;

namespace Witsml
{
    static class XmlSerializerFactory
    {
        static Dictionary<SerializerType, XmlSerializer> dictSerializer = new Dictionary<SerializerType, XmlSerializer>();
        static Dictionary<string, XmlSerializer> dictGSerializer = new Dictionary<string, XmlSerializer>();

        public static XmlSerializer Create(Type type, XmlAttributeOverrides attr, SerializerType key)
        {
            //  Suspected Race Conditions. Should use lock.
            lock (((ICollection)dictSerializer).SyncRoot)
            {
                if (dictSerializer.ContainsKey(key))
                {
                    return dictSerializer[key];
                }
                else
                {
                    XmlSerializer serializer = new XmlSerializer(type, attr);
                    dictSerializer.Add(key, serializer);
                    return serializer;
                }
            }
        }

        public static XmlSerializer Create(Type type, XmlAttributeOverrides attr, string objectTypeName)
        {
            //  Suspected Race Conditions. Should use lock.
            lock (((ICollection)dictSerializer).SyncRoot)
            {
                if (dictGSerializer.ContainsKey(objectTypeName))
                {
                    return dictGSerializer[objectTypeName];
                }
                else
                {
                    XmlSerializer serializer = new XmlSerializer(type, attr);
                    dictGSerializer.Add(objectTypeName, serializer);
                    return serializer;
                }
            }
        }
    }

    public enum SerializerType
    {
        Null,
        LogWithoutData,
        MudlogWithGeology,
        MudlogWithoutGeology,
        TrajectoryWithStation,
        TrajectoryWithoutStation,
        WelllogWithoutData,
        MudlogStoreWithoutGeology,
        MudlogStoreWithGeology,
        TrajectoryStoreWithoutStation,
        TrajectoryStoreWithStation,
        ChangeLogWithDummy,
        Generic
    }
}
