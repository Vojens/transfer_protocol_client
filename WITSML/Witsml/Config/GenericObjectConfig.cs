﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Xml.Serialization;
using System.IO;
using System.Configuration;
using Witsml.Utility;

namespace Witsml.Config
{
    [Serializable]
    [XmlRoot("types")]
    public class GenericObjectConfig
    {
        [XmlElement("object")]
        public GenericObjectInfo[] Object;

        public static string cloudPath = string.Empty;

        private static volatile GenericObjectConfig m_genericObjectConfig;
        private static object syncRoot = new Object();

        public static GenericObjectConfig Instance
        {
            get
            {
                if (m_genericObjectConfig == null)
                {
                    lock (syncRoot)
                    {
                        if (m_genericObjectConfig == null)
                        {
                            if (ConfigurationManager.AppSettings["GenericObjectConfig"] != null)
                            {
                                XmlSerializer serializer = new XmlSerializer(typeof(GenericObjectConfig));
                                string filePath = ConfigurationManager.AppSettings["GenericObjectConfig"];
                                filePath = FixBasePath(filePath);

                                if (filePath.Contains("http"))
                                {
                                    StreamReader streamr = new StreamReader(System.Net.WebRequest.Create(filePath).GetResponse().GetResponseStream());
                                    m_genericObjectConfig = (GenericObjectConfig)serializer.Deserialize(streamr);
                                }
                                else
                                {
                                    StringReader reader = new StringReader(File.ReadAllText(filePath));
                                    m_genericObjectConfig = (GenericObjectConfig)serializer.Deserialize(reader);
                                }

                            }
                        }
                    }
                }

                return m_genericObjectConfig;
            }
        }

        static string FixBasePath(string path)
        {
            try
            {
                string basePath;
                if (string.IsNullOrEmpty(cloudPath))
                    basePath = ConfigurationManager.AppSettings["BasePath"];
                else
                    basePath = cloudPath;

                if (basePath == "~")
                {
                    basePath = getHttpUrl();//System.Web.HttpContext.Current.Request.ServerVariables["PATH_INFO"].Replace("/WMLS.asmx", "");  // System.Web.HttpContext.Current.Server.so; System.Web.HttpContext.Current.Server.MapPath(serverPath);
                }
                else if (basePath == ".")
                {
                    basePath = System.AppDomain.CurrentDomain.BaseDirectory;
                }

                if (!String.IsNullOrEmpty(basePath))
                {
                    return path.Replace("{BASEPATH}", basePath);
                }
                else
                {
                    return path;
                }
            }
            catch
            {
                return path;
            }
        }

        static string getHttpUrl()
        {

            string protocol = System.Web.HttpContext.Current.Request.ServerVariables["SERVER_PORT_SECURE"];
            if ((protocol == null) || (protocol == "0"))
            {
                protocol = "http://";
            }
            else
                protocol = "https://";
            string host = protocol + System.Web.HttpContext.Current.Request.ServerVariables["HTTP_HOST"];
            host += System.Web.HttpContext.Current.Request.ApplicationPath;
            if (host.EndsWith("/"))
                host = host.Substring(0, host.Length - 1);
            return host;
        }

        public bool IsObjectTypeRegistered(string objectTypeName)
        {
            IEnumerable<string> list = from obj in GenericObjectConfig.Instance.Object
                                       where obj.objectTypeName == objectTypeName
                                       select obj.objectTypeName;
            if (list.Any())
            {
                return true;
            }

            return false;
        }
    }
}
