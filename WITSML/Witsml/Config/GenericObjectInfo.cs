﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Xml.Serialization;

namespace Witsml.Config
{
    [Serializable]
    public class GenericObjectInfo
    {
        public string objectTypeName;
        public string pluralTypeName;
        public Guid objectTypeID;
    }
}
