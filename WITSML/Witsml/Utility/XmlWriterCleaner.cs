﻿ 

using System;
using System.Collections.Generic;
using System.Text;
using System.Xml;
using System.IO;

namespace Witsml.Utility
{
    /// <summary>
    /// This class is used for Serialize without Declaration and xmlns
    /// </summary>
    public class XmlWriterCleaner : XmlWrappingWriter
    {
        bool _skip = false;

        public XmlWriterCleaner(XmlWriter writer) : base(writer, CreateSetting(null)) { }

        /// <summary>
        /// Encoding is UTF 8
        /// </summary>
        /// <param name="writer"></param>
        public XmlWriterCleaner(TextWriter writer)
            : base(writer, CreateSetting(null))
        {
        }

        public XmlWriterCleaner(TextWriter writer, Encoding encoding)
            : base(writer, CreateSetting(encoding))
        {
        }

        /// <summary>
        /// Encoding is UTF 8
        /// </summary>
        /// <param name="writer"></param>
        public XmlWriterCleaner(Stream stream)
            : base(stream, CreateSetting(null))
        {
        }

        public XmlWriterCleaner(Stream stream, Encoding encoding)
            : base(stream, CreateSetting(encoding))
        {
        }

        public override void WriteStartDocument()
        {
            //Do Nothing
        }
        public override void WriteStartElement(string prefix, string localName, string ns)
        {
            base.WriteStartElement(null, localName, null);
        }
        public override void WriteStartAttribute(string prefix, string localName, string ns)
        {
            //xmlns:xsd && xmlns:xsi is to be omitted
            bool namespaceExtras = (prefix == "xmlns" && (localName == "xsd" || localName == "xsi"));
            if (namespaceExtras || localName == "xmlns" || localName == "version")
            {
                _skip = true;
                return;
            }
            base.WriteStartAttribute(null, localName, null);
        }
        public override void WriteEndAttribute()
        {
            if (_skip)
            {
                // Reset the flag, so we keep writing.
                _skip = false;
                return;
            }
            base.WriteEndAttribute();
        }
        public override void WriteString(string text)
        {
            if (_skip) return;
            base.WriteString(text);
        }

        //static function to create setting with default encoding utf8 and no declaration
        private static XmlWriterSettings CreateSetting(Encoding encoding)
        {
            XmlWriterSettings settings = new XmlWriterSettings();
            settings.OmitXmlDeclaration = true;
            settings.Encoding = encoding == null ? System.Text.Encoding.UTF8 : encoding;
            return settings;
        }
    }
}
