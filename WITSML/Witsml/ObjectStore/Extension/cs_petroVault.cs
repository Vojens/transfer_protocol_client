﻿ 

using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Xml;

namespace Witsml.ObjectStore
{
    public partial class cs_square : IHandleXML
    {      
        public void HandleXML(XmlReader xmlReader, bool isNew)
        {
            using (xmlReader)
            {
                while (xmlReader.Read())
                {
                    switch (xmlReader.Name)
                    {
                        case "objectID":
                            this.objectID = StaticParser.ParseGuidFromString(StaticHelper.ReadString(xmlReader));
                            break;
                        case "private":
                            this.privateData = StaticParser.ParseBooleanFromString(StaticHelper.ReadString(xmlReader));
                            break;
                        default:
                            break;
                    }
                }
            }
        }
    }
}
