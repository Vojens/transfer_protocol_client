﻿ 

using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Xml.Serialization;

namespace Witsml.ObjectStore
{
    public partial class cs_square
    {
        /// <remarks/>
        public Guid objectID;
        [XmlElement("private")]
        public bool privateData;
    }
}
