 

using System;
using System.Collections.Generic;
using System.Text;
using System.IO;
using System.Xml;
using System.Xml.Serialization;


namespace Witsml
{
    public class WITSMLSubElement
    {
        public string version;

        [XmlIgnore]
        public OperationType operationType;

        public virtual void Decode(ref string xmlIn)
        {
            //overriden on WITSMLSubElement child class
        }

        /// <summary>
        /// Serialize
        /// </summary>
        /// <returns>result of serialization</returns>
        public virtual string Serialize()
        {
            return this.Serialize(null);
        }

        /// <summary>
        /// Serialize object with override
        /// </summary>
        /// <param name="xmlAttributeOverrides"></param>
        /// <returns>result of serialization</returns>
        protected string Serialize(XmlAttributeOverrides xmlAttributeOverrides)
        {
            XmlSerializer serializer;
            if (xmlAttributeOverrides == null)
                //generic serialization
                serializer = new XmlSerializer(this.GetType());
            else
                //custom serialization with override
                serializer = new XmlSerializer(this.GetType(), xmlAttributeOverrides);
            //string returned need to be in utf-8
            XmlWriterSettings settings = new XmlWriterSettings();
            settings.Encoding = System.Text.Encoding.UTF8;
            using (MemoryStream ms = new MemoryStream())
            {
                using (XmlWriter writer = XmlWriter.Create(ms,settings))
                {
                    serializer.Serialize(writer, this);
                }
                ms.Seek(0, System.IO.SeekOrigin.Begin);
                using (StreamReader sr = new StreamReader(ms))
                {
                    return sr.ReadToEnd();
                }
                
            }
        }

        /// <summary>
        /// Return WITSMLSubElement Object from xml string using XML deserialization
        /// </summary>
        /// <typeparam name="T">Type of WITSMLSubElement</typeparam>
        /// <param name="xmlIn">xml string</param>
        /// <returns>new WITSMLSubElement</returns>
        public static T Deserialize<T>(ref string xmlIn) where T : WITSMLSubElement
        {
            XmlSerializer serializer = new XmlSerializer(typeof(T));
            using (StringReader reader = new StringReader(xmlIn))
            {
                T tObj = (T)serializer.Deserialize(reader);
                return tObj;
            }
        }

        /// <summary>
        /// Check for existence of recurring object
        /// </summary>
        /// <typeparam name="T">recurring object type</typeparam>
        /// <param name="uid">uid of recurring object</param>
        /// <param name="dictionaryToMatch">additional information to match about the recurring object</param>
        /// <returns>object from DB that match</returns>
        protected virtual T CheckForExistence<T>(string uid, Dictionary<string, string> dictionaryToMatch) where T : WITSMLSubElement, new()
        {
            //1.Check Type and Get xmlstring From DB
            //2.Deserialize/Decode using xmlstring
            //3.Return object from step 2
            //Do nothing here as it will be overriden on store
            return default(T);
        }

        /// <summary>
        /// Do Add or Update to Database for recurring object
        /// </summary>
        /// <typeparam name="T">type of object to add/update</typeparam>
        /// <param name="obj">the object to add or update in db</param>
        protected virtual void AddUdpdate<T>(T obj, OperationType operationType) where T : WITSMLSubElement
        {
            //to be overriden on store
        }
    }
}
