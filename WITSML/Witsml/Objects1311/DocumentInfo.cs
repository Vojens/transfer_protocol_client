 

using System;
using System.Collections.Generic;
using System.Text;

namespace Witsml.Objects1311
{
    /// <remarks/>
    [System.CodeDom.Compiler.GeneratedCodeAttribute("xsd", "2.0.50727.42")]
    
    
    
    
    [System.Xml.Serialization.XmlRootAttribute("documentInfo", Namespace = "http://www.witsml.org/schemas/131", IsNullable = false)]
    public partial class cs_documentInfo
    {

        /// <remarks/>
        public nameStruct DocumentName;

        /// <remarks/>
        [System.Xml.Serialization.XmlElementAttribute("DocumentAlias")]
        public nameStruct[] DocumentAlias;

        /// <remarks/>
        public System.DateTime DocumentDate;

        /// <remarks/>
        [System.Xml.Serialization.XmlIgnoreAttribute()]
        public bool DocumentDateSpecified;

        /// <remarks/>
        [System.Xml.Serialization.XmlElementAttribute("documentClass")]
        public nameStruct[] documentClass;

        /// <remarks/>
        public fileCreationType FileCreationInformation;

        /// <remarks/>
        [System.Xml.Serialization.XmlElementAttribute("SecurityInformation")]
        public securityInfoType[] SecurityInformation;

        /// <remarks/>
        public string Disclaimer;

        /// <remarks/>
        [System.Xml.Serialization.XmlArrayItemAttribute("Event", IsNullable = false)]
        public eventType[] AuditTrail;

        /// <remarks/>
        public string Owner;

        /// <remarks/>
        public string Comment;
    }

    /// <remarks/>
    [System.CodeDom.Compiler.GeneratedCodeAttribute("xsd", "2.0.50727.42")]
    
    
    
    
    public partial class nameStruct
    {

        /// <remarks/>
        [System.Xml.Serialization.XmlAttributeAttribute()]
        public string namingSystem;

        /// <remarks/>
        [System.Xml.Serialization.XmlTextAttribute()]
        public string Value;
    }

    /// <remarks/>
    [System.CodeDom.Compiler.GeneratedCodeAttribute("xsd", "2.0.50727.42")]
    
    
    
    
    public partial class refNameString
    {

        /// <remarks/>
        [System.Xml.Serialization.XmlAttributeAttribute()]
        public string uidRef;

        /// <remarks/>
        [System.Xml.Serialization.XmlTextAttribute()]
        public string Value;
    }


    /// <remarks/>
    [System.CodeDom.Compiler.GeneratedCodeAttribute("xsd", "2.0.50727.42")]
    
    
    
    
    public partial class securityInfoType
    {

        /// <remarks/>
        public string Class;

        /// <remarks/>
        public string System;

        /// <remarks/>
        public System.DateTime EndDate;

        /// <remarks/>
        [System.Xml.Serialization.XmlIgnoreAttribute()]
        public bool EndDateSpecified;

        /// <remarks/>
        public string Comment;
    }

    /// <remarks/>
    [System.CodeDom.Compiler.GeneratedCodeAttribute("xsd", "2.0.50727.42")]
    
    
    
    
    public partial class fileCreationType
    {

        /// <remarks/>
        public System.DateTime FileCreationDate;

        /// <remarks/>
        public string SoftwareName;

        /// <remarks/>
        public string FileCreator;

        /// <remarks/>
        public string Comment;
    }

    /// <remarks/>
    [System.CodeDom.Compiler.GeneratedCodeAttribute("xsd", "2.0.50727.42")]
    
    
    
    
    public partial class eventType
    {

        /// <remarks/>
        public System.DateTime EventDate;

        /// <remarks/>
        public string ResponsibleParty;

        /// <remarks/>
        public string Comment;
    }
}
