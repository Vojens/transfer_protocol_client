 

//------------------------------------------------------------------------------
// <auto-generated>
//     This code was generated by a tool.
//     Runtime Version:2.0.50727.1378
//
//     Changes to this file may cause incorrect behavior and will be lost if
//     the code is regenerated.
// </auto-generated>
//------------------------------------------------------------------------------

namespace Witsml.Objects1311
{
    using System.Xml.Serialization;

    // 
    // This source code was auto-generated by xsd, Version=2.0.50727.42.
    // 


    /// <remarks/>
    [System.CodeDom.Compiler.GeneratedCodeAttribute("xsd", "2.0.50727.42")]
    
    
    
    
    [XmlRoot("formationMarkers", Namespace = "http://www.witsml.org/schemas/131", IsNullable = false)]
    public partial class obj_formationMarkers
    {

        /// <remarks/>
        public cs_documentInfo documentInfo;

        /// <remarks/>
        [XmlElement("formationMarker")]
        public obj_formationMarker[] formationMarker;

        /// <remarks/>
        [XmlAttribute()]
        public string version;
    }

    /// <remarks/>
    [System.CodeDom.Compiler.GeneratedCodeAttribute("xsd", "2.0.50727.42")]
    
    
    
    
    public partial class obj_formationMarker
    {

        /// <remarks/>
        public string nameWell;

        /// <remarks/>
        public string nameWellbore;

        /// <remarks/>
        public string name;

        /// <remarks/>
        public measuredDepthCoord mdPrognosed;

        /// <remarks/>
        public wellVerticalDepthCoord tvdPrognosed;

        /// <remarks/>
        public measuredDepthCoord mdTopSample;

        /// <remarks/>
        public wellVerticalDepthCoord tvdTopSample;

        /// <remarks/>
        public lengthMeasure thicknessBed;

        /// <remarks/>
        public lengthMeasure thicknessApparent;

        /// <remarks/>
        public lengthMeasure thicknessPerpen;

        /// <remarks/>
        public measuredDepthCoord mdLogSample;

        /// <remarks/>
        public wellVerticalDepthCoord tvdLogSample;

        /// <remarks/>
        public planeAngleMeasure dip;

        /// <remarks/>
        public planeAngleMeasure dipDirection;

        /// <remarks/>
        public string chronostratigraphic;

        /// <remarks/>
        public string nameFormation;

        /// <remarks/>
        public string description;

        /// <remarks/>
        public cs_commonData commonData;

        /// <remarks/>
        public cs_customData customData;

        /// <remarks/>
        [XmlAttribute()]
        public string uidWell;

        /// <remarks/>
        [XmlAttribute()]
        public string uidWellbore;

        /// <remarks/>
        [XmlAttribute()]
        public string uid;
    }

}