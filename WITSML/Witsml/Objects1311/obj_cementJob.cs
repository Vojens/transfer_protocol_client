﻿ 

//------------------------------------------------------------------------------
// <auto-generated>
//     This code was generated by a tool.
//     Runtime Version:2.0.50727.1433
//
//     Changes to this file may cause incorrect behavior and will be lost if
//     the code is regenerated.
// </auto-generated>
//------------------------------------------------------------------------------

// 
// This source code was auto-generated by xsd, Version=2.0.50727.42.
// 
namespace Witsml.Objects1311 {
    using System.Xml.Serialization;
    
    
    /// <remarks/>
    [System.CodeDom.Compiler.GeneratedCodeAttribute("xsd", "2.0.50727.42")]
    
    
    
    [XmlType(Namespace="http://www.witsml.org/schemas/131")]
    [XmlRoot("cementJobs", Namespace="http://www.witsml.org/schemas/131", IsNullable=false)]
    public partial class obj_cementJobs {
        
        /// <remarks/>
        public cs_documentInfo documentInfo;
        
        /// <remarks/>
        [XmlElement("cementJob")]
        public obj_cementJob[] cementJob;
        
        /// <remarks/>
        [XmlAttribute()]
        public string version;
    }
    
    
    /// <remarks/>
    [System.CodeDom.Compiler.GeneratedCodeAttribute("xsd", "2.0.50727.42")]
    
    
    
    [XmlType(Namespace="http://www.witsml.org/schemas/131")]
    public partial class cs_cementAdditive {
        
        /// <remarks/>
        public string nameAdd;
        
        /// <remarks/>
        public string typeAdd;
        
        /// <remarks/>
        public string formAdd;
        
        /// <remarks/>
        public densityMeasure densAdd;
        
        /// <remarks/>
        [XmlElement("concentration", typeof(massConcentrationMeasure))]
        [XmlElement("typeConc", typeof(string))]
        [XmlElement("volSack", typeof(volumeMeasure))]
        [XmlElement("wtSack", typeof(massMeasure))]
        public object[] Items;
        
        /// <remarks/>
        public massMeasure additive;
        
        /// <remarks/>
        [XmlAttribute()]
        public string uid;
    }
    
    /// <remarks/>
    [System.CodeDom.Compiler.GeneratedCodeAttribute("xsd", "2.0.50727.42")]
    
    
    
    [XmlType(Namespace="http://www.witsml.org/schemas/131")]
    public partial class cs_cementPumpSchedule {
        
        /// <remarks/>
        public timeMeasure eTimPump;
        
        /// <remarks/>
        public volumeFlowRateMeasure ratePump;
        
        /// <remarks/>
        public volumeMeasure volPump;
        
        /// <remarks/>
        public short strokePump;
        
        /// <remarks/>
        [XmlIgnore()]
        public bool strokePumpSpecified;
        
        /// <remarks/>
        public pressureMeasure presBack;
        
        /// <remarks/>
        public timeMeasure eTimShutdown;
        
        /// <remarks/>
        public string comments;
    }
    
    /// <remarks/>
    [System.CodeDom.Compiler.GeneratedCodeAttribute("xsd", "2.0.50727.42")]
    
    
    
    [XmlType(Namespace="http://www.witsml.org/schemas/131")]
    public partial class cs_cementingFluid {
        
        /// <remarks/>
        public string typeFluid;
        
        /// <remarks/>
        public short fluidIndex;
        
        /// <remarks/>
        [XmlIgnore()]
        public bool fluidIndexSpecified;
        
        /// <remarks/>
        public string descFluid;
        
        /// <remarks/>
        public string purpose;
        
        /// <remarks/>
        public string classSlurryDryBlend;
        
        /// <remarks/>
        public measuredDepthCoord mdFluidTop;
        
        /// <remarks/>
        public measuredDepthCoord mdFluidBottom;
        
        /// <remarks/>
        public string sourceWater;
        
        /// <remarks/>
        public volumeMeasure volWater;
        
        /// <remarks/>
        public volumeMeasure volCement;
        
        /// <remarks/>
        public specificVolumeMeasure ratioMixWater;
        
        /// <remarks/>
        public volumeMeasure volFluid;
        
        /// <remarks/>
        public cs_cementPumpSchedule cementPumpSchedule;
        
        /// <remarks/>
        public volumePerVolumeMeasure excessPc;
        
        /// <remarks/>
        public specificVolumeMeasure volYield;
        
        /// <remarks/>
        public densityMeasure density;
        
        /// <remarks/>
        public volumePerVolumeMeasure solidVolumeFraction;
        
        /// <remarks/>
        public volumeMeasure volPumped;
        
        /// <remarks/>
        public volumeMeasure volOther;
        
        /// <remarks/>
        public string fluidRheologicalModel;
        
        /// <remarks/>
        public dynamicViscosityMeasure vis;
        
        /// <remarks/>
        public pressureMeasure yp;
        
        /// <remarks/>
        public dimensionlessMeasure n;
        
        /// <remarks/>
        public dimensionlessMeasure k;
        
        /// <remarks/>
        public planeAngleMeasure gel10SecReading;
        
        /// <remarks/>
        public pressureMeasure gel10SecStrength;
        
        /// <remarks/>
        public planeAngleMeasure gel1MinReading;
        
        /// <remarks/>
        public pressureMeasure gel1MinStrength;
        
        /// <remarks/>
        public planeAngleMeasure gel10MinReading;
        
        /// <remarks/>
        public pressureMeasure gel10MinStrength;
        
        /// <remarks/>
        public string typeBaseFluid;
        
        /// <remarks/>
        public densityMeasure densBaseFluid;
        
        /// <remarks/>
        public string dryBlendName;
        
        /// <remarks/>
        public string dryBlendDescription;
        
        /// <remarks/>
        public massMeasure massDryBlend;
        
        /// <remarks/>
        public densityMeasure densDryBlend;
        
        /// <remarks/>
        public massMeasure massSackDryBlend;
        
        /// <remarks/>
        [XmlElement("cementAdditive")]
        public cs_cementAdditive[] cementAdditive;
        
        /// <remarks/>
        public bool foamUsed;
        
        /// <remarks/>
        [XmlIgnore()]
        public bool foamUsedSpecified;
        
        /// <remarks/>
        public string typeGasFoam;
        
        /// <remarks/>
        public volumeMeasure volGasFoam;
        
        /// <remarks/>
        public volumePerVolumeMeasure ratioConstGasMethodAv;
        
        /// <remarks/>
        public densityMeasure densConstGasMethod;
        
        /// <remarks/>
        public volumePerVolumeMeasure ratioConstGasMethodStart;
        
        /// <remarks/>
        public volumePerVolumeMeasure ratioConstGasMethodEnd;
        
        /// <remarks/>
        public densityMeasure densConstGasFoam;
        
        /// <remarks/>
        public timeMeasure eTimThickening;
        
        /// <remarks/>
        public thermodynamicTemperatureMeasure tempThickening;
        
        /// <remarks/>
        public pressureMeasure presTestThickening;
        
        /// <remarks/>
        public dimensionlessMeasure consTestThickening;
        
        /// <remarks/>
        public volumePerVolumeMeasure pcFreeWater;
        
        /// <remarks/>
        public thermodynamicTemperatureMeasure tempFreeWater;
        
        /// <remarks/>
        public volumeMeasure volTestFluidLoss;
        
        /// <remarks/>
        public thermodynamicTemperatureMeasure tempFluidLoss;
        
        /// <remarks/>
        public pressureMeasure presTestFluidLoss;
        
        /// <remarks/>
        public timeMeasure timeFluidLoss;
        
        /// <remarks/>
        public volumeMeasure volAPIFluidLoss;
        
        /// <remarks/>
        public timeMeasure eTimComprStren1;
        
        /// <remarks/>
        public timeMeasure eTimComprStren2;
        
        /// <remarks/>
        public pressureMeasure presComprStren1;
        
        /// <remarks/>
        public pressureMeasure presComprStren2;
        
        /// <remarks/>
        public thermodynamicTemperatureMeasure tempComprStren1;
        
        /// <remarks/>
        public thermodynamicTemperatureMeasure tempComprStren2;
        
        /// <remarks/>
        public densityMeasure densAtPres;
        
        /// <remarks/>
        public volumeMeasure volReserved;
        
        /// <remarks/>
        public volumeMeasure volTotSlurry;
    }
    
    /// <remarks/>
    [System.CodeDom.Compiler.GeneratedCodeAttribute("xsd", "2.0.50727.42")]
    
    
    
    [XmlType(Namespace="http://www.witsml.org/schemas/131")]
    public partial class cs_cementStage {
        
        /// <remarks/>
        public short numStage;
        
        /// <remarks/>
        public string typeStage;
        
        /// <remarks/>
        public System.DateTime dTimMixStart;
        
        /// <remarks/>
        [XmlIgnore()]
        public bool dTimMixStartSpecified;
        
        /// <remarks/>
        public System.DateTime dTimPumpStart;
        
        /// <remarks/>
        [XmlIgnore()]
        public bool dTimPumpStartSpecified;
        
        /// <remarks/>
        public System.DateTime dTimPumpEnd;
        
        /// <remarks/>
        [XmlIgnore()]
        public bool dTimPumpEndSpecified;
        
        /// <remarks/>
        public System.DateTime dTimDisplaceStart;
        
        /// <remarks/>
        [XmlIgnore()]
        public bool dTimDisplaceStartSpecified;
        
        /// <remarks/>
        public measuredDepthCoord mdTop;
        
        /// <remarks/>
        public measuredDepthCoord mdBottom;
        
        /// <remarks/>
        public volumeMeasure volExcess;
        
        /// <remarks/>
        public volumeFlowRateMeasure flowrateDisplaceAv;
        
        /// <remarks/>
        public volumeFlowRateMeasure flowrateDisplaceMx;
        
        /// <remarks/>
        public pressureMeasure presDisplace;
        
        /// <remarks/>
        public volumeMeasure volReturns;
        
        /// <remarks/>
        public timeMeasure eTimMudCirculation;
        
        /// <remarks/>
        public volumeFlowRateMeasure flowrateMudCirc;
        
        /// <remarks/>
        public pressureMeasure presMudCirc;
        
        /// <remarks/>
        public volumeFlowRateMeasure flowrateEnd;
        
        /// <remarks/>
        public cs_cementingFluid cementingFluid;
        
        /// <remarks/>
        public bool afterFlowAnn;
        
        /// <remarks/>
        [XmlIgnore()]
        public bool afterFlowAnnSpecified;
        
        /// <remarks/>
        public string squeezeObj;
        
        /// <remarks/>
        public bool squeezeObtained;
        
        /// <remarks/>
        [XmlIgnore()]
        public bool squeezeObtainedSpecified;
        
        /// <remarks/>
        public measuredDepthCoord mdString;
        
        /// <remarks/>
        public measuredDepthCoord mdTool;
        
        /// <remarks/>
        public measuredDepthCoord mdCoilTbg;
        
        /// <remarks/>
        public volumeMeasure volCsgIn;
        
        /// <remarks/>
        public volumeMeasure volCsgOut;
        
        /// <remarks/>
        public bool tailPipeUsed;
        
        /// <remarks/>
        [XmlIgnore()]
        public bool tailPipeUsedSpecified;
        
        /// <remarks/>
        public lengthMeasure diaTailPipe;
        
        /// <remarks/>
        public bool tailPipePerf;
        
        /// <remarks/>
        [XmlIgnore()]
        public bool tailPipePerfSpecified;
        
        /// <remarks/>
        public pressureMeasure presTbgStart;
        
        /// <remarks/>
        public pressureMeasure presTbgEnd;
        
        /// <remarks/>
        public pressureMeasure presCsgStart;
        
        /// <remarks/>
        public pressureMeasure presCsgEnd;
        
        /// <remarks/>
        public pressureMeasure presBackPressure;
        
        /// <remarks/>
        public pressureMeasure presCoilTbgStart;
        
        /// <remarks/>
        public pressureMeasure presCoilTbgEnd;
        
        /// <remarks/>
        public pressureMeasure presBreakDown;
        
        /// <remarks/>
        public volumeFlowRateMeasure flowrateBreakDown;
        
        /// <remarks/>
        public pressureMeasure presSqueezeAv;
        
        /// <remarks/>
        public pressureMeasure presSqueezeEnd;
        
        /// <remarks/>
        public bool presSqueezeHeld;
        
        /// <remarks/>
        [XmlIgnore()]
        public bool presSqueezeHeldSpecified;
        
        /// <remarks/>
        public pressureMeasure presSqueeze;
        
        /// <remarks/>
        public timeMeasure eTimPresHeld;
        
        /// <remarks/>
        public volumeFlowRateMeasure flowrateSqueezeAv;
        
        /// <remarks/>
        public volumeFlowRateMeasure flowrateSqueezeMx;
        
        /// <remarks/>
        public volumeFlowRateMeasure flowratePumpStart;
        
        /// <remarks/>
        public volumeFlowRateMeasure flowratePumpEnd;
        
        /// <remarks/>
        public bool pillBelowPlug;
        
        /// <remarks/>
        [XmlIgnore()]
        public bool pillBelowPlugSpecified;
        
        /// <remarks/>
        public bool plugCatcher;
        
        /// <remarks/>
        [XmlIgnore()]
        public bool plugCatcherSpecified;
        
        /// <remarks/>
        public measuredDepthCoord mdCircOut;
        
        /// <remarks/>
        public volumeMeasure volCircPrior;
        
        /// <remarks/>
        public string typeOriginalMud;
        
        /// <remarks/>
        public densityMeasure wtMud;
        
        /// <remarks/>
        public timeMeasure visFunnelMud;
        
        /// <remarks/>
        public dynamicViscosityMeasure pvMud;
        
        /// <remarks/>
        public pressureMeasure ypMud;
        
        /// <remarks/>
        public pressureMeasure gel10Sec;
        
        /// <remarks/>
        public pressureMeasure gel10Min;
        
        /// <remarks/>
        public thermodynamicTemperatureMeasure tempBHCT;
        
        /// <remarks/>
        public thermodynamicTemperatureMeasure tempBHST;
        
        /// <remarks/>
        public string volExcessMethod;
        
        /// <remarks/>
        public string mixMethod;
        
        /// <remarks/>
        public string densMeasBy;
        
        /// <remarks/>
        public bool annFlowAfter;
        
        /// <remarks/>
        [XmlIgnore()]
        public bool annFlowAfterSpecified;
        
        /// <remarks/>
        public bool topPlug;
        
        /// <remarks/>
        [XmlIgnore()]
        public bool topPlugSpecified;
        
        /// <remarks/>
        public bool botPlug;
        
        /// <remarks/>
        [XmlIgnore()]
        public bool botPlugSpecified;
        
        /// <remarks/>
        public short botPlugNumber;
        
        /// <remarks/>
        [XmlIgnore()]
        public bool botPlugNumberSpecified;
        
        /// <remarks/>
        public bool plugBumped;
        
        /// <remarks/>
        [XmlIgnore()]
        public bool plugBumpedSpecified;
        
        /// <remarks/>
        public pressureMeasure presPriorBump;
        
        /// <remarks/>
        public pressureMeasure presBump;
        
        /// <remarks/>
        public pressureMeasure presHeld;
        
        /// <remarks/>
        public bool floatHeld;
        
        /// <remarks/>
        [XmlIgnore()]
        public bool floatHeldSpecified;
        
        /// <remarks/>
        public volumeMeasure volMudLost;
        
        /// <remarks/>
        public string fluidDisplace;
        
        /// <remarks/>
        public densityMeasure densDisplaceFluid;
        
        /// <remarks/>
        public volumeMeasure volDisplaceFluid;
        
        /// <remarks/>
        [XmlAttribute()]
        public string uid;
    }
    
    
    /// <remarks/>
    [System.CodeDom.Compiler.GeneratedCodeAttribute("xsd", "2.0.50727.42")]
    
    
    
    [XmlType(Namespace="http://www.witsml.org/schemas/131")]
    public partial class obj_cementJob {
        
        /// <remarks/>
        public string nameWell;
        
        /// <remarks/>
        public string nameWellbore;
        
        /// <remarks/>
        public string name;
        
        /// <remarks/>
        public CementJobType jobType;
        
        /// <remarks/>
        [XmlIgnore()]
        public bool jobTypeSpecified;
        
        /// <remarks/>
        public string jobConfig;
        
        /// <remarks/>
        public System.DateTime dTimJob;
        
        /// <remarks/>
        [XmlIgnore()]
        public bool dTimJobSpecified;
        
        /// <remarks/>
        public string nameCementedString;
        
        /// <remarks/>
        public cs_wbGeometry holeConfig;
        
        /// <remarks/>
        public string nameWorkString;
        
        /// <remarks/>
        public string contractor;
        
        /// <remarks/>
        public string cementEngr;
        
        /// <remarks/>
        public bool offshoreJob;
        
        /// <remarks/>
        [XmlIgnore()]
        public bool offshoreJobSpecified;
        
        /// <remarks/>
        public measuredDepthCoord mdWater;
        
        /// <remarks/>
        public bool returnsToSeabed;
        
        /// <remarks/>
        [XmlIgnore()]
        public bool returnsToSeabedSpecified;
        
        /// <remarks/>
        public bool reciprocating;
        
        /// <remarks/>
        [XmlIgnore()]
        public bool reciprocatingSpecified;
        
        /// <remarks/>
        public timeMeasure woc;
        
        /// <remarks/>
        public measuredDepthCoord mdPlugTop;
        
        /// <remarks/>
        public measuredDepthCoord mdPlugBot;
        
        /// <remarks/>
        public measuredDepthCoord mdHole;
        
        /// <remarks/>
        public measuredDepthCoord mdShoe;
        
        /// <remarks/>
        public wellVerticalDepthCoord tvdShoe;
        
        /// <remarks/>
        public measuredDepthCoord mdStringSet;
        
        /// <remarks/>
        public wellVerticalDepthCoord tvdStringSet;
        
        /// <remarks/>
        [XmlElement("cementStage")]
        public cs_cementStage[] cementStage;
        
        /// <remarks/>
        public cs_cementTest cementTest;
        
        /// <remarks/>
        public string typePlug;
        
        /// <remarks/>
        public string nameCementString;
        
        /// <remarks/>
        public System.DateTime dTimPlugSet;
        
        /// <remarks/>
        [XmlIgnore()]
        public bool dTimPlugSetSpecified;
        
        /// <remarks/>
        public bool cementDrillOut;
        
        /// <remarks/>
        [XmlIgnore()]
        public bool cementDrillOutSpecified;
        
        /// <remarks/>
        public System.DateTime dTimCementDrillOut;
        
        /// <remarks/>
        [XmlIgnore()]
        public bool dTimCementDrillOutSpecified;
        
        /// <remarks/>
        public string typeSqueeze;
        
        /// <remarks/>
        public measuredDepthCoord mdSqueeze;
        
        /// <remarks/>
        public System.DateTime dTimSqueeze;
        
        /// <remarks/>
        [XmlIgnore()]
        public bool dTimSqueezeSpecified;
        
        /// <remarks/>
        public string toolCompany;
        
        /// <remarks/>
        public string typeTool;
        
        /// <remarks/>
        public System.DateTime dTimPipeRotStart;
        
        /// <remarks/>
        [XmlIgnore()]
        public bool dTimPipeRotStartSpecified;
        
        /// <remarks/>
        public System.DateTime dTimPipeRotEnd;
        
        /// <remarks/>
        [XmlIgnore()]
        public bool dTimPipeRotEndSpecified;
        
        /// <remarks/>
        public anglePerTimeMeasure rpmPipe;
        
        /// <remarks/>
        public momentOfForceMeasure tqInitPipeRot;
        
        /// <remarks/>
        public momentOfForceMeasure tqPipeAv;
        
        /// <remarks/>
        public momentOfForceMeasure tqPipeMx;
        
        /// <remarks/>
        public System.DateTime dTimRecipStart;
        
        /// <remarks/>
        [XmlIgnore()]
        public bool dTimRecipStartSpecified;
        
        /// <remarks/>
        public System.DateTime dTimRecipEnd;
        
        /// <remarks/>
        [XmlIgnore()]
        public bool dTimRecipEndSpecified;
        
        /// <remarks/>
        public forceMeasure overPull;
        
        /// <remarks/>
        public forceMeasure slackOff;
        
        /// <remarks/>
        public anglePerTimeMeasure rpmPipeRecip;
        
        /// <remarks/>
        public lengthMeasure lenPipeRecipStroke;
        
        /// <remarks/>
        public bool coilTubing;
        
        /// <remarks/>
        [XmlIgnore()]
        public bool coilTubingSpecified;
        
        /// <remarks/>
        public cs_commonData commonData;
        
        /// <remarks/>
        public cs_customData customData;
        
        /// <remarks/>
        [XmlAttribute()]
        public string uidWell;
        
        /// <remarks/>
        [XmlAttribute()]
        public string uidWellbore;
        
        /// <remarks/>
        [XmlAttribute()]
        public string uid;
    }

    /// <remarks/>
    [System.CodeDom.Compiler.GeneratedCodeAttribute("xsd", "2.0.50727.42")]
    
    
    
    
    public partial class cs_cementTest
    {

        /// <remarks/>
        public pressureMeasure presTest;

        /// <remarks/>
        public timeMeasure eTimTest;

        /// <remarks/>
        public bool cementShoeCollar;

        /// <remarks/>
        [XmlIgnore()]
        public bool cementShoeCollarSpecified;

        /// <remarks/>
        public bool cetRun;

        /// <remarks/>
        [XmlIgnore()]
        public bool cetRunSpecified;

        /// <remarks/>
        public bool cetBondQual;

        /// <remarks/>
        [XmlIgnore()]
        public bool cetBondQualSpecified;

        /// <remarks/>
        public bool cblRun;

        /// <remarks/>
        [XmlIgnore()]
        public bool cblRunSpecified;

        /// <remarks/>
        public bool cblBondQual;

        /// <remarks/>
        [XmlIgnore()]
        public bool cblBondQualSpecified;

        /// <remarks/>
        public pressureMeasure cblPres;

        /// <remarks/>
        public bool tempSurvey;

        /// <remarks/>
        [XmlIgnore()]
        public bool tempSurveySpecified;

        /// <remarks/>
        public timeMeasure eTimCementLog;

        /// <remarks/>
        public forcePerVolumeMeasure formPit;

        /// <remarks/>
        public string toolCompanyPit;

        /// <remarks/>
        public timeMeasure eTimPitStart;

        /// <remarks/>
        public measuredDepthCoord mdCementTop;

        /// <remarks/>
        public string topCementMethod;

        /// <remarks/>
        public bool tocOK;

        /// <remarks/>
        [XmlIgnore()]
        public bool tocOKSpecified;

        /// <remarks/>
        public string jobRating;

        /// <remarks/>
        public bool remedialCement;

        /// <remarks/>
        [XmlIgnore()]
        public bool remedialCementSpecified;

        /// <remarks/>
        public short numRemedial;

        /// <remarks/>
        [XmlIgnore()]
        public bool numRemedialSpecified;

        /// <remarks/>
        public string failureMethod;

        /// <remarks/>
        public lengthMeasure linerTop;

        /// <remarks/>
        public lengthMeasure linerLap;

        /// <remarks/>
        public timeMeasure eTimBeforeTest;

        /// <remarks/>
        public string testNegativeTool;

        /// <remarks/>
        public densityMeasure testNegativeEmw;

        /// <remarks/>
        public string testPositiveTool;

        /// <remarks/>
        public densityMeasure testPositiveEmw;

        /// <remarks/>
        public bool cementFoundOnTool;

        /// <remarks/>
        [XmlIgnore()]
        public bool cementFoundOnToolSpecified;

        /// <remarks/>
        public measuredDepthCoord mdDVTool;
    }

    /// <remarks/>
    [System.CodeDom.Compiler.GeneratedCodeAttribute("xsd", "2.0.50727.42")]
    
    [XmlType(Namespace="http://www.witsml.org/schemas/131")]
    public enum CementJobType {
        
        /// <remarks/>
        primary,
        
        /// <remarks/>
        plug,
        
        /// <remarks/>
        squeeze,
        
        /// <remarks/>
        unknown,
    }
    
}
