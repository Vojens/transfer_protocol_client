﻿using System.Xml.Serialization;

namespace Witsml.Objects1311
{
    /// <remarks/>
    [System.CodeDom.Compiler.GeneratedCodeAttribute("xsd", "2.0.50727.42")]
    [XmlType(Namespace = "http://www.witsml.org/schemas/131")]
    [XmlRoot("attachments", Namespace = "http://www.witsml.org/schemas/131", IsNullable = false)]

    public partial class obj_attachments
    {

        public cs_documentInfo documentInfo;

        /// <remarks/>
        [XmlElement("attachment")]
        public obj_attachment[] attachment;

        /// <remarks/>
        [XmlAttribute()]
        public string version;

    }


    /// <remarks/>
    //[System.CodeDom.Compiler.GeneratedCodeAttribute("xsd", "2.0.50727.42")]

    //[System.Xml.Serialization.XmlTypeAttribute(Namespace = "http://www.witsml.org/schemas/131")]
    //public partial class refObjectString
    //{

    //    /// <remarks/>
    //    [System.Xml.Serialization.XmlAttributeAttribute()]
    //    public string @object;

    //    /// <remarks/>
    //    [System.Xml.Serialization.XmlAttributeAttribute()]
    //    public string uidRef;

    //    /// <remarks/>
    //    [System.Xml.Serialization.XmlTextAttribute()]
    //    public string Value;
    //}

    /// <remarks/>
    [System.CodeDom.Compiler.GeneratedCodeAttribute("xsd", "2.0.50727.42")]


    [XmlType(Namespace = "http://www.witsml.org/schemas/131")]
    public partial class obj_attachment
    {

        private string nameWellField;

        private string nameWellboreField;

        private string nameField;

        private refObjectString objectReferenceField;

        private refObjectString subObjectReferenceField;

        private measuredDepthCoord mdField;

        private measuredDepthCoord mdBitField;

        private indexedObject[] paramField;

        private string fileNameField;

        private string descriptionField;

        private string fileTypeField;

        private byte[] contentField;

        private cs_commonData commonDataField;

        private cs_customData customDataField;

        private string uidWellField;

        private string uidWellboreField;

        private string uidField;

        /// <remarks/>
        public string nameWell
        {
            get
            {
                return this.nameWellField;
            }
            set
            {
                this.nameWellField = value;
            }
        }

        /// <remarks/>
        public string nameWellbore
        {
            get
            {
                return this.nameWellboreField;
            }
            set
            {
                this.nameWellboreField = value;
            }
        }

        /// <remarks/>
        public string name
        {
            get
            {
                return this.nameField;
            }
            set
            {
                this.nameField = value;
            }
        }

        /// <remarks/>
        public refObjectString objectReference
        {
            get
            {
                return this.objectReferenceField;
            }
            set
            {
                this.objectReferenceField = value;
            }
        }

        /// <remarks/>
        public refObjectString subObjectReference
        {
            get
            {
                return this.subObjectReferenceField;
            }
            set
            {
                this.subObjectReferenceField = value;
            }
        }

        /// <remarks/>
        public measuredDepthCoord md
        {
            get
            {
                return this.mdField;
            }
            set
            {
                this.mdField = value;
            }
        }

        /// <remarks/>
        public measuredDepthCoord mdBit
        {
            get
            {
                return this.mdBitField;
            }
            set
            {
                this.mdBitField = value;
            }
        }

        /// <remarks/>
        [XmlElement("param")]
        public indexedObject[] param
        {
            get
            {
                return this.paramField;
            }
            set
            {
                this.paramField = value;
            }
        }

        /// <remarks/>
        public string fileName
        {
            get
            {
                return this.fileNameField;
            }
            set
            {
                this.fileNameField = value;
            }
        }

        /// <remarks/>
        public string description
        {
            get
            {
                return this.descriptionField;
            }
            set
            {
                this.descriptionField = value;
            }
        }

        /// <remarks/>
        public string fileType
        {
            get
            {
                return this.fileTypeField;
            }
            set
            {
                this.fileTypeField = value;
            }
        }

        /// <remarks/>
        [XmlElement(DataType = "base64Binary")]
        public byte[] content
        {
            get
            {
                return this.contentField;
            }
            set
            {
                this.contentField = value;
            }
        }

        /// <remarks/>
        public cs_commonData commonData
        {
            get
            {
                return this.commonDataField;
            }
            set
            {
                this.commonDataField = value;
            }
        }

        /// <remarks/>
        public cs_customData customData
        {
            get
            {
                return this.customDataField;
            }
            set
            {
                this.customDataField = value;
            }
        }

        /// <remarks/>
        [XmlAttribute()]
        public string uidWell
        {
            get
            {
                return this.uidWellField;
            }
            set
            {
                this.uidWellField = value;
            }
        }

        /// <remarks/>
        [XmlAttribute()]
        public string uidWellbore
        {
            get
            {
                return this.uidWellboreField;
            }
            set
            {
                this.uidWellboreField = value;
            }
        }

        /// <remarks/>
        [XmlAttribute()]
        public string uid
        {
            get
            {
                return this.uidField;
            }
            set
            {
                this.uidField = value;
            }
        }
    }
}