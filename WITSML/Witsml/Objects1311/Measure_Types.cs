 

// Top : 
// Bottom: public enum accelerationLinearUom

namespace Witsml.Objects1311
{
    using System.Xml.Serialization;

#if OverrideMeasureEnums
    using PercentUom = System.String;
    using magneticInductionUom = System.String;
    using forcePerLengthUom = System.String;
    using WellVerticalCoordinateUom = System.String;
    using MeasuredDepthUom = System.String;
    using planeAngleUom = System.String;
    using AziRef = System.String;
    using volumePerVolumeUom = System.String;
    using volumeFlowRateUom = System.String;
    using volumeUom = System.String;
    using velocityUom = System.String;
    using timeUom = System.String;
    using thermodynamicTemperatureUom = System.String;
    using specificVolumeUom = System.String;
    using relativePowerUom = System.String;
    using pressureUom = System.String;
    using powerUom = System.String;
    using perLengthUom = System.String;
    using momentOfForceUom = System.String;
    using massPerLengthUom = System.String;
    using massUom = System.String;
    using massConcentrationUom = System.String;
    using magneticFieldStrengthUom = System.String;
    using lengthPerLengthUom = System.String;
    using lengthUom = System.String;
    using illuminanceUom = System.String;
    using frequencyUom = System.String;
    using forcePerVolumeUom = System.String;
    using forceUom = System.String;
    using equivalentPerMassUom = System.String;
    using energyPerAreaUom = System.String;
    using electricPotentialUom = System.String;
    using dynamicViscosityUom = System.String;
    using dimensionlessUom = System.String;
    using densityUom = System.String;
    using areaPerAreaUom = System.String;
    using areaUom = System.String;
    using anglePerTimeUom = System.String;
    using anglePerLengthUom = System.String;
    using accelerationLinearUom = System.String;
    using electricCurrentUom = System.String;
#endif

    /// <remarks/>
    [System.CodeDom.Compiler.GeneratedCodeAttribute("xsd", "2.0.50727.42")]
    
    
    
    
    public partial class genericMeasure : abstractMeasure
    {

        /// <remarks/>
        [XmlAttribute()]
        public string uom;
    }

    /// <remarks/>
    [XmlInclude(typeof(wellElevationCoord))]
    [XmlInclude(typeof(wellVerticalDepthCoord))]
    [XmlInclude(typeof(measuredDepthCoord))]
    [XmlInclude(typeof(ratioGenericMeasure))]
    [XmlInclude(typeof(genericMeasure))]
    [XmlInclude(typeof(volumePerVolumeMeasurePercent))]
    [XmlInclude(typeof(yAxisAzimuth))]
    [XmlInclude(typeof(temperatureSlopeMeasure))]
    [XmlInclude(typeof(generalMeasureType))]
    [XmlInclude(typeof(volumePerVolumeMeasure))]
    [XmlInclude(typeof(volumeFlowRateMeasure))]
    [XmlInclude(typeof(volumeMeasure))]
    [XmlInclude(typeof(velocityMeasure))]
    [XmlInclude(typeof(timeMeasure))]
    [XmlInclude(typeof(thermodynamicTemperatureMeasure))]
    [XmlInclude(typeof(specificVolumeMeasure))]
    [XmlInclude(typeof(relativePowerMeasure))]
    [XmlInclude(typeof(pressureMeasure))]
    [XmlInclude(typeof(powerMeasure))]
    [XmlInclude(typeof(planeAngleMeasure))]
    [XmlInclude(typeof(perLengthMeasure))]
    [XmlInclude(typeof(momentOfForceMeasure))]
    [XmlInclude(typeof(massPerLengthMeasure))]
    [XmlInclude(typeof(massMeasure))]
    [XmlInclude(typeof(massConcentrationMeasure))]
    [XmlInclude(typeof(magneticInductionMeasure))]
    [XmlInclude(typeof(magneticFieldStrengthMeasure))]
    [XmlInclude(typeof(lengthPerLengthMeasure))]
    [XmlInclude(typeof(lengthMeasure))]
    [XmlInclude(typeof(illuminanceMeasure))]
    [XmlInclude(typeof(frequencyMeasure))]
    [XmlInclude(typeof(forcePerVolumeMeasure))]
    [XmlInclude(typeof(forcePerLengthMeasure))]
    [XmlInclude(typeof(forceMeasure))]
    [XmlInclude(typeof(equivalentPerMassMeasure))]
    [XmlInclude(typeof(energyPerAreaMeasure))]
    [XmlInclude(typeof(electricPotentialMeasure))]
    [XmlInclude(typeof(electricCurrentMeasure))]
    [XmlInclude(typeof(dynamicViscosityMeasure))]
    [XmlInclude(typeof(dimensionlessMeasure))]
    [XmlInclude(typeof(densityMeasure))]
    [XmlInclude(typeof(areaPerAreaMeasure))]
    [XmlInclude(typeof(areaMeasure))]
    [XmlInclude(typeof(anglePerTimeMeasure))]
    [XmlInclude(typeof(anglePerLengthMeasure))]
    [XmlInclude(typeof(accelerationLinearMeasure))]
    [System.CodeDom.Compiler.GeneratedCodeAttribute("xsd", "2.0.50727.42")]
    
    
    
    
    public abstract partial class abstractMeasure
    {

        /// <remarks/>
        [XmlText()]
        public double Value;
    }

    /// <remarks/>
    [System.CodeDom.Compiler.GeneratedCodeAttribute("xsd", "2.0.50727.42")]
    
    
    
    
    public partial class wellElevationCoord : abstractMeasure
    {

        /// <remarks/>
        [XmlAttribute()]
        public WellVerticalCoordinateUom uom;

        /// <remarks/>
        [XmlAttribute()]
        public string datum;
    }



    /// <remarks/>
    [System.CodeDom.Compiler.GeneratedCodeAttribute("xsd", "2.0.50727.42")]
    
    
    
    
    public partial class wellVerticalDepthCoord : abstractMeasure
    {

        /// <remarks/>
        [XmlAttribute()]
        public WellVerticalCoordinateUom uom;

        /// <remarks/>
        [XmlAttribute()]
        public string datum;
    }

    /// <remarks/>
    [System.CodeDom.Compiler.GeneratedCodeAttribute("xsd", "2.0.50727.42")]
    
    
    
    
    public partial class measuredDepthCoord : abstractMeasure
    {

        /// <remarks/>
        [XmlAttribute()]
        public MeasuredDepthUom uom;

        /// <remarks/>
        [XmlAttribute()]
        public string datum;
    }



    /// <remarks/>
    [System.CodeDom.Compiler.GeneratedCodeAttribute("xsd", "2.0.50727.42")]
    
    
    
    
    public partial class ratioGenericMeasure : abstractMeasure
    {

        /// <remarks/>
        [XmlAttribute()]
        public string uom;

        /// <remarks/>
        [XmlAttribute()]
        public double numerator;

        /// <remarks/>
        [XmlIgnore()]
        public bool numeratorSpecified;

        /// <remarks/>
        [XmlAttribute()]
        public double denominator;

        /// <remarks/>
        [XmlIgnore()]
        public bool denominatorSpecified;
    }

    /// <remarks/>
    [System.CodeDom.Compiler.GeneratedCodeAttribute("xsd", "2.0.50727.42")]
    
    
    
    
    public partial class volumePerVolumeMeasurePercent : abstractMeasure
    {

        /// <remarks/>
        [XmlAttribute()]
        public PercentUom uom;
    }



    /// <remarks/>
    [System.CodeDom.Compiler.GeneratedCodeAttribute("xsd", "2.0.50727.42")]
    
    
    
    
    public partial class yAxisAzimuth : abstractMeasure
    {

        /// <remarks/>
        [XmlAttribute()]
        public planeAngleUom uom;

        /// <remarks/>
        [XmlAttribute()]
        public AziRef northDirection;

        /// <remarks/>
        [XmlIgnore()]
        public bool northDirectionSpecified;
    }



    /// <remarks/>
    [System.CodeDom.Compiler.GeneratedCodeAttribute("xsd", "2.0.50727.42")]
    
    
    
    
    public partial class temperatureSlopeMeasure : abstractMeasure
    {

        /// <remarks/>
        [XmlAttribute()]
        public string uom;
    }

    /// <remarks/>
    [System.CodeDom.Compiler.GeneratedCodeAttribute("xsd", "2.0.50727.42")]
    
    
    
    
    public partial class generalMeasureType : abstractMeasure
    {

        /// <remarks/>
        [XmlAttribute()]
        public string uom;
    }

    /// <remarks/>
    [System.CodeDom.Compiler.GeneratedCodeAttribute("xsd", "2.0.50727.42")]
    
    
    
    
    public partial class volumePerVolumeMeasure : abstractMeasure
    {

        /// <remarks/>
        [XmlAttribute()]
        public volumePerVolumeUom uom;
    }



    /// <remarks/>
    [System.CodeDom.Compiler.GeneratedCodeAttribute("xsd", "2.0.50727.42")]
    
    
    
    
    public partial class volumeFlowRateMeasure : abstractMeasure
    {

        /// <remarks/>
        [XmlAttribute()]
        public volumeFlowRateUom uom;
    }



    /// <remarks/>
    [System.CodeDom.Compiler.GeneratedCodeAttribute("xsd", "2.0.50727.42")]
    
    
    
    
    public partial class volumeMeasure : abstractMeasure
    {

        /// <remarks/>
        [XmlAttribute()]
        public volumeUom uom;
    }



    /// <remarks/>
    [System.CodeDom.Compiler.GeneratedCodeAttribute("xsd", "2.0.50727.42")]
    
    
    
    
    public partial class velocityMeasure : abstractMeasure
    {

        /// <remarks/>
        [XmlAttribute()]
        public velocityUom uom;
    }



    /// <remarks/>
    [System.CodeDom.Compiler.GeneratedCodeAttribute("xsd", "2.0.50727.42")]
    
    
    
    
    public partial class timeMeasure : abstractMeasure
    {

        /// <remarks/>
        [XmlAttribute()]
        public timeUom uom;
    }



    /// <remarks/>
    [System.CodeDom.Compiler.GeneratedCodeAttribute("xsd", "2.0.50727.42")]
    
    
    
    
    public partial class thermodynamicTemperatureMeasure : abstractMeasure
    {

        /// <remarks/>
        [XmlAttribute()]
        public thermodynamicTemperatureUom uom;
    }



    /// <remarks/>
    [System.CodeDom.Compiler.GeneratedCodeAttribute("xsd", "2.0.50727.42")]
    
    
    
    
    public partial class specificVolumeMeasure : abstractMeasure
    {

        /// <remarks/>
        [XmlAttribute()]
        public specificVolumeUom uom;
    }



    /// <remarks/>
    [System.CodeDom.Compiler.GeneratedCodeAttribute("xsd", "2.0.50727.42")]
    
    
    
    
    public partial class relativePowerMeasure : abstractMeasure
    {

        /// <remarks/>
        [XmlAttribute()]
        public relativePowerUom uom;
    }



    /// <remarks/>
    [System.CodeDom.Compiler.GeneratedCodeAttribute("xsd", "2.0.50727.42")]
    
    
    
    
    public partial class pressureMeasure : abstractMeasure
    {

        /// <remarks/>
        [XmlAttribute()]
        public pressureUom uom;
    }



    /// <remarks/>
    [System.CodeDom.Compiler.GeneratedCodeAttribute("xsd", "2.0.50727.42")]
    
    
    
    
    public partial class powerMeasure : abstractMeasure
    {

        /// <remarks/>
        [XmlAttribute()]
        public powerUom uom;
    }



    /// <remarks/>
    [System.CodeDom.Compiler.GeneratedCodeAttribute("xsd", "2.0.50727.42")]
    
    
    
    
    public partial class planeAngleMeasure : abstractMeasure
    {

        /// <remarks/>
        [XmlAttribute()]
        public planeAngleUom uom;
    }

    /// <remarks/>
    [System.CodeDom.Compiler.GeneratedCodeAttribute("xsd", "2.0.50727.42")]
    
    
    
    
    public partial class perLengthMeasure : abstractMeasure
    {

        /// <remarks/>
        [XmlAttribute()]
        public perLengthUom uom;
    }



    /// <remarks/>
    [System.CodeDom.Compiler.GeneratedCodeAttribute("xsd", "2.0.50727.42")]
    
    
    
    
    public partial class momentOfForceMeasure : abstractMeasure
    {

        /// <remarks/>
        [XmlAttribute()]
        public momentOfForceUom uom;
    }



    /// <remarks/>
    [System.CodeDom.Compiler.GeneratedCodeAttribute("xsd", "2.0.50727.42")]
    
    
    
    
    public partial class massPerLengthMeasure : abstractMeasure
    {

        /// <remarks/>
        [XmlAttribute()]
        public massPerLengthUom uom;
    }



    /// <remarks/>
    [System.CodeDom.Compiler.GeneratedCodeAttribute("xsd", "2.0.50727.42")]
    
    
    
    
    public partial class massMeasure : abstractMeasure
    {

        /// <remarks/>
        [XmlAttribute()]
        public massUom uom;
    }



    /// <remarks/>
    [System.CodeDom.Compiler.GeneratedCodeAttribute("xsd", "2.0.50727.42")]
    
    
    
    
    public partial class massConcentrationMeasure : abstractMeasure
    {

        /// <remarks/>
        [XmlAttribute()]
        public massConcentrationUom uom;
    }



    /// <remarks/>
    [System.CodeDom.Compiler.GeneratedCodeAttribute("xsd", "2.0.50727.42")]
    
    
    
    
    public partial class magneticInductionMeasure : abstractMeasure
    {

        /// <remarks/>
        [XmlAttribute()]
        public magneticInductionUom uom;
    }



    /// <remarks/>
    [System.CodeDom.Compiler.GeneratedCodeAttribute("xsd", "2.0.50727.42")]
    
    
    
    
    public partial class magneticFieldStrengthMeasure : abstractMeasure
    {

        /// <remarks/>
        [XmlAttribute()]
        public magneticFieldStrengthUom uom;
    }



    /// <remarks/>
    [System.CodeDom.Compiler.GeneratedCodeAttribute("xsd", "2.0.50727.42")]
    
    
    
    
    public partial class lengthPerLengthMeasure : abstractMeasure
    {

        /// <remarks/>
        [XmlAttribute()]
        public lengthPerLengthUom uom;
    }



    /// <remarks/>
    [System.CodeDom.Compiler.GeneratedCodeAttribute("xsd", "2.0.50727.42")]
    
    
    
    
    public partial class lengthMeasure : abstractMeasure
    {

        /// <remarks/>
        [XmlAttribute()]
        public lengthUom uom;
    }



    /// <remarks/>
    [System.CodeDom.Compiler.GeneratedCodeAttribute("xsd", "2.0.50727.42")]
    
    
    
    
    public partial class illuminanceMeasure : abstractMeasure
    {

        /// <remarks/>
        [XmlAttribute()]
        public illuminanceUom uom;
    }



    /// <remarks/>
    [System.CodeDom.Compiler.GeneratedCodeAttribute("xsd", "2.0.50727.42")]
    
    
    
    
    public partial class frequencyMeasure : abstractMeasure
    {

        /// <remarks/>
        [XmlAttribute()]
        public frequencyUom uom;
    }



    /// <remarks/>
    [System.CodeDom.Compiler.GeneratedCodeAttribute("xsd", "2.0.50727.42")]
    
    
    
    
    public partial class forcePerVolumeMeasure : abstractMeasure
    {

        /// <remarks/>
        [XmlAttribute()]
        public forcePerVolumeUom uom;
    }



    /// <remarks/>
    [System.CodeDom.Compiler.GeneratedCodeAttribute("xsd", "2.0.50727.42")]
    
    
    
    
    public partial class forcePerLengthMeasure : abstractMeasure
    {

        /// <remarks/>
        [XmlAttribute()]
        public forcePerLengthUom uom;
    }



    /// <remarks/>
    [System.CodeDom.Compiler.GeneratedCodeAttribute("xsd", "2.0.50727.42")]
    
    
    
    
    public partial class forceMeasure : abstractMeasure
    {

        /// <remarks/>
        [XmlAttribute()]
        public forceUom uom;
    }



    /// <remarks/>
    [System.CodeDom.Compiler.GeneratedCodeAttribute("xsd", "2.0.50727.42")]
    
    
    
    
    public partial class equivalentPerMassMeasure : abstractMeasure
    {

        /// <remarks/>
        [XmlAttribute()]
        public equivalentPerMassUom uom;
    }



    /// <remarks/>
    [System.CodeDom.Compiler.GeneratedCodeAttribute("xsd", "2.0.50727.42")]
    
    
    
    
    public partial class energyPerAreaMeasure : abstractMeasure
    {

        /// <remarks/>
        [XmlAttribute()]
        public energyPerAreaUom uom;
    }







    /// <remarks/>
    [System.CodeDom.Compiler.GeneratedCodeAttribute("xsd", "2.0.50727.42")]
    
    
    
    
    public partial class electricCurrentMeasure : abstractMeasure
    {

        /// <remarks/>
        [XmlAttribute()]
        public electricCurrentUom uom;
    }



    /// <remarks/>
    [System.CodeDom.Compiler.GeneratedCodeAttribute("xsd", "2.0.50727.42")]
    
    
    
    
    public partial class dynamicViscosityMeasure : abstractMeasure
    {

        /// <remarks/>
        [XmlAttribute()]
        public dynamicViscosityUom uom;
    }



    /// <remarks/>
    [System.CodeDom.Compiler.GeneratedCodeAttribute("xsd", "2.0.50727.42")]
    
    
    
    
    public partial class dimensionlessMeasure : abstractMeasure
    {

        /// <remarks/>
        [XmlAttribute()]
        public dimensionlessUom uom;
    }



    /// <remarks/>
    [System.CodeDom.Compiler.GeneratedCodeAttribute("xsd", "2.0.50727.42")]
    
    
    
    
    public partial class densityMeasure : abstractMeasure
    {

        /// <remarks/>
        [XmlAttribute()]
        public densityUom uom;
    }



    /// <remarks/>
    [System.CodeDom.Compiler.GeneratedCodeAttribute("xsd", "2.0.50727.42")]
    
    
    
    
    public partial class areaPerAreaMeasure : abstractMeasure
    {

        /// <remarks/>
        [XmlAttribute()]
        public areaPerAreaUom uom;
    }



    /// <remarks/>
    [System.CodeDom.Compiler.GeneratedCodeAttribute("xsd", "2.0.50727.42")]
    
    
    
    
    public partial class areaMeasure : abstractMeasure
    {

        /// <remarks/>
        [XmlAttribute()]
        public areaUom uom;
    }



    /// <remarks/>
    [System.CodeDom.Compiler.GeneratedCodeAttribute("xsd", "2.0.50727.42")]
    
    
    
    
    public partial class anglePerTimeMeasure : abstractMeasure
    {

        /// <remarks/>
        [XmlAttribute()]
        public anglePerTimeUom uom;
    }



    /// <remarks/>
    [System.CodeDom.Compiler.GeneratedCodeAttribute("xsd", "2.0.50727.42")]
    
    
    
    
    public partial class anglePerLengthMeasure : abstractMeasure
    {

        /// <remarks/>
        [XmlAttribute()]
        public anglePerLengthUom uom;
    }



    /// <remarks/>
    [System.CodeDom.Compiler.GeneratedCodeAttribute("xsd", "2.0.50727.42")]
    
    
    
    
    public partial class accelerationLinearMeasure : abstractMeasure
    {

        /// <remarks/>
        [XmlAttribute()]
        public accelerationLinearUom uom;
    }

    /// <remarks/>
    [System.CodeDom.Compiler.GeneratedCodeAttribute("xsd", "2.0.50727.42")]
    
    
    
    
    public partial class electricPotentialMeasure : abstractMeasure
    {

        /// <remarks/>
        [XmlAttribute()]
        public electricPotentialUom uom;
    }
}
