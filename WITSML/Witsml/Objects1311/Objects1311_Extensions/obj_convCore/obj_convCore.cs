﻿ 

using System;
using System.IO;
using System.Text;
using System.Xml;
using Witsml.Common.Exceptions;
using Witsml.Exceptions;


namespace Witsml.Objects1311
{
    [System.Xml.Serialization.XmlRootAttribute("convCore", Namespace = "http://www.witsml.org/schemas/131", IsNullable = false)]
    public partial class obj_convCore : WITSMLObject, IHandleXML
    {
        public obj_convCore()
        {
        }

        /// <summary>
        /// Decode XML string into obj_convCore
        /// </summary>
        /// <param name="xmlIn">XML string</param>
        public override void Decode(ref string xmlIn)
        {
            try
            {
                //Consume XML string and convert node value to variable based on node name
                using (StringReader stringReader = new StringReader(xmlIn))
                {
                    XmlReaderSettings setting = new XmlReaderSettings();
                    setting.ConformanceLevel = ConformanceLevel.Document;
                    setting.IgnoreComments = true;
                    setting.IgnoreWhitespace = true;
                    setting.IgnoreProcessingInstructions = true;
                    using (XmlReader xmlReader = XmlReader.Create(stringReader, setting))
                    {
                        //determine add or update operation
                        bool isNew;
                        if (this.uid == string.Empty)
                            isNew = true;
                        else isNew = false;

                        this.HandleXML(xmlReader, isNew);
                    }
                }
            }
            catch (XmlException ex)
            {
                throw new Exceptions.UnableToDecodeException(ex.Message, WITSMLReturnCode.WITSMLDocumentError, ex.InnerException);
            }
            catch (NullReferenceException ex)
            {
                throw new Exceptions.UnableToDecodeException(ex.Message, WITSMLReturnCode.WITSMLEmptyDocument, ex.InnerException);
            }
            catch (Exceptions.UnableToDecodeException ex)
            {
                throw ex;
            }
            catch (Exception ex)
            {
                throw new Exceptions.UnableToDecodeException(ex.Message, WITSMLReturnCode.WITSMLUnkownCause, ex.InnerException);
            }
        }

        /// <summary>
        /// Convert XML string data into obj_convCore fields value
        /// </summary>
        /// <param name="xmlReader">reader that has obj_convCore node</param>
        /// <param name="IsNew">Is the object from stratch(true) or from db/serialization(false)</param>
        public void HandleXML(XmlReader xmlReader, bool isNew)
        {
            using (xmlReader)
            {
                //assign uid for the new risk
                xmlReader.MoveToContent();
                if (xmlReader.HasAttributes)
                {
                    if (xmlReader.MoveToAttribute("uidWell"))
                        StaticHelper.HandleUID(xmlReader.Value, "convCore uidWell", out this.uidWell);
                    if (xmlReader.MoveToAttribute("uidWellbore"))
                        StaticHelper.HandleUID(xmlReader.Value, "convCore uidWellbore", out this.uidWellbore);
                    if (xmlReader.MoveToAttribute("uid"))
                        StaticHelper.HandleUID(xmlReader.Value, "convCore uid", out this.uid);
                    xmlReader.MoveToElement();
                }
                while (xmlReader.Read())
                {
                    //only take child node
                    if (xmlReader.Depth == 1 && xmlReader.IsStartElement() && !xmlReader.IsEmptyElement)
                    {
                        switch (xmlReader.Name)
                        {
                            case "nameWell":
                                this.nameWell = StaticHelper.ReadString(xmlReader);
                                break;
                            case "nameWellbore":
                                this.nameWellbore = StaticHelper.ReadString(xmlReader);
                                break;
                            case "name":
                                this.name = StaticHelper.ReadString(xmlReader);
                                break;
                            case "mdCoreTop":
                                if (this.mdCoreTop == null)
                                    this.mdCoreTop = new measuredDepthCoord();
                                this.mdCoreTop.HandleXML(xmlReader.ReadSubtree(), isNew);
                                break;
                            case "mdCoreBottom":
                                if (this.mdCoreBottom == null)
                                    this.mdCoreBottom = new measuredDepthCoord();
                                this.mdCoreBottom.HandleXML(xmlReader.ReadSubtree(), isNew);
                                break;
                            case "dTimCoreStart":
                                this.dTimCoreStart = StaticParser.ParseDateTimeFromString(StaticHelper.ReadString(xmlReader));
                                this.dTimCoreStartSpecified = true;
                                break;
                            case "dTimCoreEnd":
                                this.dTimCoreEnd = StaticParser.ParseDateTimeFromString(StaticHelper.ReadString(xmlReader));
                                this.dTimCoreEndSpecified = true;
                                break;
                            case "coreReference":
                                this.coreReference = StaticHelper.ReadString(xmlReader);
                                break;
                            case "coringContractor":
                                this.coringContractor = StaticHelper.ReadString(xmlReader);
                                break;
                            case "analysisContractor":
                                this.analysisContractor = StaticHelper.ReadString(xmlReader);
                                break;
                            case "coreBarrel":
                                this.coreBarrel = StaticHelper.ReadString(xmlReader);
                                break;
                            case "innerBarrelUsed":
                                this.innerBarrelUsed = StaticParser.ParseBooleanFromString(StaticHelper.ReadString(xmlReader));
                                this.innerBarrelUsedSpecified = true;
                                break;
                            case "innerBarrelType":
                                this.innerBarrelType = StaticHelper.ReadString(xmlReader);
                                break;
                            case "lenBarrel":
                                if (this.lenBarrel == null)
                                    this.lenBarrel = new lengthMeasure();
                                this.lenBarrel.HandleXML(xmlReader.ReadSubtree(), isNew);
                                break;
                            case "coreBitType":
                                this.coreBitType = StaticHelper.ReadString(xmlReader);
                                break;
                            case "diaBit":
                                if (this.diaBit == null)
                                    this.diaBit = new lengthMeasure();
                                this.diaBit.HandleXML(xmlReader.ReadSubtree(), isNew);
                                break;
                            case "diaCore":
                                if (this.diaCore == null)
                                    this.diaCore = new lengthMeasure();
                                this.diaCore.HandleXML(xmlReader.ReadSubtree(), isNew);
                                break;
                            case "lenCored":
                                if (this.lenCored == null)
                                    this.lenCored = new lengthMeasure();
                                this.lenCored.HandleXML(xmlReader.ReadSubtree(), isNew);
                                break;
                            case "lenRecovered":
                                if (this.lenRecovered == null)
                                    this.lenRecovered = new lengthMeasure();
                                this.lenRecovered.HandleXML(xmlReader.ReadSubtree(), isNew);
                                break;
                            case "recoverPc":
                                if (this.recoverPc == null)
                                    this.recoverPc = new volumePerVolumeMeasure();
                                this.recoverPc.HandleXML(xmlReader.ReadSubtree(), isNew);
                                break;
                            case "inclHole":
                                if (this.inclHole == null)
                                    this.inclHole = new planeAngleMeasure();
                                this.inclHole.HandleXML(xmlReader.ReadSubtree(), isNew);
                                break;
                            case "coreOrientation":
                                this.coreOrientation = StaticParser.ParseBooleanFromString(StaticHelper.ReadString(xmlReader));
                                this.coreOrientationSpecified = true;
                                break;
                            case "coreMethod":
                                this.coreMethod = StaticHelper.ReadString(xmlReader);
                                break;
                            case "coreTreatmentMethod":
                                this.coreTreatmentMethod = StaticHelper.ReadString(xmlReader);
                                break;
                            case "coreFluidUsed":
                                this.coreFluidUsed = StaticHelper.ReadString(xmlReader);
                                break;
                            case "nameFormation":
                                this.nameFormation = StaticHelper.ReadString(xmlReader);
                                break;
                            case "geologyInterval":
                                if (this.geologyInterval == null)
                                    this.geologyInterval = new cs_geologyInterval();
                                this.geologyInterval.HandleXML(xmlReader.ReadSubtree(), isNew);
                                break;
                            case "coreDescription":
                                this.coreDescription = StaticHelper.ReadString(xmlReader);
                                break;
                            case "commonData":
                                if (this.commonData == null)
                                    this.commonData = new cs_commonData();
                                this.commonData.HandleXML(xmlReader.ReadSubtree(), isNew);
                                break;
                            case "customData":
                                if (this.customData == null)
                                    this.customData = new cs_customData();
                                this.customData.HandleXML(xmlReader.ReadSubtree(), isNew);
                                break;
                            default:
                                break;
                        }
                    }
                }
            }
        }

        public override string ValidateGetPath(string newUid)
        {
            // Fix uid if the Object dosn't have one.
            if (!IsValidUid(this.uid))
            {
                this.uid = newUid;
                // TODO: Add some loging that we created a new UID for the object maybe.
            }

            StringBuilder path = new StringBuilder(this.GetParentPath());
            path.Append(@"\");
            path.Append(this.uid);
            return path.ToString();
        }

        public override string GetParentPath()
        {
            if (!IsValidUid(this.uidWell) || !IsValidUid(this.uidWellbore))
            {
                throw new UnableToDecodeException("One or more Parent UID's are invalid", WITSMLReturnCode.WITSMLInvalidParentUID);
                //TODO: Log the issue
            }

            StringBuilder path = new StringBuilder(@"\");
            path.Append(this.uidWell);
            path.Append(@"\");
            path.Append(this.uidWellbore);
            return path.ToString();
        }

        // NOTE: these are done with functions, instead of property access, so as not to mess around with the auto-generated / serialized classes.
        public override string Get_uid()
        {
            return this.uid;
        }

        public override string Get_uidWell()
        {
            return this.uidWell;
        }

        public override string Get_uidWellbore()
        {
            return this.uidWellbore;
        }

        public override string Get_name()
        {
            return this.name;
        }

        public override string Get_nameWell()
        {
            return this.nameWell;
        }

        public override string Get_nameWellbore()
        {
            return this.nameWellbore;
        }

        public override ICommonData Get_commonData()
        {
            if (this.commonData == null)
            {
                this.commonData = new cs_commonData();
            }
            return (ICommonData)this.commonData;
        }
        public override ICustomData Get_customData()
        {
            if (this.customData == null)
            {
                this.customData = new cs_customData();
            }
            return (ICustomData)this.customData;
        }
    }
}
