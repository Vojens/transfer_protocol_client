 

using System;
using System.Collections.Generic;
using System.Text;
using System.Xml;

namespace Witsml.Objects1311
{
    public partial class cs_drillingParams : IHandleXML, IHasID
    {
        //this will need to be ignored
        [System.Xml.Serialization.XmlIgnore]
        public string UID
        {
            get { return this.uid; }
        }
        public void HandleXML(XmlReader xmlReader, bool isNew)
        {
            using (xmlReader)
            {
                xmlReader.MoveToContent();
                if (xmlReader.HasAttributes)
                {
                    if (xmlReader.MoveToAttribute("uid"))
                        StaticHelper.HandleUID(xmlReader.Value, "drillingParams uid", out this.uid);
                    xmlReader.MoveToElement();
                }
                while (xmlReader.Read())
                {
                    //only take child node
                    if (xmlReader.Depth == 1 && xmlReader.IsStartElement() && !xmlReader.IsEmptyElement)
                    {
                        switch (xmlReader.Name)
                        {
                            case "eTimOpBit":
                                if (this.eTimOpBit == null)
                                    this.eTimOpBit = new timeMeasure();
                                this.eTimOpBit.HandleXML(xmlReader.ReadSubtree(), isNew);
                                break;
                            case "mdHoleStart":
                                if (this.mdHoleStart == null)
                                    this.mdHoleStart = new measuredDepthCoord();
                                this.mdHoleStart.HandleXML(xmlReader.ReadSubtree(), isNew);
                                break;
                            case "mdHoleStop":
                                if (this.mdHoleStop == null)
                                    this.mdHoleStop = new measuredDepthCoord();
                                this.mdHoleStop.HandleXML(xmlReader.ReadSubtree(), isNew);
                                break;
                            case "tubular":
                                if (this.tubular == null)
                                    this.tubular = new refNameString();
                                this.tubular.ObjectName = "tubular";
                                this.tubular.HandleXML(xmlReader.ReadSubtree(), isNew);
                                break;
                            case "hkldRot":
                                if (this.hkldRot == null)
                                    this.hkldRot = new forceMeasure();
                                this.hkldRot.HandleXML(xmlReader.ReadSubtree(), isNew);
                                break;
                            case "overPull":
                                if (this.overPull == null)
                                    this.overPull = new forceMeasure();
                                this.overPull.HandleXML(xmlReader.ReadSubtree(), isNew);
                                break;
                            case "slackOff":
                                if (this.slackOff == null)
                                    this.slackOff = new forceMeasure();
                                this.slackOff.HandleXML(xmlReader.ReadSubtree(), isNew);
                                break;
                            case "hkldUp":
                                if (this.hkldUp == null)
                                    this.hkldUp = new forceMeasure();
                                this.hkldUp.HandleXML(xmlReader.ReadSubtree(), isNew);
                                break;
                            case "hkldDn":
                                if (this.hkldDn == null)
                                    this.hkldDn = new forceMeasure();
                                this.hkldDn.HandleXML(xmlReader.ReadSubtree(), isNew);
                                break;
                            case "tqOnBotAv":
                                if (this.tqOnBotAv == null)
                                    this.tqOnBotAv = new momentOfForceMeasure();
                                this.tqOnBotAv.HandleXML(xmlReader.ReadSubtree(), isNew);
                                break;
                            case "tqOnBotMx":
                                if (this.tqOnBotMx == null)
                                    this.tqOnBotMx = new momentOfForceMeasure();
                                this.tqOnBotMx.HandleXML(xmlReader.ReadSubtree(), isNew);
                                break;
                            case "tqOnBotMn":
                                if (this.tqOnBotMn == null)
                                    this.tqOnBotMn = new momentOfForceMeasure();
                                this.tqOnBotMn.HandleXML(xmlReader.ReadSubtree(), isNew);
                                break;
                            case "tqOffBotAv":
                                if (this.tqOffBotAv == null)
                                    this.tqOffBotAv = new momentOfForceMeasure();
                                this.tqOffBotAv.HandleXML(xmlReader.ReadSubtree(), isNew);
                                break;
                            case "tqDhAv":
                                if (this.tqDhAv == null)
                                    this.tqDhAv = new momentOfForceMeasure();
                                this.tqDhAv.HandleXML(xmlReader.ReadSubtree(), isNew);
                                break;
                            case "wtAboveJar":
                                if (this.wtAboveJar == null)
                                    this.wtAboveJar = new forceMeasure();
                                this.wtAboveJar.HandleXML(xmlReader.ReadSubtree(), isNew);
                                break;
                            case "wtBelowJar":
                                if (this.wtBelowJar == null)
                                    this.wtBelowJar = new forceMeasure();
                                this.wtBelowJar.HandleXML(xmlReader.ReadSubtree(), isNew);
                                break;
                            case "wtMud":
                                if (this.wtMud == null)
                                    this.wtMud = new densityMeasure();
                                this.wtMud.HandleXML(xmlReader.ReadSubtree(), isNew);
                                break;
                            case "flowratePump":
                                if (this.flowratePump == null)
                                    this.flowratePump = new volumeFlowRateMeasure();
                                this.flowratePump.HandleXML(xmlReader.ReadSubtree(), isNew);
                                break;
                            case "powBit":
                                if (this.powBit == null)
                                    this.powBit = new powerMeasure();
                                this.powBit.HandleXML(xmlReader.ReadSubtree(), isNew);
                                break;
                            case "velNozzleAv":
                                if (this.velNozzleAv == null)
                                    this.velNozzleAv = new velocityMeasure();
                                this.velNozzleAv.HandleXML(xmlReader.ReadSubtree(), isNew);
                                break;
                            case "presDropBit":
                                if (this.presDropBit == null)
                                    this.presDropBit = new pressureMeasure();
                                this.presDropBit.HandleXML(xmlReader.ReadSubtree(), isNew);
                                break;
                            case "cTimHold":
                                if (this.cTimHold == null)
                                    this.cTimHold = new timeMeasure();
                                this.cTimHold.HandleXML(xmlReader.ReadSubtree(), isNew);
                                break;
                            case "cTimSteering":
                                if (this.cTimSteering == null)
                                    this.cTimSteering = new timeMeasure();
                                this.cTimSteering.HandleXML(xmlReader.ReadSubtree(), isNew);
                                break;
                            case "cTimDrillRot":
                                if (this.cTimDrillRot == null)
                                    this.cTimDrillRot = new timeMeasure();
                                this.cTimDrillRot.HandleXML(xmlReader.ReadSubtree(), isNew);
                                break;
                            case "cTimDrillSlid":
                                if (this.cTimDrillSlid == null)
                                    this.cTimDrillSlid = new timeMeasure();
                                this.cTimDrillSlid.HandleXML(xmlReader.ReadSubtree(), isNew);
                                break;
                            case "cTimCirc":
                                if (this.cTimCirc == null)
                                    this.cTimCirc = new timeMeasure();
                                this.cTimCirc.HandleXML(xmlReader.ReadSubtree(), isNew);
                                break;
                            case "cTimReam":
                                if (this.cTimReam == null)
                                    this.cTimReam = new timeMeasure();
                                this.cTimReam.HandleXML(xmlReader.ReadSubtree(), isNew);
                                break;
                            case "distDrillRot":
                                if (this.distDrillRot == null)
                                    this.distDrillRot = new lengthMeasure();
                                this.distDrillRot.HandleXML(xmlReader.ReadSubtree(), isNew);
                                break;
                            case "distDrillSlid":
                                if (this.distDrillSlid == null)
                                    this.distDrillSlid = new lengthMeasure();
                                this.distDrillSlid.HandleXML(xmlReader.ReadSubtree(), isNew);
                                break;
                            case "distReam":
                                if (this.distReam == null)
                                    this.distReam = new lengthMeasure();
                                this.distReam.HandleXML(xmlReader.ReadSubtree(), isNew);
                                break;
                            case "distHold":
                                if (this.distHold == null)
                                    this.distHold = new lengthMeasure();
                                this.distHold.HandleXML(xmlReader.ReadSubtree(), isNew);
                                break;
                            case "distSteering":
                                if (this.distSteering == null)
                                    this.distSteering = new lengthMeasure();
                                this.distSteering.HandleXML(xmlReader.ReadSubtree(), isNew);
                                break;
                            case "rpmAv":
                                if (this.rpmAv == null)
                                    this.rpmAv = new anglePerTimeMeasure();
                                this.rpmAv.HandleXML(xmlReader.ReadSubtree(), isNew);
                                break;
                            case "rpmMx":
                                if (this.rpmMx == null)
                                    this.rpmMx = new anglePerTimeMeasure();
                                this.rpmMx.HandleXML(xmlReader.ReadSubtree(), isNew);
                                break;
                            case "rpmMn":
                                if (this.rpmMn == null)
                                    this.rpmMn = new anglePerTimeMeasure();
                                this.rpmMn.HandleXML(xmlReader.ReadSubtree(), isNew);
                                break;
                            case "rpmAvDh":
                                if (this.rpmAvDh == null)
                                    this.rpmAvDh = new anglePerTimeMeasure();
                                this.rpmAvDh.HandleXML(xmlReader.ReadSubtree(), isNew);
                                break;
                            case "ropAv":
                                if (this.ropAv == null)
                                    this.ropAv = new velocityMeasure();
                                this.ropAv.HandleXML(xmlReader.ReadSubtree(), isNew);
                                break;
                            case "ropMx":
                                if (this.ropMx == null)
                                    this.ropMx = new velocityMeasure();
                                this.ropMx.HandleXML(xmlReader.ReadSubtree(), isNew);
                                break;
                            case "ropMn":
                                if (this.ropMn == null)
                                    this.ropMn = new velocityMeasure();
                                this.ropMn.HandleXML(xmlReader.ReadSubtree(), isNew);
                                break;
                            case "wobAv":
                                if (this.wobAv == null)
                                    this.wobAv = new forceMeasure();
                                this.wobAv.HandleXML(xmlReader.ReadSubtree(), isNew);
                                break;
                            case "wobMx":
                                if (this.wobMx == null)
                                    this.wobMx = new forceMeasure();
                                this.wobMx.HandleXML(xmlReader.ReadSubtree(), isNew);
                                break;
                            case "wobMn":
                                if (this.wobMn == null)
                                    this.wobMn = new forceMeasure();
                                this.wobMn.HandleXML(xmlReader.ReadSubtree(), isNew);
                                break;
                            case "wobAvDh":
                                if (this.wobAvDh == null)
                                    this.wobAvDh = new forceMeasure();
                                this.wobAvDh.HandleXML(xmlReader.ReadSubtree(), isNew);
                                break;
                            case "reasonTrip":
                                this.reasonTrip = StaticHelper.ReadString(xmlReader);
                                break;
                            case "objectiveBha":
                                this.objectiveBha = StaticHelper.ReadString(xmlReader);
                                break;
                            case "aziTop":
                                if (this.aziTop == null)
                                    this.aziTop = new planeAngleMeasure();
                                this.aziTop.HandleXML(xmlReader.ReadSubtree(), isNew);
                                break;
                            case "aziBottom":
                                if (this.aziBottom == null)
                                    this.aziBottom = new planeAngleMeasure();
                                this.aziBottom.HandleXML(xmlReader.ReadSubtree(), isNew);
                                break;
                            case "inclStart":
                                if (this.inclStart == null)
                                    this.inclStart = new planeAngleMeasure();
                                this.inclStart.HandleXML(xmlReader.ReadSubtree(), isNew);
                                break;
                            case "inclMx":
                                if (this.inclMx == null)
                                    this.inclMx = new planeAngleMeasure();
                                this.inclMx.HandleXML(xmlReader.ReadSubtree(), isNew);
                                break;
                            case "inclMn":
                                if (this.inclMn == null)
                                    this.inclMn = new planeAngleMeasure();
                                this.inclMn.HandleXML(xmlReader.ReadSubtree(), isNew);
                                break;
                            case "inclStop":
                                if (this.inclStop == null)
                                    this.inclStop = new planeAngleMeasure();
                                this.inclStop.HandleXML(xmlReader.ReadSubtree(), isNew);
                                break;
                            case "tempMudDhMx":
                                if (this.tempMudDhMx == null)
                                    this.tempMudDhMx = new thermodynamicTemperatureMeasure();
                                this.tempMudDhMx.HandleXML(xmlReader.ReadSubtree(), isNew);
                                break;
                            case "presPumpAv":
                                if (this.presPumpAv == null)
                                    this.presPumpAv = new pressureMeasure();
                                this.presPumpAv.HandleXML(xmlReader.ReadSubtree(), isNew);
                                break;
                            case "flowrateBit":
                                if (this.flowrateBit == null)
                                    this.flowrateBit = new volumeFlowRateMeasure();
                                this.flowrateBit.HandleXML(xmlReader.ReadSubtree(), isNew);
                                break;
                            case "comments":
                                this.comments = StaticHelper.ReadString(xmlReader);
                                break;
                            default:
                                break;
                        }
                    }
                }

            }
        }
    }
}
