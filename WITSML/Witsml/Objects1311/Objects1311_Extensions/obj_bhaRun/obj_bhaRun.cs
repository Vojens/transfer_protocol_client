 

using System;
using System.IO;
using System.Text;
using System.Xml;
using Witsml.Common.Exceptions;
using Witsml.Exceptions;

namespace Witsml.Objects1311
{
    [System.Xml.Serialization.XmlRootAttribute("bhaRun", Namespace = "http://www.witsml.org/schemas/131", IsNullable = false)]
    public partial class obj_bhaRun : WITSMLObject, IHandleXML
    {
        public obj_bhaRun()
        {
            this.statusBha = BhaStatus.unknown;
        }

        /// <summary>
        /// Decode XML string into rig
        /// </summary>
        /// <param name="xmlIn">XML string</param>
        public override void Decode(ref string xmlIn)
        {
            try
            {
                //Consume XML string and convert node value to variable based on node name
                using (StringReader stringReader = new StringReader(xmlIn))
                {
                    XmlReaderSettings setting = new XmlReaderSettings();
                    setting.ConformanceLevel = ConformanceLevel.Document;
                    setting.IgnoreComments = true;
                    setting.IgnoreWhitespace = true;
                    setting.IgnoreProcessingInstructions = true;
                    using (XmlReader xmlReader = XmlReader.Create(stringReader, setting))
                    {
                        //determine add or update operation
                        bool isNew;
                        if (this.uid == string.Empty)
                            isNew = true;
                        else isNew = false;

                        this.HandleXML(xmlReader, isNew);
                    }
                }
            }
            catch (XmlException ex)
            {
                throw new UnableToDecodeException(ex.Message, WITSMLReturnCode.WITSMLDocumentError
                    , ex.InnerException);
            }
            catch (NullReferenceException ex)
            {
                throw new UnableToDecodeException(ex.Message, WITSMLReturnCode.WITSMLEmptyDocument, ex.InnerException);
            }
            catch (Exceptions.UnableToDecodeException ex)
            {
                throw ex;
            }
            catch (Exception ex)
            {
                throw new Exceptions.UnableToDecodeException(ex.Message, WITSMLReturnCode.WITSMLUnkownCause, ex.InnerException);
            }
        }

        /// <summary>
        /// Convert XML string data into bhaRun fields value
        /// </summary>
        /// <param name="xmlReader">reader that has bhaRun node</param>
        /// <param name="IsNew">Is the object from stratch(true) or from db/serialization(false)</param>
        public void HandleXML(XmlReader xmlReader, bool isNew)
        {
            using (xmlReader)
            {
                //assign uid for the new well
                xmlReader.MoveToContent();
                if (xmlReader.HasAttributes)
                {
                    if (xmlReader.MoveToAttribute("uidWell"))
                        StaticHelper.HandleUID(xmlReader.Value, "bhaRun uidWell", out this.uidWell);
                    if (xmlReader.MoveToAttribute("uidWellbore"))
                        StaticHelper.HandleUID(xmlReader.Value, "bhaRun uidWellbore", out this.uidWellbore);
                    if (xmlReader.MoveToAttribute("uid"))
                        StaticHelper.HandleUID(xmlReader.Value, "bhaRun uid", out this.uid);
                    xmlReader.MoveToElement();
                }
                while (xmlReader.Read())
                {
                    //only take child node
                    if (xmlReader.Depth == 1 && xmlReader.IsStartElement() && !xmlReader.IsEmptyElement)
                    {
                        switch (xmlReader.Name)
                        {
                            case "nameWell":
                                this.nameWell = StaticHelper.ReadString(xmlReader);
                                break;
                            case "nameWellbore":
                                this.nameWellbore = StaticHelper.ReadString(xmlReader);
                                break;
                            case "name":
                                this.name = StaticHelper.ReadString(xmlReader);
                                break;
                            case "tubular":
                                if (this.tubular == null)
                                    this.tubular = new refNameString();
                                this.tubular.ObjectName = "tubular";
                                this.tubular.HandleXML(xmlReader.ReadSubtree(), isNew);
                                break;
                            case "dTimStart":
                                this.dTimStart = StaticParser.ParseDateTimeFromString(StaticHelper.ReadString(xmlReader));
                                this.dTimStartSpecified = true;
                                break;
                            case "dTimStop":
                                this.dTimStop = StaticParser.ParseDateTimeFromString(StaticHelper.ReadString(xmlReader));
                                this.dTimStopSpecified = true;
                                break;
                            case "dTimStartDrilling":
                                this.dTimStartDrilling = StaticParser.ParseDateTimeFromString(StaticHelper.ReadString(xmlReader));
                                this.dTimStartDrillingSpecified = true;
                                break;
                            case "dTimStopDrilling":
                                this.dTimStopDrilling = StaticParser.ParseDateTimeFromString(StaticHelper.ReadString(xmlReader));
                                this.dTimStopDrillingSpecified = true;
                                break;
                            case "planDogleg":
                                if (this.planDogleg == null)
                                    this.planDogleg = new anglePerLengthMeasure();
                                this.planDogleg.HandleXML(xmlReader.ReadSubtree(), isNew);
                                break;
                            case "actDogleg":
                                if (this.actDogleg == null)
                                    this.actDogleg = new anglePerLengthMeasure();
                                this.actDogleg.HandleXML(xmlReader.ReadSubtree(), isNew);
                                break;
                            case "actDoglegMx":
                                if (this.actDoglegMx == null)
                                    this.actDoglegMx = new anglePerLengthMeasure();
                                this.actDoglegMx.HandleXML(xmlReader.ReadSubtree(), isNew);
                                break;
                            case "statusBha":
                                StaticParser.SetEnumFromString<BhaStatus>(StaticHelper.ReadString(xmlReader), out this.statusBha, out this.statusBhaSpecified);
                                break;
                            case "numBitRun":
                                this.numBitRun = StaticHelper.ReadString(xmlReader);
                                break;
                            case "numStringRun":
                                this.numStringRun = StaticParser.ParseShortFromString(StaticHelper.ReadString(xmlReader));
                                this.numStringRunSpecified = true;
                                break;
                            case "reasonTrip":
                                this.reasonTrip = StaticHelper.ReadString(xmlReader);
                                break;
                            case "objectiveBha":
                                this.objectiveBha = StaticHelper.ReadString(xmlReader);
                                break;
                            case "drillingParams":
                                //object
                                if (this.drillingParams == null)
                                    this.drillingParams = new cs_drillingParams();
                                this.drillingParams.HandleXML(xmlReader.ReadSubtree(), isNew);
                                break;
                            case "commonData":
                                if (this.commonData == null)
                                    this.commonData = new cs_commonData();
                                this.commonData.HandleXML(xmlReader.ReadSubtree(), isNew);
                                break;
                            case "customData":
                                if (this.customData == null)
                                    this.customData = new cs_customData();
                                this.customData.HandleXML(xmlReader.ReadSubtree(), isNew);
                                break;
                            default:
                                break;
                        }
                    }
                }
            }
        }

        public override string ValidateGetPath(string newUid)
        {
            // Fix uid if the Object dosn't have one.
            if (!IsValidUid(this.uid))
            {
                this.uid = newUid;
                // TODO: Add some loging that we created a new UID for the object maybe.
            }

            StringBuilder path = new StringBuilder(this.GetParentPath());
            path.Append(@"\");
            path.Append(this.uid);
            return path.ToString();
        }

        public override string GetParentPath()
        {
            if (!IsValidUid(this.uidWell) || !IsValidUid(this.uidWellbore))
            {
                throw new UnableToDecodeException("One or more Parent UID's are invalid", WITSMLReturnCode.WITSMLInvalidParentUID);
                //TODO: Log the issue
            }

            StringBuilder path = new StringBuilder(@"\");
            path.Append(this.uidWell);
            path.Append(@"\");
            path.Append(this.uidWellbore);
            return path.ToString();
        }

        // NOTE: these are done with functions, instead of property access, so as not to mess around with the auto-generated / serialized classes.
        public override string Get_uid()
        {
            return this.uid;
        }

        public override string Get_uidWell()
        {
            return this.uidWell;
        }

        public override string Get_uidWellbore()
        {
            return this.uidWellbore;
        }

        public override string Get_name()
        {
            return this.name;
        }

        public override string Get_nameWell()
        {
            return this.nameWell;
        }

        public override string Get_nameWellbore()
        {
            return this.nameWellbore;
        }

        public override ICommonData Get_commonData()
        {
            if (this.commonData == null)
            {
                this.commonData = new cs_commonData();
            }
            return (ICommonData)this.commonData;
        }
        public override ICustomData Get_customData()
        {
            if (this.customData == null)
            {
                this.customData = new cs_customData();
            }
            return (ICustomData)this.customData;
        }
    }
}
