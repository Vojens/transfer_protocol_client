 

using System;
using System.Xml;

namespace Witsml.Objects1311
{
    public partial class refNameString : IHandleXML
    {
        private string objectName;
        [System.Xml.Serialization.XmlIgnore]
        public string ObjectName
        {
            get { return this.objectName; }
            set { this.objectName = value; }
        }
        /// <summary>
        /// Convert XML string data into refNameString fields value
        /// </summary>
        /// <param name="xmlReader">reader that has refNameString node</param>
        /// <param name="IsNew">Is the object from stratch(true) or from db/serialization(false)</param>
        public void HandleXML(XmlReader xmlReader, bool isNew)
        {
            using (xmlReader)
            {
                xmlReader.Read();
                if (xmlReader.HasAttributes)
                {
                    if (xmlReader.MoveToAttribute("uidRef"))
                        StaticHelper.HandleUID(xmlReader.Value, this.ObjectName + " uid", out this.uidRef);
                    xmlReader.MoveToElement();
                }
                this.Value = StaticHelper.ReadString(xmlReader);
            }
        }

    }
}
