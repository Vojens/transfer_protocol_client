 

using System;
using System.Collections.Generic;
using System.Xml;

namespace Witsml.Objects1311
{
    public partial class securityInfoType : IHandleXML
    {
        /// <summary>
        /// Convert XML string data into securityInfoType fields value
        /// </summary>
        /// <param name="xmlReader">reader that has securityInfoType node</param>
        /// <param name="IsNew">Is the object from stratch(true) or from db/serialization(false)</param>
        public void HandleXML(XmlReader xmlReader, bool isNew)
        {
            using (xmlReader)
            {
                while (xmlReader.Read())
                {
                    if (xmlReader.Depth == 1 && xmlReader.IsStartElement() && !xmlReader.IsEmptyElement)
                    {
                        switch (xmlReader.Name)
                        {
                            case "Class":
                                this.Class = StaticHelper.ReadString(xmlReader);
                                break;
                            case "System":
                                this.System = StaticHelper.ReadString(xmlReader);
                                break;
                            case "EndDate":
                                this.EndDate = StaticParser.ParseDateTimeFromString(StaticHelper.ReadString(xmlReader));
                                this.EndDateSpecified = true;
                                break;
                            case "Comment":
                                this.Comment = StaticHelper.ReadString(xmlReader);
                                break;
                            default:
                                break;
                        }
                    }
                }
            }
        }
    }
}
