 

using System;
using System.Xml;

namespace Witsml.Objects1311
{
    public partial class nameStruct : IHandleXML
    {
        /// <summary>
        /// Convert XML string data into nameStruct fields value
        /// </summary>
        /// <param name="xmlReader">reader that has nameStruct node</param>
        /// <param name="IsNew">Is the object from stratch(true) or from db/serialization(false)</param>
        public void HandleXML(XmlReader xmlReader, bool isNew)
        {
            using (xmlReader)
            {
                xmlReader.Read();
                if (xmlReader.HasAttributes)
                {
                    if (xmlReader.MoveToAttribute("namingSystem"))
                        this.namingSystem = xmlReader.Value;
                    xmlReader.MoveToElement();
                }
                this.Value = StaticHelper.ReadString(xmlReader);
            }
        }
    }
}
