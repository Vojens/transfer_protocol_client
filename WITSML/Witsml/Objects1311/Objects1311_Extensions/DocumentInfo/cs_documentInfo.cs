 

using System;
using System.Collections.Generic;
using System.Xml;

namespace Witsml.Objects1311
{
    //TODO : Is the collection handling correct
    public partial class cs_documentInfo : IHandleXML
    {
        /// <summary>
        /// Convert XML string data into DocumentInfo fields value
        /// </summary>
        /// <param name="xmlReader">reader that has DocumentInfo node</param>
        /// <param name="IsNew">Is the object from stratch(true) or from db/serialization(false)</param>
        public void HandleXML(XmlReader xmlReader, bool isNew)
        {
            bool isAuditTrailNew = this.AuditTrail == null;

            //create temporary list to handle collection data manipulation
            List<nameStruct> listDocumentAlias = this.DocumentAlias == null ? new List<nameStruct>() : new List<nameStruct>(this.DocumentAlias);
            List<nameStruct> listDocumentClass = this.documentClass == null ? new List<nameStruct>() : new List<nameStruct>(this.documentClass);
            List<securityInfoType> listSecurityInfoType = this.SecurityInformation == null ? new List<securityInfoType>() : new List<securityInfoType>(this.SecurityInformation);
            List<eventType> listAuditTrail = isAuditTrailNew ? new List<eventType>() : new List<eventType>(this.AuditTrail);

            //Read all nodes and assign the value to related fields
            using (xmlReader)
            {
                while (xmlReader.Read())
                {
                    if (xmlReader.Depth == 1 && xmlReader.IsStartElement() && !xmlReader.IsEmptyElement)
                    {
                        switch (xmlReader.Name)
                        {
                            case "DocumentName":
                                //object
                                if (this.DocumentName == null)
                                    this.DocumentName = new nameStruct();
                                this.DocumentName.HandleXML(xmlReader.ReadSubtree(), isNew);
                                break;
                            case "DocumentAlias":
                                //objects
                                nameStruct documentAliasObj = new nameStruct();
                                documentAliasObj.HandleXML(xmlReader.ReadSubtree(), isNew);
                                listDocumentAlias.Add(documentAliasObj);
                                break;
                            case "DocumentDate":
                                this.DocumentDate = StaticParser.ParseDateTimeFromString(StaticHelper.ReadString(xmlReader));
                                this.DocumentDateSpecified = true;
                                break;
                            case "documentClass":
                                //objects
                                nameStruct documentClassObj = new nameStruct();
                                documentClassObj.HandleXML(xmlReader.ReadSubtree(), isNew);
                                listDocumentClass.Add(documentClassObj);
                                break;
                            case "FileCreationInformation":
                                //object
                                if (this.FileCreationInformation == null)
                                    this.FileCreationInformation = new fileCreationType();
                                this.FileCreationInformation.HandleXML(xmlReader.ReadSubtree(), isNew);
                                break;
                            case "SecurityInformation":
                                //objects
                                securityInfoType securityInfoObj = new securityInfoType();
                                securityInfoObj.HandleXML(xmlReader.ReadSubtree(), isNew);
                                listSecurityInfoType.Add(securityInfoObj);
                                break;
                            case "Disclaimer":
                                this.Disclaimer = StaticHelper.ReadString(xmlReader);
                                break;
                            case "AuditTrail":
                                //collection of array item
                                this.HandleAuditTrail(xmlReader.ReadSubtree(), isAuditTrailNew, ref listAuditTrail);
                                break;
                            case "Owner":
                                this.Owner = StaticHelper.ReadString(xmlReader);
                                break;
                            case "Comment":
                                this.Comment = StaticHelper.ReadString(xmlReader);
                                break;
                            default:
                                break;
                        }

                    }
                }
                //Assing list to array member collection
                this.DocumentAlias = listDocumentAlias.ToArray();
                this.documentClass = listDocumentClass.ToArray();
                this.SecurityInformation = listSecurityInfoType.ToArray();
                this.AuditTrail = listAuditTrail.ToArray();
            }
        }

        /// <summary>
        /// Handle array item decode with checking of whether it has exist
        /// </summary>
        /// <typeparam name="T"></typeparam>
        /// <param name="xmlReader">pass the XMLReader.ReadSubTree()</param>
        /// <param name="isNew"></param>
        /// <param name="listEventObj"></param>
        public void HandleAuditTrail(XmlReader xmlReader, bool isNew, ref List<eventType> listAuditTrail)
        {
            using (xmlReader)
            {
                while (xmlReader.Read())
                {
                    if (xmlReader.Depth == 1 && xmlReader.IsStartElement() && !xmlReader.IsEmptyElement)
                    {
                        if (xmlReader.Name == "Event")
                        {
                            eventType eventObj = new eventType();
                            eventObj.HandleXML(xmlReader.ReadSubtree(), isNew);
                            listAuditTrail.Add(eventObj);
                        }
                    }
                }
            }
        }
    }
}
