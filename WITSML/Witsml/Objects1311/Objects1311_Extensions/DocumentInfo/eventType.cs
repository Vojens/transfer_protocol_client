 

using System;
using System.Xml;

namespace Witsml.Objects1311
{
    public partial class eventType : IHandleXML
    {
        /// <summary>
        /// Convert XML string data into eventType fields value
        /// </summary>
        /// <param name="xmlReader">reader that has eventType node</param>
        /// <param name="IsNew">Is the object from stratch(true) or from db/serialization(false)</param>
        public void HandleXML(XmlReader xmlReader, bool isNew)
        {
            using (xmlReader)
            {
                while (xmlReader.Read())
                {
                    if (xmlReader.Depth == 1 && xmlReader.IsStartElement() && !xmlReader.IsEmptyElement)
                    {
                        switch (xmlReader.Name)
                        {
                            case "EventDate":
                                this.EventDate = StaticParser.ParseDateTimeFromString(StaticHelper.ReadString(xmlReader));
                                break;
                            case "ResponsibleParty":
                                this.ResponsibleParty = StaticHelper.ReadString(xmlReader);
                                break;
                            case "Comment":
                                this.Comment = StaticHelper.ReadString(xmlReader);
                                break;
                            default:
                                break;
                        }
                    }
                }
            }
        }
    }
}
