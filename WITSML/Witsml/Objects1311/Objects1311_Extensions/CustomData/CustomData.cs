 

using System;
using System.Collections.Generic;
using System.Text;
using System.Xml;
using System.Xml.Linq;
using Witsml.ObjectStore;

namespace Witsml.Objects1311
{
    public partial class cs_customData : IHandleXML, ICustomData
    {
        ///<!-- Custom Object to store related stuff -->
        public cs_square square;

        //interface member
        [System.Xml.Serialization.XmlIgnore]
        public cs_square square
        {
            get { return this.square; }
            set { this.square = value; }
        }

        /// <summary>
        /// Convert XML string data into CustomData fields value
        /// </summary>
        /// <param name="xmlReader">reader that has CustomData node</param>
        /// <param name="IsNew">Is the object from stratch(true) or from db/serialization(false)</param>
        public void HandleXML(XmlReader xmlReader, bool isNew)
        {
            bool resetCustomData = true;
            //temporary list
            List<XElement> listAny = this.Any == null ? new List<XElement>() : new List<XElement>(this.Any);

            using (xmlReader)
            {
                xmlReader.MoveToContent();
                while (!xmlReader.EOF)
                {
                    if (xmlReader.Depth == 1)
                    {
                        switch (xmlReader.Name)
                        {
                            case "square":
                                //no checking on uid,allow it to be added all the time
                                if (this.square == null)
                                    this.square = new cs_square();
                                this.square.HandleXML(xmlReader.ReadSubtree(), isNew);
                                xmlReader.Read();
                                break;
                            default:
                                if(resetCustomData)
                                {
                                    //Reset the content of custom data when new fields are submitted
                                    resetCustomData = false;
                                    listAny.Clear();
                                }
                                XElement element = XElement.ReadFrom(xmlReader) as XElement;
                                listAny.Add(element);
                                break;
                        }
                    }
                    else
                    {
                        xmlReader.Read();
                    }

                }
                this.Any = listAny.ToArray();
            }
        }
    }
}
