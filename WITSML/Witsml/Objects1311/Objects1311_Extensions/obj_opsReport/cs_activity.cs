 

using System;
using System.Collections.Generic;
using System.Text;
using System.Xml;

namespace Witsml.Objects1311
{
    public partial class cs_activity : IHandleXML, IHasID
    {
        //this will need to be ignored
        [System.Xml.Serialization.XmlIgnore]
        public string UID
        {
            get { return this.uid; }
        }

        public cs_activity()
        {
            this.typeActivityClass = ActivityClassType.unknown;
            this.itemState = ItemState.unknown;
        }
        /// <summary>
        /// Convert XML string data into cs_activity fields value
        /// </summary>
        /// <param name="xmlReader">reader that has cs_activity node</param>
        /// <param name="IsNew">Is the object from stratch(true) or from db/serialization(false)</param>
        public void HandleXML(XmlReader xmlReader, bool isNew)
        {
            using (xmlReader)
            {
                xmlReader.MoveToContent();
                if (xmlReader.HasAttributes)
                {
                    if (xmlReader.MoveToAttribute("uid"))
                        StaticHelper.HandleUID(xmlReader.Value, "activity uid", out this.uid);
                    xmlReader.MoveToElement();
                }
                while (xmlReader.Read())
                {
                    if (xmlReader.Depth == 1 && xmlReader.IsStartElement() && !xmlReader.IsEmptyElement)
                    {
                        switch (xmlReader.Name)
                        {
                            case "dTimStart":
                                this.dTimStart = StaticParser.ParseDateTimeFromString(StaticHelper.ReadString(xmlReader));
                                this.dTimStartSpecified = true;
                                break;
                            case "dTimEnd":
                                this.dTimEnd = StaticParser.ParseDateTimeFromString(StaticHelper.ReadString(xmlReader));
                                this.dTimEndSpecified = true;
                                break;
                            case "duration":
                                if (this.duration == null)
                                    this.duration = new timeMeasure();
                                this.duration.HandleXML(xmlReader.ReadSubtree(), isNew);
                                break;
                            case "phase":
                                this.phase = StaticHelper.ReadString(xmlReader);
                                break;
                            case "activityCode":
                                this.activityCode = StaticHelper.ReadString(xmlReader);
                                break;
                            case "detailActivity":
                                this.detailActivity = StaticHelper.ReadString(xmlReader);
                                break;
                            case "typeActivityClass":
                                StaticParser.SetEnumFromString<ActivityClassType>(StaticHelper.ReadString(xmlReader), out this.typeActivityClass, out this.typeActivityClassSpecified);
                                break;
                            case "mdHoleStart":
                                if (this.mdHoleStart == null)
                                    this.mdHoleStart = new measuredDepthCoord();
                                this.mdHoleStart.HandleXML(xmlReader.ReadSubtree(), isNew);
                                break;
                            case "tvdHoleStart":
                                if (this.tvdHoleStart == null)
                                    this.tvdHoleStart = new wellVerticalDepthCoord();
                                this.tvdHoleStart.HandleXML(xmlReader.ReadSubtree(), isNew);
                                break;
                            case "mdHoleEnd":
                                if (this.mdHoleEnd == null)
                                    this.mdHoleEnd = new measuredDepthCoord();
                                this.mdHoleEnd.HandleXML(xmlReader.ReadSubtree(), isNew);
                                break;
                            case "tvdHoleEnd":
                                if (this.tvdHoleEnd == null)
                                    this.tvdHoleEnd = new wellVerticalDepthCoord();
                                this.tvdHoleEnd.HandleXML(xmlReader.ReadSubtree(), isNew);
                                break;
                            case "mdBitStart":
                                if (this.mdBitStart == null)
                                    this.mdBitStart = new measuredDepthCoord();
                                this.mdBitStart.HandleXML(xmlReader.ReadSubtree(), isNew);
                                break;
                            case "mdBitEnd":
                                if (this.mdBitEnd == null)
                                    this.mdBitEnd = new measuredDepthCoord();
                                this.mdBitEnd.HandleXML(xmlReader.ReadSubtree(), isNew);
                                break;
                            case "state":
                                this.state = StaticHelper.ReadString(xmlReader);
                                break;
                            case "operator":
                                this.@operator = StaticHelper.ReadString(xmlReader);
                                break;
                            case "tubular":
                                if (this.tubular == null)
                                    this.tubular = new refNameString();
                                this.tubular.ObjectName = "tubular";
                                this.tubular.HandleXML(xmlReader.ReadSubtree(), isNew);
                                break;
                            case "optimum":
                                this.optimum = StaticParser.ParseBooleanFromString(StaticHelper.ReadString(xmlReader));
                                this.optimumSpecified = true;
                                break;
                            case "productive":
                                this.productive = StaticParser.ParseBooleanFromString(StaticHelper.ReadString(xmlReader));
                                this.productiveSpecified = true;
                                break;
                            case "itemState":
                                StaticParser.SetEnumFromString<ItemState>(StaticHelper.ReadString(xmlReader), out this.itemState, out this.itemStateSpecified);
                                break;
                            case "comments":
                                this.comments = StaticHelper.ReadString(xmlReader);
                                break;
                            default:
                                break;
                        }
                    }
                }
            }
        }
    }
}
