 

using System;
using System.Collections.Generic;
using System.Text;
using System.Xml;

namespace Witsml.Objects1311
{
    public partial class cs_dayCost : IHandleXML, IHasID
    {
        //this will need to be ignored
        [System.Xml.Serialization.XmlIgnore]
        public string UID
        {
            get { return this.uid; }
        }
        /// <summary>
        /// Convert XML string data into cs_dayCost fields value
        /// </summary>
        /// <param name="xmlReader">reader that has cs_dayCost node</param>
        /// <param name="IsNew">Is the object from stratch(true) or from db/serialization(false)</param>
        public void HandleXML(XmlReader xmlReader, bool isNew)
        {
            //condition for add update collection
            bool isNameTagNew = this.nameTag == null;
            
            //temporary list for data manipulations
            List<cs_nameTag> listNameTag = this.nameTag == null ? new List<cs_nameTag>() : new List<cs_nameTag>(this.nameTag);
            using (xmlReader)
            {
                xmlReader.MoveToContent();
                if (xmlReader.HasAttributes)
                {
                    if (xmlReader.MoveToAttribute("uid"))
                        StaticHelper.HandleUID(xmlReader.Value, "dayCost uid", out this.uid);
                    xmlReader.MoveToElement();
                }
                while (xmlReader.Read())
                {
                    if (xmlReader.Depth == 1 && xmlReader.IsStartElement() && !xmlReader.IsEmptyElement)
                    {
                        switch (xmlReader.Name)
                        {
                            case "numAFE":
                                this.numAFE = StaticHelper.ReadString(xmlReader);
                                break;
                            case "costGroup":
                                this.costGroup = StaticHelper.ReadString(xmlReader);
                                break;
                            case "costClass":
                                this.costClass = StaticHelper.ReadString(xmlReader);
                                break;
                            case "costCode":
                                this.costCode = StaticHelper.ReadString(xmlReader);
                                break;
                            case "costSubCode":
                                this.costSubCode = StaticHelper.ReadString(xmlReader);
                                break;
                            case "costItemDescription":
                                this.costItemDescription = StaticHelper.ReadString(xmlReader);
                                break;
                            case "costPerItem":
                                if (this.costPerItem == null)
                                    this.costPerItem = new cost();
                                this.costPerItem.HandleXML(xmlReader.ReadSubtree(), isNew);
                                break;
                            case "itemKind":
                                this.itemKind = StaticHelper.ReadString(xmlReader);
                                break;
                            case "itemSize":
                                this.itemSize = StaticParser.ParseDoubleFromString(StaticHelper.ReadString(xmlReader));
                                this.itemSizeSpecified = true;
                                break;
                            case "qtyItem":
                                this.qtyItem = StaticParser.ParseShortFromString(StaticHelper.ReadString(xmlReader));
                                this.qtyItemSpecified = true;
                                break;
                            case "costAmount":
                                if (this.costAmount == null)
                                    this.costAmount = new cost();
                                this.costAmount.HandleXML(xmlReader.ReadSubtree(), isNew);
                                break;
                            case "numInvoice":
                                this.numInvoice = StaticHelper.ReadString(xmlReader);
                                break;
                            case "numPO":
                                this.numPO = StaticHelper.ReadString(xmlReader);
                                break;
                            case "numTicket":
                                this.numTicket = StaticHelper.ReadString(xmlReader);
                                break;
                            case "isCarryOver":
                                this.isCarryOver = StaticParser.ParseBooleanFromString(StaticHelper.ReadString(xmlReader));
                                this.isCarryOverSpecified = true;
                                break;
                            case "isRental":
                                this.isRental = StaticParser.ParseBooleanFromString(StaticHelper.ReadString(xmlReader));
                                this.isRentalSpecified = true;
                                break;
                            case "nameTag":
                                //collection
                                StaticHelper.AddUpdateWithUid<cs_nameTag>(xmlReader, isNameTagNew, ref listNameTag);
                                break;
                            case "numSerial":
                                this.numSerial = StaticHelper.ReadString(xmlReader);
                                break;
                            case "nameVendor":
                                this.nameVendor = StaticHelper.ReadString(xmlReader);
                                break;
                            case "numVendor":
                                this.numVendor = StaticHelper.ReadString(xmlReader);
                                break;
                            case "pool":
                                this.pool = StaticHelper.ReadString(xmlReader);
                                break;
                            case "estimated":
                                this.estimated = StaticParser.ParseBooleanFromString(StaticHelper.ReadString(xmlReader));
                                this.estimatedSpecified = true;
                                break;
                            default:
                                break;
                        }
                    }
                }
                //assign list to collection
                this.nameTag = listNameTag.ToArray();
            }
        }
    }
}
