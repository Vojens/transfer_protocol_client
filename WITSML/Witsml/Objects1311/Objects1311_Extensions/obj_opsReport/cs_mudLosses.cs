 

using System;
using System.Collections.Generic;
using System.Text;
using System.Xml;

namespace Witsml.Objects1311
{
    public partial class cs_mudLosses : IHandleXML
    {
        /// <summary>
        /// Convert XML string data into cs_mudLosses fields value
        /// </summary>
        /// <param name="xmlReader">reader that has cs_mudLosses node</param>
        /// <param name="IsNew">Is the object from stratch(true) or from db/serialization(false)</param>
        public void HandleXML(XmlReader xmlReader, bool isNew)
        {
            using (xmlReader)
            {
                while (xmlReader.Read())
                {
                    if (xmlReader.Depth == 1 && xmlReader.IsStartElement() && !xmlReader.IsEmptyElement)
                    {
                        switch (xmlReader.Name)
                        {
                            case "volLostShakerSurf":
                                if (this.volLostShakerSurf == null)
                                    this.volLostShakerSurf = new volumeMeasure();
                                this.volLostShakerSurf.HandleXML(xmlReader.ReadSubtree(), isNew);
                                break;
                            case "volLostMudCleanerSurf":
                                if (this.volLostMudCleanerSurf == null)
                                    this.volLostMudCleanerSurf = new volumeMeasure();
                                this.volLostMudCleanerSurf.HandleXML(xmlReader.ReadSubtree(), isNew);
                                break;
                            case "volLostPitsSurf":
                                if (this.volLostPitsSurf == null)
                                    this.volLostPitsSurf = new volumeMeasure();
                                this.volLostPitsSurf.HandleXML(xmlReader.ReadSubtree(), isNew);
                                break;
                            case "volLostTrippingSurf":
                                if (this.volLostTrippingSurf == null)
                                    this.volLostTrippingSurf = new volumeMeasure();
                                this.volLostTrippingSurf.HandleXML(xmlReader.ReadSubtree(), isNew);
                                break;
                            case "volLostOtherSurf":
                                if (this.volLostOtherSurf == null)
                                    this.volLostOtherSurf = new volumeMeasure();
                                this.volLostOtherSurf.HandleXML(xmlReader.ReadSubtree(), isNew);
                                break;
                            case "volTotMudLostSurf":
                                if (this.volTotMudLostSurf == null)
                                    this.volTotMudLostSurf = new volumeMeasure();
                                this.volTotMudLostSurf.HandleXML(xmlReader.ReadSubtree(), isNew);
                                break;
                            case "volLostCircHole":
                                if (this.volLostCircHole == null)
                                    this.volLostCircHole = new volumeMeasure();
                                this.volLostCircHole.HandleXML(xmlReader.ReadSubtree(), isNew);
                                break;
                            case "volLostCsgHole":
                                if (this.volLostCsgHole == null)
                                    this.volLostCsgHole = new volumeMeasure();
                                this.volLostCsgHole.HandleXML(xmlReader.ReadSubtree(), isNew);
                                break;
                            case "volLostCmtHole":
                                if (this.volLostCmtHole == null)
                                    this.volLostCmtHole = new volumeMeasure();
                                this.volLostCmtHole.HandleXML(xmlReader.ReadSubtree(), isNew);
                                break;
                            case "volLostBhdCsgHole":
                                if (this.volLostBhdCsgHole == null)
                                    this.volLostBhdCsgHole = new volumeMeasure();
                                this.volLostBhdCsgHole.HandleXML(xmlReader.ReadSubtree(), isNew);
                                break;
                            case "volLostAbandonHole":
                                if (this.volLostAbandonHole == null)
                                    this.volLostAbandonHole = new volumeMeasure();
                                this.volLostAbandonHole.HandleXML(xmlReader.ReadSubtree(), isNew);
                                break;
                            case "volLostOtherHole":
                                if (this.volLostOtherHole == null)
                                    this.volLostOtherHole = new volumeMeasure();
                                this.volLostOtherHole.HandleXML(xmlReader.ReadSubtree(), isNew);
                                break;
                            case "volTotMudLostHole":
                                if (this.volTotMudLostHole == null)
                                    this.volTotMudLostHole = new volumeMeasure();
                                this.volTotMudLostHole.HandleXML(xmlReader.ReadSubtree(), isNew);
                                break;
                            default:
                                break;
                        }
                    }
                }
            }
        }
    }
}
