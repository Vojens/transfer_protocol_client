 

using System;
using System.Collections.Generic;
using System.Text;
using System.Xml;

namespace Witsml.Objects1311
{
    public partial class cs_personnel : IHandleXML, IHasID
    {
        //this will need to be ignored
        [System.Xml.Serialization.XmlIgnore]
        public string UID
        {
            get { return this.uid; }
        }
        /// <summary>
        /// Convert XML string data into cs_personnel fields value
        /// </summary>
        /// <param name="xmlReader">reader that has cs_personnel node</param>
        /// <param name="IsNew">Is the object from stratch(true) or from db/serialization(false)</param>
        public void HandleXML(XmlReader xmlReader, bool isNew)
        {
            using (xmlReader)
            {
                xmlReader.MoveToContent();
                if (xmlReader.HasAttributes)
                {
                    if (xmlReader.MoveToAttribute("uid"))
                        StaticHelper.HandleUID(xmlReader.Value, "personnel uid", out this.uid);
                    xmlReader.MoveToElement();
                }
                while (xmlReader.Read())
                {
                    if (xmlReader.Depth == 1 && xmlReader.IsStartElement() && !xmlReader.IsEmptyElement)
                    {
                        switch (xmlReader.Name)
                        {
                            case "company":
                                this.company = StaticHelper.ReadString(xmlReader);
                                break;
                            case "typeService":
                                this.typeService = StaticHelper.ReadString(xmlReader);
                                break;
                            case "numPeople":
                                this.numPeople = StaticParser.ParseShortFromString(StaticHelper.ReadString(xmlReader));
                                this.numPeopleSpecified = true;
                                break;
                            case "totalTime":
                                if (this.totalTime == null)
                                    this.totalTime = new timeMeasure();
                                this.totalTime.HandleXML(xmlReader.ReadSubtree(), isNew);
                                break;
                            default:
                                break;
                        }
                    }
                }
            }
        }
    }
}
