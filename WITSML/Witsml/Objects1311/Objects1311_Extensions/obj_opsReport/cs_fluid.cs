 

using System;
using System.Collections.Generic;
using System.Text;
using System.Xml;

namespace Witsml.Objects1311
{
    public partial class cs_fluid : IHandleXML, IHasID
    {
        //this will need to be ignored
        [System.Xml.Serialization.XmlIgnore]
        public string UID
        {
            get { return this.uid; }
        }
        /// <summary>
        /// Convert XML string data into cs_fluid fields value
        /// </summary>
        /// <param name="xmlReader">reader that has cs_fluid node</param>
        /// <param name="IsNew">Is the object from stratch(true) or from db/serialization(false)</param>
        public void HandleXML(XmlReader xmlReader, bool isNew)
        {
            bool isRheoNew = this.rheometer == null;
            //temporary list for data manipulations
            List<cs_rheometer> listRheo = isRheoNew ? new List<cs_rheometer>() : new List<cs_rheometer>(this.rheometer);
            using (xmlReader)
            {
                xmlReader.MoveToContent();
                if (xmlReader.HasAttributes)
                {
                    if (xmlReader.MoveToAttribute("uid"))
                        StaticHelper.HandleUID(xmlReader.Value, "fluid uid", out this.uid);
                    xmlReader.MoveToElement();
                }
                while (xmlReader.Read())
                {
                    if (xmlReader.Depth == 1 && xmlReader.IsStartElement() && !xmlReader.IsEmptyElement)
                    {
                        switch (xmlReader.Name)
                        {
                            case "type":
                                this.type = StaticHelper.ReadString(xmlReader);
                                break;
                            case "locationSample":
                                this.locationSample = StaticHelper.ReadString(xmlReader);
                                break;
                            case "dTim":
                                this.dTim = StaticParser.ParseDateTimeFromString(StaticHelper.ReadString(xmlReader));
                                this.dTimSpecified = true;
                                break;
                            case "md":
                                if (this.md == null)
                                    this.md = new measuredDepthCoord();
                                this.md.HandleXML(xmlReader.ReadSubtree(), isNew);
                                break;
                            case "density":
                                if (this.density == null)
                                    this.density = new densityMeasure();
                                this.density.HandleXML(xmlReader.ReadSubtree(), isNew);
                                break;
                            case "visFunnel":
                                if (this.visFunnel == null)
                                    this.visFunnel = new timeMeasure();
                                this.visFunnel.HandleXML(xmlReader.ReadSubtree(), isNew);
                                break;
                            case "tempVis":
                                if (this.tempVis == null)
                                    this.tempVis = new thermodynamicTemperatureMeasure();
                                this.tempVis.HandleXML(xmlReader.ReadSubtree(), isNew);
                                break;
                            case "pv":
                                if (this.pv == null)
                                    this.pv = new dynamicViscosityMeasure();
                                this.pv.HandleXML(xmlReader.ReadSubtree(), isNew);
                                break;
                            case "yp":
                                if (this.yp == null)
                                    this.yp = new pressureMeasure();
                                this.yp.HandleXML(xmlReader.ReadSubtree(), isNew);
                                break;
                            case "gel10Sec":
                                if (this.gel10Sec == null)
                                    this.gel10Sec = new pressureMeasure();
                                this.gel10Sec.HandleXML(xmlReader.ReadSubtree(), isNew);
                                break;
                            case "gel10Min":
                                if (this.gel10Min == null)
                                    this.gel10Min = new pressureMeasure();
                                this.gel10Min.HandleXML(xmlReader.ReadSubtree(), isNew);
                                break;
                            case "gel30Min":
                                if (this.gel30Min == null)
                                    this.gel30Min = new pressureMeasure();
                                this.gel30Min.HandleXML(xmlReader.ReadSubtree(), isNew);
                                break;
                            case "filterCakeLtlp":
                                if (this.filterCakeLtlp == null)
                                    this.filterCakeLtlp = new lengthMeasure();
                                this.filterCakeLtlp.HandleXML(xmlReader.ReadSubtree(), isNew);
                                break;
                            case "filtrateLtlp":
                                if (this.filtrateLtlp == null)
                                    this.filtrateLtlp = new volumeMeasure();
                                this.filtrateLtlp.HandleXML(xmlReader.ReadSubtree(), isNew);
                                break;
                            case "tempHthp":
                                if (this.tempHthp == null)
                                    this.tempHthp = new thermodynamicTemperatureMeasure();
                                this.tempHthp.HandleXML(xmlReader.ReadSubtree(), isNew);
                                break;
                            case "presHthp":
                                if (this.presHthp == null)
                                    this.presHthp = new pressureMeasure();
                                this.presHthp.HandleXML(xmlReader.ReadSubtree(), isNew);
                                break;
                            case "filtrateHthp":
                                if (this.filtrateHthp == null)
                                    this.filtrateHthp = new volumeMeasure();
                                this.filtrateHthp.HandleXML(xmlReader.ReadSubtree(), isNew);
                                break;
                            case "filterCakeHthp":
                                if (this.filterCakeHthp == null)
                                    this.filterCakeHthp = new lengthMeasure();
                                this.filterCakeHthp.HandleXML(xmlReader.ReadSubtree(), isNew);
                                break;
                            case "solidsPc":
                                if (this.solidsPc == null)
                                    this.solidsPc = new volumePerVolumeMeasure();
                                this.solidsPc.HandleXML(xmlReader.ReadSubtree(), isNew);
                                break;
                            case "waterPc":
                                if (this.waterPc == null)
                                    this.waterPc = new volumePerVolumeMeasure();
                                this.waterPc.HandleXML(xmlReader.ReadSubtree(), isNew);
                                break;
                            case "oilPc":
                                if (this.oilPc == null)
                                    this.oilPc = new volumePerVolumeMeasure();
                                this.oilPc.HandleXML(xmlReader.ReadSubtree(), isNew);
                                break;
                            case "sandPc":
                                if (this.sandPc == null)
                                    this.sandPc = new volumePerVolumeMeasure();
                                this.sandPc.HandleXML(xmlReader.ReadSubtree(), isNew);
                                break;
                            case "solidsLowGravPc":
                                if (this.solidsLowGravPc == null)
                                    this.solidsLowGravPc = new volumePerVolumeMeasure();
                                this.solidsLowGravPc.HandleXML(xmlReader.ReadSubtree(), isNew);
                                break;
                            case "solidsCalcPc":
                                if (this.solidsCalcPc == null)
                                    this.solidsCalcPc = new volumePerVolumeMeasure();
                                this.solidsCalcPc.HandleXML(xmlReader.ReadSubtree(), isNew);
                                break;
                            case "baritePc":
                                if (this.baritePc == null)
                                    this.baritePc = new volumePerVolumeMeasure();
                                this.baritePc.HandleXML(xmlReader.ReadSubtree(), isNew);
                                break;
                            case "lcm":
                                if (this.lcm == null)
                                    this.lcm = new densityMeasure();
                                this.lcm.HandleXML(xmlReader.ReadSubtree(), isNew);
                                break;
                            case "mbt":
                                if (this.mbt == null)
                                    this.mbt = new equivalentPerMassMeasure();
                                this.mbt.HandleXML(xmlReader.ReadSubtree(), isNew);
                                break;
                            case "ph":
                                this.ph = StaticParser.ParseDoubleFromString(StaticHelper.ReadString(xmlReader));
                                this.phSpecified = true;
                                break;
                            case "tempPh":
                                if (this.tempPh == null)
                                    this.tempPh = new thermodynamicTemperatureMeasure();
                                this.tempPh.HandleXML(xmlReader.ReadSubtree(), isNew);
                                break;
                            case "pm":
                                if (this.pm == null)
                                    this.pm = new volumeMeasure();
                                this.pm.HandleXML(xmlReader.ReadSubtree(), isNew);
                                break;
                            case "pmFiltrate":
                                if (this.pmFiltrate == null)
                                    this.pmFiltrate = new volumeMeasure();
                                this.pmFiltrate.HandleXML(xmlReader.ReadSubtree(), isNew);
                                break;
                            case "mf":
                                if (this.mf == null)
                                    this.mf = new volumeMeasure();
                                this.mf.HandleXML(xmlReader.ReadSubtree(), isNew);
                                break;
                            case "alkalinityP1":
                                if (this.alkalinityP1 == null)
                                    this.alkalinityP1 = new volumeMeasure();
                                this.alkalinityP1.HandleXML(xmlReader.ReadSubtree(), isNew);
                                break;
                            case "alkalinityP2":
                                if (this.alkalinityP2 == null)
                                    this.alkalinityP2 = new volumeMeasure();
                                this.alkalinityP2.HandleXML(xmlReader.ReadSubtree(), isNew);
                                break;
                            case "chloride":
                                if (this.chloride == null)
                                    this.chloride = new densityMeasure();
                                this.chloride.HandleXML(xmlReader.ReadSubtree(), isNew);
                                break;
                            case "calcium":
                                if (this.calcium == null)
                                    this.calcium = new densityMeasure();
                                this.calcium.HandleXML(xmlReader.ReadSubtree(), isNew);
                                break;
                            case "magnesium":
                                if (this.magnesium == null)
                                    this.magnesium = new densityMeasure();
                                this.magnesium.HandleXML(xmlReader.ReadSubtree(), isNew);
                                break;
                            case "potassium":
                                if (this.potassium == null)
                                    this.potassium = new densityMeasure();
                                this.potassium.HandleXML(xmlReader.ReadSubtree(), isNew);
                                break;
                            case "rheometer":
                                //collection
                                StaticHelper.AddUpdateWithUid<cs_rheometer>(xmlReader, isRheoNew, ref listRheo);
                                break;
                            case "brinePc":
                                if (this.brinePc == null)
                                    this.brinePc = new volumePerVolumeMeasure();
                                this.brinePc.HandleXML(xmlReader.ReadSubtree(), isNew);
                                break;
                            case "lime":
                                if (this.lime == null)
                                    this.lime = new densityMeasure();
                                this.lime.HandleXML(xmlReader.ReadSubtree(), isNew);
                                break;
                            case "electStab":
                                if (this.electStab == null)
                                    this.electStab = new electricPotentialMeasure();
                                this.electStab.HandleXML(xmlReader.ReadSubtree(), isNew);
                                break;
                            case "calciumChloride":
                                if (this.calciumChloride == null)
                                    this.calciumChloride = new densityMeasure();
                                this.calciumChloride.HandleXML(xmlReader.ReadSubtree(), isNew);
                                break;
                            case "company":
                                this.company = StaticHelper.ReadString(xmlReader);
                                break;
                            case "engineer":
                                this.engineer = StaticHelper.ReadString(xmlReader);
                                break;
                            case "asg":
                                this.asg = StaticParser.ParseDoubleFromString(StaticHelper.ReadString(xmlReader));
                                this.asgSpecified = true;
                                break;
                            case "solidsHiGravPc":
                                if (this.solidsHiGravPc == null)
                                    this.solidsHiGravPc = new volumePerVolumeMeasure();
                                this.solidsHiGravPc.HandleXML(xmlReader.ReadSubtree(), isNew);
                                break;
                            case "polymer":
                                if (this.polymer == null)
                                    this.polymer = new volumePerVolumeMeasure();
                                this.polymer.HandleXML(xmlReader.ReadSubtree(), isNew);
                                break;
                            case "polyType":
                                this.polyType = StaticHelper.ReadString(xmlReader);
                                break;
                            case "solCorPc":
                                if (this.solCorPc == null)
                                    this.solCorPc = new volumePerVolumeMeasure();
                                this.solCorPc.HandleXML(xmlReader.ReadSubtree(), isNew);
                                break;
                            case "oilCtg":
                                if (this.oilCtg == null)
                                    this.oilCtg = new massConcentrationMeasure();
                                this.oilCtg.HandleXML(xmlReader.ReadSubtree(), isNew);
                                break;
                            case "hardnessCa":
                                if (this.hardnessCa == null)
                                    this.hardnessCa = new massConcentrationMeasure();
                                this.hardnessCa.HandleXML(xmlReader.ReadSubtree(), isNew);
                                break;
                            case "sulfide":
                                if (this.sulfide == null)
                                    this.sulfide = new densityMeasure();
                                this.sulfide.HandleXML(xmlReader.ReadSubtree(), isNew);
                                break;
                            case "comments":
                                this.comments = StaticHelper.ReadString(xmlReader);
                                break;
                            default:
                                break;
                        }
                    }
                }
                //assign list to collection
                this.rheometer = listRheo.ToArray();
            }
        }
    }
}
