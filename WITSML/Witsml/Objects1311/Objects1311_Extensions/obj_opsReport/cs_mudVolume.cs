 

using System;
using System.Collections.Generic;
using System.Text;
using System.Xml;

namespace Witsml.Objects1311
{
    public partial class cs_mudVolume : IHandleXML
    {
        /// <summary>
        /// Convert XML string data into cs_mudVolume fields value
        /// </summary>
        /// <param name="xmlReader">reader that has cs_mudVolume node</param>
        /// <param name="IsNew">Is the object from stratch(true) or from db/serialization(false)</param>
        public void HandleXML(XmlReader xmlReader, bool isNew)
        {
            using (xmlReader)
            {
                while (xmlReader.Read())
                {
                    if (xmlReader.Depth == 1 && xmlReader.IsStartElement() && !xmlReader.IsEmptyElement)
                    {
                        switch (xmlReader.Name)
                        {
                            case "volTotMudStart":
                                if (this.volTotMudStart == null)
                                    this.volTotMudStart = new volumeMeasure();
                                this.volTotMudStart.HandleXML(xmlReader.ReadSubtree(), isNew);
                                break;
                            case "volMudDumped":
                                if (this.volMudDumped == null)
                                    this.volMudDumped = new volumeMeasure();
                                this.volMudDumped.HandleXML(xmlReader.ReadSubtree(), isNew);
                                break;
                            case "volMudReceived":
                                if (this.volMudReceived == null)
                                    this.volMudReceived = new volumeMeasure();
                                this.volMudReceived.HandleXML(xmlReader.ReadSubtree(), isNew);
                                break;
                            case "volMudReturned":
                                if (this.volMudReturned == null)
                                    this.volMudReturned = new volumeMeasure();
                                this.volMudReturned.HandleXML(xmlReader.ReadSubtree(), isNew);
                                break;
                            case "mudLosses":
                                if (this.mudLosses == null)
                                    this.mudLosses = new cs_mudLosses();
                                this.mudLosses.HandleXML(xmlReader.ReadSubtree(), isNew);
                                break;
                            case "volMudBuilt":
                                if (this.volMudBuilt == null)
                                    this.volMudBuilt = new volumeMeasure();
                                this.volMudBuilt.HandleXML(xmlReader.ReadSubtree(), isNew);
                                break;
                            case "volMudString":
                                if (this.volMudString == null)
                                    this.volMudString = new volumeMeasure();
                                this.volMudString.HandleXML(xmlReader.ReadSubtree(), isNew);
                                break;
                            case "volMudCasing":
                                if (this.volMudCasing == null)
                                    this.volMudCasing = new volumeMeasure();
                                this.volMudCasing.HandleXML(xmlReader.ReadSubtree(), isNew);
                                break;
                            case "volMudHole":
                                if (this.volMudHole == null)
                                    this.volMudHole = new volumeMeasure();
                                this.volMudHole.HandleXML(xmlReader.ReadSubtree(), isNew);
                                break;
                            case "volMudRiser":
                                if (this.volMudRiser == null)
                                    this.volMudRiser = new volumeMeasure();
                                this.volMudRiser.HandleXML(xmlReader.ReadSubtree(), isNew);
                                break;
                            case "volTotMudEnd":
                                if (this.volTotMudEnd == null)
                                    this.volTotMudEnd = new volumeMeasure();
                                this.volTotMudEnd.HandleXML(xmlReader.ReadSubtree(), isNew);
                                break;
                            default:
                                break;
                        }
                    }
                }
            }
        }
    }
}
