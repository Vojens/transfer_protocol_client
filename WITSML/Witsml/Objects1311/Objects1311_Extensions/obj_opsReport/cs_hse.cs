 

using System;
using System.Collections.Generic;
using System.Text;
using System.Xml;

namespace Witsml.Objects1311
{
    public partial class cs_hse : IHandleXML
    {
        /// <summary>
        /// Convert XML string data into cs_hse fields value
        /// </summary>
        /// <param name="xmlReader">reader that has cs_hse node</param>
        /// <param name="IsNew">Is the object from stratch(true) or from db/serialization(false)</param>
        public void HandleXML(XmlReader xmlReader, bool isNew)
        {
            //condition for add update collection
            bool isIncidentNew = this.incident == null;

            //temporary list for data manipulations
            List<cs_incident> listIncident = isIncidentNew ? new List<cs_incident>() : new List<cs_incident>(this.incident);

            using (xmlReader)
            {
                while (xmlReader.Read())
                {
                    if (xmlReader.Depth == 1 && xmlReader.IsStartElement() && !xmlReader.IsEmptyElement)
                    {
                        switch (xmlReader.Name)
                        {
                            case "daysIncFree":
                                if (this.daysIncFree == null)
                                    this.daysIncFree = new timeMeasure();
                                this.daysIncFree.HandleXML(xmlReader.ReadSubtree(), isNew);
                                break;
                            case "incident":
                                //collection
                                StaticHelper.AddUpdateWithUid<cs_incident>(xmlReader, isIncidentNew, ref listIncident);
                                break;
                            case "lastCsgPresTest":
                                this.lastCsgPresTest = StaticParser.ParseDateTimeFromString(StaticHelper.ReadString(xmlReader));
                                this.lastCsgPresTestSpecified = true;
                                break;
                            case "presLastCsg":
                                if (this.presLastCsg == null)
                                    this.presLastCsg = new pressureMeasure();
                                this.presLastCsg.HandleXML(xmlReader.ReadSubtree(), isNew);
                                break;
                            case "lastBopPresTest":
                                this.lastBopPresTest = StaticParser.ParseDateTimeFromString(StaticHelper.ReadString(xmlReader));
                                this.lastBopPresTestSpecified = true;
                                break;
                            case "nextBopPresTest":
                                this.nextBopPresTest = StaticParser.ParseDateTimeFromString(StaticHelper.ReadString(xmlReader));
                                this.nextBopPresTestSpecified = true;
                                break;
                            case "presStdPipe":
                                if (this.presStdPipe == null)
                                    this.presStdPipe = new pressureMeasure();
                                this.presStdPipe.HandleXML(xmlReader.ReadSubtree(), isNew);
                                break;
                            case "presKellyHose":
                                if (this.presKellyHose == null)
                                    this.presKellyHose = new pressureMeasure();
                                this.presKellyHose.HandleXML(xmlReader.ReadSubtree(), isNew);
                                break;
                            case "presDiverter":
                                if (this.presDiverter == null)
                                    this.presDiverter = new pressureMeasure();
                                this.presDiverter.HandleXML(xmlReader.ReadSubtree(), isNew);
                                break;
                            case "presAnnular":
                                if (this.presAnnular == null)
                                    this.presAnnular = new pressureMeasure();
                                this.presAnnular.HandleXML(xmlReader.ReadSubtree(), isNew);
                                break;
                            case "presRams":
                                if (this.presRams == null)
                                    this.presRams = new pressureMeasure();
                                this.presRams.HandleXML(xmlReader.ReadSubtree(), isNew);
                                break;
                            case "presChokeLine":
                                if (this.presChokeLine == null)
                                    this.presChokeLine = new pressureMeasure();
                                this.presChokeLine.HandleXML(xmlReader.ReadSubtree(), isNew);
                                break;
                            case "presChokeMan":
                                if (this.presChokeMan == null)
                                    this.presChokeMan = new pressureMeasure();
                                this.presChokeMan.HandleXML(xmlReader.ReadSubtree(), isNew);
                                break;
                            case "lastFireBoatDrill":
                                this.lastFireBoatDrill = StaticParser.ParseDateTimeFromString(StaticHelper.ReadString(xmlReader));
                                this.lastFireBoatDrillSpecified = true;
                                break;
                            case "lastAbandonDrill":
                                this.lastAbandonDrill = StaticParser.ParseDateTimeFromString(StaticHelper.ReadString(xmlReader));
                                this.lastAbandonDrillSpecified = true;
                                break;
                            case "lastRigInspection":
                                this.lastRigInspection = StaticParser.ParseDateTimeFromString(StaticHelper.ReadString(xmlReader));
                                this.lastRigInspectionSpecified = true;
                                break;
                            case "lastSafetyMeeting":
                                this.lastSafetyMeeting = StaticParser.ParseDateTimeFromString(StaticHelper.ReadString(xmlReader));
                                this.lastSafetyMeetingSpecified = true;
                                break;
                            case "lastSafetyInspection":
                                this.lastSafetyInspection = StaticParser.ParseDateTimeFromString(StaticHelper.ReadString(xmlReader));
                                this.lastSafetyInspectionSpecified = true;
                                break;
                            case "lastTripDrill":
                                this.lastTripDrill = StaticParser.ParseDateTimeFromString(StaticHelper.ReadString(xmlReader));
                                this.lastTripDrillSpecified = true;
                                break;
                            case "lastDiverterDrill":
                                this.lastDiverterDrill = StaticParser.ParseDateTimeFromString(StaticHelper.ReadString(xmlReader));
                                this.lastDiverterDrillSpecified = true;
                                break;
                            case "lastBopDrill":
                                this.lastBopDrill = StaticParser.ParseDateTimeFromString(StaticHelper.ReadString(xmlReader));
                                this.lastBopDrillSpecified = true;
                                break;
                            case "regAgencyInsp":
                                this.regAgencyInsp = StaticParser.ParseBooleanFromString(StaticHelper.ReadString(xmlReader));
                                this.regAgencyInspSpecified = true;
                                break;
                            case "nonComplianceIssued":
                                this.nonComplianceIssued = StaticParser.ParseBooleanFromString(StaticHelper.ReadString(xmlReader));
                                this.nonComplianceIssuedSpecified = true;
                                break;
                            case "numStopCards":
                                this.numStopCards = StaticParser.ParseShortFromString(StaticHelper.ReadString(xmlReader));
                                this.numStopCardsSpecified = true;
                                break;
                            case "fluidDischarged":
                                if (this.fluidDischarged == null)
                                    this.fluidDischarged = new volumeMeasure();
                                this.fluidDischarged.HandleXML(xmlReader.ReadSubtree(), isNew);
                                break;
                            case "volCtgDischarged":
                                if (this.volCtgDischarged == null)
                                    this.volCtgDischarged = new volumeMeasure();
                                this.volCtgDischarged.HandleXML(xmlReader.ReadSubtree(), isNew);
                                break;
                            case "volOilCtgDischarge":
                                if (this.volOilCtgDischarge == null)
                                    this.volOilCtgDischarge = new volumeMeasure();
                                this.volOilCtgDischarge.HandleXML(xmlReader.ReadSubtree(), isNew);
                                break;
                            case "wasteDischarged":
                                if (this.wasteDischarged == null)
                                    this.wasteDischarged = new volumeMeasure();
                                this.wasteDischarged.HandleXML(xmlReader.ReadSubtree(), isNew);
                                break;
                            case "comments":
                                this.comments = StaticHelper.ReadString(xmlReader);
                                break;
                            default:
                                break;
                        }
                    }
                }
                //assign list to collection
                this.incident = listIncident.ToArray();
            }
        }
    }
}
