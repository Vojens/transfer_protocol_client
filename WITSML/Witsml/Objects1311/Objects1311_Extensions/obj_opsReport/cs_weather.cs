 

using System;
using System.Collections.Generic;
using System.Text;
using System.Xml;

namespace Witsml.Objects1311
{
    public partial class cs_weather : IHandleXML, IHasID
    {
        //this will need to be ignored
        [System.Xml.Serialization.XmlIgnore]
        public string UID
        {
            get { return this.uid; }
        }
        /// <summary>
        /// Convert XML string data into cs_weather fields value
        /// </summary>
        /// <param name="xmlReader">reader that has cs_weather node</param>
        /// <param name="IsNew">Is the object from stratch(true) or from db/serialization(false)</param>
        public void HandleXML(XmlReader xmlReader, bool isNew)
        {
            using (xmlReader)
            {
                xmlReader.MoveToContent();
                if (xmlReader.HasAttributes)
                {
                    if (xmlReader.MoveToAttribute("uid"))
                        StaticHelper.HandleUID(xmlReader.Value, "weather uid", out this.uid);
                    xmlReader.MoveToElement();
                }
                while (xmlReader.Read())
                {
                    if (xmlReader.Depth == 1 && xmlReader.IsStartElement() && !xmlReader.IsEmptyElement)
                    {
                        switch (xmlReader.Name)
                        {
                            case "dTim":
                                this.dTim = StaticParser.ParseDateTimeFromString(StaticHelper.ReadString(xmlReader));
                                break;
                            case "agency":
                                this.agency = StaticHelper.ReadString(xmlReader);
                                break;
                            case "barometricPressure":
                                if (this.barometricPressure == null)
                                    this.barometricPressure = new pressureMeasure();
                                this.barometricPressure.HandleXML(xmlReader.ReadSubtree(), isNew);
                                break;
                            case "beaufortScaleNumber":
                                this.beaufortScaleNumber = StaticParser.ParseShortFromString(StaticHelper.ReadString(xmlReader));
                                this.beaufortScaleNumberSpecified = true;
                                break;
                            case "tempSurfaceMn":
                                if (this.tempSurfaceMn == null)
                                    this.tempSurfaceMn = new thermodynamicTemperatureMeasure();
                                this.tempSurfaceMn.HandleXML(xmlReader.ReadSubtree(), isNew);
                                break;
                            case "tempSurfaceMx":
                                if (this.tempSurfaceMx == null)
                                    this.tempSurfaceMx = new thermodynamicTemperatureMeasure();
                                this.tempSurfaceMx.HandleXML(xmlReader.ReadSubtree(), isNew);
                                break;
                            case "tempWindChill":
                                if (this.tempWindChill == null)
                                    this.tempWindChill = new thermodynamicTemperatureMeasure();
                                this.tempWindChill.HandleXML(xmlReader.ReadSubtree(), isNew);
                                break;
                            case "tempsea":
                                if (this.tempsea == null)
                                    this.tempsea = new thermodynamicTemperatureMeasure();
                                this.tempsea.HandleXML(xmlReader.ReadSubtree(), isNew);
                                break;
                            case "visibility":
                                if (this.visibility == null)
                                    this.visibility = new lengthMeasure();
                                this.visibility.HandleXML(xmlReader.ReadSubtree(), isNew);
                                break;
                            case "aziWave":
                                if (this.aziWave == null)
                                    this.aziWave = new planeAngleMeasure();
                                this.aziWave.HandleXML(xmlReader.ReadSubtree(), isNew);
                                break;
                            case "htWave":
                                if (this.htWave == null)
                                    this.htWave = new lengthMeasure();
                                this.htWave.HandleXML(xmlReader.ReadSubtree(), isNew);
                                break;
                            case "significantWave":
                                if (this.significantWave == null)
                                    this.significantWave = new lengthMeasure();
                                this.significantWave.HandleXML(xmlReader.ReadSubtree(), isNew);
                                break;
                            case "maxWave":
                                if (this.maxWave == null)
                                    this.maxWave = new lengthMeasure();
                                this.maxWave.HandleXML(xmlReader.ReadSubtree(), isNew);
                                break;
                            case "periodWave":
                                if (this.periodWave == null)
                                    this.periodWave = new timeMeasure();
                                this.periodWave.HandleXML(xmlReader.ReadSubtree(), isNew);
                                break;
                            case "aziWind":
                                if (this.aziWind == null)
                                    this.aziWind = new planeAngleMeasure();
                                this.aziWind.HandleXML(xmlReader.ReadSubtree(), isNew);
                                break;
                            case "velWind":
                                if (this.velWind == null)
                                    this.velWind = new velocityMeasure();
                                this.velWind.HandleXML(xmlReader.ReadSubtree(), isNew);
                                break;
                            case "typePrecip":
                                this.typePrecip = StaticHelper.ReadString(xmlReader);
                                break;
                            case "amtPrecip":
                                if (this.amtPrecip == null)
                                    this.amtPrecip = new lengthMeasure();
                                this.amtPrecip.HandleXML(xmlReader.ReadSubtree(), isNew);
                                break;
                            case "coverCloud":
                                this.coverCloud = StaticHelper.ReadString(xmlReader);
                                break;
                            case "ceilingCloud":
                                if (this.ceilingCloud == null)
                                    this.ceilingCloud = new lengthMeasure();
                                this.ceilingCloud.HandleXML(xmlReader.ReadSubtree(), isNew);
                                break;
                            case "currentSea":
                                if (this.currentSea == null)
                                    this.currentSea = new velocityMeasure();
                                this.currentSea.HandleXML(xmlReader.ReadSubtree(), isNew);
                                break;
                            case "aziCurrentSea":
                                if (this.aziCurrentSea == null)
                                    this.aziCurrentSea = new planeAngleMeasure();
                                this.aziCurrentSea.HandleXML(xmlReader.ReadSubtree(), isNew);
                                break;
                            case "comments":
                                this.comments = StaticHelper.ReadString(xmlReader);
                                break;
                            default:
                                break;
                        }
                    }
                }
            }
        }
    }
}
