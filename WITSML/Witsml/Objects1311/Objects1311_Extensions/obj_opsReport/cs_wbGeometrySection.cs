 

using System;
using System.Collections.Generic;
using System.Text;
using System.Xml;

namespace Witsml.Objects1311
{
    public partial class cs_wbGeometrySection : IHandleXML, IHasID
    {
        //this will need to be ignored
        [System.Xml.Serialization.XmlIgnore]
        public string UID
        {
            get { return this.uid; }
        }
        public cs_wbGeometrySection()
        {
            this.typeHoleCasing = HoleCasingType.unknown;
        }
        /// <summary>
        /// Convert XML string data into cs_wbGeometrySection fields value
        /// </summary>
        /// <param name="xmlReader">reader that has cs_wbGeometrySection node</param>
        /// <param name="IsNew">Is the object from stratch(true) or from db/serialization(false)</param>
        public void HandleXML(XmlReader xmlReader, bool isNew)
        {
            using (xmlReader)
            {
                xmlReader.MoveToContent();
                if (xmlReader.HasAttributes)
                {
                    if (xmlReader.MoveToAttribute("uid"))
                        StaticHelper.HandleUID(xmlReader.Value, "wbGeometrySection uid", out this.uid);
                    xmlReader.MoveToElement();
                }
                while (xmlReader.Read())
                {
                    if (xmlReader.Depth == 1 && xmlReader.IsStartElement() && !xmlReader.IsEmptyElement)
                    {
                        switch (xmlReader.Name)
                        {
                            case "typeHoleCasing":
                                StaticParser.SetEnumFromString<HoleCasingType>(StaticHelper.ReadString(xmlReader), out this.typeHoleCasing, out this.typeHoleCasingSpecified);
                                break;
                            case "mdTop":
                                if (this.mdTop == null)
                                    this.mdTop = new measuredDepthCoord();
                                this.mdTop.HandleXML(xmlReader.ReadSubtree(), isNew);
                                break;
                            case "mdBottom":
                                if (this.mdBottom == null)
                                    this.mdBottom = new measuredDepthCoord();
                                this.mdBottom.HandleXML(xmlReader.ReadSubtree(), isNew);
                                break;
                            case "tvdTop":
                                if (this.tvdTop == null)
                                    this.tvdTop = new wellVerticalDepthCoord();
                                this.tvdTop.HandleXML(xmlReader.ReadSubtree(), isNew);
                                break;
                            case "tvdBottom":
                                if (this.tvdBottom == null)
                                    this.tvdBottom = new wellVerticalDepthCoord();
                                this.tvdBottom.HandleXML(xmlReader.ReadSubtree(), isNew);
                                break;
                            case "idSection":
                                if (this.idSection == null)
                                    this.idSection = new lengthMeasure();
                                this.idSection.HandleXML(xmlReader.ReadSubtree(), isNew);
                                break;
                            case "odSection":
                                if (this.odSection == null)
                                    this.odSection = new lengthMeasure();
                                this.odSection.HandleXML(xmlReader.ReadSubtree(), isNew);
                                break;
                            case "wtPerLen":
                                //object
                                if (this.wtPerLen == null)
                                    this.wtPerLen = new massPerLengthMeasure();
                                this.wtPerLen.HandleXML(xmlReader.ReadSubtree(), isNew);
                                break;
                            case "grade":
                                this.grade = StaticHelper.ReadString(xmlReader);
                                break;
                            case "curveConductor":
                                this.curveConductor = StaticParser.ParseBooleanFromString(StaticHelper.ReadString(xmlReader));
                                this.curveConductorSpecified = true;
                                break;
                            case "diaDrift":
                                if (this.diaDrift == null)
                                    this.diaDrift = new lengthMeasure();
                                this.diaDrift.HandleXML(xmlReader.ReadSubtree(), isNew);
                                break;
                            case "factFric":
                                this.factFric = StaticParser.ParseDoubleFromString(StaticHelper.ReadString(xmlReader));
                                this.factFricSpecified = true;
                                break;
                            default:
                                break;
                        }
                    }
                }
            }
        }
    }
}
