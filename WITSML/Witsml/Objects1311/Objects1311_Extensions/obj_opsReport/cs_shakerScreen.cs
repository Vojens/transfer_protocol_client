 

using System;
using System.Collections.Generic;
using System.Text;
using System.Xml;

namespace Witsml.Objects1311
{
    public partial class cs_shakerScreen : IHandleXML
    {
        /// <summary>
        /// Convert XML string data into cs_shakerScreen fields value
        /// </summary>
        /// <param name="xmlReader">reader that has cs_shakerScreen node</param>
        /// <param name="IsNew">Is the object from stratch(true) or from db/serialization(false)</param>
        public void HandleXML(XmlReader xmlReader, bool isNew)
        {
            using (xmlReader)
            {
                while (xmlReader.Read())
                {
                    if (xmlReader.Depth == 1 && xmlReader.IsStartElement() && !xmlReader.IsEmptyElement)
                    {
                        switch (xmlReader.Name)
                        {
                            case "dTimStart":
                                this.dTimStart = StaticParser.ParseDateTimeFromString(StaticHelper.ReadString(xmlReader));
                                this.dTimStartSpecified = true;
                                break;
                            case "dTimEnd":
                                this.dTimEnd = StaticParser.ParseDateTimeFromString(StaticHelper.ReadString(xmlReader));
                                this.dTimEndSpecified = true;
                                break;
                            case "numDeck":
                                this.numDeck = StaticParser.ParseShortFromString(StaticHelper.ReadString(xmlReader));
                                this.numDeckSpecified = true;
                                break;
                            case "meshX":
                                if (this.meshX == null)
                                    this.meshX = new lengthMeasure();
                                this.meshX.HandleXML(xmlReader.ReadSubtree(), isNew);
                                break;
                            case "meshY":
                                if (this.meshY == null)
                                    this.meshY = new lengthMeasure();
                                this.meshY.HandleXML(xmlReader.ReadSubtree(), isNew);
                                break;
                            case "manufacturer":
                                this.manufacturer = StaticHelper.ReadString(xmlReader);
                                break;
                            case "model":
                                this.model = StaticHelper.ReadString(xmlReader);
                                break;
                            case "cutPoint":
                                if (this.cutPoint == null)
                                    this.cutPoint = new lengthMeasure();
                                this.cutPoint.HandleXML(xmlReader.ReadSubtree(), isNew);
                                break;
                            default:
                                break;
                        }
                    }
                }
            }
        }
    }
}
