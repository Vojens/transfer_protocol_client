 

using System;
using System.Collections.Generic;
using System.IO;
using System.Text;
using System.Xml;
using Witsml.Common.Exceptions;
using Witsml.Exceptions;

namespace Witsml.Objects1311
{
    [System.Xml.Serialization.XmlRootAttribute("opsReport", Namespace = "http://www.witsml.org/schemas/131", IsNullable = false)]
    public partial class obj_opsReport : WITSMLObject, IHandleXML
    {
        /// <summary>
        /// Decode XML string into rig
        /// </summary>
        /// <param name="xmlIn">XML string</param>
        public override void Decode(ref string xmlIn)
        {
            try
            {
                //Consume XML string and convert node value to variable based on node name
                using (StringReader stringReader = new StringReader(xmlIn))
                {
                    XmlReaderSettings setting = new XmlReaderSettings();
                    setting.ConformanceLevel = ConformanceLevel.Document;
                    setting.IgnoreComments = true;
                    setting.IgnoreWhitespace = true;
                    setting.IgnoreProcessingInstructions = true;
                    using (XmlReader xmlReader = XmlReader.Create(stringReader, setting))
                    {
                        //determine add or update operation
                        bool isNew;
                        if (this.uid == string.Empty)
                            isNew = true;
                        else isNew = false;

                        this.HandleXML(xmlReader, isNew);
                    }
                }
            }
            catch (XmlException ex)
            {
                throw new Exceptions.UnableToDecodeException(ex.Message, WITSMLReturnCode.WITSMLDocumentError, ex.InnerException);
            }
            catch (NullReferenceException ex)
            {
                throw new Exceptions.UnableToDecodeException(ex.Message, WITSMLReturnCode.WITSMLEmptyDocument, ex.InnerException);
            }
            catch (Exceptions.UnableToDecodeException ex)
            {
                throw ex;
            }
            catch (Exception ex)
            {
                throw new Exceptions.UnableToDecodeException(ex.Message, WITSMLReturnCode.WITSMLUnkownCause, ex.InnerException);
            }
        }

        /// <summary>
        /// Convert XML string data into formationMarker fields value
        /// </summary>
        /// <param name="xmlReader">reader that has formationMarker node</param>
        /// <param name="IsNew">Is the object from stratch(true) or from db/serialization(false)</param>
        public void HandleXML(XmlReader xmlReader, bool isNew)
        {
            //condition for add update of collection
            bool isActivityNew = this.activity == null;
            bool isDrillingParamNew = this.drillingParams == null;
            bool isDayCostNew = this.dayCost == null;
            bool isTrajectoryStatNew = this.trajectoryStation == null;
            bool isFluidNew = this.fluid == null;
            bool isScrNew = this.scr == null;
            bool isPitVolumeNew = this.pitVolume == null;
            bool isMudInvNew = this.mudInventory == null;
            bool isBulkNew = this.bulk == null;
            bool isPumpOpNew = this.pumpOp == null;
            bool isShakerOpNew = this.shakerOp == null;
            bool isPersonnelNew = this.personnel == null;
            bool isSupportCraftNew = this.supportCraft == null;
            bool isWeatherNew = this.weather == null;

            //temporary list
            List<cs_activity> listActivity = isActivityNew ? new List<cs_activity>() : new List<cs_activity>(this.activity);
            List<cs_drillingParams> listDrillingParams = isDrillingParamNew ? new List<cs_drillingParams>() : new List<cs_drillingParams>(this.drillingParams);
            List<cs_dayCost> listDayCost = isDayCostNew ? new List<cs_dayCost>() : new List<cs_dayCost>(this.dayCost);
            List<cs_trajectoryStation> listTrajStat = isTrajectoryStatNew ? new List<cs_trajectoryStation>() : new List<cs_trajectoryStation>(this.trajectoryStation);
            List<cs_fluid> listFluid = isFluidNew ? new List<cs_fluid>() : new List<cs_fluid>(this.fluid);
            List<cs_scr> listScr = isScrNew ? new List<cs_scr>() : new List<cs_scr>(this.scr);
            List<cs_pitVolume> listPitVolume = isPitVolumeNew ? new List<cs_pitVolume>() : new List<cs_pitVolume>(this.pitVolume);
            List<cs_inventory> listMudInventory = isMudInvNew ? new List<cs_inventory>() : new List<cs_inventory>(this.mudInventory);
            List<cs_inventory> listBulk = isBulkNew ? new List<cs_inventory>() : new List<cs_inventory>(this.bulk);
            List<cs_pumpOp> listPumpOp = isPumpOpNew ? new List<cs_pumpOp>() : new List<cs_pumpOp>(this.pumpOp);
            List<cs_shakerOp> listShakerOp = isShakerOpNew ? new List<cs_shakerOp>() : new List<cs_shakerOp>(this.shakerOp);
            List<cs_personnel> listPersonnel = isPersonnelNew ? new List<cs_personnel>() : new List<cs_personnel>(this.personnel);
            List<cs_supportCraft> listSupportCraft = isSupportCraftNew ? new List<cs_supportCraft>() : new List<cs_supportCraft>(this.supportCraft);
            List<cs_weather> listWeather = isWeatherNew ? new List<cs_weather>() : new List<cs_weather>(this.weather);

            using (xmlReader)
            {
                //assign uid for the new well
                xmlReader.MoveToContent();
                if (xmlReader.HasAttributes)
                {
                    if (xmlReader.MoveToAttribute("uidWell"))
                        StaticHelper.HandleUID(xmlReader.Value, "opsReport uidWell", out this.uidWell);
                    if (xmlReader.MoveToAttribute("uidWellbore"))
                        StaticHelper.HandleUID(xmlReader.Value, "opsReport uidWellbore", out this.uidWellbore);
                    if (xmlReader.MoveToAttribute("uid"))
                        StaticHelper.HandleUID(xmlReader.Value, "opsReport uid", out this.uid);
                    xmlReader.MoveToElement();
                }
                while (xmlReader.Read())
                {
                    //only take child node
                    if (xmlReader.Depth == 1 && xmlReader.IsStartElement() && !xmlReader.IsEmptyElement)
                    {
                        switch (xmlReader.Name)
                        {
                            case "nameWell":
                                this.nameWell = StaticHelper.ReadString(xmlReader);
                                break;
                            case "nameWellbore":
                                this.nameWellbore = StaticHelper.ReadString(xmlReader);
                                break;
                            case "name":
                                this.name = StaticHelper.ReadString(xmlReader);
                                break;
                            case "rig":
                                if (this.rig == null)
                                    this.rig = new refNameString();
                                this.rig.ObjectName = "rig";
                                this.rig.HandleXML(xmlReader.ReadSubtree(), isNew);
                                break;
                            case "dTim":
                                this.dTim = StaticParser.ParseDateTimeFromString(StaticHelper.ReadString(xmlReader));
                                break;
                            case "eTimStart":
                                if (this.eTimStart == null)
                                    this.eTimStart = new timeMeasure();
                                this.eTimStart.HandleXML(xmlReader.ReadSubtree(), isNew);
                                break;
                            case "eTimSpud":
                                if (this.eTimSpud == null)
                                    this.eTimSpud = new timeMeasure();
                                this.eTimSpud.HandleXML(xmlReader.ReadSubtree(), isNew);
                                break;
                            case "eTimLoc":
                                if (this.eTimLoc == null)
                                    this.eTimLoc = new timeMeasure();
                                this.eTimLoc.HandleXML(xmlReader.ReadSubtree(), isNew);
                                break;
                            case "mdReport":
                                if (this.mdReport == null)
                                    this.mdReport = new measuredDepthCoord();
                                this.mdReport.HandleXML(xmlReader.ReadSubtree(), isNew);
                                break;
                            case "tvdReport":
                                if (this.tvdReport == null)
                                    this.tvdReport = new wellVerticalDepthCoord();
                                this.tvdReport.HandleXML(xmlReader.ReadSubtree(), isNew);
                                break;
                            case "distDrill":
                                if (this.distDrill == null)
                                    this.distDrill = new lengthMeasure();
                                this.distDrill.HandleXML(xmlReader.ReadSubtree(), isNew);
                                break;
                            case "eTimDrill":
                                if (this.eTimDrill == null)
                                    this.eTimDrill = new timeMeasure();
                                this.eTimDrill.HandleXML(xmlReader.ReadSubtree(), isNew);
                                break;
                            case "mdPlanned":
                                if (this.mdPlanned == null)
                                    this.mdPlanned = new measuredDepthCoord();
                                this.mdPlanned.HandleXML(xmlReader.ReadSubtree(), isNew);
                                break;
                            case "ropAv":
                                if (this.ropAv == null)
                                    this.ropAv = new velocityMeasure();
                                this.ropAv.HandleXML(xmlReader.ReadSubtree(), isNew);
                                break;
                            case "ropCurrent":
                                if (this.ropCurrent == null)
                                    this.ropCurrent = new velocityMeasure();
                                this.ropCurrent.HandleXML(xmlReader.ReadSubtree(), isNew);
                                break;
                            case "supervisor":
                                this.supervisor = StaticHelper.ReadString(xmlReader);
                                break;
                            case "engineer":
                                this.engineer = StaticHelper.ReadString(xmlReader);
                                break;
                            case "geologist":
                                this.geologist = StaticHelper.ReadString(xmlReader);
                                break;
                            case "eTimDrillRot":
                                if (this.eTimDrillRot == null)
                                    this.eTimDrillRot = new timeMeasure();
                                this.eTimDrillRot.HandleXML(xmlReader.ReadSubtree(), isNew);
                                break;
                            case "eTimDrillSlid":
                                if (this.eTimDrillSlid == null)
                                    this.eTimDrillSlid = new timeMeasure();
                                this.eTimDrillSlid.HandleXML(xmlReader.ReadSubtree(), isNew);
                                break;
                            case "eTimCirc":
                                if (this.eTimCirc == null)
                                    this.eTimCirc = new timeMeasure();
                                this.eTimCirc.HandleXML(xmlReader.ReadSubtree(), isNew);
                                break;
                            case "eTimReam":
                                if (this.eTimReam == null)
                                    this.eTimReam = new timeMeasure();
                                this.eTimReam.HandleXML(xmlReader.ReadSubtree(), isNew);
                                break;
                            case "eTimHold":
                                if (this.eTimHold == null)
                                    this.eTimHold = new timeMeasure();
                                this.eTimHold.HandleXML(xmlReader.ReadSubtree(), isNew);
                                break;
                            case "eTimSteering":
                                if (this.eTimSteering == null)
                                    this.eTimSteering = new timeMeasure();
                                this.eTimSteering.HandleXML(xmlReader.ReadSubtree(), isNew);
                                break;
                            case "distDrillRot":
                                if (this.distDrillRot == null)
                                    this.distDrillRot = new lengthMeasure();
                                this.distDrillRot.HandleXML(xmlReader.ReadSubtree(), isNew);
                                break;
                            case "distDrillSlid":
                                if (this.distDrillSlid == null)
                                    this.distDrillSlid = new lengthMeasure();
                                this.distDrillSlid.HandleXML(xmlReader.ReadSubtree(), isNew);
                                break;
                            case "distReam":
                                if (this.distReam == null)
                                    this.distReam = new lengthMeasure();
                                this.distReam.HandleXML(xmlReader.ReadSubtree(), isNew);
                                break;
                            case "distHold":
                                if (this.distHold == null)
                                    this.distHold = new lengthMeasure();
                                this.distHold.HandleXML(xmlReader.ReadSubtree(), isNew);
                                break;
                            case "distSteering":
                                if (this.distSteering == null)
                                    this.distSteering = new lengthMeasure();
                                this.distSteering.HandleXML(xmlReader.ReadSubtree(), isNew);
                                break;
                            case "numPob":
                                this.numPob = StaticParser.ParseShortFromString(StaticHelper.ReadString(xmlReader));
                                this.numPobSpecified = true;
                                break;
                            case "numContract":
                                this.numContract = StaticParser.ParseShortFromString(StaticHelper.ReadString(xmlReader));
                                this.numContractSpecified = true;
                                break;
                            case "numOperator":
                                this.numOperator = StaticParser.ParseShortFromString(StaticHelper.ReadString(xmlReader));
                                this.numOperatorSpecified = true;
                                break;
                            case "numService":
                                this.numService = StaticParser.ParseShortFromString(StaticHelper.ReadString(xmlReader));
                                this.numServiceSpecified = true;
                                break;
                            case "activity":
                                //collection
                                StaticHelper.AddUpdateWithUid<cs_activity>(xmlReader, isActivityNew, ref listActivity);
                                break;
                            case "drillingParams":
                                //collection
                                StaticHelper.AddUpdateWithUid<cs_drillingParams>(xmlReader, isDrillingParamNew, ref listDrillingParams);
                                break;
                            case "wbGeometry":
                                //object
                                if (this.wbGeometry == null)
                                    this.wbGeometry = new cs_wbGeometry();
                                this.wbGeometry.HandleXML(xmlReader.ReadSubtree(), isNew);
                                break;
                            case "dayCost":
                                //collection
                                StaticHelper.AddUpdateWithUid<cs_dayCost>(xmlReader, isDayCostNew, ref listDayCost);
                                break;
                            case "trajectoryStation":
                                //collection
                                StaticHelper.AddUpdateWithUid<cs_trajectoryStation>(xmlReader, isTrajectoryStatNew, ref listTrajStat);
                                break;
                            case "fluid":
                                //collection
                                StaticHelper.AddUpdateWithUid<cs_fluid>(xmlReader, isFluidNew, ref listFluid);
                                break;
                            case "scr":
                                //collection
                                StaticHelper.AddUpdateWithUid<cs_scr>(xmlReader, isScrNew, ref listScr);
                                break;
                            case "pitVolume":
                                //collection
                                StaticHelper.AddUpdateWithUid<cs_pitVolume>(xmlReader, isPitVolumeNew, ref listPitVolume);
                                break;
                            case "mudVolume":
                                //object
                                if (this.mudVolume == null)
                                    this.mudVolume = new cs_mudVolume();
                                this.mudVolume.HandleXML(xmlReader.ReadSubtree(), isNew);
                                break;
                            case "mudInventory":
                                //collection
                                StaticHelper.AddUpdateWithUid<cs_inventory>(xmlReader, isMudInvNew, ref listMudInventory);
                                break;
                            case "bulk":
                                //collection
                                StaticHelper.AddUpdateWithUid<cs_inventory>(xmlReader, isBulkNew, ref listBulk);
                                break;
                            case "rigResponse":
                                //object
                                if (this.rigResponse == null)
                                    this.rigResponse = new cs_rigResponse();
                                this.rigResponse.HandleXML(xmlReader.ReadSubtree(), isNew);
                                break;
                            case "pumpOp":
                                //collection
                                StaticHelper.AddUpdateWithUid<cs_pumpOp>(xmlReader, isPumpOpNew, ref listPumpOp);
                                break;
                            case "shakerOp":
                                //collection
                                StaticHelper.AddUpdateWithUid<cs_shakerOp>(xmlReader, isShakerOpNew, ref listShakerOp);
                                break;
                            case "hse":
                                //object
                                if (this.hse == null)
                                    this.hse = new cs_hse();
                                this.hse.HandleXML(xmlReader.ReadSubtree(), isNew);
                                break;
                            case "personnel":
                                //collection
                                StaticHelper.AddUpdateWithUid<cs_personnel>(xmlReader, isPersonnelNew, ref listPersonnel);
                                break;
                            case "supportCraft":
                                //collection
                                StaticHelper.AddUpdateWithUid<cs_supportCraft>(xmlReader, isSupportCraftNew, ref listSupportCraft);
                                break;
                            case "weather":
                                //collection
                                StaticHelper.AddUpdateWithUid<cs_weather>(xmlReader, isWeatherNew, ref listWeather);
                                break;
                            case "numAFE":
                                this.numAFE = StaticHelper.ReadString(xmlReader);
                                break;
                            case "costDay":
                                if (this.costDay == null)
                                    this.costDay = new cost();
                                this.costDay.HandleXML(xmlReader.ReadSubtree(), isNew);
                                break;
                            case "costDayMud":
                                if (this.costDayMud == null)
                                    this.costDayMud = new cost();
                                this.costDayMud.HandleXML(xmlReader.ReadSubtree(), isNew);
                                break;
                            case "diaHole":
                                if (this.diaHole == null)
                                    this.diaHole = new lengthMeasure();
                                this.diaHole.HandleXML(xmlReader.ReadSubtree(), isNew);
                                break;
                            case "conditionHole":
                                this.conditionHole = StaticHelper.ReadString(xmlReader);
                                break;
                            case "lithology":
                                this.lithology = StaticHelper.ReadString(xmlReader);
                                break;
                            case "nameFormation":
                                this.nameFormation = StaticHelper.ReadString(xmlReader);
                                break;
                            case "diaCsgLast":
                                if (this.diaCsgLast == null)
                                    this.diaCsgLast = new lengthMeasure();
                                this.diaCsgLast.HandleXML(xmlReader.ReadSubtree(), isNew);
                                break;
                            case "mdCsgLast":
                                if (this.mdCsgLast == null)
                                    this.mdCsgLast = new measuredDepthCoord();
                                this.mdCsgLast.HandleXML(xmlReader.ReadSubtree(), isNew);
                                break;
                            case "tvdCsgLast":
                                if (this.tvdCsgLast == null)
                                    this.tvdCsgLast = new wellVerticalDepthCoord();
                                this.tvdCsgLast.HandleXML(xmlReader.ReadSubtree(), isNew);
                                break;
                            case "tvdLot":
                                if (this.tvdLot == null)
                                    this.tvdLot = new wellVerticalDepthCoord();
                                this.tvdLot.HandleXML(xmlReader.ReadSubtree(), isNew);
                                break;
                            case "presLotEmw":
                                if (this.presLotEmw == null)
                                    this.presLotEmw = new densityMeasure();
                                this.presLotEmw.HandleXML(xmlReader.ReadSubtree(), isNew);
                                break;
                            case "presKickTol":
                                if (this.presKickTol == null)
                                    this.presKickTol = new pressureMeasure();
                                this.presKickTol.HandleXML(xmlReader.ReadSubtree(), isNew);
                                break;
                            case "volKickTol":
                                if (this.volKickTol == null)
                                    this.volKickTol = new volumeMeasure();
                                this.volKickTol.HandleXML(xmlReader.ReadSubtree(), isNew);
                                break;
                            case "maasp":
                                if (this.maasp == null)
                                    this.maasp = new pressureMeasure();
                                this.maasp.HandleXML(xmlReader.ReadSubtree(), isNew);
                                break;
                            case "tubular":
                                if (this.tubular == null)
                                    this.tubular = new refNameString();
                                this.tubular.ObjectName = "tubular";
                                this.tubular.HandleXML(xmlReader.ReadSubtree(), isNew);
                                break;
                            case "sum24Hr":
                                this.sum24Hr = StaticHelper.ReadString(xmlReader);
                                break;
                            case "forecast24Hr":
                                this.forecast24Hr = StaticHelper.ReadString(xmlReader);
                                break;
                            case "statusCurrent":
                                this.statusCurrent = StaticHelper.ReadString(xmlReader);
                                break;
                            case "commonData":
                                if (this.commonData == null)
                                    this.commonData = new cs_commonData();
                                this.commonData.HandleXML(xmlReader.ReadSubtree(), isNew);
                                break;
                            case "customData":
                                if (this.customData == null)
                                    this.customData = new cs_customData();
                                this.customData.HandleXML(xmlReader.ReadSubtree(), isNew);
                                break;
                            default:
                                break;
                        }
                    }
                }
                //assign list to collection
                this.activity = listActivity.ToArray();
                this.drillingParams = listDrillingParams.ToArray();
                this.dayCost = listDayCost.ToArray();
                this.trajectoryStation = listTrajStat.ToArray();
                this.fluid = listFluid.ToArray();
                this.scr = listScr.ToArray();
                this.pitVolume = listPitVolume.ToArray();
                this.mudInventory = listMudInventory.ToArray();
                this.bulk = listBulk.ToArray();
                this.pumpOp = listPumpOp.ToArray();
                this.shakerOp = listShakerOp.ToArray();
                this.personnel = listPersonnel.ToArray();
                this.supportCraft = listSupportCraft.ToArray();
                this.weather = listWeather.ToArray();
            }
        }

        public override string ValidateGetPath(string newUid)
        {
            // Fix uid if the Object dosn't have one.
            if (!IsValidUid(this.uid))
            {
                this.uid = newUid;
                // TODO: Add some loging that we created a new UID for the object maybe.
            }

            StringBuilder path = new StringBuilder(this.GetParentPath());
            path.Append(@"\");
            path.Append(this.uid);
            return path.ToString();
        }

        public override string GetParentPath()
        {
            if (!IsValidUid(this.uidWell) || !IsValidUid(this.uidWellbore))
            {
                throw new UnableToDecodeException("One or more Parent UID's are invalid", WITSMLReturnCode.WITSMLInvalidParentUID);
                //TODO: Log the issue
            }

            StringBuilder path = new StringBuilder(@"\");
            path.Append(this.uidWell);
            path.Append(@"\");
            path.Append(this.uidWellbore);
            return path.ToString();
        }

        // NOTE: these are done with functions, instead of property access, so as not to mess around with the auto-generated / serialized classes.
        public override string Get_uid()
        {
            return this.uid;
        }

        public override string Get_uidWell()
        {
            return this.uidWell;
        }

        public override string Get_uidWellbore()
        {
            return this.uidWellbore;
        }

        public override string Get_name()
        {
            return this.name;
        }

        public override string Get_nameWell()
        {
            return this.nameWell;
        }

        public override string Get_nameWellbore()
        {
            return this.nameWellbore;
        }

        public override ICommonData Get_commonData()
        {
            if (this.commonData == null)
            {
                this.commonData = new cs_commonData();
            }
            return (ICommonData)this.commonData;
        }
        public override ICustomData Get_customData()
        {
            if (this.customData == null)
            {
                this.customData = new cs_customData();
            }
            return (ICustomData)this.customData;
        }
    }
}
