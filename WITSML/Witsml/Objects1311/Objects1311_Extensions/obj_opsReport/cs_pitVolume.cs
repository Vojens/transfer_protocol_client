 

using System;
using System.Collections.Generic;
using System.Text;
using System.Xml;

namespace Witsml.Objects1311
{
    public partial class cs_pitVolume : IHandleXML, IHasID
    {
        //this will need to be ignored
        [System.Xml.Serialization.XmlIgnore]
        public string UID
        {
            get { return this.uid; }
        }
        /// <summary>
        /// Convert XML string data into cs_pitVolume fields value
        /// </summary>
        /// <param name="xmlReader">reader that has cs_pitVolume node</param>
        /// <param name="IsNew">Is the object from stratch(true) or from db/serialization(false)</param>
        public void HandleXML(XmlReader xmlReader, bool isNew)
        {
            using (xmlReader)
            {
                xmlReader.MoveToContent();
                if (xmlReader.HasAttributes)
                {
                    if (xmlReader.MoveToAttribute("uid"))
                        StaticHelper.HandleUID(xmlReader.Value, "pitVolume uid", out this.uid);
                    xmlReader.MoveToElement();
                }
                while (xmlReader.Read())
                {
                    if (xmlReader.Depth == 1 && xmlReader.IsStartElement() && !xmlReader.IsEmptyElement)
                    {
                        switch (xmlReader.Name)
                        {
                            case "pit":
                                if (this.pit == null)
                                    this.pit = new refPositiveCount();
                                this.pit.ObjectName = "pit";
                                this.pit.HandleXML(xmlReader.ReadSubtree(), isNew);
                                break;
                            case "dTim":
                                this.dTim = StaticParser.ParseDateTimeFromString(StaticHelper.ReadString(xmlReader));
                                this.dTimSpecified = true;
                                break;
                            case "volPit":
                                if (this.volPit == null)
                                    this.volPit = new volumeMeasure();
                                this.volPit.HandleXML(xmlReader.ReadSubtree(), isNew);
                                break;
                            case "densFluid":
                                if (this.densFluid == null)
                                    this.densFluid = new densityMeasure();
                                this.densFluid.HandleXML(xmlReader.ReadSubtree(), isNew);
                                break;
                            case "descFluid":
                                this.descFluid = StaticHelper.ReadString(xmlReader);
                                break;
                            case "visFunnel":
                                if (this.visFunnel == null)
                                    this.visFunnel = new timeMeasure();
                                this.visFunnel.HandleXML(xmlReader.ReadSubtree(), isNew);
                                break;
                            default:
                                break;
                        }
                    }
                }
            }
        }
    }
}
