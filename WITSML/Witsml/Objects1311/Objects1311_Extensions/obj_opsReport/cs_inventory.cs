 

using System;
using System.Collections.Generic;
using System.Text;
using System.Xml;

namespace Witsml.Objects1311
{
    public partial class cs_inventory : IHandleXML, IHasID
    {
        //this will need to be ignored
        [System.Xml.Serialization.XmlIgnore]
        public string UID
        {
            get { return this.uid; }
        }
        /// <summary>
        /// Convert XML string data into cs_inventory fields value
        /// </summary>
        /// <param name="xmlReader">reader that has cs_inventory node</param>
        /// <param name="IsNew">Is the object from stratch(true) or from db/serialization(false)</param>
        public void HandleXML(XmlReader xmlReader, bool isNew)
        {
            //condition for choice
            bool hasItem = false;

            using (xmlReader)
            {
                xmlReader.MoveToContent();
                if (xmlReader.HasAttributes)
                {
                    if (xmlReader.MoveToAttribute("uid"))
                        StaticHelper.HandleUID(xmlReader.Value, "inventory uid", out this.uid);
                    xmlReader.MoveToElement();
                }
                while (xmlReader.Read())
                {
                    if (xmlReader.Depth == 1 && xmlReader.IsStartElement() && !xmlReader.IsEmptyElement)
                    {
                        switch (xmlReader.Name)
                        {
                            case "name":
                                this.name = StaticHelper.ReadString(xmlReader);
                                break;
                            case "itemVolPerUnit":
                                if (!hasItem)
                                {
                                    volumeMeasure obj = new volumeMeasure();
                                    obj.HandleXML(xmlReader.ReadSubtree(), isNew);
                                    this.Item = obj;
                                }
                                break;
                            case "itemWtPerUnit":
                                if (!hasItem)
                                {
                                    massMeasure obj = new massMeasure();
                                    obj.HandleXML(xmlReader.ReadSubtree(), isNew);
                                    this.Item = obj;
                                }
                                break;
                            case "pricePerUnit":
                                if (this.pricePerUnit == null)
                                    this.pricePerUnit = new cost();
                                this.pricePerUnit.HandleXML(xmlReader.ReadSubtree(), isNew);
                                break;
                            case "qtyStart":
                                this.qtyStart = StaticParser.ParseShortFromString(StaticHelper.ReadString(xmlReader));
                                this.qtyStartSpecified = true;
                                break;
                            case "qtyAdjustment":
                                this.qtyAdjustment = StaticParser.ParseShortFromString(StaticHelper.ReadString(xmlReader));
                                this.qtyAdjustmentSpecified = true;
                                break;
                            case "qtyReceived":
                                this.qtyReceived = StaticParser.ParseShortFromString(StaticHelper.ReadString(xmlReader));
                                this.qtyReceivedSpecified = true;
                                break;
                            case "qtyReturned":
                                this.qtyReturned = StaticParser.ParseShortFromString(StaticHelper.ReadString(xmlReader));
                                this.qtyReturnedSpecified = true;
                                break;
                            case "qtyUsed":
                                this.qtyUsed = StaticParser.ParseShortFromString(StaticHelper.ReadString(xmlReader));
                                this.qtyUsedSpecified = true;
                                break;
                            case "costItem":
                                if (this.costItem == null)
                                    this.costItem = new cost();
                                this.costItem.HandleXML(xmlReader.ReadSubtree(), isNew);
                                break;
                            case "qtyOnLocation":
                                this.qtyOnLocation = StaticParser.ParseShortFromString(StaticHelper.ReadString(xmlReader));
                                this.qtyOnLocationSpecified = true;
                                break;
                            default:
                                break;
                        }
                    }
                }
            }
        }
    }
}
