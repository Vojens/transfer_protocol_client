 

using System;
using System.Collections.Generic;
using System.Text;
using System.Xml;

namespace Witsml.Objects1311
{
    public partial class cs_pumpOp : IHandleXML, IHasID
    {
        //this will need to be ignored
        [System.Xml.Serialization.XmlIgnore]
        public string UID
        {
            get { return this.uid; }
        }
        public cs_pumpOp()
        {
            this.typeOperation = PumpOpType.unknown;
        }
        /// <summary>
        /// Convert XML string data into cs_pumpOp fields value
        /// </summary>
        /// <param name="xmlReader">reader that has cs_pumpOp node</param>
        /// <param name="IsNew">Is the object from stratch(true) or from db/serialization(false)</param>
        public void HandleXML(XmlReader xmlReader, bool isNew)
        {
            using (xmlReader)
            {
                xmlReader.MoveToContent();
                if (xmlReader.HasAttributes)
                {
                    if (xmlReader.MoveToAttribute("uid"))
                        StaticHelper.HandleUID(xmlReader.Value, "pumpOp uid", out this.uid);
                    xmlReader.MoveToElement();
                }
                while (xmlReader.Read())
                {
                    if (xmlReader.Depth == 1 && xmlReader.IsStartElement() && !xmlReader.IsEmptyElement)
                    {
                        switch (xmlReader.Name)
                        {
                            case "dTim":
                                this.dTim = StaticParser.ParseDateTimeFromString(StaticHelper.ReadString(xmlReader));
                                this.dTimSpecified = true;
                                break;
                            case "pump":
                                if (this.pump == null)
                                    this.pump = new refPositiveCount();
                                this.pump.HandleXML(xmlReader.ReadSubtree(), isNew);
                                break;
                            case "typeOperation":
                                StaticParser.SetEnumFromString<PumpOpType>(StaticHelper.ReadString(xmlReader), out this.typeOperation, out this.typeOperationSpecified);
                                break;
                            case "idLiner":
                                if (this.idLiner == null)
                                    this.idLiner = new lengthMeasure();
                                this.idLiner.HandleXML(xmlReader.ReadSubtree(), isNew);
                                break;
                            case "lenStroke":
                                if (this.lenStroke == null)
                                    this.lenStroke = new lengthMeasure();
                                this.lenStroke.HandleXML(xmlReader.ReadSubtree(), isNew);
                                break;
                            case "rateStroke":
                                if (this.rateStroke == null)
                                    this.rateStroke = new anglePerTimeMeasure();
                                this.rateStroke.HandleXML(xmlReader.ReadSubtree(), isNew);
                                break;
                            case "pressure":
                                if (this.pressure == null)
                                    this.pressure = new pressureMeasure();
                                this.pressure.HandleXML(xmlReader.ReadSubtree(), isNew);
                                break;
                            case "pcEfficiency":
                                if (this.pcEfficiency == null)
                                    this.pcEfficiency = new relativePowerMeasure();
                                this.pcEfficiency.HandleXML(xmlReader.ReadSubtree(), isNew);
                                break;
                            case "pumpOutput":
                                if (this.pumpOutput == null)
                                    this.pumpOutput = new volumeFlowRateMeasure();
                                this.pumpOutput.HandleXML(xmlReader.ReadSubtree(), isNew);
                                break;
                            case "mdBit":
                                if (this.mdBit == null)
                                    this.mdBit = new measuredDepthCoord();
                                this.mdBit.HandleXML(xmlReader.ReadSubtree(), isNew);
                                break;
                            default:
                                break;
                        }
                    }
                }
            }
        }
    }
}
