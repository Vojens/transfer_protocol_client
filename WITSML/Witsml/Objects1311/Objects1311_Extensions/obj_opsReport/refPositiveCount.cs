 

using System;
using System.Xml;

namespace Witsml.Objects1311
{
    public partial class refPositiveCount : IHandleXML
    {
        private string objectName;
        [System.Xml.Serialization.XmlIgnore]
        public string ObjectName
        {
            get { return this.objectName; }
            set { this.objectName = value; }
        }
        /// <summary>
        /// Convert XML string data into refPositiveCount fields value
        /// </summary>
        /// <param name="xmlReader">reader that has refPositiveCount node</param>
        /// <param name="IsNew">Is the object from stratch(true) or from db/serialization(false)</param>
        public void HandleXML(XmlReader xmlReader, bool isNew)
        {
            using (xmlReader)
            {
                xmlReader.Read();
                if (xmlReader.HasAttributes)
                {
                    if (xmlReader.MoveToAttribute("uidRef"))
                        StaticHelper.HandleUID(xmlReader.Value, this.objectName + " uid", out this.uidRef);
                    xmlReader.MoveToElement();
                }
                this.Value = StaticParser.ParseShortFromString(StaticHelper.ReadString(xmlReader));
            }
        }

    }
}
