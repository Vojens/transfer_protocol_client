 

using System;
using System.Collections.Generic;
using System.Text;
using System.Xml;

namespace Witsml.Objects1311
{
    public partial class cs_rheometer : IHandleXML, IHasID
    {
        //this will need to be ignored
        [System.Xml.Serialization.XmlIgnore]
        public string UID
        {
            get { return this.uid; }
        }
        /// <summary>
        /// Convert XML string data into cs_rheometer fields value
        /// </summary>
        /// <param name="xmlReader">reader that has cs_rheometer node</param>
        /// <param name="IsNew">Is the object from stratch(true) or from db/serialization(false)</param>
        public void HandleXML(XmlReader xmlReader, bool isNew)
        {
            using (xmlReader)
            {
                xmlReader.MoveToContent();
                if (xmlReader.HasAttributes)
                {
                    if (xmlReader.MoveToAttribute("uid"))
                        StaticHelper.HandleUID(xmlReader.Value, "rheometer uid", out this.uid);
                    xmlReader.MoveToElement();
                }
                while (xmlReader.Read())
                {
                    if (xmlReader.Depth == 1 && xmlReader.IsStartElement() && !xmlReader.IsEmptyElement)
                    {
                        switch (xmlReader.Name)
                        {
                            case "tempRheom":
                                if (this.tempRheom == null)
                                    this.tempRheom = new thermodynamicTemperatureMeasure();
                                this.tempRheom.HandleXML(xmlReader.ReadSubtree(), isNew);
                                break;
                            case "presRheom":
                                if (this.presRheom == null)
                                    this.presRheom = new pressureMeasure();
                                this.presRheom.HandleXML(xmlReader.ReadSubtree(), isNew);
                                break;
                            case "vis3Rpm":
                                this.vis3Rpm = StaticParser.ParseDoubleFromString(StaticHelper.ReadString(xmlReader));
                                this.vis3RpmSpecified = true;
                                break;
                            case "vis6Rpm":
                                this.vis6Rpm = StaticParser.ParseDoubleFromString(StaticHelper.ReadString(xmlReader));
                                this.vis6RpmSpecified = true;
                                break;
                            case "vis100Rpm":
                                this.vis100Rpm = StaticParser.ParseDoubleFromString(StaticHelper.ReadString(xmlReader));
                                this.vis100RpmSpecified = true;
                                break;
                            case "vis200Rpm":
                                this.vis200Rpm = StaticParser.ParseDoubleFromString(StaticHelper.ReadString(xmlReader));
                                this.vis200RpmSpecified = true;
                                break;
                            case "vis300Rpm":
                                this.vis300Rpm = StaticParser.ParseDoubleFromString(StaticHelper.ReadString(xmlReader));
                                this.vis300RpmSpecified = true;
                                break;
                            case "vis600Rpm":
                                this.vis600Rpm = StaticParser.ParseDoubleFromString(StaticHelper.ReadString(xmlReader));
                                this.vis600RpmSpecified = true;
                                break;
                            default:
                                break;
                        }
                    }
                }
            }
        }
    }
}
