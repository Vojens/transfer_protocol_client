 

using System;
using System.Collections.Generic;
using System.Text;
using System.Xml;

namespace Witsml.Objects1311
{
    public partial class cs_scr : IHandleXML, IHasID
    {
        //this will need to be ignored
        [System.Xml.Serialization.XmlIgnore]
        public string UID
        {
            get { return this.uid; }
        }
        public cs_scr()
        {
            this.typeScr = ScrType.unknown;
        }
        /// <summary>
        /// Convert XML string data into cs_scr fields value
        /// </summary>
        /// <param name="xmlReader">reader that has cs_scr node</param>
        /// <param name="IsNew">Is the object from stratch(true) or from db/serialization(false)</param>
        public void HandleXML(XmlReader xmlReader, bool isNew)
        {
            using (xmlReader)
            {
                xmlReader.MoveToContent();
                if (xmlReader.HasAttributes)
                {
                    if (xmlReader.MoveToAttribute("uid"))
                        StaticHelper.HandleUID(xmlReader.Value, "scr uid", out this.uid);
                    xmlReader.MoveToElement();
                }
                while (xmlReader.Read())
                {
                    if (xmlReader.Depth == 1 && xmlReader.IsStartElement() && !xmlReader.IsEmptyElement)
                    {
                        switch (xmlReader.Name)
                        {
                            case "dTim":
                                this.dTim = StaticParser.ParseDateTimeFromString(StaticHelper.ReadString(xmlReader));
                                break;
                            case "pump":
                                if (this.pump == null)
                                    this.pump = new refPositiveCount();
                                this.pump.ObjectName = "pump";
                                this.pump.HandleXML(xmlReader.ReadSubtree(), isNew);
                                break;
                            case "typeScr":
                                this.typeScr = StaticParser.ParseEnumFromString<ScrType>(StaticHelper.ReadString(xmlReader), ScrType.unknown);
                                break;
                            case "rateStroke":
                                if (this.rateStroke == null)
                                    this.rateStroke = new anglePerTimeMeasure();
                                this.rateStroke.HandleXML(xmlReader.ReadSubtree(), isNew);
                                break;
                            case "presRecorded":
                                if (this.presRecorded == null)
                                    this.presRecorded = new pressureMeasure ();
                                this.presRecorded.HandleXML(xmlReader.ReadSubtree(), isNew);
                                break;
                            case "mdBit":
                                if (this.mdBit == null)
                                    this.mdBit = new measuredDepthCoord();
                                this.mdBit.HandleXML(xmlReader.ReadSubtree(), isNew);
                                break;
                            default:
                                break;
                        }
                    }
                }
            }
        }
    }
}
