 

using System;
using System.Collections.Generic;
using System.Text;
using System.Xml;

namespace Witsml.Objects1311
{
    public partial class cs_incident : IHandleXML, IHasID
    {
        //this will need to be ignored
        [System.Xml.Serialization.XmlIgnore]
        public string UID
        {
            get { return this.uid; }
        }
        /// <summary>
        /// Convert XML string data into cs_incident fields value
        /// </summary>
        /// <param name="xmlReader">reader that has cs_incident node</param>
        /// <param name="IsNew">Is the object from stratch(true) or from db/serialization(false)</param>
        public void HandleXML(XmlReader xmlReader, bool isNew)
        {
            using (xmlReader)
            {
                xmlReader.MoveToContent();
                if (xmlReader.HasAttributes)
                {
                    if (xmlReader.MoveToAttribute("uid"))
                        StaticHelper.HandleUID(xmlReader.Value, "incident uid", out this.uid);
                    xmlReader.MoveToElement();
                }
                while (xmlReader.Read())
                {
                    if (xmlReader.Depth == 1 && xmlReader.IsStartElement() && !xmlReader.IsEmptyElement)
                    {
                        switch (xmlReader.Name)
                        {
                            case "dTim":
                                this.dTim = StaticParser.ParseDateTimeFromString(StaticHelper.ReadString(xmlReader));
                                break;
                            case "reporter":
                                this.reporter = StaticHelper.ReadString(xmlReader);
                                break;
                            case "numMinorInjury":
                                this.numMinorInjury = StaticParser.ParseShortFromString(StaticHelper.ReadString(xmlReader));
                                this.numMinorInjurySpecified = true;
                                break;
                            case "numMajorInjury":
                                this.numMajorInjury = StaticParser.ParseShortFromString(StaticHelper.ReadString(xmlReader));
                                this.numMajorInjurySpecified = true;
                                break;
                            case "numFatality":
                                this.numFatality = StaticParser.ParseShortFromString(StaticHelper.ReadString(xmlReader));
                                this.numFatalitySpecified = true;
                                break;
                            case "isNearMiss":
                                this.isNearMiss = StaticParser.ParseBooleanFromString(StaticHelper.ReadString(xmlReader));
                                this.isNearMissSpecified = true;
                                break;
                            case "descLocation":
                                this.descLocation = StaticHelper.ReadString(xmlReader);
                                break;
                            case "descAccident":
                                this.descAccident = StaticHelper.ReadString(xmlReader);
                                break;
                            case "remedialActionDesc":
                                this.remedialActionDesc = StaticHelper.ReadString(xmlReader);
                                break;
                            case "causeDesc":
                                this.causeDesc = StaticHelper.ReadString(xmlReader);
                                break;
                            case "eTimLostGross":
                                if (this.eTimLostGross == null)
                                    this.eTimLostGross = new timeMeasure();
                                this.eTimLostGross.HandleXML(xmlReader.ReadSubtree(), isNew);
                                break;
                            case "costLostGross":
                                if (this.costLostGross == null)
                                    this.costLostGross = new cost();
                                this.costLostGross.HandleXML(xmlReader.ReadSubtree(), isNew);
                                break;
                            case "responsibleCompany":
                                this.responsibleCompany = StaticHelper.ReadString(xmlReader);
                                break;
                            default:
                                break;
                        }
                    }
                }
            }
        }
    }
}
