 

using System;
using System.Collections.Generic;
using System.Text;
using System.Xml;

namespace Witsml.Objects1311
{
    public partial class cs_wbGeometry : IHandleXML
    {
        /// <summary>
        /// Convert XML string data into cs_wbGeometry fields value
        /// </summary>
        /// <param name="xmlReader">reader that has cs_wbGeometry node</param>
        /// <param name="IsNew">Is the object from stratch(true) or from db/serialization(false)</param>
        public void HandleXML(XmlReader xmlReader, bool isNew)
        {
            //condition for add update collection
            bool isSectionNew = this.wbGeometrySection == null;

            //temporary list for data manipulations
            List<cs_wbGeometrySection> listSection = isSectionNew ? new List<cs_wbGeometrySection>() : new List<cs_wbGeometrySection>(this.wbGeometrySection);

            using (xmlReader)
            {
                while (xmlReader.Read())
                {
                    if (xmlReader.Depth == 1 && xmlReader.IsStartElement() && !xmlReader.IsEmptyElement)
                    {
                        switch (xmlReader.Name)
                        {
                            case "dTimReport":
                                this.dTimReport = StaticParser.ParseDateTimeFromString(StaticHelper.ReadString(xmlReader));
                                break;
                            case "mdBottom":
                                if (this.mdBottom == null)
                                    this.mdBottom = new measuredDepthCoord();
                                this.mdBottom.HandleXML(xmlReader.ReadSubtree(), isNew);
                                break;
                            case "gapAir":
                                if (this.gapAir == null)
                                    this.gapAir = new lengthMeasure();
                                this.gapAir.HandleXML(xmlReader.ReadSubtree(), isNew);
                                break;
                            case "depthWaterMean":
                                if (this.depthWaterMean == null)
                                    this.depthWaterMean = new lengthMeasure();
                                this.depthWaterMean.HandleXML(xmlReader.ReadSubtree(), isNew);
                                break;
                            case "wbGeometrySection":
                                //collection
                                StaticHelper.AddUpdateWithUid<cs_wbGeometrySection>(xmlReader, isSectionNew, ref listSection);
                                break;
                            default:
                                break;
                        }
                    }
                }
                //assign list to collection
                this.wbGeometrySection = listSection.ToArray();
            }
        }
    }
}
