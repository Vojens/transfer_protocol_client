 

using System;
using System.Collections.Generic;
using System.Text;
using System.Xml;

namespace Witsml.Objects1311
{
    public partial class cs_shakerOp : IHandleXML, IHasID
    {
        //this will need to be ignored
        [System.Xml.Serialization.XmlIgnore]
        public string UID
        {
            get { return this.uid; }
        }
        /// <summary>
        /// Convert XML string data into cs_shakerOp fields value
        /// </summary>
        /// <param name="xmlReader">reader that has cs_shakerOp node</param>
        /// <param name="IsNew">Is the object from stratch(true) or from db/serialization(false)</param>
        public void HandleXML(XmlReader xmlReader, bool isNew)
        {
            using (xmlReader)
            {
                xmlReader.MoveToContent();
                if (xmlReader.HasAttributes)
                {
                    if (xmlReader.MoveToAttribute("uid"))
                        StaticHelper.HandleUID(xmlReader.Value, "shakerOp uid", out this.uid);
                    xmlReader.MoveToElement();
                }
                while (xmlReader.Read())
                {
                    if (xmlReader.Depth == 1 && xmlReader.IsStartElement() && !xmlReader.IsEmptyElement)
                    {
                        switch (xmlReader.Name)
                        {
                            case "shaker":
                                if (this.shaker == null)
                                    this.shaker = new refNameString();
                                this.shaker.ObjectName = "shaker";
                                this.shaker.HandleXML(xmlReader.ReadSubtree(), isNew);
                                break;
                            case "mdHole":
                                if (this.mdHole == null)
                                    this.mdHole = new measuredDepthCoord();
                                this.mdHole.HandleXML(xmlReader.ReadSubtree(), isNew);
                                break;
                            case "dTim":
                                this.dTim = StaticParser.ParseDateTimeFromString(StaticHelper.ReadString(xmlReader));
                                this.dTimSpecified = true;
                                break;
                            case "hoursRun":
                                if (this.hoursRun == null)
                                    this.hoursRun = new timeMeasure();
                                this.hoursRun.HandleXML(xmlReader.ReadSubtree(), isNew);
                                break;
                            case "pcScreenCovered":
                                if (this.pcScreenCovered == null)
                                    this.pcScreenCovered = new areaPerAreaMeasure();
                                this.pcScreenCovered.HandleXML(xmlReader.ReadSubtree(), isNew);
                                break;
                            case "shakerScreen":
                                if (this.shakerScreen == null)
                                    this.shakerScreen = new cs_shakerScreen();
                                this.shakerScreen.HandleXML(xmlReader.ReadSubtree(), isNew);
                                break;
                            default:
                                break;
                        }
                    }
                }
            }
        }
    }
}
