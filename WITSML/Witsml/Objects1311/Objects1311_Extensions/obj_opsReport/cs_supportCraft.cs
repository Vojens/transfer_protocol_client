 

using System;
using System.Collections.Generic;
using System.Text;
using System.Xml;

namespace Witsml.Objects1311
{
    public partial class cs_supportCraft : IHandleXML, IHasID
    {
        //this will need to be ignored
        [System.Xml.Serialization.XmlIgnore]
        public string UID
        {
            get { return this.uid; }
        }
        public cs_supportCraft()
        {
            this.typeSuppCraft = SupportCraft.unknown;
        }
        /// <summary>
        /// Convert XML string data into cs_supportCraft fields value
        /// </summary>
        /// <param name="xmlReader">reader that has cs_supportCraft node</param>
        /// <param name="IsNew">Is the object from stratch(true) or from db/serialization(false)</param>
        public void HandleXML(XmlReader xmlReader, bool isNew)
        {
            using (xmlReader)
            {
                xmlReader.MoveToContent();
                if (xmlReader.HasAttributes)
                {
                    if (xmlReader.MoveToAttribute("uid"))
                        StaticHelper.HandleUID(xmlReader.Value, "supportCraft uid", out this.uid);
                    xmlReader.MoveToElement();
                }
                while (xmlReader.Read())
                {
                    if (xmlReader.Depth == 1 && xmlReader.IsStartElement() && !xmlReader.IsEmptyElement)
                    {
                        switch (xmlReader.Name)
                        {
                            case "name":
                                this.name = StaticHelper.ReadString(xmlReader);
                                break;
                            case "typeSuppCraft":
                                StaticParser.SetEnumFromString<SupportCraft>(StaticHelper.ReadString(xmlReader), out this.typeSuppCraft, out this.typeSuppCraftSpecified);
                                break;
                            case "dTimArrived":
                                this.dTimArrived = StaticParser.ParseDateTimeFromString(StaticHelper.ReadString(xmlReader));
                                this.dTimArrivedSpecified = true;
                                break;
                            case "dTimDeparted":
                                this.dTimDeparted = StaticParser.ParseDateTimeFromString(StaticHelper.ReadString(xmlReader));
                                this.dTimDepartedSpecified = true;
                                break;
                            case "comments":
                                this.comments = StaticHelper.ReadString(xmlReader);
                                break;
                            default:
                                break;
                        }
                    }
                }
            }
        }
    }
}
