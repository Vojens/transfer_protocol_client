 

using System;
using System.Collections.Generic;
using System.Text;
using System.Xml;

namespace Witsml.Objects1311
{
    public partial class cs_rigResponse : IHandleXML
    {
        /// <summary>
        /// Convert XML string data into cs_rigResponse fields value
        /// </summary>
        /// <param name="xmlReader">reader that has cs_rigResponse node</param>
        /// <param name="IsNew">Is the object from stratch(true) or from db/serialization(false)</param>
        public void HandleXML(XmlReader xmlReader, bool isNew)
        {
            //condition for add update collection
            bool isAnchorAngleNew = this.anchorAngle == null;
            bool isAnchorTensionNew = this.anchorTension == null;

            //temporary list for data manipulations
            List<indexedObject> listAnchorAngle = isAnchorAngleNew ? new List<indexedObject>() : new List<indexedObject>(this.anchorAngle);
            List<indexedObject> listAnchorTension = isAnchorTensionNew ? new List<indexedObject>() : new List<indexedObject>(this.anchorTension);

            using (xmlReader)
            {
                while (xmlReader.Read())
                {
                    if (xmlReader.Depth == 1 && xmlReader.IsStartElement() && !xmlReader.IsEmptyElement)
                    {
                        switch (xmlReader.Name)
                        {
                            case "anchorTension":
                                //collection
                                StaticHelper.AddUpdateIndexedObj(xmlReader, isAnchorTensionNew, ref listAnchorTension);
                                break;
                            case "anchorAngle":
                                //collection
                                StaticHelper.AddUpdateIndexedObj(xmlReader, isAnchorAngleNew, ref listAnchorAngle);
                                break;
                            case "rigHeading":
                                if (this.rigHeading == null)
                                    this.rigHeading = new planeAngleMeasure();
                                this.rigHeading.HandleXML(xmlReader.ReadSubtree(), isNew);
                                break;
                            case "rigHeave":
                                if (this.rigHeave == null)
                                    this.rigHeave = new lengthMeasure();
                                this.rigHeave.HandleXML(xmlReader.ReadSubtree(), isNew);
                                break;
                            case "rigPitchAngle":
                                if (this.rigPitchAngle == null)
                                    this.rigPitchAngle = new planeAngleMeasure();
                                this.rigPitchAngle.HandleXML(xmlReader.ReadSubtree(), isNew);
                                break;
                            case "rigRollAngle":
                                if (this.rigRollAngle == null)
                                    this.rigRollAngle = new planeAngleMeasure();
                                this.rigRollAngle.HandleXML(xmlReader.ReadSubtree(), isNew);
                                break;
                            case "riserAngle":
                                if (this.riserAngle == null)
                                    this.riserAngle = new planeAngleMeasure();
                                this.riserAngle.HandleXML(xmlReader.ReadSubtree(), isNew);
                                break;
                            case "riserDirection":
                                if (this.riserDirection == null)
                                    this.riserDirection = new planeAngleMeasure();
                                this.riserDirection.HandleXML(xmlReader.ReadSubtree(), isNew);
                                break;
                            case "riserTension":
                                if (this.riserTension == null)
                                    this.riserTension = new forceMeasure();
                                this.riserTension.HandleXML(xmlReader.ReadSubtree(), isNew);
                                break;
                            case "variableDeckLoad":
                                if (this.variableDeckLoad == null)
                                    this.variableDeckLoad = new forceMeasure();
                                this.variableDeckLoad.HandleXML(xmlReader.ReadSubtree(), isNew);
                                break;
                            case "totalDeckLoad":
                                if (this.totalDeckLoad == null)
                                    this.totalDeckLoad = new forceMeasure();
                                this.totalDeckLoad.HandleXML(xmlReader.ReadSubtree(), isNew);
                                break;
                            case "guideBaseAngle":
                                if (this.guideBaseAngle == null)
                                    this.guideBaseAngle = new planeAngleMeasure();
                                this.guideBaseAngle.HandleXML(xmlReader.ReadSubtree(), isNew);
                                break;
                            case "ballJointAngle":
                                if (this.ballJointAngle == null)
                                    this.ballJointAngle = new planeAngleMeasure();
                                this.ballJointAngle.HandleXML(xmlReader.ReadSubtree(), isNew);
                                break;
                            case "ballJointDirection":
                                if (this.ballJointDirection == null)
                                    this.ballJointDirection = new planeAngleMeasure();
                                this.ballJointDirection.HandleXML(xmlReader.ReadSubtree(), isNew);
                                break;
                            case "offsetRig":
                                if (this.offsetRig == null)
                                    this.offsetRig = new lengthMeasure();
                                this.offsetRig.HandleXML(xmlReader.ReadSubtree(), isNew);
                                break;
                            case "loadLeg1":
                                if (this.loadLeg1 == null)
                                    this.loadLeg1 = new forceMeasure();
                                this.loadLeg1.HandleXML(xmlReader.ReadSubtree(), isNew);
                                break;
                            case "loadLeg2":
                                if (this.loadLeg2 == null)
                                    this.loadLeg2 = new forceMeasure();
                                this.loadLeg2.HandleXML(xmlReader.ReadSubtree(), isNew);
                                break;
                            case "loadLeg3":
                                if (this.loadLeg3 == null)
                                    this.loadLeg3 = new forceMeasure();
                                this.loadLeg3.HandleXML(xmlReader.ReadSubtree(), isNew);
                                break;
                            case "loadLeg4":
                                if (this.loadLeg4 == null)
                                    this.loadLeg4 = new forceMeasure();
                                this.loadLeg4.HandleXML(xmlReader.ReadSubtree(), isNew);
                                break;
                            case "penetrationLeg1":
                                if (this.penetrationLeg1 == null)
                                    this.penetrationLeg1 = new lengthMeasure();
                                this.penetrationLeg1.HandleXML(xmlReader.ReadSubtree(), isNew);
                                break;
                            case "penetrationLeg2":
                                if (this.penetrationLeg2 == null)
                                    this.penetrationLeg2 = new lengthMeasure();
                                this.penetrationLeg2.HandleXML(xmlReader.ReadSubtree(), isNew);
                                break;
                            case "penetrationLeg3":
                                if (this.penetrationLeg3 == null)
                                    this.penetrationLeg3 = new lengthMeasure();
                                this.penetrationLeg3.HandleXML(xmlReader.ReadSubtree(), isNew);
                                break;
                            case "penetrationLeg4":
                                if (this.penetrationLeg4 == null)
                                    this.penetrationLeg4 = new lengthMeasure();
                                this.penetrationLeg4.HandleXML(xmlReader.ReadSubtree(), isNew);
                                break;
                            case "dispRig":
                                if (this.dispRig == null)
                                    this.dispRig = new lengthMeasure();
                                this.dispRig.HandleXML(xmlReader.ReadSubtree(), isNew);
                                break;
                            case "meanDraft":
                                if (this.meanDraft == null)
                                    this.meanDraft = new lengthMeasure();
                                this.meanDraft.HandleXML(xmlReader.ReadSubtree(), isNew);
                                break;
                            default:
                                break;
                        }
                    }
                }
                //assign list to collection
                this.anchorAngle = listAnchorAngle.ToArray();
                this.anchorTension = listAnchorTension.ToArray();
            }
        }
    }
}
