 

using System;
using System.Collections.Generic;
using System.IO;
using System.Text;
using System.Xml;
using System.Xml.Linq;
using System.Xml.Serialization;
using System.Linq;
using Witsml.Common.Exceptions;
using Witsml.Exceptions;


namespace Witsml.Objects1311
{
    //TODO logCurveInfo and logData
    [XmlRoot("log", Namespace = "http://www.witsml.org/schemas/131", IsNullable = false)]
    public partial class obj_log : WITSMLObject, IHandleXML
    {
        //field to hold log data
        [System.Xml.Serialization.XmlIgnore]
        public string[][] extensionLogData;

        [System.Xml.Serialization.XmlIgnore]
        public bool startIndexSpecified;

        [System.Xml.Serialization.XmlIgnore]
        public bool endIndexSpecified;

        public obj_log()
        {
            this.indexType = LogIndexType.unknown;
            this.direction = LogIndexDirection.unknown;
            this.startIndex = new genericMeasure();
            this.startIndex.Value = double.NegativeInfinity;
            this.startIndexSpecified = false;
            this.endIndex = new genericMeasure();
            this.endIndex.Value = double.NegativeInfinity;
            this.endIndexSpecified = false;

        }

        /// <summary>
        /// Decode XML string into Log
        /// </summary>
        /// <param name="xmlIn">XML string</param>
        public override void Decode(ref string xmlIn)
        {
            //used to determine add or update
            try
            {
                //Consume XML string and convert node value to variable based on node name
                using (StringReader stringReader = new StringReader(xmlIn))
                {
                    XmlReaderSettings setting = new XmlReaderSettings();
                    setting.ConformanceLevel = ConformanceLevel.Document;
                    setting.IgnoreComments = true;
                    setting.IgnoreWhitespace = true;
                    setting.IgnoreProcessingInstructions = true;
                    using (XmlReader xmlReader = XmlReader.Create(stringReader, setting))
                    {
                        //determine add or update operation
                        bool isNew;
                        if (this.uid == string.Empty)
                            isNew = true;
                        else isNew = false;

                        this.HandleXML(xmlReader, isNew);
                    }
                }
            }
            catch (XmlException ex)
            {
                throw new Exceptions.UnableToDecodeException(ex.Message, WITSMLReturnCode.WITSMLDocumentError, ex.InnerException);
            }
            catch (NullReferenceException ex)
            {
                throw new Exceptions.UnableToDecodeException(ex.Message, WITSMLReturnCode.WITSMLEmptyDocument, ex.InnerException);
            }
            catch (Exceptions.UnableToDecodeException ex)
            {
                throw ex;
            }
            catch (Exception ex)
            {
                throw new Exceptions.UnableToDecodeException(ex.Message, WITSMLReturnCode.WITSMLUnkownCause, ex.InnerException);
            }
        }

        /// <summary>
        /// Convert XML string data into log fields value
        /// </summary>
        /// <param name="xmlReader">reader that has log node</param>
        /// <param name="IsNew">Is the object from stratch(true) or from db/serialization(false)</param>
        public void HandleXML(XmlReader xmlReader, bool isNew)
        {
            //
            bool isColumnIndexExist = false;
            //used to skip read on logCurveInfo handling
            bool hasRead = false;

            //to be used for determining add or update
            bool isLogParamNew = this.logParam == null;
            bool isLogCurveInfoNew = this.logCurveInfo == null;

            //create temporary list for data manipulation
            List<indexedObject> listLogParam = isLogParamNew ? new List<indexedObject>() : new List<indexedObject>(this.logParam);
            List<cs_logCurveInfo> listLogCurveInfo = isLogCurveInfoNew ? new List<cs_logCurveInfo>() : new List<cs_logCurveInfo>(this.logCurveInfo);
            using (xmlReader)
            {
                //assign uid for the new well
                xmlReader.MoveToContent();
                if (xmlReader.HasAttributes)
                {
                    if (xmlReader.MoveToAttribute("uidWell"))
                        StaticHelper.HandleUID(xmlReader.Value, "log uidWell", out this.uidWell);
                    if (xmlReader.MoveToAttribute("uidWellbore"))
                        StaticHelper.HandleUID(xmlReader.Value, "log uidWellbore", out this.uidWellbore);
                    
                    if (xmlReader.MoveToAttribute("uid"))
                    {
                        StaticHelper.HandleUID(xmlReader.Value, "log uid", out this.uid);
                    }
                    
                    xmlReader.MoveToElement();
                }
                while (!xmlReader.EOF)
                {
                    if (!hasRead)
                    {
                        xmlReader.Read();
                    }
                    else
                    {
                        //skip reading on logcurveInfo node
                        hasRead = false;
                    }
                    //only take child node
                    if (xmlReader.Depth == 1 && xmlReader.IsStartElement() && !xmlReader.IsEmptyElement)
                    {
                        switch (xmlReader.Name)
                        {
                            case "nameWell":
                                this.nameWell = StaticHelper.ReadString(xmlReader);
                                break;
                            case "nameWellbore":
                                this.nameWellbore = StaticHelper.ReadString(xmlReader);
                                break;
                            case "name":
                                this.name = StaticHelper.ReadString(xmlReader);
                                break;
                            case "objectGrowing":
                                this.objectGrowing = StaticParser.ParseBooleanFromString(StaticHelper.ReadString(xmlReader));
                                this.objectGrowingSpecified = true;
                                break;
                            case "dataRowCount":
                                this.dataRowCount = StaticParser.ParseIntFromString(StaticHelper.ReadString(xmlReader));
                                this.dataRowCountSpecified = true;
                                break;
                            case "serviceCompany":
                                this.serviceCompany = StaticHelper.ReadString(xmlReader);
                                break;
                            case "runNumber":
                                this.runNumber = StaticHelper.ReadString(xmlReader);
                                break;
                            case "bhaRunNumber":
                                this.bhaRunNumber = StaticParser.ParseShortFromString(StaticHelper.ReadString(xmlReader));
                                this.bhaRunNumberSpecified = true;
                                break;
                            case "pass":
                                this.pass = StaticHelper.ReadString(xmlReader);
                                break;
                            case "creationDate":
                                this.creationDate = StaticParser.ParseDateTimeFromString(StaticHelper.ReadString(xmlReader));
                                this.creationDateSpecified = true;
                                break;
                            case "description":
                                this.description = StaticHelper.ReadString(xmlReader);
                                break;
                            case "indexType":
                                this.indexType = StaticParser.ParseEnumFromString<LogIndexType>(StaticHelper.ReadString(xmlReader), LogIndexType.unknown);
                                break;
                            case "startIndex":
                                //object
                                if (this.startIndex == null)
                                    this.startIndex = new genericMeasure();
                                this.startIndex.HandleXML(xmlReader.ReadSubtree(), isNew);
                                if (!double.IsInfinity(this.startIndex.Value))
                                    this.startIndexSpecified = true;
                                break;
                            case "endIndex":
                                //object
                                if (this.endIndex == null)
                                    this.endIndex = new genericMeasure();
                                this.endIndex.HandleXML(xmlReader.ReadSubtree(), isNew);
                                if (!double.IsInfinity(this.endIndex.Value))
                                    this.endIndexSpecified = true;
                                break;
                            case "stepIncrement":
                                //object
                                if (this.stepIncrement == null)
                                    this.stepIncrement = new ratioGenericMeasure();
                                this.stepIncrement.HandleXML(xmlReader.ReadSubtree(), isNew);
                                break;
                            case "startDateTimeIndex":
                                this.startDateTimeIndex = StaticParser.ParseDateTimeFromString(StaticHelper.ReadString(xmlReader));
                                this.startDateTimeIndexSpecified = true;
                                break;
                            case "endDateTimeIndex":
                                this.endDateTimeIndex = StaticParser.ParseDateTimeFromString(StaticHelper.ReadString(xmlReader)); ;
                                this.endDateTimeIndexSpecified = true;
                                break;
                            case "direction":
                                StaticParser.SetEnumFromString<LogIndexDirection>(StaticHelper.ReadString(xmlReader), out this.direction, out this.directionSpecified);
                                break;
                            case "indexCurve":
                                //object
                                if (this.indexCurve == null)
                                    this.indexCurve = new indexCurve();
                                this.indexCurve.HandleXML(xmlReader.ReadSubtree(), isNew);
                                if (this.indexCurve.columnIndex != 0)
                                {
                                    isColumnIndexExist = true;
                                }
                                break;
                            case "nullValue":
                                this.nullValue = StaticHelper.ReadString(xmlReader);
                                break;
                            case "logParam":
                                //collection
                                StaticHelper.AddUpdateIndexedObj(xmlReader, isLogParamNew, ref listLogParam);
                                break;
                            case "logCurveInfo":
                                //collection
                                cs_logCurveInfo logCurveInfoObj;
                                XElement logCurveElement = XElement.ReadFrom(xmlReader) as XElement;
                                hasRead = true;

                                //Only care about Mnemonic and columnIndex,which handling is on object level
                                string mnemonicValue = string.Empty;
                                XNamespace wits = xmlReader.NamespaceURI;
                                if (logCurveElement.HasElements && logCurveElement.Element(wits + "mnemonic") != null)
                                {
                                    mnemonicValue = logCurveElement.Element(wits + "mnemonic").Value;
                                }

                                //look for existing object
                                logCurveInfoObj = listLogCurveInfo.FirstOrDefault(
                                        delegate(cs_logCurveInfo objLogCurve)
                                        { if (objLogCurve.mnemonic == mnemonicValue)return true; else return false; });

                                //collection exists
                                if (!isLogCurveInfoNew)
                                {
                                    //UpdateToStore

                                    //update if there is existing object with the same mnemonic
                                    if (logCurveInfoObj != null)
                                    {
                                        //update
                                        logCurveInfoObj.objectState = ObjectState.Update;
                                        logCurveInfoObj.HandleXML(logCurveElement.CreateReader(), isLogCurveInfoNew);
                                    }
                                    else
                                    {
                                        //new
                                        logCurveInfoObj = new cs_logCurveInfo();
                                        logCurveInfoObj.HandleXML(logCurveElement.CreateReader(), isLogCurveInfoNew);
                                        logCurveInfoObj.objectState = ObjectState.New;
                                        listLogCurveInfo.Add(logCurveInfoObj);
                                    }
                                }
                                else
                                {
                                    //AddToStore and GetFromStore

                                    //check whether mnemonic is duplicate first,and then only process xml handling
                                    if (logCurveInfoObj != null)
                                    {
                                        throw new Exceptions.UnableToDecodeException("Duplicate Mnemonic on Mnemonic =" + mnemonicValue, WITSMLReturnCode.WITSMLMnemonicExists, true);
                                    }

                                    //Add
                                    logCurveInfoObj = new cs_logCurveInfo();
                                    logCurveInfoObj.HandleXML(logCurveElement.CreateReader(), isLogCurveInfoNew);
                                    logCurveInfoObj.objectState = ObjectState.Normal;
                                    listLogCurveInfo.Add(logCurveInfoObj);

                                }
                                //Fill indexCurve columnIndex if it is empty
                                if (!isColumnIndexExist)
                                {
                                    if (this.indexCurve != null)
                                    {
                                        if (this.indexCurve.Value == logCurveInfoObj.mnemonic)
                                        {
                                            this.indexCurve.columnIndex = logCurveInfoObj.columnIndex;
                                        }
                                    }
                                    isColumnIndexExist = true;
                                }

                                //check if unknown typeLogData of index logCurveinfo
                                //typeLogData will be changed depend on log.indexType
                                if ((this.indexCurve != null) && (this.indexCurve.Value == logCurveInfoObj.mnemonic) && (logCurveInfoObj.typeLogData == LogDataType.unknown))
                                {
                                    if ((this.indexType == LogIndexType.datetime) || (this.indexType == LogIndexType.elapsedtime))
                                        logCurveInfoObj.typeLogData = LogDataType.datetime;
                                }

                                break;
                            case "logData":
                                //collection
                                this.HandleLogData(xmlReader.ReadSubtree(), isNew);
                                break;
                            case "commonData":
                                if (this.commonData == null)
                                    this.commonData = new cs_commonData();
                                this.commonData.HandleXML(xmlReader.ReadSubtree(), isNew);
                                break;
                            case "customData":
                                if (this.customData == null)
                                    this.customData = new cs_customData();
                                this.customData.HandleXML(xmlReader.ReadSubtree(), isNew);
                                break;
                            default:
                                break;
                        }
                    }
                }

                //Assing list back to array
                this.logParam = listLogParam.ToArray();
                this.logCurveInfo = listLogCurveInfo.ToArray();
            }
        }

        /// <summary>
        /// Handle Log data
        /// </summary>
        /// <param name="xmlReader">reader that contains logData node</param>
        /// <param name="isNew">is log Data</param>
        private void HandleLogData(XmlReader xmlReader, bool isNew)
        {
            using (xmlReader)
            {
                //temporary list for multidimensional array
                List<string[]> listLogData = new List<string[]>();

                while (xmlReader.Read())
                {
                    //only take child node
                    if (xmlReader.Depth == 1 && xmlReader.IsStartElement() && !xmlReader.IsEmptyElement)
                    {
                        //convert it into array
                        if (xmlReader.Name == "data")
                            //remove new line and empty space from values
                            listLogData.Add(StaticHelper.CleanDataValue(StaticHelper.ReadString(xmlReader)).Split(','));
                    }
                }
                //assign temporary list to the custom logdata field
                this.extensionLogData = listLogData.ToArray();
            }
        }

        /// <summary>
        /// Serialize without LogData
        /// </summary>
        /// <returns>xml string representation of obj_log</returns>
        public override string Serialize()
        {
            //we want to ignore logData on serialize,so we create xmlAttribute that will be ignored
            XmlAttributes xmlAtts = new XmlAttributes();
            xmlAtts.XmlIgnore = true;

            //specify override for logData which accept ignore attribute
            XmlAttributeOverrides xmlAttsOverrides = new XmlAttributeOverrides();
            xmlAttsOverrides.Add(typeof(obj_log), "logData", xmlAtts);

            //do serialize with override parameter
            return this.Serialize(xmlAttsOverrides,SerializerType.LogWithoutData);
        }
        /// <summary>
        /// Serialize without LogData and namespace or declaration
        /// </summary>
        /// <returns>xml string representation of obj_log</returns>
        public override string SerializeClean()
        {
            //we want to ignore logData on serialize,so we create xmlAttribute that will be ignored
            XmlAttributes xmlAtts = new XmlAttributes();
            xmlAtts.XmlIgnore = true;

            //specify override for logData which accept ignore attribute
            XmlAttributeOverrides xmlAttsOverrides = new XmlAttributeOverrides();
            xmlAttsOverrides.Add(typeof(obj_log), "logData", xmlAtts);

            //do serialize with override parameter
            return this.SerializeClean(xmlAttsOverrides, SerializerType.LogWithoutData);
        }

        /// <summary>
        /// Transfer value from custom 2D array to field LogData
        /// </summary>
        internal void PrepareLogDataForSerialize()
        {
            //convert logData custom field into logData serializable field
            if (extensionLogData != null)
            {
                List<string> listLogData = new List<string>();
                for (int i = 0; i < extensionLogData.Length; i++)
                {
                    listLogData.Add(string.Join(",", extensionLogData[i]));
                }
                this.logData = listLogData.ToArray();
            }
        }

        /// <summary>
        /// Serialize With Log Data
        /// </summary>
        /// <returns>xml string representation of obj_log</returns>
        public string SerializeWithData()
        {
            //call method to convert custom log data container to serialization log data container
            this.PrepareLogDataForSerialize();
            return base.Serialize();
        }
        /// <summary>
        /// Serialize With Log Data without namespace or declaration
        /// </summary>
        /// <returns>xml string representation of obj_log</returns>
        public string SerializeWithDataClean()
        {
            //call method to convert custom log data container to serialization log data container
            this.PrepareLogDataForSerialize();
            return base.SerializeClean(null,SerializerType.Null);
        }
        public override string ValidateGetPath(string newUid)
        {
            // Fix uid if the Object dosn't have one.
            if (!IsValidUid(this.uid))
            {
                this.uid = newUid;
                // TODO: Add some loging that we created a new UID for the object maybe.
            }

            StringBuilder path = new StringBuilder(this.GetParentPath());
            path.Append(@"\");
            path.Append(this.uid);
            return path.ToString();
        }

        public override string GetParentPath()
        {
            if (!IsValidUid(this.uidWell) || !IsValidUid(this.uidWellbore))
            {
                throw new UnableToDecodeException("One or more Parent UID's are invalid", WITSMLReturnCode.WITSMLInvalidParentUID);
                //TODO: Log the issue
            }

            StringBuilder path = new StringBuilder(@"\");
            path.Append(this.uidWell);
            path.Append(@"\");
            path.Append(this.uidWellbore);
            return path.ToString();
        }

        // NOTE: these are done with functions, instead of property access, so as not to mess around with the auto-generated / serialized classes.
        public override string Get_uid()
        {
            return this.uid;
        }

        public override string Get_uidWell()
        {
            return this.uidWell;
        }

        public override string Get_uidWellbore()
        {
            return this.uidWellbore;
        }

        public override string Get_name()
        {
            return this.name;
        }

        public override string Get_nameWell()
        {
            return this.nameWell;
        }

        public override string Get_nameWellbore()
        {
            return this.nameWellbore;
        }

        public override ICommonData Get_commonData()
        {
            if (this.commonData == null)
            {
                this.commonData = new cs_commonData();
            }
            return (ICommonData)this.commonData;
        }

        public override ICustomData Get_customData()
        {
            if (this.customData == null)
            {
                this.customData = new cs_customData();
            }
            return (ICustomData)this.customData;
        }
    }
}
