 

using System;
using System.Xml;

namespace Witsml.Objects1311
{
    public partial class indexedObject : IHandleXML
    {
        /// <summary>
        /// Convert XML string data into indexedObject fields value
        /// </summary>
        /// <param name="xmlReader">reader that has indexedObject node</param>
        /// <param name="IsNew">Is the object from stratch(true) or from db/serialization(false)</param>
        public void HandleXML(XmlReader xmlReader, bool isNew)
        {
            using (xmlReader)
            {
                xmlReader.Read();
                if (xmlReader.HasAttributes)
                {
                    if (xmlReader.MoveToAttribute("index"))
                        this.index = StaticParser.ParseShortFromString(xmlReader.Value);
                    if (xmlReader.MoveToAttribute("name"))
                        this.name = xmlReader.Value;
                    if (xmlReader.MoveToAttribute("uom"))
                        this.uom = xmlReader.Value;
                    if (xmlReader.MoveToAttribute("description"))
                        this.description = xmlReader.Value;
                    xmlReader.MoveToElement();
                }
                this.Value = StaticHelper.ReadString(xmlReader);
            }
        }
    }
}
