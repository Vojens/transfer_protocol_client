 

using System;
using System.Collections.Generic;
using System.Text;
using System.Xml;

namespace Witsml.Objects1311
{
    //TO DO:check whether data handling for doubleValues/stringValues is correct
    public partial class cs_axisDefinition : IHandleXML
    {

        /// <summary>
        /// Convert XML string data into axisDefinition fields value
        /// </summary>
        /// <param name="xmlReader">reader that has axisDefinition node</param>
        /// <param name="IsNew">Is the object from stratch(true) or from db/serialization(false)</param>
        public void HandleXML(XmlReader xmlReader, bool isNew)
        {
            //to  handle choice of doubleValues or stringValues
            bool hasChoice = false;
            //Create temporary list to help with data manipulation
            using (xmlReader)
            {
                //read first node and assign uid and other attribute of the object
                xmlReader.MoveToContent();

                if (xmlReader.HasAttributes)
                {
                    if (xmlReader.MoveToAttribute("uid"))
                        StaticHelper.HandleUID(xmlReader.Value, "axisDefinition uid", out this.uid);

                    xmlReader.MoveToElement();
                }
                //assign default uid
                if (string.IsNullOrEmpty(this.uid))
                    this.uid = "default";
                while (xmlReader.Read())
                {
                    //start on depth 1 which are the childs that we want to check against
                    //check for starting tag and convert node value to its expected variable
                    if (xmlReader.Depth == 1 && xmlReader.IsStartElement() && !xmlReader.IsEmptyElement)
                    {
                        switch (xmlReader.Name)
                        {
                            case "order":
                                this.order = StaticParser.ParseShortFromString(StaticHelper.ReadString(xmlReader));
                                break;
                            case "count":
                                this.count = StaticParser.ParseShortFromString(StaticHelper.ReadString(xmlReader));
                                break;
                            case "name":
                                this.name = StaticHelper.ReadString(xmlReader);
                                break;
                            case "propertyType":
                                this.propertyType = StaticHelper.ReadString(xmlReader);
                                break;
                            case "uom":
                                this.uom = StaticHelper.ReadString(xmlReader);
                                break;
                            case "doubleValues":
                                /*The serialziation of the axis positions of an array as a 
						        whitespace-delimited list values of type xsd:double.  If the length of this list
						        is less than 'count' then the difference in the last two values represents the increment
						        to be used to fill out the list. For example, the list '2 4' with count=4 
						        represents the  list '2 4 6 8'.*/
                                if (!hasChoice)
                                {
                                    //TO DO:Need to be able return list of double value
                                    this.Item = StaticHelper.ReadString(xmlReader);
                                    this.ItemElement = ItemAxisValueChoice.doubleValues;
                                    hasChoice = true;
                                }
                                break;
                            case "stringValues":
                                if (!hasChoice)
                                {
                                    this.Item = StaticHelper.ReadString(xmlReader);
                                    this.ItemElement = ItemAxisValueChoice.stringValues;
                                    hasChoice = true;
                                }
                                break;
                            default:
                                break;
                        }
                    }
                }
            }
        }
    }
}
