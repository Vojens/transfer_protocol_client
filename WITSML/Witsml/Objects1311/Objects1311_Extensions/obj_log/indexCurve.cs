 

using System;
using System.Collections.Generic;
using System.Text;
using System.Xml;

namespace Witsml.Objects1311
{
    public partial class indexCurve : IHandleXML
    {
        /// <summary>
        /// Convert XML string data into indexCurve fields value
        /// </summary>
        /// <param name="xmlReader">reader that has indexCurve node</param>
        /// <param name="IsNew">Is the object from stratch(true) or from db/serialization(false)</param>
        public void HandleXML(XmlReader xmlReader, bool isNew)
        {
            using (xmlReader)
            {
                xmlReader.Read();
                if (xmlReader.HasAttributes)
                {
                    if (xmlReader.MoveToAttribute("columnIndex"))
                        this.columnIndex = StaticParser.ParseShortFromString(xmlReader.Value);
                    xmlReader.MoveToElement();
                }
                this.Value = StaticHelper.ReadString(xmlReader);
            }
        }
    }
}
