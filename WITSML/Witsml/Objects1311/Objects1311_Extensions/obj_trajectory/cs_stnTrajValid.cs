 

using System;
using System.Collections.Generic;
using System.Text;
using System.Xml;

namespace Witsml.Objects1311
{
    public partial class cs_stnTrajValid : IHandleXML
    {
        /// <summary>
        /// Convert XML string data into cs_stnTrajValid fields value
        /// </summary>
        /// <param name="xmlReader">reader that has cs_stnTrajValid node</param>
        /// <param name="IsNew">Is the object from stratch(true) or from db/serialization(false)</param>
        public void HandleXML(XmlReader xmlReader, bool isNew)
        {
            using (xmlReader)
            {
                while (xmlReader.Read())
                {
                    //start on depth 1 which are the childs that we want to check against
                    //check for starting tag and convert node value to its expected variable
                    if (xmlReader.Depth == 1 && xmlReader.IsStartElement() && !xmlReader.IsEmptyElement)
                    {
                        switch (xmlReader.Name)
                        {
                            case "magTotalFieldCalc":
                                if (this.magTotalFieldCalc == null)
                                    this.magTotalFieldCalc = new magneticInductionMeasure();
                                this.magTotalFieldCalc.HandleXML(xmlReader.ReadSubtree(), isNew);
                                break;
                            case "magDipAngleCalc":
                                if (this.magDipAngleCalc == null)
                                    this.magDipAngleCalc = new planeAngleMeasure();
                                this.magDipAngleCalc.HandleXML(xmlReader.ReadSubtree(), isNew);
                                break;
                            case "gravTotalFieldCalc":
                                if (this.gravTotalFieldCalc == null)
                                    this.gravTotalFieldCalc = new accelerationLinearMeasure();
                                this.gravTotalFieldCalc.HandleXML(xmlReader.ReadSubtree(), isNew);
                                break;
                            default:
                                break;
                        }
                    }
                }
            }
        }
    }
}
