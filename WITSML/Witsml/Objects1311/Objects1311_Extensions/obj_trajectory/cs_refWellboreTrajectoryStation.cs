 

using System;
using System.Collections.Generic;
using System.Text;
using System.Xml;

namespace Witsml.Objects1311
{
    public partial class cs_refWellboreTrajectoryStation : IHandleXML
    {
        /// <summary>
        /// Convert XML string data into cs_refWellboreTrajectoryStation fields value
        /// </summary>
        /// <param name="xmlReader">reader that has cs_refWellboreTrajectoryStation node</param>
        /// <param name="IsNew">Is the object from stratch(true) or from db/serialization(false)</param>
        public void HandleXML(XmlReader xmlReader, bool isNew)
        {
            using (xmlReader)
            {
                while (xmlReader.Read())
                {
                    //start on depth 1 which are the childs that we want to check against
                    //check for starting tag and convert node value to its expected variable
                    if (xmlReader.Depth == 1 && xmlReader.IsStartElement() && !xmlReader.IsEmptyElement)
                    {
                        switch (xmlReader.Name)
                        {
                            case "stationReference":
                                this.stationReference = StaticHelper.ReadString(xmlReader);
                                break;
                            case "trajectoryParent":
                                if (this.trajectoryParent == null)
                                    this.trajectoryParent = new refNameString();
                                this.trajectoryParent.ObjectName = "trajectoryParent";
                                this.trajectoryParent.HandleXML(xmlReader.ReadSubtree(), isNew);
                                break;
                            case "wellboreParent":
                                if (this.wellboreParent == null)
                                    this.wellboreParent = new refNameString();
                                this.wellboreParent.ObjectName = "wellboreParent";
                                this.wellboreParent.HandleXML(xmlReader.ReadSubtree(), isNew);
                                break;
                            default:
                                break;
                        }
                    }
                }
            }
        }
    }
}
