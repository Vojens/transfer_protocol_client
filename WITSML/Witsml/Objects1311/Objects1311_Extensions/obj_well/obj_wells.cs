 

using System;
using System.Collections.Generic;
using System.IO;
using System.Text;
using System.Xml;
using System.Xml.Serialization;
using System.Linq;
using Witsml.Common.Exceptions;


namespace Witsml.Objects1311
{
    //Extension class for wells document that inherit from WITSMLDocument and Implement Decode Function
    public partial class obj_wells : WITSMLDocument
    {

        public override WITSMLObject GetSingleObject(int Item)
        {
            return this.well[Item];
        }

        /// <summary>
        /// Deserialize XML string into WitsmlDocument object
        /// </summary>
        /// <param name="XMLin">XML string to be serialized</param>
        /// <returns>Number of singular well inside</returns>
        public override int Decode(ref string xmlIn)
        {
            //return value
            //number of well inside xmlIn
            int countOfWell = 0;
            try
            {
                //check whether collection is from stratch(isNew = true) or from deserialization(isNew=false)
                bool isNew = this.well == null;

                //create temporary list to help with data manipulation
                List<obj_well> listWell = isNew ? new List<obj_well>() : new List<obj_well>(this.well);

                //Consume XML string and convert node value to variable based on node name
                using (StringReader stringReader = new StringReader(xmlIn))
                {
                    XmlReaderSettings setting = new XmlReaderSettings();
                    setting.ConformanceLevel = ConformanceLevel.Document;
                    setting.IgnoreComments = true;
                    setting.IgnoreWhitespace = true;
                    setting.IgnoreProcessingInstructions = true;
                    using (XmlReader xmlReader = XmlReader.Create(stringReader, setting))
                    {
                        //assign version
                        xmlReader.MoveToContent();
                        if (xmlReader.MoveToAttribute("version"))
                            this.version = xmlReader.Value;
                        else
                            this.version = "1.3.1.1";//no version specified,set it to 1.3.1.1
                        while (xmlReader.Read())
                        {
                            //pre-checking on whether node is the correct child based on depth,start tag and not empty
                            if (xmlReader.Depth == 1 && xmlReader.IsStartElement() && !xmlReader.IsEmptyElement)
                            {
                                switch (xmlReader.Name)
                                {
                                    case "documentInfo":
                                        if (this.documentInfo == null)
                                            this.documentInfo = new cs_documentInfo();
                                        this.documentInfo.HandleXML(xmlReader.ReadSubtree(), isNew);
                                        break;
                                    case "well":
                                        obj_well well;
                                        string currentUID = string.Empty;

                                        //Update Well
                                        if (!isNew)
                                        {
                                            //check for uid attribute and assign it to temporary string for searching in collection
                                            if (xmlReader.HasAttributes)
                                            {
                                                if (xmlReader.MoveToAttribute("uid"))
                                                {
                                                    currentUID = xmlReader.Value;
                                                    xmlReader.MoveToElement();
                                                }
                                            }
                                            //look for well based on its UID and update the well with xml string
                                            if (currentUID != string.Empty)
                                            {
                                                well = listWell.FirstOrDefault(
                                                    delegate(obj_well objWell)
                                                    { if (objWell.uid == currentUID)return true; else return false; });
                                                if (well != null)
                                                {
                                                    well.HandleXML(xmlReader.ReadSubtree(), isNew);
                                                    countOfWell++;
                                                    continue;
                                                }
                                                else
                                                {
                                                    //Update can't process well without match
                                                    continue;
                                                }
                                            }
                                            else
                                            {
                                                //Update can't process well without uid
                                                continue;
                                            }
                                        }

                                        //Add New Well 
                                        well = new obj_well();
                                        well.HandleXML(xmlReader.ReadSubtree(), true);
                                        listWell.Add(well);
                                        countOfWell++;
                                        break;
                                    default:
                                        break;
                                }
                            }
                        }
                        //set reference of well array to the list that is used for data manipulation
                        this.well = listWell.ToArray();
                    }
                }
            }
            catch (XmlException ex)
            {
                throw new Exceptions.UnableToDecodeException(ex.Message, WITSMLReturnCode.WITSMLDocumentError, ex.InnerException);
            }
            catch (NullReferenceException ex)
            {
                throw new Exceptions.UnableToDecodeException(ex.Message, WITSMLReturnCode.WITSMLEmptyDocument, ex.InnerException);
            }
            catch (Exceptions.UnableToDecodeException ex)
            {
                throw ex;
            }
            catch (Exception ex)
            {
                throw new Exceptions.UnableToDecodeException(ex.Message, WITSMLReturnCode.WITSMLUnkownCause, ex.InnerException);
            }
            return countOfWell;
        }

       
    }
}
