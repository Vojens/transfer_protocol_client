 

using System;
using System.Collections.Generic;
using System.IO;
using System.Text;
using System.Xml;
using System.Xml.Linq;
using System.Linq;
using Witsml.Common.Exceptions;


namespace Witsml.Objects1311
{
    [System.Xml.Serialization.XmlRootAttribute("well", Namespace = "http://www.witsml.org/schemas/131", IsNullable = false)]
    public partial class obj_well : WITSMLObject, IHandleXML
    {
        public obj_well()
        {
            this.statusWell = WellStatus.unknown;
            this.purposeWell = WellPurpose.unknown;
            this.fluidWell = WellFluid.unknown;
            this.directionWell = WellDirection.unknown;
        }

        /// <summary>
        /// Decode XML string into Well
        /// </summary>
        /// <param name="xmlIn">XML string</param>
        public override void Decode(ref string xmlIn)
        {
            try
            {
                //Consume XML string and convert node value to variable based on node name
                using (StringReader stringReader = new StringReader(xmlIn))
                {
                    XmlReaderSettings setting = new XmlReaderSettings();
                    setting.ConformanceLevel = ConformanceLevel.Document;
                    setting.IgnoreComments = true;
                    setting.IgnoreWhitespace = true;
                    setting.IgnoreProcessingInstructions = true;
                    using (XmlReader xmlReader = XmlReader.Create(stringReader, setting))
                    {
                        //determine add or update operation
                        bool isNew;
                        if (this.uid == string.Empty)
                            isNew = true;
                        else isNew = false;

                        this.HandleXML(xmlReader, isNew);
                    }
                }
            }
            catch (XmlException ex)
            {
                throw new Exceptions.UnableToDecodeException(ex.Message, WITSMLReturnCode.WITSMLDocumentError, ex.InnerException);
            }
            catch (NullReferenceException ex)
            {
                throw new Exceptions.UnableToDecodeException(ex.Message, WITSMLReturnCode.WITSMLEmptyDocument, ex.InnerException);
            }
            catch (Exceptions.UnableToDecodeException ex)
            {
                throw ex;
            }
            catch (Exception ex)
            {
                throw new Exceptions.UnableToDecodeException(ex.Message, WITSMLReturnCode.WITSMLUnkownCause, ex.InnerException);
            }
        }

        /// <summary>
        /// Convert XML string data into Well fields value
        /// </summary>
        /// <param name="xmlReader">reader that has Well node</param>
        /// <param name="IsNew">Is the object from stratch(true) or from db/serialization(false)</param>
        public void HandleXML(XmlReader xmlReader, bool isNew)
        {
            //used to skip read on logCurveInfo handling
            bool hasRead = false;
            //check for existence of object with UID collection,to be used to determine add and update
            bool isWellDatumNew = this.wellDatum == null;
            bool isWellLocationNew = this.wellLocation == null;
            bool isReferecePointNew = this.referencePoint == null;
            bool isWellCRSNew = this.wellCRS == null;

            //create temporary list based to help with data manipulation
            List<cs_wellDatum> listWellDatum = isWellDatumNew ? new List<cs_wellDatum>() : new List<cs_wellDatum>(this.wellDatum);
            List<cs_location> listWellLocation = isWellLocationNew ? new List<cs_location>() : new List<cs_location>(this.wellLocation);
            List<cs_referencePoint> listReferencePoint = isReferecePointNew ? new List<cs_referencePoint>() : new List<cs_referencePoint>(this.referencePoint);
            List<cs_wellCRS> listWellCRS = isWellCRSNew ? new List<cs_wellCRS>() : new List<cs_wellCRS>(this.wellCRS);

            //Read all nodes and assign the value to related fields
            using (xmlReader)
            {
                //assign uid for the new well
                xmlReader.MoveToContent();
                if (xmlReader.HasAttributes)
                {
                    if (xmlReader.MoveToAttribute("uid"))
                        StaticHelper.HandleUID(xmlReader.Value, "well uid", out this.uid);
                    xmlReader.MoveToElement();
                }
                while (!xmlReader.EOF)
                {
                    if (!hasRead)
                    {
                        xmlReader.Read();
                    }
                    else
                    {
                        //skip reading on logcurveInfo node
                        hasRead = false;
                    }
                    //only take child node
                    if (xmlReader.Depth == 1 && xmlReader.IsStartElement() && !xmlReader.IsEmptyElement)
                    {
                        switch (xmlReader.Name)
                        {
                            case "name":
                                this.name = StaticHelper.ReadString(xmlReader);
                                break;
                            case "nameLegal":
                                this.nameLegal = StaticHelper.ReadString(xmlReader);
                                break;
                            case "numLicense":
                                this.numLicense = StaticHelper.ReadString(xmlReader);
                                break;
                            case "numGovt":
                                this.numGovt = StaticHelper.ReadString(xmlReader);
                                break;
                            case "dTimLicense":
                                this.dTimLicense = StaticParser.ParseDateTimeFromString(StaticHelper.ReadString(xmlReader));
                                this.dTimLicenseSpecified = true;
                                break;
                            case "field":
                                this.field = StaticHelper.ReadString(xmlReader);
                                break;
                            case "country":
                                this.country = StaticHelper.ReadString(xmlReader);
                                break;
                            case "state":
                                this.state = StaticHelper.ReadString(xmlReader);
                                break;
                            case "county":
                                this.county = StaticHelper.ReadString(xmlReader);
                                break;
                            case "region":
                                this.region = StaticHelper.ReadString(xmlReader);
                                break;
                            case "district":
                                this.district = StaticHelper.ReadString(xmlReader);
                                break;
                            case "block":
                                this.block = StaticHelper.ReadString(xmlReader);
                                break;
                            case "timeZone":
                                this.timeZone = StaticHelper.ReadString(xmlReader);
                                break;
                            case "operator":
                                this.@operator = StaticHelper.ReadString(xmlReader);
                                break;
                            case "operatorDiv":
                                this.operatorDiv = StaticHelper.ReadString(xmlReader);
                                break;
                            case "pcInterest":
                                if (this.pcInterest == null)
                                    this.pcInterest = new dimensionlessMeasure();
                                this.pcInterest.HandleXML(xmlReader.ReadSubtree(), isNew);
                                break;
                            case "numAPI":
                                this.numAPI = StaticHelper.ReadString(xmlReader);
                                break;
                            case "statusWell":
                                StaticParser.SetEnumFromString<WellStatus>(StaticHelper.ReadString(xmlReader), out this.statusWell, out this.statusWellSpecified);
                                break;
                            case "purposeWell":
                                StaticParser.SetEnumFromString<WellPurpose>(StaticHelper.ReadString(xmlReader), out this.purposeWell, out this.purposeWellSpecified);
                                break;
                            case "fluidWell":
                                StaticParser.SetEnumFromString<WellFluid>(StaticHelper.ReadString(xmlReader), out this.fluidWell, out this.fluidWellSpecified);
                                break;
                            case "directionWell":
                                StaticParser.SetEnumFromString<WellDirection>(StaticHelper.ReadString(xmlReader), out this.directionWell, out this.directionWellSpecified);
                                break;
                            case "dTimSpud":
                                this.dTimSpud = StaticParser.ParseDateTimeFromString(StaticHelper.ReadString(xmlReader));
                                this.dTimSpudSpecified = true;
                                break;
                            case "dTimPa":
                                this.dTimPa = StaticParser.ParseDateTimeFromString(StaticHelper.ReadString(xmlReader));
                                this.dTimPaSpecified = true;
                                break;
                            case "wellheadElevation":
                                if (this.wellheadElevation == null)
                                    this.wellheadElevation = new wellElevationCoord();
                                this.wellheadElevation.HandleXML(xmlReader.ReadSubtree(), isNew);
                                break;
                            case "wellDatum":
                                //multiple objects with uid
                                cs_wellDatum wellDatum;
                                string currentWellDatumUID = string.Empty;

                                if (xmlReader.HasAttributes)
                                {
                                    //check for uid attribute and assign it to temporary string for searching in collection
                                    if (xmlReader.MoveToAttribute("uid"))
                                    {
                                        currentWellDatumUID = xmlReader.Value;
                                        xmlReader.MoveToElement();
                                    }
                                }

                                //handling for recursive object with no uid
                                if (String.IsNullOrEmpty(currentWellDatumUID))
                                {
                                    //Delete/Clear Operation for update 
                                    //Skip Operation for Add
                                    if (!isWellDatumNew)
                                    {
                                        listWellDatum.Clear();
                                    }
                                    continue;
                                }

                                //Check Operation to be perform
                                if (!isWellDatumNew)
                                {
                                    //Update Operation
                                    //look for wellDatum based on its UID and update the well with xml string
                                    wellDatum = listWellDatum.FirstOrDefault(
                                        delegate(cs_wellDatum objWellDatum)
                                        { if (objWellDatum.uid == currentWellDatumUID)return true; else return false; });
                                    if (wellDatum != null)
                                    {
                                        wellDatum.HandleXML(xmlReader.ReadSubtree(), isWellDatumNew);
                                        continue;
                                    }
                                }

                                //Add Operation
                                wellDatum = new cs_wellDatum();
                                wellDatum.HandleXML(xmlReader.ReadSubtree(), isWellDatumNew);
                                listWellDatum.Add(wellDatum);
                                break;
                            case "groundElevation":
                                if (this.groundElevation == null)
                                    this.groundElevation = new wellElevationCoord();
                                this.groundElevation.HandleXML(xmlReader.ReadSubtree(), isNew);
                                break;
                            case "waterDepth":
                                if (this.waterDepth == null)
                                    this.waterDepth = new wellVerticalDepthCoord();
                                this.waterDepth.HandleXML(xmlReader.ReadSubtree(), isNew);
                                break;
                            case "wellLocation":
                                //multiple objects with uid
                                cs_location wellLocation;
                                string currentWellLocationUID = string.Empty;

                                if (xmlReader.HasAttributes)
                                {
                                    //check for uid attribute and assign it to temporary string for searching in collection
                                    if (xmlReader.MoveToAttribute("uid"))
                                    {
                                        currentWellLocationUID = xmlReader.Value;
                                        xmlReader.MoveToElement();
                                    }
                                }

                                //handling for recursive object with no uid
                                if (String.IsNullOrEmpty(currentWellLocationUID))
                                {
                                    //Delete/Clear Operation for update 
                                    //Skip Operation for Add
                                    if (!isWellLocationNew)
                                    {
                                        listWellLocation.Clear();
                                    }
                                    continue;
                                }


                                //Check Operation to be perform
                                if (!isWellLocationNew)
                                {
                                    //Update Operation
                                    if (xmlReader.HasAttributes)
                                    {
                                        //check for uid attribute and assign it to temporary string for searching in collection
                                        //look for wellDatum based on its UID and update the well with xml string
                                        wellLocation = listWellLocation.FirstOrDefault(
                                            delegate(cs_location objWellLocation)
                                            { if (objWellLocation.uid == currentWellLocationUID)return true; else return false; });
                                        if (wellLocation != null)
                                        {
                                            wellLocation.HandleXML(xmlReader.ReadSubtree(), isWellLocationNew);
                                            continue;
                                        }
                                    }
                                }

                                //Add Operation
                                wellLocation = new cs_location();
                                wellLocation.HandleXML(xmlReader.ReadSubtree(), isWellLocationNew);
                                listWellLocation.Add(wellLocation);
                                break;
                            case "referencePoint":
                                //multiple objects with uid
                                cs_referencePoint referencePointObj;
                                string currentRefPointUID = string.Empty;

                                if (xmlReader.HasAttributes)
                                {
                                    //check for uid attribute and assign it to temporary string for searching in collection
                                    if (xmlReader.MoveToAttribute("uid"))
                                    {
                                        currentRefPointUID = xmlReader.Value;
                                        xmlReader.MoveToElement();
                                    }
                                }

                                //handling for recursive object with no uid
                                if (String.IsNullOrEmpty(currentRefPointUID))
                                {
                                    //Delete/Clear Operation for update 
                                    //Skip Operation for Add
                                    if (!isReferecePointNew)
                                    {
                                        listReferencePoint.Clear();
                                    }
                                    continue;
                                }

                                //Check Operation to be perform
                                if (!isReferecePointNew)
                                {
                                    //Update Operation
                                    if (xmlReader.HasAttributes)
                                    {
                                        //check for uid attribute and assign it to temporary string for searching in collection
                                        //look for wellDatum based on its UID and update the well with xml string
                                        referencePointObj = listReferencePoint.FirstOrDefault(
                                            delegate(cs_referencePoint objRefPoint)
                                            { if (objRefPoint.uid == currentRefPointUID)return true; else return false; });
                                        if (referencePointObj != null)
                                        {
                                            referencePointObj.HandleXML(xmlReader.ReadSubtree(), isReferecePointNew);
                                            continue;
                                        }
                                    }
                                }

                                //Add Operation
                                referencePointObj = new cs_referencePoint();
                                referencePointObj.HandleXML(xmlReader.ReadSubtree(), isReferecePointNew);
                                listReferencePoint.Add(referencePointObj);
                                break;
                            case "wellCRS":
                                //multiple objects with uid
                                //handling check for name if uid is empty.This is to help conversion of 1.2.0 to 1.3.1
                                //collection
                                cs_wellCRS wellCRSObj;
                                string currentWellCrsUID = string.Empty;

                                if (xmlReader.HasAttributes)
                                {
                                    //check for uid attribute and assign it to temporary string for searching in collection
                                    if (xmlReader.MoveToAttribute("uid"))
                                    {
                                        currentWellCrsUID = xmlReader.Value;
                                        xmlReader.MoveToElement();
                                    }
                                }

                                if (String.IsNullOrEmpty(currentWellCrsUID))
                                {
                                    //check on name
                                    //read into element first
                                    XElement wellCRSElement = XElement.ReadFrom(xmlReader) as XElement;
                                    hasRead = true;

                                    //get the name of the well CRS
                                    string name = string.Empty;
                                    XNamespace wits = xmlReader.NamespaceURI;
                                    if (wellCRSElement.HasElements && wellCRSElement.Element(wits + "name") != null)
                                    {
                                        name = wellCRSElement.Element(wits + "name").Value;
                                    }

                                    if (name != string.Empty)
                                    {
                                        //update
                                        if (!isWellCRSNew)
                                        {
                                            //Determine operation to be performed
                                            wellCRSObj = listWellCRS.FirstOrDefault(
                                                delegate(cs_wellCRS objWellCRS)
                                                { if (objWellCRS.name == name)return true; else return false; });
                                            if (wellCRSObj != null)
                                            {
                                                //update
                                                wellCRSObj.HandleXML(wellCRSElement.CreateReader(), isWellCRSNew);
                                                continue;
                                            }
                                        }
                                        //add
                                        wellCRSObj = new cs_wellCRS();
                                        wellCRSObj.HandleXML(xmlReader.ReadSubtree(), isWellCRSNew);
                                        listWellCRS.Add(wellCRSObj);
                                    }

                                }
                                else
                                {
                                    if (!isWellCRSNew)
                                    {
                                        //look for wellDatum based on its UID and update the well with xml string
                                        wellCRSObj = listWellCRS.FirstOrDefault(
                                            delegate(cs_wellCRS objWellCRS)
                                            { if (objWellCRS.uid == currentWellCrsUID)return true; else return false; });
                                        if (wellCRSObj != null)
                                        {
                                            wellCRSObj.HandleXML(xmlReader.ReadSubtree(), isWellCRSNew);
                                            continue;
                                        }
                                    }
                                    //add
                                    wellCRSObj = new cs_wellCRS();
                                    wellCRSObj.HandleXML(xmlReader.ReadSubtree(), isWellCRSNew);
                                    listWellCRS.Add(wellCRSObj);
                                }

                                break;
                            case "commonData":
                                //extension object
                                if (this.commonData == null)
                                    this.commonData = new cs_commonData();
                                this.commonData.HandleXML(xmlReader.ReadSubtree(), isNew);
                                break;
                            case "customData":
                                //extension object
                                if (this.customData == null)
                                    this.customData = new cs_customData();
                                this.customData.HandleXML(xmlReader.ReadSubtree(), isNew);
                                break;
                            default:
                                break;
                        }
                    }
                }

                //Set Array collections of Well to list
                this.wellDatum = listWellDatum.ToArray();
                this.wellLocation = listWellLocation.ToArray();
                this.referencePoint = listReferencePoint.ToArray();
                this.wellCRS = listWellCRS.ToArray();
            }
        }

        public override string ValidateGetPath(string newUid)
        {
            // Fix uid if the Object dosn't have one.
            if (!IsValidUid(this.uid))
            {
                this.uid = newUid;
                // TODO: Add some loging that we created a new UID for the object maybe.
            }

            StringBuilder path = new StringBuilder(this.GetParentPath());
            path.Append(@"\");
            path.Append(this.uid);
            return path.ToString();
        }

        public override string GetParentPath()
        {
            return "";  // Well's don't have a parent path.
        }

        // NOTE: these are done with functions, instead of property access, so as not to mess around with the auto-generated / serialized classes.
        public override string Get_uid()
        {
            return this.uid;
        }

        public override string Get_uidWell()
        {
            return this.uid;
        }

        public override string Get_uidWellbore()
        {
            return "";
        }

        public override string Get_name()
        {
            return this.name;
        }

        public override string Get_nameWell()
        {
            return this.name;
        }

        public override string Get_nameWellbore()
        {
            return "";
        }

        public override ICommonData Get_commonData()
        {
            if (this.commonData == null)
            {
                this.commonData = new cs_commonData();
            }
            return (ICommonData)this.commonData;
        }
        public override ICustomData Get_customData()
        {
            if (this.customData == null)
            {
                this.customData = new cs_customData();
            }
            return (ICustomData)this.customData;
        }
    }

}
