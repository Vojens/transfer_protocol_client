 

using System;
using System.Xml;

namespace Witsml.Objects1311
{
    public partial class cs_wellCRS : IHandleXML
    {
        /// <summary>
        /// Convert XML string data into Well CRS fields value
        /// </summary>
        /// <param name="xmlReader">reader that has Well CRS node</param>
        /// <param name="IsNew">Is the object from stratch(true) or from db/serialization(false)</param>
        public void HandleXML(XmlReader xmlReader, bool isNew)
        {
            //condition for allowing only one of geographic,localCRS,mapProjection to represent member item
            bool hasChoice = false;
            //Read all nodes and assign the value to related fields
            using (xmlReader)
            {
                xmlReader.MoveToContent();
                if (xmlReader.HasAttributes)
                {
                    if (xmlReader.MoveToAttribute("uid"))
                    {
                        string value = xmlReader.Value;
                        if (!string.IsNullOrEmpty(value))
                            StaticHelper.HandleUID(xmlReader.Value, "wellCRS uid", out this.uid);
                    }
                    else
                        this.uid = StaticHelper.GenerateObjectUID();
                    xmlReader.MoveToElement();
                }
                while (xmlReader.Read())
                {
                    if (xmlReader.Depth == 1 && xmlReader.IsStartElement() && !xmlReader.IsEmptyElement)
                    {
                        switch (xmlReader.Name)
                        {
                            case "name":
                                this.name = StaticHelper.ReadString(xmlReader);
                                break;
                            //there are 3 choices for item
                            //1.geographic
                            //2.localCRS
                            //3.mapProjection
                            case "geographic":
                                if (!hasChoice)
                                {
                                    cs_geodeticModel geodetic = new cs_geodeticModel();
                                    geodetic.HandleXML(xmlReader.ReadSubtree(), isNew);
                                    this.Item = geodetic;
                                    hasChoice = true;
                                }
                                break;
                            case "localCRS":
                                if (!hasChoice)
                                {
                                    cs_localCRS localCRS = new cs_localCRS();
                                    localCRS.HandleXML(xmlReader.ReadSubtree(), isNew);
                                    this.Item = localCRS;
                                    hasChoice = true;
                                }
                                break;
                            case "mapProjection":
                                if (!hasChoice)
                                {
                                    cs_projectionx projection = new cs_projectionx();
                                    projection.HandleXML(xmlReader.ReadSubtree(), isNew);
                                    this.Item = projection;
                                    hasChoice = true;
                                }
                                break;
                            case "description":
                                this.description = StaticHelper.ReadString(xmlReader);
                                break;
                            default:
                                break;
                        }

                    }
                }
            }
        }

    }
}
