 

using System;
using System.Collections.Generic;
using System.Xml;

namespace Witsml.Objects1311
{
    public partial class cs_geodeticModel : IHandleXML
    {
        public cs_geodeticModel()
        {
            this.geodeticDatumCode = GeodeticDatum.unknown;
            this.ellipsoidCode = Ellipsoid.unknown;
        }
        /// <summary>
        /// Convert XML string data into cs_projectionx fields value
        /// </summary>
        /// <param name="xmlReader">reader that has cs_projectionx node</param>
        /// <param name="IsNew">Is the object from stratch(true) or from db/serialization(false)</param>
        public void HandleXML(XmlReader xmlReader, bool isNew)
        {
            //Read all nodes and assign the value to related fields
            using (xmlReader)
            {
                while (xmlReader.Read())
                {
                    if (xmlReader.Depth == 1 && xmlReader.IsStartElement() && !xmlReader.IsEmptyElement)
                    {
                        switch (xmlReader.Name)
                        {
                            case "nameCRS":
                                if (this.nameCRS == null)
                                    this.nameCRS = new wellKnownNameStruct();
                                this.nameCRS.HandleXML(xmlReader.ReadSubtree(), isNew);
                                break;
                            case "geodeticDatumCode":
                                StaticParser.SetEnumFromString<GeodeticDatum>(StaticHelper.ReadString(xmlReader), out this.geodeticDatumCode, out this.geodeticDatumCodeSpecified);
                                break;
                            case "xTranslation":
                                if (this.xTranslation == null)
                                    this.xTranslation = new lengthMeasure();
                                this.xTranslation.HandleXML(xmlReader.ReadSubtree(), isNew);
                                break;
                            case "yTranslation":
                                if (this.yTranslation == null)
                                    this.yTranslation = new lengthMeasure();
                                this.yTranslation.HandleXML(xmlReader.ReadSubtree(), isNew);
                                break;
                            case "zTranslation":
                                if (this.zTranslation == null)
                                    this.zTranslation = new lengthMeasure();
                                this.zTranslation.HandleXML(xmlReader.ReadSubtree(), isNew);
                                break;
                            case "xRotation":
                                if (this.xRotation == null)
                                    this.xRotation = new planeAngleMeasure();
                                this.xRotation.HandleXML(xmlReader.ReadSubtree(), isNew);
                                break;
                            case "yRotation":
                                if (this.yRotation == null)
                                    this.yRotation = new planeAngleMeasure();
                                this.yRotation.HandleXML(xmlReader.ReadSubtree(), isNew);
                                break;
                            case "zRotation":
                                if (this.zRotation == null)
                                    this.zRotation = new planeAngleMeasure();
                                this.zRotation.HandleXML(xmlReader.ReadSubtree(), isNew);
                                break;
                            case "scaleFactor":
                                this.scaleFactor = StaticParser.ParseDoubleFromString(StaticHelper.ReadString(xmlReader));
                                this.scaleFactorSpecified = true;
                                break;
                            case "ellipsoidCode":
                                StaticParser.SetEnumFromString<Ellipsoid>(StaticHelper.ReadString(xmlReader), out this.ellipsoidCode, out this.ellipsoidCodeSpecified);
                                break;
                            case "ellipsoidSemiMajorAxis":
                                if (this.ellipsoidSemiMajorAxis == null)
                                    this.ellipsoidSemiMajorAxis = new lengthMeasure();
                                this.ellipsoidSemiMajorAxis.HandleXML(xmlReader.ReadSubtree(), isNew);
                                break;
                            case "ellipsoidInverseFlattening":
                                this.ellipsoidInverseFlattening = StaticParser.ParseDoubleFromString(StaticHelper.ReadString(xmlReader));
                                this.ellipsoidInverseFlatteningSpecified = true;
                                break;
                            default:
                                break;
                        }
                    }
                }
            }
        }
    }
}
