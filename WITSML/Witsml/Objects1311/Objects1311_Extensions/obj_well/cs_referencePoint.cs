 

using System;
using System.Collections.Generic;
using System.Xml;
using System.Linq;

namespace Witsml.Objects1311
{
    public partial class cs_referencePoint : IHandleXML
    {
        /// <summary>
        /// Convert XML string data into ReferencePoint fields value
        /// </summary>
        /// <param name="xmlReader">reader that has ReferencePoint node</param>
        /// <param name="IsNew">Is the object from stratch(true) or from db/serialization(false)</param>
        public void HandleXML(XmlReader xmlReader, bool isNew)
        {
            //check for existence of object with UID collection,to be used to determine add and update
            bool isWellLocationNew = this.location == null;

            //create temporary list to be used for data manipulations
            List<cs_location> listWellLocation = isWellLocationNew ? new List<cs_location>() : new List<cs_location>(this.location);

            //Read all nodes and assign the value to related fields
            using (xmlReader)
            {
                xmlReader.MoveToContent();
                if (xmlReader.HasAttributes)
                {
                    if (xmlReader.MoveToAttribute("uid"))
                        StaticHelper.HandleUID(xmlReader.Value, "referencePoint uid", out this.uid);
                    else
                        this.uid = StaticHelper.GenerateObjectUID();
                    xmlReader.MoveToElement();
                }
                while (xmlReader.Read())
                {
                    if (xmlReader.Depth == 1 && xmlReader.IsStartElement() && !xmlReader.IsEmptyElement)
                    {
                        switch (xmlReader.Name)
                        {
                            case "name":
                                this.name = StaticHelper.ReadString(xmlReader);
                                break;
                            case "type":
                                this.type = StaticHelper.ReadString(xmlReader);
                                break;
                            case "elevation":
                                if (this.elevation == null)
                                    this.elevation = new wellElevationCoord();
                                this.elevation.HandleXML(xmlReader.ReadSubtree(), isNew);
                                break;
                            case "measuredDepth":
                                if (this.measuredDepth == null)
                                    this.measuredDepth = new measuredDepthCoord();
                                this.measuredDepth.HandleXML(xmlReader.ReadSubtree(), isNew);
                                break;
                            case "location":
                                //objects with uid
                                cs_location wellLocation;
                                string currentWellLocationUID = string.Empty;

                                if (xmlReader.HasAttributes)
                                {
                                    //check for uid attribute and assign it to temporary string for searching in collection
                                    if (xmlReader.MoveToAttribute("uid"))
                                    {
                                        currentWellLocationUID = xmlReader.Value;
                                        xmlReader.MoveToElement();
                                    }
                                }

                                //handling for recursive object with no uid
                                if (String.IsNullOrEmpty(currentWellLocationUID))
                                {
                                    //Delete/Clear Operation for update 
                                    //Skip Operation for Add
                                    if (!isWellLocationNew)
                                    {
                                        listWellLocation.Clear();
                                    }
                                    continue;
                                }

                                //Update
                                if (!isWellLocationNew)
                                {
                                    //check for uid attribute and assign it to temporary string for searching in collection
                                    if (xmlReader.HasAttributes)
                                    {
                                        //look for wellLocation based on its UID and update the well with xml string
                                        wellLocation = listWellLocation.FirstOrDefault(
                                            delegate(cs_location objWellLocation)
                                            { if (objWellLocation.uid == currentWellLocationUID)return true; else return false; });
                                        if (wellLocation != null)
                                        {
                                            wellLocation.HandleXML(xmlReader.ReadSubtree(), isWellLocationNew);
                                            continue;
                                        }
                                    }
                                }

                                //Add New wellLocation
                                wellLocation = new cs_location();
                                wellLocation.HandleXML(xmlReader.ReadSubtree(), isWellLocationNew);
                                listWellLocation.Add(wellLocation);
                                break;
                            case "description":
                                this.description = StaticHelper.ReadString(xmlReader);
                                break;
                            default:
                                break;
                        }
                    }
                }
                //assign list to this class member collection
                this.location = listWellLocation.ToArray();
            }
        }
    }
}
