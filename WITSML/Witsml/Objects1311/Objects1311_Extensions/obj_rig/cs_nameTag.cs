 

using System;
using System.Collections.Generic;
using System.Text;
using System.Xml;

namespace Witsml.Objects1311
{
    public partial class cs_nameTag : IHandleXML, IHasID
    {
        //this will need to be ignored
        [System.Xml.Serialization.XmlIgnore]
        public string UID
        {
            get { return this.uid; }
        }
        public void HandleXML(XmlReader xmlReader, bool isNew)
        {
            using (xmlReader)
            {
                xmlReader.MoveToContent();
                if (xmlReader.HasAttributes)
                {
                    if (xmlReader.MoveToAttribute("uid"))
                        StaticHelper.HandleUID(xmlReader.Value, "nameTag uid", out this.uid);
                    xmlReader.MoveToElement();
                }
                while (xmlReader.Read())
                {
                    //only take child node
                    if (xmlReader.Depth == 1 && xmlReader.IsStartElement() && !xmlReader.IsEmptyElement)
                    {
                        switch (xmlReader.Name)
                        {
                            case "name":
                                this.name = StaticHelper.ReadString(xmlReader);
                                break;
                            case "numberingScheme":
                                this.numberingScheme = StaticHelper.ReadString(xmlReader);
                                break;
                            case "technology":
                                this.technology = StaticHelper.ReadString(xmlReader);
                                break;
                            case "location":
                                this.location = StaticHelper.ReadString(xmlReader);
                                break;
                            case "installationDate":
                                this.installationDate = StaticParser.ParseDateTimeFromString(StaticHelper.ReadString(xmlReader));
                                this.installationDateSpecified = true;
                                break;
                            case "installationCompany":
                                this.installationCompany = StaticHelper.ReadString(xmlReader);
                                break;
                            case "mountingCode":
                                this.mountingCode = StaticHelper.ReadString(xmlReader);
                                break;
                            case "comment":
                                this.comment = StaticHelper.ReadString(xmlReader);
                                break;
                            default:
                                break;
                        }
                    }
                }
            }
        }

    }
}
