 

using System;
using System.Collections.Generic;
using System.Text;
using System.Xml;

namespace Witsml.Objects1311
{
    public partial class cs_bopComponent : IHandleXML,IHasID
    {
        //this will need to be ignored
        [System.Xml.Serialization.XmlIgnore]
        public string UID
        {
            get { return this.uid; }
        }

        public cs_bopComponent()
        {
            this.typeBopComp = BopType.unknown;
        }
        public void HandleXML(XmlReader xmlReader, bool isNew)
        {
            using (xmlReader)
            {
                xmlReader.MoveToContent();
                if (xmlReader.HasAttributes)
                {
                    if (xmlReader.MoveToAttribute("uid"))
                        StaticHelper.HandleUID(xmlReader.Value, "bopComponent uid", out this.uid);
                    xmlReader.MoveToElement();
                }
                while (xmlReader.Read())
                {
                    //only take child node
                    if (xmlReader.Depth == 1 && xmlReader.IsStartElement() && !xmlReader.IsEmptyElement)
                    {
                        switch (xmlReader.Name)
                        {
                            case "typeBopComp":
                                StaticParser.SetEnumFromString<BopType>(StaticHelper.ReadString(xmlReader), out this.typeBopComp, out this.typeBopCompSpecified);
                                break;
                            case "descComp":
                                this.descComp = StaticHelper.ReadString(xmlReader);
                                break;
                            case "idPassThru":
                                if (this.idPassThru == null)
                                    this.idPassThru = new lengthMeasure();
                                this.idPassThru.HandleXML(xmlReader.ReadSubtree(), isNew);
                                break;
                            case "presWork":
                                if (this.presWork == null)
                                    this.presWork = new pressureMeasure();
                                this.presWork.HandleXML(xmlReader.ReadSubtree(), isNew);
                                break;
                            case "diaCloseMn":
                                if (this.diaCloseMn == null)
                                    this.diaCloseMn = new lengthMeasure();
                                this.diaCloseMn.HandleXML(xmlReader.ReadSubtree(), isNew);
                                break;
                            case "diaCloseMx":
                                if (this.diaCloseMx == null)
                                    this.diaCloseMx = new lengthMeasure();
                                this.diaCloseMx.HandleXML(xmlReader.ReadSubtree(), isNew);
                                break;
                            case "nomenclature":
                                this.nomenclature = StaticHelper.ReadString(xmlReader);
                                break;
                            case "isVariable":
                                this.isVariable = StaticParser.ParseBooleanFromString(StaticHelper.ReadString(xmlReader));
                                this.isVariableSpecified = true;
                                break;
                            default:
                                break;
                        }
                    }
                }
            }
        }
    }
}
