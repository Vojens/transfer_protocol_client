 

using System;
using System.Collections.Generic;
using System.Text;
using System.Xml;

namespace Witsml.Objects1311
{
    public partial class cs_degasser : IHandleXML, IHasID
    {
        //this will need to be ignored
        [System.Xml.Serialization.XmlIgnore]
        public string UID
        {
            get { return this.uid; }
        }
        public void HandleXML(XmlReader xmlReader, bool isNew)
        {
            bool isNameTagNew = this.nameTag == null;

            List<cs_nameTag> listNameTag = this.nameTag == null ? new List<cs_nameTag>() : new List<cs_nameTag>(this.nameTag);
            using (xmlReader)
            {
                xmlReader.MoveToContent();
                if (xmlReader.HasAttributes)
                {
                    if (xmlReader.MoveToAttribute("uid"))
                        StaticHelper.HandleUID(xmlReader.Value, "degasser uid", out this.uid);
                    xmlReader.MoveToElement();
                }
                while (xmlReader.Read())
                {
                    //only take child node
                    if (xmlReader.Depth == 1 && xmlReader.IsStartElement() && !xmlReader.IsEmptyElement)
                    {
                        switch (xmlReader.Name)
                        {
                            case "manufacturer":
                                this.manufacturer = StaticHelper.ReadString(xmlReader);
                                break;
                            case "model":
                                this.model = StaticHelper.ReadString(xmlReader);
                                break;
                            case "dTimInstall":
                                this.dTimInstall = StaticParser.ParseDateTimeFromString(StaticHelper.ReadString(xmlReader));
                                this.dTimInstallSpecified = true;
                                break;
                            case "dTimRemove":
                                this.dTimRemove = StaticParser.ParseDateTimeFromString(StaticHelper.ReadString(xmlReader));
                                this.dTimRemoveSpecified = true;
                                break;
                            case "type":
                                this.type = StaticHelper.ReadString(xmlReader);
                                break;
                            case "owner":
                                this.owner = StaticHelper.ReadString(xmlReader);
                                break;
                            case "height":
                                if (this.height == null)
                                    this.height = new lengthMeasure();
                                this.height.HandleXML(xmlReader.ReadSubtree(), isNew);
                                break;
                            case "len":
                                if (this.len == null)
                                    this.len = new lengthMeasure();
                                this.len.HandleXML(xmlReader.ReadSubtree(), isNew);
                                break;
                            case "id":
                                if (this.id == null)
                                    this.id = new lengthMeasure();
                                this.id.HandleXML(xmlReader.ReadSubtree(), isNew);
                                break;
                            case "capFlow":
                                if (this.capFlow == null)
                                    this.capFlow = new volumeFlowRateMeasure();
                                this.capFlow.HandleXML(xmlReader.ReadSubtree(), isNew);
                                break;
                            case "areaSeparatorFlow":
                                if (this.areaSeparatorFlow == null)
                                    this.areaSeparatorFlow = new areaMeasure();
                                this.areaSeparatorFlow.HandleXML(xmlReader.ReadSubtree(), isNew);
                                break;
                            case "htMudSeal":
                                if (this.htMudSeal == null)
                                    this.htMudSeal = new lengthMeasure();
                                this.htMudSeal.HandleXML(xmlReader.ReadSubtree(), isNew);
                                break;
                            case "idInlet":
                                if (this.idInlet == null)
                                    this.idInlet = new lengthMeasure();
                                this.idInlet.HandleXML(xmlReader.ReadSubtree(), isNew);
                                break;
                            case "idVentLine":
                                if (this.idVentLine == null)
                                    this.idVentLine = new lengthMeasure();
                                this.idVentLine.HandleXML(xmlReader.ReadSubtree(), isNew);
                                break;
                            case "lenVentLine":
                                if (this.lenVentLine == null)
                                    this.lenVentLine = new lengthMeasure();
                                this.lenVentLine.HandleXML(xmlReader.ReadSubtree(), isNew);
                                break;
                            case "capGasSep":
                                if (this.capGasSep == null)
                                    this.capGasSep = new volumeFlowRateMeasure();
                                this.capGasSep.HandleXML(xmlReader.ReadSubtree(), isNew);
                                break;
                            case "capBlowdown":
                                if (this.capBlowdown == null)
                                    this.capBlowdown = new volumeFlowRateMeasure();
                                this.capBlowdown.HandleXML(xmlReader.ReadSubtree(), isNew);
                                break;
                            case "presRating":
                                if (this.presRating == null)
                                    this.presRating = new pressureMeasure();
                                this.presRating.HandleXML(xmlReader.ReadSubtree(), isNew);
                                break;
                            case "tempRating":
                                if (this.tempRating == null)
                                    this.tempRating = new thermodynamicTemperatureMeasure();
                                this.tempRating.HandleXML(xmlReader.ReadSubtree(), isNew);
                                break;
                            case "nameTag":
                                //collection object
                                StaticHelper.AddUpdateWithUid<cs_nameTag>(xmlReader, isNameTagNew, ref listNameTag);
                                break;
                            default:
                                break;
                        }
                    }
                }
                //assign to list
                this.nameTag = listNameTag.ToArray();
            }
        }

    }
}
