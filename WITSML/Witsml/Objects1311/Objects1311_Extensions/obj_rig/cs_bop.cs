 

using System;
using System.Collections.Generic;
using System.Text;
using System.Xml;

namespace Witsml.Objects1311
{
    public partial class cs_bop : IHandleXML
    {
        public void HandleXML(XmlReader xmlReader, bool isNew)
        {
            bool isNameTagNew = this.nameTag == null;
            bool isBopCompNew = this.bopComponent == null;

            List<cs_nameTag> listNameTag = this.nameTag == null ? new List<cs_nameTag>() : new List<cs_nameTag>(this.nameTag);
            List<cs_bopComponent> listBopComp = this.bopComponent == null ? new List<cs_bopComponent>() : new List<cs_bopComponent>(this.bopComponent);
            using (xmlReader)
            {
                while (xmlReader.Read())
                {
                    //only take child node
                    if (xmlReader.Depth == 1 && xmlReader.IsStartElement() && !xmlReader.IsEmptyElement)
                    {
                        switch (xmlReader.Name)
                        {
                            case "manufacturer":
                                this.manufacturer = StaticHelper.ReadString(xmlReader);
                                break;
                            case "model":
                                this.model = StaticHelper.ReadString(xmlReader);
                                break;
                            case "dTimInstall":
                                this.dTimInstall = StaticParser.ParseDateTimeFromString(StaticHelper.ReadString(xmlReader));
                                this.dTimInstallSpecified = true;
                                break;
                            case "dTimRemove":
                                this.dTimRemove = StaticParser.ParseDateTimeFromString(StaticHelper.ReadString(xmlReader));
                                this.dTimRemoveSpecified = true;
                                break;
                            case "nameTag":
                                //collection object
                                StaticHelper.AddUpdateWithUid<cs_nameTag>(xmlReader, isNameTagNew, ref listNameTag);
                                break;
                            case "typeConnectionBop":
                                this.typeConnectionBop = StaticHelper.ReadString(xmlReader);
                                break;
                            case "sizeConnectionBop":
                                if (this.sizeConnectionBop == null)
                                    this.sizeConnectionBop = new lengthMeasure();
                                this.sizeConnectionBop.HandleXML(xmlReader.ReadSubtree(), isNew);
                                break;
                            case "presBopRating":
                                //object
                                if (this.presBopRating == null)
                                    this.presBopRating = new pressureMeasure();
                                this.presBopRating.HandleXML(xmlReader.ReadSubtree(), isNew);
                                break;
                            case "sizeBopSys":
                                if (this.sizeBopSys == null)
                                    this.sizeBopSys = new lengthMeasure();
                                this.sizeBopSys.HandleXML(xmlReader.ReadSubtree(), isNew);
                                break;
                            case "rotBop":
                                this.rotBop = StaticParser.ParseBooleanFromString(StaticHelper.ReadString(xmlReader));
                                this.rotBopSpecified = true;
                                break;
                            case "idBoosterLine":
                                if (this.idBoosterLine == null)
                                    this.idBoosterLine = new lengthMeasure();
                                this.idBoosterLine.HandleXML(xmlReader.ReadSubtree(), isNew);
                                break;
                            case "odBoosterLine":
                                if (this.odBoosterLine == null)
                                    this.odBoosterLine = new lengthMeasure();
                                this.odBoosterLine.HandleXML(xmlReader.ReadSubtree(), isNew);
                                break;
                            case "lenBoosterLine":
                                if (this.lenBoosterLine == null)
                                    this.lenBoosterLine = new lengthMeasure();
                                this.lenBoosterLine.HandleXML(xmlReader.ReadSubtree(), isNew);
                                break;
                            case "idSurfLine":
                                if (this.idSurfLine == null)
                                    this.idSurfLine = new lengthMeasure();
                                this.idSurfLine.HandleXML(xmlReader.ReadSubtree(), isNew);
                                break;
                            case "odSurfLine":
                                if (this.odSurfLine == null)
                                    this.odSurfLine = new lengthMeasure();
                                this.odSurfLine.HandleXML(xmlReader.ReadSubtree(), isNew);
                                break;
                            case "lenSurfLine":
                                if (this.lenSurfLine == null)
                                    this.lenSurfLine = new lengthMeasure();
                                this.lenSurfLine.HandleXML(xmlReader.ReadSubtree(), isNew);
                                break;
                            case "idChkLine":
                                if (this.idChkLine == null)
                                    this.idChkLine = new lengthMeasure();
                                this.idChkLine.HandleXML(xmlReader.ReadSubtree(), isNew);
                                break;
                            case "odChkLine":
                                if (this.odChkLine == null)
                                    this.odChkLine = new lengthMeasure();
                                this.odChkLine.HandleXML(xmlReader.ReadSubtree(), isNew);
                                break;
                            case "lenChkLine":
                                if (this.lenChkLine == null)
                                    this.lenChkLine = new lengthMeasure();
                                this.lenChkLine.HandleXML(xmlReader.ReadSubtree(), isNew);
                                break;
                            case "idKillLine":
                                if (this.idKillLine == null)
                                    this.idKillLine = new lengthMeasure();
                                this.idKillLine.HandleXML(xmlReader.ReadSubtree(), isNew);
                                break;
                            case "odKillLine":
                                if (this.odKillLine == null)
                                    this.odKillLine = new lengthMeasure();
                                this.odKillLine.HandleXML(xmlReader.ReadSubtree(), isNew);
                                break;
                            case "lenKillLine":
                                if (this.lenKillLine == null)
                                    this.lenKillLine = new lengthMeasure();
                                this.lenKillLine.HandleXML(xmlReader.ReadSubtree(), isNew);
                                break;
                            case "bopComponent":
                                //collection object
                                StaticHelper.AddUpdateWithUid<cs_bopComponent>(xmlReader, isBopCompNew, ref listBopComp);
                                break;
                            case "typeDiverter":
                                this.typeDiverter = StaticHelper.ReadString(xmlReader);
                                break;
                            case "diaDiverter":
                                if (this.diaDiverter == null)
                                    this.diaDiverter = new lengthMeasure();
                                this.diaDiverter.HandleXML(xmlReader.ReadSubtree(), isNew);
                                break;
                            case "presWorkDiverter":
                                if (this.presWorkDiverter == null)
                                    this.presWorkDiverter = new pressureMeasure();
                                this.presWorkDiverter.HandleXML(xmlReader.ReadSubtree(), isNew);
                                break;
                            case "accumulator":
                                this.accumulator = StaticHelper.ReadString(xmlReader);
                                break;
                            case "capAccFluid":
                                if (this.capAccFluid == null)
                                    this.capAccFluid = new volumeMeasure();
                                this.capAccFluid.HandleXML(xmlReader.ReadSubtree(), isNew);
                                break;
                            case "presAccPreCharge":
                                if (this.presAccPreCharge == null)
                                    this.presAccPreCharge = new pressureMeasure();
                                this.presAccPreCharge.HandleXML(xmlReader.ReadSubtree(), isNew);
                                break;
                            case "volAccPreCharge":
                                if (this.volAccPreCharge == null)
                                    this.volAccPreCharge = new volumeMeasure();
                                this.volAccPreCharge.HandleXML(xmlReader.ReadSubtree(), isNew);
                                break;
                            case "presAccOpRating":
                                if (this.presAccOpRating == null)
                                    this.presAccOpRating = new pressureMeasure();
                                this.presAccOpRating.HandleXML(xmlReader.ReadSubtree(), isNew);
                                break;
                            case "typeControlManifold":
                                this.typeControlManifold = StaticHelper.ReadString(xmlReader);
                                break;
                            case "descControlManifold":
                                this.descControlManifold = StaticHelper.ReadString(xmlReader);
                                break;
                            case "typeChokeManifold":
                                this.typeChokeManifold = StaticHelper.ReadString(xmlReader);
                                break;
                            case "presChokeManifold":
                                if (this.presChokeManifold == null)
                                    this.presChokeManifold = new pressureMeasure();
                                this.presChokeManifold.HandleXML(xmlReader.ReadSubtree(), isNew);
                                break;
                            default:
                                break;
                        }
                    }
                }

                //assign back to list
                this.nameTag = listNameTag.ToArray();
                this.bopComponent = listBopComp.ToArray();
            }
        }
    }
}
