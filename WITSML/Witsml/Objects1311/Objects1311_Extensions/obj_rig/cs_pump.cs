 

using System;
using System.Collections.Generic;
using System.Text;
using System.Xml;

namespace Witsml.Objects1311
{
    public partial class cs_pump : IHandleXML, IHasID
    {
        //this will need to be ignored
        [System.Xml.Serialization.XmlIgnore]
        public string UID
        {
            get { return this.uid; }
        }
        public void HandleXML(XmlReader xmlReader, bool isNew)
        {
            //condition for add/update
            bool isNameTagNew = this.nameTag == null;
            //temporary list
            List<cs_nameTag> listNameTag = this.nameTag == null ? new List<cs_nameTag>() : new List<cs_nameTag>(this.nameTag);
            using (xmlReader)
            {
                xmlReader.MoveToContent();
                if (xmlReader.HasAttributes)
                {
                    if (xmlReader.MoveToAttribute("uid"))
                        StaticHelper.HandleUID(xmlReader.Value, "pump uid", out this.uid);
                    xmlReader.MoveToElement();
                }
                while (xmlReader.Read())
                {
                    //only take child node
                    if (xmlReader.Depth == 1 && xmlReader.IsStartElement() && !xmlReader.IsEmptyElement)
                    {
                        switch (xmlReader.Name)
                        {
                            case "index":
                                this.index = StaticParser.ParseShortFromString(StaticHelper.ReadString(xmlReader));
                                break;
                            case "manufacturer":
                                this.manufacturer = StaticHelper.ReadString(xmlReader);
                                break;
                            case "model":
                                this.model = StaticHelper.ReadString(xmlReader);
                                break;
                            case "dTimInstall":
                                this.dTimInstall = StaticParser.ParseDateTimeFromString(StaticHelper.ReadString(xmlReader));
                                this.dTimInstallSpecified = true;
                                break;
                            case "dTimRemove":
                                this.dTimRemove = StaticParser.ParseDateTimeFromString(StaticHelper.ReadString(xmlReader));
                                this.dTimRemoveSpecified = true;
                                break;
                            case "owner":
                                this.owner = StaticHelper.ReadString(xmlReader);
                                break;
                            case "typePump":
                                StaticParser.SetEnumFromString<PumpType>(StaticHelper.ReadString(xmlReader), out this.typePump, out this.typePumpSpecified);
                                break;
                            case "numCyl":
                                this.numCyl = StaticParser.ParseShortFromString(StaticHelper.ReadString(xmlReader));
                                this.numCylSpecified = true;
                                break;
                            case "odRod":
                                if (this.odRod == null)
                                    this.odRod = new lengthMeasure();
                                this.odRod.HandleXML(xmlReader.ReadSubtree(), isNew);
                                break;
                            case "idLiner":
                                if (this.idLiner == null)
                                    this.idLiner = new lengthMeasure();
                                this.idLiner.HandleXML(xmlReader.ReadSubtree(), isNew);
                                break;
                            case "pumpAction":
                                this.pumpAction = StaticParser.ParseShortFromString(StaticHelper.ReadString(xmlReader));
                                this.pumpActionSpecified = true;
                                break;
                            case "eff":
                                if (this.eff == null)
                                    this.eff = new relativePowerMeasure();
                                this.eff.HandleXML(xmlReader.ReadSubtree(), isNew);
                                break;
                            case "lenStroke":
                                if (this.lenStroke == null)
                                    this.lenStroke = new lengthMeasure();
                                this.lenStroke.HandleXML(xmlReader.ReadSubtree(), isNew);
                                break;
                            case "presMx":
                                if (this.presMx == null)
                                    this.presMx = new pressureMeasure();
                                this.presMx.HandleXML(xmlReader.ReadSubtree(), isNew);
                                break;
                            case "powHydMx":
                                if (this.powHydMx == null)
                                    this.powHydMx = new powerMeasure();
                                this.powHydMx.HandleXML(xmlReader.ReadSubtree(), isNew);
                                break;
                            case "spmMx":
                                if (this.spmMx == null)
                                    this.spmMx = new anglePerTimeMeasure();
                                this.spmMx.HandleXML(xmlReader.ReadSubtree(), isNew);
                                break;
                            case "displacement":
                                if (this.displacement == null)
                                    this.displacement = new volumeMeasure();
                                this.displacement.HandleXML(xmlReader.ReadSubtree(), isNew);
                                break;
                            case "presDamp":
                                if (this.presDamp == null)
                                    this.presDamp = new pressureMeasure();
                                this.presDamp.HandleXML(xmlReader.ReadSubtree(), isNew);
                                break;
                            case "volDamp":
                                if (this.volDamp == null)
                                    this.volDamp = new volumeMeasure();
                                this.volDamp.HandleXML(xmlReader.ReadSubtree(), isNew);
                                break;
                            case "powMechMx":
                                if (this.powMechMx == null)
                                    this.powMechMx = new powerMeasure();
                                this.powMechMx.HandleXML(xmlReader.ReadSubtree(), isNew);
                                break;
                            case "nameTag":
                                //collection object
                                StaticHelper.AddUpdateWithUid<cs_nameTag>(xmlReader, isNameTagNew, ref listNameTag);
                                break;
                            default:
                                break;
                        }
                    }
                }
                //assign to list
                this.nameTag = listNameTag.ToArray();
            }
        }

    }
}
