 

using System;
using System.Collections.Generic;
using System.Text;
using System.Xml;

namespace Witsml.Objects1311
{
    public partial class cs_shaker : IHandleXML, IHasID
    {
        //this will need to be ignored
        [System.Xml.Serialization.XmlIgnore]
        public string UID
        {
            get { return this.uid; }
        }
        public void HandleXML(XmlReader xmlReader, bool isNew)
        {
            bool isNameTagNew = this.nameTag == null;

            List<cs_nameTag> listNameTag = this.nameTag == null ? new List<cs_nameTag>() : new List<cs_nameTag>(this.nameTag);
            using (xmlReader)
            {
                xmlReader.MoveToContent();
                if (xmlReader.HasAttributes)
                {
                    if (xmlReader.MoveToAttribute("uid"))
                        StaticHelper.HandleUID(xmlReader.Value, "shaker uid", out this.uid);
                    xmlReader.MoveToElement();
                }
                while (xmlReader.Read())
                {
                    //only take child node
                    if (xmlReader.Depth == 1 && xmlReader.IsStartElement() && !xmlReader.IsEmptyElement)
                    {
                        switch (xmlReader.Name)
                        {
                            case "name":
                                this.name = StaticHelper.ReadString(xmlReader);
                                break;
                            case "manufacturer":
                                this.manufacturer = StaticHelper.ReadString(xmlReader);
                                break;
                            case "model":
                                this.model = StaticHelper.ReadString(xmlReader);
                                break;
                            case "dTimInstall":
                                this.dTimInstall = StaticParser.ParseDateTimeFromString(StaticHelper.ReadString(xmlReader));
                                this.dTimInstallSpecified = true;
                                break;
                            case "dTimRemove":
                                this.dTimRemove = StaticParser.ParseDateTimeFromString(StaticHelper.ReadString(xmlReader));
                                this.dTimRemoveSpecified = true;
                                break;
                            case "type":
                                this.type = StaticHelper.ReadString(xmlReader);
                                break;
                            case "locationShaker":
                                this.locationShaker = StaticHelper.ReadString(xmlReader);
                                break;
                            case "numDecks":
                                this.numDecks = StaticParser.ParseShortFromString(StaticHelper.ReadString(xmlReader));
                                this.numDecksSpecified = true;
                                break;
                            case "numCascLevel":
                                this.numCascLevel = StaticParser.ParseShortFromString(StaticHelper.ReadString(xmlReader));
                                this.numCascLevelSpecified = true;
                                break;
                            case "mudCleaner":
                                this.mudCleaner = StaticParser.ParseBooleanFromString(StaticHelper.ReadString(xmlReader));
                                this.mudCleanerSpecified = true;
                                break;
                            case "capFlow":
                                //object
                                if (this.capFlow == null)
                                    this.capFlow = new volumeFlowRateMeasure();
                                this.capFlow.HandleXML(xmlReader.ReadSubtree(), isNew);
                                break;
                            case "owner":
                                this.owner = StaticHelper.ReadString(xmlReader);
                                break;
                            case "sizeMeshMn":
                                if (this.sizeMeshMn == null)
                                    this.sizeMeshMn = new lengthMeasure();
                                this.sizeMeshMn.HandleXML(xmlReader.ReadSubtree(), isNew);
                                break;
                            case "nameTag":
                                //collection object
                                StaticHelper.AddUpdateWithUid<cs_nameTag>(xmlReader, isNameTagNew, ref listNameTag);
                                break;
                            default:
                                break;
                        }
                    }
                }
                //assign to list
                this.nameTag = listNameTag.ToArray();
            }
        }

    }
}
