 

using System;
using System.Collections.Generic;
using System.IO;
using System.Text;
using System.Xml;
using Witsml.Common.Exceptions;
using Witsml.Exceptions;


namespace Witsml.Objects1311
{
    [System.Xml.Serialization.XmlRootAttribute("rig", Namespace = "http://www.witsml.org/schemas/131", IsNullable = false)]
    public partial class obj_rig : WITSMLObject, IHandleXML
    {
        public obj_rig()
        {
            this.typeRig = RigType.unknown;
            this.typeDerrick = DerrickType.unknown;
            this.typeDrawWorks = DrawWorksType.unknown;
            this.rotSystem = DriveType.unknown;
        }

        /// <summary>
        /// Decode XML string into rig
        /// </summary>
        /// <param name="xmlIn">XML string</param>
        public override void Decode(ref string xmlIn)
        {
            try
            {
                //Consume XML string and convert node value to variable based on node name
                using (StringReader stringReader = new StringReader(xmlIn))
                {
                    XmlReaderSettings setting = new XmlReaderSettings();
                    setting.ConformanceLevel = ConformanceLevel.Document;
                    setting.IgnoreComments = true;
                    setting.IgnoreWhitespace = true;
                    setting.IgnoreProcessingInstructions = true;
                    using (XmlReader xmlReader = XmlReader.Create(stringReader, setting))
                    {
                        //determine add or update operation
                        bool isNew;
                        if (this.uid == string.Empty)
                            isNew = true;
                        else isNew = false;

                        this.HandleXML(xmlReader, isNew);
                    }
                }
            }
            catch (XmlException ex)
            {
                throw new Exceptions.UnableToDecodeException(ex.Message, WITSMLReturnCode.WITSMLDocumentError, ex.InnerException);
            }
            catch (NullReferenceException ex)
            {
                throw new Exceptions.UnableToDecodeException(ex.Message, WITSMLReturnCode.WITSMLEmptyDocument, ex.InnerException);
            }
            catch (Exceptions.UnableToDecodeException ex)
            {
                throw ex;
            }
            catch (Exception ex)
            {
                throw new Exceptions.UnableToDecodeException(ex.Message, WITSMLReturnCode.WITSMLUnkownCause, ex.InnerException);
            }
        }

        /// <summary>
        /// Convert XML string data into rig fields value
        /// </summary>
        /// <param name="xmlReader">reader that has Rig node</param>
        /// <param name="IsNew">Is the object from stratch(true) or from db/serialization(false)</param>
        public void HandleXML(XmlReader xmlReader, bool isNew)
        {
            //condition for add/update
            bool isPitNew = this.pit == null;
            bool isPumpNew = this.pump == null;
            bool isShakerNew = this.shaker == null;
            bool isCentrifugeNew = this.centrifuge == null;
            bool ishydroNew = this.hydrocyclone == null;
            bool isDegasserNew = this.degasser == null;

            //temporary list
            List<cs_pit> listPit = this.pit == null ? new List<cs_pit>() : new List<cs_pit>(this.pit);
            List<cs_pump> listPump = this.pump == null ? new List<cs_pump>() : new List<cs_pump>(this.pump);
            List<cs_shaker> listShaker = this.shaker == null ? new List<cs_shaker>() : new List<cs_shaker>(this.shaker);
            List<cs_centrifuge> listCentrifuge = this.centrifuge == null ? new List<cs_centrifuge>() : new List<cs_centrifuge>(this.centrifuge);
            List<cs_hydrocyclone> listHydro = this.hydrocyclone == null ? new List<cs_hydrocyclone>() : new List<cs_hydrocyclone>(this.hydrocyclone);
            List<cs_degasser> listDegasser = this.degasser == null ? new List<cs_degasser>() : new List<cs_degasser>(this.degasser);

            using (xmlReader)
            {
                //assign uid for the new well
                xmlReader.MoveToContent();
                if (xmlReader.HasAttributes)
                {
                    if (xmlReader.MoveToAttribute("uidWell"))
                        StaticHelper.HandleUID(xmlReader.Value, "rig uidWell", out this.uidWell);
                    if (xmlReader.MoveToAttribute("uidWellbore"))
                        StaticHelper.HandleUID(xmlReader.Value, "rig uidWellbore", out this.uidWellbore);
                    if (xmlReader.MoveToAttribute("uid"))
                        StaticHelper.HandleUID(xmlReader.Value, "rig uid", out this.uid);
                    xmlReader.MoveToElement();
                }
                while (xmlReader.Read())
                {
                    //only take child node
                    if (xmlReader.Depth == 1 && xmlReader.IsStartElement() && !xmlReader.IsEmptyElement)
                    {
                        switch (xmlReader.Name)
                        {
                            case "nameWell":
                                this.nameWell = StaticHelper.ReadString(xmlReader);
                                break;
                            case "nameWellbore":
                                this.nameWellbore = StaticHelper.ReadString(xmlReader);
                                break;
                            case "name":
                                this.name = StaticHelper.ReadString(xmlReader);
                                break;
                            case "owner":
                                this.owner = StaticHelper.ReadString(xmlReader);
                                break;
                            case "typeRig":
                                StaticParser.SetEnumFromString<RigType>(StaticHelper.ReadString(xmlReader), out this.typeRig, out this.typeRigSpecified);
                                break;
                            case "manufacturer":
                                this.manufacturer = StaticHelper.ReadString(xmlReader);
                                break;
                            case "yearEntService":
                                this.yearEntService = StaticHelper.ReadString(xmlReader);
                                break;
                            case "classRig":
                                this.classRig = StaticHelper.ReadString(xmlReader);
                                break;
                            case "approvals":
                                this.approvals = StaticHelper.ReadString(xmlReader);
                                break;
                            case "registration":
                                this.registration = StaticHelper.ReadString(xmlReader);
                                break;
                            case "telNumber":
                                this.telNumber = StaticHelper.ReadString(xmlReader);
                                break;
                            case "faxNumber":
                                this.faxNumber = StaticHelper.ReadString(xmlReader);
                                break;
                            case "emailAddress":
                                this.emailAddress = StaticHelper.ReadString(xmlReader);
                                break;
                            case "nameContact":
                                this.nameContact = StaticHelper.ReadString(xmlReader);
                                break;
                            case "ratingDrillDepth":
                                if (this.ratingDrillDepth == null)
                                    this.ratingDrillDepth = new lengthMeasure();
                                this.ratingDrillDepth.HandleXML(xmlReader.ReadSubtree(), isNew);
                                break;
                            case "ratingWaterDepth":
                                if (this.ratingWaterDepth == null)
                                    this.ratingWaterDepth = new lengthMeasure();
                                this.ratingWaterDepth.HandleXML(xmlReader.ReadSubtree(), isNew);
                                break;
                            case "isOffshore":
                                this.isOffshore = StaticParser.ParseBooleanFromString(StaticHelper.ReadString(xmlReader));
                                this.isOffshoreSpecified = true;
                                break;
                            case "airGap":
                                if (this.airGap == null)
                                    this.airGap = new lengthMeasure();
                                this.airGap.HandleXML(xmlReader.ReadSubtree(), isNew);
                                break;
                            case "dTimStartOp":
                                this.dTimStartOp = StaticParser.ParseDateTimeFromString(StaticHelper.ReadString(xmlReader));
                                this.dTimStartOpSpecified = true;
                                break;
                            case "dTimEndOp":
                                this.dTimEndOp = StaticParser.ParseDateTimeFromString(StaticHelper.ReadString(xmlReader));
                                this.dTimEndOpSpecified = true;
                                break;
                            case "bop":
                                //object
                                if (this.bop == null)
                                    this.bop = new cs_bop();
                                this.bop.HandleXML(xmlReader.ReadSubtree(), isNew);
                                break;
                            case "pit":
                                //collection object
                                StaticHelper.AddUpdateWithUid<cs_pit>(xmlReader, isPitNew, ref listPit);
                                break;
                            case "pump":
                                //collection object
                                StaticHelper.AddUpdateWithUid<cs_pump>(xmlReader, isPumpNew, ref listPump);
                                break;
                            case "shaker":
                                //collection object
                                StaticHelper.AddUpdateWithUid<cs_shaker>(xmlReader, isShakerNew, ref listShaker);
                                break;
                            case "centrifuge":
                                //collection object
                                StaticHelper.AddUpdateWithUid<cs_centrifuge>(xmlReader, isCentrifugeNew, ref listCentrifuge);
                                break;
                            case "hydrocyclone":
                                //collection object
                                StaticHelper.AddUpdateWithUid<cs_hydrocyclone>(xmlReader, ishydroNew, ref listHydro);
                                break;
                            case "degasser":
                                //collection object
                                StaticHelper.AddUpdateWithUid<cs_degasser>(xmlReader, isDegasserNew, ref listDegasser);
                                break;
                            case "surfaceEquipment":
                                //object
                                if (this.surfaceEquipment == null)
                                    this.surfaceEquipment = new cs_surfaceEquipment();
                                this.surfaceEquipment.HandleXML(xmlReader.ReadSubtree(), isNew);
                                break;
                            case "numDerricks":
                                this.numDerricks = StaticParser.ParseShortFromString(StaticHelper.ReadString(xmlReader));
                                this.numDerricksSpecified = true;
                                break;
                            case "typeDerrick":
                                StaticParser.SetEnumFromString<DerrickType>(StaticHelper.ReadString(xmlReader), out this.typeDerrick, out this.typeDerrickSpecified);
                                break;
                            case "ratingDerrick":
                                if (this.ratingDerrick == null)
                                    this.ratingDerrick = new forceMeasure();
                                this.ratingDerrick.HandleXML(xmlReader.ReadSubtree(), isNew);
                                break;
                            case "htDerrick":
                                if (this.htDerrick == null)
                                    this.htDerrick = new lengthMeasure();
                                this.htDerrick.HandleXML(xmlReader.ReadSubtree(), isNew);
                                break;
                            case "ratingHkld":
                                if (this.ratingHkld == null)
                                    this.ratingHkld = new forceMeasure();
                                this.ratingHkld.HandleXML(xmlReader.ReadSubtree(), isNew);
                                break;
                            case "capWindDerrick":
                                if (this.capWindDerrick == null)
                                    this.capWindDerrick = new velocityMeasure();
                                this.capWindDerrick.HandleXML(xmlReader.ReadSubtree(), isNew);
                                break;
                            case "wtBlock":
                                if (this.wtBlock == null)
                                    this.wtBlock = new forceMeasure();
                                this.wtBlock.HandleXML(xmlReader.ReadSubtree(), isNew);
                                break;
                            case "ratingBlock":
                                if (this.ratingBlock == null)
                                    this.ratingBlock = new forceMeasure();
                                this.ratingBlock.HandleXML(xmlReader.ReadSubtree(), isNew);
                                break;
                            case "numBlockLines":
                                this.numBlockLines = StaticParser.ParseShortFromString(StaticHelper.ReadString(xmlReader));
                                this.numBlockLinesSpecified = true;
                                break;
                            case "typeHook":
                                this.typeHook = StaticHelper.ReadString(xmlReader);
                                break;
                            case "ratingHook":
                                if (this.ratingHook == null)
                                    this.ratingHook = new forceMeasure();
                                this.ratingHook.HandleXML(xmlReader.ReadSubtree(), isNew);
                                break;
                            case "sizeDrillLine":
                                if (this.sizeDrillLine == null)
                                    this.sizeDrillLine = new lengthMeasure();
                                this.sizeDrillLine.HandleXML(xmlReader.ReadSubtree(), isNew);
                                break;
                            case "typeDrawWorks":
                                StaticParser.SetEnumFromString<DrawWorksType>(StaticHelper.ReadString(xmlReader), out this.typeDrawWorks, out this.typeDrawWorksSpecified);
                                break;
                            case "powerDrawWorks":
                                //object
                                if (this.powerDrawWorks == null)
                                    this.powerDrawWorks = new powerMeasure();
                                this.powerDrawWorks.HandleXML(xmlReader.ReadSubtree(), isNew);
                                break;
                            case "ratingDrawWorks":
                                if (this.ratingDrawWorks == null)
                                    this.ratingDrawWorks = new forceMeasure();
                                this.ratingDrawWorks.HandleXML(xmlReader.ReadSubtree(), isNew);
                                break;
                            case "motorDrawWorks":
                                this.motorDrawWorks = StaticHelper.ReadString(xmlReader);
                                break;
                            case "descBrake":
                                this.descBrake = StaticHelper.ReadString(xmlReader);
                                break;
                            case "typeSwivel":
                                this.typeSwivel = StaticHelper.ReadString(xmlReader);
                                break;
                            case "ratingSwivel":
                                if (this.ratingSwivel == null)
                                    this.ratingSwivel = new forceMeasure();
                                this.ratingSwivel.HandleXML(xmlReader.ReadSubtree(), isNew);
                                break;
                            case "rotSystem":
                                StaticParser.SetEnumFromString<DriveType>(StaticHelper.ReadString(xmlReader), out this.rotSystem, out this.rotSystemSpecified);
                                break;
                            case "descRotSystem":
                                this.descRotSystem = StaticHelper.ReadString(xmlReader);
                                break;
                            case "ratingTqRotSys":
                                if (this.ratingTqRotSys == null)
                                    this.ratingTqRotSys = new momentOfForceMeasure();
                                this.ratingTqRotSys.HandleXML(xmlReader.ReadSubtree(), isNew);
                                break;
                            case "rotSizeOpening":
                                if (this.rotSizeOpening == null)
                                    this.rotSizeOpening = new lengthMeasure();
                                this.rotSizeOpening.HandleXML(xmlReader.ReadSubtree(), isNew);
                                break;
                            case "ratingRotSystem":
                                if (this.ratingRotSystem == null)
                                    this.ratingRotSystem = new forceMeasure();
                                this.ratingRotSystem.HandleXML(xmlReader.ReadSubtree(), isNew);
                                break;
                            case "scrSystem":
                                this.scrSystem = StaticHelper.ReadString(xmlReader);
                                break;
                            case "pipeHandlingSystem":
                                this.pipeHandlingSystem = StaticHelper.ReadString(xmlReader);
                                break;
                            case "capBulkMud":
                                //object
                                if (this.capBulkMud == null)
                                    this.capBulkMud = new volumeMeasure();
                                this.capBulkMud.HandleXML(xmlReader.ReadSubtree(), isNew);
                                break;
                            case "capLiquidMud":
                                //object
                                if (this.capLiquidMud == null)
                                    this.capLiquidMud = new volumeMeasure();
                                this.capLiquidMud.HandleXML(xmlReader.ReadSubtree(), isNew);
                                break;
                            case "capDrillWater":
                                //object
                                if (this.capDrillWater == null)
                                    this.capDrillWater = new volumeMeasure();
                                this.capDrillWater.HandleXML(xmlReader.ReadSubtree(), isNew);
                                break;
                            case "capPotableWater":
                                //object
                                if (this.capPotableWater == null)
                                    this.capPotableWater = new volumeMeasure();
                                this.capPotableWater.HandleXML(xmlReader.ReadSubtree(), isNew);
                                break;
                            case "capFuel":
                                //object
                                if (this.capFuel == null)
                                    this.capFuel = new volumeMeasure();
                                this.capFuel.HandleXML(xmlReader.ReadSubtree(), isNew);
                                break;
                            case "capBulkCement":
                                //object
                                if (this.capBulkCement == null)
                                    this.capBulkCement = new volumeMeasure();
                                this.capBulkCement.HandleXML(xmlReader.ReadSubtree(), isNew);
                                break;
                            case "mainEngine":
                                this.mainEngine = StaticHelper.ReadString(xmlReader);
                                break;
                            case "generator":
                                this.generator = StaticHelper.ReadString(xmlReader);
                                break;
                            case "cementUnit":
                                this.cementUnit = StaticHelper.ReadString(xmlReader);
                                break;
                            case "numBunks":
                                this.numBunks = StaticParser.ParseShortFromString(StaticHelper.ReadString(xmlReader));
                                this.numBunksSpecified = true;
                                break;
                            case "bunksPerRoom":
                                this.bunksPerRoom = StaticParser.ParseShortFromString(StaticHelper.ReadString(xmlReader));
                                this.bunksPerRoomSpecified = true;
                                break;
                            case "numCranes":
                                this.numCranes = StaticParser.ParseShortFromString(StaticHelper.ReadString(xmlReader));
                                this.numCranesSpecified = true;
                                break;
                            case "numAnch":
                                this.numAnch = StaticParser.ParseShortFromString(StaticHelper.ReadString(xmlReader));
                                this.numAnchSpecified = true;
                                break;
                            case "moorType":
                                this.moorType = StaticHelper.ReadString(xmlReader);
                                break;
                            case "numGuideTens":
                                this.numGuideTens = StaticParser.ParseShortFromString(StaticHelper.ReadString(xmlReader));
                                this.numGuideTensSpecified = true;
                                break;
                            case "numRiserTens":
                                this.numRiserTens = StaticParser.ParseShortFromString(StaticHelper.ReadString(xmlReader));
                                this.numRiserTensSpecified = true;
                                break;
                            case "varDeckLdMx":
                                if (this.varDeckLdMx == null)
                                    this.varDeckLdMx = new forceMeasure();
                                this.varDeckLdMx.HandleXML(xmlReader.ReadSubtree(), isNew);
                                break;
                            case "vdlStorm":
                                if (this.vdlStorm == null)
                                    this.vdlStorm = new forceMeasure();
                                this.vdlStorm.HandleXML(xmlReader.ReadSubtree(), isNew);
                                break;
                            case "numThrusters":
                                this.numThrusters = StaticParser.ParseShortFromString(StaticHelper.ReadString(xmlReader));
                                this.numThrustersSpecified = true;
                                break;
                            case "azimuthing":
                                this.azimuthing = StaticParser.ParseBooleanFromString(StaticHelper.ReadString(xmlReader));
                                this.azimuthingSpecified = true;
                                break;
                            case "motionCompensationMn":
                                if (this.motionCompensationMn == null)
                                    this.motionCompensationMn = new forceMeasure();
                                this.motionCompensationMn.HandleXML(xmlReader.ReadSubtree(), isNew);
                                break;
                            case "motionCompensationMx":
                                if (this.motionCompensationMx == null)
                                    this.motionCompensationMx = new forceMeasure();
                                this.motionCompensationMx.HandleXML(xmlReader.ReadSubtree(), isNew);
                                break;
                            case "strokeMotionCompensation":
                                if (this.strokeMotionCompensation == null)
                                    this.strokeMotionCompensation = new lengthMeasure();
                                this.strokeMotionCompensation.HandleXML(xmlReader.ReadSubtree(), isNew);
                                break;
                            case "riserAngleLimit":
                                if (this.riserAngleLimit == null)
                                    this.riserAngleLimit = new planeAngleMeasure();
                                this.riserAngleLimit.HandleXML(xmlReader.ReadSubtree(), isNew);
                                break;
                            case "heaveMx":
                                if (this.heaveMx == null)
                                    this.heaveMx = new lengthMeasure();
                                this.heaveMx.HandleXML(xmlReader.ReadSubtree(), isNew);
                                break;
                            case "gantry":
                                this.gantry = StaticHelper.ReadString(xmlReader);
                                break;
                            case "flares":
                                this.flares = StaticHelper.ReadString(xmlReader);
                                break;
                            case "commonData":
                                if (this.commonData == null)
                                    this.commonData = new cs_commonData();
                                this.commonData.HandleXML(xmlReader.ReadSubtree(), isNew);
                                break;
                            case "customData":
                                if (this.customData == null)
                                    this.customData = new cs_customData();
                                this.customData.HandleXML(xmlReader.ReadSubtree(), isNew);
                                break;
                            default:
                                break;
                        }
                    }
                }
                //assign list to collection
                this.pit = listPit.ToArray();
                this.pump = listPump.ToArray();
                this.shaker = listShaker.ToArray();
                this.centrifuge = listCentrifuge.ToArray();
                this.hydrocyclone = listHydro.ToArray();
                this.degasser = listDegasser.ToArray();
            }
        }

        public override string ValidateGetPath(string newUid)
        {
            // Fix uid if the Object dosn't have one.
            if (!IsValidUid(this.uid))
            {
                this.uid = newUid;
                // TODO: Add some loging that we created a new UID for the object maybe.
            }

            StringBuilder path = new StringBuilder(this.GetParentPath());
            path.Append(@"\");
            path.Append(this.uid);
            return path.ToString();
        }

        public override string GetParentPath()
        {
            if (!IsValidUid(this.uidWell) || !IsValidUid(this.uidWellbore))
            {
                throw new UnableToDecodeException("One or more Parent UID's are invalid", WITSMLReturnCode.WITSMLInvalidParentUID);
                //TODO: Log the issue
            }

            StringBuilder path = new StringBuilder(@"\");
            path.Append(this.uidWell);
            path.Append(@"\");
            path.Append(this.uidWellbore);
            return path.ToString();
        }

        // NOTE: these are done with functions, instead of property access, so as not to mess around with the auto-generated / serialized classes.
        public override string Get_uid()
        {
            return this.uid;
        }

        public override string Get_uidWell()
        {
            return this.uidWell;
        }

        public override string Get_uidWellbore()
        {
            return this.uidWellbore;
        }

        public override string Get_name()
        {
            return this.name;
        }

        public override string Get_nameWell()
        {
            return this.nameWell;
        }

        public override string Get_nameWellbore()
        {
            return this.nameWellbore;
        }

        public override ICommonData Get_commonData()
        {
            if (this.commonData == null)
            {
                this.commonData = new cs_commonData();
            }
            return (ICommonData)this.commonData;
        }
        public override ICustomData Get_customData()
        {
            if (this.customData == null)
            {
                this.customData = new cs_customData();
            }
            return (ICustomData)this.customData;
        }
    }
}
