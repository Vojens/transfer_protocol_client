 

using System;
using System.Collections.Generic;
using System.Text;
using System.Xml;

namespace Witsml.Objects1311
{
    public partial class cs_surfaceEquipment : IHandleXML
    {
        public cs_surfaceEquipment()
        {
            this.typeSurfEquip = SurfEquipType.unknown;

        }
        public void HandleXML(XmlReader xmlReader, bool isNew)
        {
            using (xmlReader)
            {
                while (xmlReader.Read())
                {
                    //only take child node
                    if (xmlReader.Depth == 1 && xmlReader.IsStartElement() && !xmlReader.IsEmptyElement)
                    {
                        switch (xmlReader.Name)
                        {
                            case "description":
                                this.description = StaticHelper.ReadString(xmlReader);
                                break;
                            case "presRating":
                                //object
                                if (this.presRating == null)
                                    this.presRating = new pressureMeasure();
                                this.presRating.HandleXML(xmlReader.ReadSubtree(), isNew);
                                break;
                            case "typeSurfEquip":
                                this.typeSurfEquip = StaticParser.ParseEnumFromString<SurfEquipType>(StaticHelper.ReadString(xmlReader), SurfEquipType.unknown);
                                break;
                            case "usePumpDischarge":
                                this.usePumpDischarge = StaticParser.ParseBooleanFromString(StaticHelper.ReadString(xmlReader));
                                this.usePumpDischargeSpecified = true;
                                break;
                            case "useStandpipe":
                                this.useStandpipe = StaticParser.ParseBooleanFromString(StaticHelper.ReadString(xmlReader));
                                this.useStandpipeSpecified = true;
                                break;
                            case "useHose":
                                this.useHose = StaticParser.ParseBooleanFromString(StaticHelper.ReadString(xmlReader));
                                this.useHoseSpecified = true;
                                break;
                            case "useSwivel":
                                this.useSwivel = StaticParser.ParseBooleanFromString(StaticHelper.ReadString(xmlReader));
                                this.useSwivelSpecified = true;
                                break;
                            case "useKelly":
                                this.useKelly = StaticParser.ParseBooleanFromString(StaticHelper.ReadString(xmlReader));
                                this.useKellySpecified = true;
                                break;
                            case "useTopStack":
                                this.useTopStack = StaticParser.ParseBooleanFromString(StaticHelper.ReadString(xmlReader));
                                this.useTopStackSpecified = true;
                                break;
                            case "useInjStack":
                                this.useInjStack = StaticParser.ParseBooleanFromString(StaticHelper.ReadString(xmlReader));
                                this.useInjStackSpecified = true;
                                break;
                            case "useSurfaceIron":
                                this.useSurfaceIron = StaticParser.ParseBooleanFromString(StaticHelper.ReadString(xmlReader));
                                this.useSurfaceIronSpecified = true;
                                break;
                            case "idStandpipe":
                                if (this.idStandpipe == null)
                                    this.idStandpipe = new lengthMeasure();
                                this.idStandpipe.HandleXML(xmlReader.ReadSubtree(), isNew);
                                break;
                            case "lenStandpipe":
                                if (this.lenStandpipe == null)
                                    this.lenStandpipe = new lengthMeasure();
                                this.lenStandpipe.HandleXML(xmlReader.ReadSubtree(), isNew);
                                break;
                            case "idHose":
                                if (this.idHose == null)
                                    this.idHose = new lengthMeasure();
                                this.idHose.HandleXML(xmlReader.ReadSubtree(), isNew);
                                break;
                            case "lenHose":
                                if (this.lenHose == null)
                                    this.lenHose = new lengthMeasure();
                                this.lenHose.HandleXML(xmlReader.ReadSubtree(), isNew);
                                break;
                            case "idSwivel":
                                if (this.idSwivel == null)
                                    this.idSwivel = new lengthMeasure();
                                this.idSwivel.HandleXML(xmlReader.ReadSubtree(), isNew);
                                break;
                            case "lenSwivel":
                                if (this.lenSwivel == null)
                                    this.lenSwivel = new lengthMeasure();
                                this.lenSwivel.HandleXML(xmlReader.ReadSubtree(), isNew);
                                break;
                            case "idKelly":
                                if (this.idKelly == null)
                                    this.idKelly = new lengthMeasure();
                                this.idKelly.HandleXML(xmlReader.ReadSubtree(), isNew);
                                break;
                            case "lenKelly":
                                if (this.lenKelly == null)
                                    this.lenKelly = new lengthMeasure();
                                this.lenKelly.HandleXML(xmlReader.ReadSubtree(), isNew);
                                break;
                            case "idSurfaceIron":
                                if (this.idSurfaceIron == null)
                                    this.idSurfaceIron = new lengthMeasure();
                                this.idSurfaceIron.HandleXML(xmlReader.ReadSubtree(), isNew);
                                break;
                            case "lenSurfaceIron":
                                if (this.lenSurfaceIron == null)
                                    this.lenSurfaceIron = new lengthMeasure();
                                this.lenSurfaceIron.HandleXML(xmlReader.ReadSubtree(), isNew);
                                break;
                            case "htSurfaceIron":
                                if (this.htSurfaceIron == null)
                                    this.htSurfaceIron = new lengthMeasure();
                                this.htSurfaceIron.HandleXML(xmlReader.ReadSubtree(), isNew);
                                break;
                            case "idDischargeLine":
                                if (this.idDischargeLine == null)
                                    this.idDischargeLine = new lengthMeasure();
                                this.idDischargeLine.HandleXML(xmlReader.ReadSubtree(), isNew);
                                break;
                            case "lenDischargeLine":
                                if (this.lenDischargeLine == null)
                                    this.lenDischargeLine = new lengthMeasure();
                                this.lenDischargeLine.HandleXML(xmlReader.ReadSubtree(), isNew);
                                break;
                            case "ctWrapType":
                                ctWrapType = StaticHelper.ReadString(xmlReader);
                                break;
                            case "odReel":
                                if (this.odReel == null)
                                    this.odReel = new lengthMeasure();
                                this.odReel.HandleXML(xmlReader.ReadSubtree(), isNew);
                                break;
                            case "odCore":
                                if (this.odCore == null)
                                    this.odCore = new lengthMeasure();
                                this.odCore.HandleXML(xmlReader.ReadSubtree(), isNew);
                                break;
                            case "widReelWrap":
                                if (this.widReelWrap == null)
                                    this.widReelWrap = new lengthMeasure();
                                this.widReelWrap.HandleXML(xmlReader.ReadSubtree(), isNew);
                                break;
                            case "lenReel":
                                if (this.lenReel == null)
                                    this.lenReel = new lengthMeasure();
                                this.lenReel.HandleXML(xmlReader.ReadSubtree(), isNew);
                                break;
                            case "injStkUp":
                                this.injStkUp = StaticParser.ParseBooleanFromString(StaticHelper.ReadString(xmlReader));
                                this.injStkUpSpecified = true;
                                break;
                            case "htInjStk":
                                if (this.htInjStk == null)
                                    this.htInjStk = new lengthMeasure();
                                this.htInjStk.HandleXML(xmlReader.ReadSubtree(), isNew);
                                break;
                            case "umbInside":
                                this.umbInside = StaticParser.ParseBooleanFromString(StaticHelper.ReadString(xmlReader), isNew);
                                this.umbInsideSpecified = true;
                                break;
                            case "odUmbilical":
                                if (this.odUmbilical == null)
                                    this.odUmbilical = new lengthMeasure();
                                this.odUmbilical.HandleXML(xmlReader.ReadSubtree(), isNew);
                                break;
                            case "lenUmbilical":
                                if (this.lenUmbilical == null)
                                    this.lenUmbilical = new lengthMeasure();
                                this.lenUmbilical.HandleXML(xmlReader.ReadSubtree(), isNew);
                                break;
                            case "idTopStk":
                                if (this.idTopStk == null)
                                    this.idTopStk = new lengthMeasure();
                                this.idTopStk.HandleXML(xmlReader.ReadSubtree(), isNew);
                                break;
                            case "htTopStk":
                                if (this.htTopStk == null)
                                    this.htTopStk = new lengthMeasure();
                                this.htTopStk.HandleXML(xmlReader.ReadSubtree(), isNew);
                                break;
                            case "htFlange":
                                if (this.htFlange == null)
                                    this.htFlange = new lengthMeasure();
                                this.htFlange.HandleXML(xmlReader.ReadSubtree(), isNew);
                                break;
                            default:
                                break;
                        }
                    }
                }
            }
        }
    }
}
