 

using System;
using System.Collections.Generic;
using System.Text;
using System.Xml;
using System.Linq;
using Witsml.Common.Exceptions;


namespace Witsml.Objects1311
{
    public partial class cs_wellLogCurveInfo : IHandleXML
    {
        [System.Xml.Serialization.XmlIgnore]
        public ObjectState objectState;


        public cs_wellLogCurveInfo()
        {
            this.traceState = LogTraceState.unknown;
            this.traceOrigin = LogTraceOrigin.unknown;
        }

        /// <summary>
        /// Convert XML string data into logCurveInfo fields value
        /// </summary>
        /// <param name="xmlReader">reader that has logCurveInfo node</param>
        /// <param name="IsNew">Is the object from stratch(true) or from db/serialization(false)</param>
        public void HandleXML(XmlReader xmlReader, bool isNew)
        {
            //condition choice for item
            bool hasChoice = false;

            //local flag to check for required mandatory filled in
            bool isMnemonicSpecified = false;
            bool isColumnIndexSpecified = false;

            //will be used to determine add or update operation for collection
            bool isAxisNew = this.axisDefinition == null;
            //create temporary list for data manipulation
            List<cs_axisDefinition> listAxis = isAxisNew ? new List<cs_axisDefinition>() : new List<cs_axisDefinition>(this.axisDefinition);

            //Create temporary list to help with data manipulation
            using (xmlReader)
            {
                //read first node and assign uid and other attribute of the object
                xmlReader.Read();
                if (xmlReader.HasAttributes)
                {
                    //generate uid if uid empty or no uid
                    if (xmlReader.MoveToAttribute("uid"))
                        if (xmlReader.Value != string.Empty)
                            StaticHelper.HandleUID(xmlReader.Value, "wellLogCurveInfo uid", out this.uid);
                        else//uid empty
                            this.uid = StaticHelper.GenerateObjectUID();//empty uid,so generate one for it
                    else//no uid
                        this.uid = StaticHelper.GenerateObjectUID();//empty uid,so generate one for it

                    xmlReader.MoveToElement();
                }
                while (xmlReader.Read())
                {
                    //start on depth 1 which are the childs that we want to check against
                    //check for starting tag and convert node value to its expected variable
                    if (xmlReader.Depth == 1 && xmlReader.IsStartElement() && !xmlReader.IsEmptyElement)
                    {
                        switch (xmlReader.Name)
                        {
                            case "mnemonic":
                                this.mnemonic = StaticHelper.ReadString(xmlReader);
                                //check whether there is a value for mandatory field
                                if (this.mnemonic != string.Empty)
                                    isMnemonicSpecified = true;
                                else
                                    //mandatory field is empty
                                    throw new Exceptions.UnableToDecodeException("Mnemonic is not specified", WITSMLReturnCode.WITSMLMnemonicNotSpecified);
                                break;
                            case "classWitsml":
                                this.classWitsml = StaticHelper.ReadString(xmlReader);
                                break;
                            case "unit":
                                this.unit = StaticHelper.ReadString(xmlReader);
                                break;
                            case "mnemAlias":
                                this.mnemAlias = StaticHelper.ReadString(xmlReader);
                                break;
                            case "nullValue":
                                this.nullValue = StaticHelper.ReadString(xmlReader);
                                break;
                            case "alternateIndex":
                                this.alternateIndex = StaticParser.ParseBooleanFromString(StaticHelper.ReadString(xmlReader));
                                this.alternateIndexSpecified = true;
                                break;
                            case "wellDatum":
                                //object
                                if (this.wellDatum == null)
                                    this.wellDatum = new refNameString();
                                this.wellDatum.ObjectName = "wellDatum";
                                this.wellDatum.HandleXML(xmlReader.ReadSubtree(), isNew);
                                break;
                            case "curveDescription":
                                this.curveDescription = StaticHelper.ReadString(xmlReader);
                                break;
                            case "sensorOffset":
                                //object
                                if (this.sensorOffset == null)
                                    this.sensorOffset = new lengthMeasure();
                                this.sensorOffset.HandleXML(xmlReader.ReadSubtree(), isNew);
                                break;
                            case "dataSource":
                                this.dataSource = StaticHelper.ReadString(xmlReader);
                                break;
                            case "densData":
                                //object
                                if (this.densData == null)
                                    this.densData = new perLengthMeasure();
                                this.densData.HandleXML(xmlReader.ReadSubtree(), isNew);
                                break;
                            case "traceState":
                                StaticParser.SetEnumFromString<LogTraceState>(StaticHelper.ReadString(xmlReader), out this.traceState, out this.traceStateSpecified);
                                break;
                            case "traceOrigin":
                                StaticParser.SetEnumFromString<LogTraceOrigin>(StaticHelper.ReadString(xmlReader), out this.traceOrigin, out this.traceOriginSpecified);
                                break;
                            case "encodingDef":
                                if (!hasChoice)
                                {
                                    this.Item = StaticParser.ParseEnumFromString<ArrayElementDataType>(StaticHelper.ReadString(xmlReader), ArrayElementDataType.boolean);
                                    hasChoice = true;
                                }
                                break;
                            case "typeLogData":
                                if (!hasChoice)
                                {
                                    this.Item = StaticParser.ParseEnumFromString<LogDataType>(StaticHelper.ReadString(xmlReader), LogDataType.unknown);
                                    hasChoice = true;
                                }
                                break;
                            case "axisDefinition":
                                //multiple objects with uid
                                cs_axisDefinition axisObj;
                                string currentAxisUID = string.Empty;

                                if (xmlReader.HasAttributes)
                                {
                                    //check for uid attribute and assign it to temporary string for searching in collection
                                    if (xmlReader.MoveToAttribute("uid"))
                                    {
                                        currentAxisUID = xmlReader.Value;
                                        xmlReader.MoveToElement();
                                    }
                                }

                                //handling for recursive object with no uid
                                if (String.IsNullOrEmpty(currentAxisUID))
                                {
                                    //Delete/Clear Operation for update 
                                    //Skip Operation for Add
                                    if (!isAxisNew)
                                    {
                                        listAxis.Clear();
                                    }
                                    continue;
                                }

                                //Check Operation to be perform
                                if (!isAxisNew)
                                {
                                    //Update Operation
                                    if (xmlReader.HasAttributes)
                                    {
                                        //check for uid attribute and assign it to temporary string for searching in collection
                                        //look for wellDatum based on its UID and update the well with xml string
                                        axisObj = listAxis.FirstOrDefault(
                                            delegate(cs_axisDefinition objAxis)
                                            { if (objAxis.uid == currentAxisUID)return true; else return false; });
                                        if (axisObj != null)
                                        {
                                            axisObj.HandleXML(xmlReader.ReadSubtree(), isAxisNew);
                                            continue;
                                        }
                                    }
                                }

                                //Add Operation
                                axisObj = new cs_axisDefinition();
                                axisObj.HandleXML(xmlReader.ReadSubtree(), isAxisNew);
                                listAxis.Add(axisObj);
                                break;
                            default:
                                break;
                        }


                    }
                }
                //check for mandatory field
                if (!isMnemonicSpecified)
                    throw new Exceptions.UnableToDecodeException("Mnemonic is not specified",WITSMLReturnCode.WITSMLMnemonicNotSpecified);
                // TODO: Commented by Mark.  Needs further refining.
                if (!isColumnIndexSpecified)
                {
                    // If no Column index, AND were doing an 'Update' then infer the column index from the position in the XML file
                    // This is a 'fix' to handle non-standard 1.3 and standard 1.2.0 implentations.  (1.2.0 ColumnIndex is not Mandatory)



                    //    throw new Exceptions.UnableToDecodeException("Column Index is not specified");
                }

                //assign copy of list back to collection
                this.axisDefinition = listAxis.ToArray();
            }
        }

    }
}
