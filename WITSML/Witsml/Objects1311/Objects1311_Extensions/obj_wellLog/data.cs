 

using System;
using System.Collections.Generic;
using System.Text;
using System.Xml;

namespace Witsml.Objects1311
{
    public partial class data : IHandleXML
    {
        public void HandleXML(XmlReader xmlReader, bool isNew)
        {
            using (xmlReader)
            {
                xmlReader.Read();
                if (xmlReader.HasAttributes)
                {
                    if (xmlReader.MoveToAttribute("id"))
                        this.id = xmlReader.Value;
                    xmlReader.MoveToElement();
                }
                this.Value = StaticHelper.ReadString(xmlReader).Replace(System.Environment.NewLine,"");
            }
        }

    }
}
