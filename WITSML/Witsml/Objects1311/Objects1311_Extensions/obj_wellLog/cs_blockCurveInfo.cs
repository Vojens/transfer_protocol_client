 

using System;
using System.Collections.Generic;
using System.Text;
using System.Xml;

namespace Witsml.Objects1311
{
    public partial class cs_blockCurveInfo : IHandleXML, IHasID
    {
        //this will need to be ignored
        [System.Xml.Serialization.XmlIgnore]
        public string UID
        {
            get { return this.uid; }
        }
        [System.Xml.Serialization.XmlIgnore]
        public ObjectState objectState;
        public void HandleXML(XmlReader xmlReader, bool isNew)
        {
            using (xmlReader)
            {
                xmlReader.MoveToContent();
                if (xmlReader.HasAttributes)
                {
                    if (xmlReader.MoveToAttribute("uid"))
                        StaticHelper.HandleUID(xmlReader.Value, "blockCurveInfo uid", out this.uid);
                    xmlReader.MoveToElement();
                }
                while (xmlReader.Read())
                {
                    //only take child node
                    if (xmlReader.Depth == 1 && xmlReader.IsStartElement() && !xmlReader.IsEmptyElement)
                    {
                        switch (xmlReader.Name)
                        {
                            case "curveId":
                                this.curveId = StaticHelper.ReadString(xmlReader);
                                break;
                            case "columnIndex":
                                this.columnIndex = StaticParser.ParseShortFromString(StaticHelper.ReadString(xmlReader));
                                break;
                            case "minIndex":
                                if (this.minIndex == null)
                                    this.minIndex = new genericMeasure();
                                this.minIndex.HandleXML(xmlReader.ReadSubtree(), isNew);
                                break;
                            case "maxIndex":
                                if (this.maxIndex == null)
                                    this.maxIndex = new genericMeasure();
                                this.maxIndex.HandleXML(xmlReader.ReadSubtree(), isNew);
                                break;
                            case "minDateTimeIndex":
                                this.minDateTimeIndex = StaticParser.ParseDateTimeFromString(StaticHelper.ReadString(xmlReader));
                                this.minDateTimeIndexSpecified = true;
                                break;
                            case "maxDateTimeIndex":
                                this.maxDateTimeIndex = StaticParser.ParseDateTimeFromString(StaticHelper.ReadString(xmlReader));
                                this.maxDateTimeIndexSpecified = true;
                                break;
                            default:
                                break;
                        }
                    }
                }
            }
        }

    }
}
