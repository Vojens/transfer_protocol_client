﻿ 

using System;
using System.Collections.Generic;
using System.Text;
using System.Xml;

namespace Witsml.Objects1311
{
    public partial class cs_calibration : IHandleXML, IHasID
    {
        //interface for generic helper
        [System.Xml.Serialization.XmlIgnore]
        public string UID
        {
            get { return this.uid; }
        }
        public cs_calibration()
        {
        }
        /// <summary>
        /// Convert XML string data into cs_calibration fields value
        /// </summary>
        /// <param name="xmlReader">reader that has cs_calibration node</param>
        /// <param name="IsNew">Is the object from stratch(true) or from db/serialization(false)</param>
        public void HandleXML(XmlReader xmlReader, bool isNew)
        {
            //condition for add/update
            bool isParameterNew = this.parameter == null;

            //create temporary list for data manipulation
            List<cs_calibrationParameter> listParameter = isParameterNew ? new List<cs_calibrationParameter>() : new List<cs_calibrationParameter>(this.parameter);

            using (xmlReader)
            {
                xmlReader.MoveToContent();
                if (xmlReader.HasAttributes)
                {
                    if (xmlReader.MoveToAttribute("uid"))
                        StaticHelper.HandleUID(xmlReader.Value, "calibration uid", out this.uid);
                    xmlReader.MoveToElement();
                }
                while (xmlReader.Read())
                {
                    //start on depth 1 which are the childs that we want to check against
                    //check for starting tag and convert node value to its expected variable
                    if (xmlReader.Depth == 1 && xmlReader.IsStartElement() && !xmlReader.IsEmptyElement)
                    {
                        switch (xmlReader.Name)
                        {
                            case "dTimCalibration":
                                this.dTimCalibration = StaticParser.ParseDateTimeFromString(StaticHelper.ReadString(xmlReader));
                                this.dTimCalibrationSpecified = true;
                                break;
                            case "calibratedBy":
                                this.calibratedBy = StaticHelper.ReadString(xmlReader);
                                break;
                            case "calibrationProtocol":
                                this.calibrationProtocol = StaticHelper.ReadString(xmlReader);
                                break;
                            case "parameter":
                                //collection
                                StaticHelper.AddUpdateWithUid<cs_calibrationParameter>(xmlReader, isParameterNew, ref listParameter);
                                break;
                            case "remarks":
                                this.remarks = StaticHelper.ReadString(xmlReader);
                                break;
                            default:
                                break;
                        }
                    }
                }
                //assign temporary list to collection
                this.parameter = listParameter.ToArray();
            }
        }
    }
}
