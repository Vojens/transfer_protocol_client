﻿ 

using System;
using System.Collections.Generic;
using System.Text;
using System.Xml;

namespace Witsml.Objects1311
{
    public partial class cs_fiber : IHandleXML, IHasID
    {
        //interface for generic helper
        [System.Xml.Serialization.XmlIgnore]
        public string UID
        {
            get { return this.uid; }
        }
        public cs_fiber()
        {
            this.mode = FiberMode.unknown;
        }
        /// <summary>
        /// Convert XML string data into cs_fiber fields value
        /// </summary>
        /// <param name="xmlReader">reader that has cs_fiber node</param>
        /// <param name="IsNew">Is the object from stratch(true) or from db/serialization(false)</param>
        public void HandleXML(XmlReader xmlReader, bool isNew)
        {
            //condition for add/update
            bool isTypeNew = this.type == null;
            bool isRefractiveIndexNew = this.refractiveIndex == null;
            bool isOneWayLossNew = this.oneWayLoss == null;
            bool isParameterNew = this.parameter == null;
            bool isFactoryCalibrationNew = this.factoryCalibration == null;
            bool isInstallationHistoryNew = this.installationHistory == null;

            //create temporary list for data manipulation
            List<typeOptionalClassString> listType = isTypeNew ? new List<typeOptionalClassString>() : new List<typeOptionalClassString>(this.type);
            List<cs_refractiveIndex> listRefractiveIndex = isRefractiveIndexNew ? new List<cs_refractiveIndex>() : new List<cs_refractiveIndex>(this.refractiveIndex);
            List<cs_oneWayLoss> listOneWayLoss = isOneWayLossNew ? new List<cs_oneWayLoss>() : new List<cs_oneWayLoss>(this.oneWayLoss);
            List<indexedObject> listParameter = isParameterNew ? new List<indexedObject>() : new List<indexedObject>(this.parameter);
            List<cs_calibration> listFactoryCalibration = isFactoryCalibrationNew ? new List<cs_calibration>() : new List<cs_calibration>(this.factoryCalibration);
            List<refNameString> listInstallationHistory = isInstallationHistoryNew ? new List<refNameString>() : new List<refNameString>(this.installationHistory);


            using (xmlReader)
            {
                xmlReader.MoveToContent();
                if (xmlReader.HasAttributes)
                {
                    if (xmlReader.MoveToAttribute("uid"))
                        StaticHelper.HandleUID(xmlReader.Value, "fiber uid", out this.uid);
                    xmlReader.MoveToElement();
                }
                while (xmlReader.Read())
                {
                    //start on depth 1 which are the childs that we want to check against
                    //check for starting tag and convert node value to its expected variable
                    if (xmlReader.Depth == 1 && xmlReader.IsStartElement() && !xmlReader.IsEmptyElement)
                    {
                        switch (xmlReader.Name)
                        {
                            case "name":
                                this.name = StaticHelper.ReadString(xmlReader);
                                break;
                            case "mode":
                                this.mode = StaticParser.ParseEnumFromString<FiberMode>(StaticHelper.ReadString(xmlReader),FiberMode.unknown);
                                break;
                            case "type":
                                //collection
                                StaticHelper.AddUpdateWithUid<typeOptionalClassString>(xmlReader, isTypeNew, ref listType);
                                break;
                            case "coating":
                                this.coating = StaticHelper.ReadString(xmlReader);
                                break;
                            case "jacket":
                                this.jacket = StaticHelper.ReadString(xmlReader);
                                break;
                            case "coreDiameter":
                                if (this.coreDiameter == null)
                                    this.coreDiameter = new lengthMeasure();
                                this.coreDiameter.HandleXML(xmlReader.ReadSubtree(), isNew);
                                break;
                            case "claddedDiameter":
                                if (this.claddedDiameter == null)
                                    this.claddedDiameter = new lengthMeasure();
                                this.claddedDiameter.HandleXML(xmlReader.ReadSubtree(), isNew);
                                break;
                            case "outsideDiameter":
                                if (this.outsideDiameter == null)
                                    this.outsideDiameter = new lengthMeasure();
                                this.outsideDiameter.HandleXML(xmlReader.ReadSubtree(), isNew);
                                break;
                            case "refractiveIndex":
                                //collection
                                StaticHelper.AddUpdateWithUid<cs_refractiveIndex>(xmlReader, isRefractiveIndexNew, ref listRefractiveIndex);
                                break;
                            case "oneWayLoss":
                                //collection
                                StaticHelper.AddUpdateWithUid<cs_oneWayLoss>(xmlReader, isOneWayLossNew, ref listOneWayLoss);
                                break;
                            case "parameter":
                                //collection
                                StaticHelper.AddUpdateIndexedObj(xmlReader, isParameterNew, ref listParameter);
                                break;
                            case "spoolNumberTag":
                                this.spoolNumberTag = StaticHelper.ReadString(xmlReader);
                                break;
                            case "spoolLength":
                                if (this.spoolLength == null)
                                    this.spoolLength = new lengthMeasure();
                                this.spoolLength.HandleXML(xmlReader.ReadSubtree(), isNew);
                                break;
                            case "supplyDate":
                                this.supplyDate = StaticHelper.ReadString(xmlReader);
                                break;
                            case "supplier":
                                this.supplier = StaticHelper.ReadString(xmlReader);
                                break;
                            case "supplierModelNumber":
                                this.supplierModelNumber = StaticHelper.ReadString(xmlReader);
                                break;
                            case "factoryCalibration":
                                //collection
                                StaticHelper.AddUpdateWithUid<cs_calibration>(xmlReader, isFactoryCalibrationNew, ref listFactoryCalibration);
                                break;
                            case "installationHistory":
                                //collection
                                refNameString installationHistory = new refNameString();
                                installationHistory.HandleXML(xmlReader.ReadSubtree(), isNew);
                                listInstallationHistory.Add(installationHistory);
                                break;
                            default:
                                break;
                        }
                    }
                }

                //assign temporary list to collection
                this.type = listType.ToArray();
                this.refractiveIndex = listRefractiveIndex.ToArray();
                this.oneWayLoss = listOneWayLoss.ToArray();
                this.parameter = listParameter.ToArray();
                this.factoryCalibration = listFactoryCalibration.ToArray();
                this.installationHistory = listInstallationHistory.ToArray();
            }
        }
    }
}
