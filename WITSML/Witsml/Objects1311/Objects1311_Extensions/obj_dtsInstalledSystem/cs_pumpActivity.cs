﻿ 

using System;
using System.Collections.Generic;
using System.Text;
using System.Xml;

namespace Witsml.Objects1311
{
    public partial class cs_pumpActivity : IHandleXML, IHasID
    {
        //interface for generic helper
        [System.Xml.Serialization.XmlIgnore]
        public string UID
        {
            get { return this.uid; }
        }
        public cs_pumpActivity()
        {
        }

        /// <summary>
        /// Convert XML string data into cs_pumpActivity fields value
        /// </summary>
        /// <param name="xmlReader">reader that has cs_pumpActivity node</param>
        /// <param name="IsNew">Is the object from stratch(true) or from db/serialization(false)</param>
        public void HandleXML(XmlReader xmlReader, bool isNew)
        {
            //condition for add/update
            bool isPumpLogNew = this.pumpLog == null;
            bool isItemNew = this.Items == null;

            //create temporary list for data manipulation
            List<cs_pumpLogRow> listPumpLog = isPumpLogNew ? new List<cs_pumpLogRow>() : new List<cs_pumpLogRow>(this.pumpLog);
            List<object> listItem = isItemNew ? new List<object>() : new List<object>(this.Items);

            using (xmlReader)
            {
                xmlReader.MoveToContent();
                if (xmlReader.HasAttributes)
                {
                    if (xmlReader.MoveToAttribute("uid"))
                        StaticHelper.HandleUID(xmlReader.Value, "pumpActivity uid", out this.uid);
                    xmlReader.MoveToElement();
                }
                while (xmlReader.Read())
                {
                    //start on depth 1 which are the childs that we want to check against
                    //check for starting tag and convert node value to its expected variable
                    if (xmlReader.Depth == 1 && xmlReader.IsStartElement() && !xmlReader.IsEmptyElement)
                    {
                        switch (xmlReader.Name)
                        {
                            case "name":
                                this.name = StaticHelper.ReadString(xmlReader);
                                break;
                            case "installedFiber":
                                if (this.installedFiber == null)
                                    this.installedFiber = new refNameString();
                                this.installedFiber.HandleXML(xmlReader.ReadSubtree(), isNew);
                                break;
                            case "pumpingDate":
                                this.pumpingDate = StaticHelper.ReadString(xmlReader);
                                break;
                            case "engineerName":
                                this.engineerName = StaticHelper.ReadString(xmlReader);
                                break;
                            case "serviceCompany":
                                this.serviceCompany = StaticHelper.ReadString(xmlReader);
                                break;
                            case "pumpFluidType":
                                this.pumpFluidType = StaticHelper.ReadString(xmlReader);
                                break;
                            case "controlLineFluid":
                                this.controlLineFluid = StaticHelper.ReadString(xmlReader);
                                break;
                            case "pumpDirection":
                                this.pumpDirection = StaticHelper.ReadString(xmlReader);
                                break;
                            case "fiberEndSeal":
                                this.fiberEndSeal = StaticHelper.ReadString(xmlReader);
                                break;
                            case "cableMeterType":
                                this.cableMeterType = StaticHelper.ReadString(xmlReader);
                                break;
                            case "cableMeterSerialNumber":
                                this.cableMeterSerialNumber = StaticHelper.ReadString(xmlReader);
                                break;
                            case "cableMeterCalibrationDate":
                                this.cableMeterCalibrationDate = StaticHelper.ReadString(xmlReader);
                                break;
                            case "OTDRPerformed":
                                //collection
                                refNameString refNameObj = new refNameString();
                                refNameObj.HandleXML(xmlReader.ReadSubtree(), isNew);
                                listItem.Add(refNameObj);
                                break;
                            case "wasDriftRunPerformed":
                                //collection
                                listItem.Add(StaticParser.ParseBooleanFromString(StaticHelper.ReadString(xmlReader)));
                                break;
                            case "excessFiberRecovered":
                                if (this.excessFiberRecovered == null)
                                    this.excessFiberRecovered = new lengthMeasure();
                                this.excessFiberRecovered.HandleXML(xmlReader.ReadSubtree(), isNew);
                                break;
                            case "pumpLog":
                                //array items
                                StaticHelper.HandleArrayItemChecking<cs_pumpLogRow>(xmlReader.ReadSubtree(), isPumpLogNew, ref listPumpLog, "tableRow", "uid");
                                break;

                            default:
                                break;
                        }
                    }
                }
                //assign temporary list to collection
                this.pumpLog = listPumpLog.ToArray();
                this.Items = listItem.ToArray();
            }
        }
    }
}
