﻿ 

using System;
using System.Collections.Generic;
using System.Text;
using System.Xml;

namespace Witsml.Objects1311
{
    public partial class cs_fiberInformation : IHandleXML
    {
        public cs_fiberInformation()
        {
        }
        /// <summary>
        /// Convert XML string data into cs_fiberInformation fields value
        /// </summary>
        /// <param name="xmlReader">reader that has cs_fiberInformation node</param>
        /// <param name="IsNew">Is the object from stratch(true) or from db/serialization(false)</param>
        public void HandleXML(XmlReader xmlReader, bool isNew)
        {
            //condition for add/update
            bool isFiberNew = this.fiber == null;

            //create temporary list for data manipulation
            List<cs_fiber> listFiber = isFiberNew ? new List<cs_fiber>() : new List<cs_fiber>(this.fiber);

            using (xmlReader)
            {
                while (xmlReader.Read())
                {
                    //start on depth 1 which are the childs that we want to check against
                    //check for starting tag and convert node value to its expected variable
                    if (xmlReader.Depth == 1 && xmlReader.IsStartElement() && !xmlReader.IsEmptyElement)
                    {
                        switch (xmlReader.Name)
                        {
                            case "installationDate":
                                this.installationDate = StaticHelper.ReadString(xmlReader);
                                break;
                            case "installationCompany":
                                this.installationCompany = StaticHelper.ReadString(xmlReader);
                                break;
                            case "deInstallationDate":
                                this.deInstallationDate = StaticHelper.ReadString(xmlReader);
                                break;
                            case "capillaryTubeDiameter":
                                if (this.capillaryTubeDiameter == null)
                                    this.capillaryTubeDiameter = new lengthMeasure();
                                this.capillaryTubeDiameter.HandleXML(xmlReader.ReadSubtree(), isNew);
                                break;
                            case "fiber":
                                //collection
                                StaticHelper.AddUpdateWithUid<cs_fiber>(xmlReader, isFiberNew, ref listFiber);
                                break;
                            default:
                                break;
                        }
                    }
                }
                //assign temporary list to collection
                this.fiber = listFiber.ToArray();
            }
        }
    }
}
