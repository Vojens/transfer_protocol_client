﻿ 

using System;
using System.Collections.Generic;
using System.Text;
using System.Xml;

namespace Witsml.Objects1311
{
    public partial class cs_boxInformation : IHandleXML
    {
        public cs_boxInformation()
        {
        }
        /// <summary>
        /// Convert XML string data into cs_boxInformation fields value
        /// </summary>
        /// <param name="xmlReader">reader that has cs_boxInformation node</param>
        /// <param name="IsNew">Is the object from stratch(true) or from db/serialization(false)</param>
        public void HandleXML(XmlReader xmlReader, bool isNew)
        {
            using (xmlReader)
            {
                xmlReader.MoveToContent();
                if (xmlReader.HasAttributes)
                {
                    if (xmlReader.MoveToAttribute("uid"))
                        StaticHelper.HandleUID(xmlReader.Value, "boxInformation uid", out this.uid);
                    xmlReader.MoveToElement();
                }
                while (xmlReader.Read())
                {
                    //start on depth 1 which are the childs that we want to check against
                    //check for starting tag and convert node value to its expected variable
                    if (xmlReader.Depth == 1 && xmlReader.IsStartElement() && !xmlReader.IsEmptyElement)
                    {
                        switch (xmlReader.Name)
                        {
                            case "installationDate":
                                this.installationDate = StaticHelper.ReadString(xmlReader);
                                break;
                            case "installationCompany":
                                this.installationCompany = StaticHelper.ReadString(xmlReader);
                                break;
                            case "deInstallationDate":
                                this.deInstallationDate = StaticHelper.ReadString(xmlReader);
                                break;
                            case "instrumentBox":
                                if (this.instrumentBox == null)
                                    this.instrumentBox = new cs_instrumentBox();
                                this.instrumentBox.HandleXML(xmlReader.ReadSubtree(), isNew);
                                break;
                            default:
                                break;
                        }
                    }
                }
            }
        }
    }
}
