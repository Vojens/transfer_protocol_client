﻿ 

using System;
using System.Collections.Generic;
using System.Text;
using System.Xml;

namespace Witsml.Objects1311
{
    public partial class cs_calibrationParameter : IHandleXML, IHasID
    {
        //interface for generic helper
        [System.Xml.Serialization.XmlIgnore]
        public string UID
        {
            get { return this.uom; }
        }
        public cs_calibrationParameter()
        {
        }
        /// <summary>
        /// Convert XML string data into cs_calibrationParameter fields value
        /// </summary>
        /// <param name="xmlReader">reader that has cs_calibrationParameter node</param>
        /// <param name="IsNew">Is the object from stratch(true) or from db/serialization(false)</param>
        public void HandleXML(XmlReader xmlReader, bool isNew)
        {
            using (xmlReader)
            {
                xmlReader.MoveToContent();
                if (xmlReader.HasAttributes)
                {
                    if (xmlReader.MoveToAttribute("uom"))
                    {
                        this.uom = xmlReader.Value;
                    }
                    if (xmlReader.MoveToAttribute("name"))
                    {
                        this.name = xmlReader.Value;
                    }
                    xmlReader.MoveToElement();
                }
                 this.Value = StaticHelper.ReadString(xmlReader);
            }
        }
    }
}
