﻿ 

using System;
using System.Collections.Generic;
using System.Text;
using System.Xml;

namespace Witsml.Objects1311
{
    public partial class cs_pumpLogRow : IHandleXML, IHasID
    {
        //interface for generic helper
        [System.Xml.Serialization.XmlIgnore]
        public string UID
        {
            get { return this.uid; }
        }
        public cs_pumpLogRow()
        {
        }
        /// <summary>
        /// Convert XML string data into cs_pumpLogRow fields value
        /// </summary>
        /// <param name="xmlReader">reader that has cs_pumpLogRow node</param>
        /// <param name="IsNew">Is the object from stratch(true) or from db/serialization(false)</param>
        public void HandleXML(XmlReader xmlReader, bool isNew)
        {
            using (xmlReader)
            {
                xmlReader.MoveToContent();
                if (xmlReader.HasAttributes)
                {
                    if (xmlReader.MoveToAttribute("uid"))
                        StaticHelper.HandleUID(xmlReader.Value, "pumpLogRow uid", out this.uid);
                    xmlReader.MoveToElement();
                }
                while (xmlReader.Read())
                {
                    //start on depth 1 which are the childs that we want to check against
                    //check for starting tag and convert node value to its expected variable
                    if (xmlReader.Depth == 1 && xmlReader.IsStartElement() && !xmlReader.IsEmptyElement)
                    {
                        switch (xmlReader.Name)
                        {
                            case "cableSpeed":
                                if (this.cableSpeed == null)
                                    this.cableSpeed = new velocityMeasure();
                                this.cableSpeed.HandleXML(xmlReader.ReadSubtree(),isNew);
                                break;
                            case "fiberPumpedLength":
                                if (this.fiberPumpedLength == null)
                                    this.fiberPumpedLength = new lengthMeasure();
                                this.fiberPumpedLength.HandleXML(xmlReader.ReadSubtree(), isNew);
                                break;
                            case "pumpPressure":
                                if (this.pumpPressure == null)
                                    this.pumpPressure = new pressureMeasure();
                                this.pumpPressure.HandleXML(xmlReader.ReadSubtree(), isNew);
                                break;
                            case "pumpFluid":
                                this.pumpFluid = StaticHelper.ReadString(xmlReader);
                                break;
                            case "temperatureIn":
                                if (this.temperatureIn == null)
                                    this.temperatureIn = new thermodynamicTemperatureMeasure();
                                this.temperatureIn.HandleXML(xmlReader.ReadSubtree(), isNew);
                                break;
                            case "temperatureOut":
                                if (this.temperatureOut == null)
                                    this.temperatureOut = new thermodynamicTemperatureMeasure();
                                this.temperatureOut.HandleXML(xmlReader.ReadSubtree(), isNew);
                                break;
                            case "remarks":
                                this.remarks = StaticHelper.ReadString(xmlReader);
                                break;
                            default:
                                break;
                        }
                    }
                }
            }
        }
    }
}
