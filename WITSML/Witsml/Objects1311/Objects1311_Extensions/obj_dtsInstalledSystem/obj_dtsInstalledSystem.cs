﻿ 

using System;
using System.Collections.Generic;
using System.IO;
using System.Text;
using System.Xml;
using Witsml.Common.Exceptions;
using Witsml.Exceptions;


namespace Witsml.Objects1311
{
    [System.Xml.Serialization.XmlRootAttribute("dtsInstalledSystem", Namespace = "http://www.witsml.org/schemas/131", IsNullable = false)]
    public partial class obj_dtsInstalledSystem : WITSMLObject, IHandleXML
    {
        public obj_dtsInstalledSystem()
        {
        }

        /// <summary>
        /// Decode XML string into obj_dtsInstalledSystem
        /// </summary>
        /// <param name="xmlIn">XML string</param>
        public override void Decode(ref string xmlIn)
        {
            try
            {
                //Consume XML string and convert node value to variable based on node name
                using (StringReader stringReader = new StringReader(xmlIn))
                {
                    XmlReaderSettings setting = new XmlReaderSettings();
                    setting.ConformanceLevel = ConformanceLevel.Document;
                    setting.IgnoreComments = true;
                    setting.IgnoreWhitespace = true;
                    setting.IgnoreProcessingInstructions = true;
                    using (XmlReader xmlReader = XmlReader.Create(stringReader, setting))
                    {
                        //determine add or update operation
                        bool isNew;
                        if (this.uid == string.Empty)
                            isNew = true;
                        else isNew = false;

                        this.HandleXML(xmlReader, isNew);
                    }
                }
            }
            catch (XmlException ex)
            {
                throw new Exceptions.UnableToDecodeException(ex.Message, WITSMLReturnCode.WITSMLDocumentError, ex.InnerException);
            }
            catch (NullReferenceException ex)
            {
                throw new Exceptions.UnableToDecodeException(ex.Message, WITSMLReturnCode.WITSMLEmptyDocument, ex.InnerException);
            }
            catch (Exceptions.UnableToDecodeException ex)
            {
                throw ex;
            }
            catch (Exception ex)
            {
                throw new Exceptions.UnableToDecodeException(ex.Message, WITSMLReturnCode.WITSMLUnkownCause, ex.InnerException);
            }
        }

        /// <summary>
        /// Convert XML string data into obj_fluidsReport fields value
        /// </summary>
        /// <param name="xmlReader">reader that has obj_fluidsReport node</param>
        /// <param name="IsNew">Is the object from stratch(true) or from db/serialization(false)</param>
        public void HandleXML(XmlReader xmlReader, bool isNew)
        {
            //condition for add or update
            bool isFiberEndPointNew = this.fiberEndPoint == null;
            bool isWellBoreSchematicNew = this.wellboreFiberSchematic == null;
            bool isOtdrInstallationTestNew = this.otdrInstallationTest == null;
            bool isInstallationCalibrationNew = this.installationCalibration == null;

            //temporary list for data manipulations
            List<cs_fiberEndPoint> listFiberEndPoint = isFiberEndPointNew ? new List<cs_fiberEndPoint>() : new List<cs_fiberEndPoint>(this.fiberEndPoint);
            List<cs_singleInstalledPoint> listSinglePoint = isWellBoreSchematicNew ? new List<cs_singleInstalledPoint>() : new List<cs_singleInstalledPoint>(this.wellboreFiberSchematic);
            List<cs_otdr> listOtdr = isOtdrInstallationTestNew ? new List<cs_otdr>() : new List<cs_otdr>(this.otdrInstallationTest);
            List<cs_calibration> listCalibration = isInstallationCalibrationNew ? new List<cs_calibration>() : new List<cs_calibration>(this.installationCalibration);


            using (xmlReader)
            {
                //assign uid for the new message
                xmlReader.MoveToContent();
                if (xmlReader.HasAttributes)
                {
                    if (xmlReader.MoveToAttribute("uidWell"))
                        StaticHelper.HandleUID(xmlReader.Value, "dtsInstalledSystem uidWell", out this.uidWell);
                    if (xmlReader.MoveToAttribute("uidWellbore"))
                        StaticHelper.HandleUID(xmlReader.Value, "dtsInstalledSystem uidWellbore", out this.uidWellbore);
                    if (xmlReader.MoveToAttribute("uid"))
                        StaticHelper.HandleUID(xmlReader.Value, "dtsInstalledSystem uid", out this.uid);
                    xmlReader.MoveToElement();
                }
                while (xmlReader.Read())
                {
                    //only take child node
                    if (xmlReader.Depth == 1 && xmlReader.IsStartElement() && !xmlReader.IsEmptyElement)
                    {
                        switch (xmlReader.Name)
                        {
                            case "nameWell":
                                this.nameWell = StaticHelper.ReadString(xmlReader);
                                break;
                            case "nameWellbore":
                                this.nameWellbore = StaticHelper.ReadString(xmlReader);
                                break;
                            case "name":
                                this.name = StaticHelper.ReadString(xmlReader);
                                break;
                            case "isPlanned":
                                this.isPlanned = StaticParser.ParseBooleanFromString(StaticHelper.ReadString(xmlReader));
                                this.isPlannedSpecified = true;
                                break;
                            case "fiberInformation":
                                if (this.fiberInformation == null)
                                    this.fiberInformation = new cs_fiberInformation();
                                this.fiberInformation.HandleXML(xmlReader.ReadSubtree(), isNew);
                                break;
                            case "instrumentBoxInformation":
                                if (this.instrumentBoxInformation == null)
                                    this.instrumentBoxInformation = new cs_boxInformation();
                                this.instrumentBoxInformation.HandleXML(xmlReader.ReadSubtree(), isNew);
                                break;
                            case "fiberConfiguration":
                                this.fiberConfiguration = StaticHelper.ReadString(xmlReader);
                                break;
                            case "fiberEndPoint":
                                //collection
                                StaticHelper.AddUpdateWithUid<cs_fiberEndPoint>(xmlReader, isFiberEndPointNew, ref listFiberEndPoint);
                                break;
                            case "fiberLength":
                                if (this.fiberLength == null)
                                    this.fiberLength = new lengthMeasure();
                                this.fiberLength.HandleXML(xmlReader.ReadSubtree(), isNew);
                                break;
                            case "wellboreFiberSchematic":
                                //collection of array item
                                StaticHelper.HandleArrayItemChecking<cs_singleInstalledPoint>(xmlReader.ReadSubtree(), isWellBoreSchematicNew, ref listSinglePoint, "installedPoint","uid");
                                break;
                            case "opticalBudget":
                                if (this.opticalBudget == null)
                                    this.opticalBudget = new generalMeasureType();
                                this.opticalBudget.HandleXML(xmlReader.ReadSubtree(), isNew);
                                break;
                            case "pumpActivity":
                                if (this.pumpActivity == null)
                                    this.pumpActivity = new cs_pumpActivity();
                                this.pumpActivity.HandleXML(xmlReader.ReadSubtree(), isNew);
                                break;
                            case "otdrInstallationTest":
                                //collection
                                StaticHelper.AddUpdateWithUid<cs_otdr>(xmlReader, isOtdrInstallationTestNew, ref listOtdr);
                                break;
                            case "installationCalibration":
                                //collection
                                StaticHelper.AddUpdateWithUid<cs_calibration>(xmlReader, isInstallationCalibrationNew, ref listCalibration);
                                break;
                            case "basedOnInstalledFiber":
                                if (this.basedOnInstalledFiber == null)
                                    this.basedOnInstalledFiber = new refNameString();
                                this.basedOnInstalledFiber.HandleXML(xmlReader.ReadSubtree(), isNew);
                                break;
                            case "commonData":
                                if (this.commonData == null)
                                    this.commonData = new cs_commonData();
                                this.commonData.HandleXML(xmlReader.ReadSubtree(), isNew);
                                break;
                            case "customData":
                                if (this.customData == null)
                                    this.customData = new cs_customData();
                                this.customData.HandleXML(xmlReader.ReadSubtree(), isNew);
                                break;
                            default:
                                break;
                        }
                    }
                }
                //assign list to collection
                this.fiberEndPoint = listFiberEndPoint.ToArray();
                this.wellboreFiberSchematic = listSinglePoint.ToArray();
                this.otdrInstallationTest = listOtdr.ToArray();
                this.installationCalibration = listCalibration.ToArray();
            }
        }

        public override string ValidateGetPath(string newUid)
        {
            // Fix uid if the Object dosn't have one.
            if (!IsValidUid(this.uid))
            {
                this.uid = newUid;
                // TODO: Add some loging that we created a new UID for the object maybe.
            }

            StringBuilder path = new StringBuilder(this.GetParentPath());
            path.Append(@"\");
            path.Append(this.uid);
            return path.ToString();
        }

        public override string GetParentPath()
        {
            if (!IsValidUid(this.uidWell) || !IsValidUid(this.uidWellbore))
            {
                throw new UnableToDecodeException("One or more Parent UID's are invalid", WITSMLReturnCode.WITSMLInvalidParentUID);
                //TODO: Log the issue
            }

            StringBuilder path = new StringBuilder(@"\");
            path.Append(this.uidWell);
            path.Append(@"\");
            path.Append(this.uidWellbore);
            return path.ToString();
        }

        // NOTE: these are done with functions, instead of property access, so as not to mess around with the auto-generated / serialized classes.
        public override string Get_uid()
        {
            return this.uid;
        }

        public override string Get_uidWell()
        {
            return this.uidWell;
        }

        public override string Get_uidWellbore()
        {
            return this.uidWellbore;
        }

        public override string Get_name()
        {
            return this.name;
        }

        public override string Get_nameWell()
        {
            return this.nameWell;
        }

        public override string Get_nameWellbore()
        {
            return this.nameWellbore;
        }

        public override ICommonData Get_commonData()
        {
            if (this.commonData == null)
            {
                this.commonData = new cs_commonData();
            }
            return (ICommonData)this.commonData;
        }
        public override ICustomData Get_customData()
        {
            if (this.customData == null)
            {
                this.customData = new cs_customData();
            }
            return (ICustomData)this.customData;
        }
    }
}
