﻿ 

using System;
using System.Collections.Generic;
using System.Text;
using System.Xml;

namespace Witsml.Objects1311
{
    public partial class cs_instrumentBox : IHandleXML, IHasID
    {
        //interface for generic helper
        [System.Xml.Serialization.XmlIgnore]
        public string UID
        {
            get { return this.uid; }
        }
        public cs_instrumentBox()
        {
        }
        /// <summary>
        /// Convert XML string data into cs_instrumentBox fields value
        /// </summary>
        /// <param name="xmlReader">reader that has cs_instrumentBox node</param>
        /// <param name="IsNew">Is the object from stratch(true) or from db/serialization(false)</param>
        public void HandleXML(XmlReader xmlReader, bool isNew)
        {
            //condition for add/update
            bool isNameTagNew = this.nameTag == null;
            bool isFactoryCalibrationNew = this.factoryCalibration == null;
            bool isParameterNew = this.parameter == null;

            //create temporary list for data manipulation
            List<cs_nameTag> listNameTag = isNameTagNew ? new List<cs_nameTag>() : new List<cs_nameTag>(this.nameTag);
            List<cs_instrumentBoxCalibration> listFactoryCalibration = isFactoryCalibrationNew ? new List<cs_instrumentBoxCalibration>() : new List<cs_instrumentBoxCalibration>(this.factoryCalibration);
            List<indexedObject> listParameter = isParameterNew ? new List<indexedObject>() : new List<indexedObject>(this.parameter);

            using (xmlReader)
            {
                xmlReader.MoveToContent();
                if (xmlReader.HasAttributes)
                {
                    if (xmlReader.MoveToAttribute("uid"))
                        StaticHelper.HandleUID(xmlReader.Value, "instrumenBox uid", out this.uid);
                    xmlReader.MoveToElement();
                }
                while (xmlReader.Read())
                {
                    //start on depth 1 which are the childs that we want to check against
                    //check for starting tag and convert node value to its expected variable
                    if (xmlReader.Depth == 1 && xmlReader.IsStartElement() && !xmlReader.IsEmptyElement)
                    {
                        switch (xmlReader.Name)
                        {
                            case "name":
                                this.name = StaticHelper.ReadString(xmlReader);
                                break;
                            case "manufacturer":
                                this.manufacturer = StaticHelper.ReadString(xmlReader);
                                break;
                            case "manufacturingDate":
                                this.manufacturingDate = StaticHelper.ReadString(xmlReader);
                                break;
                            case "type":
                                this.type = StaticHelper.ReadString(xmlReader);
                                break;
                            case "nameTag":
                                //collection
                                StaticHelper.AddUpdateWithUid<cs_nameTag>(xmlReader, isNameTagNew, ref listNameTag);
                                break;
                            case "softwareVersion":
                                this.softwareVersion = StaticHelper.ReadString(xmlReader);
                                break;
                            case "factoryCalibration":
                                //collection
                                StaticHelper.AddUpdateWithUid<cs_instrumentBoxCalibration>(xmlReader, isFactoryCalibrationNew, ref listFactoryCalibration);
                                break;
                            case "internalOvenLocationNear":
                                if (this.internalOvenLocationNear == null)
                                    this.internalOvenLocationNear = new lengthMeasure();
                                this.internalOvenLocationNear.HandleXML(xmlReader.ReadSubtree(), isNew);
                                break;
                            case "internalOvenLocationFar":
                                if (this.internalOvenLocationFar == null)
                                    this.internalOvenLocationFar = new lengthMeasure();
                                this.internalOvenLocationFar.HandleXML(xmlReader.ReadSubtree(), isNew);
                                break;
                            case "ovenSetPoint":
                                if (this.ovenSetPoint == null)
                                    this.ovenSetPoint = new thermodynamicTemperatureMeasure();
                                this.ovenSetPoint.HandleXML(xmlReader.ReadSubtree(), isNew);
                                break;
                            case "testCable":
                                this.testCable = StaticHelper.ReadString(xmlReader);
                                break;
                            case "offset":
                                this.offset = StaticHelper.ReadString(xmlReader);
                                break;
                            case "differentialLossSetting":
                                if (this.differentialLossSetting == null)
                                    this.differentialLossSetting = new generalMeasureType();
                                this.differentialLossSetting.HandleXML(xmlReader.ReadSubtree(), isNew);
                                break;
                            case "parameter":
                                //collection
                                StaticHelper.AddUpdateIndexedObj(xmlReader, isParameterNew, ref listParameter);
                                break;
                            default:
                                break;
                        }
                    }
                }
                //assign temporary list to collection
                this.nameTag = listNameTag.ToArray();
                this.factoryCalibration = listFactoryCalibration.ToArray();
                this.parameter = listParameter.ToArray();
            }
        }
    }
}
