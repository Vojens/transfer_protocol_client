﻿ 

using System;
using System.Collections.Generic;
using System.Text;
using System.Xml;

namespace Witsml.Objects1311
{
    public partial class cs_refractiveIndex : IHandleXML, IHasID
    {
        //interface for generic helper
        [System.Xml.Serialization.XmlIgnore]
        public string UID
        {
            get { return this.uid; }
        }
        public cs_refractiveIndex()
        {
        }
        /// <summary>
        /// Convert XML string data into cs_refractiveIndex fields value
        /// </summary>
        /// <param name="xmlReader">reader that has cs_refractiveIndex node</param>
        /// <param name="IsNew">Is the object from stratch(true) or from db/serialization(false)</param>
        public void HandleXML(XmlReader xmlReader, bool isNew)
        {
            using (xmlReader)
            {
                xmlReader.MoveToContent();
                if (xmlReader.HasAttributes)
                {
                    if (xmlReader.MoveToAttribute("uid"))
                        StaticHelper.HandleUID(xmlReader.Value, "refractiveIndex uid", out this.uid);
                    xmlReader.MoveToElement();
                }
                while (xmlReader.Read())
                {
                    //start on depth 1 which are the childs that we want to check against
                    //check for starting tag and convert node value to its expected variable
                    if (xmlReader.Depth == 1 && xmlReader.IsStartElement() && !xmlReader.IsEmptyElement)
                    {
                        switch (xmlReader.Name)
                        {
                            case "value":
                                this.value = StaticParser.ParseDoubleFromString(StaticHelper.ReadString(xmlReader));
                                break;
                            case "frequency":
                                if (this.frequency == null)
                                    this.frequency = new frequencyMeasure();
                                this.frequency.HandleXML(xmlReader.ReadSubtree(), isNew);
                                break;
                            case "wavelength":
                                if (this.wavelength == null)
                                    this.wavelength = new lengthMeasure();
                                this.wavelength.HandleXML(xmlReader.ReadSubtree(), isNew);
                                break;
                            default:
                                break;
                        }
                    }
                }
            }
        }
    }
}
