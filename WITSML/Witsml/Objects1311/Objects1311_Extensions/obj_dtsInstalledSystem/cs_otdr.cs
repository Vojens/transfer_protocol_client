﻿ 

using System;
using System.Collections.Generic;
using System.Text;
using System.Xml;

namespace Witsml.Objects1311
{
    public partial class cs_otdr : IHandleXML, IHasID
    {
        //interface for generic helper
        [System.Xml.Serialization.XmlIgnore]
        public string UID
        {
            get { return this.uid; }
        }
        public cs_otdr()
        {
            this.reasonForRun = OTDRReason.unknown;
        }
        /// <summary>
        /// Convert XML string data into cs_otdr fields value
        /// </summary>
        /// <param name="xmlReader">reader that has cs_otdr node</param>
        /// <param name="IsNew">Is the object from stratch(true) or from db/serialization(false)</param>
        public void HandleXML(XmlReader xmlReader, bool isNew)
        {
            using (xmlReader)
            {
                xmlReader.MoveToContent();
                if (xmlReader.HasAttributes)
                {
                    if (xmlReader.MoveToAttribute("uid"))
                        StaticHelper.HandleUID(xmlReader.Value, "otdr uid", out this.uid);
                    xmlReader.MoveToElement();
                }
                while (xmlReader.Read())
                {
                    //start on depth 1 which are the childs that we want to check against
                    //check for starting tag and convert node value to its expected variable
                    if (xmlReader.Depth == 1 && xmlReader.IsStartElement() && !xmlReader.IsEmptyElement)
                    {
                        switch (xmlReader.Name)
                        {
                            case "reasonForRun":
                                this.reasonForRun = StaticParser.ParseEnumFromString<OTDRReason>(StaticHelper.ReadString(xmlReader),OTDRReason.unknown);
                                break;
                            case "dTimRun":
                                this.dTimRun = StaticParser.ParseDateTimeFromString(StaticHelper.ReadString(xmlReader));
                                this.dTimRunSpecified = true;
                                break;
                            case "dataInWellLog":
                                if (this.dataInWellLog == null)
                                    this.dataInWellLog = new refNameString();
                                this.dataInWellLog.HandleXML(xmlReader.ReadSubtree(), isNew);
                                break;
                            case "fiberEndConncected":
                                if (this.fiberEndConncected == null)
                                    this.fiberEndConncected = new refNameString();
                                this.fiberEndConncected.HandleXML(xmlReader.ReadSubtree(), isNew);
                                break;
                            case "comment":
                                this.comment = StaticHelper.ReadString(xmlReader);
                                break;
                            default:
                                break;
                        }
                    }
                }
            }
        }
    }
}
