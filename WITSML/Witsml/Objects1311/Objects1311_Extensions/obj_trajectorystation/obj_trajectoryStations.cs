﻿ 

using System;
using System.Collections.Generic;
using System.Text;
using System.IO;
using System.Xml;
using System.Linq;
using Witsml.Common.Exceptions;


namespace Witsml.Objects1311
{
    //Extension class for trajectory that inherit from WITSMLDocument and Implement Decode Function
    public partial class obj_trajectoryStations : WITSMLDocument
    {
        public override WITSMLObject GetSingleObject(int Item)
        {
            return this.trajectoryStation[Item];
        }

        /// <summary>
        /// Deserialize XML string into WitsmlDocument object
        /// </summary>
        /// <param name="XMLin">XML string to be serialized</param>
        /// <returns>Number of singular mudLog inside</returns>
        public override int Decode(ref string xmlIn)
        {
            //return value
            //number of mugLog inside xmlIn
            int countOfTrajectoryStation = 0;
            try
            {
                //check whether collection is from stratch(isNew = true) or from deserialization(isNew=false)
                bool isNew = this.trajectoryStation == null;

                //create temporary list to help with data manipulation
                List<obj_trajectoryStation> listTrajectoryStation = isNew ? new List<obj_trajectoryStation>() : new List<obj_trajectoryStation>(this.trajectoryStation);

                //Consume XML string and convert node value to variable based on node name
                using (StringReader stringReader = new StringReader(xmlIn))
                {
                    XmlReaderSettings setting = new XmlReaderSettings();
                    setting.ConformanceLevel = ConformanceLevel.Document;
                    setting.IgnoreComments = true;
                    setting.IgnoreWhitespace = true;
                    setting.IgnoreProcessingInstructions = true;
                    using (XmlReader xmlReader = XmlReader.Create(stringReader, setting))
                    {
                        while (xmlReader.Read())
                        {
                            //assign version
                            xmlReader.MoveToContent();
                            if (xmlReader.MoveToAttribute("version"))
                                this.version = xmlReader.Value;
                            else
                                this.version = "1.3.1.1";//no version specified,set it to 1.3.1.1

                            //pre-checking on whether node is the correct child based on depth,start tag and not empty
                            if (xmlReader.Depth == 1 && xmlReader.IsStartElement() && !xmlReader.IsEmptyElement)
                            {
                                switch (xmlReader.Name)
                                {
                                    case "documentInfo":
                                        if (this.documentInfo == null)
                                            this.documentInfo = new cs_documentInfo();
                                        this.documentInfo.HandleXML(xmlReader.ReadSubtree(), isNew);
                                        break;
                                    case "trajectoryStation":
                                        obj_trajectoryStation trajectoryStationObj;
                                        //foreign key(well id,wellbore id) and primary key(wellbore id) to identify existing mudLog
                                        string currentUIDWell = string.Empty;
                                        string currentUIDWellbore = string.Empty;
                                        string currentUID = string.Empty;
                                        string currentUIDTrajectory = string.Empty;

                                        //Update trajectory station
                                        if (!isNew)
                                        {
                                            //check for uid attribute and assign it to temporary string for searching in collection
                                            if (xmlReader.HasAttributes)
                                            {
                                                if (xmlReader.MoveToAttribute("uidWell"))
                                                {
                                                    currentUIDWell = xmlReader.Value;
                                                }
                                                if (xmlReader.MoveToAttribute("uidWellbore"))
                                                {
                                                    currentUIDWellbore = xmlReader.Value;
                                                }
                                                if (xmlReader.MoveToAttribute("uid"))
                                                {
                                                    currentUID = xmlReader.Value;
                                                }
                                                if (xmlReader.MoveToAttribute("uidTrajectory"))
                                                {
                                                    currentUIDTrajectory = xmlReader.Value;
                                                }
                                                xmlReader.MoveToElement();
                                            }
                                            //look for trajectory station based on its UID and update the trajectory station with xml string
                                            if (currentUID != string.Empty)
                                            {
                                                trajectoryStationObj = listTrajectoryStation.FirstOrDefault(
                                                    delegate(obj_trajectoryStation objTrajStation)
                                                    { if (objTrajStation.uidWell == currentUIDWell && objTrajStation.uidWellbore == currentUIDWellbore && objTrajStation.uid == currentUID && objTrajStation.uidTrajectory == currentUIDTrajectory)return true; else return false; });
                                                if (trajectoryStationObj != null)
                                                {
                                                    trajectoryStationObj.HandleXML(xmlReader.ReadSubtree(), isNew);
                                                    countOfTrajectoryStation++;
                                                    continue;
                                                }
                                                else
                                                {
                                                    //Update can't process trajectory station without match
                                                    continue;
                                                }
                                            }
                                            else
                                            {
                                                //Update can't process trajectory station without uid
                                                continue;
                                            }
                                        }

                                        //Add New trajectory station 
                                        trajectoryStationObj = new obj_trajectoryStation();
                                        trajectoryStationObj.HandleXML(xmlReader.ReadSubtree(), true);
                                        listTrajectoryStation.Add(trajectoryStationObj);
                                        countOfTrajectoryStation++;
                                        break;
                                    default:
                                        break;
                                }
                            }
                        }
                        //assign list to trajectory station collection
                        this.trajectoryStation = listTrajectoryStation.ToArray();
                    }
                }
            }
            catch (XmlException ex)
            {
                throw new Exceptions.UnableToDecodeException(ex.Message, WITSMLReturnCode.WITSMLDocumentError, ex.InnerException);
            }
            catch (NullReferenceException ex)
            {
                throw new Exceptions.UnableToDecodeException(ex.Message, WITSMLReturnCode.WITSMLEmptyDocument, ex.InnerException);
            }
            catch (Exceptions.UnableToDecodeException ex)
            {
                throw ex;
            }
            catch (Exception ex)
            {
                throw new Exceptions.UnableToDecodeException(ex.Message, WITSMLReturnCode.WITSMLUnkownCause, ex.InnerException);
            }
            return countOfTrajectoryStation;
        }
    }
}
