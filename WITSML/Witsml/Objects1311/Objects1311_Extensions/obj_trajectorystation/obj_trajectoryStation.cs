﻿ 

using System;
using System.Collections.Generic;
using System.IO;
using System.Text;
using System.Xml;
using System.Xml.Serialization;
using System.Linq;
using Witsml.Common.Exceptions;
using Witsml.Exceptions;


namespace Witsml.Objects1311
{
    [XmlRoot("trajectoryStation", Namespace = "http://www.witsml.org/schemas/131", IsNullable = false)]
    public partial class obj_trajectoryStation : WITSMLObject, IHandleXML
    {
        public obj_trajectoryStation()
        {
            this.typeTrajStation = TrajStationType.unknown;
            this.typeSurveyTool = TypeSurveyTool.unknown;
            this.statusTrajStation = TrajStationStatus.unknown;
        }

        //Decode string to fill this object
        public override void Decode(ref string xmlIn)
        {
            //used to determine add or update
            try
            {
                //Consume XML string and convert node value to variable based on node name
                using (StringReader stringReader = new StringReader(xmlIn))
                {
                    XmlReaderSettings setting = new XmlReaderSettings();
                    setting.ConformanceLevel = ConformanceLevel.Document;
                    setting.IgnoreComments = true;
                    setting.IgnoreWhitespace = true;
                    setting.IgnoreProcessingInstructions = true;
                    using (XmlReader xmlReader = XmlReader.Create(stringReader, setting))
                    {
                        //determine add or update operation
                        bool isNew;
                        if (this.uid == string.Empty)
                            isNew = true;
                        else isNew = false;

                        this.HandleXML(xmlReader, isNew);
                    }
                }
            }
            catch (XmlException ex)
            {
                throw new Exceptions.UnableToDecodeException(ex.Message, WITSMLReturnCode.WITSMLDocumentError, ex.InnerException);
            }
            catch (NullReferenceException ex)
            {
                throw new Exceptions.UnableToDecodeException(ex.Message, WITSMLReturnCode.WITSMLEmptyDocument, ex.InnerException);
            }
            catch (Exceptions.UnableToDecodeException ex)
            {
                throw ex;
            }
            catch (Exception ex)
            {
                throw new Exceptions.UnableToDecodeException(ex.Message, WITSMLReturnCode.WITSMLUnkownCause, ex.InnerException);
            }
        }

        /// <summary>
        /// Convert XML string data into Trajectory Station fields value
        /// </summary>
        /// <param name="xmlReader">reader that has log node</param>
        /// <param name="IsNew">Is the object from stratch(true) or from db/serialization(false)</param>
        public void HandleXML(XmlReader xmlReader, bool isNew)
        {
            //to be used to determine add or update of recurring object
            bool isLocationNew = this.location == null;
            //create temporary list to handle data manipulation
            List<cs_location> listLocation = this.location == null ? new List<cs_location>() : new List<cs_location>(this.location);
            using (xmlReader)
            {
                xmlReader.MoveToContent();
                if (xmlReader.HasAttributes)
                {
                    if (xmlReader.MoveToAttribute("uidWell"))
                        StaticHelper.HandleUID(xmlReader.Value, "trajectoryStation uidWell", out this.uidWell);
                    if (xmlReader.MoveToAttribute("uidWellbore"))
                        StaticHelper.HandleUID(xmlReader.Value, "trajectoryStation uidWellbore", out this.uidWellbore);
                    if (xmlReader.MoveToAttribute("uid"))
                        StaticHelper.HandleUID(xmlReader.Value, "trajectoryStation uid", out this.uid);
                    if (xmlReader.MoveToAttribute("uidTrajectory"))
                        StaticHelper.HandleUID(xmlReader.Value, "trajectoryStation uidTrajectory", out this.uidTrajectory);
                    xmlReader.MoveToElement();
                }
                while (xmlReader.Read())
                {
                    if (xmlReader.Depth == 1 && xmlReader.IsStartElement() && !xmlReader.IsEmptyElement)
                    {
                        switch (xmlReader.Name)
                        {
                            case "target":
                                if (this.target == null)
                                    this.target = new refNameString();
                                this.target.ObjectName = "target";
                                this.target.HandleXML(xmlReader.ReadSubtree(), isNew);
                                break;
                            case "dTimStn":
                                this.dTimStn = StaticParser.ParseDateTimeFromString(StaticHelper.ReadString(xmlReader));
                                this.dTimStnSpecified = true;
                                break;
                            case "typeTrajStation":
                                this.typeTrajStation = StaticParser.ParseEnumFromString<TrajStationType>(StaticHelper.ReadString(xmlReader), TrajStationType.unknown);
                                break;
                            case "typeSurveyTool":
                                StaticParser.SetEnumFromString<TypeSurveyTool>(StaticHelper.ReadString(xmlReader), out this.typeSurveyTool, out this.typeSurveyToolSpecified);
                                break;
                            case "md":
                                if (this.md == null)
                                    this.md = new measuredDepthCoord();
                                this.md.HandleXML(xmlReader.ReadSubtree(), isNew);
                                break;
                            case "tvd":
                                if (this.tvd == null)
                                    this.tvd = new wellVerticalDepthCoord();
                                this.tvd.HandleXML(xmlReader.ReadSubtree(), isNew);
                                break;
                            case "incl":
                                if (this.incl == null)
                                    this.incl = new planeAngleMeasure();
                                this.incl.HandleXML(xmlReader.ReadSubtree(), isNew);
                                break;
                            case "azi":
                                if (this.azi == null)
                                    this.azi = new planeAngleMeasure();
                                this.azi.HandleXML(xmlReader.ReadSubtree(), isNew);
                                break;
                            case "mtf":
                                if (this.mtf == null)
                                    this.mtf = new planeAngleMeasure();
                                this.mtf.HandleXML(xmlReader.ReadSubtree(), isNew);
                                break;
                            case "gtf":
                                if (this.gtf == null)
                                    this.gtf = new planeAngleMeasure();
                                this.gtf.HandleXML(xmlReader.ReadSubtree(), isNew);
                                break;
                            case "dispNs":
                                if (this.dispNs == null)
                                    this.dispNs = new lengthMeasure();
                                this.dispNs.HandleXML(xmlReader.ReadSubtree(), isNew);
                                break;
                            case "dispEw":
                                if (this.dispEw == null)
                                    this.dispEw = new lengthMeasure();
                                this.dispEw.HandleXML(xmlReader.ReadSubtree(), isNew);
                                break;
                            case "vertSect":
                                if (this.vertSect == null)
                                    this.vertSect = new lengthMeasure();
                                this.vertSect.HandleXML(xmlReader.ReadSubtree(), isNew);
                                break;
                            case "dls":
                                if (this.dls == null)
                                    this.dls = new anglePerLengthMeasure();
                                this.dls.HandleXML(xmlReader.ReadSubtree(), isNew);
                                break;
                            case "rateTurn":
                                if (this.rateTurn == null)
                                    this.rateTurn = new anglePerLengthMeasure();
                                this.rateTurn.HandleXML(xmlReader.ReadSubtree(), isNew);
                                break;
                            case "rateBuild":
                                if (this.rateBuild == null)
                                    this.rateBuild = new anglePerLengthMeasure();
                                this.rateBuild.HandleXML(xmlReader.ReadSubtree(), isNew);
                                break;
                            case "mdDelta":
                                if (this.mdDelta == null)
                                    this.mdDelta = new measuredDepthCoord();
                                this.mdDelta.HandleXML(xmlReader.ReadSubtree(), isNew);
                                break;
                            case "tvdDelta":
                                if (this.tvdDelta == null)
                                    this.tvdDelta = new wellVerticalDepthCoord();
                                this.tvdDelta.HandleXML(xmlReader.ReadSubtree(), isNew);
                                break;
                            case "modelToolError":
                                this.modelToolError = StaticHelper.ReadString(xmlReader);
                                break;
                            case "gravTotalUncert":
                                if (this.gravTotalUncert == null)
                                    this.gravTotalUncert = new accelerationLinearMeasure();
                                this.gravTotalUncert.HandleXML(xmlReader.ReadSubtree(), isNew);
                                break;
                            case "dipAngleUncert":
                                if (this.dipAngleUncert == null)
                                    this.dipAngleUncert = new planeAngleMeasure();
                                this.dipAngleUncert.HandleXML(xmlReader.ReadSubtree(), isNew);
                                break;
                            case "magTotalUncert":
                                if (this.magTotalUncert == null)
                                    this.magTotalUncert = new magneticInductionMeasure();
                                this.magTotalUncert.HandleXML(xmlReader.ReadSubtree(), isNew);
                                break;
                            case "gravAccelCorUsed":
                                this.gravAccelCorUsed = StaticParser.ParseBooleanFromString(StaticHelper.ReadString(xmlReader));
                                this.gravAccelCorUsedSpecified = true;
                                break;
                            case "magXAxialCorUsed":
                                this.magXAxialCorUsed = StaticParser.ParseBooleanFromString(StaticHelper.ReadString(xmlReader));
                                this.magXAxialCorUsedSpecified = true;
                                break;
                            case "sagCorUsed":
                                this.sagCorUsed = StaticParser.ParseBooleanFromString(StaticHelper.ReadString(xmlReader));
                                this.sagCorUsedSpecified = true;
                                break;
                            case "magDrlstrCorUsed":
                                this.magDrlstrCorUsed = StaticParser.ParseBooleanFromString(StaticHelper.ReadString(xmlReader));
                                this.magDrlstrCorUsedSpecified = true;
                                break;
                            case "gravTotalFieldReference":
                                if (this.gravTotalFieldReference == null)
                                    this.gravTotalFieldReference = new accelerationLinearMeasure();
                                this.gravTotalFieldReference.HandleXML(xmlReader.ReadSubtree(), isNew);
                                break;
                            case "magTotalFieldReference":
                                if (this.magTotalFieldReference == null)
                                    this.magTotalFieldReference = new magneticInductionMeasure();
                                this.magTotalFieldReference.HandleXML(xmlReader.ReadSubtree(), isNew);
                                break;
                            case "magDipAngleReference":
                                if (this.magDipAngleReference == null)
                                    this.magDipAngleReference = new planeAngleMeasure();
                                this.magDipAngleReference.HandleXML(xmlReader.ReadSubtree(), isNew);
                                break;
                            case "magModelUsed":
                                this.magModelUsed = StaticHelper.ReadString(xmlReader);
                                break;
                            case "magModelValid":
                                this.magModelValid = StaticHelper.ReadString(xmlReader);
                                break;
                            case "geoModelUsed":
                                this.geoModelUsed = StaticHelper.ReadString(xmlReader);
                                break;
                            case "statusTrajStation":
                                StaticParser.SetEnumFromString<TrajStationStatus>(StaticHelper.ReadString(xmlReader), out this.statusTrajStation, out this.statusTrajStationSpecified);
                                break;
                            case "rawData":
                                if (this.rawData == null)
                                    this.rawData = new cs_stnTrajRawData();
                                this.rawData.HandleXML(xmlReader.ReadSubtree(), isNew);
                                break;
                            case "corUsed":
                                if (this.corUsed == null)
                                    this.corUsed = new cs_stnTrajCorUsed();
                                this.corUsed.HandleXML(xmlReader.ReadSubtree(), isNew);
                                break;
                            case "valid":
                                if (this.valid == null)
                                    this.valid = new cs_stnTrajValid();
                                this.valid.HandleXML(xmlReader.ReadSubtree(), isNew);
                                break;
                            case "matrixCov":
                                if (this.matrixCov == null)
                                    this.matrixCov = new cs_stnTrajMatrixCov();
                                this.matrixCov.HandleXML(xmlReader.ReadSubtree(), isNew);
                                break;
                            case "location":
                                //multiple recurring object
                                cs_location locationObj;
                                string currentLocationUID = string.Empty;

                                if (xmlReader.HasAttributes)
                                {
                                    //check for uid attribute and assign it to temporary string for searching in collection
                                    if (xmlReader.MoveToAttribute("uid"))
                                    {
                                        currentLocationUID = xmlReader.Value;
                                        xmlReader.MoveToElement();
                                    }
                                }

                                //TODO :decide what happen with empty uid
                                if (String.IsNullOrEmpty(currentLocationUID))
                                {
                                    continue;
                                }

                                //Check Operation to be perform
                                if (!isLocationNew)
                                {
                                    //Update Operation
                                    if (xmlReader.HasAttributes)
                                    {
                                        //check for uid attribute and assign it to temporary string for searching in collection
                                        //look for qualifier based on its UID and update the well with xml string
                                        locationObj = listLocation.FirstOrDefault(
                                            delegate(cs_location objLocation)
                                            { if (objLocation.uid == currentLocationUID)return true; else return false; });
                                        if (locationObj != null)
                                        {
                                            locationObj.HandleXML(xmlReader.ReadSubtree(), isLocationNew);
                                            continue;
                                        }
                                    }
                                }

                                //Add Operation
                                locationObj = new cs_location();
                                locationObj.HandleXML(xmlReader.ReadSubtree(), isLocationNew);
                                listLocation.Add(locationObj);
                                break;
                            case "sourceStation":
                                if (this.sourceStation == null)
                                    this.sourceStation = new cs_refWellboreTrajectoryStation();
                                this.sourceStation.HandleXML(xmlReader.ReadSubtree(), isNew);
                                break;
                            case "commonData":
                                if (this.commonData == null)
                                    this.commonData = new cs_commonData();
                                this.commonData.HandleXML(xmlReader.ReadSubtree(), isNew);
                                break;
                            default:
                                break;
                        }
                    }
                }
                //assign back list member to array
                this.location = listLocation.ToArray();
            }
        }

        public override string Serialize()
        {
            //we want to ignore trajectoryStation on serialize,so we create xmlAttribute that will be ignored
            XmlAttributes xmlAtts = new XmlAttributes();
            xmlAtts.XmlIgnore = true;

            //specify override for trajectoryStation which accept ignore attribute
            XmlAttributeOverrides xmlAttsOverrides = new XmlAttributeOverrides();
            xmlAttsOverrides.Add(typeof(obj_trajectory), "trajectoryStation", xmlAtts);

            //do serialize with override parameter
            return this.Serialize(xmlAttsOverrides,SerializerType.TrajectoryWithoutStation);
        }

        /// <summary>
        /// Serialize Trajectory include with Trajectory Station.XML Declaration and prefix not included
        /// </summary>
        /// <returns>serialize result without xml declaration</returns>
        public string SerializeWithTrajectoryStation()
        {
            //do serialize with override parameter
            return this.SerializeClean(null,SerializerType.Null);
        }

        public override string ValidateGetPath(string newUid)
        {
            // Fix uid if the Object dosn't have one.
            if (!IsValidUid(this.uid))
            {
                this.uid = newUid;
                // TODO: Add some loging that we created a new UID for the object maybe.
            }

            StringBuilder path = new StringBuilder(this.GetParentPath());
            path.Append(@"\");
            path.Append(this.uid);
            return path.ToString();
        }

        public override string GetParentPath()
        {
            if (!IsValidUid(this.uidWell) || !IsValidUid(this.uidWellbore))
            {
                throw new UnableToDecodeException("One or more Parent UID's are invalid", WITSMLReturnCode.WITSMLInvalidParentUID);
                //TODO: Log the issue
            }

            StringBuilder path = new StringBuilder(@"\");
            path.Append(this.uidWell);
            path.Append(@"\");
            path.Append(this.uidWellbore);
            return path.ToString();
        }

        // NOTE: these are done with functions, instead of property access, so as not to mess around with the auto-generated / serialized classes.
        public override string Get_uid()
        {
            return this.uid;
        }

        public override string Get_uidWell()
        {
            return this.uidWell;
        }

        public override string Get_uidWellbore()
        {
            return this.uidWellbore;
        }

        public override string Get_name()
        {
            return "";
        }

        public override string Get_nameWell()
        {
            return this.nameWell;
        }

        public override string Get_nameWellbore()
        {
            return this.nameWellbore;
        }

        public override ICommonData Get_commonData()
        {
            if (this.commonData == null)
            {
                this.commonData = new cs_commonData();
            }
            return (ICommonData)this.commonData;
        }
        public override ICustomData Get_customData()
        {
            if (this.customData == null)
            {
                this.customData = new cs_customData();
            }
            return (ICustomData)this.customData;
        }
    }
}
