 

using System;
using System.Collections.Generic;
using System.Text;
using System.Xml;

namespace Witsml.Objects1311
{
    public partial class genericMeasure : IHandleXML
    {
        /// <summary>
        /// Convert XML string data into genericMeasure fields value
        /// </summary>
        /// <param name="xmlReader">reader that has genericMeasure node</param>
        /// <param name="IsNew">Is the object from stratch(true) or from db/serialization(false)</param>
        public void HandleXML(XmlReader xmlReader, bool isNew)
        {
            try
            {
                using (xmlReader)
                {
                    xmlReader.Read();
                    if (xmlReader.HasAttributes)
                    {
                        if (xmlReader.MoveToAttribute("uom"))
                            this.uom = xmlReader.Value;
                        xmlReader.MoveToElement();
                    }
                    this.Value = StaticParser.ParseDoubleFromString(StaticHelper.ReadString(xmlReader));
                }
            }
            catch (Exception ex)
            {
                throw ex;
            }
        }

    }
}
