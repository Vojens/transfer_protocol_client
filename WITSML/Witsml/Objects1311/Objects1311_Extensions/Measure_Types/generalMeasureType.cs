﻿ 

using System;
using System.Collections.Generic;
using System.Text;
using System.Xml;

namespace Witsml.Objects1311
{
    public partial class generalMeasureType : IHandleXML
    {
        public generalMeasureType()
        {
        }
        /// <summary>
        /// Convert XML string data into generalMeasureType fields value
        /// </summary>
        /// <param name="xmlReader">reader that has generalMeasureType node</param>
        /// <param name="IsNew">Is the object from stratch(true) or from db/serialization(false)</param>
        public void HandleXML(XmlReader xmlReader, bool isNew)
        {
            using (xmlReader)
            {
                xmlReader.Read();
                if (xmlReader.HasAttributes)
                {
                    if (xmlReader.MoveToAttribute("uom"))
                    {
                        this.uom = xmlReader.Value;
                    }
                    xmlReader.MoveToElement();
                }
                this.Value = StaticParser.ParseDoubleFromString(StaticHelper.ReadString(xmlReader));
            }
        }
    }
}
