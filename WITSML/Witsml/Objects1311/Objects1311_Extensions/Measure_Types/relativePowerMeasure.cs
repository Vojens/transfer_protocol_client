 

using System;
using System.Collections.Generic;
using System.Text;
using System.Xml;

namespace Witsml.Objects1311
{
    public partial class relativePowerMeasure : IHandleXML
    {
        public relativePowerMeasure()
        {
            this.uom = relativePowerUom.unknown;
        }
        /// <summary>
        /// Convert XML string data into relativePowerMeasure fields value
        /// </summary>
        /// <param name="xmlReader">reader that has relativePowerMeasure node</param>
        /// <param name="IsNew">Is the object from stratch(true) or from db/serialization(false)</param>
        public void HandleXML(XmlReader xmlReader, bool isNew)
        {
            using (xmlReader)
            {
                xmlReader.Read();
                if (xmlReader.HasAttributes)
                {
                    if (xmlReader.MoveToAttribute("uom"))
                    {
                        string value = xmlReader.Value;
                        this.uom = StaticParser.ParseEnumUomFromString<relativePowerUom>(value);
                    }
                    xmlReader.MoveToElement();
                }
                this.Value = StaticParser.ParseDoubleFromString(StaticHelper.ReadString(xmlReader));
            }
        }
    }
}
