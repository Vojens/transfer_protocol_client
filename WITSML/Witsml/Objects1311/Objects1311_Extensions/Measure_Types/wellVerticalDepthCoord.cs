 

using System;
using System.Xml;

namespace Witsml.Objects1311
{
    public partial class wellVerticalDepthCoord : IHandleXML
    {
        public wellVerticalDepthCoord()
        {
            this.uom = WellVerticalCoordinateUom.unknown;
        }
        /// <summary>
        /// Convert XML string data into wellVerticalDepthCoord fields value
        /// </summary>
        /// <param name="xmlReader">reader that has wellVerticalDepthCoord node</param>
        /// <param name="IsNew">Is the object from stratch(true) or from db/serialization(false)</param>
        public void HandleXML(XmlReader xmlReader, bool isNew)
        {
            using (xmlReader)
            {
                xmlReader.Read();
                if (xmlReader.HasAttributes)
                {
                    if (xmlReader.MoveToAttribute("uom"))
                    {
                        string value = xmlReader.Value;
                        this.uom = StaticParser.ParseEnumUomFromString<WellVerticalCoordinateUom>(value);
                    }
                    if (xmlReader.MoveToAttribute("datum"))
                        this.datum = xmlReader.Value;
                    xmlReader.MoveToElement();
                }
                this.Value = StaticParser.ParseDoubleFromString(StaticHelper.ReadString(xmlReader));
            }
        }
    }
}
