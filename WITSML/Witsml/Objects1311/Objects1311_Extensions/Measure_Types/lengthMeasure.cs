 

using System;
using System.Xml;

namespace Witsml.Objects1311
{
    public partial class lengthMeasure : IHandleXML
    {
        public lengthMeasure()
        {
            this.uom = lengthUom.unknown;
        }

        /// <summary>
        /// Convert XML string data into planeAngleMeasure fields value
        /// </summary>
        /// <param name="xmlReader">reader that has planeAngleMeasure node</param>
        /// <param name="IsNew">Is the object from stratch(true) or from db/serialization(false)</param>
        public void HandleXML(XmlReader xmlReader, bool isNew)
        {
            using (xmlReader)
            {
                xmlReader.Read();
                if (xmlReader.HasAttributes)
                {
                    if (xmlReader.MoveToAttribute("uom"))
                    {
                        string value = xmlReader.Value;
                        this.uom = StaticParser.ParseEnumUomFromString<lengthUom>(value);
                    }
                    xmlReader.MoveToElement();
                }
                this.Value = StaticParser.ParseDoubleFromString(StaticHelper.ReadString(xmlReader));
            }
        }

    }
}
