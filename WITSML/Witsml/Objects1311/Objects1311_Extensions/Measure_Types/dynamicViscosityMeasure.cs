 

using System;
using System.Xml;

namespace Witsml.Objects1311
{
    public partial class dynamicViscosityMeasure : IHandleXML
    {
        public dynamicViscosityMeasure()
        {
            this.uom = dynamicViscosityUom.unknown;
        }
        /// <summary>
        /// Convert XML string data into dynamicViscosityMeasure fields value
        /// </summary>
        /// <param name="xmlReader">reader that has dynamicViscosityMeasure node</param>
        /// <param name="IsNew">Is the object from stratch(true) or from db/serialization(false)</param>
        public void HandleXML(XmlReader xmlReader, bool isNew)
        {
            using (xmlReader)
            {
                xmlReader.Read();
                if (xmlReader.HasAttributes)
                {
                    if (xmlReader.MoveToAttribute("uom"))
                    {
                        //special conversion of enum string
                        string value = xmlReader.Value;
                        this.uom = StaticParser.ParseEnumUomFromString<dynamicViscosityUom>(value);
                    }

                    xmlReader.MoveToElement();
                }
                this.Value = StaticParser.ParseDoubleFromString(StaticHelper.ReadString(xmlReader));
            }
        }
    }
}
