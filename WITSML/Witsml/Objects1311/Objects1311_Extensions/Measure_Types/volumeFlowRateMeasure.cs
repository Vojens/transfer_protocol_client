 

using System;
using System.Collections.Generic;
using System.Text;
using System.Xml;

namespace Witsml.Objects1311
{
    public partial class volumeFlowRateMeasure : IHandleXML
    {
        public volumeFlowRateMeasure()
        {
            this.uom = volumeFlowRateUom.unknown;
        }
        /// <summary>
        /// Convert XML string data into volumeFlowRateMeasure fields value
        /// </summary>
        /// <param name="xmlReader">reader that has volumeFlowRateMeasure node</param>
        /// <param name="IsNew">Is the object from stratch(true) or from db/serialization(false)</param>
        public void HandleXML(XmlReader xmlReader, bool isNew)
        {
            using (xmlReader)
            {
                xmlReader.Read();
                if (xmlReader.HasAttributes)
                {
                    if (xmlReader.MoveToAttribute("uom"))
                    {
                        string value = xmlReader.Value;
                        this.uom = StaticParser.ParseEnumUomFromString<volumeFlowRateUom>(value);
                    }
                    xmlReader.MoveToElement();
                }
                this.Value = StaticParser.ParseDoubleFromString(StaticHelper.ReadString(xmlReader));
            }
        }
    }
}
