 

using System;
using System.Xml;

namespace Witsml.Objects1311
{
    public partial class massConcentrationMeasure : IHandleXML
    {
        public massConcentrationMeasure()
        {
            this.uom = massConcentrationUom.unknown;
        }
        /// <summary>
        /// Convert XML string data into massConcentrationMeasure fields value
        /// </summary>
        /// <param name="xmlReader">reader that has massConcentrationMeasure node</param>
        /// <param name="IsNew">Is the object from stratch(true) or from db/serialization(false)</param>
        public void HandleXML(XmlReader xmlReader, bool isNew)
        {
            using (xmlReader)
            {
                xmlReader.Read();
                if (xmlReader.HasAttributes)
                {
                    if (xmlReader.MoveToAttribute("uom"))
                    {
                        //special conversion of enum string
                        string value = xmlReader.Value;
                        this.uom = StaticParser.ParseEnumUomFromString<massConcentrationUom>(value);
                    }

                    xmlReader.MoveToElement();
                }
                this.Value = StaticParser.ParseDoubleFromString(StaticHelper.ReadString(xmlReader));
            }
        }
    }
}
