 

using System;
using System.Xml;

namespace Witsml.Objects1311
{
    public partial class electricPotentialMeasure : IHandleXML
    {
        public electricPotentialMeasure()
        {
            this.uom = electricPotentialUom.unknown;
        }
        /// <summary>
        /// Convert XML string data into electricPotentialMeasure fields value
        /// </summary>
        /// <param name="xmlReader">reader that has electricPotentialMeasure node</param>
        /// <param name="IsNew">Is the object from stratch(true) or from db/serialization(false)</param>
        public void HandleXML(XmlReader xmlReader, bool isNew)
        {
            using (xmlReader)
            {
                xmlReader.Read();
                if (xmlReader.HasAttributes)
                {
                    if (xmlReader.MoveToAttribute("uom"))
                    {
                        //special conversion of enum string
                        string value = xmlReader.Value;
                        this.uom = StaticParser.ParseEnumUomFromString<electricPotentialUom>(value);
                    }

                    xmlReader.MoveToElement();
                }
                this.Value = StaticParser.ParseDoubleFromString(StaticHelper.ReadString(xmlReader));
            }
        }
    }
}
