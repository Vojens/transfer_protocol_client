 

using System;
using System.Collections.Generic;
using System.Text;
using System.Xml;

namespace Witsml.Objects1311
{
    public partial class cs_chromatograph : IHandleXML
    {
        /// <summary>
        /// Convert XML string data into chromatograph fields value
        /// </summary>
        /// <param name="xmlReader">reader that has chromatograph node</param>
        /// <param name="IsNew">Is the object from stratch(true) or from db/serialization(false)</param>
        public void HandleXML(XmlReader xmlReader, bool isNew)
        {
            using (xmlReader)
            {
                while (xmlReader.Read())
                {
                    if (xmlReader.Depth == 1 && xmlReader.IsStartElement() && !xmlReader.IsEmptyElement)
                    {
                        switch (xmlReader.Name)
                        {
                            case "dTim":
                                this.dTim = StaticParser.ParseDateTimeFromString(StaticHelper.ReadString(xmlReader));
                                this.dTimSpecified = true;
                                break;
                            case "mdTop":
                                if (this.mdTop == null)
                                    this.mdTop = new measuredDepthCoord();
                                this.mdTop.HandleXML(xmlReader.ReadSubtree(), isNew);
                                break;
                            case "mdBottom":
                                if (this.mdBottom == null)
                                    this.mdBottom = new measuredDepthCoord();
                                this.mdBottom.HandleXML(xmlReader.ReadSubtree(), isNew);
                                break;
                            case "wtMudIn":
                                if (this.wtMudIn == null)
                                    this.wtMudIn = new densityMeasure();
                                this.wtMudIn.HandleXML(xmlReader.ReadSubtree(), isNew);
                                break;
                            case "wtMudOut":
                                if (this.wtMudOut == null)
                                    this.wtMudOut = new densityMeasure();
                                this.wtMudOut.HandleXML(xmlReader.ReadSubtree(), isNew);
                                break;
                            case "chromType":
                                this.chromType = StaticHelper.ReadString(xmlReader);
                                break;
                            case "eTimChromCycle":
                                if (this.eTimChromCycle == null)
                                    this.eTimChromCycle = new timeMeasure();
                                this.eTimChromCycle.HandleXML(xmlReader.ReadSubtree(), isNew);
                                break;
                            case "chromIntRpt":
                                this.chromIntRpt = StaticParser.ParseDateTimeFromString(StaticHelper.ReadString(xmlReader));
                                this.chromIntRptSpecified = true;
                                break;
                            case "methAv":
                                if (this.methAv == null)
                                    this.methAv = new volumePerVolumeMeasure();
                                this.methAv.HandleXML(xmlReader.ReadSubtree(), isNew);
                                break;
                            case "methMn":
                                if (this.methMn == null)
                                    this.methMn = new volumePerVolumeMeasure();
                                this.methMn.HandleXML(xmlReader.ReadSubtree(), isNew);
                                break;
                            case "methMx":
                                if (this.methMx == null)
                                    this.methMx = new volumePerVolumeMeasure();
                                this.methMx.HandleXML(xmlReader.ReadSubtree(), isNew);
                                break;
                            case "ethAv":
                                if (this.ethAv == null)
                                    this.ethAv = new volumePerVolumeMeasure();
                                this.ethAv.HandleXML(xmlReader.ReadSubtree(), isNew);
                                break;
                            case "ethMn":
                                if (this.ethMn == null)
                                    this.ethMn = new volumePerVolumeMeasure();
                                this.ethMn.HandleXML(xmlReader.ReadSubtree(), isNew);
                                break;
                            case "ethMx":
                                if (this.ethMx == null)
                                    this.ethMx = new volumePerVolumeMeasure();
                                this.ethMx.HandleXML(xmlReader.ReadSubtree(), isNew);
                                break;
                            case "propAv":
                                if (this.propAv == null)
                                    this.propAv = new volumePerVolumeMeasure();
                                this.propAv.HandleXML(xmlReader.ReadSubtree(), isNew);
                                break;
                            case "propMn":
                                if (this.propMn == null)
                                    this.propMn = new volumePerVolumeMeasure();
                                this.propMn.HandleXML(xmlReader.ReadSubtree(), isNew);
                                break;
                            case "propMx":
                                if (this.propMx == null)
                                    this.propMx = new volumePerVolumeMeasure();
                                this.propMx.HandleXML(xmlReader.ReadSubtree(), isNew);
                                break;
                            case "ibutAv":
                                if (this.ibutAv == null)
                                    this.ibutAv = new volumePerVolumeMeasure();
                                this.ibutAv.HandleXML(xmlReader.ReadSubtree(), isNew);
                                break;
                            case "ibutMn":
                                if (this.ibutMn == null)
                                    this.ibutMn = new volumePerVolumeMeasure();
                                this.ibutMn.HandleXML(xmlReader.ReadSubtree(), isNew);
                                break;
                            case "ibutMx":
                                if (this.ibutMx == null)
                                    this.ibutMx = new volumePerVolumeMeasure();
                                this.ibutMx.HandleXML(xmlReader.ReadSubtree(), isNew);
                                break;
                            case "nbutAv":
                                if (this.nbutAv == null)
                                    this.nbutAv = new volumePerVolumeMeasure();
                                this.nbutAv.HandleXML(xmlReader.ReadSubtree(), isNew);
                                break;
                            case "nbutMn":
                                if (this.nbutMn == null)
                                    this.nbutMn = new volumePerVolumeMeasure();
                                this.nbutMn.HandleXML(xmlReader.ReadSubtree(), isNew);
                                break;
                            case "nbutMx":
                                if (this.nbutMx == null)
                                    this.nbutMx = new volumePerVolumeMeasure();
                                this.nbutMx.HandleXML(xmlReader.ReadSubtree(), isNew);
                                break;
                            case "ipentAv":
                                if (this.ipentAv == null)
                                    this.ipentAv = new volumePerVolumeMeasure();
                                this.ipentAv.HandleXML(xmlReader.ReadSubtree(), isNew);
                                break;
                            case "ipentMn":
                                if (this.ipentMn == null)
                                    this.ipentMn = new volumePerVolumeMeasure();
                                this.ipentMn.HandleXML(xmlReader.ReadSubtree(), isNew);
                                break;
                            case "ipentMx":
                                if (this.ipentMx == null)
                                    this.ipentMx = new volumePerVolumeMeasure();
                                this.ipentMx.HandleXML(xmlReader.ReadSubtree(), isNew);
                                break;
                            case "npentAv":
                                if (this.npentAv == null)
                                    this.npentAv = new volumePerVolumeMeasure();
                                this.npentAv.HandleXML(xmlReader.ReadSubtree(), isNew);
                                break;
                            case "npentMn":
                                if (this.npentMn == null)
                                    this.npentMn = new volumePerVolumeMeasure();
                                this.npentMn.HandleXML(xmlReader.ReadSubtree(), isNew);
                                break;
                            case "npentMx":
                                if (this.npentMx == null)
                                    this.npentMx = new volumePerVolumeMeasure();
                                this.npentMx.HandleXML(xmlReader.ReadSubtree(), isNew);
                                break;
                            case "epentAv":
                                if (this.epentAv == null)
                                    this.epentAv = new volumePerVolumeMeasure();
                                this.epentAv.HandleXML(xmlReader.ReadSubtree(), isNew);
                                break;
                            case "epentMn":
                                if (this.epentMn == null)
                                    this.epentMn = new volumePerVolumeMeasure();
                                this.epentMn.HandleXML(xmlReader.ReadSubtree(), isNew);
                                break;
                            case "epentMx":
                                if (this.epentMx == null)
                                    this.epentMx = new volumePerVolumeMeasure();
                                this.epentMx.HandleXML(xmlReader.ReadSubtree(), isNew);
                                break;
                            case "ihexAv":
                                if (this.ihexAv == null)
                                    this.ihexAv = new volumePerVolumeMeasure();
                                this.ihexAv.HandleXML(xmlReader.ReadSubtree(), isNew);
                                break;
                            case "ihexMn":
                                if (this.ihexMn == null)
                                    this.ihexMn = new volumePerVolumeMeasure();
                                this.ihexMn.HandleXML(xmlReader.ReadSubtree(), isNew);
                                break;
                            case "ihexMx":
                                if (this.ihexMx == null)
                                    this.ihexMx = new volumePerVolumeMeasure();
                                this.ihexMx.HandleXML(xmlReader.ReadSubtree(), isNew);
                                break;
                            case "nhexAv":
                                if (this.nhexAv == null)
                                    this.nhexAv = new volumePerVolumeMeasure();
                                this.nhexAv.HandleXML(xmlReader.ReadSubtree(), isNew);
                                break;
                            case "nhexMn":
                                if (this.nhexMn == null)
                                    this.nhexMn = new volumePerVolumeMeasure();
                                this.nhexMn.HandleXML(xmlReader.ReadSubtree(), isNew);
                                break;
                            case "nhexMx":
                                if (this.nhexMx == null)
                                    this.nhexMx = new volumePerVolumeMeasure();
                                this.nhexMx.HandleXML(xmlReader.ReadSubtree(), isNew);
                                break;
                            case "co2Av":
                                if (this.co2Av == null)
                                    this.co2Av = new volumePerVolumeMeasure();
                                this.co2Av.HandleXML(xmlReader.ReadSubtree(), isNew);
                                break;
                            case "co2Mn":
                                if (this.co2Mn == null)
                                    this.co2Mn = new volumePerVolumeMeasure();
                                this.co2Mn.HandleXML(xmlReader.ReadSubtree(), isNew);
                                break;
                            case "co2Mx":
                                if (this.co2Mx == null)
                                    this.co2Mx = new volumePerVolumeMeasure();
                                this.co2Mx.HandleXML(xmlReader.ReadSubtree(), isNew);
                                break;
                            case "h2sAv":
                                if (this.h2sAv == null)
                                    this.h2sAv = new volumePerVolumeMeasure();
                                this.h2sAv.HandleXML(xmlReader.ReadSubtree(), isNew);
                                break;
                            case "h2sMn":
                                if (this.h2sMn == null)
                                    this.h2sMn = new volumePerVolumeMeasure();
                                this.h2sMn.HandleXML(xmlReader.ReadSubtree(), isNew);
                                break;
                            case "h2sMx":
                                if (this.h2sMx == null)
                                    this.h2sMx = new volumePerVolumeMeasure();
                                this.h2sMx.HandleXML(xmlReader.ReadSubtree(), isNew);
                                break;
                            case "acetylene":
                                if (this.acetylene == null)
                                    this.acetylene = new volumePerVolumeMeasure();
                                this.acetylene.HandleXML(xmlReader.ReadSubtree(), isNew);
                                break;
                            default:
                                break;
                        }
                    }
                }
            }
        }
    }
}
