 

using System;
using System.Collections.Generic;
using System.IO;
using System.Text;
using System.Xml;
using System.Xml.Serialization;
using System.Linq;
using Witsml;
using Witsml.Common.Exceptions;
using Witsml.Exceptions;


namespace Witsml.Objects1311
{
    //TODO : Handle XML
    [XmlRoot("mudLog", Namespace = "http://www.witsml.org/schemas/131", IsNullable = false)]
    public partial class obj_mudLog : WITSMLObject, IHandleXML
    {
        public override void Decode(ref string xmlIn)
        {
            //used to determine add or update
            try
            {
                //Consume XML string and convert node value to variable based on node name
                using (StringReader stringReader = new StringReader(xmlIn))
                {
                    XmlReaderSettings setting = new XmlReaderSettings();
                    setting.ConformanceLevel = ConformanceLevel.Document;
                    setting.IgnoreComments = true;
                    setting.IgnoreWhitespace = true;
                    setting.IgnoreProcessingInstructions = true;
                    using (XmlReader xmlReader = XmlReader.Create(stringReader, setting))
                    {
                        //determine add or update operation
                        bool isNew;
                        if (this.uid == string.Empty)
                            isNew = true;
                        else isNew = false;

                        this.HandleXML(xmlReader, isNew);
                    }
                }
            }
            catch (XmlException ex)
            {
                throw new Exceptions.UnableToDecodeException(ex.Message, WITSMLReturnCode.WITSMLDocumentError, ex.InnerException);
            }
            catch (NullReferenceException ex)
            {
                throw new Exceptions.UnableToDecodeException(ex.Message, WITSMLReturnCode.WITSMLEmptyDocument, ex.InnerException);
            }
            catch (Exceptions.UnableToDecodeException ex)
            {
                throw ex;
            }
            catch (Exception ex)
            {
                throw new Exceptions.UnableToDecodeException(ex.Message, WITSMLReturnCode.WITSMLUnkownCause, ex.InnerException);
            }
        }

        /// <summary>
        /// This will serialize without geologyInterval element(s)
        /// </summary>
        /// <returns></returns>
        public override string Serialize()
        {
            //we want to ignore trajectoryStation on serialize,so we create xmlAttribute that will be ignored
            XmlAttributes xmlAtts = new XmlAttributes();
            xmlAtts.XmlIgnore = true;

            //specify override for trajectoryStation which accept ignore attribute
            XmlAttributeOverrides xmlAttsOverrides = new XmlAttributeOverrides();
            xmlAttsOverrides.Add(typeof(obj_mudLog), "geologyInterval", xmlAtts);

            //do serialize with override parameter
            return this.Serialize(xmlAttsOverrides, SerializerType.MudlogWithoutGeology);
        }
        /// <summary>
        /// Force Serialize geologyInterval
        /// </summary>
        /// <returns></returns>
        public string SerializeWithGeologyInterval()
        {
            XmlAttributes xmlAtts = new XmlAttributes();
            xmlAtts.XmlIgnore = false;

            //specify override for trajectoryStation which accept ignore attribute
            XmlAttributeOverrides xmlAttsOverrides = new XmlAttributeOverrides();
            xmlAttsOverrides.Add(typeof(obj_mudLog), "geologyInterval", xmlAtts);

            //do serialize with override parameter
            return this.Serialize(xmlAttsOverrides, SerializerType.MudlogWithGeology);
        }

        /// <summary>
        /// Force Serialize geologyInterval
        /// </summary>
        /// <returns></returns>
        public string SerializeWithGeologyIntervalClean()
        {
            XmlAttributes xmlAtts = new XmlAttributes();
            xmlAtts.XmlIgnore = false;

            //specify override for trajectoryStation which accept ignore attribute
            XmlAttributeOverrides xmlAttsOverrides = new XmlAttributeOverrides();
            xmlAttsOverrides.Add(typeof(obj_mudLog), "geologyInterval", xmlAtts);

            //do serialize with override parameter
            return this.SerializeClean(xmlAttsOverrides, SerializerType.MudlogWithGeology);
        }

        /// <summary>
        /// Convert XML string data into log fields value
        /// </summary>
        /// <param name="xmlReader">reader that has log node</param>
        /// <param name="IsNew">Is the object from stratch(true) or from db/serialization(false)</param>
        public void HandleXML(XmlReader xmlReader, bool isNew)
        {
            //to be used for determining add or update for recurring object with uid
            bool isParameterNew = this.parameter == null;

            //create temporary list for data manipulation
            List<refNameString> listRelatedLog = this.relatedLog == null ? new List<refNameString>() : new List<refNameString>(this.relatedLog);
            List<cs_mudLogParameter> listMudParameter = isParameterNew ? new List<cs_mudLogParameter>() : new List<cs_mudLogParameter>(this.parameter);
            //temporary list
            List<cs_geologyInterval> listGeologyInterval = this.geologyInterval == null ? new List<cs_geologyInterval>() : new List<cs_geologyInterval>(this.geologyInterval);

            using (xmlReader)
            {
                //assign uid for the new well
                xmlReader.MoveToContent();
                if (xmlReader.HasAttributes)
                {
                    if (xmlReader.MoveToAttribute("uidWell"))
                        StaticHelper.HandleUID(xmlReader.Value, "mudLog uidWell", out this.uidWell);
                    if (xmlReader.MoveToAttribute("uidWellbore"))
                        StaticHelper.HandleUID(xmlReader.Value, "mudLog uidWellbore", out this.uidWellbore);
                    if (xmlReader.MoveToAttribute("uid"))
                        StaticHelper.HandleUID(xmlReader.Value, "mudLog uid", out this.uid);
                    xmlReader.MoveToElement();
                }
                while (xmlReader.Read())
                {
                    //only take child node
                    if (xmlReader.Depth == 1 && xmlReader.IsStartElement() && (!xmlReader.IsEmptyElement || xmlReader.HasAttributes))
                    {
                        switch (xmlReader.Name)
                        {
                            case "nameWell":
                                this.nameWell = StaticHelper.ReadString(xmlReader);
                                break;
                            case "nameWellbore":
                                this.nameWellbore = StaticHelper.ReadString(xmlReader);
                                break;
                            case "name":
                                this.name = StaticHelper.ReadString(xmlReader);
                                break;
                            case "objectGrowing":
                                this.objectGrowing = StaticParser.ParseBooleanFromString(StaticHelper.ReadString(xmlReader));
                                this.objectGrowingSpecified = true;
                                break;
                            case "dTim":
                                this.dTim = StaticParser.ParseDateTimeFromString(StaticHelper.ReadString(xmlReader));
                                this.dTimSpecified = true;
                                break;
                            case "mudLogCompany":
                                this.mudLogCompany = StaticHelper.ReadString(xmlReader);
                                break;
                            case "mudLogEngineers":
                                this.mudLogEngineers = StaticHelper.ReadString(xmlReader);
                                break;
                            case "startMd":
                                //object
                                if (this.startMd == null)
                                    this.startMd = new measuredDepthCoord();
                                this.startMd.HandleXML(xmlReader.ReadSubtree(), isNew);
                                break;
                            case "endMd":
                                //object
                                if (this.endMd == null)
                                    this.endMd = new measuredDepthCoord();
                                this.endMd.HandleXML(xmlReader.ReadSubtree(), isNew);
                                break;
                            case "relatedLog":
                                //multiple object
                                refNameString relatedLog = new refNameString();
                                relatedLog.HandleXML(xmlReader.ReadSubtree(), isNew);
                                listRelatedLog.Add(relatedLog);
                                break;
                            case "parameter":
                                //multiple recurring object
                                StaticHelper.AddUpdateWithUid<cs_mudLogParameter>(xmlReader, isParameterNew, ref listMudParameter);
                                break;
                            case "geologyInterval":
                                bool isUidEmpty = false;
                                //multiple recurring object
                                //do checking to database
                                cs_geologyInterval geologyIntervalObj = null;
                                string currentGeoIntervalUID = string.Empty;

                                if (xmlReader.HasAttributes)
                                {
                                    //check for uid attribute and assign it to temporary string for searching in collection
                                    if (xmlReader.MoveToAttribute("uid"))
                                    {
                                        currentGeoIntervalUID = xmlReader.Value;
                                        xmlReader.MoveToElement();
                                    }
                                }

                                //handling for recursive object with no uid
                                if (String.IsNullOrEmpty(currentGeoIntervalUID))
                                {
                                    //generate uid for it
                                    currentGeoIntervalUID = StaticHelper.GenerateObjectUID();
                                    isUidEmpty = true;
                                }
                                else
                                {
                                    //check whether the object exist on database and get it from db(this method is overriden on store)
                                    geologyIntervalObj = this.CheckForExistence<cs_geologyInterval>(currentGeoIntervalUID, null);
                                }

                                bool isGeologyIntervalNew;
                                //assign operationtype to cs_geology_interval
                                if (geologyIntervalObj == null)
                                {
                                    //new
                                    isGeologyIntervalNew = true;

                                    //check whether object with same uid already exists on collection,if exist,throw exception
                                    cs_geologyInterval geoIntervalObjCheck = listGeologyInterval.FirstOrDefault(
                                       delegate(cs_geologyInterval objGeoInterval)
                                       { if (objGeoInterval.uid == currentGeoIntervalUID)return true; else return false; });
                                    if (geoIntervalObjCheck != null)
                                    {
                                        throw new Exceptions.UnableToDecodeException("Geology Interval with same uid value of " + currentGeoIntervalUID + " already exists on the xml file", WITSMLReturnCode.WITSMLObjectExistOnXMLFile, true);
                                    }

                                    geologyIntervalObj = new cs_geologyInterval();
                                    geologyIntervalObj.operationType = OperationType.Add;
                                }
                                else
                                {
                                    //update
                                    isGeologyIntervalNew = false;
                                    geologyIntervalObj.operationType = OperationType.Update;

                                }
                                //Fill/Update the object on memory
                                geologyIntervalObj.HandleXML(xmlReader.ReadSubtree(), isGeologyIntervalNew);
                                //fill uid if it is empty
                                if (isUidEmpty)
                                    geologyIntervalObj.uid = currentGeoIntervalUID;
                                //Add to list for common handle
                                listGeologyInterval.Add(geologyIntervalObj);
                                break;
                            case "commonData":
                                if (this.commonData == null)
                                    this.commonData = new cs_commonData();
                                this.commonData.HandleXML(xmlReader.ReadSubtree(), isNew);
                                break;
                            case "customData":
                                if (this.customData == null)
                                    this.customData = new cs_customData();
                                this.customData.HandleXML(xmlReader.ReadSubtree(), isNew);
                                break;
                            default:
                                break;
                        }
                    }
                }
                //assign temp list back to array
                this.relatedLog = listRelatedLog.ToArray();
                this.parameter = listMudParameter.ToArray();
                this.geologyInterval = listGeologyInterval.ToArray();
            }
        }

        public override string ValidateGetPath(string newUid)
        {
            // Fix uid if the Object dosn't have one.
            if (!IsValidUid(this.uid))
            {
                this.uid = newUid;
                // TODO: Add some loging that we created a new UID for the object maybe.
            }

            StringBuilder path = new StringBuilder(this.GetParentPath());
            path.Append(@"\");
            path.Append(this.uid);
            return path.ToString();
        }

        public override string GetParentPath()
        {
            if (!IsValidUid(this.uidWell) || !IsValidUid(this.uidWellbore))
            {
                throw new UnableToDecodeException("One or more Parent UID's are invalid", WITSMLReturnCode.WITSMLInvalidParentUID);
                //TODO: Log the issue
            }

            StringBuilder path = new StringBuilder(@"\");
            path.Append(this.uidWell);
            path.Append(@"\");
            path.Append(this.uidWellbore);
            return path.ToString();
        }

        // NOTE: these are done with functions, instead of property access, so as not to mess around with the auto-generated / serialized classes.
        public override string Get_uid()
        {
            return this.uid;
        }

        public override string Get_uidWell()
        {
            return this.uidWell;
        }

        public override string Get_uidWellbore()
        {
            return this.uidWellbore;
        }

        public override string Get_name()
        {
            return this.name;
        }

        public override string Get_nameWell()
        {
            return this.nameWell;
        }

        public override string Get_nameWellbore()
        {
            return this.nameWellbore;
        }

        public override ICommonData Get_commonData()
        {
            if (this.commonData == null)
            {
                this.commonData = new cs_commonData();
            }
            return (ICommonData)this.commonData;
        }
        public override ICustomData Get_customData()
        {
            if (this.customData == null)
            {
                this.customData = new cs_customData();
            }
            return (ICustomData)this.customData;
        }
    }
}
