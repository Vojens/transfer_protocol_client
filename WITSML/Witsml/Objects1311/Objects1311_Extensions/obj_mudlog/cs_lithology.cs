 

using System;
using System.Collections.Generic;
using System.Text;
using System.Xml;

namespace Witsml.Objects1311
{
    //TODO : decide what happen wtih qualifier no uid
    public partial class cs_lithology : IHandleXML
    {
        /// <summary>
        /// Convert XML string data into lithology fields value
        /// </summary>
        /// <param name="xmlReader">reader that has lithology node</param>
        /// <param name="IsNew">Is the object from stratch(true) or from db/serialization(false)</param>
        public void HandleXML(XmlReader xmlReader, bool isNew)
        {
            //to be used to determine add or update of recurring object
            bool isQualifierNew = this.qualifier == null;
            //create temporary list to handle data manipulation
            List<cs_qualifier> listQualifier = isQualifierNew ? new List<cs_qualifier>() : new List<cs_qualifier>(this.qualifier);
            using (xmlReader)
            {
                xmlReader.MoveToContent();
                if (xmlReader.HasAttributes)
                {
                    if (xmlReader.MoveToAttribute("uid"))
                        StaticHelper.HandleUID(xmlReader.Value, "lithology uid", out this.uid);
                    xmlReader.MoveToElement();
                }
                while (xmlReader.Read())
                {
                    if (xmlReader.Depth == 1 && xmlReader.IsStartElement() && !xmlReader.IsEmptyElement)
                    {
                        switch (xmlReader.Name)
                        {
                            case "type":
                                this.type = StaticHelper.ReadString(xmlReader);
                                break;
                            case "codeLith":
                                this.codeLith = StaticHelper.ReadString(xmlReader);
                                break;
                            case "lithPc":
                                if (this.lithPc == null)
                                    this.lithPc = new volumePerVolumeMeasurePercent();
                                this.lithPc.HandleXML(xmlReader.ReadSubtree(), isNew);
                                break;
                            case "description":
                                this.description = StaticHelper.ReadString(xmlReader);
                                break;
                            case "lithClass":
                                this.lithClass = StaticHelper.ReadString(xmlReader);
                                break;
                            case "grainType":
                                this.grainType = StaticHelper.ReadString(xmlReader);
                                break;
                            case "dunhamClass":
                                this.dunhamClass = StaticHelper.ReadString(xmlReader);
                                break;
                            case "color":
                                this.color = StaticHelper.ReadString(xmlReader);
                                break;
                            case "texture":
                                this.texture = StaticHelper.ReadString(xmlReader);
                                break;
                            case "hardness":
                                this.hardness = StaticHelper.ReadString(xmlReader);
                                break;
                            case "sizeGrain":
                                this.sizeGrain = StaticHelper.ReadString(xmlReader);
                                break;
                            case "roundness":
                                this.roundness = StaticHelper.ReadString(xmlReader);
                                break;
                            case "sorting":
                                this.sorting = StaticHelper.ReadString(xmlReader);
                                break;
                            case "matrixCement":
                                this.matrixCement = StaticHelper.ReadString(xmlReader);
                                break;
                            case "porosityVisible":
                                this.porosityVisible = StaticHelper.ReadString(xmlReader);
                                break;
                            case "permeability":
                                this.permeability = StaticHelper.ReadString(xmlReader);
                                break;
                            case "densShale":
                                if (this.densShale == null)
                                    this.densShale = new densityMeasure();
                                this.densShale.HandleXML(xmlReader.ReadSubtree(), isNew);
                                break;
                            case "qualifier":
                                //collection
                                StaticHelper.AddUpdateWithUid<cs_qualifier>(xmlReader, isQualifierNew, ref listQualifier);
                                break;
                            default:
                                break;
                        }
                    }
                }
                //assing back collection array
                this.qualifier = listQualifier.ToArray();
            }
        }
    }
}
