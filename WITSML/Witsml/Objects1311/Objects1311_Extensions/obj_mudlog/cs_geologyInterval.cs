 

using System;
using System.Collections.Generic;
using System.Text;
using System.Xml;
using System.IO;
using System.Linq;
using Witsml.Common.Exceptions;


namespace Witsml.Objects1311
{
    //TODO : Lithology
    [System.Xml.Serialization.XmlRootAttribute("geologyInterval", Namespace = "http://www.witsml.org/schemas/131", IsNullable = false)]
    public partial class cs_geologyInterval : WITSMLSubElement, IHandleXML
    {
        public cs_geologyInterval()
        {
            this.typeLithology = LithologySource.unknown;
        }

        public override void Decode(ref string xmlIn)
        {
            this.version = "1.3.1.1";
            //used to determine add or update
            try
            {
                //Consume XML string and convert node value to variable based on node name
                using (StringReader stringReader = new StringReader(xmlIn))
                {
                    XmlReaderSettings setting = new XmlReaderSettings();
                    setting.ConformanceLevel = ConformanceLevel.Document;
                    setting.IgnoreComments = true;
                    setting.IgnoreWhitespace = true;
                    setting.IgnoreProcessingInstructions = true;
                    using (XmlReader xmlReader = XmlReader.Create(stringReader, setting))
                    {
                        //determine add or update operation
                        bool isNew;
                        if (this.uid == string.Empty)
                            isNew = true;
                        else isNew = false;

                        this.HandleXML(xmlReader, isNew);
                    }
                }
            }
            catch (XmlException ex)
            {
                throw new Exceptions.UnableToDecodeException(ex.Message, WITSMLReturnCode.WITSMLDocumentError, ex.InnerException);
            }
            catch (NullReferenceException ex)
            {
                throw new Exceptions.UnableToDecodeException(ex.Message, WITSMLReturnCode.WITSMLEmptyDocument, ex.InnerException);
            }
        }

        /// <summary>
        /// Convert XML string data into geologyInterval fields value
        /// </summary>
        /// <param name="xmlReader">reader that has geologyInterval node</param>
        /// <param name="IsNew">Is the object from stratch(true) or from db/serialization(false)</param>
        public void HandleXML(XmlReader xmlReader, bool isNew)
        {
            //to be used to determine add or update of recurring object
            bool isLithologyNew = this.lithology == null;
            //create temporary list to handle data manipulation
            List<cs_lithology> listLithology = isLithologyNew ? new List<cs_lithology>() : new List<cs_lithology>(this.lithology);
            List<string> listNameFormation = this.nameFormation == null ? new List<string>() : new List<string>(this.nameFormation);
            List<string> listLithostratigraphic = this.lithostratigraphic == null ? new List<string>() : new List<string>(this.lithostratigraphic);
            List<string> listChronostratigraphic = this.chronostratigraphic == null ? new List<string>() : new List<string>(this.chronostratigraphic);
            using (xmlReader)
            {
                xmlReader.MoveToContent();
                if (xmlReader.HasAttributes)
                {
                    if (xmlReader.MoveToAttribute("uid"))
                        StaticHelper.HandleUID(xmlReader.Value, "geologyInterval uid", out this.uid);
                    xmlReader.MoveToElement();
                }
                while (xmlReader.Read())
                {
                    if (xmlReader.Depth == 1 && xmlReader.IsStartElement() && !xmlReader.IsEmptyElement)
                    {
                        switch (xmlReader.Name)
                        {
                            case "typeLithology":
                                this.typeLithology = StaticParser.ParseEnumFromString<LithologySource>(StaticHelper.ReadString(xmlReader), LithologySource.unknown);
                                break;
                            case "mdTop":
                                if (this.mdTop == null)
                                    this.mdTop = new measuredDepthCoord();
                                this.mdTop.HandleXML(xmlReader.ReadSubtree(), isNew);
                                break;
                            case "mdBottom":
                                if (this.mdBottom == null)
                                    this.mdBottom = new measuredDepthCoord();
                                this.mdBottom.HandleXML(xmlReader.ReadSubtree(), isNew);
                                break;
                            case "dTim":
                                this.dTim = StaticParser.ParseDateTimeFromString(StaticHelper.ReadString(xmlReader));
                                this.dTimSpecified = true;
                                break;
                            case "tvdTop":
                                if (this.tvdTop == null)
                                    this.tvdTop = new wellVerticalDepthCoord();
                                this.tvdTop.HandleXML(xmlReader.ReadSubtree(), isNew);
                                break;
                            case "tvdBase":
                                if (this.tvdBase == null)
                                    this.tvdBase = new wellVerticalDepthCoord();
                                this.tvdBase.HandleXML(xmlReader.ReadSubtree(), isNew);
                                break;
                            case "ropAv":
                                //
                                if (this.ropAv == null)
                                    this.ropAv = new velocityMeasure();
                                this.ropAv.HandleXML(xmlReader.ReadSubtree(), isNew);
                                break;
                            case "ropMn":
                                if (this.ropMn == null)
                                    this.ropMn = new velocityMeasure();
                                this.ropMn.HandleXML(xmlReader.ReadSubtree(), isNew);
                                break;
                            case "ropMx":
                                if (this.ropMx == null)
                                    this.ropMx = new velocityMeasure();
                                this.ropMx.HandleXML(xmlReader.ReadSubtree(), isNew);
                                break;
                            case "wobAv":
                                if (this.wobAv == null)
                                    this.wobAv = new forceMeasure();
                                this.wobAv.HandleXML(xmlReader.ReadSubtree(), isNew);
                                break;
                            case "tqAv":
                                if (this.tqAv == null)
                                    this.tqAv = new momentOfForceMeasure();
                                this.tqAv.HandleXML(xmlReader.ReadSubtree(), isNew);
                                break;
                            case "currentAv":
                                if (this.currentAv == null)
                                    this.currentAv = new electricCurrentMeasure();
                                this.currentAv.HandleXML(xmlReader.ReadSubtree(), isNew);
                                break;
                            case "rpmAv":
                                if (this.rpmAv == null)
                                    this.rpmAv = new anglePerTimeMeasure();
                                this.rpmAv.HandleXML(xmlReader.ReadSubtree(), isNew);
                                break;
                            case "wtMudAv":
                                if (this.wtMudAv == null)
                                    this.wtMudAv = new densityMeasure();
                                this.wtMudAv.HandleXML(xmlReader.ReadSubtree(), isNew);
                                break;
                            case "ecdTdAv":
                                if (this.ecdTdAv == null)
                                    this.ecdTdAv = new densityMeasure();
                                this.ecdTdAv.HandleXML(xmlReader.ReadSubtree(), isNew);
                                break;
                            case "dxcAv":
                                this.dxcAv = StaticParser.ParseDoubleFromString(StaticHelper.ReadString(xmlReader));
                                this.dxcAvSpecified = true;
                                break;
                            case "lithology":
                                //collection
                                cs_lithology lithologyObj;
                                string currentLithologyUID = string.Empty;

                                if (xmlReader.HasAttributes)
                                {
                                    //check for uid attribute and assign it to temporary string for searching in collection
                                    if (xmlReader.MoveToAttribute("uid"))
                                    {
                                        currentLithologyUID = xmlReader.Value;
                                        xmlReader.MoveToElement();
                                    }
                                }

                                //TODO :decide what happen with empty uid
                                if (String.IsNullOrEmpty(currentLithologyUID))
                                {
                                    continue;
                                }

                                //Check Operation to be perform
                                if (!isLithologyNew)
                                {
                                    //Update Operation
                                    if (xmlReader.HasAttributes)
                                    {
                                        //check for uid attribute and assign it to temporary string for searching in collection
                                        //look for qualifier based on its UID and update the well with xml string
                                        lithologyObj = listLithology.FirstOrDefault(
                                            delegate(cs_lithology objLithology)
                                            { if (objLithology.uid == currentLithologyUID)return true; else return false; });
                                        if (lithologyObj != null)
                                        {
                                            lithologyObj.HandleXML(xmlReader.ReadSubtree(), isLithologyNew);
                                            continue;
                                        }
                                    }
                                }

                                //Add Operation
                                lithologyObj = new cs_lithology();
                                lithologyObj.HandleXML(xmlReader.ReadSubtree(), isLithologyNew);
                                listLithology.Add(lithologyObj);
                                break;
                            case "show":
                                if (this.show == null)
                                    this.show = new cs_show();
                                this.show.HandleXML(xmlReader.ReadSubtree(), isNew);
                                break;
                            case "chromatograph":
                                if (this.chromatograph == null)
                                    this.chromatograph = new cs_chromatograph();
                                this.chromatograph.HandleXML(xmlReader.ReadSubtree(), isNew);
                                break;
                            case "mudGas":
                                if (this.mudGas == null)
                                    this.mudGas = new cs_mudGas();
                                this.mudGas.HandleXML(xmlReader.ReadSubtree(), isNew);
                                break;
                            case "densBulk":
                                if (this.densBulk == null)
                                    this.densBulk = new densityMeasure();
                                this.densBulk.HandleXML(xmlReader.ReadSubtree(), isNew);
                                break;
                            case "densShale":
                                if (this.densShale == null)
                                    this.densShale = new densityMeasure();
                                this.densShale.HandleXML(xmlReader.ReadSubtree(), isNew);
                                break;
                            case "calcite":
                                if (this.calcite == null)
                                    this.calcite = new volumePerVolumeMeasure();
                                this.calcite.HandleXML(xmlReader.ReadSubtree(), isNew);
                                break;
                            case "dolomite":
                                if (this.dolomite == null)
                                    this.dolomite = new volumePerVolumeMeasure();
                                this.dolomite.HandleXML(xmlReader.ReadSubtree(), isNew);
                                break;
                            case "cec":
                                if (this.cec == null)
                                    this.cec = new equivalentPerMassMeasure();
                                this.cec.HandleXML(xmlReader.ReadSubtree(), isNew);
                                break;
                            case "qft":
                                if (this.qft == null)
                                    this.qft = new illuminanceMeasure();
                                this.qft.HandleXML(xmlReader.ReadSubtree(), isNew);
                                break;
                            case "calcStab":
                                if (this.calcStab == null)
                                    this.calcStab = new volumePerVolumeMeasure();
                                this.calcStab.HandleXML(xmlReader.ReadSubtree(), isNew);
                                break;
                            case "nameFormation":
                                //collection string
                                listNameFormation.Add(StaticHelper.ReadString(xmlReader));
                                break;
                            case "lithostratigraphic":
                                //collection string
                                listLithostratigraphic.Add(StaticHelper.ReadString(xmlReader));
                                break;
                            case "chronostratigraphic":
                                //collection string
                                listChronostratigraphic.Add(StaticHelper.ReadString(xmlReader));
                                break;
                            case "sizeMn":
                                if (this.sizeMn == null)
                                    this.sizeMn = new lengthMeasure();
                                this.sizeMn.HandleXML(xmlReader.ReadSubtree(), isNew);
                                break;
                            case "sizeMx":
                                if (this.sizeMx == null)
                                    this.sizeMx = new lengthMeasure();
                                this.sizeMx.HandleXML(xmlReader.ReadSubtree(), isNew);
                                break;
                            case "lenPlug":
                                if (this.lenPlug == null)
                                    this.lenPlug = new lengthMeasure();
                                this.lenPlug.HandleXML(xmlReader.ReadSubtree(), isNew);
                                break;
                            case "description":
                                this.description = StaticHelper.ReadString(xmlReader);
                                break;
                            case "cuttingFluid":
                                this.cuttingFluid = StaticHelper.ReadString(xmlReader);
                                break;
                            case "cleaningMethod":
                                this.cleaningMethod = StaticHelper.ReadString(xmlReader);
                                break;
                            case "dryingMethod":
                                this.dryingMethod = StaticHelper.ReadString(xmlReader);
                                break;
                            case "comments":
                                this.comments = StaticHelper.ReadString(xmlReader);
                                break;
                            default:
                                break;
                        }
                    }
                }
                //assign back list member to array
                this.lithology = listLithology.ToArray();
                this.nameFormation = listNameFormation.ToArray();
                this.lithostratigraphic = listLithostratigraphic.ToArray();
                this.chronostratigraphic = listChronostratigraphic.ToArray();
            }
        }

    }
}
