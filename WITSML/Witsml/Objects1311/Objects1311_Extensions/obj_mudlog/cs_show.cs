 

using System;
using System.Collections.Generic;
using System.Text;
using System.Xml;

namespace Witsml.Objects1311
{
    public partial class cs_show : IHandleXML
    {
        public cs_show()
        {
            this.showRat = ShowRating.unknown;
            this.natFlorLevel = ShowFluorescence.unknown;
            this.cutSpeed = ShowSpeed.unknown;
            this.cutForm = ShowLevel.unknown;
            this.cutFlorSpeed = ShowSpeed.unknown;
            this.cutFlorForm = ShowLevel.unknown;
            this.cutFlorLevel = ShowFluorescence.unknown;
        }
        /// <summary>
        /// Convert XML string data into show fields value
        /// </summary>
        /// <param name="xmlReader">reader that has show node</param>
        /// <param name="IsNew">Is the object from stratch(true) or from db/serialization(false)</param>
        public void HandleXML(XmlReader xmlReader, bool isNew)
        {
            using (xmlReader)
            {
                while (xmlReader.Read())
                {
                    if (xmlReader.Depth == 1 && xmlReader.IsStartElement() && !xmlReader.IsEmptyElement)
                    {
                        switch (xmlReader.Name)
                        {
                            case "showRat":
                                StaticParser.SetEnumFromString<ShowRating>(StaticHelper.ReadString(xmlReader), out this.showRat, out this.showRatSpecified);
                                break;
                            case "stainColor":
                                this.stainColor = StaticHelper.ReadString(xmlReader);
                                break;
                            case "stainDistr":
                                this.stainDistr = StaticHelper.ReadString(xmlReader);
                                break;
                            case "stainPc":
                                if (this.stainPc == null)
                                    this.stainPc = new areaPerAreaMeasure();
                                this.stainPc.HandleXML(xmlReader.ReadSubtree(), isNew);
                                break;
                            case "natFlorColor":
                                this.natFlorColor = StaticHelper.ReadString(xmlReader);
                                break;
                            case "natFlorPc":
                                if (this.natFlorPc == null)
                                    this.natFlorPc = new areaPerAreaMeasure();
                                this.natFlorPc.HandleXML(xmlReader.ReadSubtree(), isNew);
                                break;
                            case "natFlorLevel":
                                StaticParser.SetEnumFromString<ShowFluorescence>(StaticHelper.ReadString(xmlReader), out this.natFlorLevel, out this.natFlorLevelSpecified);
                                break;
                            case "natFlorDesc":
                                this.natFlorDesc = StaticHelper.ReadString(xmlReader);
                                break;
                            case "cutColor":
                                this.cutColor = StaticHelper.ReadString(xmlReader);
                                break;
                            case "cutSpeed":
                                StaticParser.SetEnumFromString<ShowSpeed>(StaticHelper.ReadString(xmlReader), out this.cutSpeed, out this.cutSpeedSpecified);
                                break;
                            case "cutStrength":
                                this.cutStrength = StaticHelper.ReadString(xmlReader);
                                break;
                            case "cutForm":
                                StaticParser.SetEnumFromString<ShowLevel>(StaticHelper.ReadString(xmlReader), out this.cutForm, out this.cutFormSpecified);
                                break;
                            case "cutLevel":
                                this.cutLevel = StaticHelper.ReadString(xmlReader);
                                break;
                            case "cutFlorColor":
                                this.cutFlorColor = StaticHelper.ReadString(xmlReader);
                                break;
                            case "cutFlorSpeed":
                                StaticParser.SetEnumFromString<ShowSpeed>(StaticHelper.ReadString(xmlReader),out this.cutFlorSpeed,out this.cutFlorSpeedSpecified);
                                break;
                            case "cutFlorStrength":
                                this.cutFlorStrength = StaticHelper.ReadString(xmlReader);
                                break;
                            case "cutFlorForm":
                                StaticParser.SetEnumFromString<ShowLevel>(StaticHelper.ReadString(xmlReader), out this.cutFlorForm, out this.cutFlorFormSpecified);
                                break;
                            case "cutFlorLevel":
                                StaticParser.SetEnumFromString<ShowFluorescence>(StaticHelper.ReadString(xmlReader), out this.cutFlorLevel, out this.cutFlorLevelSpecified);
                                break;
                            case "residueColor":
                                this.residueColor = StaticHelper.ReadString(xmlReader);
                                break;
                            case "showDesc":
                                this.showDesc = StaticHelper.ReadString(xmlReader);
                                break;
                            case "impregnatedLitho":
                                this.impregnatedLitho = StaticHelper.ReadString(xmlReader);
                                break;
                            case "odor":
                                this.odor = StaticHelper.ReadString(xmlReader);
                                break;
                            default:
                                break;
                        }
                    }
                }
            }
        }
    }
}
