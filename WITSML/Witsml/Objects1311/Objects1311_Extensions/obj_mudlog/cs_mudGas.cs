 

using System;
using System.Collections.Generic;
using System.Text;
using System.Xml;

namespace Witsml.Objects1311
{
    public partial class cs_mudGas : IHandleXML
    {
        /// <summary>
        /// Convert XML string data into chromatograph fields value
        /// </summary>
        /// <param name="xmlReader">reader that has chromatograph node</param>
        /// <param name="IsNew">Is the object from stratch(true) or from db/serialization(false)</param>
        public void HandleXML(XmlReader xmlReader, bool isNew)
        {
            using (xmlReader)
            {
                while (xmlReader.Read())
                {
                    if (xmlReader.Depth == 1 && xmlReader.IsStartElement() && !xmlReader.IsEmptyElement)
                    {
                        switch (xmlReader.Name)
                        {
                            case "gasAv":
                                if (this.gasAv == null)
                                    this.gasAv = new volumePerVolumeMeasure();
                                this.gasAv.HandleXML(xmlReader.ReadSubtree(), isNew);
                                break;
                            case "gasPeak":
                                if (this.gasPeak == null)
                                    this.gasPeak = new volumePerVolumeMeasure();
                                this.gasPeak.HandleXML(xmlReader.ReadSubtree(), isNew);
                                break;
                            case "gasPeakType":
                                StaticParser.SetEnumFromString<GasPeakType>(StaticHelper.ReadString(xmlReader), out this.gasPeakType, out this.gasPeakTypeSpecified);
                                break;
                            case "gasBackgnd":
                                if (this.gasBackgnd == null)
                                    this.gasBackgnd = new volumePerVolumeMeasure();
                                this.gasBackgnd.HandleXML(xmlReader.ReadSubtree(), isNew);
                                break;
                            case "gasConAv":
                                if (this.gasConAv == null)
                                    this.gasConAv = new volumePerVolumeMeasure();
                                this.gasConAv.HandleXML(xmlReader.ReadSubtree(), isNew);
                                break;
                            case "gasConMx":
                                if (this.gasConMx == null)
                                    this.gasConMx = new volumePerVolumeMeasure();
                                this.gasConMx.HandleXML(xmlReader.ReadSubtree(), isNew);
                                break;
                            case "gasTrip":
                                if (this.gasTrip == null)
                                    this.gasTrip = new volumePerVolumeMeasure();
                                this.gasTrip.HandleXML(xmlReader.ReadSubtree(), isNew);
                                break;
                            default:
                                break;
                        }
                    }
                }
            }
        }
    }
}
