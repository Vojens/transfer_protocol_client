 

using System;
using System.Collections.Generic;
using System.Text;
using System.Xml;

namespace Witsml.Objects1311
{
    public partial class cs_channelDefinition : IHandleXML
    {
        public cs_channelDefinition()
        {
            this.dataType = LogDataType.unknown;
        }

        /// <summary>
        /// Convert XML string data into channel Def fields value
        /// </summary>
        /// <param name="xmlReader">reader that has timeMeasure node</param>
        /// <param name="IsNew">Is the object from stratch(true) or from db/serialization(false)</param>
        public void HandleXML(XmlReader xmlReader, bool isNew)
        {
            //temporary list for data manipulations
            List<cs_axisDefinition> listAxisDef = this.axisDefinition == null ? new List<cs_axisDefinition>() : new List<cs_axisDefinition>(this.axisDefinition);

            using (xmlReader)
            {
                while (xmlReader.Read())
                {
                    if (xmlReader.Depth == 1 && xmlReader.IsStartElement() && !xmlReader.IsEmptyElement)
                    {
                        switch (xmlReader.Name)
                        {
                            case "mnemonic":
                                this.mnemonic = StaticHelper.ReadString(xmlReader);
                                break;
                            case "dataType":
                                StaticParser.SetEnumFromString<LogDataType>(StaticHelper.ReadString(xmlReader), out this.dataType, out this.dataTypeSpecified);
                                break;
                            case "classWitsml":
                                this.classWitsml = StaticHelper.ReadString(xmlReader);
                                break;
                            case "columnIndex":
                                this.columnIndex = StaticParser.ParseShortFromString(StaticHelper.ReadString(xmlReader));
                                this.columnIndexSpecified = true;
                                break;
                            case "unit":
                                this.unit = StaticHelper.ReadString(xmlReader);
                                break;
                            case "description":
                                this.description = StaticHelper.ReadString(xmlReader);
                                break;
                            case "mnemAlias":
                                this.mnemAlias = StaticHelper.ReadString(xmlReader);
                                break;
                            case "sensorOffset":
                                if (this.sensorOffset == null)
                                    this.sensorOffset = new lengthMeasure();
                                this.sensorOffset.HandleXML(xmlReader.ReadSubtree(), isNew);
                                break;
                            case "dataSource":
                                this.dataSource = StaticHelper.ReadString(xmlReader);
                                break;
                            case "interval":
                                if (this.interval == null)
                                    this.interval = new cs_interval();
                                this.interval.HandleXML(xmlReader.ReadSubtree(), isNew);
                                break;
                            case "axisDefinition":
                                //collection
                                cs_axisDefinition axisDef = new cs_axisDefinition();
                                axisDef.HandleXML(xmlReader.ReadSubtree(), isNew);
                                listAxisDef.Add(axisDef);
                                break;
                            default:
                                break;
                        }
                    }
                }
                //assign list to collection
                this.axisDefinition = listAxisDef.ToArray();
            }
        }
    }
}
