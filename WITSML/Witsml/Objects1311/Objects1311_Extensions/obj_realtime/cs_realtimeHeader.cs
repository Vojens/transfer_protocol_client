 

using System;
using System.Collections.Generic;
using System.Text;
using System.Xml;

namespace Witsml.Objects1311
{
    public partial class cs_realtimeHeader : IHandleXML
    {
        /// <summary>
        /// Convert XML string data into realtimeheader fields value
        /// </summary>
        /// <param name="xmlReader">reader that has forcePerVolumeMeasure node</param>
        /// <param name="IsNew">Is the object from stratch(true) or from db/serialization(false)</param>
        public void HandleXML(XmlReader xmlReader, bool isNew)
        {
            List<cs_groupDefinition> listGroupDefinition = this.groupDefinition == null ? new List<cs_groupDefinition>() : new List<cs_groupDefinition>(this.groupDefinition);

            using (xmlReader)
            {
                while (xmlReader.Read())
                {
                    //only take child node
                    if (xmlReader.Depth == 1 && xmlReader.IsStartElement() && !xmlReader.IsEmptyElement)
                    {
                        switch (xmlReader.Name)
                        {
                            case "nameWell":
                                this.nameWell = StaticHelper.ReadString(xmlReader);
                                break;
                            case "nameWellbore":
                                this.nameWellbore = StaticHelper.ReadString(xmlReader);
                                break;
                            case "serviceCompany":
                                this.serviceCompany = StaticHelper.ReadString(xmlReader);
                                break;
                            case "runNumber":
                                this.runNumber = StaticHelper.ReadString(xmlReader);
                                break;
                            case "bhaRunNumber":
                                this.bhaRunNumber = StaticParser.ParseShortFromString(StaticHelper.ReadString(xmlReader));
                                this.bhaRunNumberSpecified = true;
                                break;
                            case "pass":
                                this.pass = StaticHelper.ReadString(xmlReader);
                                break;
                            case "creationDate":
                                this.creationDate = StaticParser.ParseDateTimeFromString(StaticHelper.ReadString(xmlReader));
                                this.creationDateSpecified = true;
                                break;
                            case "description":
                                this.description = StaticHelper.ReadString(xmlReader);
                                break;
                            case "groupDefinition":
                                //collection
                                cs_groupDefinition groupDefinition = new cs_groupDefinition();
                                groupDefinition.HandleXML(xmlReader.ReadSubtree(), isNew);
                                listGroupDefinition.Add(groupDefinition);
                                break;
                            default:
                                break;
                        }
                    }
                }
                //assign list to collection
                this.groupDefinition = listGroupDefinition.ToArray();
            }
        }
    }
}
