 

using System;
using System.Collections.Generic;
using System.Text;
using System.Xml;
using System.IO;
using Witsml.Common.Exceptions;


namespace Witsml.Objects1311
{
    public partial class obj_realtimes : WITSMLDocument
    {
        public override WITSMLObject GetSingleObject(int Item)
        {
            return this.realtime[Item];
        }

        /// <summary>
        /// Deserialize XML string into WitsmlDocument object
        /// </summary>
        /// <param name="XMLin">XML string to be serialized</param>
        /// <returns>Number of singular well inside</returns>
        public override int Decode(ref string xmlIn)
        {
            int countOfRealtime = 0;
            try
            {
                //check whether collection is from stratch(isNew = true) or from deserialization(isNew=false)
                bool isNew = this.realtime == null;

                //create temporary list to help with data manipulation
                List<obj_realtime> listRealtime = this.realtime == null ? new List<obj_realtime>() : new List<obj_realtime>(this.realtime);

                //Consume XML string and convert node value to variable based on node name
                using (StringReader stringReader = new StringReader(xmlIn))
                {
                    XmlReaderSettings setting = new XmlReaderSettings();
                    setting.ConformanceLevel = ConformanceLevel.Document;
                    setting.IgnoreComments = true;
                    setting.IgnoreWhitespace = true;
                    setting.IgnoreProcessingInstructions = true;
                    using (XmlReader xmlReader = XmlReader.Create(stringReader, setting))
                    {
                        //assign version
                        xmlReader.MoveToContent();
                        if (xmlReader.MoveToAttribute("version"))
                            this.version = xmlReader.Value;
                        else
                            this.version = "1.3.1.1";//no version specified,set it to 1.3.1.1
                        while (xmlReader.Read())
                        {
                            //pre-checking on whether node is the correct child based on depth,start tag and not empty
                            if (xmlReader.Depth == 1 && xmlReader.IsStartElement() && !xmlReader.IsEmptyElement)
                            {
                                switch (xmlReader.Name)
                                {
                                    case "documentInfo":
                                        if (this.documentInfo == null)
                                            this.documentInfo = new cs_documentInfo();
                                        this.documentInfo.HandleXML(xmlReader.ReadSubtree(), isNew);
                                        break;
                                    case "realtime":
                                        //no update for realtime obj
                                        obj_realtime realtime = new obj_realtime();
                                        realtime.HandleXML(xmlReader.ReadSubtree(), isNew);
                                        listRealtime.Add(realtime);
                                        countOfRealtime++;
                                        break;
                                    default:
                                        break;
                                }
                            }
                        }
                    }
                }
                //assign list to realtime collection
                this.realtime = listRealtime.ToArray();

            }
            catch (XmlException ex)
            {
                throw new Exceptions.UnableToDecodeException(ex.Message, WITSMLReturnCode.WITSMLDocumentError, ex.InnerException);
            }
            catch (NullReferenceException ex)
            {
                throw new Exceptions.UnableToDecodeException(ex.Message, WITSMLReturnCode.WITSMLEmptyDocument, ex.InnerException);
            }
            catch (Exceptions.UnableToDecodeException ex)
            {
                throw ex;
            }
            catch (Exception ex)
            {
                throw new Exceptions.UnableToDecodeException(ex.Message, WITSMLReturnCode.WITSMLUnkownCause, ex.InnerException);
            }
            return countOfRealtime;
        }
    }
}
