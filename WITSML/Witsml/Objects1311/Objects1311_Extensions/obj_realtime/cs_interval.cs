 

using System;
using System.Collections.Generic;
using System.Text;
using System.Xml;

namespace Witsml.Objects1311
{
    public partial class cs_interval : IHandleXML
    {
        public cs_interval()
        {
            this.type = IntervalType.unknown;
            this.method = IntervalMethod.unknown;
        }

        /// <summary>
        /// Convert XML string data into interval fields value
        /// </summary>
        /// <param name="xmlReader">reader that has timeMeasure node</param>
        /// <param name="IsNew">Is the object from stratch(true) or from db/serialization(false)</param>
        public void HandleXML(XmlReader xmlReader, bool isNew)
        {
            //to allow only one choice between distance and timeinterval
            bool hasChoice = false;
            using (xmlReader)
            {
                while (xmlReader.Read())
                {
                    if (xmlReader.Depth == 1 && xmlReader.IsStartElement() && !xmlReader.IsEmptyElement)
                    {
                        switch (xmlReader.Name)
                        {
                            case "type":
                                this.type = StaticParser.ParseEnumFromString<IntervalType>(StaticHelper.ReadString(xmlReader), IntervalType.unknown);
                                break;
                            case "method":
                                //replace space for special case "spot sample"
                               this.method = StaticParser.ParseEnumFromString<IntervalMethod>(StaticHelper.ReadString(xmlReader), IntervalMethod.unknown);
                                break;
                            case "distanceInterval":
                                if (!hasChoice)
                                {
                                    lengthMeasure obj = new lengthMeasure();
                                    obj.HandleXML(xmlReader.ReadSubtree(), isNew);
                                    this.Item = obj;
                                    hasChoice = true;
                                }
                                break;
                            case "timeInterval":
                                if (!hasChoice)
                                {
                                    timeMeasure obj = new timeMeasure();
                                    obj.HandleXML(xmlReader.ReadSubtree(), isNew);
                                    this.Item = obj;
                                    hasChoice = true;
                                }
                                break;
                            default:
                                break;
                        }
                    }
                }
            }
        }
    }
}
