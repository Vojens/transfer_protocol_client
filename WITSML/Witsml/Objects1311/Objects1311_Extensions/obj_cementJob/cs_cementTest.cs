﻿ 

using System;
using System.Collections.Generic;
using System.Text;
using System.Xml;

namespace Witsml.Objects1311
{
    public partial class cs_cementTest : IHandleXML
    {
        public cs_cementTest()
        {
        }
        /// <summary>
        /// Convert XML string data into cs_cementTest fields value
        /// </summary>
        /// <param name="xmlReader">reader that has cs_cementTest node</param>
        /// <param name="IsNew">Is the object from stratch(true) or from db/serialization(false)</param>
        public void HandleXML(XmlReader xmlReader, bool isNew)
        {
            using (xmlReader)
            {
                while (xmlReader.Read())
                {
                    //start on depth 1 which are the childs that we want to check against
                    //check for starting tag and convert node value to its expected variable
                    if (xmlReader.Depth == 1 && xmlReader.IsStartElement() && !xmlReader.IsEmptyElement)
                    {
                        switch (xmlReader.Name)
                        {
                            case "presTest":
                                if (this.presTest == null)
                                    this.presTest = new pressureMeasure();
                                this.presTest.HandleXML(xmlReader.ReadSubtree(), isNew);
                                break;
                            case "eTimTest":
                                if (this.eTimTest == null)
                                    this.eTimTest = new timeMeasure();
                                this.eTimTest.HandleXML(xmlReader.ReadSubtree(), isNew);
                                break;
                            case "cementShoeCollar":
                                this.cementShoeCollar = StaticParser.ParseBooleanFromString(StaticHelper.ReadString(xmlReader));
                                this.cementShoeCollarSpecified = true;
                                break;
                            case "cetRun":
                                this.cetRun = StaticParser.ParseBooleanFromString(StaticHelper.ReadString(xmlReader));
                                this.cetRunSpecified = true;
                                break;
                            case "cetBondQual":
                                this.cetBondQual = StaticParser.ParseBooleanFromString(StaticHelper.ReadString(xmlReader));
                                this.cetBondQualSpecified = true;
                                break;
                            case "cblRun":
                                this.cblRun = StaticParser.ParseBooleanFromString(StaticHelper.ReadString(xmlReader));
                                this.cblRunSpecified = true;
                                break;
                            case "cblBondQual":
                                this.cblBondQual = StaticParser.ParseBooleanFromString(StaticHelper.ReadString(xmlReader));
                                this.cblBondQualSpecified = true;
                                break;
                            case "cblPres":
                                if (this.cblPres == null)
                                    this.cblPres = new pressureMeasure();
                                this.cblPres.HandleXML(xmlReader.ReadSubtree(), isNew);
                                break;
                            case "tempSurvey":
                                this.tempSurvey = StaticParser.ParseBooleanFromString(StaticHelper.ReadString(xmlReader));
                                this.tempSurveySpecified = true;
                                break;
                            case "eTimCementLog":
                                if (this.eTimCementLog == null)
                                    this.eTimCementLog = new timeMeasure();
                                this.eTimCementLog.HandleXML(xmlReader.ReadSubtree(), isNew);
                                break;
                            case "formPit":
                                if (this.formPit == null)
                                    this.formPit = new forcePerVolumeMeasure();
                                this.formPit.HandleXML(xmlReader.ReadSubtree(), isNew);
                                break;
                            case "toolCompanyPit":
                                this.toolCompanyPit = StaticHelper.ReadString(xmlReader);
                                break;
                            case "eTimPitStart":
                                if (this.eTimPitStart == null)
                                    this.eTimPitStart = new timeMeasure();
                                this.eTimPitStart.HandleXML(xmlReader.ReadSubtree(), isNew);
                                break;
                            case "mdCementTop":
                                if (this.mdCementTop == null)
                                    this.mdCementTop = new measuredDepthCoord();
                                this.mdCementTop.HandleXML(xmlReader.ReadSubtree(), isNew);
                                break;
                            case "topCementMethod":
                                this.topCementMethod = StaticHelper.ReadString(xmlReader);
                                break;
                            case "tocOK":
                                this.tocOK = StaticParser.ParseBooleanFromString(StaticHelper.ReadString(xmlReader));
                                this.tocOKSpecified = true;
                                break;
                            case "jobRating":
                                this.jobRating = StaticHelper.ReadString(xmlReader);
                                break;
                            case "remedialCement":
                                this.remedialCement = StaticParser.ParseBooleanFromString(StaticHelper.ReadString(xmlReader));
                                this.remedialCementSpecified = true;
                                break;
                            case "numRemedial":
                                this.numRemedial = StaticParser.ParseShortFromString(StaticHelper.ReadString(xmlReader));
                                this.numRemedialSpecified = true;
                                break;
                            case "failureMethod":
                                this.failureMethod = StaticHelper.ReadString(xmlReader);
                                break;
                            case "linerTop":
                                if (this.linerTop == null)
                                    this.linerTop = new lengthMeasure();
                                this.linerTop.HandleXML(xmlReader.ReadSubtree(), isNew);
                                break;
                            case "linerLap":
                                if (this.linerLap == null)
                                    this.linerLap = new lengthMeasure();
                                this.linerLap.HandleXML(xmlReader.ReadSubtree(), isNew);
                                break;
                            case "eTimBeforeTest":
                                if (this.eTimBeforeTest == null)
                                    this.eTimBeforeTest = new timeMeasure();
                                this.eTimBeforeTest.HandleXML(xmlReader.ReadSubtree(), isNew);
                                break;
                            case "testNegativeTool":
                                this.testNegativeTool = StaticHelper.ReadString(xmlReader);
                                break;
                            case "testNegativeEmw":
                                if (this.testNegativeEmw == null)
                                    this.testNegativeEmw = new densityMeasure();
                                this.testNegativeEmw.HandleXML(xmlReader.ReadSubtree(), isNew);
                                break;
                            case "testPositiveTool":
                                this.testPositiveTool = StaticHelper.ReadString(xmlReader);
                                break;
                            case "testPositiveEmw":
                                if (this.testPositiveEmw == null)
                                    this.testPositiveEmw = new densityMeasure();
                                this.testPositiveEmw.HandleXML(xmlReader.ReadSubtree(), isNew);
                                break;
                            case "cementFoundOnTool":
                                this.cementFoundOnTool = StaticParser.ParseBooleanFromString(StaticHelper.ReadString(xmlReader));
                                this.cementFoundOnToolSpecified = true;
                                break;
                            case "mdDVTool":
                                if (this.mdDVTool == null)
                                    this.mdDVTool = new measuredDepthCoord();
                                this.mdDVTool.HandleXML(xmlReader.ReadSubtree(), isNew);
                                break;
                            default:
                                break;
                        }
                    }
                }
            }
        }
    }
}
