﻿ 

using System;
using System.Collections.Generic;
using System.IO;
using System.Text;
using System.Xml;
using Witsml.Common.Exceptions;
using Witsml.Exceptions;


namespace Witsml.Objects1311
{
    [System.Xml.Serialization.XmlRootAttribute("cementJob", Namespace = "http://www.witsml.org/schemas/131", IsNullable = false)]
    public partial class obj_cementJob : WITSMLObject, IHandleXML
    {
        public obj_cementJob()
        {
            this.jobType = CementJobType.unknown;
        }

        /// <summary>
        /// Decode XML string into obj_cementJob
        /// </summary>
        /// <param name="xmlIn">XML string</param>
        public override void Decode(ref string xmlIn)
        {
            try
            {
                //Consume XML string and convert node value to variable based on node name
                using (StringReader stringReader = new StringReader(xmlIn))
                {
                    XmlReaderSettings setting = new XmlReaderSettings();
                    setting.ConformanceLevel = ConformanceLevel.Document;
                    setting.IgnoreComments = true;
                    setting.IgnoreWhitespace = true;
                    setting.IgnoreProcessingInstructions = true;
                    using (XmlReader xmlReader = XmlReader.Create(stringReader, setting))
                    {
                        //determine add or update operation
                        bool isNew;
                        if (this.uid == string.Empty)
                            isNew = true;
                        else isNew = false;

                        this.HandleXML(xmlReader, isNew);
                    }
                }
            }
            catch (XmlException ex)
            {
                throw new Exceptions.UnableToDecodeException(ex.Message, WITSMLReturnCode.WITSMLDocumentError, ex.InnerException);
            }
            catch (NullReferenceException ex)
            {
                throw new Exceptions.UnableToDecodeException(ex.Message, WITSMLReturnCode.WITSMLEmptyDocument, ex.InnerException);
            }
            catch (Exceptions.UnableToDecodeException ex)
            {
                throw ex;
            }
            catch (Exception ex)
            {
                throw new Exceptions.UnableToDecodeException(ex.Message, WITSMLReturnCode.WITSMLUnkownCause, ex.InnerException);
            }
        }

        /// <summary>
        /// Convert XML string data into obj_cementJob fields value
        /// </summary>
        /// <param name="xmlReader">reader that has obj_cementJob node</param>
        /// <param name="IsNew">Is the object from stratch(true) or from db/serialization(false)</param>
        public void HandleXML(XmlReader xmlReader, bool isNew)
        {
            //condition for add update collection
            bool isCementStageNew = this.cementStage == null;

            //temporary list for data manipulations
            List<cs_cementStage> listCementStage = isCementStageNew ? new List<cs_cementStage>() : new List<cs_cementStage>(this.cementStage);

            using (xmlReader)
            {
                //assign uid for the new well
                xmlReader.MoveToContent();
                if (xmlReader.HasAttributes)
                {
                    if (xmlReader.MoveToAttribute("uidWell"))
                        StaticHelper.HandleUID(xmlReader.Value, "cementJob uidWell", out this.uidWell);
                    if (xmlReader.MoveToAttribute("uidWellbore"))
                        StaticHelper.HandleUID(xmlReader.Value, "cementJob uidWellbore", out this.uidWellbore);
                    if (xmlReader.MoveToAttribute("uid"))
                        StaticHelper.HandleUID(xmlReader.Value, "cementJob uid", out this.uid);
                    xmlReader.MoveToElement();
                }
                while (xmlReader.Read())
                {
                    //only take child node
                    if (xmlReader.Depth == 1 && xmlReader.IsStartElement() && !xmlReader.IsEmptyElement)
                    {
                        switch (xmlReader.Name)
                        {
                            case "nameWell":
                                this.nameWell = StaticHelper.ReadString(xmlReader);
                                break;
                            case "nameWellbore":
                                this.nameWellbore = StaticHelper.ReadString(xmlReader);
                                break;
                            case "name":
                                this.name = StaticHelper.ReadString(xmlReader);
                                break;
                            case "jobType":
                                StaticParser.SetEnumFromString<CementJobType>(StaticHelper.ReadString(xmlReader), out this.jobType, out this.jobTypeSpecified, CementJobType.unknown);
                                break;
                            case "jobConfig":
                                this.jobConfig = StaticHelper.ReadString(xmlReader);
                                break;
                            case "dTimJob":
                                this.dTimJob = StaticParser.ParseDateTimeFromString(StaticHelper.ReadString(xmlReader));
                                this.dTimJobSpecified = true;
                                break;
                            case "nameCementedString":
                                this.nameCementedString = StaticHelper.ReadString(xmlReader);
                                break;
                            case "holeConfig":
                                if (this.holeConfig == null)
                                    this.holeConfig = new cs_wbGeometry();
                                this.holeConfig.HandleXML(xmlReader.ReadSubtree(), isNew);
                                break;
                            case "nameWorkString":
                                this.nameWorkString = StaticHelper.ReadString(xmlReader);
                                break;
                            case "contractor":
                                this.contractor = StaticHelper.ReadString(xmlReader);
                                break;
                            case "cementEngr":
                                this.cementEngr = StaticHelper.ReadString(xmlReader);
                                break;
                            case "offshoreJob":
                                this.offshoreJob = StaticParser.ParseBooleanFromString(StaticHelper.ReadString(xmlReader));
                                this.offshoreJobSpecified = true;
                                break;
                            case "mdWater":
                                if (this.mdWater == null)
                                    this.mdWater = new measuredDepthCoord();
                                this.mdWater.HandleXML(xmlReader.ReadSubtree(), isNew);
                                break;
                            case "returnsToSeabed":
                                this.returnsToSeabed = StaticParser.ParseBooleanFromString(StaticHelper.ReadString(xmlReader));
                                this.returnsToSeabedSpecified = true;
                                break;
                            case "reciprocating":
                                this.reciprocating = StaticParser.ParseBooleanFromString(StaticHelper.ReadString(xmlReader));
                                this.reciprocatingSpecified = true;
                                break;
                            case "woc":
                                if (this.woc == null)
                                    this.woc = new timeMeasure();
                                this.woc.HandleXML(xmlReader.ReadSubtree(), isNew);
                                break;
                            case "mdPlugTop":
                                if (this.mdPlugTop == null)
                                    this.mdPlugTop = new measuredDepthCoord();
                                this.mdPlugTop.HandleXML(xmlReader.ReadSubtree(), isNew);
                                break;
                            case "mdPlugBot":
                                if (this.mdPlugBot == null)
                                    this.mdPlugBot = new measuredDepthCoord();
                                this.mdPlugBot.HandleXML(xmlReader.ReadSubtree(), isNew);
                                break;
                            case "mdHole":
                                if (this.mdHole == null)
                                    this.mdHole = new measuredDepthCoord();
                                this.mdHole.HandleXML(xmlReader.ReadSubtree(), isNew);
                                break;
                            case "mdShoe":
                                if (this.mdShoe == null)
                                    this.mdShoe = new measuredDepthCoord();
                                this.mdShoe.HandleXML(xmlReader.ReadSubtree(), isNew);
                                break;
                            case "tvdShoe":
                                if (this.tvdShoe == null)
                                    this.tvdShoe = new wellVerticalDepthCoord();
                                this.tvdShoe.HandleXML(xmlReader.ReadSubtree(), isNew);
                                break;
                            case "mdStringSet":
                                if (this.mdStringSet == null)
                                    this.mdStringSet = new measuredDepthCoord();
                                this.mdStringSet.HandleXML(xmlReader.ReadSubtree(), isNew);
                                break;
                            case "tvdStringSet":
                                if (this.tvdStringSet == null)
                                    this.tvdStringSet = new wellVerticalDepthCoord();
                                this.tvdStringSet.HandleXML(xmlReader.ReadSubtree(), isNew);
                                break;
                            case "cementStage":
                                //collection
                                StaticHelper.AddUpdateWithUid<cs_cementStage>(xmlReader, isCementStageNew, ref listCementStage);
                                break;
                            case "cementTest":
                                if (this.cementTest == null)
                                    this.cementTest = new cs_cementTest();
                                this.cementTest.HandleXML(xmlReader.ReadSubtree(), isNew);
                                break;
                            case "typePlug":
                                this.typePlug = StaticHelper.ReadString(xmlReader);
                                break;
                            case "nameCementString":
                                this.nameCementString = StaticHelper.ReadString(xmlReader);
                                break;
                            case "dTimPlugSet":
                                this.dTimPlugSet = StaticParser.ParseDateTimeFromString(StaticHelper.ReadString(xmlReader));
                                this.dTimPlugSetSpecified = true;
                                break;
                            case "cementDrillOut":
                                this.cementDrillOut = StaticParser.ParseBooleanFromString(StaticHelper.ReadString(xmlReader));
                                this.cementDrillOutSpecified = true;
                                break;
                            case "dTimCementDrillOut":
                                this.dTimCementDrillOut = StaticParser.ParseDateTimeFromString(StaticHelper.ReadString(xmlReader));
                                this.dTimCementDrillOutSpecified = true;
                                break;
                            case "typeSqueeze":
                                this.typeSqueeze = StaticHelper.ReadString(xmlReader);
                                break;
                            case "mdSqueeze":
                                if (this.mdSqueeze == null)
                                    this.mdSqueeze = new measuredDepthCoord();
                                this.mdSqueeze.HandleXML(xmlReader.ReadSubtree(), isNew);
                                break;
                            case "dTimSqueeze":
                                this.dTimSqueeze = StaticParser.ParseDateTimeFromString(StaticHelper.ReadString(xmlReader));
                                this.dTimSqueezeSpecified = true;
                                break;
                            case "toolCompany":
                                this.toolCompany = StaticHelper.ReadString(xmlReader);
                                break;
                            case "typeTool":
                                this.typeTool = StaticHelper.ReadString(xmlReader);
                                break;
                            case "dTimPipeRotStart":
                                this.dTimPipeRotStart = StaticParser.ParseDateTimeFromString(StaticHelper.ReadString(xmlReader));
                                this.dTimPipeRotStartSpecified = true;
                                break;
                            case "dTimPipeRotEnd":
                                this.dTimPipeRotEnd = StaticParser.ParseDateTimeFromString(StaticHelper.ReadString(xmlReader));
                                this.dTimPipeRotEndSpecified = true;
                                break;
                            case "rpmPipe":
                                if (this.rpmPipe == null)
                                    this.rpmPipe = new anglePerTimeMeasure();
                                this.rpmPipe.HandleXML(xmlReader.ReadSubtree(), isNew);
                                break;
                            case "tqInitPipeRot":
                                if (this.tqInitPipeRot == null)
                                    this.tqInitPipeRot = new momentOfForceMeasure();
                                this.tqInitPipeRot.HandleXML(xmlReader.ReadSubtree(), isNew);
                                break;
                            case "tqPipeAv":
                                if (this.tqPipeAv == null)
                                    this.tqPipeAv = new momentOfForceMeasure();
                                this.tqPipeAv.HandleXML(xmlReader.ReadSubtree(), isNew);
                                break;
                            case "tqPipeMx":
                                if (this.tqPipeMx == null)
                                    this.tqPipeMx = new momentOfForceMeasure();
                                this.tqPipeMx.HandleXML(xmlReader.ReadSubtree(), isNew);
                                break;
                            case "dTimRecipStart":
                                this.dTimRecipStart = StaticParser.ParseDateTimeFromString(StaticHelper.ReadString(xmlReader));
                                this.dTimRecipStartSpecified = true;
                                break;
                            case "dTimRecipEnd":
                                this.dTimRecipEnd = StaticParser.ParseDateTimeFromString(StaticHelper.ReadString(xmlReader));
                                this.dTimRecipEndSpecified = true;
                                break;
                            case "overPull":
                                if (this.overPull == null)
                                    this.overPull = new forceMeasure();
                                this.overPull.HandleXML(xmlReader.ReadSubtree(), isNew);
                                break;
                            case "slackOff":
                                if (this.slackOff == null)
                                    this.slackOff = new forceMeasure();
                                this.slackOff.HandleXML(xmlReader.ReadSubtree(), isNew);
                                break;
                            case "rpmPipeRecip":
                                if (this.rpmPipeRecip == null)
                                    this.rpmPipeRecip = new anglePerTimeMeasure();
                                this.rpmPipeRecip.HandleXML(xmlReader.ReadSubtree(), isNew);
                                break;
                            case "lenPipeRecipStroke":
                                if (this.lenPipeRecipStroke == null)
                                    this.lenPipeRecipStroke = new lengthMeasure();
                                this.lenPipeRecipStroke.HandleXML(xmlReader.ReadSubtree(), isNew);
                                break;
                            case "coilTubing":
                                this.coilTubing = StaticParser.ParseBooleanFromString(StaticHelper.ReadString(xmlReader));
                                this.coilTubingSpecified = true;
                                break;
                            case "commonData":
                                if (this.commonData == null)
                                    this.commonData = new cs_commonData();
                                this.commonData.HandleXML(xmlReader.ReadSubtree(), isNew);
                                break;
                            case "customData":
                                if (this.customData == null)
                                    this.customData = new cs_customData();
                                this.customData.HandleXML(xmlReader.ReadSubtree(), isNew);
                                break;
                            default:
                                break;
                        }
                    }
                }
                //assign list to collection
                this.cementStage = listCementStage.ToArray();
            }
        }

        public override string ValidateGetPath(string newUid)
        {
            // Fix uid if the Object dosn't have one.
            if (!IsValidUid(this.uid))
            {
                this.uid = newUid;
                // TODO: Add some loging that we created a new UID for the object maybe.
            }

            StringBuilder path = new StringBuilder(this.GetParentPath());
            path.Append(@"\");
            path.Append(this.uid);
            return path.ToString();
        }

        public override string GetParentPath()
        {
            if (!IsValidUid(this.uidWell) || !IsValidUid(this.uidWellbore))
            {
                throw new UnableToDecodeException("One or more Parent UID's are invalid", WITSMLReturnCode.WITSMLInvalidParentUID);
                //TODO: Log the issue
            }

            StringBuilder path = new StringBuilder(@"\");
            path.Append(this.uidWell);
            path.Append(@"\");
            path.Append(this.uidWellbore);
            return path.ToString();
        }

        // NOTE: these are done with functions, instead of property access, so as not to mess around with the auto-generated / serialized classes.
        public override string Get_uid()
        {
            return this.uid;
        }

        public override string Get_uidWell()
        {
            return this.uidWell;
        }

        public override string Get_uidWellbore()
        {
            return this.uidWellbore;
        }

        public override string Get_name()
        {
            return this.name;
        }

        public override string Get_nameWell()
        {
            return this.nameWell;
        }

        public override string Get_nameWellbore()
        {
            return this.nameWellbore;
        }

        public override ICommonData Get_commonData()
        {
            if (this.commonData == null)
            {
                this.commonData = new cs_commonData();
            }
            return (ICommonData)this.commonData;
        }
        public override ICustomData Get_customData()
        {
            if (this.customData == null)
            {
                this.customData = new cs_customData();
            }
            return (ICustomData)this.customData;
        }
    }
}
