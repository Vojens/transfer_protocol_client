﻿ 

using System;
using System.Collections.Generic;
using System.Text;
using System.Xml;

namespace Witsml.Objects1311
{
    public partial class cs_cementPumpSchedule : IHandleXML
    {
        public cs_cementPumpSchedule()
        {
        }
        /// <summary>
        /// Convert XML string data into cs_cementPumpSchedule fields value
        /// </summary>
        /// <param name="xmlReader">reader that has cs_cementPumpSchedule node</param>
        /// <param name="IsNew">Is the object from stratch(true) or from db/serialization(false)</param>
        public void HandleXML(XmlReader xmlReader, bool isNew)
        {
            using (xmlReader)
            {
                while (xmlReader.Read())
                {
                    //start on depth 1 which are the childs that we want to check against
                    //check for starting tag and convert node value to its expected variable
                    if (xmlReader.Depth == 1 && xmlReader.IsStartElement() && !xmlReader.IsEmptyElement)
                    {
                        switch (xmlReader.Name)
                        {
                            case "eTimPump":
                                if (this.eTimPump == null)
                                    this.eTimPump = new timeMeasure();
                                this.eTimPump.HandleXML(xmlReader.ReadSubtree(), isNew);
                                break;
                            case "ratePump":
                                if (this.ratePump == null)
                                    this.ratePump = new volumeFlowRateMeasure();
                                this.ratePump.HandleXML(xmlReader.ReadSubtree(), isNew);
                                break;
                            case "volPump":
                                if (this.volPump == null)
                                    this.volPump = new volumeMeasure();
                                this.volPump.HandleXML(xmlReader.ReadSubtree(), isNew);
                                break;
                            case "strokePump":
                                this.strokePump = StaticParser.ParseShortFromString(StaticHelper.ReadString(xmlReader));
                                this.strokePumpSpecified = true;
                                break;
                            case "presBack":
                                if (this.presBack == null)
                                    this.presBack = new pressureMeasure();
                                this.presBack.HandleXML(xmlReader.ReadSubtree(), isNew);
                                break;
                            case "eTimShutdown":
                                if (this.eTimShutdown == null)
                                    this.eTimShutdown = new timeMeasure();
                                this.eTimShutdown.HandleXML(xmlReader.ReadSubtree(), isNew);
                                break;
                            case "comments":
                                this.comments = StaticHelper.ReadString(xmlReader);
                                break;
                            default:
                                break;
                        }
                    }
                }
            }
        }
    }
}
