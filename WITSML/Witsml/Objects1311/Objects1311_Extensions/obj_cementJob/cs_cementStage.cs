﻿ 

using System;
using System.Collections.Generic;
using System.Text;
using System.Xml;

namespace Witsml.Objects1311
{
    public partial class cs_cementStage : IHandleXML, IHasID
    {
        //interface for generic helper
        [System.Xml.Serialization.XmlIgnore]
        public string UID
        {
            get { return this.uid; }
        }
        public cs_cementStage()
        {
        }
        /// <summary>
        /// Convert XML string data into cs_cementStage fields value
        /// </summary>
        /// <param name="xmlReader">reader that has cs_cementStage node</param>
        /// <param name="IsNew">Is the object from stratch(true) or from db/serialization(false)</param>
        public void HandleXML(XmlReader xmlReader, bool isNew)
        {
            using (xmlReader)
            {
                xmlReader.MoveToContent();
                if (xmlReader.HasAttributes)
                {
                    if (xmlReader.MoveToAttribute("uid"))
                        StaticHelper.HandleUID(xmlReader.Value, "cementStage uid", out this.uid);
                    xmlReader.MoveToElement();
                }
                while (xmlReader.Read())
                {
                    //start on depth 1 which are the childs that we want to check against
                    //check for starting tag and convert node value to its expected variable
                    if (xmlReader.Depth == 1 && xmlReader.IsStartElement() && !xmlReader.IsEmptyElement)
                    {
                        switch (xmlReader.Name)
                        {
                            case "numStage":
                                this.numStage = StaticParser.ParseShortFromString(StaticHelper.ReadString(xmlReader));
                                break;
                            case "typeStage":
                                this.typeStage = StaticHelper.ReadString(xmlReader);
                                break;
                            case "dTimMixStart":
                                this.dTimMixStart = StaticParser.ParseDateTimeFromString(StaticHelper.ReadString(xmlReader));
                                this.dTimMixStartSpecified = true;
                                break;
                            case "dTimPumpStart":
                                this.dTimPumpStart = StaticParser.ParseDateTimeFromString(StaticHelper.ReadString(xmlReader));
                                this.dTimPumpStartSpecified = true;
                                break;
                            case "dTimPumpEnd":
                                this.dTimPumpEnd = StaticParser.ParseDateTimeFromString(StaticHelper.ReadString(xmlReader));
                                this.dTimPumpEndSpecified = true;
                                break;
                            case "dTimDisplaceStart":
                                this.dTimDisplaceStart = StaticParser.ParseDateTimeFromString(StaticHelper.ReadString(xmlReader));
                                this.dTimDisplaceStartSpecified = true;
                                break;
                            case "mdTop":
                                if (this.mdTop == null)
                                    this.mdTop = new measuredDepthCoord();
                                this.mdTop.HandleXML(xmlReader.ReadSubtree(), isNew);
                                break;
                            case "mdBottom":
                                if (this.mdBottom == null)
                                    this.mdBottom = new measuredDepthCoord();
                                this.mdBottom.HandleXML(xmlReader.ReadSubtree(), isNew);
                                break;
                            case "volExcess":
                                if (this.volExcess == null)
                                    this.volExcess = new volumeMeasure();
                                this.volExcess.HandleXML(xmlReader.ReadSubtree(), isNew);
                                break;
                            case "flowrateDisplaceAv":
                                if (this.flowrateDisplaceAv == null)
                                    this.flowrateDisplaceAv = new volumeFlowRateMeasure();
                                this.flowrateDisplaceAv.HandleXML(xmlReader.ReadSubtree(), isNew);
                                break;
                            case "flowrateDisplaceMx":
                                if (this.flowrateDisplaceMx == null)
                                    this.flowrateDisplaceMx = new volumeFlowRateMeasure();
                                this.flowrateDisplaceMx.HandleXML(xmlReader.ReadSubtree(), isNew);
                                break;
                            case "presDisplace":
                                if (this.presDisplace == null)
                                    this.presDisplace = new pressureMeasure();
                                this.presDisplace.HandleXML(xmlReader.ReadSubtree(), isNew);
                                break;
                            case "volReturns":
                                if (this.volReturns == null)
                                    this.volReturns = new volumeMeasure();
                                this.volReturns.HandleXML(xmlReader.ReadSubtree(), isNew);
                                break;
                            case "eTimMudCirculation":
                                if (this.eTimMudCirculation == null)
                                    this.eTimMudCirculation = new timeMeasure();
                                this.eTimMudCirculation.HandleXML(xmlReader.ReadSubtree(), isNew);
                                break;
                            case "flowrateMudCirc":
                                if (this.flowrateMudCirc == null)
                                    this.flowrateMudCirc = new volumeFlowRateMeasure();
                                this.flowrateMudCirc.HandleXML(xmlReader.ReadSubtree(), isNew);
                                break;
                            case "presMudCirc":
                                if (this.presMudCirc == null)
                                    this.presMudCirc = new pressureMeasure();
                                this.presMudCirc.HandleXML(xmlReader.ReadSubtree(), isNew);
                                break;
                            case "flowrateEnd":
                                if (this.flowrateEnd == null)
                                    this.flowrateEnd = new volumeFlowRateMeasure();
                                this.flowrateEnd.HandleXML(xmlReader.ReadSubtree(), isNew);
                                break;
                            case "cementingFluid":
                                if (this.cementingFluid == null)
                                    this.cementingFluid = new cs_cementingFluid();
                                this.cementingFluid.HandleXML(xmlReader.ReadSubtree(), isNew);
                                break;
                            case "afterFlowAnn":
                                this.afterFlowAnn = StaticParser.ParseBooleanFromString(StaticHelper.ReadString(xmlReader));
                                this.afterFlowAnnSpecified = true;
                                break;
                            case "squeezeObj":
                                this.squeezeObj = StaticHelper.ReadString(xmlReader);
                                break;
                            case "squeezeObtained":
                                this.squeezeObtained = StaticParser.ParseBooleanFromString(StaticHelper.ReadString(xmlReader));
                                this.squeezeObtainedSpecified = true;
                                break;
                            case "mdString":
                                if (this.mdString == null)
                                    this.mdString = new measuredDepthCoord();
                                this.mdString.HandleXML(xmlReader.ReadSubtree(), isNew);
                                break;
                            case "mdTool":
                                if (this.mdTool == null)
                                    this.mdTool = new measuredDepthCoord();
                                this.mdTool.HandleXML(xmlReader.ReadSubtree(), isNew);
                                break;
                            case "mdCoilTbg":
                                if (this.mdCoilTbg == null)
                                    this.mdCoilTbg = new measuredDepthCoord();
                                this.mdCoilTbg.HandleXML(xmlReader.ReadSubtree(), isNew);
                                break;
                            case "volCsgIn":
                                if (this.volCsgIn == null)
                                    this.volCsgIn = new volumeMeasure();
                                this.volCsgIn.HandleXML(xmlReader.ReadSubtree(), isNew);
                                break;
                            case "volCsgOut":
                                if (this.volCsgOut == null)
                                    this.volCsgOut = new volumeMeasure();
                                this.volCsgOut.HandleXML(xmlReader.ReadSubtree(), isNew);
                                break;
                            case "tailPipeUsed":
                                this.tailPipeUsed= StaticParser.ParseBooleanFromString(StaticHelper.ReadString(xmlReader));
                                this.tailPipeUsedSpecified = true;
                                break;
                            case "diaTailPipe":
                                if (this.diaTailPipe == null)
                                    this.diaTailPipe = new lengthMeasure();
                                this.diaTailPipe.HandleXML(xmlReader.ReadSubtree(), isNew);
                                break;
                            case "tailPipePerf":
                                this.tailPipePerf = StaticParser.ParseBooleanFromString(StaticHelper.ReadString(xmlReader));
                                this.tailPipePerfSpecified = true;
                                break;
                            case "presTbgStart":
                                if (this.presTbgStart == null)
                                    this.presTbgStart = new pressureMeasure();
                                this.presTbgStart.HandleXML(xmlReader.ReadSubtree(), isNew);
                                break;
                            case "presTbgEnd":
                                if (this.presTbgEnd == null)
                                    this.presTbgEnd = new pressureMeasure();
                                this.presTbgEnd.HandleXML(xmlReader.ReadSubtree(), isNew);
                                break;
                            case "presCsgStart":
                                if (this.presCsgStart == null)
                                    this.presCsgStart = new pressureMeasure();
                                this.presCsgStart.HandleXML(xmlReader.ReadSubtree(), isNew);
                                break;
                            case "presCsgEnd":
                                if (this.presCsgEnd == null)
                                    this.presCsgEnd = new pressureMeasure();
                                this.presCsgEnd.HandleXML(xmlReader.ReadSubtree(), isNew);
                                break;
                            case "presBackPressure":
                                if (this.presBackPressure == null)
                                    this.presBackPressure = new pressureMeasure();
                                this.presBackPressure.HandleXML(xmlReader.ReadSubtree(), isNew);
                                break;
                            case "presCoilTbgStart":
                                if (this.presCoilTbgStart == null)
                                    this.presCoilTbgStart = new pressureMeasure();
                                this.presCoilTbgStart.HandleXML(xmlReader.ReadSubtree(), isNew);
                                break;
                            case "presCoilTbgEnd":
                                if (this.presCoilTbgEnd == null)
                                    this.presCoilTbgEnd = new pressureMeasure();
                                this.presCoilTbgEnd.HandleXML(xmlReader.ReadSubtree(), isNew);
                                break;
                            case "presBreakDown":
                                if (this.presBreakDown == null)
                                    this.presBreakDown = new pressureMeasure();
                                this.presBreakDown.HandleXML(xmlReader.ReadSubtree(), isNew);
                                break;
                            case "flowrateBreakDown":
                                if (this.flowrateBreakDown == null)
                                    this.flowrateBreakDown = new volumeFlowRateMeasure();
                                this.flowrateBreakDown.HandleXML(xmlReader.ReadSubtree(), isNew);
                                break;
                            case "presSqueezeAv":
                                if (this.presSqueezeAv == null)
                                    this.presSqueezeAv = new pressureMeasure();
                                this.presSqueezeAv.HandleXML(xmlReader.ReadSubtree(), isNew);
                                break;
                            case "presSqueezeEnd":
                                if (this.presSqueezeEnd == null)
                                    this.presSqueezeEnd = new pressureMeasure();
                                this.presSqueezeEnd.HandleXML(xmlReader.ReadSubtree(), isNew);
                                break;
                            case "presSqueezeHeld":
                                this.presSqueezeHeld = StaticParser.ParseBooleanFromString(StaticHelper.ReadString(xmlReader));
                                this.presSqueezeHeldSpecified = true;
                                break;
                            case "presSqueeze":
                                if (this.presSqueeze == null)
                                    this.presSqueeze = new pressureMeasure();
                                this.presSqueeze.HandleXML(xmlReader.ReadSubtree(), isNew);
                                break;
                            case "eTimPresHeld":
                                if (this.eTimPresHeld == null)
                                    this.eTimPresHeld = new timeMeasure();
                                this.eTimPresHeld.HandleXML(xmlReader.ReadSubtree(), isNew);
                                break;
                            case "flowrateSqueezeAv":
                                if (this.flowrateSqueezeAv == null)
                                    this.flowrateSqueezeAv = new volumeFlowRateMeasure();
                                this.flowrateSqueezeAv.HandleXML(xmlReader.ReadSubtree(), isNew);
                                break;
                            case "flowrateSqueezeMx":
                                if (this.flowrateSqueezeMx == null)
                                    this.flowrateSqueezeMx = new volumeFlowRateMeasure();
                                this.flowrateSqueezeMx.HandleXML(xmlReader.ReadSubtree(), isNew);
                                break;
                            case "flowratePumpStart":
                                if (this.flowratePumpStart == null)
                                    this.flowratePumpStart = new volumeFlowRateMeasure();
                                this.flowratePumpStart.HandleXML(xmlReader.ReadSubtree(), isNew);
                                break;
                            case "flowratePumpEnd":
                                if (this.flowratePumpEnd == null)
                                    this.flowratePumpEnd = new volumeFlowRateMeasure();
                                this.flowratePumpEnd.HandleXML(xmlReader.ReadSubtree(), isNew);
                                break;
                            case "pillBelowPlug":
                                this.pillBelowPlug = StaticParser.ParseBooleanFromString(StaticHelper.ReadString(xmlReader));
                                this.pillBelowPlugSpecified = true;
                                break;
                            case "plugCatcher":
                                this.plugCatcher = StaticParser.ParseBooleanFromString(StaticHelper.ReadString(xmlReader));
                                this.plugCatcherSpecified = true;
                                break;
                            case "mdCircOut":
                                if (this.mdCircOut == null)
                                    this.mdCircOut = new measuredDepthCoord();
                                this.mdCircOut.HandleXML(xmlReader.ReadSubtree(), isNew);
                                break;
                            case "volCircPrior":
                                if (this.volCircPrior == null)
                                    this.volCircPrior = new volumeMeasure();
                                this.volCircPrior.HandleXML(xmlReader.ReadSubtree(), isNew);
                                break;
                            case "typeOriginalMud":
                                this.typeOriginalMud = StaticHelper.ReadString(xmlReader);
                                break;
                            case "wtMud":
                                if (this.wtMud == null)
                                    this.wtMud = new densityMeasure();
                                this.wtMud.HandleXML(xmlReader.ReadSubtree(), isNew);
                                break;
                            case "visFunnelMud":
                                if (this.visFunnelMud == null)
                                    this.visFunnelMud = new timeMeasure();
                                this.visFunnelMud.HandleXML(xmlReader.ReadSubtree(), isNew);
                                break;
                            case "pvMud":
                                if (this.pvMud == null)
                                    this.pvMud = new dynamicViscosityMeasure();
                                this.pvMud.HandleXML(xmlReader.ReadSubtree(), isNew);
                                break;
                            case "ypMud":
                                if (this.ypMud == null)
                                    this.ypMud = new pressureMeasure();
                                this.ypMud.HandleXML(xmlReader.ReadSubtree(), isNew);
                                break;
                            case "gel10Sec":
                                if (this.gel10Sec == null)
                                    this.gel10Sec = new pressureMeasure();
                                this.gel10Sec.HandleXML(xmlReader.ReadSubtree(), isNew);
                                break;
                            case "gel10Min":
                                if (this.gel10Min == null)
                                    this.gel10Min = new pressureMeasure();
                                this.gel10Min.HandleXML(xmlReader.ReadSubtree(), isNew);
                                break;
                            case "tempBHCT":
                                if (this.tempBHCT == null)
                                    this.tempBHCT = new thermodynamicTemperatureMeasure();
                                this.tempBHCT.HandleXML(xmlReader.ReadSubtree(), isNew);
                                break;
                            case "tempBHST":
                                if (this.tempBHST == null)
                                    this.tempBHST = new thermodynamicTemperatureMeasure();
                                this.tempBHST.HandleXML(xmlReader.ReadSubtree(), isNew);
                                break;
                            case "volExcessMethod":
                                this.volExcessMethod = StaticHelper.ReadString(xmlReader);
                                break;
                            case "mixMethod":
                                this.mixMethod = StaticHelper.ReadString(xmlReader);
                                break;
                            case "densMeasBy":
                                this.densMeasBy = StaticHelper.ReadString(xmlReader);
                                break;
                            case "annFlowAfter":
                                this.annFlowAfter = StaticParser.ParseBooleanFromString(StaticHelper.ReadString(xmlReader));
                                this.annFlowAfterSpecified = true;
                                break;
                            case "topPlug":
                                this.topPlug = StaticParser.ParseBooleanFromString(StaticHelper.ReadString(xmlReader));
                                this.topPlugSpecified = true;
                                break;
                            case "botPlug":
                                this.botPlug = StaticParser.ParseBooleanFromString(StaticHelper.ReadString(xmlReader));
                                this.botPlugSpecified = true;
                                break;
                            case "botPlugNumber":
                                this.botPlugNumber = StaticParser.ParseShortFromString(StaticHelper.ReadString(xmlReader));
                                this.botPlugNumberSpecified = true;
                                break;
                            case "plugBumped":
                                this.plugBumped = StaticParser.ParseBooleanFromString(StaticHelper.ReadString(xmlReader));
                                this.plugBumpedSpecified = true;
                                break;
                            case "presPriorBump":
                                if (this.presPriorBump == null)
                                    this.presPriorBump = new pressureMeasure();
                                this.presPriorBump.HandleXML(xmlReader.ReadSubtree(), isNew);
                                break;
                            case "presBump":
                                if (this.presBump == null)
                                    this.presBump = new pressureMeasure();
                                this.presBump.HandleXML(xmlReader.ReadSubtree(), isNew);
                                break;
                            case "presHeld":
                                if (this.presHeld == null)
                                    this.presHeld = new pressureMeasure();
                                this.presHeld.HandleXML(xmlReader.ReadSubtree(), isNew);
                                break;
                            case "floatHeld":
                                this.floatHeld = StaticParser.ParseBooleanFromString(StaticHelper.ReadString(xmlReader));
                                this.floatHeldSpecified = true;
                                break;
                            case "volMudLost":
                                if (this.volMudLost == null)
                                    this.volMudLost = new volumeMeasure();
                                this.volMudLost.HandleXML(xmlReader.ReadSubtree(), isNew);
                                break;
                            case "fluidDisplace":
                                this.fluidDisplace = StaticHelper.ReadString(xmlReader);
                                break;
                            case "densDisplaceFluid":
                                if (this.densDisplaceFluid == null)
                                    this.densDisplaceFluid = new densityMeasure();
                                this.densDisplaceFluid.HandleXML(xmlReader.ReadSubtree(), isNew);
                                break;
                            case "volDisplaceFluid":
                                if (this.volDisplaceFluid == null)
                                    this.volDisplaceFluid = new volumeMeasure();
                                this.volDisplaceFluid.HandleXML(xmlReader.ReadSubtree(), isNew);
                                break;
                            default:
                                break;
                        }
                    }
                }
            }
        }
    }
}
