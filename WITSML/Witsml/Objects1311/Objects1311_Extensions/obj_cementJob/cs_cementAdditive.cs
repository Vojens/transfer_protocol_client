﻿ 

using System;
using System.Collections.Generic;
using System.Text;
using System.Xml;

namespace Witsml.Objects1311
{
    public partial class cs_cementAdditive : IHandleXML, IHasID
    {
        //interface for generic helper
        [System.Xml.Serialization.XmlIgnore]
        public string UID
        {
            get { return this.uid; }
        }
        public cs_cementAdditive()
        {
        }
        /// <summary>
        /// Convert XML string data into cs_cementAdditive fields value
        /// </summary>
        /// <param name="xmlReader">reader that has cs_cementAdditive node</param>
        /// <param name="IsNew">Is the object from stratch(true) or from db/serialization(false)</param>
        public void HandleXML(XmlReader xmlReader, bool isNew)
        {
            //temporary list for data manipulation
            List<object> listItem = this.Items == null ? new List<object>() : new List<object>(this.Items);
            using (xmlReader)
            {
                xmlReader.MoveToContent();
                if (xmlReader.HasAttributes)
                {
                    if (xmlReader.MoveToAttribute("uid"))
                        StaticHelper.HandleUID(xmlReader.Value, "cementAdditive uid", out this.uid);
                    xmlReader.MoveToElement();
                }
                while (xmlReader.Read())
                {
                    //start on depth 1 which are the childs that we want to check against
                    //check for starting tag and convert node value to its expected variable
                    if (xmlReader.Depth == 1 && xmlReader.IsStartElement() && !xmlReader.IsEmptyElement)
                    {
                        switch (xmlReader.Name)
                        {
                            case "nameAdd":
                                this.nameAdd = StaticHelper.ReadString(xmlReader);
                                break;
                            case "typeAdd":
                                this.typeAdd = StaticHelper.ReadString(xmlReader);
                                break;
                            case "formAdd":
                                this.formAdd = StaticHelper.ReadString(xmlReader);
                                break;
                            case "densAdd":
                                if (this.densAdd == null)
                                    this.densAdd = new densityMeasure();
                                this.densAdd.HandleXML(xmlReader.ReadSubtree(), isNew);
                                break;
                            case "concentration":
                                massConcentrationMeasure objMassConc = new massConcentrationMeasure();
                                objMassConc.HandleXML(xmlReader.ReadSubtree(), isNew);
                                listItem.Add(objMassConc);
                                break;
                            case "typeConc":
                                listItem.Add(StaticHelper.ReadString(xmlReader));
                                break;
                            case "volSack":
                                volumeMeasure objVol = new volumeMeasure();
                                objVol.HandleXML(xmlReader.ReadSubtree(), isNew);
                                listItem.Add(objVol);
                                break;
                            case "wtSack":
                                massMeasure objMassMeasure = new massMeasure();
                                objMassMeasure.HandleXML(xmlReader.ReadSubtree(), isNew);
                                listItem.Add(objMassMeasure);
                                break;
                            case "additive":
                                if (this.additive == null)
                                    this.additive = new massMeasure();
                                this.additive.HandleXML(xmlReader.ReadSubtree(), isNew);
                                break;
                            default:
                                break;
                        }
                    }
                }
                //assign list to array
                this.Items = listItem.ToArray();
            }
        }
    }
}
