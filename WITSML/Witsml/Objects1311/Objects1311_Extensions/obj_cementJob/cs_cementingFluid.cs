﻿ 

using System;
using System.Collections.Generic;
using System.Text;
using System.Xml;

namespace Witsml.Objects1311
{
    public partial class cs_cementingFluid : IHandleXML
    {
        public cs_cementingFluid()
        {
        }
        /// <summary>
        /// Convert XML string data into cs_cementingFluid fields value
        /// </summary>
        /// <param name="xmlReader">reader that has cs_cementingFluid node</param>
        /// <param name="IsNew">Is the object from stratch(true) or from db/serialization(false)</param>
        public void HandleXML(XmlReader xmlReader, bool isNew)
        {
            //condition for add update collection
            bool isCementAdditNew = this.cementAdditive == null;

            //temporary list for data manipulations
            List<cs_cementAdditive> listCementAddit = isCementAdditNew ? new List<cs_cementAdditive>() : new List<cs_cementAdditive>(this.cementAdditive);

            using (xmlReader)
            {
                while (xmlReader.Read())
                {
                    //start on depth 1 which are the childs that we want to check against
                    //check for starting tag and convert node value to its expected variable
                    if (xmlReader.Depth == 1 && xmlReader.IsStartElement() && !xmlReader.IsEmptyElement)
                    {
                        switch (xmlReader.Name)
                        {
                            case "typeFluid":
                                this.typeFluid = StaticHelper.ReadString(xmlReader);
                                break;
                            case "fluidIndex":
                                this.fluidIndex = StaticParser.ParseShortFromString(StaticHelper.ReadString(xmlReader));
                                this.fluidIndexSpecified = true;
                                break;
                            case "descFluid":
                                this.descFluid = StaticHelper.ReadString(xmlReader);
                                break;
                            case "purpose":
                                this.purpose = StaticHelper.ReadString(xmlReader);
                                break;
                            case "classSlurryDryBlend":
                                this.classSlurryDryBlend = StaticHelper.ReadString(xmlReader);
                                break;
                            case "mdFluidTop":
                                if (this.mdFluidTop == null)
                                    this.mdFluidTop = new measuredDepthCoord();
                                this.mdFluidTop.HandleXML(xmlReader.ReadSubtree(), isNew);
                                break;
                            case "mdFluidBottom":
                                if (this.mdFluidBottom == null)
                                    this.mdFluidBottom = new measuredDepthCoord();
                                this.mdFluidBottom.HandleXML(xmlReader.ReadSubtree(), isNew);
                                break;
                            case "sourceWater":
                                this.sourceWater = StaticHelper.ReadString(xmlReader);
                                break;
                            case "volWater":
                                if (this.volWater == null)
                                    this.volWater = new volumeMeasure();
                                this.volWater.HandleXML(xmlReader.ReadSubtree(), isNew);
                                break;
                            case "volCement":
                                if (this.volCement == null)
                                    this.volCement = new volumeMeasure();
                                this.volCement.HandleXML(xmlReader.ReadSubtree(), isNew);
                                break;
                            case "ratioMixWater":
                                if (this.ratioMixWater == null)
                                    this.ratioMixWater = new specificVolumeMeasure();
                                this.ratioMixWater.HandleXML(xmlReader.ReadSubtree(), isNew);
                                break;
                            case "volFluid":
                                if (this.volFluid == null)
                                    this.volFluid = new volumeMeasure();
                                this.volFluid.HandleXML(xmlReader.ReadSubtree(), isNew);
                                break;
                            case "cementPumpSchedule":
                                if (this.cementPumpSchedule == null)
                                    this.cementPumpSchedule = new cs_cementPumpSchedule();
                                this.cementPumpSchedule.HandleXML(xmlReader.ReadSubtree(), isNew);
                                break;
                            case "excessPc":
                                if (this.excessPc == null)
                                    this.excessPc = new volumePerVolumeMeasure();
                                this.excessPc.HandleXML(xmlReader.ReadSubtree(), isNew);
                                break;
                            case "volYield":
                                if (this.volYield == null)
                                    this.volYield = new specificVolumeMeasure();
                                this.volYield.HandleXML(xmlReader.ReadSubtree(), isNew);
                                break;
                            case "density":
                                if (this.density == null)
                                    this.density = new densityMeasure();
                                this.density.HandleXML(xmlReader.ReadSubtree(), isNew);
                                break;
                            case "solidVolumeFraction":
                                if (this.solidVolumeFraction == null)
                                    this.solidVolumeFraction = new volumePerVolumeMeasure();
                                this.solidVolumeFraction.HandleXML(xmlReader.ReadSubtree(), isNew);
                                break;
                            case "volPumped":
                                if (this.volPumped == null)
                                    this.volPumped = new volumeMeasure();
                                this.volPumped.HandleXML(xmlReader.ReadSubtree(), isNew);
                                break;
                            case "volOther":
                                if (this.volOther == null)
                                    this.volOther = new volumeMeasure();
                                this.volOther.HandleXML(xmlReader.ReadSubtree(), isNew);
                                break;
                            case "fluidRheologicalModel":
                                this.fluidRheologicalModel = StaticHelper.ReadString(xmlReader);
                                break;
                            case "vis":
                                if (this.vis == null)
                                    this.vis = new dynamicViscosityMeasure();
                                this.vis.HandleXML(xmlReader.ReadSubtree(), isNew);
                                break;
                            case "yp":
                                if (this.yp == null)
                                    this.yp = new pressureMeasure();
                                this.yp.HandleXML(xmlReader.ReadSubtree(), isNew);
                                break;
                            case "n":
                                if (this.n == null)
                                    this.n = new dimensionlessMeasure();
                                this.n.HandleXML(xmlReader.ReadSubtree(), isNew);
                                break;
                            case "k":
                                if (this.k == null)
                                    this.k = new dimensionlessMeasure();
                                this.k.HandleXML(xmlReader.ReadSubtree(), isNew);
                                break;
                            case "gel10SecReading":
                                if (this.gel10SecReading == null)
                                    this.gel10SecReading = new planeAngleMeasure();
                                this.gel10SecReading.HandleXML(xmlReader.ReadSubtree(), isNew);
                                break;
                            case "gel10SecStrength":
                                if (this.gel10SecStrength == null)
                                    this.gel10SecStrength = new pressureMeasure();
                                this.gel10SecStrength.HandleXML(xmlReader.ReadSubtree(), isNew);
                                break;
                            case "gel1MinReading":
                                if (this.gel1MinReading == null)
                                    this.gel1MinReading = new planeAngleMeasure();
                                this.gel1MinReading.HandleXML(xmlReader.ReadSubtree(), isNew);
                                break;
                            case "gel1MinStrength":
                                if (this.gel1MinStrength == null)
                                    this.gel1MinStrength = new pressureMeasure();
                                this.gel1MinStrength.HandleXML(xmlReader.ReadSubtree(), isNew);
                                break;
                            case "gel10MinReading":
                                if (this.gel10MinReading == null)
                                    this.gel10MinReading = new planeAngleMeasure();
                                this.gel10MinReading.HandleXML(xmlReader.ReadSubtree(), isNew);
                                break;
                            case "gel10MinStrength":
                                if (this.gel10MinStrength == null)
                                    this.gel10MinStrength = new pressureMeasure();
                                this.gel10MinStrength.HandleXML(xmlReader.ReadSubtree(), isNew);
                                break;
                            case "typeBaseFluid":
                                this.typeBaseFluid = StaticHelper.ReadString(xmlReader);
                                break;
                            case "densBaseFluid":
                                if (this.densBaseFluid == null)
                                    this.densBaseFluid = new densityMeasure();
                                this.densBaseFluid.HandleXML(xmlReader.ReadSubtree(), isNew);
                                break;
                            case "dryBlendName":
                                this.dryBlendName = StaticHelper.ReadString(xmlReader);
                                break;
                            case "dryBlendDescription":
                                this.dryBlendDescription = StaticHelper.ReadString(xmlReader);
                                break;
                            case "massDryBlend":
                                if (this.massDryBlend == null)
                                    this.massDryBlend = new massMeasure();
                                this.massDryBlend.HandleXML(xmlReader.ReadSubtree(), isNew);
                                break;
                            case "densDryBlend":
                                if (this.densDryBlend == null)
                                    this.densDryBlend = new densityMeasure();
                                this.densDryBlend.HandleXML(xmlReader.ReadSubtree(), isNew);
                                break;
                            case "massSackDryBlend":
                                if (this.massSackDryBlend == null)
                                    this.massSackDryBlend = new massMeasure();
                                this.massSackDryBlend.HandleXML(xmlReader.ReadSubtree(), isNew);
                                break;
                            case "cementAdditive":
                                //collection
                                StaticHelper.AddUpdateWithUid<cs_cementAdditive>(xmlReader, isCementAdditNew, ref listCementAddit);
                                break;
                            case "foamUsed":
                                this.foamUsed = StaticParser.ParseBooleanFromString(StaticHelper.ReadString(xmlReader));
                                this.foamUsedSpecified = true;
                                break;
                            case "typeGasFoam":
                                this.typeGasFoam = StaticHelper.ReadString(xmlReader);
                                break;
                            case "volGasFoam":
                                if (this.volGasFoam == null)
                                    this.volGasFoam = new volumeMeasure();
                                this.volGasFoam.HandleXML(xmlReader.ReadSubtree(), isNew);
                                break;
                            case "ratioConstGasMethodAv":
                                if (this.ratioConstGasMethodAv == null)
                                    this.ratioConstGasMethodAv = new volumePerVolumeMeasure();
                                this.ratioConstGasMethodAv.HandleXML(xmlReader.ReadSubtree(), isNew);
                                break;
                            case "densConstGasMethod":
                                if (this.densConstGasMethod == null)
                                    this.densConstGasMethod = new densityMeasure();
                                this.densConstGasMethod.HandleXML(xmlReader.ReadSubtree(), isNew);
                                break;
                            case "ratioConstGasMethodStart":
                                if (this.ratioConstGasMethodStart == null)
                                    this.ratioConstGasMethodStart = new volumePerVolumeMeasure();
                                this.ratioConstGasMethodStart.HandleXML(xmlReader.ReadSubtree(), isNew);
                                break;
                            case "ratioConstGasMethodEnd":
                                if (this.ratioConstGasMethodEnd == null)
                                    this.ratioConstGasMethodEnd = new volumePerVolumeMeasure();
                                this.ratioConstGasMethodEnd.HandleXML(xmlReader.ReadSubtree(), isNew);
                                break;
                            case "densConstGasFoam":
                                if (this.densConstGasFoam == null)
                                    this.densConstGasFoam = new densityMeasure();
                                this.densConstGasFoam.HandleXML(xmlReader.ReadSubtree(), isNew);
                                break;
                            case "eTimThickening":
                                if (this.eTimThickening == null)
                                    this.eTimThickening = new timeMeasure();
                                this.eTimThickening.HandleXML(xmlReader.ReadSubtree(), isNew);
                                break;
                            case "tempThickening":
                                if (this.tempThickening == null)
                                    this.tempThickening = new thermodynamicTemperatureMeasure();
                                this.tempThickening.HandleXML(xmlReader.ReadSubtree(), isNew);
                                break;
                            case "presTestThickening":
                                if (this.presTestThickening == null)
                                    this.presTestThickening = new pressureMeasure();
                                this.presTestThickening.HandleXML(xmlReader.ReadSubtree(), isNew);
                                break;
                            case "consTestThickening":
                                if (this.consTestThickening == null)
                                    this.consTestThickening = new dimensionlessMeasure();
                                this.consTestThickening.HandleXML(xmlReader.ReadSubtree(), isNew);
                                break;
                            case "pcFreeWater":
                                if (this.pcFreeWater == null)
                                    this.pcFreeWater = new volumePerVolumeMeasure();
                                this.pcFreeWater.HandleXML(xmlReader.ReadSubtree(), isNew);
                                break;
                            case "tempFreeWater":
                                if (this.tempFreeWater == null)
                                    this.tempFreeWater = new thermodynamicTemperatureMeasure();
                                this.tempFreeWater.HandleXML(xmlReader.ReadSubtree(), isNew);
                                break;
                            case "volTestFluidLoss":
                                if (this.volTestFluidLoss == null)
                                    this.volTestFluidLoss = new volumeMeasure();
                                this.volTestFluidLoss.HandleXML(xmlReader.ReadSubtree(), isNew);
                                break;
                            case "tempFluidLoss":
                                if (this.tempFluidLoss == null)
                                    this.tempFluidLoss = new thermodynamicTemperatureMeasure();
                                this.tempFluidLoss.HandleXML(xmlReader.ReadSubtree(), isNew);
                                break;
                            case "presTestFluidLoss":
                                if (this.presTestFluidLoss == null)
                                    this.presTestFluidLoss = new pressureMeasure();
                                this.presTestFluidLoss.HandleXML(xmlReader.ReadSubtree(), isNew);
                                break;
                            case "timeFluidLoss":
                                if (this.timeFluidLoss == null)
                                    this.timeFluidLoss = new timeMeasure();
                                this.timeFluidLoss.HandleXML(xmlReader.ReadSubtree(), isNew);
                                break;
                            case "volAPIFluidLoss":
                                if (this.volAPIFluidLoss == null)
                                    this.volAPIFluidLoss = new volumeMeasure();
                                this.volAPIFluidLoss.HandleXML(xmlReader.ReadSubtree(), isNew);
                                break;
                            case "eTimComprStren1":
                                if (this.eTimComprStren1 == null)
                                    this.eTimComprStren1 = new timeMeasure();
                                this.eTimComprStren1.HandleXML(xmlReader.ReadSubtree(), isNew);
                                break;
                            case "eTimComprStren2":
                                if (this.eTimComprStren2 == null)
                                    this.eTimComprStren2 = new timeMeasure();
                                this.eTimComprStren2.HandleXML(xmlReader.ReadSubtree(), isNew);
                                break;
                            case "presComprStren1":
                                if (this.presComprStren1 == null)
                                    this.presComprStren1 = new pressureMeasure();
                                this.presComprStren1.HandleXML(xmlReader.ReadSubtree(), isNew);
                                break;
                            case "presComprStren2":
                                if (this.presComprStren2 == null)
                                    this.presComprStren2 = new pressureMeasure();
                                this.presComprStren2.HandleXML(xmlReader.ReadSubtree(), isNew);
                                break;
                            case "tempComprStren1":
                                if (this.tempComprStren1 == null)
                                    this.tempComprStren1 = new thermodynamicTemperatureMeasure();
                                this.tempComprStren1.HandleXML(xmlReader.ReadSubtree(), isNew);
                                break;
                            case "tempComprStren2":
                                if (this.tempComprStren2 == null)
                                    this.tempComprStren2 = new thermodynamicTemperatureMeasure();
                                this.tempComprStren2.HandleXML(xmlReader.ReadSubtree(), isNew);
                                break;
                            case "densAtPres":
                                if (this.densAtPres == null)
                                    this.densAtPres = new densityMeasure();
                                this.densAtPres.HandleXML(xmlReader.ReadSubtree(), isNew);
                                break;
                            case "volReserved":
                                if (this.volReserved == null)
                                    this.volReserved = new volumeMeasure();
                                this.volReserved.HandleXML(xmlReader.ReadSubtree(), isNew);
                                break;
                            case "volTotSlurry":
                                if (this.volTotSlurry == null)
                                    this.volTotSlurry = new volumeMeasure();
                                this.volTotSlurry.HandleXML(xmlReader.ReadSubtree(), isNew);
                                break;
                            default:
                                break;
                        }
                    }
                }
                //assign list to array
                this.cementAdditive = listCementAddit.ToArray();
            }
        }
    }
}
