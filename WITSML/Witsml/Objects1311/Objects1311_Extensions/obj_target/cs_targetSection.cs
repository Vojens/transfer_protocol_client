﻿ 

using System;
using System.Collections.Generic;
using System.Text;
using System.Xml;

namespace Witsml.Objects1311
{
    public partial class cs_targetSection : IHandleXML, IHasID
    {
        //interface for generic helper
        [System.Xml.Serialization.XmlIgnore]
        public string UID
        {
            get { return this.uid; }
        }
        public cs_targetSection()
        {
            this.typeTargetSectionScope = TargetSectionScope.unknown;
        }
        /// <summary>
        /// Convert XML string data into cs_targetSection fields value
        /// </summary>
        /// <param name="xmlReader">reader that has cs_targetSection node</param>
        /// <param name="IsNew">Is the object from stratch(true) or from db/serialization(false)</param>
        public void HandleXML(XmlReader xmlReader, bool isNew)
        {
            ////condition for add update collection
            bool isLocationNew = this.location == null;

            ////temporary list for data manipulations
            List<cs_location> listLocation = isLocationNew ? new List<cs_location>() : new List<cs_location>(this.location);

            using (xmlReader)
            {
                xmlReader.MoveToContent();
                if (xmlReader.HasAttributes)
                {
                    if (xmlReader.MoveToAttribute("uid"))
                        StaticHelper.HandleUID(xmlReader.Value, "targetSection uid", out this.uid);
                    xmlReader.MoveToElement();
                }
                while (xmlReader.Read())
                {
                    //start on depth 1 which are the childs that we want to check against
                    //check for starting tag and convert node value to its expected variable
                    if (xmlReader.Depth == 1 && xmlReader.IsStartElement() && !xmlReader.IsEmptyElement)
                    {
                        switch (xmlReader.Name)
                        {
                            case "sectNumber":
                                this.sectNumber = StaticParser.ParseShortFromString(StaticHelper.ReadString(xmlReader));
                                break;
                            case "typeTargetSectionScope":
                                this.typeTargetSectionScope = StaticParser.ParseEnumFromString<TargetSectionScope>(StaticHelper.ReadString(xmlReader), TargetSectionScope.unknown);
                                break;
                            case "lenRadius":
                                if (this.lenRadius == null)
                                    this.lenRadius = new lengthMeasure();
                                this.lenRadius.HandleXML(xmlReader.ReadSubtree(), isNew);
                                break;
                            case "angleArc":
                                if (this.angleArc == null)
                                    this.angleArc = new planeAngleMeasure();
                                this.angleArc.HandleXML(xmlReader.ReadSubtree(), isNew);
                                break;
                            case "thickAbove":
                                if (this.thickAbove == null)
                                    this.thickAbove = new lengthMeasure();
                                this.thickAbove.HandleXML(xmlReader.ReadSubtree(), isNew);
                                break;
                            case "thickBelow":
                                if (this.thickBelow == null)
                                    this.thickBelow = new lengthMeasure();
                                this.thickBelow.HandleXML(xmlReader.ReadSubtree(), isNew);
                                break;
                            case "location":
                                //collection
                                StaticHelper.AddUpdateWithUid<cs_location>(xmlReader, isLocationNew, ref listLocation);
                                break;
                            default:
                                break;
                        }
                    }
                }
                //assign list to array
                this.location = listLocation.ToArray();
            }
        }
    }
}
