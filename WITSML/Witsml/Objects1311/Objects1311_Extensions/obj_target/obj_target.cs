﻿ 

using System;
using System.Collections.Generic;
using System.IO;
using System.Text;
using System.Xml;
using Witsml.Common.Exceptions;
using Witsml.Exceptions;


namespace Witsml.Objects1311
{
    [System.Xml.Serialization.XmlRootAttribute("target", Namespace = "http://www.witsml.org/schemas/131", IsNullable = false)]
    public partial class obj_target : WITSMLObject, IHandleXML
    {
        public obj_target()
        {
            this.typeTargetScope = TargetScope.unknown;
            this.aziRef = AziRef.unknown;
            this.catTarg = TargetCategory.unknown;
        }

        /// <summary>
        /// Decode XML string into target
        /// </summary>
        /// <param name="xmlIn">XML string</param>
        public override void Decode(ref string xmlIn)
        {
            try
            {
                //Consume XML string and convert node value to variable based on node name
                using (StringReader stringReader = new StringReader(xmlIn))
                {
                    XmlReaderSettings setting = new XmlReaderSettings();
                    setting.ConformanceLevel = ConformanceLevel.Document;
                    setting.IgnoreComments = true;
                    setting.IgnoreWhitespace = true;
                    setting.IgnoreProcessingInstructions = true;
                    using (XmlReader xmlReader = XmlReader.Create(stringReader, setting))
                    {
                        //determine add or update operation
                        bool isNew;
                        if (this.uid == string.Empty)
                            isNew = true;
                        else isNew = false;

                        this.HandleXML(xmlReader, isNew);
                    }
                }
            }
            catch (XmlException ex)
            {
                throw new Exceptions.UnableToDecodeException(ex.Message, WITSMLReturnCode.WITSMLDocumentError, ex.InnerException);
            }
            catch (NullReferenceException ex)
            {
                throw new Exceptions.UnableToDecodeException(ex.Message, WITSMLReturnCode.WITSMLEmptyDocument, ex.InnerException);
            }
            catch (Exceptions.UnableToDecodeException ex)
            {
                throw ex;
            }
            catch (Exception ex)
            {
                throw new Exceptions.UnableToDecodeException(ex.Message, WITSMLReturnCode.WITSMLUnkownCause, ex.InnerException);
            }
        }

        /// <summary>
        /// Convert XML string data into surveyProgram fields value
        /// </summary>
        /// <param name="xmlReader">reader that has surveyProgram node</param>
        /// <param name="IsNew">Is the object from stratch(true) or from db/serialization(false)</param>
        public void HandleXML(XmlReader xmlReader, bool isNew)
        {
            ////condition for add update collection
            bool isLocationNew = this.location == null;
            bool isTargetSectionNew = this.targetSection == null;

            ////temporary list for data manipulations
            List<cs_location> listLocation = isLocationNew ? new List<cs_location>() : new List<cs_location>(this.location);
            List<cs_targetSection> listTargetSection = isTargetSectionNew ? new List<cs_targetSection>() : new List<cs_targetSection>(this.targetSection);

            using (xmlReader)
            {
                //assign uid for the new well
                xmlReader.MoveToContent();
                if (xmlReader.HasAttributes)
                {
                    if (xmlReader.MoveToAttribute("uidWell"))
                        StaticHelper.HandleUID(xmlReader.Value, "target uidWell", out this.uidWell);
                    if (xmlReader.MoveToAttribute("uidWellbore"))
                        StaticHelper.HandleUID(xmlReader.Value, "target uidWellbore", out this.uidWellbore);
                    if (xmlReader.MoveToAttribute("uid"))
                        StaticHelper.HandleUID(xmlReader.Value, "target uid", out this.uid);
                    xmlReader.MoveToElement();
                }
                while (xmlReader.Read())
                {
                    //only take child node
                    if (xmlReader.Depth == 1 && xmlReader.IsStartElement() && !xmlReader.IsEmptyElement)
                    {
                        switch (xmlReader.Name)
                        {
                            case "nameWell":
                                this.nameWell = StaticHelper.ReadString(xmlReader);
                                break;
                            case "nameWellbore":
                                this.nameWellbore = StaticHelper.ReadString(xmlReader);
                                break;
                            case "name":
                                this.name = StaticHelper.ReadString(xmlReader);
                                break;
                            case "parentTarget":
                                if (this.parentTarget == null)
                                    this.parentTarget = new refNameString();
                                this.parentTarget.ObjectName = "parentTarget";
                                this.parentTarget.HandleXML(xmlReader.ReadSubtree(), isNew);
                                break;
                            case "dispNsCenter":
                                if (this.dispNsCenter == null)
                                    this.dispNsCenter = new lengthMeasure();
                                this.dispNsCenter.HandleXML(xmlReader.ReadSubtree(), isNew);
                                break;
                            case "dispEwCenter":
                                if (this.dispEwCenter == null)
                                    this.dispEwCenter = new lengthMeasure();
                                this.dispEwCenter.HandleXML(xmlReader.ReadSubtree(), isNew);
                                break;
                            case "tvd":
                                if (this.tvd == null)
                                    this.tvd = new wellVerticalDepthCoord();
                                this.tvd.HandleXML(xmlReader.ReadSubtree(), isNew);
                                break;
                            case "dispNsOffset":
                                if (this.dispNsOffset == null)
                                    this.dispNsOffset = new lengthMeasure();
                                this.dispNsOffset.HandleXML(xmlReader.ReadSubtree(), isNew);
                                break;
                            case "dispEwOffset":
                                if (this.dispEwOffset == null)
                                    this.dispEwOffset = new lengthMeasure();
                                this.dispEwOffset.HandleXML(xmlReader.ReadSubtree(), isNew);
                                break;
                            case "thickAbove":
                                if (this.thickAbove == null)
                                    this.thickAbove = new lengthMeasure();
                                this.thickAbove.HandleXML(xmlReader.ReadSubtree(), isNew);
                                break;
                            case "thickBelow":
                                if (this.thickBelow == null)
                                    this.thickBelow = new lengthMeasure();
                                this.thickBelow.HandleXML(xmlReader.ReadSubtree(), isNew);
                                break;
                            case "dip":
                                if (this.dip == null)
                                    this.dip = new planeAngleMeasure();
                                this.dip.HandleXML(xmlReader.ReadSubtree(), isNew);
                                break;
                            case "strike":
                                if (this.strike == null)
                                    this.strike = new planeAngleMeasure();
                                this.strike.HandleXML(xmlReader.ReadSubtree(), isNew);
                                break;
                            case "rotation":
                                if (this.rotation == null)
                                    this.rotation = new planeAngleMeasure();
                                this.rotation.HandleXML(xmlReader.ReadSubtree(), isNew);
                                break;
                            case "lenMajorAxis":
                                if (this.lenMajorAxis == null)
                                    this.lenMajorAxis = new lengthMeasure();
                                this.lenMajorAxis.HandleXML(xmlReader.ReadSubtree(), isNew);
                                break;
                            case "widMinorAxis":
                                if (this.widMinorAxis == null)
                                    this.widMinorAxis = new lengthMeasure();
                                this.widMinorAxis.HandleXML(xmlReader.ReadSubtree(), isNew);
                                break;
                            case "typeTargetScope":
                                StaticParser.SetEnumFromString<TargetScope>(StaticHelper.ReadString(xmlReader), out this.typeTargetScope, out this.typeTargetScopeSpecified, TargetScope.unknown);
                                break;
                            case "dispNsSectOrig":
                                if (this.dispNsSectOrig == null)
                                    this.dispNsSectOrig = new lengthMeasure();
                                this.dispNsSectOrig.HandleXML(xmlReader.ReadSubtree(), isNew);
                                break;
                            case "dispEwSectOrig":
                                if (this.dispEwSectOrig == null)
                                    this.dispEwSectOrig = new lengthMeasure();
                                this.dispEwSectOrig.HandleXML(xmlReader.ReadSubtree(), isNew);
                                break;
                            case "aziRef":
                                StaticParser.SetEnumFromString<AziRef>(StaticHelper.ReadString(xmlReader), out this.aziRef, out this.aziRefSpecified, AziRef.unknown);
                                break;
                            case "catTarg":
                                StaticParser.SetEnumFromString<TargetCategory>(StaticHelper.ReadString(xmlReader), out this.catTarg, out this.catTargSpecified, TargetCategory.unknown);
                                break;
                            case "location":
                                //collection
                                StaticHelper.AddUpdateWithUid<cs_location>(xmlReader, isLocationNew, ref listLocation);
                                break;
                            case "targetSection":
                                //collection
                                StaticHelper.AddUpdateWithUid<cs_targetSection>(xmlReader, isTargetSectionNew, ref listTargetSection);
                                break;
                            case "commonData":
                                if (this.commonData == null)
                                    this.commonData = new cs_commonData();
                                this.commonData.HandleXML(xmlReader.ReadSubtree(), isNew);
                                break;
                            case "customData":
                                if (this.customData == null)
                                    this.customData = new cs_customData();
                                this.customData.HandleXML(xmlReader.ReadSubtree(), isNew);
                                break;
                            default:
                                break;
                        }
                    }
                }
                //assign list to collection
                this.location = listLocation.ToArray();
                this.targetSection = listTargetSection.ToArray();
            }
        }

        public override string ValidateGetPath(string newUid)
        {
            // Fix uid if the Object dosn't have one.
            if (!IsValidUid(this.uid))
            {
                this.uid = newUid;
                // TODO: Add some loging that we created a new UID for the object maybe.
            }

            StringBuilder path = new StringBuilder(this.GetParentPath());
            path.Append(@"\");
            path.Append(this.uid);
            return path.ToString();
        }

        public override string GetParentPath()
        {
            if (!IsValidUid(this.uidWell) || !IsValidUid(this.uidWellbore))
            {
                throw new UnableToDecodeException("One or more Parent UID's are invalid", WITSMLReturnCode.WITSMLInvalidParentUID);
                //TODO: Log the issue
            }

            StringBuilder path = new StringBuilder(@"\");
            path.Append(this.uidWell);
            path.Append(@"\");
            path.Append(this.uidWellbore);
            return path.ToString();
        }

        // NOTE: these are done with functions, instead of property access, so as not to mess around with the auto-generated / serialized classes.
        public override string Get_uid()
        {
            return this.uid;
        }

        public override string Get_uidWell()
        {
            return this.uidWell;
        }

        public override string Get_uidWellbore()
        {
            return this.uidWellbore;
        }

        public override string Get_name()
        {
            return this.name;
        }

        public override string Get_nameWell()
        {
            return this.nameWell;
        }

        public override string Get_nameWellbore()
        {
            return this.nameWellbore;
        }

        public override ICommonData Get_commonData()
        {
            if (this.commonData == null)
            {
                this.commonData = new cs_commonData();
            }
            return (ICommonData)this.commonData;
        }
        public override ICustomData Get_customData()
        {
            if (this.customData == null)
            {
                this.customData = new cs_customData();
            }
            return (ICustomData)this.customData;
        }
    }
}
