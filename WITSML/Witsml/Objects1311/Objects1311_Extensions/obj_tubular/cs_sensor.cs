﻿ 

using System;
using System.Collections.Generic;
using System.Text;
using System.Xml;

namespace Witsml.Objects1311
{
    public partial class cs_sensor : IHandleXML, IHasID
    {
        //interface for generic helper
        [System.Xml.Serialization.XmlIgnore]
        public string UID
        {
            get { return this.uid; }
        }
        public cs_sensor()
        {
            this.typeMeasurement = MeasurementType.unknown;
        }
        /// <summary>
        /// Convert XML string data into cs_sensor fields value
        /// </summary>
        /// <param name="xmlReader">reader that has cs_sensor node</param>
        /// <param name="IsNew">Is the object from stratch(true) or from db/serialization(false)</param>
        public void HandleXML(XmlReader xmlReader, bool isNew)
        {
            using (xmlReader)
            {
                xmlReader.MoveToContent();
                if (xmlReader.HasAttributes)
                {
                    if (xmlReader.MoveToAttribute("uid"))
                        StaticHelper.HandleUID(xmlReader.Value, "sensor uid", out this.uid);
                    xmlReader.MoveToElement();
                }
                while (xmlReader.Read())
                {
                    //start on depth 1 which are the childs that we want to check against
                    //check for starting tag and convert node value to its expected variable
                    if (xmlReader.Depth == 1 && xmlReader.IsStartElement() && !xmlReader.IsEmptyElement)
                    {
                        switch (xmlReader.Name)
                        {
                            case "typeMeasurement":
                                StaticParser.SetEnumFromString<MeasurementType>(StaticHelper.ReadString(xmlReader), out this.typeMeasurement, out this.typeMeasurementSpecified, MeasurementType.unknown);
                                break;
                            case "offsetBot":
                                if (this.offsetBot == null)
                                    this.offsetBot = new lengthMeasure();
                                this.offsetBot.HandleXML(xmlReader.ReadSubtree(), isNew);
                                break;
                            case "comments":
                                this.comments = StaticHelper.ReadString(xmlReader);
                                break;
                            case "customData":
                                if (this.customData == null)
                                    this.customData = new cs_customData();
                                this.customData.HandleXML(xmlReader.ReadSubtree(), isNew);
                                break;
                            default:
                                break;
                        }
                    }
                }
            }
        }
    }
}
