﻿ 

using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Xml;

namespace Witsml.Objects1311
{
    public partial class cs_tubularComponent : IHandleXML, IHasID
    {
        //interface for generic helper
        [System.Xml.Serialization.XmlIgnore]
        public string UID
        {
            get { return this.uid; }
        }
        public cs_tubularComponent()
        {
            this.typeTubularComp = TubularComponent.unknown;
            this.configCon = BoxPinConfig.unknown;
            this.typeMaterial = MaterialType.unknown;
        }
        public void HandleXML(XmlReader xmlReader, bool isNew)
        {
            bool isNameTagNew = this.nameTag == null;
            bool isNozzleNew = this.nozzle == null;
            bool isConnectionNew = this.connection == null;
            bool isStabilizerNew = this.stabilizer == null;
            bool isBendNew = this.bend == null;

            //temporary list
            List<cs_nameTag> listNameTag = isNameTagNew ? new List<cs_nameTag>() : new List<cs_nameTag>(this.nameTag);
            List<cs_nozzle> listNozzle = isNozzleNew ? new List<cs_nozzle>() : new List<cs_nozzle>(this.nozzle);
            List<cs_connection> listConnection = isConnectionNew ? new List<cs_connection>() : new List<cs_connection>(this.connection);
            List<cs_stabilizer> listStabilizer = isStabilizerNew ? new List<cs_stabilizer>() : new List<cs_stabilizer>(this.stabilizer);
            List<cs_bend> listBend = isBendNew ? new List<cs_bend>() : new List<cs_bend>(this.bend);

            using (xmlReader)
            {
                xmlReader.MoveToContent();
                if (xmlReader.HasAttributes)
                {
                    if (xmlReader.MoveToAttribute("uid"))
                        StaticHelper.HandleUID(xmlReader.Value, "tubularComponent uid", out this.uid);
                    xmlReader.MoveToElement();
                }
                while (xmlReader.Read())
                {
                    //only take child node
                    if (xmlReader.Depth == 1 && xmlReader.IsStartElement() && !xmlReader.IsEmptyElement)
                    {
                        switch (xmlReader.Name)
                        {
                            case "typeTubularComp":
                                this.typeTubularComp = StaticParser.ParseEnumFromString<TubularComponent>(StaticHelper.ReadString(xmlReader), TubularComponent.unknown);
                                break;
                            case "sequence":
                                this.sequence = StaticParser.ParseShortFromString(StaticHelper.ReadString(xmlReader));
                                break;
                            case "description":
                                this.description = StaticHelper.ReadString(xmlReader);
                                break;
                            case "id":
                                if (this.id == null)
                                    this.id = new lengthMeasure();
                                this.id.HandleXML(xmlReader.ReadSubtree(), isNew);
                                break;
                            case "od":
                                if (this.od == null)
                                    this.od = new lengthMeasure();
                                this.od.HandleXML(xmlReader.ReadSubtree(), isNew);
                                break;
                            case "odMx":
                                if (this.odMx == null)
                                    this.odMx = new lengthMeasure();
                                this.odMx.HandleXML(xmlReader.ReadSubtree(), isNew);
                                break;
                            case "len":
                                if (this.len == null)
                                    this.len = new lengthMeasure();
                                this.len.HandleXML(xmlReader.ReadSubtree(), isNew);
                                break;
                            case "lenJointAv":
                                if (this.lenJointAv == null)
                                    this.lenJointAv = new lengthMeasure();
                                this.lenJointAv.HandleXML(xmlReader.ReadSubtree(), isNew);
                                break;
                            case "numJointStand":
                                this.numJointStand = StaticParser.ParseShortFromString(StaticHelper.ReadString(xmlReader));
                                this.numJointStandSpecified = true;
                                break;
                            case "wtPerLen":
                                if (this.wtPerLen == null)
                                    this.wtPerLen = new massPerLengthMeasure();
                                this.wtPerLen.HandleXML(xmlReader.ReadSubtree(), isNew);
                                break;
                            case "grade":
                                this.grade = StaticHelper.ReadString(xmlReader);
                                break;
                            case "odDrift":
                                if (this.odDrift == null)
                                    this.odDrift = new lengthMeasure();
                                this.odDrift.HandleXML(xmlReader.ReadSubtree(), isNew);
                                break;
                            case "tensYield":
                                if (this.tensYield == null)
                                    this.tensYield = new pressureMeasure();
                                this.tensYield.HandleXML(xmlReader.ReadSubtree(), isNew);
                                break;
                            case "tqYield":
                                if (this.tqYield == null)
                                    this.tqYield = new pressureMeasure();
                                this.tqYield.HandleXML(xmlReader.ReadSubtree(), isNew);
                                break;
                            case "stressFatig":
                                if (this.stressFatig == null)
                                    this.stressFatig = new pressureMeasure();
                                this.stressFatig.HandleXML(xmlReader.ReadSubtree(), isNew);
                                break;
                            case "lenFishneck":
                                if (this.lenFishneck == null)
                                    this.lenFishneck = new lengthMeasure();
                                this.lenFishneck.HandleXML(xmlReader.ReadSubtree(), isNew);
                                break;
                            case "idFishneck":
                                if (this.idFishneck == null)
                                    this.idFishneck = new lengthMeasure();
                                this.idFishneck.HandleXML(xmlReader.ReadSubtree(), isNew);
                                break;
                            case "odFishneck":
                                if (this.odFishneck == null)
                                    this.odFishneck = new lengthMeasure();
                                this.odFishneck.HandleXML(xmlReader.ReadSubtree(), isNew);
                                break;
                            case "disp":
                                if (this.disp == null)
                                    this.disp = new volumeMeasure();
                                this.disp.HandleXML(xmlReader.ReadSubtree(), isNew);
                                break;
                            case "presBurst":
                                if (this.presBurst == null)
                                    this.presBurst = new pressureMeasure();
                                this.presBurst.HandleXML(xmlReader.ReadSubtree(), isNew);
                                break;
                            case "presCollapse":
                                if (this.presCollapse == null)
                                    this.presCollapse = new pressureMeasure();
                                this.presCollapse.HandleXML(xmlReader.ReadSubtree(), isNew);
                                break;
                            case "classService":
                                this.classService = StaticHelper.ReadString(xmlReader);
                                break;
                            case "wearWall":
                                if (this.wearWall == null)
                                    this.wearWall = new lengthPerLengthMeasure();
                                this.wearWall.HandleXML(xmlReader.ReadSubtree(), isNew);
                                break;
                            case "thickWall":
                                if (this.thickWall == null)
                                    this.thickWall = new lengthMeasure();
                                this.thickWall.HandleXML(xmlReader.ReadSubtree(), isNew);
                                break;
                            case "configCon":
                                StaticParser.SetEnumFromString<BoxPinConfig>(StaticHelper.ReadString(xmlReader), out this.configCon, out this.configConSpecified, BoxPinConfig.unknown);
                                break;
                            case "bendStiffness":
                                if (this.bendStiffness == null)
                                    this.bendStiffness = new forcePerLengthMeasure();
                                this.bendStiffness.HandleXML(xmlReader.ReadSubtree(), isNew);
                                break;
                            case "axialStiffness":
                                if (this.axialStiffness == null)
                                    this.axialStiffness = new forcePerLengthMeasure();
                                this.axialStiffness.HandleXML(xmlReader.ReadSubtree(), isNew);
                                break;
                            case "torsionalStiffness":
                                if (this.torsionalStiffness == null)
                                    this.torsionalStiffness = new forcePerLengthMeasure();
                                this.torsionalStiffness.HandleXML(xmlReader.ReadSubtree(), isNew);
                                break;
                            case "typeMaterial":
                                StaticParser.SetEnumFromString<MaterialType>(StaticHelper.ReadString(xmlReader), out this.typeMaterial, out this.typeMaterialSpecified, MaterialType.unknown);
                                break;
                            case "doglegMx":
                                if (this.doglegMx == null)
                                    this.doglegMx = new anglePerLengthMeasure();
                                this.doglegMx.HandleXML(xmlReader.ReadSubtree(), isNew);
                                break;
                            case "vendor":
                                this.vendor = StaticHelper.ReadString(xmlReader);
                                break;
                            case "model":
                                this.model = StaticHelper.ReadString(xmlReader);
                                break;
                            case "nameTag":
                                //collection
                                StaticHelper.AddUpdateWithUid<cs_nameTag>(xmlReader, isNameTagNew, ref listNameTag);
                                break;
                            case "bitRecord":
                                if (this.bitRecord == null)
                                    this.bitRecord = new cs_bitRecord();
                                this.bitRecord.HandleXML(xmlReader.ReadSubtree(), isNew);
                                break;
                            case "areaNozzleFlow":
                                if (this.areaNozzleFlow == null)
                                    this.areaNozzleFlow = new areaMeasure();
                                this.areaNozzleFlow.HandleXML(xmlReader.ReadSubtree(), isNew);
                                break;
                            case "nozzle":
                                //collection
                                StaticHelper.AddUpdateWithUid<cs_nozzle>(xmlReader, isNozzleNew, ref listNozzle);
                                break;
                            case "connection":
                                //collection
                                StaticHelper.AddUpdateWithUid<cs_connection>(xmlReader, isConnectionNew, ref listConnection);
                                break;
                            case "jar":
                                if (this.jar == null)
                                    this.jar = new cs_jar();
                                this.jar.HandleXML(xmlReader.ReadSubtree(), isNew);
                                break;
                            case "mwdTool":
                                if (this.mwdTool == null)
                                    this.mwdTool = new cs_mwdTool();
                                this.mwdTool.HandleXML(xmlReader.ReadSubtree(), isNew);
                                break;
                            case "motor":
                                if (this.motor == null)
                                    this.motor = new cs_motor();
                                this.motor.HandleXML(xmlReader.ReadSubtree(), isNew);
                                break;
                            case "stabilizer":
                                //collection
                                StaticHelper.AddUpdateWithUid<cs_stabilizer>(xmlReader, isStabilizerNew, ref listStabilizer);
                                break;
                            case "bend":
                                //collection
                                StaticHelper.AddUpdateWithUid<cs_bend>(xmlReader, isBendNew, ref listBend);
                                break;
                            case "holeOpener":
                                if (this.holeOpener == null)
                                    this.holeOpener = new cs_holeOpener();
                                this.holeOpener.HandleXML(xmlReader.ReadSubtree(), isNew);
                                break;
                            case "rotarySteerableTool":
                                if (this.rotarySteerableTool == null)
                                    this.rotarySteerableTool = new cs_rotarySteerableTool();
                                this.rotarySteerableTool.HandleXML(xmlReader.ReadSubtree(), isNew);
                                break;
                            case "customData":
                                if (this.customData == null)
                                    this.customData = new cs_customData();
                                this.customData.HandleXML(xmlReader.ReadSubtree(), isNew);
                                break;
                            default:
                                break;
                        }
                    }
                }
                //assign list to array
                this.nameTag = listNameTag.ToArray();
                this.nozzle = listNozzle.ToArray();
                this.connection = listConnection.ToArray();
                this.stabilizer = listStabilizer.ToArray();
                this.bend = listBend.ToArray();
            }
        }
    }
}
