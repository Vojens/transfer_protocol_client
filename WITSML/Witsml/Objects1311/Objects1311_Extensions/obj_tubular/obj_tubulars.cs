﻿ 

using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.IO;
using System.Xml;
using Witsml.Common.Exceptions;


namespace Witsml.Objects1311
{
    public partial class obj_tubulars : WITSMLDocument
    {
        public override WITSMLObject GetSingleObject(int Item)
        {
            return this.tubular[Item];
        }

        public override int Decode(ref string xmlIn)
        {
            int countOfTubular = 0;
            try
            {
                //check whether collection is from stratch(isNew = true) or from deserialization(isNew=false)
                bool isNew = this.tubular == null;

                //create temporary list to help with data manipulation
                List<obj_tubular> listTubular = isNew ? new List<obj_tubular>() : new List<obj_tubular>(this.tubular);

                using (StringReader stringReader = new StringReader(xmlIn))
                {
                    XmlReaderSettings setting = new XmlReaderSettings();
                    setting.ConformanceLevel = ConformanceLevel.Document;
                    setting.IgnoreComments = true;
                    setting.IgnoreWhitespace = true;
                    setting.IgnoreProcessingInstructions = true;
                    using (XmlReader xmlReader = XmlReader.Create(stringReader, setting))
                    {
                        while (xmlReader.Read())
                        {
                            //assign version
                            xmlReader.MoveToContent();
                            if (xmlReader.MoveToAttribute("version"))
                                this.version = xmlReader.Value;
                            else
                                this.version = "1.3.1.1";//no version specified,set it to 1.3.1.1

                            //pre-checking on whether node is the correct child based on depth,start tag and not empty
                            if (xmlReader.Depth == 1 && xmlReader.IsStartElement() && !xmlReader.IsEmptyElement)
                            {
                                switch (xmlReader.Name)
                                {
                                    case "documentInfo":
                                        if (this.documentInfo == null)
                                            this.documentInfo = new cs_documentInfo();
                                        this.documentInfo.HandleXML(xmlReader.ReadSubtree(), isNew);
                                        break;
                                    case "tubular":
                                        obj_tubular tubularObj;
                                        string currentUID = string.Empty;

                                        //Update tubular
                                        if (!isNew)
                                        {
                                            //check for uid attribute and assign it to temporary string for searching in collection
                                            if (xmlReader.HasAttributes)
                                            {
                                                if (xmlReader.MoveToAttribute("uid"))
                                                    currentUID = xmlReader.Value;
                                                xmlReader.MoveToElement();
                                            }
                                            //look for tubular based on its UID and update the tubular with xml string
                                            if (currentUID != string.Empty)
                                            {
                                                tubularObj = listTubular.FirstOrDefault(
                                                    delegate(obj_tubular objTubular)
                                                    { if (objTubular.uid == currentUID)return true; else return false; });
                                                if (tubularObj != null)
                                                {
                                                    tubularObj.HandleXML(xmlReader.ReadSubtree(), isNew);
                                                    countOfTubular++;
                                                    continue;
                                                }
                                                else
                                                {
                                                    //Update can't process tubular without match
                                                    continue;
                                                }
                                            }
                                            else
                                            {
                                                //Update can't process tubular without uid
                                                continue;
                                            }
                                        }

                                        //Add New tubular 
                                        tubularObj = new obj_tubular();
                                        tubularObj.HandleXML(xmlReader.ReadSubtree(), true);
                                        listTubular.Add(tubularObj);
                                        countOfTubular++;
                                        break;
                                    default:
                                        break;
                                }
                            }
                        }
                        //assign list to folder collection
                        this.tubular = listTubular.ToArray();
                    }
                }
            }
            catch (XmlException ex)
            {
                throw new Exceptions.UnableToDecodeException(ex.Message, WITSMLReturnCode.WITSMLDocumentError, ex.InnerException);
            }
            catch (NullReferenceException ex)
            {
                throw new Exceptions.UnableToDecodeException(ex.Message, WITSMLReturnCode.WITSMLEmptyDocument, ex.InnerException);
            }
            catch (Exceptions.UnableToDecodeException ex)
            {
                throw ex;
            }
            catch (Exception ex)
            {
                throw new Exceptions.UnableToDecodeException(ex.Message, WITSMLReturnCode.WITSMLUnkownCause, ex.InnerException);
            }
            return countOfTubular;
        }
    }
}
