﻿ 

using System;
using System.Collections.Generic;
using System.Text;
using System.Xml;

namespace Witsml.Objects1311
{
    public partial class cs_mwdTool : IHandleXML
    {
        /// <summary>
        /// Convert XML string data into cs_mwdTool fields value
        /// </summary>
        /// <param name="xmlReader">reader that has cs_mwdTool node</param>
        /// <param name="IsNew">Is the object from stratch(true) or from db/serialization(false)</param>
        public void HandleXML(XmlReader xmlReader, bool isNew)
        {
            bool isSensorNew = this.sensor == null;

            //temporary list for collection manipulation
            List<cs_sensor> listSensor = isSensorNew ? new List<cs_sensor>() : new List<cs_sensor>(this.sensor);

            using (xmlReader)
            {
                while (xmlReader.Read())
                {
                    //start on depth 1 which are the childs that we want to check against
                    //check for starting tag and convert node value to its expected variable
                    if (xmlReader.Depth == 1 && xmlReader.IsStartElement() && !xmlReader.IsEmptyElement)
                    {
                        switch (xmlReader.Name)
                        {
                            case "flowrateMn":
                                if (this.flowrateMn == null)
                                    this.flowrateMn = new volumeFlowRateMeasure();
                                this.flowrateMn.HandleXML(xmlReader.ReadSubtree(), isNew);
                                break;
                            case "flowrateMx":
                                if (this.flowrateMx == null)
                                    this.flowrateMx = new volumeFlowRateMeasure();
                                this.flowrateMx.HandleXML(xmlReader.ReadSubtree(), isNew);
                                break;
                            case "tempMx":
                                if (this.tempMx == null)
                                    this.tempMx = new thermodynamicTemperatureMeasure();
                                this.tempMx.HandleXML(xmlReader.ReadSubtree(), isNew);
                                break;
                            case "idEquv":
                                if (this.idEquv == null)
                                    this.idEquv = new lengthMeasure();
                                this.idEquv.HandleXML(xmlReader.ReadSubtree(), isNew);
                                break;
                            case "sensor":
                                //collection
                                StaticHelper.AddUpdateWithUid<cs_sensor>(xmlReader, isSensorNew, ref listSensor);
                                break;
                            case "customData":
                                if (this.customData == null)
                                    this.customData = new cs_customData();
                                this.customData.HandleXML(xmlReader.ReadSubtree(), isNew);
                                break;
                            default:
                                break;
                        }
                    }
                }
                //assign list to array
                this.sensor = listSensor.ToArray();
            }
        }
    }
}
