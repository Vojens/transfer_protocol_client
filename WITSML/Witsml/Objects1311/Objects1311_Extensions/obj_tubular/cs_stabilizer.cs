﻿ 

using System;
using System.Collections.Generic;
using System.Text;
using System.Xml;

namespace Witsml.Objects1311
{
    public partial class cs_stabilizer : IHandleXML, IHasID
    {
        //interface for generic helper
        [System.Xml.Serialization.XmlIgnore]
        public string UID
        {
            get { return this.uid; }
        }
        public cs_stabilizer()
        {
            this.shapeBlade = BladeShapeType.unknown;
            this.typeBlade = BladeType.unknown;
        }
        /// <summary>
        /// Convert XML string data into cs_stabilizer fields value
        /// </summary>
        /// <param name="xmlReader">reader that has cs_stabilizer node</param>
        /// <param name="IsNew">Is the object from stratch(true) or from db/serialization(false)</param>
        public void HandleXML(XmlReader xmlReader, bool isNew)
        {
            using (xmlReader)
            {
                xmlReader.MoveToContent();
                if (xmlReader.HasAttributes)
                {
                    if (xmlReader.MoveToAttribute("uid"))
                        StaticHelper.HandleUID(xmlReader.Value, "stabilizer uid", out this.uid);
                    xmlReader.MoveToElement();
                }
                while (xmlReader.Read())
                {
                    //start on depth 1 which are the childs that we want to check against
                    //check for starting tag and convert node value to its expected variable
                    if (xmlReader.Depth == 1 && xmlReader.IsStartElement() && !xmlReader.IsEmptyElement)
                    {
                        switch (xmlReader.Name)
                        {
                            case "lenBlade":
                                if (this.lenBlade == null)
                                    this.lenBlade = new lengthMeasure();
                                this.lenBlade.HandleXML(xmlReader.ReadSubtree(), isNew);
                                break;
                            case "lenBladeGauge":
                                if (this.lenBladeGauge == null)
                                    this.lenBladeGauge = new lengthMeasure();
                                this.lenBladeGauge.HandleXML(xmlReader.ReadSubtree(), isNew);
                                break;
                            case "odBladeMx":
                                if (this.odBladeMx == null)
                                    this.odBladeMx = new lengthMeasure();
                                this.odBladeMx.HandleXML(xmlReader.ReadSubtree(), isNew);
                                break;
                            case "odBladeMn":
                                if (this.odBladeMn == null)
                                    this.odBladeMn = new lengthMeasure();
                                this.odBladeMn.HandleXML(xmlReader.ReadSubtree(), isNew);
                                break;
                            case "distBladeBot":
                                if (this.distBladeBot == null)
                                    this.distBladeBot = new lengthMeasure();
                                this.distBladeBot.HandleXML(xmlReader.ReadSubtree(), isNew);
                                break;
                            case "shapeBlade":
                                StaticParser.SetEnumFromString<BladeShapeType>(StaticHelper.ReadString(xmlReader), out this.shapeBlade, out this.shapeBladeSpecified, BladeShapeType.unknown);
                                break;
                            case "factFric":
                                this.factFric = StaticParser.ParseDoubleFromString(StaticHelper.ReadString(xmlReader));
                                this.factFricSpecified = true;
                                break;
                            case "typeBlade":
                                StaticParser.SetEnumFromString<BladeType>(StaticHelper.ReadString(xmlReader), out this.typeBlade, out this.typeBladeSpecified, BladeType.unknown);
                                break;
                            case "customData":
                                if (this.customData == null)
                                    this.customData = new cs_customData();
                                this.customData.HandleXML(xmlReader.ReadSubtree(), isNew);
                                break;
                            default:
                                break;
                        }
                    }
                }
            }
        }
    }
}
