﻿ 

using System;
using System.Collections.Generic;
using System.Text;
using System.Xml;

namespace Witsml.Objects1311
{
    public partial class cs_nozzle : IHandleXML, IHasID
    {
        //interface for generic helper
        [System.Xml.Serialization.XmlIgnore]
        public string UID
        {
            get { return this.uid; }
        }
        public cs_nozzle()
        {
            this.typeNozzle = NozzleType.unknown;
        }
        /// <summary>
        /// Convert XML string data into cs_noozle fields value
        /// </summary>
        /// <param name="xmlReader">reader that has cs_noozle node</param>
        /// <param name="IsNew">Is the object from stratch(true) or from db/serialization(false)</param>
        public void HandleXML(XmlReader xmlReader, bool isNew)
        {
            using (xmlReader)
            {
                xmlReader.MoveToContent();
                if (xmlReader.HasAttributes)
                {
                    if (xmlReader.MoveToAttribute("uid"))
                        StaticHelper.HandleUID(xmlReader.Value, "nozzle uid", out this.uid);
                    xmlReader.MoveToElement();
                }
                while (xmlReader.Read())
                {
                    //start on depth 1 which are the childs that we want to check against
                    //check for starting tag and convert node value to its expected variable
                    if (xmlReader.Depth == 1 && xmlReader.IsStartElement() && !xmlReader.IsEmptyElement)
                    {
                        switch (xmlReader.Name)
                        {
                            case "index":
                                this.index = StaticParser.ParseShortFromString(StaticHelper.ReadString(xmlReader));
                                this.indexSpecified = true;
                                break;
                            case "diaNozzle":
                                if (this.diaNozzle == null)
                                    this.diaNozzle = new lengthMeasure();
                                this.diaNozzle.HandleXML(xmlReader.ReadSubtree(), isNew);
                                break;
                            case "typeNozzle":
                                StaticParser.SetEnumFromString<NozzleType>(StaticHelper.ReadString(xmlReader), out this.typeNozzle, out this.typeNozzleSpecified, NozzleType.unknown);
                                break;
                            case "len":
                                if (this.len == null)
                                    this.len = new lengthMeasure();
                                this.len.HandleXML(xmlReader.ReadSubtree(), isNew);
                                break;
                            case "orientation":
                                this.orientation = StaticHelper.ReadString(xmlReader);
                                break;
                            case "customData":
                                if (this.customData == null)
                                    this.customData = new cs_customData();
                                this.customData.HandleXML(xmlReader.ReadSubtree(), isNew);
                                break;
                            default:
                                break;
                        }
                    }
                }
            }
        }
    }
}
