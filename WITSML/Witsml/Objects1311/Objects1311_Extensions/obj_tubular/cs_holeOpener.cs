﻿ 

using System;
using System.Collections.Generic;
using System.Text;
using System.Xml;

namespace Witsml.Objects1311
{
    public partial class cs_holeOpener : IHandleXML
    {
        public cs_holeOpener()
        {
            this.typeHoleOpener = HoleOpenerType.unknown;
        }
        /// <summary>
        /// Convert XML string data into cs_holeOpener.cs fields value
        /// </summary>
        /// <param name="xmlReader">reader that has cs_holeOpener.cs node</param>
        /// <param name="IsNew">Is the object from stratch(true) or from db/serialization(false)</param>
        public void HandleXML(XmlReader xmlReader, bool isNew)
        {
            using (xmlReader)
            {
                while (xmlReader.Read())
                {
                    //start on depth 1 which are the childs that we want to check against
                    //check for starting tag and convert node value to its expected variable
                    if (xmlReader.Depth == 1 && xmlReader.IsStartElement() && !xmlReader.IsEmptyElement)
                    {
                        switch (xmlReader.Name)
                        {
                            case "typeHoleOpener":
                                StaticParser.SetEnumFromString<HoleOpenerType>(StaticHelper.ReadString(xmlReader), out this.typeHoleOpener, out this.typeHoleOpenerSpecified, HoleOpenerType.unknown);
                                break;
                            case "numCutter":
                                this.numCutter = StaticParser.ParseShortFromString(StaticHelper.ReadString(xmlReader));
                                this.numCutterSpecified = true;
                                break;
                            case "manufacturer":
                                this.manufacturer = StaticHelper.ReadString(xmlReader);
                                break;
                            case "diaHoleOpener":
                                if (this.diaHoleOpener == null)
                                    this.diaHoleOpener = new lengthMeasure();
                                this.diaHoleOpener.HandleXML(xmlReader.ReadSubtree(), isNew);
                                break;
                            case "customData":
                                if (this.customData == null)
                                    this.customData = new cs_customData();
                                this.customData.HandleXML(xmlReader.ReadSubtree(), isNew);
                                break;
                            default:
                                break;
                        }
                    }
                }
            }
        }
    }
}
