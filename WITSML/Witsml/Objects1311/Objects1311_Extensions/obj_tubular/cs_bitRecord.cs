﻿ 

using System;
using System.Collections.Generic;
using System.Text;
using System.Xml;

namespace Witsml.Objects1311
{
    public partial class cs_bitRecord : IHandleXML
    {
        public cs_bitRecord()
        {
            this.typeBit = BitType.unknown;
            this.condInitDull = BitDullCode.unknown;
            this.condInitReason = BitReasonPulled.unknown;
            this.condFinalDull = BitDullCode.unknown;
            this.condFinalReason = BitReasonPulled.unknown;
        }
        /// <summary>
        /// Convert XML string data into cs_bitRecord fields value
        /// </summary>
        /// <param name="xmlReader">reader that has cs_bitRecord node</param>
        /// <param name="IsNew">Is the object from stratch(true) or from db/serialization(false)</param>
        public void HandleXML(XmlReader xmlReader, bool isNew)
        {
            using (xmlReader)
            {
                while (xmlReader.Read())
                {
                    //start on depth 1 which are the childs that we want to check against
                    //check for starting tag and convert node value to its expected variable
                    if (xmlReader.Depth == 1 && xmlReader.IsStartElement() && !xmlReader.IsEmptyElement)
                    {
                        switch (xmlReader.Name)
                        {
                            case "numBit":
                                this.numBit = StaticHelper.ReadString(xmlReader);
                                break;
                            case "diaBit":
                                if (this.diaBit == null)
                                    this.diaBit = new lengthMeasure();
                                this.diaBit.HandleXML(xmlReader.ReadSubtree(), isNew);
                                break;
                            case "diaPassThru":
                                if (this.diaPassThru == null)
                                    this.diaPassThru = new lengthMeasure();
                                this.diaPassThru.HandleXML(xmlReader.ReadSubtree(), isNew);
                                break;
                            case "diaPilot":
                                if (this.diaPilot == null)
                                    this.diaPilot = new lengthMeasure();
                                this.diaPilot.HandleXML(xmlReader.ReadSubtree(), isNew);
                                break;
                            case "manufacturer":
                                this.manufacturer = StaticHelper.ReadString(xmlReader);
                                break;
                            case "typeBit":
                                StaticParser.SetEnumFromString<BitType>(StaticHelper.ReadString(xmlReader), out this.typeBit, out this.typeBitSpecified,BitType.unknown);
                                break;
                            case "cost":
                                if (this.cost == null)
                                    this.cost = new cost();
                                this.cost.HandleXML(xmlReader.ReadSubtree(), isNew);
                                break;
                            case "codeIADC":
                                this.codeIADC = StaticHelper.ReadString(xmlReader);
                                break;
                            case "condInitInner":
                                this.condInitInner = StaticParser.ParseShortFromString(StaticHelper.ReadString(xmlReader));
                                this.condInitInnerSpecified = true;
                                break;
                            case "condInitOuter":
                                this.condInitOuter = StaticParser.ParseShortFromString(StaticHelper.ReadString(xmlReader));
                                this.condInitOuterSpecified = true;
                                break;
                            case "condInitDull":
                                StaticParser.SetEnumFromString<BitDullCode>(StaticHelper.ReadString(xmlReader), out this.condInitDull, out this.condInitDullSpecified,BitDullCode.unknown);
                                break;
                            case "condInitLocation":
                                this.condInitLocation = StaticHelper.ReadString(xmlReader);
                                break;
                            case "condInitBearing":
                                this.condInitBearing = StaticHelper.ReadString(xmlReader);
                                break;
                            case "condInitGauge":
                                this.condInitGauge = StaticHelper.ReadString(xmlReader);
                                break;
                            case "condInitOther":
                                this.condInitOther = StaticHelper.ReadString(xmlReader);
                                break;
                            case "condInitReason":
                                StaticParser.SetEnumFromString<BitReasonPulled>(StaticHelper.ReadString(xmlReader), out this.condInitReason, out this.condInitReasonSpecified,BitReasonPulled.unknown);
                                break;
                            case "condFinalInner":
                                this.condFinalInner = StaticParser.ParseShortFromString(StaticHelper.ReadString(xmlReader));
                                this.condFinalInnerSpecified = true;
                                break;
                            case "condFinalOuter":
                                this.condFinalOuter = StaticParser.ParseShortFromString(StaticHelper.ReadString(xmlReader));
                                this.condFinalOuterSpecified = true;
                                break;
                            case "condFinalDull":
                                StaticParser.SetEnumFromString<BitDullCode>(StaticHelper.ReadString(xmlReader), out this.condFinalDull, out this.condFinalDullSpecified, BitDullCode.unknown);
                                break;
                            case "condFinalLocation":
                                this.condFinalLocation = StaticHelper.ReadString(xmlReader);
                                break;
                            case "condFinalBearing":
                                this.condFinalBearing = StaticHelper.ReadString(xmlReader);
                                break;
                            case "condFinalGauge":
                                this.condFinalGauge = StaticHelper.ReadString(xmlReader);
                                break;
                            case "condFinalOther":
                                this.condFinalOther = StaticHelper.ReadString(xmlReader);
                                break;
                            case "condFinalReason":
                                StaticParser.SetEnumFromString<BitReasonPulled>(StaticHelper.ReadString(xmlReader), out this.condFinalReason, out this.condFinalReasonSpecified, BitReasonPulled.unknown);
                                break;
                            case "drive":
                                this.drive = StaticHelper.ReadString(xmlReader);
                                break;
                            case "bitClass":
                                this.bitClass = StaticHelper.ReadString(xmlReader);
                                break;
                            case "customData":
                                if (this.customData == null)
                                    this.customData = new cs_customData();
                                this.customData.HandleXML(xmlReader.ReadSubtree(), isNew);
                                break;
                            default:
                                break;
                        }
                    }
                }
            }
        }
    }
}
