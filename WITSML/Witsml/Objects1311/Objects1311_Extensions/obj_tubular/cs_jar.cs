﻿ 

using System;
using System.Collections.Generic;
using System.Text;
using System.Xml;

namespace Witsml.Objects1311
{
    public partial class cs_jar : IHandleXML
    {
        public cs_jar()
        {
            this.jarAction = JarAction.unknown;
            this.typeJar = JarType.unknown;
        }
        /// <summary>
        /// Convert XML string data into cs_jar fields value
        /// </summary>
        /// <param name="xmlReader">reader that has cs_jar node</param>
        /// <param name="IsNew">Is the object from stratch(true) or from db/serialization(false)</param>
        public void HandleXML(XmlReader xmlReader, bool isNew)
        {
            using (xmlReader)
            {
                while (xmlReader.Read())
                {
                    //start on depth 1 which are the childs that we want to check against
                    //check for starting tag and convert node value to its expected variable
                    if (xmlReader.Depth == 1 && xmlReader.IsStartElement() && !xmlReader.IsEmptyElement)
                    {
                        switch (xmlReader.Name)
                        {
                            case "forUpSet":
                                if (this.forUpSet == null)
                                    this.forUpSet = new forceMeasure();
                                this.forUpSet.HandleXML(xmlReader.ReadSubtree(), isNew);
                                break;
                            case "forDownSet":
                                if (this.forDownSet == null)
                                    this.forDownSet = new forceMeasure();
                                this.forDownSet.HandleXML(xmlReader.ReadSubtree(), isNew);
                                break;
                            case "forUpTrip":
                                if (this.forUpTrip == null)
                                    this.forUpTrip = new forceMeasure();
                                this.forUpTrip.HandleXML(xmlReader.ReadSubtree(), isNew);
                                break;
                            case "forDownTrip":
                                if (this.forDownTrip == null)
                                    this.forDownTrip = new forceMeasure();
                                this.forDownTrip.HandleXML(xmlReader.ReadSubtree(), isNew);
                                break;
                            case "forPmpOpen":
                                if (this.forPmpOpen == null)
                                    this.forPmpOpen = new forceMeasure();
                                this.forPmpOpen.HandleXML(xmlReader.ReadSubtree(), isNew);
                                break;
                            case "forSealFric":
                                if (this.forSealFric == null)
                                    this.forSealFric = new forceMeasure();
                                this.forSealFric.HandleXML(xmlReader.ReadSubtree(), isNew);
                                break;
                            case "typeJar":
                                StaticParser.SetEnumFromString<JarType>(StaticHelper.ReadString(xmlReader), out this.typeJar, out this.typeJarSpecified, JarType.unknown);
                                break;
                            case "jarAction":
                                StaticParser.SetEnumFromString<JarAction>(StaticHelper.ReadString(xmlReader), out this.jarAction, out this.jarActionSpecified, JarAction.unknown);
                                break;
                            case "customData":
                                if (this.customData == null)
                                    this.customData = new cs_customData();
                                this.customData.HandleXML(xmlReader.ReadSubtree(), isNew);
                                break;
                            default:
                                break;
                        }
                    }
                }
            }
        }
    }
}
