﻿ 

using System;
using System.Collections.Generic;
using System.Text;
using System.Xml;

namespace Witsml.Objects1311
{
    public partial class cs_rotarySteerableTool : IHandleXML
    {
        public cs_rotarySteerableTool()
        {
            this.deflectionMethod = DeflectionMethod.unknown;
        }
        /// <summary>
        /// Convert XML string data into cs_rotarySteerableTool value
        /// </summary>
        /// <param name="xmlReader">reader that has cs_rotarySteerableToolnode</param>
        /// <param name="IsNew">Is the object from stratch(true) or from db/serialization(false)</param>
        public void HandleXML(XmlReader xmlReader, bool isNew)
        {
            bool isSensorNew = this.sensor == null;

            //temporary list for collection manipulation
            List<cs_sensor> listSensor = isSensorNew ? new List<cs_sensor>() : new List<cs_sensor>(this.sensor);

            using (xmlReader)
            {
                while (xmlReader.Read())
                {
                    //start on depth 1 which are the childs that we want to check against
                    //check for starting tag and convert node value to its expected variable
                    if (xmlReader.Depth == 1 && xmlReader.IsStartElement() && !xmlReader.IsEmptyElement)
                    {
                        switch (xmlReader.Name)
                        {
                            case "deflectionMethod":
                                this.deflectionMethod = StaticParser.ParseEnumFromString<DeflectionMethod>(StaticHelper.ReadString(xmlReader), DeflectionMethod.unknown);
                                break;
                            case "bendAngle":
                                //related with item
                                planeAngleMeasure planeObj = new planeAngleMeasure();
                                planeObj.HandleXML(xmlReader.ReadSubtree(), isNew);
                                this.Item = planeObj;
                                break;
                            case "bendOffset":
                                //related with item
                                lengthMeasure lengthObj = new lengthMeasure();
                                lengthObj.HandleXML(xmlReader.ReadSubtree(), isNew);
                                this.Item = lengthObj;
                                break;
                            case "holeSizeMn":
                                if (this.holeSizeMn == null)
                                    this.holeSizeMn = new lengthMeasure();
                                this.holeSizeMn.HandleXML(xmlReader.ReadSubtree(), isNew);
                                break;
                            case "holeSizeMx":
                                if (this.holeSizeMx == null)
                                    this.holeSizeMx = new lengthMeasure();
                                this.holeSizeMx.HandleXML(xmlReader.ReadSubtree(), isNew);
                                break;
                            case "wobMx":
                                if (this.wobMx == null)
                                    this.wobMx = new forceMeasure();
                                this.wobMx.HandleXML(xmlReader.ReadSubtree(), isNew);
                                break;
                            case "operatingSpeed":
                                if (this.operatingSpeed == null)
                                    this.operatingSpeed = new anglePerTimeMeasure();
                                this.operatingSpeed.HandleXML(xmlReader.ReadSubtree(), isNew);
                                break;
                            case "speedMx":
                                if (this.speedMx == null)
                                    this.speedMx = new anglePerTimeMeasure();
                                this.speedMx.HandleXML(xmlReader.ReadSubtree(), isNew);
                                break;
                            case "flowRateMn":
                                if (this.flowRateMn == null)
                                    this.flowRateMn = new volumeFlowRateMeasure();
                                this.flowRateMn.HandleXML(xmlReader.ReadSubtree(), isNew);
                                break;
                            case "flowRateMx":
                                if (this.flowRateMx == null)
                                    this.flowRateMx = new volumeFlowRateMeasure();
                                this.flowRateMx.HandleXML(xmlReader.ReadSubtree(), isNew);
                                break;
                            case "downLinkFlowRateMn":
                                if (this.downLinkFlowRateMn == null)
                                    this.downLinkFlowRateMn = new volumeFlowRateMeasure();
                                this.downLinkFlowRateMn.HandleXML(xmlReader.ReadSubtree(), isNew);
                                break;
                            case "downLinkFlowRateMx":
                                if (this.downLinkFlowRateMx == null)
                                    this.downLinkFlowRateMx = new volumeFlowRateMeasure();
                                this.downLinkFlowRateMx.HandleXML(xmlReader.ReadSubtree(), isNew);
                                break;
                            case "pressLossFact":
                                this.pressLossFact = StaticParser.ParseDoubleFromString(StaticHelper.ReadString(xmlReader));
                                this.pressLossFactSpecified = true;
                                break;
                            case "padCount":
                                this.padCount = StaticParser.ParseShortFromString(StaticHelper.ReadString(xmlReader));
                                this.padCountSpecified = true;
                                break;
                            case "padLen":
                                if (this.padLen == null)
                                    this.padLen = new lengthMeasure();
                                this.padLen.HandleXML(xmlReader.ReadSubtree(), isNew);
                                break;
                            case "padWidth":
                                if (this.padWidth == null)
                                    this.padWidth = new lengthMeasure();
                                this.padWidth.HandleXML(xmlReader.ReadSubtree(), isNew);
                                break;
                            case "padOffset":
                                if (this.padOffset == null)
                                    this.padOffset = new lengthMeasure();
                                this.padOffset.HandleXML(xmlReader.ReadSubtree(), isNew);
                                break;
                            case "openPadOd":
                                if (this.openPadOd == null)
                                    this.openPadOd = new lengthMeasure();
                                this.openPadOd.HandleXML(xmlReader.ReadSubtree(), isNew);
                                break;
                            case "closePadOd":
                                if (this.closePadOd == null)
                                    this.closePadOd = new lengthMeasure();
                                this.closePadOd.HandleXML(xmlReader.ReadSubtree(), isNew);
                                break;
                            case "sensor":
                                //collection
                                StaticHelper.AddUpdateWithUid<cs_sensor>(xmlReader, isSensorNew, ref listSensor);
                                break;
                            case "customData":
                                if (this.customData == null)
                                    this.customData = new cs_customData();
                                this.customData.HandleXML(xmlReader.ReadSubtree(), isNew);
                                break;
                            default:
                                break;
                        }
                    }
                }
                //assing list to array
                this.sensor = listSensor.ToArray();
            }
        }
    }
}
