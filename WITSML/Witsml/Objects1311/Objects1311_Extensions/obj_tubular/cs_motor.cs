﻿ 

using System;
using System.Collections.Generic;
using System.Text;
using System.Xml;

namespace Witsml.Objects1311
{
    public partial class cs_motor : IHandleXML
    {
        public cs_motor()
        {
            this.typeBearing = BearingType.unknown;
        }
        /// <summary>
        /// Convert XML string data into cs_motor fields value
        /// </summary>
        /// <param name="xmlReader">reader that has cs_motor node</param>
        /// <param name="IsNew">Is the object from stratch(true) or from db/serialization(false)</param>
        public void HandleXML(XmlReader xmlReader, bool isNew)
        {
            using (xmlReader)
            {
                while (xmlReader.Read())
                {
                    //start on depth 1 which are the childs that we want to check against
                    //check for starting tag and convert node value to its expected variable
                    if (xmlReader.Depth == 1 && xmlReader.IsStartElement() && !xmlReader.IsEmptyElement)
                    {
                        switch (xmlReader.Name)
                        {
                            case "offsetTool":
                                if (this.offsetTool == null)
                                    this.offsetTool = new lengthMeasure();
                                this.offsetTool.HandleXML(xmlReader.ReadSubtree(), isNew);
                                break;
                            case "presLossFact":
                                this.presLossFact = StaticParser.ParseDoubleFromString(StaticHelper.ReadString(xmlReader));
                                this.presLossFactSpecified = true;
                                break;
                            case "flowrateMn":
                                if (this.flowrateMn == null)
                                    this.flowrateMn = new volumeFlowRateMeasure();
                                this.flowrateMn.HandleXML(xmlReader.ReadSubtree(), isNew);
                                break;
                            case "flowrateMx":
                                if (this.flowrateMx == null)
                                    this.flowrateMx = new volumeFlowRateMeasure();
                                this.flowrateMx.HandleXML(xmlReader.ReadSubtree(), isNew);
                                break;
                            case "diaRotorNozzle":
                                if (this.diaRotorNozzle == null)
                                    this.diaRotorNozzle = new lengthMeasure();
                                this.diaRotorNozzle.HandleXML(xmlReader.ReadSubtree(), isNew);
                                break;
                            case "clearanceBearBox":
                                if (this.clearanceBearBox == null)
                                    this.clearanceBearBox = new lengthMeasure();
                                this.clearanceBearBox.HandleXML(xmlReader.ReadSubtree(), isNew);
                                break;
                            case "lobesRotor":
                                this.lobesRotor = StaticParser.ParseShortFromString(StaticHelper.ReadString(xmlReader));
                                this.lobesRotorSpecified = true;
                                break;
                            case "lobesStator":
                                this.lobesStator = StaticParser.ParseShortFromString(StaticHelper.ReadString(xmlReader));
                                this.lobesStatorSpecified = true;
                                break;
                            case "typeBearing":
                                StaticParser.SetEnumFromString<BearingType>(StaticHelper.ReadString(xmlReader), out this.typeBearing, out this.typeBearingSpecified, BearingType.unknown);
                                break;
                            case "tempOpMx":
                                if (this.tempOpMx == null)
                                    this.tempOpMx = new thermodynamicTemperatureMeasure();
                                this.tempOpMx.HandleXML(xmlReader.ReadSubtree(), isNew);
                                break;
                            case "rotorCatcher":
                                this.rotorCatcher = StaticParser.ParseBooleanFromString(StaticHelper.ReadString(xmlReader));
                                this.rotorCatcherSpecified = true;
                                break;
                            case "dumpValve":
                                this.dumpValve = StaticParser.ParseBooleanFromString(StaticHelper.ReadString(xmlReader));
                                this.dumpValveSpecified = true;
                                break;
                            case "diaNozzle":
                                if (this.diaNozzle == null)
                                    this.diaNozzle = new lengthMeasure();
                                this.diaNozzle.HandleXML(xmlReader.ReadSubtree(), isNew);
                                break;
                            case "rotatable":
                                this.rotatable = StaticParser.ParseBooleanFromString(StaticHelper.ReadString(xmlReader));
                                this.rotatableSpecified = true;
                                break;
                            case "bendSettingsMn":
                                if (this.bendSettingsMn == null)
                                    this.bendSettingsMn = new planeAngleMeasure();
                                this.bendSettingsMn.HandleXML(xmlReader.ReadSubtree(), isNew);
                                break;
                            case "bendSettingsMx":
                                if (this.bendSettingsMx == null)
                                    this.bendSettingsMx = new planeAngleMeasure();
                                this.bendSettingsMx.HandleXML(xmlReader.ReadSubtree(), isNew);
                                break;
                            case "customData":
                                if (this.customData == null)
                                    this.customData = new cs_customData();
                                this.customData.HandleXML(xmlReader.ReadSubtree(), isNew);
                                break;
                            default:
                                break;
                        }
                    }
                }
            }
        }
    }
}
