﻿ 

using System;
using System.Collections.Generic;
using System.Text;
using System.Xml;

namespace Witsml.Objects1311
{
    public partial class cs_connection : IHandleXML, IHasID
    {
        //interface for generic helper
        [System.Xml.Serialization.XmlIgnore]
        public string UID
        {
            get { return this.uid; }
        }
        public cs_connection()
        {
            this.position = ConnectionPosition.unknown;
        }
        /// <summary>
        /// Convert XML string data into cs_connection fields value
        /// </summary>
        /// <param name="xmlReader">reader that has cs_connection node</param>
        /// <param name="IsNew">Is the object from stratch(true) or from db/serialization(false)</param>
        public void HandleXML(XmlReader xmlReader, bool isNew)
        {
            using (xmlReader)
            {
                xmlReader.MoveToContent();
                if (xmlReader.HasAttributes)
                {
                    if (xmlReader.MoveToAttribute("uid"))
                        StaticHelper.HandleUID(xmlReader.Value, "connection uid", out this.uid);
                    xmlReader.MoveToElement();
                }
                while (xmlReader.Read())
                {
                    //start on depth 1 which are the childs that we want to check against
                    //check for starting tag and convert node value to its expected variable
                    if (xmlReader.Depth == 1 && xmlReader.IsStartElement() && !xmlReader.IsEmptyElement)
                    {
                        switch (xmlReader.Name)
                        {
                            case "id":
                                if (this.id == null)
                                    this.id = new lengthMeasure();
                                this.id.HandleXML(xmlReader.ReadSubtree(), isNew);
                                break;
                            case "od":
                                if (this.od == null)
                                    this.od = new lengthMeasure();
                                this.od.HandleXML(xmlReader.ReadSubtree(), isNew);
                                break;
                            case "len":
                                if (this.len == null)
                                    this.len = new lengthMeasure();
                                this.len.HandleXML(xmlReader.ReadSubtree(), isNew);
                                break;
                            case "typeThread":
                                this.typeThread = StaticHelper.ReadString(xmlReader);
                                break;
                            case "sizeThread":
                                if (this.sizeThread == null)
                                    this.sizeThread = new lengthMeasure();
                                this.sizeThread.HandleXML(xmlReader.ReadSubtree(), isNew);
                                break;
                            case "tensYield":
                                if (this.tensYield == null)
                                    this.tensYield = new pressureMeasure();
                                this.tensYield.HandleXML(xmlReader.ReadSubtree(), isNew);
                                break;
                            case "tqYield":
                                if (this.tqYield == null)
                                    this.tqYield = new pressureMeasure();
                                this.tqYield.HandleXML(xmlReader.ReadSubtree(), isNew);
                                break;
                            case "position":
                                StaticParser.SetEnumFromString<ConnectionPosition>(StaticHelper.ReadString(xmlReader), out this.position, out this.positionSpecified, ConnectionPosition.unknown);
                                break;
                            case "criticalCrossSection":
                                if (this.criticalCrossSection == null)
                                    this.criticalCrossSection = new areaMeasure();
                                this.criticalCrossSection.HandleXML(xmlReader.ReadSubtree(), isNew);
                                break;
                            case "presLeak":
                                if (this.presLeak == null)
                                    this.presLeak = new pressureMeasure();
                                this.presLeak.HandleXML(xmlReader.ReadSubtree(), isNew);
                                break;
                            case "tqMakeup":
                                if (this.tqMakeup == null)
                                    this.tqMakeup = new momentOfForceMeasure();
                                this.tqMakeup.HandleXML(xmlReader.ReadSubtree(), isNew);
                                break;
                            case "customData":
                                if (this.customData == null)
                                    this.customData = new cs_customData();
                                this.customData.HandleXML(xmlReader.ReadSubtree(), isNew);
                                break;
                            default:
                                break;
                        }
                    }
                }
            }
        }
    }
}
