﻿using System;
using System.Collections.Generic;
using System.Text;
using System.IO;
using System.Xml;
using System.Xml.Serialization;
using System.Linq;
using Witsml.Common.Exceptions;

namespace Witsml.Objects1311
{
    [XmlRoot("changeLog", Namespace = "http://www.witsml.org/schemas/131", IsNullable = false)]
    partial class obj_changeLog : WITSMLObject
    {
        //TODO: Added a property named dummy

        public override void Decode(ref string xmlIn)
        {
            //used to determine add or update
            try
            {
                //Consume XML string and convert node value to variable based on node name
                using (StringReader stringReader = new StringReader(xmlIn))
                {
                    XmlReaderSettings setting = new XmlReaderSettings();
                    setting.ConformanceLevel = ConformanceLevel.Document;
                    setting.IgnoreComments = true;
                    setting.IgnoreWhitespace = true;
                    setting.IgnoreProcessingInstructions = true;
                    using (XmlReader xmlReader = XmlReader.Create(stringReader, setting))
                    {
                        //determine add or update operation
                        bool isNew;
                        if (this.uid == string.Empty)
                            isNew = true;
                        else isNew = false;

                        this.HandleXML(xmlReader, isNew);
                    }
                }
            }
            catch (XmlException ex)
            {
                throw new Exceptions.UnableToDecodeException(ex.Message, WITSMLReturnCode.WITSMLDocumentError, ex.InnerException);
            }
            catch (NullReferenceException ex)
            {
                throw new Exceptions.UnableToDecodeException(ex.Message, WITSMLReturnCode.WITSMLEmptyDocument, ex.InnerException);
            }
            catch (Exceptions.UnableToDecodeException ex)
            {
                throw ex;
            }
            catch (Exception ex)
            {
                throw new Exceptions.UnableToDecodeException(ex.Message, WITSMLReturnCode.WITSMLUnkownCause, ex.InnerException);
            }
        }

        public override string ValidateGetPath(string newUid)
        {
            StringBuilder path = new StringBuilder(this.GetParentPath());
            path.Append(@"\" + this.uid);

            return path.ToString();
        }

        public override string GetParentPath()
        {
            StringBuilder path = new StringBuilder();
            if (!String.IsNullOrEmpty(this.uidWell))
            {
                path.Append(@"\" + this.uidWell);
            }

            if (!String.IsNullOrEmpty(this.uidWellbore))
            {
                path.Append(@"\" + this.uidWellbore);
            }

            return path.ToString();
        }

        // NOTE: these are done with functions, instead of property access, so as not to mess around with the auto-generated / serialized classes.
        public override string Get_uid()
        {
            return this.uid;
        }

        public override string Get_uidWell()
        {
            return this.uidWell;
        }

        public override string Get_uidWellbore()
        {
            return this.uidWellbore;
        }

        public override string Get_name()
        {
            return this.nameObject;
        }

        public override string Get_nameWell()
        {
            return this.nameWell;
        }

        public override string Get_nameWellbore()
        {
            return this.nameWellbore;
        }

        public override ICommonData Get_commonData()
        {
            if (this.commonData == null)
            {
                this.commonData = new cs_commonData();
            }
            return (ICommonData)this.commonData;
        }

        public override ICustomData Get_customData()
        {
            if (this.customData == null)
            {
                this.customData = new cs_customData();
            }
            return (ICustomData)this.customData;
        }

        public void HandleXML(XmlReader xmlReader, bool isNew)
        {

        }

        /// <summary>
        /// Serialize Trajectory include with Trajectory Station.XML Declaration and prefix not included
        /// </summary>
        /// <returns>serialize result without xml declaration</returns>
        public string SerializeWithDummy()
        {
            XmlAttributes xmlAtts = new XmlAttributes();
            xmlAtts.XmlIgnore = false;

            //specify override for trajectoryStation which accept ignore attribute
            XmlAttributeOverrides xmlAttsOverrides = new XmlAttributeOverrides();
            xmlAttsOverrides.Add(typeof(obj_changeLog), "dummy", xmlAtts);

            //do serialize with override parameter
            return this.Serialize(xmlAttsOverrides, SerializerType.ChangeLogWithDummy);
        }
    }
}
