﻿ 

using System;
using System.Collections.Generic;
using System.IO;
using System.Text;
using System.Xml;
using Witsml.Common.Exceptions;
using Witsml.Exceptions;


namespace Witsml.Objects1311
{
    [System.Xml.Serialization.XmlRootAttribute("risk", Namespace = "http://www.witsml.org/schemas/131", IsNullable = false)]
    public partial class obj_risk : WITSMLObject, IHandleXML
    {
        public obj_risk()
        {
            this.type = RiskType.unknown;
            this.category = RiskCategory.unknown;
            this.subCategory = RiskSubCategory.unknown;
        }

        /// <summary>
        /// Decode XML string into obj_risk
        /// </summary>
        /// <param name="xmlIn">XML string</param>
        public override void Decode(ref string xmlIn)
        {
            try
            {
                //Consume XML string and convert node value to variable based on node name
                using (StringReader stringReader = new StringReader(xmlIn))
                {
                    XmlReaderSettings setting = new XmlReaderSettings();
                    setting.ConformanceLevel = ConformanceLevel.Document;
                    setting.IgnoreComments = true;
                    setting.IgnoreWhitespace = true;
                    setting.IgnoreProcessingInstructions = true;
                    using (XmlReader xmlReader = XmlReader.Create(stringReader, setting))
                    {
                        //determine add or update operation
                        bool isNew;
                        if (this.uid == string.Empty)
                            isNew = true;
                        else isNew = false;

                        this.HandleXML(xmlReader, isNew);
                    }
                }
            }
            catch (XmlException ex)
            {
                throw new Exceptions.UnableToDecodeException(ex.Message, WITSMLReturnCode.WITSMLDocumentError, ex.InnerException);
            }
            catch (NullReferenceException ex)
            {
                throw new Exceptions.UnableToDecodeException(ex.Message, WITSMLReturnCode.WITSMLEmptyDocument, ex.InnerException);
            }
            catch (Exceptions.UnableToDecodeException ex)
            {
                throw ex;
            }
            catch (Exception ex)
            {
                throw new Exceptions.UnableToDecodeException(ex.Message, WITSMLReturnCode.WITSMLUnkownCause, ex.InnerException);
            }
        }

        /// <summary>
        /// Convert XML string data into obj_risk fields value
        /// </summary>
        /// <param name="xmlReader">reader that has obj_risk node</param>
        /// <param name="IsNew">Is the object from stratch(true) or from db/serialization(false)</param>
        public void HandleXML(XmlReader xmlReader, bool isNew)
        {
            //temporary list for data manipulations
            List<RiskAffectedPersonnel> listRiskAffect = this.affectedPersonnel == null ? new List<RiskAffectedPersonnel>() : new List<RiskAffectedPersonnel>(this.affectedPersonnel);
            List<string> listMitigation = this.mitigation == null ? new List<string>() : new List<string>(this.mitigation);

            using (xmlReader)
            {
                //assign uid for the new risk
                xmlReader.MoveToContent();
                if (xmlReader.HasAttributes)
                {
                    if (xmlReader.MoveToAttribute("uidWell"))
                        StaticHelper.HandleUID(xmlReader.Value, "risk uidWell", out this.uidWell);
                    if (xmlReader.MoveToAttribute("uidWellbore"))
                        StaticHelper.HandleUID(xmlReader.Value, "risk uidWellbore", out this.uidWellbore);
                    if (xmlReader.MoveToAttribute("uid"))
                        StaticHelper.HandleUID(xmlReader.Value, "risk uid", out this.uid);
                    xmlReader.MoveToElement();
                }
                while (xmlReader.Read())
                {
                    //only take child node
                    if (xmlReader.Depth == 1 && xmlReader.IsStartElement() && !xmlReader.IsEmptyElement)
                    {
                        switch (xmlReader.Name)
                        {
                            case "nameWell":
                                this.nameWell = StaticHelper.ReadString(xmlReader);
                                break;
                            case "nameWellbore":
                                this.nameWellbore = StaticHelper.ReadString(xmlReader);
                                break;
                            case "name":
                                this.name = StaticHelper.ReadString(xmlReader);
                                break;
                            case "objectReference":
                                if (this.objectReference == null)
                                    this.objectReference = new refObjectString();
                                this.objectReference.ObjectName = "objectReference";
                                this.objectReference.HandleXML(xmlReader.ReadSubtree(), isNew);
                                break;
                            case "type":
                                this.type = StaticParser.ParseEnumFromString<RiskType>(StaticHelper.ReadString(xmlReader), RiskType.unknown);
                                break;
                            case "category":
                                this.category = StaticParser.ParseEnumFromString<RiskCategory>(StaticHelper.ReadString(xmlReader), RiskCategory.unknown);
                                break;
                            case "subCategory":
                                this.subCategory = StaticParser.ParseEnumFromString<RiskSubCategory>(StaticHelper.ReadString(xmlReader), RiskSubCategory.unknown);
                                this.subCategorySpecified = true;
                                break;
                            case "extendCategory":
                                this.extendCategory = StaticHelper.ReadString(xmlReader);
                                break;
                            case "affectedPersonnel":
                                //collection
                                RiskAffectedPersonnel riskAffectObj = StaticParser.ParseEnumFromString<RiskAffectedPersonnel>(StaticHelper.ReadString(xmlReader), RiskAffectedPersonnel.unknown);
                                if (!listRiskAffect.Contains(riskAffectObj))
                                    listRiskAffect.Add(riskAffectObj);
                                break;
                            case "dTimStart":
                                this.dTimStart = StaticParser.ParseDateTimeFromString(StaticHelper.ReadString(xmlReader));
                                this.dTimStartSpecified = true;
                                break;
                            case "dTimEnd":
                                this.dTimEnd = StaticParser.ParseDateTimeFromString(StaticHelper.ReadString(xmlReader));
                                this.dTimEndSpecified = true;
                                break;
                            case "mdHoleStart":
                                if (this.mdHoleStart == null)
                                    this.mdHoleStart = new measuredDepthCoord();
                                this.mdHoleStart.HandleXML(xmlReader.ReadSubtree(), isNew);
                                break;
                            case "mdHoleEnd":
                                if (this.mdHoleEnd == null)
                                    this.mdHoleEnd = new measuredDepthCoord();
                                this.mdHoleEnd.HandleXML(xmlReader.ReadSubtree(), isNew);
                                break;
                            case "tvdHoleStart":
                                if (this.tvdHoleStart == null)
                                    this.tvdHoleStart = new wellVerticalDepthCoord();
                                this.tvdHoleStart.HandleXML(xmlReader.ReadSubtree(), isNew);
                                break;
                            case "tvdHoleEnd":
                                if (this.tvdHoleEnd == null)
                                    this.tvdHoleEnd = new wellVerticalDepthCoord();
                                this.tvdHoleEnd.HandleXML(xmlReader.ReadSubtree(), isNew);
                                break;
                            case "mdBitStart":
                                if (this.mdBitStart == null)
                                    this.mdBitStart = new measuredDepthCoord();
                                this.mdBitStart.HandleXML(xmlReader.ReadSubtree(), isNew);
                                break;
                            case "mdBitEnd":
                                if (this.mdBitEnd == null)
                                    this.mdBitEnd = new measuredDepthCoord();
                                this.mdBitEnd.HandleXML(xmlReader.ReadSubtree(), isNew);
                                break;
                            case "diaHole":
                                if (this.diaHole == null)
                                    this.diaHole = new lengthMeasure();
                                this.diaHole.HandleXML(xmlReader.ReadSubtree(), isNew);
                                break;
                            case "severityLevel":
                                this.severityLevel = StaticParser.ParseShortFromString(StaticHelper.ReadString(xmlReader));
                                this.severityLevelSpecified = true;
                                break;
                            case "probabilityLevel":
                                this.probabilityLevel = StaticParser.ParseShortFromString(StaticHelper.ReadString(xmlReader));
                                this.probabilityLevelSpecified = true;
                                break;
                            case "summary":
                                this.summary = StaticHelper.ReadString(xmlReader);
                                break;
                            case "details":
                                this.details = StaticHelper.ReadString(xmlReader);
                                break;
                            case "identification":
                                this.identification = StaticHelper.ReadString(xmlReader);
                                break;
                            case "contingency":
                                this.contingency = StaticHelper.ReadString(xmlReader);
                                break;
                            case "mitigation":
                                //collection string
                                listMitigation.Add(StaticHelper.ReadString(xmlReader));
                                break;
                            case "commonData":
                                if (this.commonData == null)
                                    this.commonData = new cs_commonData();
                                this.commonData.HandleXML(xmlReader.ReadSubtree(), isNew);
                                break;
                            case "customData":
                                if (this.customData == null)
                                    this.customData = new cs_customData();
                                this.customData.HandleXML(xmlReader.ReadSubtree(), isNew);
                                break;
                            default:
                                break;
                        }
                    }
                }
                //assign list to collection
                this.affectedPersonnel = listRiskAffect.ToArray();
                this.mitigation = listMitigation.ToArray();
            }
        }

        public override string ValidateGetPath(string newUid)
        {
            // Fix uid if the Object dosn't have one.
            if (!IsValidUid(this.uid))
            {
                this.uid = newUid;
                // TODO: Add some loging that we created a new UID for the object maybe.
            }

            StringBuilder path = new StringBuilder(this.GetParentPath());
            path.Append(@"\");
            path.Append(this.uid);
            return path.ToString();
        }

        public override string GetParentPath()
        {
            if (!IsValidUid(this.uidWell) || !IsValidUid(this.uidWellbore))
            {
                throw new UnableToDecodeException("One or more Parent UID's are invalid", WITSMLReturnCode.WITSMLInvalidParentUID);
                //TODO: Log the issue
            }

            StringBuilder path = new StringBuilder(@"\");
            path.Append(this.uidWell);
            path.Append(@"\");
            path.Append(this.uidWellbore);
            return path.ToString();
        }

        // NOTE: these are done with functions, instead of property access, so as not to mess around with the auto-generated / serialized classes.
        public override string Get_uid()
        {
            return this.uid;
        }

        public override string Get_uidWell()
        {
            return this.uidWell;
        }

        public override string Get_uidWellbore()
        {
            return this.uidWellbore;
        }

        public override string Get_name()
        {
            return this.name;
        }

        public override string Get_nameWell()
        {
            return this.nameWell;
        }

        public override string Get_nameWellbore()
        {
            return this.nameWellbore;
        }

        public override ICommonData Get_commonData()
        {
            if (this.commonData == null)
            {
                this.commonData = new cs_commonData();
            }
            return (ICommonData)this.commonData;
        }
        public override ICustomData Get_customData()
        {
            if (this.customData == null)
            {
                this.customData = new cs_customData();
            }
            return (ICustomData)this.customData;
        }
    }
}
