﻿using System.Collections.Generic;
using System.Linq;
using System.Xml;
using System.IO;

namespace Witsml.Objects1311
{
    public partial class obj_attachments : WITSMLDocument
    {

        public override WITSMLObject GetSingleObject(int Item)
        {
            return this.attachment[Item];
        }

        public override int Decode(ref string xmlIn)
        {
            //return value
            //number of changeLog inside xmlIn
            int countOfAttachment = 0;

            //check whether collection is from stratch(isNew = true) or from deserialization(isNew=false)
            bool isNew = this.attachment == null;

            //create temporary list to help with data manipulation
            List<obj_attachment> listAttachment = isNew ? new List<obj_attachment>() : new List<obj_attachment>(this.attachment);
            using (StringReader stringReader = new StringReader(xmlIn))
            {
                XmlReaderSettings setting = new XmlReaderSettings();
                setting.ConformanceLevel = ConformanceLevel.Document;
                setting.IgnoreComments = true;
                setting.IgnoreWhitespace = true;
                setting.IgnoreProcessingInstructions = true;
                using (XmlReader xmlReader = XmlReader.Create(stringReader, setting))
                {
                    while (xmlReader.Read())
                    {
                        //assign version
                        xmlReader.MoveToContent();
                        if (xmlReader.MoveToAttribute("version"))
                            this.version = xmlReader.Value;
                        else
                            this.version = "1.3.1.1";//no version specified,set it to 1.4.1

                        //pre-checking on whether node is the correct child based on depth,start tag and not empty
                        if (xmlReader.Depth == 1 && xmlReader.IsStartElement() && !xmlReader.IsEmptyElement)
                        {
                            switch (xmlReader.Name)
                            {
                                case "documentInfo":
                                    if (this.documentInfo == null)
                                        this.documentInfo = new cs_documentInfo();
                                    this.documentInfo.HandleXML(xmlReader.ReadSubtree(), isNew);
                                    break;
                                case "attachment":
                                    obj_attachment attachment;
                                    //foreign key(well id,wellbore id) and primary key(wellbore id) to identify existing changeLog
                                    string currentUIDWell = string.Empty;
                                    string currentUIDWellbore = string.Empty;
                                    string currentUID = string.Empty;

                                    //Update changeLog
                                    if (!isNew)
                                    {
                                        //check for uid attribute and assign it to temporary string for searching in collection
                                        if (xmlReader.HasAttributes)
                                        {
                                            if (xmlReader.MoveToAttribute("uidWell"))
                                            {
                                                currentUIDWell = xmlReader.Value;
                                            }
                                            if (xmlReader.MoveToAttribute("uidWellbore"))
                                            {
                                                currentUIDWellbore = xmlReader.Value;
                                            }
                                            if (xmlReader.MoveToAttribute("uid"))
                                            {
                                                currentUID = xmlReader.Value;
                                            }
                                            xmlReader.MoveToElement();
                                        }
                                        //look for changeLog based on its UID and update the changeLog with xml string
                                        if (currentUID != string.Empty)
                                        {
                                            attachment = listAttachment.FirstOrDefault(
                                                delegate(obj_attachment objAttachment)
                                                { if (objAttachment.uidWell == currentUIDWell && objAttachment.uidWellbore == currentUIDWellbore && objAttachment.uid == currentUID)return true; else return false; });
                                            if (attachment != null)
                                            {
                                                attachment.HandleXML(xmlReader.ReadSubtree(), isNew);
                                                countOfAttachment++;
                                                continue;
                                            }
                                            else
                                            {
                                                //Update can't process changeLog without match
                                                continue;
                                            }
                                        }
                                        else
                                        {
                                            //Update can't process changeLog without uid
                                            continue;
                                        }
                                    }

                                    //Add New Attachment 
                                    attachment = new obj_attachment();
                                    attachment.HandleXML(xmlReader.ReadSubtree(), true);
                                    listAttachment.Add(attachment);
                                    countOfAttachment++;
                                    break;
                                default:
                                    break;
                            }
                        }
                    }
                }
            }
            //assign list to wellbore collection
            this.attachment = listAttachment.ToArray();
            return countOfAttachment;
        }
    }
}
