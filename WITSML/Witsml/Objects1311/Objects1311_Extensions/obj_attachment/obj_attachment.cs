﻿using System;
using System.Collections.Generic;
using System.IO;
using System.Text;
using System.Xml;
using System.Xml.Serialization;
using Witsml.Common.Exceptions;
using Witsml.Exceptions;

namespace Witsml.Objects1311
{
    [XmlRoot("attachment", Namespace = "http://www.witsml.org/schemas/131", IsNullable = false)]
    public partial class obj_attachment : WITSMLObject, IHandleXML
    {
        public obj_attachment()
        {
            this.version = WITSMLVersion.V131;
        }

        [XmlIgnore()]
        public string dummy;

        /// <summary>
        /// Decode XML string into obj_attachment
        /// </summary>
        /// <param name="xmlIn">XML string</param>
        public override void Decode(ref string xmlIn)
        {
            try
            {
                //Consume XML string and convert node value to variable based on node name
                using (StringReader stringReader = new StringReader(xmlIn))
                {
                    XmlReaderSettings setting = new XmlReaderSettings();
                    setting.ConformanceLevel = ConformanceLevel.Document;
                    setting.IgnoreComments = true;
                    setting.IgnoreWhitespace = true;
                    setting.IgnoreProcessingInstructions = true;
                    using (XmlReader xmlReader = XmlReader.Create(stringReader, setting))
                    {
                        //determine add or update operation
                        bool isNew;
                        if (this.uid == string.Empty)
                            isNew = true;
                        else isNew = false;

                        this.HandleXML(xmlReader, isNew);
                    }
                }
            }
            catch (XmlException ex)
            {
                throw new Exceptions.UnableToDecodeException(ex.Message, WITSMLReturnCode.WITSMLDocumentError, ex.InnerException);
            }
            catch (NullReferenceException ex)
            {
                throw new Exceptions.UnableToDecodeException(ex.Message, WITSMLReturnCode.WITSMLEmptyDocument, ex.InnerException);
            }
            catch (Exceptions.UnableToDecodeException ex)
            {
                throw ex;
            }
            catch (Exception ex)
            {
                throw new Exceptions.UnableToDecodeException(ex.Message, WITSMLReturnCode.WITSMLUnkownCause, ex.InnerException);
            }
        }

        /// <summary>
        /// Convert XML string data into log fields value
        /// </summary>
        /// <param name="xmlReader">reader that has log node</param>
        /// <param name="IsNew">Is the object from stratch(true) or from db/serialization(false)</param>
        public void HandleXML(XmlReader xmlReader, bool isNew)
        {
            //condition for add or update
            bool isParamNew = this.param == null;

            //temporary list for data manipulations
            List<indexedObject> listParams = isParamNew ? new List<indexedObject>() : new List<indexedObject>(this.param);

            using (xmlReader)
            {
                //assign uid for the new well
                xmlReader.MoveToContent();
                if (xmlReader.HasAttributes)
                {
                    if (xmlReader.MoveToAttribute("uidWell"))
                        StaticHelper.HandleUID(xmlReader.Value, "attachment uidWell", out this.uidWellField);
                    if (xmlReader.MoveToAttribute("uidWellbore"))
                        StaticHelper.HandleUID(xmlReader.Value, "attachment uidWellbore", out this.uidWellboreField);
                    if (xmlReader.MoveToAttribute("uidParentObject"))
                        StaticHelper.HandleUID(xmlReader.Value, "attachment uidParentObject", out this.uidWellboreField);
                    if (xmlReader.MoveToAttribute("uidObject"))
                        StaticHelper.HandleUID(xmlReader.Value, "attachment uidObject", out this.uidWellboreField);
                    if (xmlReader.MoveToAttribute("uid"))
                        StaticHelper.HandleUID(xmlReader.Value, "attachment uid", out this.uidField);
                    xmlReader.MoveToElement();
                }
                while (xmlReader.Read())
                {
                    //only take child node
                    if (xmlReader.Depth == 1 && xmlReader.IsStartElement() && !xmlReader.IsEmptyElement)
                    {
                        switch (xmlReader.Name)
                        {
                            case "nameWell":
                                this.nameWell = StaticHelper.ReadString(xmlReader);
                                break;
                            case "nameWellbore":
                                this.nameWellbore = StaticHelper.ReadString(xmlReader);
                                break;
                            case "name":
                                this.name = StaticHelper.ReadString(xmlReader);
                                break;
                            //---  Group Attachment.
                            case "objectReference":
                                if (objectReference == null)
                                {
                                    objectReference = new refObjectString();
                                }
                                objectReference.ObjectName = "objectReference";
                                objectReference.HandleXML(xmlReader.ReadSubtree(), isNew);
                                break;
                            case "subObjectReference":
                                if (subObjectReference == null)
                                {
                                    subObjectReference = new refObjectString();
                                }
                                subObjectReference.ObjectName = "subObjectReference";
                                subObjectReference.HandleXML(xmlReader.ReadSubtree(), isNew);
                                break;
                            case "md":
                                if (this.md == null)
                                    this.md = new measuredDepthCoord();
                                this.md.HandleXML(xmlReader.ReadSubtree(), isNew);
                                break;
                            case "mdBit":
                                if (this.mdBit == null)
                                    this.mdBit = new measuredDepthCoord();
                                this.mdBit.HandleXML(xmlReader.ReadSubtree(), isNew);
                                break;
                            case "param":
                                //collection
                                StaticHelper.AddUpdateIndexedObj(xmlReader, isParamNew, ref listParams);
                                break;
                            case "fileName":
                                this.fileName = StaticHelper.ReadString(xmlReader);
                                break;
                            case "description":
                                this.description = StaticHelper.ReadString(xmlReader);
                                break;
                            case "fileType":
                                this.fileType = StaticHelper.ReadString(xmlReader);
                                break;
                            case "content":
                                this.content = StaticHelper.ReadByte(xmlReader);
                                break;
                            //  --- End of Group Attachment.
                            case "commonData":
                                if (this.commonData == null)
                                    this.commonData = new cs_commonData();
                                this.commonData.HandleXML(xmlReader.ReadSubtree(), isNew);
                                break;
                            case "customData":
                                if (this.customData == null)
                                    this.customData = new cs_customData();
                                this.customData.HandleXML(xmlReader.ReadSubtree(), isNew);
                                break;
                            default:
                                break;
                        }
                    }
                }
            }

            //assign list to collection
            param = listParams.ToArray();
        }

        public override string ValidateGetPath(string newUid)
        {
            // Fix uid if the Object dosn't have one.
            if (!IsValidUid(this.uid))
            {
                this.uid = newUid;
                // TODO: Add some loging that we created a new UID for the object maybe.
            }

            StringBuilder path = new StringBuilder(this.GetParentPath());
            path.Append(@"\");
            path.Append(this.uid);
            return path.ToString();
        }

        public override string GetParentPath()
        {
            if (!IsValidUid(this.uidWell) || !IsValidUid(this.uidWellbore))
            {
                throw new UnableToDecodeException("One or more Parent UID's are invalid", WITSMLReturnCode.WITSMLInvalidParentUID);
                //TODO: Log the issue
            }

            StringBuilder path = new StringBuilder(@"\");
            path.Append(this.uidWell);
            path.Append(@"\");
            path.Append(this.uidWellbore);
            return path.ToString();
        }

        // NOTE: these are done with functions, instead of property access, so as not to mess around with the auto-generated / serialized classes.
        public override string Get_uid()
        {
            return this.uid;
        }

        public override string Get_uidWell()
        {
            return this.uidWell;
        }

        public override string Get_uidWellbore()
        {
            return this.uidWellbore;
        }

        public override string Get_name()
        {
            return this.name;
        }

        public override string Get_nameWell()
        {
            return this.nameWell;
        }

        public override string Get_nameWellbore()
        {
            return this.nameWellbore;
        }

        public override ICommonData Get_commonData()
        {
            if (this.commonData == null)
            {
                this.commonData = new cs_commonData();
            }
            return (ICommonData)this.commonData;
        }
        public override ICustomData Get_customData()
        {
            if (this.customData == null)
            {
                this.customData = new cs_customData();
            }
            return (ICustomData)this.customData;
        }

        /// <summary>
        /// Serialize Trajectory include with Trajectory Station.XML Declaration and prefix not included
        /// </summary>
        /// <returns>serialize result without xml declaration</returns>
        //public string SerializeWithDummy()
        //{
        //    XmlAttributes xmlAtts = new XmlAttributes();
        //    xmlAtts.XmlIgnore = false;

        //    //specify override for trajectoryStation which accept ignore attribute
        //    XmlAttributeOverrides xmlAttsOverrides = new XmlAttributeOverrides();
        //    xmlAttsOverrides.Add(typeof(obj_changeLog), "dummy", xmlAtts);

        //    //do serialize with override parameter
        //    return this.Serialize(xmlAttsOverrides, SerializerType.ChangeLogWithDummy);
        //}
    }
}
