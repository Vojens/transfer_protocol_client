﻿ 

using System;
using System.Collections.Generic;
using System.Text;
using System.Xml;

namespace Witsml.Objects1311
{
    public partial class cs_surveySection : IHandleXML, IHasID
    {
        //interface for generic helper
        [System.Xml.Serialization.XmlIgnore]
        public string UID
        {
            get { return this.uid; }
        }
        public cs_surveySection()
        {
            this.itemState = ItemState.unknown;
        }
        /// <summary>
        /// Convert XML string data into cs_surveySection fields value
        /// </summary>
        /// <param name="xmlReader">reader that has cs_surveySection node</param>
        /// <param name="IsNew">Is the object from stratch(true) or from db/serialization(false)</param>
        public void HandleXML(XmlReader xmlReader, bool isNew)
        {
            using (xmlReader)
            {
                xmlReader.MoveToContent();
                if (xmlReader.HasAttributes)
                {
                    if (xmlReader.MoveToAttribute("uid"))
                        StaticHelper.HandleUID(xmlReader.Value, "surveySection uid", out this.uid);
                    xmlReader.MoveToElement();
                }
                while (xmlReader.Read())
                {
                    //start on depth 1 which are the childs that we want to check against
                    //check for starting tag and convert node value to its expected variable
                    if (xmlReader.Depth == 1 && xmlReader.IsStartElement() && !xmlReader.IsEmptyElement)
                    {
                        switch (xmlReader.Name)
                        {
                            case "sequence":
                                this.sequence = StaticParser.ParseShortFromString(StaticHelper.ReadString(xmlReader));
                                break;
                            case "name":
                                this.name = StaticHelper.ReadString(xmlReader);
                                break;
                            case "mdStart":
                                if (this.mdStart == null)
                                    this.mdStart = new measuredDepthCoord();
                                this.mdStart.HandleXML(xmlReader.ReadSubtree(), isNew);
                                break;
                            case "mdEnd":
                                if (this.mdEnd == null)
                                    this.mdEnd = new measuredDepthCoord();
                                this.mdEnd.HandleXML(xmlReader.ReadSubtree(), isNew);
                                break;
                            case "nameSurveyCompany":
                                this.nameSurveyCompany = StaticHelper.ReadString(xmlReader);
                                break;
                            case "nameTool":
                                this.nameTool = StaticHelper.ReadString(xmlReader);
                                break;
                            case "typeTool":
                                this.typeTool = StaticHelper.ReadString(xmlReader);
                                break;
                            case "modelError":
                                this.modelError = StaticHelper.ReadString(xmlReader);
                                break;
                            case "overwrite":
                                this.overwrite = StaticParser.ParseBooleanFromString(StaticHelper.ReadString(xmlReader));
                                this.overwriteSpecified = true;
                                break;
                            case "frequencyMx":
                                if (this.frequencyMx == null)
                                    this.frequencyMx = new lengthMeasure();
                                this.frequencyMx.HandleXML(xmlReader.ReadSubtree(), isNew);
                                break;
                            case "itemState":
                                StaticParser.SetEnumFromString<ItemState>(StaticHelper.ReadString(xmlReader), out this.itemState, out this.itemStateSpecified, ItemState.unknown);
                                break;
                            case "comments":
                                this.comments = StaticHelper.ReadString(xmlReader);
                                break;
                            default:
                                break;
                        }
                    }
                }
            }
        }
    }
}
