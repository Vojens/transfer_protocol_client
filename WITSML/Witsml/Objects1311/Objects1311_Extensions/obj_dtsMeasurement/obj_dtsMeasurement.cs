﻿ 

using System;
using System.Collections.Generic;
using System.IO;
using System.Text;
using System.Xml;
using Witsml.Common.Exceptions;
using Witsml.Exceptions;


namespace Witsml.Objects1311
{
    [System.Xml.Serialization.XmlRootAttribute("dtsMeasurement", Namespace = "http://www.witsml.org/schemas/131", IsNullable = false)]
    public partial class obj_dtsMeasurement : WITSMLObject, IHandleXML
    {
        public obj_dtsMeasurement()
        {
        }

        /// <summary>
        /// Decode XML string into obj_dtsMeasurement
        /// </summary>
        /// <param name="xmlIn">XML string</param>
        public override void Decode(ref string xmlIn)
        {
            try
            {
                //Consume XML string and convert node value to variable based on node name
                using (StringReader stringReader = new StringReader(xmlIn))
                {
                    XmlReaderSettings setting = new XmlReaderSettings();
                    setting.ConformanceLevel = ConformanceLevel.Document;
                    setting.IgnoreComments = true;
                    setting.IgnoreWhitespace = true;
                    setting.IgnoreProcessingInstructions = true;
                    using (XmlReader xmlReader = XmlReader.Create(stringReader, setting))
                    {
                        //determine add or update operation
                        bool isNew;
                        if (this.uid == string.Empty)
                            isNew = true;
                        else isNew = false;

                        this.HandleXML(xmlReader, isNew);
                    }
                }
            }
            catch (XmlException ex)
            {
                throw new Exceptions.UnableToDecodeException(ex.Message, WITSMLReturnCode.WITSMLDocumentError, ex.InnerException);
            }
            catch (NullReferenceException ex)
            {
                throw new Exceptions.UnableToDecodeException(ex.Message, WITSMLReturnCode.WITSMLEmptyDocument, ex.InnerException);
            }
            catch (Exceptions.UnableToDecodeException ex)
            {
                throw ex;
            }
            catch (Exception ex)
            {
                throw new Exceptions.UnableToDecodeException(ex.Message, WITSMLReturnCode.WITSMLUnkownCause, ex.InnerException);
            }
        }

        /// <summary>
        /// Convert XML string data into obj_fluidsReport fields value
        /// </summary>
        /// <param name="xmlReader">reader that has obj_fluidsReport node</param>
        /// <param name="IsNew">Is the object from stratch(true) or from db/serialization(false)</param>
        public void HandleXML(XmlReader xmlReader, bool isNew)
        {
            //condition for add or update
            bool isErrorReportNew = this.errorReport == null;

            //temporary list for data manipulations
            List<cs_errorReport> listErrorReport = isErrorReportNew ? new List<cs_errorReport>() : new List<cs_errorReport>(this.errorReport);

            using (xmlReader)
            {
                //assign uid for the new message
                xmlReader.MoveToContent();
                if (xmlReader.HasAttributes)
                {
                    if (xmlReader.MoveToAttribute("uidWell"))
                        StaticHelper.HandleUID(xmlReader.Value, "dtsMeasurement uidWell", out this.uidWell);
                    if (xmlReader.MoveToAttribute("uidWellbore"))
                        StaticHelper.HandleUID(xmlReader.Value, "dtsMeasurement uidWellbore", out this.uidWellbore);
                    if (xmlReader.MoveToAttribute("uid"))
                        StaticHelper.HandleUID(xmlReader.Value, "dtsMeasurement uid", out this.uid);
                    xmlReader.MoveToElement();
                }
                while (xmlReader.Read())
                {
                    //only take child node
                    if (xmlReader.Depth == 1 && xmlReader.IsStartElement() && !xmlReader.IsEmptyElement)
                    {
                        switch (xmlReader.Name)
                        {
                            case "nameWell":
                                this.nameWell = StaticHelper.ReadString(xmlReader);
                                break;
                            case "nameWellbore":
                                this.nameWellbore = StaticHelper.ReadString(xmlReader);
                                break;
                            case "name":
                                this.name = StaticHelper.ReadString(xmlReader);
                                break;
                            case "runDuration":
                                if (this.runDuration == null)
                                    this.runDuration = new timeMeasure();
                                this.runDuration.HandleXML(xmlReader.ReadSubtree(), isNew);
                                break;
                            case "installedSystemUsed":
                                if (this.installedSystemUsed == null)
                                    this.installedSystemUsed = new refNameString();
                                this.installedSystemUsed.HandleXML(xmlReader.ReadSubtree(), isNew);
                                break;
                            case "dataInWellLog":
                                if (this.dataInWellLog == null)
                                    this.dataInWellLog = new refNameString();
                                this.dataInWellLog.HandleXML(xmlReader.ReadSubtree(), isNew);
                                break;
                            case "connectedToFiber":
                                if (this.connectedToFiber == null)
                                    this.connectedToFiber = new refNameString();
                                this.connectedToFiber.HandleXML(xmlReader.ReadSubtree(), isNew);
                                break;
                            case "fiberEndConnected":
                                if (this.fiberEndConnected == null)
                                    this.fiberEndConnected = new refNameString();
                                this.fiberEndConnected.HandleXML(xmlReader.ReadSubtree(), isNew);
                                break;
                            case "fiberDefinedPoint":
                                if (this.fiberDefinedPoint == null)
                                    this.fiberDefinedPoint = new cs_singleInstalledPoint();
                                this.fiberDefinedPoint.HandleXML(xmlReader.ReadSubtree(), isNew);
                                break;
                            case "errorReport":
                                //collection
                                StaticHelper.AddUpdateWithUid<cs_errorReport>(xmlReader, isErrorReportNew, ref listErrorReport);
                                break;
                            case "commonData":
                                if (this.commonData == null)
                                    this.commonData = new cs_commonData();
                                this.commonData.HandleXML(xmlReader.ReadSubtree(), isNew);
                                break;
                            case "customData":
                                if (this.customData == null)
                                    this.customData = new cs_customData();
                                this.customData.HandleXML(xmlReader.ReadSubtree(), isNew);
                                break;
                            default:
                                break;
                        }
                    }
                }
                //assign list to collection
                this.errorReport = listErrorReport.ToArray();
            }
        }

        public override string ValidateGetPath(string newUid)
        {
            // Fix uid if the Object dosn't have one.
            if (!IsValidUid(this.uid))
            {
                this.uid = newUid;
                // TODO: Add some loging that we created a new UID for the object maybe.
            }

            StringBuilder path = new StringBuilder(this.GetParentPath());
            path.Append(@"\");
            path.Append(this.uid);
            return path.ToString();
        }

        public override string GetParentPath()
        {
            if (!IsValidUid(this.uidWell) || !IsValidUid(this.uidWellbore))
            {
                throw new UnableToDecodeException("One or more Parent UID's are invalid", WITSMLReturnCode.WITSMLInvalidParentUID);
                //TODO: Log the issue
            }

            StringBuilder path = new StringBuilder(@"\");
            path.Append(this.uidWell);
            path.Append(@"\");
            path.Append(this.uidWellbore);
            return path.ToString();
        }

        // NOTE: these are done with functions, instead of property access, so as not to mess around with the auto-generated / serialized classes.
        public override string Get_uid()
        {
            return this.uid;
        }

        public override string Get_uidWell()
        {
            return this.uidWell;
        }

        public override string Get_uidWellbore()
        {
            return this.uidWellbore;
        }

        public override string Get_name()
        {
            return this.name;
        }

        public override string Get_nameWell()
        {
            return this.nameWell;
        }

        public override string Get_nameWellbore()
        {
            return this.nameWellbore;
        }

        public override ICommonData Get_commonData()
        {
            if (this.commonData == null)
            {
                this.commonData = new cs_commonData();
            }
            return (ICommonData)this.commonData;
        }
        public override ICustomData Get_customData()
        {
            if (this.customData == null)
            {
                this.customData = new cs_customData();
            }
            return (ICustomData)this.customData;
        }
    }
}
