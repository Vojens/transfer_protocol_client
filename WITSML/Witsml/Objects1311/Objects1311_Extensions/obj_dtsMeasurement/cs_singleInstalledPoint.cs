﻿ 

using System;
using System.Collections.Generic;
using System.Text;
using System.Xml;

namespace Witsml.Objects1311
{
    public partial class cs_singleInstalledPoint : IHandleXML, IHasID
    {
        //interface for generic helper
        [System.Xml.Serialization.XmlIgnore]
        public string UID
        {
            get { return this.uid; }
        }
        public cs_singleInstalledPoint()
        {
            this.type = InstalledFiberPoint.unknown;
        }
        /// <summary>
        /// Convert XML string data into cs_singleInstalledPoint fields value
        /// </summary>
        /// <param name="xmlReader">reader that has cs_cementAdditive node</param>
        /// <param name="IsNew">Is the object from stratch(true) or from db/serialization(false)</param>
        public void HandleXML(XmlReader xmlReader, bool isNew)
        {
            using (xmlReader)
            {
                xmlReader.MoveToContent();
                if (xmlReader.HasAttributes)
                {
                    if (xmlReader.MoveToAttribute("uid"))
                        StaticHelper.HandleUID(xmlReader.Value, "singleInstalledPoint uid", out this.uid);
                    xmlReader.MoveToElement();
                }
                while (xmlReader.Read())
                {
                    //start on depth 1 which are the childs that we want to check against
                    //check for starting tag and convert node value to its expected variable
                    if (xmlReader.Depth == 1 && xmlReader.IsStartElement() && !xmlReader.IsEmptyElement)
                    {
                        switch (xmlReader.Name)
                        {
                            case "md":
                                if (this.md == null)
                                    this.md = new measuredDepthCoord();
                                this.md.HandleXML(xmlReader.ReadSubtree(), isNew);
                                break;
                            case "lengthAlongFiber":
                                if (this.lengthAlongFiber == null)
                                    this.lengthAlongFiber = new lengthMeasure();
                                this.lengthAlongFiber.HandleXML(xmlReader.ReadSubtree(), isNew);
                                break;
                            case "type":
                                StaticParser.SetEnumFromString<InstalledFiberPoint>(StaticHelper.ReadString(xmlReader), out this.type, out this.typeSpecified);
                                break;
                            default:
                                break;
                        }
                    }
                }
            }
        }
    }
}
