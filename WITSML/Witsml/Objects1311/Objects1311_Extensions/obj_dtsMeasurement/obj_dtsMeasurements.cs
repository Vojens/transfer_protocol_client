﻿ 

using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.IO;
using System.Xml;
using Witsml.Common.Exceptions;


namespace Witsml.Objects1311
{
    public partial class obj_dtsMeasurements : WITSMLDocument
    {
        public override WITSMLObject GetSingleObject(int Item)
        {
            return this.dtsMeasurement[Item];
        }

        public override int Decode(ref string xmlIn)
        {
            int countOfMeasurement = 0;
            try
            {
                //check whether collection is from stratch(isNew = true) or from deserialization(isNew=false)
                bool isNew = this.dtsMeasurement == null;

                //create temporary list to help with data manipulation
                List<obj_dtsMeasurement> listMeasurement = isNew ? new List<obj_dtsMeasurement>() : new List<obj_dtsMeasurement>(this.dtsMeasurement);

                using (StringReader stringReader = new StringReader(xmlIn))
                {
                    XmlReaderSettings setting = new XmlReaderSettings();
                    setting.ConformanceLevel = ConformanceLevel.Document;
                    setting.IgnoreComments = true;
                    setting.IgnoreWhitespace = true;
                    setting.IgnoreProcessingInstructions = true;
                    using (XmlReader xmlReader = XmlReader.Create(stringReader, setting))
                    {
                        while (xmlReader.Read())
                        {
                            //assign version
                            xmlReader.MoveToContent();
                            if (xmlReader.MoveToAttribute("version"))
                                this.version = xmlReader.Value;
                            else
                                this.version = "1.3.1.1";//no version specified,set it to 1.3.1.1

                            //pre-checking on whether node is the correct child based on depth,start tag and not empty
                            if (xmlReader.Depth == 1 && xmlReader.IsStartElement() && !xmlReader.IsEmptyElement)
                            {
                                switch (xmlReader.Name)
                                {
                                    case "documentInfo":
                                        if (this.documentInfo == null)
                                            this.documentInfo = new cs_documentInfo();
                                        this.documentInfo.HandleXML(xmlReader.ReadSubtree(), isNew);
                                        break;
                                    case "dtsMeasurement":
                                        obj_dtsMeasurement measurementObj;
                                        string currentUID = string.Empty;

                                        //Update measurement
                                        if (!isNew)
                                        {
                                            //check for uid attribute and assign it to temporary string for searching in collection
                                            if (xmlReader.HasAttributes)
                                            {
                                                if (xmlReader.MoveToAttribute("uid"))
                                                    currentUID = xmlReader.Value;
                                                xmlReader.MoveToElement();
                                            }
                                            //look for measurement based on its UID and update the measurement with xml string
                                            if (currentUID != string.Empty)
                                            {
                                                measurementObj = listMeasurement.FirstOrDefault(
                                                    delegate(obj_dtsMeasurement objMeasurement)
                                                    { if (objMeasurement.uid == currentUID)return true; else return false; });
                                                if (measurementObj != null)
                                                {
                                                    measurementObj.HandleXML(xmlReader.ReadSubtree(), isNew);
                                                    countOfMeasurement++;
                                                    continue;
                                                }
                                                else
                                                {
                                                    //Update can't process measurement without match
                                                    continue;
                                                }
                                            }
                                            else
                                            {
                                                //Update can't process measurement without uid
                                                continue;
                                            }
                                        }

                                        //Add New fluid 
                                        measurementObj = new obj_dtsMeasurement();
                                        measurementObj.HandleXML(xmlReader.ReadSubtree(), true);
                                        listMeasurement.Add(measurementObj);
                                        countOfMeasurement++;
                                        break;
                                    default:
                                        break;
                                }
                            }
                        }
                        //assign list to folder collection
                        this.dtsMeasurement = listMeasurement.ToArray();
                    }
                }
            }
            catch (XmlException ex)
            {
                throw new Exceptions.UnableToDecodeException(ex.Message, WITSMLReturnCode.WITSMLDocumentError, ex.InnerException);
            }
            catch (NullReferenceException ex)
            {
                throw new Exceptions.UnableToDecodeException(ex.Message, WITSMLReturnCode.WITSMLEmptyDocument, ex.InnerException);
            }
            catch (Exceptions.UnableToDecodeException ex)
            {
                throw ex;
            }
            catch (Exception ex)
            {
                throw new Exceptions.UnableToDecodeException(ex.Message, WITSMLReturnCode.WITSMLUnkownCause, ex.InnerException);
            }
            return countOfMeasurement;
        }
    }
}
