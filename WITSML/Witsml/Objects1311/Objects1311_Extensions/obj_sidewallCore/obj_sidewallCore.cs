﻿ 

using System;
using System.Collections.Generic;
using System.IO;
using System.Text;
using System.Xml;
using Witsml.Common.Exceptions;
using Witsml.Exceptions;


namespace Witsml.Objects1311
{
    [System.Xml.Serialization.XmlRootAttribute("sidewallCore", Namespace = "http://www.witsml.org/schemas/131", IsNullable = false)]
    public partial class obj_sidewallCore : WITSMLObject, IHandleXML
    {
        public obj_sidewallCore()
        {
        }

        /// <summary>
        /// Decode XML string into sidewallCore
        /// </summary>
        /// <param name="xmlIn">XML string</param>
        public override void Decode(ref string xmlIn)
        {
            try
            {
                //Consume XML string and convert node value to variable based on node name
                using (StringReader stringReader = new StringReader(xmlIn))
                {
                    XmlReaderSettings setting = new XmlReaderSettings();
                    setting.ConformanceLevel = ConformanceLevel.Document;
                    setting.IgnoreComments = true;
                    setting.IgnoreWhitespace = true;
                    setting.IgnoreProcessingInstructions = true;
                    using (XmlReader xmlReader = XmlReader.Create(stringReader, setting))
                    {
                        //determine add or update operation
                        bool isNew;
                        if (this.uid == string.Empty)
                            isNew = true;
                        else isNew = false;

                        this.HandleXML(xmlReader, isNew);
                    }
                }
            }
            catch (XmlException ex)
            {
                throw new Exceptions.UnableToDecodeException(ex.Message, WITSMLReturnCode.WITSMLDocumentError, ex.InnerException);
            }
            catch (NullReferenceException ex)
            {
                throw new Exceptions.UnableToDecodeException(ex.Message, WITSMLReturnCode.WITSMLEmptyDocument, ex.InnerException);
            }
            catch (Exceptions.UnableToDecodeException ex)
            {
                throw ex;
            }
            catch (Exception ex)
            {
                throw new Exceptions.UnableToDecodeException(ex.Message, WITSMLReturnCode.WITSMLUnkownCause, ex.InnerException);
            }
        }

        /// <summary>
        /// Convert XML string data into obj_sidewallCore fields value
        /// </summary>
        /// <param name="xmlReader">reader that has obj_sidewallCore node</param>
        /// <param name="IsNew">Is the object from stratch(true) or from db/serialization(false)</param>
        public void HandleXML(XmlReader xmlReader, bool isNew)
        {
            //condition for add update collection
            bool isSwcSampleNew = this.swcSample == null;

            //temporary list for data manipulations
            List<cs_swcSample> listSwcSample = isSwcSampleNew ? new List<cs_swcSample>() : new List<cs_swcSample>(this.swcSample);
            using (xmlReader)
            {
                //assign uid for the new well
                xmlReader.MoveToContent();
                if (xmlReader.HasAttributes)
                {
                    if (xmlReader.MoveToAttribute("uidWell"))
                        StaticHelper.HandleUID(xmlReader.Value, "sidewallCore uidWell", out this.uidWell);
                    if (xmlReader.MoveToAttribute("uidWellbore"))
                        StaticHelper.HandleUID(xmlReader.Value, "sidewallCore uidWellbore", out this.uidWellbore);
                    if (xmlReader.MoveToAttribute("uid"))
                        StaticHelper.HandleUID(xmlReader.Value, "sidewallCore uid", out this.uid);
                    xmlReader.MoveToElement();
                }
                while (xmlReader.Read())
                {
                    //only take child node
                    if (xmlReader.Depth == 1 && xmlReader.IsStartElement() && !xmlReader.IsEmptyElement)
                    {
                        switch (xmlReader.Name)
                        {
                            case "nameWell":
                                this.nameWell = StaticHelper.ReadString(xmlReader);
                                break;
                            case "nameWellbore":
                                this.nameWellbore = StaticHelper.ReadString(xmlReader);
                                break;
                            case "name":
                                this.name = StaticHelper.ReadString(xmlReader);
                                break;
                            case "dTimToolRun":
                                this.dTimToolRun = StaticParser.ParseDateTimeFromString(StaticHelper.ReadString(xmlReader));
                                this.dTimToolRunSpecified = true;
                                break;
                            case "dTimToolPull":
                                this.dTimToolPull = StaticParser.ParseDateTimeFromString(StaticHelper.ReadString(xmlReader));
                                this.dTimToolPullSpecified = true;
                                break;
                            case "mdToolReference":
                                if (this.mdToolReference == null)
                                    this.mdToolReference = new measuredDepthCoord();
                                this.mdToolReference.HandleXML(xmlReader.ReadSubtree(), isNew);
                                break;
                            case "coreReferenceLog":
                                this.coreReferenceLog = StaticHelper.ReadString(xmlReader);
                                break;
                            case "mdCore":
                                if (this.mdCore == null)
                                    this.mdCore = new measuredDepthCoord();
                                this.mdCore.HandleXML(xmlReader.ReadSubtree(), isNew);
                                break;
                            case "serviceCompany":
                                this.serviceCompany = StaticHelper.ReadString(xmlReader);
                                break;
                            case "analysisContractor":
                                this.analysisContractor = StaticHelper.ReadString(xmlReader);
                                break;
                            case "analysisBy":
                                this.analysisBy = StaticHelper.ReadString(xmlReader);
                                break;
                            case "sidewallCoringTool":
                                this.sidewallCoringTool = StaticHelper.ReadString(xmlReader);
                                break;
                            case "diaHole":
                                if (this.diaHole == null)
                                    this.diaHole = new lengthMeasure();
                                this.diaHole.HandleXML(xmlReader.ReadSubtree(), isNew);
                                break;
                            case "diaPlug":
                                if (this.diaPlug == null)
                                    this.diaPlug = new lengthMeasure();
                                this.diaPlug.HandleXML(xmlReader.ReadSubtree(), isNew);
                                break;
                            case "numPlugsShot":
                                this.numPlugsShot = StaticParser.ParseShortFromString(StaticHelper.ReadString(xmlReader));
                                this.numPlugsShotSpecified = true;
                                break;
                            case "numRecPlugs":
                                this.numRecPlugs = StaticParser.ParseShortFromString(StaticHelper.ReadString(xmlReader));
                                this.numRecPlugsSpecified = true;
                                break;
                            case "numMisfiredPlugs":
                                this.numMisfiredPlugs = StaticParser.ParseShortFromString(StaticHelper.ReadString(xmlReader));
                                this.numMisfiredPlugsSpecified = true;
                                break;
                            case "numEmptyPlugs":
                                this.numEmptyPlugs = StaticParser.ParseShortFromString(StaticHelper.ReadString(xmlReader));
                                this.numEmptyPlugsSpecified = true;
                                break;
                            case "numLostPlugs":
                                this.numLostPlugs = StaticParser.ParseShortFromString(StaticHelper.ReadString(xmlReader));
                                this.numLostPlugsSpecified = true;
                                break;
                            case "numPaidPlugs":
                                this.numPaidPlugs = StaticParser.ParseShortFromString(StaticHelper.ReadString(xmlReader));
                                this.numPaidPlugsSpecified = true;
                                break;
                            case "swcSample":
                                //collection
                                StaticHelper.AddUpdateWithUid<cs_swcSample>(xmlReader, isSwcSampleNew, ref listSwcSample);
                                break;
                            case "commonData":
                                if (this.commonData == null)
                                    this.commonData = new cs_commonData();
                                this.commonData.HandleXML(xmlReader.ReadSubtree(), isNew);
                                break;
                            case "customData":
                                if (this.customData == null)
                                    this.customData = new cs_customData();
                                this.customData.HandleXML(xmlReader.ReadSubtree(), isNew);
                                break;
                            default:
                                break;
                        }
                    }
                }
                //assign list to collection
                this.swcSample = listSwcSample.ToArray();
            }
        }

        public override string ValidateGetPath(string newUid)
        {
            // Fix uid if the Object dosn't have one.
            if (!IsValidUid(this.uid))
            {
                this.uid = newUid;
                // TODO: Add some loging that we created a new UID for the object maybe.
            }

            StringBuilder path = new StringBuilder(this.GetParentPath());
            path.Append(@"\");
            path.Append(this.uid);
            return path.ToString();
        }

        public override string GetParentPath()
        {
            if (!IsValidUid(this.uidWell) || !IsValidUid(this.uidWellbore))
            {
                throw new UnableToDecodeException("One or more Parent UID's are invalid",WITSMLReturnCode.WITSMLInvalidParentUID);
                //TODO: Log the issue
            }

            StringBuilder path = new StringBuilder(@"\");
            path.Append(this.uidWell);
            path.Append(@"\");
            path.Append(this.uidWellbore);
            return path.ToString();
        }

        // NOTE: these are done with functions, instead of property access, so as not to mess around with the auto-generated / serialized classes.
        public override string Get_uid()
        {
            return this.uid;
        }

        public override string Get_uidWell()
        {
            return this.uidWell;
        }

        public override string Get_uidWellbore()
        {
            return this.uidWellbore;
        }

        public override string Get_name()
        {
            return this.name;
        }

        public override string Get_nameWell()
        {
            return this.nameWell;
        }

        public override string Get_nameWellbore()
        {
            return this.nameWellbore;
        }

        public override ICommonData Get_commonData()
        {
            if (this.commonData == null)
            {
                this.commonData = new cs_commonData();
            }
            return (ICommonData)this.commonData;
        }
        public override ICustomData Get_customData()
        {
            if (this.customData == null)
            {
                this.customData = new cs_customData();
            }
            return (ICustomData)this.customData;
        }
    }
}
