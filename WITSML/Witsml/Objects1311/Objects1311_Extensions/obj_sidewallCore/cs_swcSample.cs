﻿ 

using System;
using System.Collections.Generic;
using System.Text;
using System.Xml;

namespace Witsml.Objects1311
{
    public partial class cs_swcSample : IHandleXML, IHasID
    {
        //interface for generic helper
        [System.Xml.Serialization.XmlIgnore]
        public string UID
        {
            get { return this.uid; }
        }
        public cs_swcSample()
        {
        }
        /// <summary>
        /// Convert XML string data into cs_swcSample fields value
        /// </summary>
        /// <param name="xmlReader">reader that has cs_swcSample node</param>
        /// <param name="IsNew">Is the object from stratch(true) or from db/serialization(false)</param>
        public void HandleXML(XmlReader xmlReader, bool isNew)
        {
            using (xmlReader)
            {
                xmlReader.MoveToContent();
                if (xmlReader.HasAttributes)
                {
                    if (xmlReader.MoveToAttribute("uid"))
                        StaticHelper.HandleUID(xmlReader.Value, "swcSample uid", out this.uid);
                    xmlReader.MoveToElement();
                }
                while (xmlReader.Read())
                {
                    //start on depth 1 which are the childs that we want to check against
                    //check for starting tag and convert node value to its expected variable
                    if (xmlReader.Depth == 1 && xmlReader.IsStartElement() && !xmlReader.IsEmptyElement)
                    {
                        switch (xmlReader.Name)
                        {
                            case "md":
                                if (this.md == null)
                                    this.md = new measuredDepthCoord();
                                this.md.HandleXML(xmlReader.ReadSubtree(), isNew);
                                break;
                            case "lithology":
                                if (this.lithology == null)
                                    this.lithology = new cs_lithology();
                                this.lithology.HandleXML(xmlReader.ReadSubtree(), isNew);
                                break;
                            case "show":
                                if (this.show == null)
                                    this.show = new cs_show();
                                this.show.HandleXML(xmlReader.ReadSubtree(), isNew);
                                break;
                            case "nameFormation":
                                this.nameFormation = StaticHelper.ReadString(xmlReader);
                                break;
                            case "comments":
                                this.comments = StaticHelper.ReadString(xmlReader);
                                break;
                            default:
                                break;
                        }
                    }
                }
            }
        }
    }
}
