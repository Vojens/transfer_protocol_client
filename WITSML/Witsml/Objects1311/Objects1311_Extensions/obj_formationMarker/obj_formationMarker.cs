 

using System;
using System.IO;
using System.Text;
using System.Xml;
using Witsml.Common.Exceptions;
using Witsml.Exceptions;


namespace Witsml.Objects1311
{
    [System.Xml.Serialization.XmlRootAttribute("formationMarker", Namespace = "http://www.witsml.org/schemas/131", IsNullable = false)]
    public partial class obj_formationMarker : WITSMLObject, IHandleXML
    {
        public obj_formationMarker()
        {
        }

        /// <summary>
        /// Decode XML string into rig
        /// </summary>
        /// <param name="xmlIn">XML string</param>
        public override void Decode(ref string xmlIn)
        {
            try
            {
                //Consume XML string and convert node value to variable based on node name
                using (StringReader stringReader = new StringReader(xmlIn))
                {
                    XmlReaderSettings setting = new XmlReaderSettings();
                    setting.ConformanceLevel = ConformanceLevel.Document;
                    setting.IgnoreComments = true;
                    setting.IgnoreWhitespace = true;
                    setting.IgnoreProcessingInstructions = true;
                    using (XmlReader xmlReader = XmlReader.Create(stringReader, setting))
                    {
                        //determine add or update operation
                        bool isNew;
                        if (this.uid == string.Empty)
                            isNew = true;
                        else isNew = false;

                        this.HandleXML(xmlReader, isNew);
                    }
                }
            }
            catch (XmlException ex)
            {
                throw new Exceptions.UnableToDecodeException(ex.Message, WITSMLReturnCode.WITSMLDocumentError, ex.InnerException);
            }
            catch (NullReferenceException ex)
            {
                throw new Exceptions.UnableToDecodeException(ex.Message, WITSMLReturnCode.WITSMLEmptyDocument, ex.InnerException);
            }
            catch (Exceptions.UnableToDecodeException ex)
            {
                throw ex;
            }
            catch (Exception ex)
            {
                throw new Exceptions.UnableToDecodeException(ex.Message, WITSMLReturnCode.WITSMLUnkownCause, ex.InnerException);
            }
        }

        /// <summary>
        /// Convert XML string data into formationMarker fields value
        /// </summary>
        /// <param name="xmlReader">reader that has formationMarker node</param>
        /// <param name="IsNew">Is the object from stratch(true) or from db/serialization(false)</param>
        public void HandleXML(XmlReader xmlReader, bool isNew)
        {
            using (xmlReader)
            {
                //assign uid for the new well
                xmlReader.MoveToContent();
                if (xmlReader.HasAttributes)
                {
                    if (xmlReader.MoveToAttribute("uidWell"))
                        StaticHelper.HandleUID(xmlReader.Value, "formationMarker uidWell", out this.uidWell);
                    if (xmlReader.MoveToAttribute("uidWellbore"))
                        StaticHelper.HandleUID(xmlReader.Value, "formationMarker uidWellbore", out this.uidWellbore);
                    if (xmlReader.MoveToAttribute("uid"))
                        StaticHelper.HandleUID(xmlReader.Value, "formationMarker uid", out this.uid);
                    xmlReader.MoveToElement();
                }
                while (xmlReader.Read())
                {
                    //only take child node
                    if (xmlReader.Depth == 1 && xmlReader.IsStartElement() && !xmlReader.IsEmptyElement)
                    {
                        switch (xmlReader.Name)
                        {
                            case "nameWell":
                                this.nameWell = StaticHelper.ReadString(xmlReader);
                                break;
                            case "nameWellbore":
                                this.nameWellbore = StaticHelper.ReadString(xmlReader);
                                break;
                            case "name":
                                this.name = StaticHelper.ReadString(xmlReader);
                                break;
                            case "mdPrognosed":
                                if (this.mdPrognosed == null)
                                    this.mdPrognosed = new measuredDepthCoord();
                                this.mdPrognosed.HandleXML(xmlReader.ReadSubtree(), isNew);
                                break;
                            case "tvdPrognosed":
                                if (this.tvdPrognosed == null)
                                    this.tvdPrognosed = new wellVerticalDepthCoord();
                                this.tvdPrognosed.HandleXML(xmlReader.ReadSubtree(), isNew);
                                break;
                            case "mdTopSample":
                                if (this.mdTopSample == null)
                                    this.mdTopSample = new measuredDepthCoord();
                                this.mdTopSample.HandleXML(xmlReader.ReadSubtree(), isNew);
                                break;
                            case "tvdTopSample":
                                if (this.tvdTopSample == null)
                                    this.tvdTopSample = new wellVerticalDepthCoord();
                                this.tvdTopSample.HandleXML(xmlReader.ReadSubtree(), isNew);
                                break;
                            case "thicknessBed":
                                if (this.thicknessBed == null)
                                    this.thicknessBed = new lengthMeasure();
                                this.thicknessBed.HandleXML(xmlReader.ReadSubtree(), isNew);
                                break;
                            case "thicknessApparent":
                                if (this.thicknessApparent == null)
                                    this.thicknessApparent = new lengthMeasure();
                                this.thicknessApparent.HandleXML(xmlReader.ReadSubtree(), isNew);
                                break;
                            case "thicknessPerpen":
                                if (this.thicknessPerpen == null)
                                    this.thicknessPerpen = new lengthMeasure();
                                this.thicknessPerpen.HandleXML(xmlReader.ReadSubtree(), isNew);
                                break;
                            case "mdLogSample":
                                if (this.mdLogSample == null)
                                    this.mdLogSample = new measuredDepthCoord();
                                this.mdLogSample.HandleXML(xmlReader.ReadSubtree(), isNew);
                                break;
                            case "tvdLogSample":
                                if (this.tvdLogSample == null)
                                    this.tvdLogSample = new wellVerticalDepthCoord();
                                this.tvdLogSample.HandleXML(xmlReader.ReadSubtree(), isNew);
                                break;
                            case "dip":
                                if (this.dip == null)
                                    this.dip = new planeAngleMeasure();
                                this.dip.HandleXML(xmlReader.ReadSubtree(), isNew);
                                break;
                            case "dipDirection":
                                if (this.dipDirection == null)
                                    this.dipDirection = new planeAngleMeasure();
                                this.dipDirection.HandleXML(xmlReader.ReadSubtree(), isNew);
                                break;
                            case "chronostratigraphic":
                                this.chronostratigraphic = StaticHelper.ReadString(xmlReader);
                                break;
                            case "nameFormation":
                                this.nameFormation = StaticHelper.ReadString(xmlReader);
                                break;
                            case "description":
                                this.description = StaticHelper.ReadString(xmlReader);
                                break;
                            case "commonData":
                                if (this.commonData == null)
                                    this.commonData = new cs_commonData();
                                this.commonData.HandleXML(xmlReader.ReadSubtree(), isNew);
                                break;
                            case "customData":
                                if (this.customData == null)
                                    this.customData = new cs_customData();
                                this.customData.HandleXML(xmlReader.ReadSubtree(), isNew);
                                break;
                            default:
                                break;
                        }
                    }
                }
            }
        }

        public override string ValidateGetPath(string newUid)
        {
            // Fix uid if the Object dosn't have one.
            if (!IsValidUid(this.uid))
            {
                this.uid = newUid;
                // TODO: Add some loging that we created a new UID for the object maybe.
            }

            StringBuilder path = new StringBuilder(this.GetParentPath());
            path.Append(@"\");
            path.Append(this.uid);
            return path.ToString();
        }

        public override string GetParentPath()
        {
            if (!IsValidUid(this.uidWell) || !IsValidUid(this.uidWellbore))
            {
                throw new UnableToDecodeException("One or more Parent UID's are invalid", WITSMLReturnCode.WITSMLInvalidParentUID);
                //TODO: Log the issue
            }

            StringBuilder path = new StringBuilder(@"\");
            path.Append(this.uidWell);
            path.Append(@"\");
            path.Append(this.uidWellbore);
            return path.ToString();
        }

        // NOTE: these are done with functions, instead of property access, so as not to mess around with the auto-generated / serialized classes.
        public override string Get_uid()
        {
            return this.uid;
        }

        public override string Get_uidWell()
        {
            return this.uidWell;
        }

        public override string Get_uidWellbore()
        {
            return this.uidWellbore;
        }

        public override string Get_name()
        {
            return this.name;
        }

        public override string Get_nameWell()
        {
            return this.nameWell;
        }

        public override string Get_nameWellbore()
        {
            return this.nameWellbore;
        }

        public override ICommonData Get_commonData()
        {
            if (this.commonData == null)
            {
                this.commonData = new cs_commonData();
            }
            return (ICommonData)this.commonData;
        }
        public override ICustomData Get_customData()
        {
            if (this.customData == null)
            {
                this.customData = new cs_customData();
            }
            return (ICustomData)this.customData;
        }
    }
}
