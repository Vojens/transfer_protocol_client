 

using System;
using System.Collections.Generic;
using System.Text;

//TODO: Need to bring in the rest of the objects in the 'New_NotIntegrated' directory at some point.

namespace Witsml.Objects1311
{
    /// <remarks/>
    [System.CodeDom.Compiler.GeneratedCodeAttribute("xsd", "2.0.50727.42")]
    
    
    
    
    public partial class cs_commonData
    {
        /// <remarks/>
        public string sourceName;

        /// <remarks/>
        public System.DateTime dTimCreation;

        /// <remarks/>
        [System.Xml.Serialization.XmlIgnoreAttribute()]
        public bool dTimCreationSpecified;

        /// <remarks/>
        public System.DateTime dTimLastChange;

        /// <remarks/>
        [System.Xml.Serialization.XmlIgnoreAttribute()]
        public bool dTimLastChangeSpecified;

        /// <remarks/>
        public ItemState itemState;

        /// <remarks/>
        [System.Xml.Serialization.XmlIgnoreAttribute()]
        public bool itemStateSpecified;

        /// <remarks/>
        public string comments;
    }

    /// <remarks/>
    [System.CodeDom.Compiler.GeneratedCodeAttribute("xsd", "2.0.50727.42")]
    
    
    public enum ItemState
    {

        /// <remarks/>
        actual,

        /// <remarks/>
        model,

        /// <remarks/>
        plan,

        /// <remarks/>
        unknown,
    }
}
