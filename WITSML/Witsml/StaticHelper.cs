 

using System;
using System.Collections.Generic;
using System.Text;
using System.Xml;
using System.Linq;
using System.Xml.Linq;
using System.Text.RegularExpressions;
using Support.Common;
using System.Reflection;
using System.Xml.Serialization;
using Witsml.Common.Exceptions;
using Witsml.Exceptions;

namespace Witsml
{
    // test comment 7
    /// <summary>
    /// Helper Class to handle useful static function
    /// </summary>
    public static class StaticHelper
    {

        /// <summary>
        /// Handle processing of UID including pattern checking
        /// </summary>
        /// <param name="value">uid value</param>
        /// <param name="UIDField">uid field</param>
        public static void HandleUID(string value, string objectName, out string UIDField)
        {
            value = XmlHelper.UnescapeXml(value.Trim());
            if (CheckUid(value))
                UIDField = value;
            else
                throw new UnableToDecodeException(
                    string.Format("Fail to Decode,Error uid pattern mismatch(contain space) on {0}", "object <" + objectName + " = " + value + ">"), WITSMLReturnCode.WITSMLUidContainsSpace, true);
        }
        /// <summary>
        /// Check uid whether it does not contain whitespace inside it
        /// </summary>
        /// <param name="value"></param>
        /// <returns></returns>
        private static bool CheckUid(string value)
        {
            //uid shouldn't contain whitespace inside it
            Regex regex = new Regex("^[\\S]*$");
            if (regex.IsMatch(value))
                return true;
            else
                //                return false;
                // TODO: Hack to remove the pattern check for all uid's for now.
                return true;
        }

        //Generate a new UID for an object.
        public static string GenerateObjectUID()
        {
            return System.Guid.NewGuid().ToString();  // use a GUID for now, although we need to have a better, shorter, unique identifier generation function.
        }

        public static string CleanDataValue(string data)
        {
            StringBuilder sb = new StringBuilder(data);
            sb.Replace(System.Environment.NewLine, "");
            return sb.ToString().Trim();
        }

        public static bool CheckPowerStoreExist(ref string xmlIn)
        {
            XElement root = XElement.Parse(xmlIn);
            return (string)(from powerStore in root.Descendants("powerStore")
                            select powerStore).FirstOrDefault() == string.Empty;
        }


        /// <summary>
        /// will be used to return array after processing using temporary list 
        /// </summary>
        /// <typeparam name="T"></typeparam>
        /// <param name="listT"></param>
        /// <returns></returns>
        public static T[] ReturnList<T>(List<T> listT)
        {
            if (listT.Count == 0)
                return null;
            else
                return listT.ToArray();
        }

        /// <summary>
        /// Function to replace XMLReader.ReadString where the end position is on EndElement
        /// </summary>
        /// <param name="xmlReader"></param>
        /// <returns></returns>
        public static string ReadString(XmlReader xmlReader)
        {
            string value = string.Empty;
            xmlReader.ReadStartElement();
            if (xmlReader.NodeType != XmlNodeType.None)
                value = xmlReader.ReadContentAsString();
            return value;
        }

        /// <summary>
        /// Generic AddUpdate for Recurring WITSML Object with UID(string)
        /// </summary>
        /// <typeparam name="T">type of recurring object</typeparam>
        /// <param name="xmlReader">pass the xmlReader</param>
        /// <param name="isTNew">is the collection new(determine add or update)</param>
        /// <param name="listT">collection of the recurring object</param>
        public static void AddUpdateWithUid<T>(XmlReader xmlReader, bool isTNew, ref List<T> listT) where T : IHandleXML, IHasID, new()
        {
            bool isUpdateOp = false;
            //collection
            T TObj;
            string currentID = string.Empty;

            if (xmlReader.HasAttributes)
            {
                //check for uid attribute and assign it to temporary string for searching in collection
                if (xmlReader.MoveToAttribute("uid"))
                {
                    currentID = xmlReader.Value;
                    xmlReader.MoveToElement();
                }
            }

            //check whether uid is empty
            if (!string.IsNullOrEmpty(currentID))
            {
                //update
                if (!isTNew || listT.Count > 0)
                {
                    TObj = listT.FirstOrDefault(
                        delegate(T objT)
                        { if (objT.UID == currentID)return true; else return false; });
                    if (TObj != null)
                    {
                        TObj.HandleXML(xmlReader.ReadSubtree(), isTNew);
                        isUpdateOp = true;
                    }
                }

                //add
                if (!isUpdateOp)
                {
                    TObj = new T();
                    TObj.HandleXML(xmlReader.ReadSubtree(), isTNew);
                    listT.Add(TObj);
                }
            }
        }

        /// <summary>
        /// Generic AddUpdate for Recurring WITSML Object with UID(string)
        /// </summary>
        /// <typeparam name="T">type of recurring object</typeparam>
        /// <param name="xmlReader">pass the xmlReader</param>
        /// <param name="isTNew">is the collection new(determine add or update)</param>
        /// <param name="listT">collection of the recurring object</param>
        /// <param name="uidName">uid attribute name</param>
        public static void AddUpdateWithUid<T>(XmlReader xmlReader, bool isTNew, ref List<T> listT, string uidName) where T : IHandleXML, IHasID, new()
        {
            bool isUpdateOp = false;
            //collection
            T TObj;
            string currentID = string.Empty;

            if (xmlReader.HasAttributes)
            {
                //check for uid attribute and assign it to temporary string for searching in collection
                if (xmlReader.MoveToAttribute(uidName))
                {
                    currentID = xmlReader.Value;
                    xmlReader.MoveToElement();
                }
            }

            //check whether uid is empty
            if (!string.IsNullOrEmpty(currentID))
            {
                //update
                if (!isTNew || listT.Count > 0)
                {
                    TObj = listT.FirstOrDefault(
                        delegate(T objT)
                        { if (objT.UID == currentID)return true; else return false; });
                    if (TObj != null)
                    {
                        TObj.HandleXML(xmlReader.ReadSubtree(), isTNew);
                        isUpdateOp = true;
                    }
                }

                //add
                if (!isUpdateOp)
                {
                    TObj = new T();
                    TObj.HandleXML(xmlReader.ReadSubtree(), isTNew);
                    listT.Add(TObj);
                }
            }
        }




        //GUID uid

        /// <summary>
        /// Generic AddUpdate for Recurring WITSML Object with UID
        /// </summary>
        /// <typeparam name="T">type of recurring object</typeparam>
        /// <param name="xmlReader">pass the xmlReader</param>
        /// <param name="isTNew">is the collection new(determine add or update)</param>
        /// <param name="listT">collection of the recurring object</param>
        public static void AddUpdateWithGuid<T>(XmlReader xmlReader, bool isTNew, ref List<T> listT) where T : IHandleXML, IHasGUID, new()
        {
            bool isUpdateOp = false;
            //collection
            T TObj;
            Guid currentID = Guid.Empty;

            if (xmlReader.HasAttributes)
            {
                //check for uid attribute and assign it to temporary string for searching in collection
                if (xmlReader.MoveToAttribute("uid"))
                {
                    currentID = StaticParser.ParseGuidFromString(xmlReader.Value);
                    xmlReader.MoveToElement();
                }
            }

            //check whether uid is empty
            if (currentID != Guid.Empty)
            {
                //update
                if (!isTNew || listT.Count > 0)
                {
                    TObj = listT.FirstOrDefault(
                        delegate(T objT)
                        { if (objT.ID == currentID)return true; else return false; });
                    if (TObj != null)
                    {
                        TObj.HandleXML(xmlReader.ReadSubtree(), isTNew);
                        isUpdateOp = true;
                    }
                }

                //add
                if (!isUpdateOp)
                {
                    TObj = new T();
                    TObj.HandleXML(xmlReader.ReadSubtree(), isTNew);
                    listT.Add(TObj);
                }
            }
        }

        /// <summary>
        /// Generic AddUpdate for Recurring WITSML Object with UID
        /// </summary>
        /// <typeparam name="T">type of recurring object</typeparam>
        /// <param name="xmlReader">pass the xmlReader</param>
        /// <param name="isTNew">is the collection new(determine add or update)</param>
        /// <param name="listT">collection of the recurring object</param>
        /// <param name="uidName">uid attribute name</param>
        public static void AddUpdateWithGuid<T>(XmlReader xmlReader, bool isTNew, ref List<T> listT, string uidName) where T : IHandleXML, IHasGUID, new()
        {
            bool isUpdateOp = false;
            //collection
            T TObj;
            Guid currentID = Guid.Empty;

            if (xmlReader.HasAttributes)
            {
                //check for uid attribute and assign it to temporary string for searching in collection
                if (xmlReader.MoveToAttribute(uidName))
                {
                    currentID = StaticParser.ParseGuidFromString(xmlReader.Value);
                    xmlReader.MoveToElement();
                }
            }

            //check whether uid is empty
            if (currentID != Guid.Empty)
            {
                //update
                if (!isTNew || listT.Count > 0)
                {
                    TObj = listT.FirstOrDefault(
                        delegate(T objT)
                        { if (objT.ID == currentID)return true; else return false; });
                    if (TObj != null)
                    {
                        TObj.HandleXML(xmlReader.ReadSubtree(), isTNew);
                        isUpdateOp = true;
                    }
                }

                //add
                if (!isUpdateOp)
                {
                    TObj = new T();
                    TObj.HandleXML(xmlReader.ReadSubtree(), isTNew);
                    listT.Add(TObj);
                }
            }
        }

        /// <summary>
        /// Generic AddUpdate for Recurring WITSML Object with UID and Update Objec State information
        /// </summary>
        /// <typeparam name="T">type of recurring object</typeparam>
        /// <param name="xmlReader">pass the xmlReader</param>
        /// <param name="isTNew">is the collection new(determine add or update)</param>
        /// <param name="listT">collection of the recurring object</param>
        public static void AddUpdateWithGuidnObjectState<T>(XmlReader xmlReader, bool isTNew, ref List<T> listT) where T : IHandleXML, IHasGUID, IHasObjectState, new()
        {
            bool isUpdateOp = false;
            //collection
            T TObj;
            Guid currentID = Guid.Empty;

            if (xmlReader.HasAttributes)
            {
                //check for uid attribute and assign it to temporary string for searching in collection
                if (xmlReader.MoveToAttribute("uid"))
                {
                    currentID = StaticParser.ParseGuidFromString(xmlReader.Value);
                    xmlReader.MoveToElement();
                }
            }

            //check whether uid is empty
            if (currentID != Guid.Empty)
            {
                //update
                if (!isTNew || listT.Count > 0)
                {
                    TObj = listT.FirstOrDefault(
                        delegate(T objT)
                        { if (objT.ID == currentID)return true; else return false; });
                    if (TObj != null)
                    {
                        TObj.State = ObjectState.Update;
                        TObj.HandleXML(xmlReader.ReadSubtree(), isTNew);
                        isUpdateOp = true;
                    }
                }

                //add
                if (!isUpdateOp)
                {
                    TObj = new T();
                    //this is new item on existing collection
                    if (!isTNew)
                        TObj.State = ObjectState.New;
                    TObj.HandleXML(xmlReader.ReadSubtree(), isTNew);
                    listT.Add(TObj);
                }
            }
        }

        /// <summary>
        /// Handle array item decode with checking of whether it has exist and different uid name (string).UID is string.
        /// </summary>
        /// <typeparam name="T"></typeparam>
        /// <param name="xmlReader">pass the XMLReader.ReadSubTree()</param>
        /// <param name="isNew"></param>
        /// <param name="listT"></param>
        /// <param name="arrayItemName">name of array item</param>
        /// <param name="uidName">uid field name(uid type is string)</param>
        public static void HandleArrayItemChecking<T>(XmlReader xmlReader, bool isNew, ref List<T> listT, string arrayItemName, string uidName) where T : IHandleXML, IHasID, new()
        {
            using (xmlReader)
            {
                while (xmlReader.Read())
                {
                    if (xmlReader.Depth == 1 && xmlReader.IsStartElement())
                    {
                        if (xmlReader.Name == arrayItemName)
                        {
                            StaticHelper.AddUpdateWithUid<T>(xmlReader, isNew, ref listT, uidName);
                        }
                    }
                }
            }
        }

        /// <summary>
        /// Handle array item decode with checking of whether it has exist.UID is GUID.
        /// </summary>
        /// <typeparam name="T"></typeparam>
        /// <param name="xmlReader">pass the XMLReader.ReadSubTree()</param>
        /// <param name="isNew"></param>
        /// <param name="listT"></param>
        /// <param name="arrayItemName">name of array item</param>
        public static void HandleArrayItemChecking<T>(XmlReader xmlReader, bool isNew, ref List<T> listT, string arrayItemName) where T : IHandleXML, IHasGUID, new()
        {
            using (xmlReader)
            {
                while (xmlReader.Read())
                {
                    if (xmlReader.Depth == 1 && xmlReader.IsStartElement())
                    {
                        if (xmlReader.Name == arrayItemName)
                        {
                            StaticHelper.AddUpdateWithGuid<T>(xmlReader, isNew, ref listT);
                        }
                    }
                }
            }
        }

        /// <summary>
        /// Handle array item decode with checking of whether it has exist and update object state
        /// </summary>
        /// <typeparam name="T"></typeparam>
        /// <param name="xmlReader">pass the XMLReader.ReadSubTree()</param>
        /// <param name="isNew"></param>
        /// <param name="listT"></param>
        /// <param name="arrayItemName"></param>
        public static void HandleArrayItemCheckingState<T>(XmlReader xmlReader, bool isNew, ref List<T> listT, string arrayItemName) where T : IHandleXML, IHasGUID, IHasObjectState, new()
        {
            using (xmlReader)
            {
                while (xmlReader.Read())
                {
                    if (xmlReader.Depth == 1 && xmlReader.IsStartElement())
                    {
                        if (xmlReader.Name == arrayItemName)
                        {
                            StaticHelper.AddUpdateWithGuidnObjectState<T>(xmlReader, isNew, ref listT);
                        }
                    }
                }
            }
        }

        /// <summary>
        /// AddUpdate for indexed object for object 1.3.1.1
        /// </summary>
        /// <param name="xmlReader">pass the xmlReader</param>
        /// <param name="isObjNew">is the collection new(determine add or update)</param>
        /// <param name="listIndexObj">collection of indexed object</param>
        public static void AddUpdateIndexedObj(XmlReader xmlReader, bool isObjNew, ref List<Objects1311.indexedObject> listIndexObj)
        {
            bool isUpdateOp = false;
            //collection
            Objects1311.indexedObject indexedObj;
            string currentIndex = string.Empty;

            if (xmlReader.HasAttributes)
            {
                //check for uid attribute and assign it to temporary string for searching in collection
                if (xmlReader.MoveToAttribute("index"))
                {
                    currentIndex = xmlReader.Value;
                    xmlReader.MoveToElement();
                }
            }

            //check whether uid is empty
            if (!string.IsNullOrEmpty(currentIndex))
            {
                //Check Operation to be perform
                if (!isObjNew || listIndexObj.Count > 0)
                {
                    //Update Operation
                    //look for wellDatum based on its UID and update the well with xml string
                    short index = StaticParser.ParseShortFromString(currentIndex);
                    indexedObj = listIndexObj.FirstOrDefault(
                        delegate(Objects1311.indexedObject objIndexed)
                        { if (objIndexed.index == index)return true; else return false; });
                    if (indexedObj != null)
                    {
                        indexedObj.HandleXML(xmlReader.ReadSubtree(), isObjNew);
                        isUpdateOp = true;
                    }
                }

                //add
                if (!isUpdateOp)
                {
                    //Add Operation
                    indexedObj = new Objects1311.indexedObject();
                    indexedObj.HandleXML(xmlReader.ReadSubtree(), isObjNew);
                    listIndexObj.Add(indexedObj);
                }
            }
            else
            {
                //Delete/Clear Operation for update 
                //Skip Operation for Add
                if (!isObjNew)
                {
                    listIndexObj.Clear();
                }
            }
        }

        public static string ConvertToString(Enum e)
        {
            // Get the Type of the enum
            Type t = e.GetType();

            // Get the FieldInfo for the member field with the enums name
            FieldInfo info = t.GetField(e.ToString("G"));

            // Check to see if the XmlEnumAttribute is defined on this field
            if (!info.IsDefined(typeof(XmlEnumAttribute), false))
            {
                // If no XmlEnumAttribute then return the string version of the enum.
                return e.ToString("G");
            }

            // Get the XmlEnumAttribute
            object[] o = info.GetCustomAttributes(typeof(XmlEnumAttribute), false);
            XmlEnumAttribute att = (XmlEnumAttribute)o[0];
            return att.Name;
        }

        public static byte[] ReadByte(XmlReader xmlReader)
        {
            List<byte> value = new List<byte>();
            xmlReader.ReadStartElement();
            if (xmlReader.NodeType != XmlNodeType.None && xmlReader.CanReadBinaryContent)
            {
                byte[] buf = new byte[1024];
                int numRead = 0;
                while ((numRead = xmlReader.ReadContentAsBase64(buf, 0, 1024)) > 0)
                {
                    //fs.Write(buf, 0, numRead);
                    value.AddRange(buf);
                }
            }

            return value.ToArray();
        }

        /*
        /// <summary>
        /// AddUpdate for indexed object for object 1.3.1.1
        /// </summary>
        /// <param name="xmlReader">pass the xmlReader</param>
        /// <param name="isObjNew">is the collection new(determine add or update)</param>
        /// <param name="listIndexObj">collection of indexed object</param>
        public static void AddUpdateIndexedObj(XmlReader xmlReader, bool isObjNew, ref List<Objects140.indexedObject> listIndexObj)
        {
            bool isUpdateOp = false;
            //collection
            Objects140.indexedObject indexedObj;
            string currentIndex = string.Empty;

            if (xmlReader.HasAttributes)
            {
                //check for uid attribute and assign it to temporary string for searching in collection
                if (xmlReader.MoveToAttribute("index"))
                {
                    currentIndex = xmlReader.Value;
                    xmlReader.MoveToElement();
                }
            }

            //check whether uid is empty
            if (!string.IsNullOrEmpty(currentIndex))
            {
                //Check Operation to be perform
                if (!isObjNew || listIndexObj.Count > 0)
                {
                    //Update Operation
                    //look for wellDatum based on its UID and update the well with xml string
                    short index = StaticParser.ParseShortFromString(currentIndex);
                    indexedObj = listIndexObj.FirstOrDefault(
                        delegate(Objects140.indexedObject objIndexed)
                        { if (objIndexed.index == index)return true; else return false; });
                    if (indexedObj != null)
                    {
                        indexedObj.HandleXML(xmlReader.ReadSubtree(), isObjNew);
                        isUpdateOp = true;
                    }
                }

                //add
                if (!isUpdateOp)
                {
                    //Add Operation
                    indexedObj = new Objects140.indexedObject();
                    indexedObj.HandleXML(xmlReader.ReadSubtree(), isObjNew);
                    listIndexObj.Add(indexedObj);
                }
            }
            else
            {
                //Delete/Clear Operation for update 
                //Skip Operation for Add
                if (!isObjNew)
                {
                    listIndexObj.Clear();
                }
            }
        }
         * */
    }
}

