﻿ 

using System;
using System.Collections.Generic;
using System.IO;
using System.Text;
using System.Xml;
using System.Xml.Serialization;
using System.Linq;
using Witsml.Common.Exceptions;


namespace Witsml.Objects140
{
    //Extension class for wellbore document that inherit from WITSMLDocument and Implement Decode Function
    public partial class obj_wellbores : WITSMLDocument
    {

        public override WITSMLObject GetSingleObject(int Item)
        {
            return this.wellbore[Item];
        }

        /// <summary>
        /// Deserialize XML string into WitsmlDocument object
        /// </summary>
        /// <param name="XMLin">XML string to be serialized</param>
        /// <returns>Number of singular wellbore inside</returns>
        public override int Decode(ref string xmlIn)
        {
            //return value
            //number of wellbore inside xmlIn
            int countOfWellbore = 0;
            try
            {
                //check whether collection is from stratch(isNew = true) or from deserialization(isNew=false)
                bool isNew = this.wellbore == null ? true : false;

                //create temporary list to help with data manipulation
                List<obj_wellbore> listWellbore = isNew ? new List<obj_wellbore>() : new List<obj_wellbore>(this.wellbore);

                //Consume XML string and convert node value to variable based on node name
                using (StringReader stringReader = new StringReader(xmlIn))
                {
                    //create setting for reader
                    XmlReaderSettings setting = new XmlReaderSettings();
                    setting.ConformanceLevel = ConformanceLevel.Document;
                    setting.IgnoreComments = true;
                    setting.IgnoreWhitespace = true;
                    setting.IgnoreProcessingInstructions = true;
                    using (XmlReader xmlReader = XmlReader.Create(stringReader, setting))
                    {
                        //assign version
                        xmlReader.MoveToContent();
                        if (xmlReader.MoveToAttribute("version"))
                            this.version = xmlReader.Value;
                        else
                            this.version = "1.4.0";//no version specified,set it to 1.4.0

                        while (xmlReader.Read())
                        {
                            //pre-checking on whether node is the correct child based on depth,start tag and not empty
                            if (xmlReader.Depth == 1 && xmlReader.IsStartElement() && !xmlReader.IsEmptyElement)
                            {
                                switch (xmlReader.Name)
                                {
                                    case "documentInfo":
                                        if (this.documentInfo == null)
                                            this.documentInfo = new cs_documentInfo();
                                        this.documentInfo.HandleXML(xmlReader.ReadSubtree(), isNew);
                                        break;
                                    case "wellbore":
                                        obj_wellbore wellbore;
                                        //foreign key(well id) and primary key(wellbore id) to identify existing wellbore
                                        string currentUIDWell = string.Empty;
                                        string currentUID = string.Empty;

                                        //Update wellbore
                                        if (!isNew)
                                        {
                                            //check for uid attribute and assign it to temporary string for searching in collection
                                            if (xmlReader.HasAttributes)
                                            {
                                                //get parent well id
                                                if (xmlReader.MoveToAttribute("uidWell"))
                                                {
                                                    currentUIDWell = xmlReader.Value;
                                                }
                                                //get wellbore id
                                                if (xmlReader.MoveToAttribute("uid"))
                                                {
                                                    currentUID = xmlReader.Value;
                                                }
                                                xmlReader.MoveToElement();
                                            }
                                            //look for wellbore based on its UID and update the wellbore with xml string
                                            if (currentUID != string.Empty)
                                            {
                                                wellbore = listWellbore.FirstOrDefault(
                                                    delegate(obj_wellbore objWellbore)
                                                    { if (objWellbore.uidWell == currentUIDWell && objWellbore.uid == currentUID)return true; else return false; });
                                                if (wellbore != null)
                                                {
                                                    wellbore.HandleXML(xmlReader.ReadSubtree(), isNew);
                                                    countOfWellbore++;
                                                    continue;
                                                }
                                                else
                                                {
                                                    //Update can't process wellbore without match
                                                    continue;
                                                }
                                            }
                                            else
                                            {
                                                //Update can't process wellbore without uid
                                                continue;
                                            }
                                        }

                                        //Add New wellbore 
                                        wellbore = new obj_wellbore();
                                        wellbore.HandleXML(xmlReader.ReadSubtree(), true);
                                        listWellbore.Add(wellbore);
                                        countOfWellbore++;
                                        break;
                                    default:
                                        break;
                                }
                            }
                        }
                        //assign list to wellbore collection
                        this.wellbore = listWellbore.ToArray();
                    }
                }
            }
            catch (XmlException ex)
            {
                throw new Exceptions.UnableToDecodeException(ex.Message, WITSMLReturnCode.WITSMLDocumentError, ex.InnerException);
            }
            catch (NullReferenceException ex)
            {
                throw new Exceptions.UnableToDecodeException(ex.Message, WITSMLReturnCode.WITSMLEmptyDocument, ex.InnerException);
            }
            catch (Exceptions.UnableToDecodeException ex)
            {
                throw ex;
            }
            catch (Exception ex)
            {
                throw new Exceptions.UnableToDecodeException(ex.Message, WITSMLReturnCode.WITSMLUnkownCause, ex.InnerException);
            }
            return countOfWellbore;
        }

    }
}
