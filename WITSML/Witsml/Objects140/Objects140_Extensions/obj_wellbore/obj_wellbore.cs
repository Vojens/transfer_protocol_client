﻿ 

using System;
using System.IO;
using System.Text;
using System.Xml;
using Witsml.Common.Exceptions;
using Witsml.Exceptions;


namespace Witsml.Objects140
{
    //mdBitCurrent,tvdBitCurrent added 
    [System.Xml.Serialization.XmlRootAttribute("wellbore", Namespace = "http://www.witsml.org/schemas/140", IsNullable = false)]
    public partial class obj_wellbore : WITSMLObject, IHandleXML
    {
        public obj_wellbore()
        {
            this.statusWellbore = WellStatus.unknown;
            this.purposeWellbore = WellPurpose.unknown;
            this.typeWellbore = WellboreType.unknown;
            this.shape = WellboreShape.unknown;
            this.version = WITSMLVersion.V140;
        }

        /// <summary>
        /// Decode XML string into Wellbore
        /// </summary>
        /// <param name="xmlIn">XML string</param>
        public override void Decode(ref string xmlIn)
        {
            try
            {
                //Consume XML string and convert node value to variable based on node name
                using (StringReader stringReader = new StringReader(xmlIn))
                {
                    XmlReaderSettings setting = new XmlReaderSettings();
                    setting.ConformanceLevel = ConformanceLevel.Document;
                    setting.IgnoreComments = true;
                    setting.IgnoreWhitespace = true;
                    setting.IgnoreProcessingInstructions = true;
                    using (XmlReader xmlReader = XmlReader.Create(stringReader, setting))
                    {
                        //determine add or update operation
                        bool isNew;
                        if (this.uid == string.Empty)
                            isNew = true;
                        else isNew = false;

                        this.HandleXML(xmlReader, isNew);
                    }
                }
            }
            catch (XmlException ex)
            {
                throw new Exceptions.UnableToDecodeException(ex.Message, WITSMLReturnCode.WITSMLDocumentError, ex.InnerException);
            }
            catch (NullReferenceException ex)
            {
                throw new Exceptions.UnableToDecodeException(ex.Message, WITSMLReturnCode.WITSMLEmptyDocument, ex.InnerException);
            }
            catch (Exceptions.UnableToDecodeException ex)
            {
                throw ex;
            }
            catch (Exception ex)
            {
                throw new Exceptions.UnableToDecodeException(ex.Message, WITSMLReturnCode.WITSMLUnkownCause, ex.InnerException);
            }
        }

        /// <summary>
        /// Convert XML string data into Wellbore fields value
        /// </summary>
        /// <param name="xmlReader">reader that has Wellbore node</param>
        /// <param name="IsNew">Is the object from stratch(true) or from db/serialization(false)</param>
        public void HandleXML(XmlReader xmlReader, bool isNew)
        {
            using (xmlReader)
            {
                //assign uid for the new well
                xmlReader.MoveToContent();
                if (xmlReader.HasAttributes)
                {
                    if (xmlReader.MoveToAttribute("uidWell"))
                        StaticHelper.HandleUID(xmlReader.Value, "wellbore uidWell", out this.uidWell);
                    if (xmlReader.MoveToAttribute("uid"))
                        StaticHelper.HandleUID(xmlReader.Value, "wellbore uid", out this.uid);
                    xmlReader.MoveToElement();
                }
                while (xmlReader.Read())
                {
                    //only take child node
                    if (xmlReader.Depth == 1 && xmlReader.IsStartElement() && !xmlReader.IsEmptyElement)
                    {
                        switch (xmlReader.Name)
                        {
                            case "nameWell":
                                this.nameWell = StaticHelper.ReadString(xmlReader);
                                break;
                            case "name":
                                this.name = StaticHelper.ReadString(xmlReader);
                                break;
                            case "parentWellbore":
                                if (this.parentWellbore == null)
                                    this.parentWellbore = new refNameString();
                                this.parentWellbore.ObjectName = "parentWellbore";
                                this.parentWellbore.HandleXML(xmlReader.ReadSubtree(), isNew);
                                break;
                            case "number":
                                this.number = StaticHelper.ReadString(xmlReader);
                                break;
                            case "suffixAPI":
                                this.suffixAPI = StaticHelper.ReadString(xmlReader);
                                break;
                            case "numGovt":
                                this.numGovt = StaticHelper.ReadString(xmlReader);
                                break;
                            case "statusWellbore":
                                StaticParser.SetEnumFromString<WellStatus>(StaticHelper.ReadString(xmlReader), out this.statusWellbore, out this.statusWellboreSpecified);
                                break;
                            case "isActive":
                                this.isActive = StaticParser.ParseBooleanFromString(StaticHelper.ReadString(xmlReader));
                                this.isActiveSpecified = true;
                                break;
                            case "purposeWellbore":
                                StaticParser.SetEnumFromString<WellPurpose>(StaticHelper.ReadString(xmlReader), out this.purposeWellbore, out this.purposeWellboreSpecified);
                                break;
                            case "typeWellbore":
                                StaticParser.SetEnumFromString<WellboreType>(StaticHelper.ReadString(xmlReader), out this.typeWellbore, out this.typeWellboreSpecified);
                                break;
                            case "shape":
                                StaticParser.SetEnumFromString<WellboreShape>(StaticHelper.ReadString(xmlReader), out this.shape, out this.shapeSpecified);
                                break;
                            case "dTimKickoff":
                                this.dTimKickoff = StaticParser.ParseDateTimeFromString(StaticHelper.ReadString(xmlReader));
                                this.dTimKickoffSpecified = true;
                                break;
                            case "achievedTD":
                                this.achievedTD = StaticParser.ParseBooleanFromString(StaticHelper.ReadString(xmlReader));
                                this.achievedTDSpecified = true;
                                break;
                            case "mdCurrent":
                                if (this.mdCurrent == null)
                                    this.mdCurrent = new measuredDepthCoord();
                                this.mdCurrent.HandleXML(xmlReader.ReadSubtree(), isNew);
                                break;
                            case "tvdCurrent":
                                if (this.tvdCurrent == null)
                                    this.tvdCurrent = new wellVerticalDepthCoord();
                                this.tvdCurrent.HandleXML(xmlReader.ReadSubtree(), isNew);
                                break;
                            case "mdBitCurrent":
                                if (this.mdBitCurrent == null)
                                    this.mdBitCurrent = new measuredDepthCoord();
                                this.mdBitCurrent.HandleXML(xmlReader.ReadSubtree(), isNew);
                                break;
                            case "tvdBitCurrent":
                                if (this.tvdBitCurrent == null)
                                    this.tvdBitCurrent = new wellVerticalDepthCoord();
                                this.tvdBitCurrent.HandleXML(xmlReader.ReadSubtree(), isNew);
                                break;
                            case "mdKickoff":
                                if (this.mdKickoff == null)
                                    this.mdKickoff = new measuredDepthCoord();
                                this.mdKickoff.HandleXML(xmlReader.ReadSubtree(), isNew);
                                break;
                            case "tvdKickoff":
                                if (this.tvdKickoff == null)
                                    this.tvdKickoff = new wellVerticalDepthCoord();
                                this.tvdKickoff.HandleXML(xmlReader.ReadSubtree(), isNew);
                                break;
                            case "mdPlanned":
                                if (this.mdPlanned == null)
                                    this.mdPlanned = new measuredDepthCoord();
                                this.mdPlanned.HandleXML(xmlReader.ReadSubtree(), isNew);
                                break;
                            case "tvdPlanned":
                                if (this.tvdPlanned == null)
                                    this.tvdPlanned = new wellVerticalDepthCoord();
                                this.tvdPlanned.HandleXML(xmlReader.ReadSubtree(), isNew);
                                break;
                            case "mdSubSeaPlanned":
                                if (this.mdSubSeaPlanned == null)
                                    this.mdSubSeaPlanned = new measuredDepthCoord();
                                this.mdSubSeaPlanned.HandleXML(xmlReader.ReadSubtree(), isNew);
                                break;
                            case "tvdSubSeaPlanned":
                                if (this.tvdSubSeaPlanned == null)
                                    this.tvdSubSeaPlanned = new wellVerticalDepthCoord();
                                this.tvdSubSeaPlanned.HandleXML(xmlReader.ReadSubtree(), isNew);
                                break;
                            case "dayTarget":
                                if (this.dayTarget == null)
                                    this.dayTarget = new timeMeasure();
                                this.dayTarget.HandleXML(xmlReader.ReadSubtree(), isNew);
                                break;
                            case "commonData":
                                if (this.commonData == null)
                                    this.commonData = new cs_commonData();
                                this.commonData.HandleXML(xmlReader.ReadSubtree(), isNew);
                                break;
                            case "customData":
                                if (this.customData == null)
                                    this.customData = new cs_customData();
                                this.customData.HandleXML(xmlReader.ReadSubtree(), isNew);
                                break;
                            default:
                                break;
                        }
                    }
                }
            }
        }

        public override string ValidateGetPath(string newUid)
        {
            // Fix uid if the Object dosn't have one.
            if (!IsValidUid(this.uid))
            {
                this.uid = newUid;
                // TODO: Add some loging that we created a new UID for the object maybe.
            }

            StringBuilder path = new StringBuilder(this.GetParentPath());
            path.Append(@"\");
            path.Append(this.uid);
            return path.ToString();
        }

        public override string GetParentPath()
        {
            if (!IsValidUid(this.uidWell))
            {
                throw new UnableToDecodeException("One or more Parent UID's are invalid", WITSMLReturnCode.WITSMLInvalidParentUID);
                //TODO: Log the issue
            }

            StringBuilder path = new StringBuilder(@"\");
            path.Append(this.uidWell);
            return path.ToString();
        }

        // NOTE: these are done with functions, instead of property access, so as not to mess around with the auto-generated / serialized classes.
        public override string Get_uid()
        {
            return this.uid;
        }

        public override string Get_uidWell()
        {
            return this.uidWell;
        }

        public override string Get_uidWellbore()
        {
            return this.uid;
        }

        public override string Get_name()
        {
            return this.name;
        }

        public override string Get_nameWell()
        {
            return this.nameWell;
        }

        public override string Get_nameWellbore()
        {
            return this.name;
        }

        public override ICommonData Get_commonData()
        {
            if (this.commonData == null)
            {
                this.commonData = new cs_commonData();
            }
            return (ICommonData)this.commonData;
        }
        public override ICustomData Get_customData()
        {
            if (this.customData == null)
            {
                this.customData = new cs_customData();
            }
            return (ICustomData)this.customData;
        }
    }
}
