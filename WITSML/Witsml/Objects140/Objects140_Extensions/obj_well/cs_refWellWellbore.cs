﻿ 

using System;
using System.Xml;

namespace Witsml.Objects140
{
    public partial class cs_refWellWellbore : IHandleXML
    {
        /// <summary>
        /// Convert XML string data into wellbore fields value
        /// </summary>
        /// <param name="xmlReader">reader that has wellbore node</param>
        /// <param name="IsNew">Is the object from stratch(true) or from db/serialization(false)</param>
        public void HandleXML(XmlReader xmlReader, bool isNew)
        {
            //Read all nodes and assign the value to related fields
            using (xmlReader)
            {
                while (xmlReader.Read())
                {
                    if (xmlReader.Depth == 1 && xmlReader.IsStartElement() && !xmlReader.IsEmptyElement)
                    {
                        switch (xmlReader.Name)
                        {
                            case "wellboreReference":
                                if (this.wellboreReference == null)
                                    this.wellboreReference = new refNameString();
                                this.wellboreReference.ObjectName = "wellboreReference";
                                this.wellboreReference.HandleXML(xmlReader.ReadSubtree(), isNew);
                                break;
                            case "wellParent":
                                if (this.wellParent == null)
                                    this.wellParent = new refNameString();
                                this.wellParent.ObjectName = "wellParent";
                                this.wellParent.HandleXML(xmlReader.ReadSubtree(), isNew);
                                break;
                            default:
                                break;
                        }

                    }
                }
            }
        }
    }
}
