﻿ 

using System;
using System.Xml;

namespace Witsml.Objects140
{
    public partial class cs_refWellWellboreRig : IHandleXML
    {
        /// <summary>
        /// Convert XML string data into Rig fields value
        /// </summary>
        /// <param name="xmlReader">reader that has Rig node</param>
        /// <param name="IsNew">Is the object from stratch(true) or from db/serialization(false)</param>
        public void HandleXML(XmlReader xmlReader, bool isNew)
        {
            //Read all nodes and assign the value to related fields
            using (xmlReader)
            {
                while (xmlReader.Read())
                {
                    if (xmlReader.Depth == 1 && xmlReader.IsStartElement() && !xmlReader.IsEmptyElement)
                    {
                        switch (xmlReader.Name)
                        {
                            case "rigReference":
                                if (this.rigReference == null)
                                    this.rigReference = new refNameString();
                                this.rigReference.ObjectName = "rigReference";
                                this.rigReference.HandleXML(xmlReader.ReadSubtree(), isNew);
                                break;
                            case "wellboreParent":
                                if (this.wellboreParent == null)
                                    this.wellboreParent = new refNameString();
                                this.wellboreParent.ObjectName = "wellboreParent";
                                this.wellboreParent.HandleXML(xmlReader.ReadSubtree(), isNew);
                                break;
                            case "wellParent":
                                if (this.wellParent == null)
                                    this.wellParent = new refNameString();
                                this.wellParent.ObjectName = "wellParent";
                                this.wellParent.HandleXML(xmlReader.ReadSubtree(), isNew);
                                break;
                            default:
                                break;
                        }

                    }
                }
            }
        }
    }
}
