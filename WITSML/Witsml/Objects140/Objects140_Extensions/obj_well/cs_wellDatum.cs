﻿ 

using System;
using System.Collections.Generic;
using System.Xml;

namespace Witsml.Objects140
{
    public partial class cs_wellDatum : IHandleXML
    {
        public cs_wellDatum()
        {
            this.code = ElevCodeEnum.unknown;
        }

        /// <summary>
        /// Convert XML string data into WellDatum fields value
        /// </summary>
        /// <param name="xmlReader">reader that has WellDatum node</param>
        /// <param name="IsNew">Is the object from stratch(true) or from db/serialization(false)</param>
        public void HandleXML(XmlReader xmlReader, bool isNew)
        {
            bool hasChoice = false;
            //Create temporary list to help with data manipulation
            List<string> listKind = this.kind == null ? new List<string>() : new List<string>(this.kind);
            using (xmlReader)
            {
                //read first node and assign uid and other attribute of the object
                xmlReader.MoveToContent();
                if (xmlReader.HasAttributes)
                {
                    if (xmlReader.MoveToAttribute("uid"))
                        StaticHelper.HandleUID(xmlReader.Value, "wellDatum uid", out this.uid);
                    else
                        this.uid = StaticHelper.GenerateObjectUID();
                    if (xmlReader.MoveToAttribute("defaultElevation"))
                    {
                        this.defaultElevation = StaticParser.ParseBooleanFromString(xmlReader.Value);
                        this.defaultElevationSpecified = true;
                    }
                    if (xmlReader.MoveToAttribute("defaultMeasuredDepth"))
                    {
                        this.defaultMeasuredDepth = StaticParser.ParseBooleanFromString(xmlReader.Value);
                        this.defaultMeasuredDepthSpecified = true;
                    }
                    if (xmlReader.MoveToAttribute("defaultVerticalDepth"))
                    {
                        this.defaultVerticalDepth = StaticParser.ParseBooleanFromString(xmlReader.Value);
                        this.defaultVerticalDepthSpecified = true;
                    }

                    xmlReader.MoveToElement();
                }
                while (xmlReader.Read())
                {
                    //start on depth 1 which are the childs that we want to check against
                    //check for starting tag and convert node value to its expected variable
                    if (xmlReader.Depth == 1 && xmlReader.IsStartElement() && !xmlReader.IsEmptyElement)
                    {
                        switch (xmlReader.Name)
                        {
                            case "name":
                                this.name = StaticHelper.ReadString(xmlReader);
                                break;
                            case "code":
                                StaticParser.SetEnumFromString<ElevCodeEnum>(StaticHelper.ReadString(xmlReader), out this.code, out this.codeSpecified);
                                break;
                            case "datumCRS":
                                if (!hasChoice)
                                {
                                    refNameString datumCRS = new refNameString();
                                    datumCRS.HandleXML(xmlReader.ReadSubtree(), isNew);
                                    this.Item = datumCRS;
                                    hasChoice = true;
                                }
                                break;
                            case "datumName":
                                if (!hasChoice)
                                {
                                    wellKnownNameStruct datumName = new wellKnownNameStruct();
                                    datumName.HandleXML(xmlReader.ReadSubtree(), isNew);
                                    this.Item = datumName;
                                    hasChoice = true;
                                }
                                break;
                            case "kind":
                                //multiple string
                                if (StaticHelper.ReadString(xmlReader) != string.Empty)
                                    if (!listKind.Contains(StaticHelper.ReadString(xmlReader)))
                                        listKind.Add(StaticHelper.ReadString(xmlReader));
                                break;
                            case "wellbore":
                                if (this.wellbore == null)
                                    this.wellbore = new cs_refWellWellbore();
                                this.wellbore.HandleXML(xmlReader.ReadSubtree(), isNew);
                                break;
                            case "rig":
                                if (this.rig == null)
                                    this.rig = new cs_refWellWellboreRig();
                                this.rig.HandleXML(xmlReader.ReadSubtree(), isNew);
                                break;
                            case "elevation":
                                if (this.elevation == null)
                                    this.elevation = new wellElevationCoord();
                                this.elevation.HandleXML(xmlReader.ReadSubtree(), isNew);
                                break;
                            case "measuredDepth":
                                if (this.measuredDepth == null)
                                    this.measuredDepth = new measuredDepthCoord();
                                this.measuredDepth.HandleXML(xmlReader.ReadSubtree(), isNew);
                                break;
                            case "horizontalLocation":
                                if (this.horizontalLocation == null)
                                    this.horizontalLocation = new cs_location();
                                this.horizontalLocation.HandleXML(xmlReader.ReadSubtree(), isNew);
                                break;
                            case "comment":
                                this.comment = StaticHelper.ReadString(xmlReader);
                                break;
                            default:
                                break;
                        }

                    }
                }
                //assign copy of list back to collection
                this.kind = listKind.ToArray();
            }
        }
    }
}
