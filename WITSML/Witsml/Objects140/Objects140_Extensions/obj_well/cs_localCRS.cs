﻿ 

using System;
using System.Collections.Generic;
using System.Xml;

namespace Witsml.Objects140
{
    public partial class cs_localCRS : IHandleXML
    {
        /// <summary>
        /// Convert XML string data into cs_localCRS fields value
        /// </summary>
        /// <param name="xmlReader">reader that has cs_localCRS node</param>
        /// <param name="IsNew">Is the object from stratch(true) or from db/serialization(false)</param>
        public void HandleXML(XmlReader xmlReader, bool isNew)
        {
            //condition for allowing only one of origin,originDescription,usesWellAsOrigin to represent member item
            bool hasChoice = false;

            using (xmlReader)
            {
                while (xmlReader.Read())
                {
                    if (xmlReader.Depth == 1 && xmlReader.IsStartElement() && !xmlReader.IsEmptyElement)
                    {
                        switch (xmlReader.Name)
                        {
                            //Choice
                            case "origin":
                                if (!hasChoice)
                                {
                                    refNameString refName = new refNameString();
                                    refName.ObjectName = "origin";
                                    refName.HandleXML(xmlReader.ReadSubtree(), isNew);
                                    this.Item = refName;
                                    hasChoice = true;
                                }
                                break;
                            case "originDescription":
                                if (!hasChoice)
                                {
                                    this.Item = StaticHelper.ReadString(xmlReader);
                                    hasChoice = true;
                                }
                                break;
                            case "usesWellAsOrigin":
                                if (!hasChoice)
                                {
                                    this.Item = StaticParser.ParseBooleanFromString(StaticHelper.ReadString(xmlReader));
                                    hasChoice = true;
                                }
                                break;
                            //End Choice
                            case "yAxisAzimuth":
                                if (this.yAxisAzimuth == null)
                                    this.yAxisAzimuth = new yAxisAzimuth();
                                this.yAxisAzimuth.HandleXML(xmlReader.ReadSubtree(), isNew);
                                break;
                            case "magneticDeclination":
                                if (this.magneticDeclination == null)
                                    this.magneticDeclination = new planeAngleMeasure();
                                this.magneticDeclination.HandleXML(xmlReader.ReadSubtree(), isNew);
                                break;
                            case "gridConvergence":
                                if (this.gridConvergence == null)
                                    this.gridConvergence = new planeAngleMeasure();
                                this.gridConvergence.HandleXML(xmlReader.ReadSubtree(), isNew);
                                break;
                            case "yAxisDescription":
                                this.yAxisDescription = StaticHelper.ReadString(xmlReader);
                                break;
                            case "xRotationCounterClockwise":
                                this.xRotationCounterClockwise = StaticParser.ParseBooleanFromString(StaticHelper.ReadString(xmlReader));
                                this.xRotationCounterClockwiseSpecified = true;
                                break;
                            default:
                                break;
                        }
                    }
                }
            }
        }
    }
}
