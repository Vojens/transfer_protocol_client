﻿ 

using System;
using System.Collections.Generic;
using System.Xml;

namespace Witsml.Objects140
{
    // TODO : is parameter right?
    public partial class cs_projectionx : IHandleXML
    {
        public cs_projectionx()
        {
            this.projectionCode = Projection.unknown;
            this.methodVariant = ProjectionVariantsObliqueMercator.unknown;
            this.NADType = NADTypes.unknown;
            this.hemisphere = Hemispheres.unknown;
        }

        /// <summary>
        /// Convert XML string data into cs_projectionx fields value
        /// </summary>
        /// <param name="xmlReader">reader that has cs_projectionx node</param>
        /// <param name="IsNew">Is the object from stratch(true) or from db/serialization(false)</param>
        public void HandleXML(XmlReader xmlReader, bool isNew)
        {
            //check for existence of object with UID collection,to be used to determine add and update
            bool isParameterNew = this.parameter == null;
            //Create temp list to handle data manipulation
            List<indexedObject> listParameter = this.parameter == null ? new List<indexedObject>() : new List<indexedObject>(this.parameter);

            using (xmlReader)
            {
                while (xmlReader.Read())
                {
                    if (xmlReader.Depth == 1 && xmlReader.IsStartElement() && !xmlReader.IsEmptyElement)
                    {
                        switch (xmlReader.Name)
                        {
                            case "nameCRS":
                                if (this.nameCRS == null)
                                    this.nameCRS = new wellKnownNameStruct();
                                this.nameCRS.HandleXML(xmlReader.ReadSubtree(), isNew);
                                break;
                            case "projectionCode":
                                StaticParser.SetEnumFromString<Projection>(StaticHelper.ReadString(xmlReader), out this.projectionCode, out this.projectionCodeSpecified);
                                break;
                            case "projectedFrom":
                                if (this.projectedFrom == null)
                                    this.projectedFrom = new refNameString();
                                this.projectedFrom.ObjectName = "projectedFrom";
                                this.projectedFrom.HandleXML(xmlReader.ReadSubtree(), isNew);
                                break;
                            case "stdParallel1":
                                if (this.stdParallel1 == null)
                                    this.stdParallel1 = new planeAngleMeasure();
                                this.stdParallel1.HandleXML(xmlReader.ReadSubtree(), isNew);
                                break;
                            case "stdParallel2":
                                if (this.stdParallel2 == null)
                                    this.stdParallel2 = new planeAngleMeasure();
                                this.stdParallel2.HandleXML(xmlReader.ReadSubtree(), isNew);
                                break;
                            case "centralMeridian":
                                if (this.centralMeridian == null)
                                    this.centralMeridian = new planeAngleMeasure();
                                this.centralMeridian.HandleXML(xmlReader.ReadSubtree(), isNew);
                                break;
                            case "originLatitude":
                                if (this.originLatitude == null)
                                    this.originLatitude = new planeAngleMeasure();
                                this.originLatitude.HandleXML(xmlReader.ReadSubtree(), isNew);
                                break;
                            case "originLongitude":
                                if (this.stdParallel1 == null)
                                    this.stdParallel1 = new planeAngleMeasure();
                                this.stdParallel1.HandleXML(xmlReader.ReadSubtree(), isNew);
                                break;
                            case "latitude1":
                                if (this.latitude1 == null)
                                    this.latitude1 = new planeAngleMeasure();
                                this.latitude1.HandleXML(xmlReader.ReadSubtree(), isNew);
                                break;
                            case "longitude1":
                                if (this.longitude1 == null)
                                    this.longitude1 = new planeAngleMeasure();
                                this.longitude1.HandleXML(xmlReader.ReadSubtree(), isNew);
                                break;
                            case "latitude2":
                                if (this.latitude2 == null)
                                    this.latitude2 = new planeAngleMeasure();
                                this.latitude2.HandleXML(xmlReader.ReadSubtree(), isNew);
                                break;
                            case "longitude2":
                                if (this.longitude2 == null)
                                    this.longitude2 = new planeAngleMeasure();
                                this.longitude2.HandleXML(xmlReader.ReadSubtree(), isNew);
                                break;
                            case "latitudeForScale":
                                if (this.latitudeForScale == null)
                                    this.latitudeForScale = new planeAngleMeasure();
                                this.latitudeForScale.HandleXML(xmlReader.ReadSubtree(), isNew);
                                break;
                            case "longitudeForScale":
                                if (this.longitudeForScale == null)
                                    this.longitudeForScale = new planeAngleMeasure();
                                this.longitudeForScale.HandleXML(xmlReader.ReadSubtree(), isNew);
                                break;
                            case "trueScaleLatitude":
                                if (this.trueScaleLatitude == null)
                                    this.trueScaleLatitude = new planeAngleMeasure();
                                this.trueScaleLatitude.HandleXML(xmlReader.ReadSubtree(), isNew);
                                break;
                            case "spheroidRadius":
                                if (this.spheroidRadius == null)
                                    this.spheroidRadius = new lengthMeasure();
                                this.spheroidRadius.HandleXML(xmlReader.ReadSubtree(), isNew);
                                break;
                            case "scaleFactor":
                                this.scaleFactor = StaticParser.ParseDoubleFromString(StaticHelper.ReadString(xmlReader));
                                this.scaleFactorSpecified = true;
                                break;
                            case "methodVariant":
                                StaticParser.SetEnumFromString<ProjectionVariantsObliqueMercator>(StaticHelper.ReadString(xmlReader), out this.methodVariant, out this.methodVariantSpecified);
                                break;
                            case "perspectiveHeight":
                                if (this.perspectiveHeight == null)
                                    this.perspectiveHeight = new lengthMeasure();
                                this.perspectiveHeight.HandleXML(xmlReader.ReadSubtree(), isNew);
                                break;
                            case "zone":
                                this.zone = StaticHelper.ReadString(xmlReader);
                                break;
                            case "NADType":
                                StaticParser.SetEnumFromString<NADTypes>(StaticHelper.ReadString(xmlReader), out this.NADType, out this.NADTypeSpecified);
                                break;
                            case "falseEasting":
                                if (this.falseEasting == null)
                                    this.falseEasting = new lengthMeasure();
                                this.falseEasting.HandleXML(xmlReader.ReadSubtree(), isNew);
                                break;
                            case "falseNorthing":
                                if (this.falseNorthing == null)
                                    this.falseNorthing = new lengthMeasure();
                                this.falseNorthing.HandleXML(xmlReader.ReadSubtree(), isNew);
                                break;
                            case "bearing":
                                if (this.bearing == null)
                                    this.bearing = new planeAngleMeasure();
                                this.bearing.HandleXML(xmlReader.ReadSubtree(), isNew);
                                break;
                            case "hemisphere":
                                StaticParser.SetEnumFromString<Hemispheres>(StaticHelper.ReadString(xmlReader), out this.hemisphere, out this.hemisphereSpecified);
                                break;
                            case "description":
                                this.description = StaticHelper.ReadString(xmlReader);
                                break;
                            case "parameter":
                                //multiple objects with uid
                                //StaticHelper.AddUpdateIndexedObj(xmlReader, isParameterNew, ref listParameter);
                                break;
                            default:
                                break;
                        }
                    }
                }
                //assing list parameter to member parameters
                this.parameter = listParameter.ToArray();
            }
        }
    }
}
