﻿ 

using System;
using System.Xml;

namespace Witsml.Objects140
{
    public partial class cs_location : IHandleXML, IHasID
    {
        [System.Xml.Serialization.XmlIgnore]
        public string UID
        {
            get { return this.uid; }
        }
        /// <summary>
        /// Convert XML string data into Location fields value
        /// </summary>
        /// <param name="xmlReader">reader that has Location node</param>
        /// <param name="IsNew">Is the object from stratch(true) or from db/serialization(false)</param>
        public void HandleXML(XmlReader xmlReader, bool isNew)
        {
            //init enum that will be used to keep track of choice made 
            localChoice choice = localChoice.unknown;
            //initialize initial capacity of collection that related with choice(which means maximum 2 member inside the collection)
            int arryIndex = 0;
            this.Items = new abstractMeasure[2];
            this.ItemsElementName = new ItemsChoiceType[2];

            //Read all nodes and assign the value to related fields
            using (xmlReader)
            {
                xmlReader.MoveToContent();
                if (xmlReader.HasAttributes)
                {
                    if (xmlReader.MoveToAttribute("uid"))
                        StaticHelper.HandleUID(xmlReader.Value, "location uid", out this.uid);
                    else
                        this.uid = StaticHelper.GenerateObjectUID();
                    xmlReader.MoveToElement();
                }
                while (xmlReader.Read())
                {
                    if (xmlReader.Depth == 1 && xmlReader.IsStartElement() && !xmlReader.IsEmptyElement)
                    {
                        switch (xmlReader.Name)
                        {
                            case "wellCRS":
                                if (this.wellCRS == null)
                                    this.wellCRS = new refNameString();
                                this.wellCRS.ObjectName = "wellCRS";
                                this.wellCRS.HandleXML(xmlReader.ReadSubtree(), isNew);
                                break;
                            //Case for Choice
                            //1.latitude and longitude(lalo)
                            //2.easting and northing(eano)
                            //3.westing and southing(weso)
                            //4.projectedX and projectedY
                            //5.localX and localY
                            case "latitude":
                                //valid only if Choice is not determine yet or has specified as a specific choice
                                if (choice == localChoice.unknown || choice == localChoice.lalo)
                                {
                                    if (arryIndex < 2)
                                    {
                                        planeAngleMeasure latitude = new planeAngleMeasure();
                                        latitude.HandleXML(xmlReader.ReadSubtree(), isNew);
                                        this.Items.SetValue(latitude, arryIndex);
                                        this.ItemsElementName.SetValue(ItemsChoiceType.latitude, arryIndex);
                                        arryIndex++;
                                    }
                                    choice = localChoice.lalo;
                                }
                                break;
                            case "longitude":
                                //valid only if Choice is not determine yet or has specified as a specific choice
                                if (choice == localChoice.unknown || choice == localChoice.lalo)
                                {
                                    if (arryIndex < 2)
                                    {
                                        planeAngleMeasure longitude = new planeAngleMeasure();
                                        longitude.HandleXML(xmlReader.ReadSubtree(), isNew);
                                        this.Items.SetValue(longitude, arryIndex);
                                        this.ItemsElementName.SetValue(ItemsChoiceType.longitude, arryIndex);
                                        arryIndex++;
                                    }
                                    choice = localChoice.lalo;
                                }
                                break;
                            case "easting":
                                //valid only if Choice is not determine yet or has specified as a specific choice
                                if (choice == localChoice.unknown || choice == localChoice.eano)
                                {
                                    if (arryIndex < 2)
                                    {
                                        lengthMeasure easting = new lengthMeasure();
                                        easting.HandleXML(xmlReader.ReadSubtree(), isNew);
                                        this.Items.SetValue(easting, arryIndex);
                                        this.ItemsElementName.SetValue(ItemsChoiceType.easting, arryIndex);
                                        arryIndex++;
                                    }
                                    choice = localChoice.eano;
                                }
                                break;
                            case "northing":
                                //valid only if Choice is not determine yet or has specified as a specific choice
                                if (choice == localChoice.unknown || choice == localChoice.eano)
                                {
                                    if (arryIndex < 2)
                                    {
                                        lengthMeasure northing = new lengthMeasure();
                                        northing.HandleXML(xmlReader.ReadSubtree(), isNew);
                                        this.Items.SetValue(northing, arryIndex);
                                        this.ItemsElementName.SetValue(ItemsChoiceType.northing, arryIndex);
                                        arryIndex++;
                                    }
                                    choice = localChoice.eano;
                                }
                                break;
                            case "westing":
                                //valid only if Choice is not determine yet or has specified as a specific choice
                                if (choice == localChoice.unknown || choice == localChoice.weso)
                                {
                                    if (arryIndex < 2)
                                    {
                                        lengthMeasure westing = new lengthMeasure();
                                        westing.HandleXML(xmlReader.ReadSubtree(), isNew);
                                        this.Items.SetValue(westing, arryIndex);
                                        this.ItemsElementName.SetValue(ItemsChoiceType.westing, arryIndex);
                                        arryIndex++;
                                    }
                                    choice = localChoice.weso;
                                }
                                break;
                            case "southing":
                                //valid only if Choice is not determine yet or has specified as a specific choice
                                if (choice == localChoice.unknown || choice == localChoice.weso)
                                {
                                    if (arryIndex < 2)
                                    {
                                        lengthMeasure southing = new lengthMeasure();
                                        southing.HandleXML(xmlReader.ReadSubtree(), isNew);
                                        this.Items.SetValue(southing, arryIndex);
                                        this.ItemsElementName.SetValue(ItemsChoiceType.southing, arryIndex);
                                        arryIndex++;
                                    }
                                    choice = localChoice.weso;
                                }
                                break;
                            case "projectedX":
                                //valid only if Choice is not determine yet or has specified as a specific choice
                                if (choice == localChoice.unknown || choice == localChoice.prpr)
                                {
                                    if (arryIndex < 2)
                                    {
                                        lengthMeasure projectedX = new lengthMeasure();
                                        projectedX.HandleXML(xmlReader.ReadSubtree(), isNew);
                                        this.Items.SetValue(projectedX, arryIndex);
                                        this.ItemsElementName.SetValue(ItemsChoiceType.projectedX, arryIndex);
                                        arryIndex++;
                                    }
                                    choice = localChoice.prpr;
                                }
                                break;
                            case "projectedY":
                                //valid only if Choice is not determine yet or has specified as a specific choice
                                if (choice == localChoice.unknown || choice == localChoice.prpr)
                                {
                                    if (arryIndex < 2)
                                    {
                                        lengthMeasure projectedY = new lengthMeasure();
                                        projectedY.HandleXML(xmlReader.ReadSubtree(), isNew);
                                        this.Items.SetValue(projectedY, arryIndex);
                                        this.ItemsElementName.SetValue(ItemsChoiceType.projectedY, arryIndex);
                                        arryIndex++;
                                    }
                                    choice = localChoice.prpr;
                                }
                                break;
                            case "localX":
                                //valid only if Choice is not determine yet or has specified as a specific choice
                                if (choice == localChoice.unknown || choice == localChoice.lolo)
                                {
                                    if (arryIndex < 2)
                                    {
                                        lengthMeasure localX = new lengthMeasure();
                                        localX.HandleXML(xmlReader.ReadSubtree(), isNew);
                                        this.Items.SetValue(localX, arryIndex);
                                        this.ItemsElementName.SetValue(ItemsChoiceType.localX, arryIndex);
                                        arryIndex++;
                                    }
                                    choice = localChoice.lolo;
                                }
                                break;
                            case "localY":
                                //valid only if Choice is not determine yet or has specified as a specific choice
                                if (choice == localChoice.unknown || choice == localChoice.lolo)
                                {
                                    if (arryIndex < 2)
                                    {
                                        lengthMeasure localY = new lengthMeasure();
                                        localY.HandleXML(xmlReader.ReadSubtree(), isNew);
                                        this.Items.SetValue(localY, arryIndex);
                                        this.ItemsElementName.SetValue(ItemsChoiceType.localY, arryIndex);
                                        arryIndex++;
                                    }
                                    choice = localChoice.lolo;
                                }
                                break;
                            //End Case for Choice
                            case "original":
                                this.original = StaticParser.ParseBooleanFromString(StaticHelper.ReadString(xmlReader));
                                this.originalSpecified = true;
                                break;
                            case "description":
                                this.description = StaticHelper.ReadString(xmlReader);
                                break;
                            default:
                                break;
                        }
                    }
                }
                //check collection to see whether choice items contain valid value(0 or 2) and reset it if the total value incorrect
                if (arryIndex == 1)
                {
                    this.Items = null;
                    this.ItemsElementName = null;
                }

            }
        }

        //enum for choice(member this.Items) in this class
        private enum localChoice
        {
            //choice can only be
            lalo,//latitude and longitude
            eano,//easting and northing
            weso,//westing and southing
            prpr,//projectedX and projectedY
            lolo,//localX and localY
            unknown//choice not determinde yet
        }
    }
}
