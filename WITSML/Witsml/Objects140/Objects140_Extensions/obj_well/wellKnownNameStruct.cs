﻿ 

using System;
using System.Xml;

namespace Witsml.Objects140
{
    public partial class wellKnownNameStruct : IHandleXML
    {
        /// <summary>
        /// Convert XML string data into wellKnownNameStruct fields value
        /// </summary>
        /// <param name="xmlReader">reader that has wellKnownNameStruct node</param>
        /// <param name="IsNew">Is the object from stratch(true) or from db/serialization(false)</param>
        public void HandleXML(XmlReader xmlReader, bool isNew)
        {
            using (xmlReader)
            {
                xmlReader.Read();
                if (xmlReader.HasAttributes)
                {
                    if (xmlReader.MoveToAttribute("namingSystem"))
                        this.namingSystem = xmlReader.Value;
                    if (xmlReader.MoveToAttribute("code"))
                        this.code = xmlReader.Value;
                    xmlReader.MoveToElement();
                }
                this.Value = StaticHelper.ReadString(xmlReader);
            }
        }
    }
}
