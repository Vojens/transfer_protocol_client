﻿ 

using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Xml;

namespace Witsml.Objects140
{
    public partial class cs_publicLandSurveySystem
    {
        public cs_publicLandSurveySystem()
        {
            this.principalMeridian = PrincipalMeridian.unknown;
            this.rangeDir = EastOrWest.unknown;
            this.townshipDir = NorthOrSouth.unknown;
        }
        /// <summary>
        /// Convert XML string data into cs_publicLandSurveySystem fields value
        /// </summary>
        /// <param name="xmlReader">reader that has cs_localCRS node</param>
        /// <param name="IsNew">Is the object from stratch(true) or from db/serialization(false)</param>
        public void HandleXML(XmlReader xmlReader, bool isNew)
        {
            using (xmlReader)
            {
                while (xmlReader.Read())
                {
                    if (xmlReader.Depth == 1 && xmlReader.IsStartElement() && !xmlReader.IsEmptyElement)
                    {
                        switch (xmlReader.Name)
                        {
                            case "principalMeridian":
                                StaticParser.SetEnumFromString<PrincipalMeridian>(StaticHelper.ReadString(xmlReader), out this.principalMeridian, out this.principalMeridianSpecified);
                                break;
                            case "range":
                                this.range = StaticParser.ParseShortFromString(StaticHelper.ReadString(xmlReader));
                                this.rangeSpecified = true;
                                break;
                            case "rangeDir":
                                StaticParser.SetEnumFromString<EastOrWest>(StaticHelper.ReadString(xmlReader), out this.rangeDir, out this.rangeDirSpecified);
                                break;
                            case "township":
                                this.township = StaticParser.ParseShortFromString(StaticHelper.ReadString(xmlReader));
                                this.townshipSpecified = true;
                                break;
                            case "townshipDir":
                                StaticParser.SetEnumFromString<NorthOrSouth>(StaticHelper.ReadString(xmlReader), out this.townshipDir, out this.townshipDirSpecified);
                                break;
                            case "section":
                                this.section = StaticHelper.ReadString(xmlReader);
                                break;
                            case "quarterSection":
                                this.quarterSection = StaticHelper.ReadString(xmlReader);
                                break;
                            case "quarterTownship":
                                this.quarterTownship = StaticHelper.ReadString(xmlReader);
                                break;
                            case "footageNS":
                                if (this.footageNS == null)
                                    this.footageNS = new footageNorthSouth();
                                this.footageNS.HandleXML(xmlReader.ReadSubtree(), isNew);
                                break;
                            case "footageEW":
                                if (this.footageEW == null)
                                    this.footageEW = new footageEastWest();
                                this.footageEW.HandleXML(xmlReader.ReadSubtree(), isNew);
                                break;
                            default:
                                break;
                        }
                    }
                }
            }
        }
    }
}
