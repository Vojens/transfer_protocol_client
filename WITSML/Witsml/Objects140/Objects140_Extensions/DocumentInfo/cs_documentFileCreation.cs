﻿ 

using System;
using System.Xml;

namespace Witsml.Objects140
{
    public partial class cs_documentFileCreation : IHandleXML
    {
        /// <summary>
        /// Convert XML string data into cs_documentFileCreation fields value
        /// </summary>
        /// <param name="xmlReader">reader that has fileCreationType node</param>
        /// <param name="IsNew">Is the object from stratch(true) or from db/serialization(false)</param>
        public void HandleXML(XmlReader xmlReader, bool isNew)
        {
            using (xmlReader)
            {
                while (xmlReader.Read())
                {
                    if (xmlReader.Depth == 1 && xmlReader.IsStartElement() && !xmlReader.IsEmptyElement)
                    {
                        switch (xmlReader.Name)
                        {
                            case "fileCreationDate":
                                this.fileCreationDate = StaticParser.ParseDateTimeFromString(StaticHelper.ReadString(xmlReader));
                                break;
                            case "softwareName":
                                this.softwareName = StaticHelper.ReadString(xmlReader);
                                break;
                            case "fileCreator":
                                this.fileCreator = StaticHelper.ReadString(xmlReader);
                                break;
                            case "comment":
                                this.comment = StaticHelper.ReadString(xmlReader);
                                break;
                            default:
                                break;
                        }
                    }
                }
            }
        }
    }
}
