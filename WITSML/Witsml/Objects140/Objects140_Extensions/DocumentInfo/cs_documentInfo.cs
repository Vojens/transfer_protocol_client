﻿ 

using System;
using System.Collections.Generic;
using System.Xml;

namespace Witsml.Objects140
{
    //TODO : Is the collection handling correct
    public partial class cs_documentInfo : IHandleXML
    {
        /// <summary>
        /// Convert XML string data into DocumentInfo fields value
        /// </summary>
        /// <param name="xmlReader">reader that has DocumentInfo node</param>
        /// <param name="IsNew">Is the object from stratch(true) or from db/serialization(false)</param>
        public void HandleXML(XmlReader xmlReader, bool isNew)
        {
            bool isAuditTrailNew = this.auditTrail == null;
            //create temporary list to handle collection data manipulation
            List<nameStruct> listDocumentAlias = this.documentAlias == null ? new List<nameStruct>() : new List<nameStruct>(this.documentAlias);
            List<nameStruct> listDocumentClass = this.documentClass == null ? new List<nameStruct>() : new List<nameStruct>(this.documentClass);
            List<cs_documentSecurityInfo> listSecurityInfoType = this.securityInformation == null ? new List<cs_documentSecurityInfo>() : new List<cs_documentSecurityInfo>(this.securityInformation);
            List<cs_documentEvent> listAuditTrail = isAuditTrailNew ? new List<cs_documentEvent>() : new List<cs_documentEvent>(this.auditTrail);

            //Read all nodes and assign the value to related fields
            using (xmlReader)
            {
                while (xmlReader.Read())
                {
                    if (xmlReader.Depth == 1 && xmlReader.IsStartElement() && !xmlReader.IsEmptyElement)
                    {
                        switch (xmlReader.Name)
                        {
                            case "documentName":
                                //object
                                if (this.documentName == null)
                                    this.documentName = new nameStruct();
                                this.documentName.HandleXML(xmlReader.ReadSubtree(), isNew);
                                break;
                            case "documentAlias":
                                //collection
                                nameStruct documentAliasObj = new nameStruct();
                                documentAliasObj.HandleXML(xmlReader.ReadSubtree(), isNew);
                                listDocumentAlias.Add(documentAliasObj);
                                break;
                            case "documentDate":
                                this.documentDate = StaticParser.ParseDateTimeFromString(StaticHelper.ReadString(xmlReader));
                                this.documentDateSpecified = true;
                                break;
                            case "documentClass":
                                //collection
                                nameStruct documentClassObj = new nameStruct();
                                documentClassObj.HandleXML(xmlReader.ReadSubtree(), isNew);
                                listDocumentClass.Add(documentClassObj);
                                break;
                            case "fileCreationInformation":
                                //object
                                if (this.fileCreationInformation == null)
                                    this.fileCreationInformation = new cs_documentFileCreation();
                                this.fileCreationInformation.HandleXML(xmlReader.ReadSubtree(), isNew);
                                break;
                            case "securityInformation":
                                //collection
                                cs_documentSecurityInfo securityInfoObj = new cs_documentSecurityInfo();
                                securityInfoObj.HandleXML(xmlReader.ReadSubtree(), isNew);
                                listSecurityInfoType.Add(securityInfoObj);
                                break;
                            case "disclaimer":
                                this.disclaimer = StaticHelper.ReadString(xmlReader);
                                break;
                            case "auditTrail":
                                //collection
                                this.HandleEvent(xmlReader.ReadSubtree(), isAuditTrailNew, ref listAuditTrail);
                                break;
                            case "owner":
                                this.owner = StaticHelper.ReadString(xmlReader);
                                break;
                            case "comment":
                                this.comment = StaticHelper.ReadString(xmlReader);
                                break;
                            default:
                                break;
                        }

                    }
                }
                //Assing list to array member collection
                this.documentAlias = listDocumentAlias.ToArray();
                this.documentClass = listDocumentClass.ToArray();
                this.securityInformation = listSecurityInfoType.ToArray();
                this.auditTrail = listAuditTrail.ToArray();
            }
        }


        private void HandleEvent(XmlReader xmlReader, bool isNew, ref List<cs_documentEvent> listEvent)
        {
            cs_documentEvent documentEventObj = null;
            using (xmlReader)
            {
                while (xmlReader.Read())
                {
                    if (xmlReader.Depth == 1 && xmlReader.IsStartElement() && !xmlReader.IsEmptyElement)
                    {
                        if (xmlReader.Name == "event")
                        {
                            documentEventObj = new cs_documentEvent();
                            documentEventObj.HandleXML(xmlReader, isNew);
                            listEvent.Add(documentEventObj);
                        }
                    }
                }
            }
        }
    }
}
