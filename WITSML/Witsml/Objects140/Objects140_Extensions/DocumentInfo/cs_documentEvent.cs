﻿ 

using System;
using System.Xml;

namespace Witsml.Objects140
{
    public partial class cs_documentEvent : IHandleXML
    {
        /// <summary>
        /// Convert XML string data into cs_documentEvent fields value
        /// </summary>
        /// <param name="xmlReader">reader that has eventType node</param>
        /// <param name="IsNew">Is the object from stratch(true) or from db/serialization(false)</param>
        public void HandleXML(XmlReader xmlReader, bool isNew)
        {
            using (xmlReader)
            {
                while (xmlReader.Read())
                {
                    if (xmlReader.Depth == 1 && xmlReader.IsStartElement() && !xmlReader.IsEmptyElement)
                    {
                        switch (xmlReader.Name)
                        {
                            case "eventDate":
                                this.eventDate = StaticParser.ParseDateTimeFromString(StaticHelper.ReadString(xmlReader));
                                break;
                            case "eventType":
                                this.eventType = StaticHelper.ReadString(xmlReader);
                                break;
                            case "responsibleParty":
                                this.responsibleParty = StaticHelper.ReadString(xmlReader);
                                break;
                            case "comment":
                                this.comment = StaticHelper.ReadString(xmlReader);
                                break;
                            default:
                                break;
                        }
                    }
                }
            }
        }
    }
}
