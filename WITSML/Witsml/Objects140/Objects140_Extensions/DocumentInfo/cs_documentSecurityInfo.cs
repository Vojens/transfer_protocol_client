﻿ 

using System;
using System.Collections.Generic;
using System.Xml;

namespace Witsml.Objects140
{
    public partial class cs_documentSecurityInfo : IHandleXML
    {
        /// <summary>
        /// Convert XML string data into cs_documentSecurityInfo fields value
        /// </summary>
        /// <param name="xmlReader">reader that has securityInfoType node</param>
        /// <param name="IsNew">Is the object from stratch(true) or from db/serialization(false)</param>
        public void HandleXML(XmlReader xmlReader, bool isNew)
        {
            using (xmlReader)
            {
                while (xmlReader.Read())
                {
                    if (xmlReader.Depth == 1 && xmlReader.IsStartElement() && !xmlReader.IsEmptyElement)
                    {
                        switch (xmlReader.Name)
                        {
                            case "class":
                                this.@class = StaticHelper.ReadString(xmlReader);
                                break;
                            case "securitySystem":
                                this.securitySystem = StaticHelper.ReadString(xmlReader);
                                break;
                            case "endDate":
                                this.endDate = StaticParser.ParseDateTimeFromString(StaticHelper.ReadString(xmlReader));
                                this.endDateSpecified = true;
                                break;
                            case "comment":
                                this.comment = StaticHelper.ReadString(xmlReader);
                                break;
                            default:
                                break;
                        }
                    }
                }
            }
        }
    }
}
