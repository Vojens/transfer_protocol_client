﻿ 

using System;
using System.Collections.Generic;
using System.Text;
using System.Xml;

namespace Witsml.Objects140
{
    public partial class cs_mudLogParameter : IHandleXML, IHasID
    {
        //this will need to be ignored
        [System.Xml.Serialization.XmlIgnore]
        public string UID
        {
            get { return this.uid; }
        }
        public cs_mudLogParameter()
        {
            this.type = MudLogParameterType.unknown;
        }

        /// <summary>
        /// Convert XML string data into mudLogParameter fields value
        /// </summary>
        /// <param name="xmlReader">reader that has mudLogParameter node</param>
        /// <param name="IsNew">Is the object from stratch(true) or from db/serialization(false)</param>
        public void HandleXML(XmlReader xmlReader, bool isNew)
        {
            //to be used to get only one of the choice
            bool hasChoice = false;
            using (xmlReader)
            {
                xmlReader.MoveToContent();
                if (xmlReader.HasAttributes)
                {
                    if (xmlReader.MoveToAttribute("uid"))
                        StaticHelper.HandleUID(xmlReader.Value, "mudLogParameter uid", out this.uid);
                    xmlReader.MoveToElement();
                }
                while (xmlReader.Read())
                {
                    if (xmlReader.Depth == 1 && xmlReader.IsStartElement() && !xmlReader.IsEmptyElement)
                    {
                        switch (xmlReader.Name)
                        {
                            case "type":
                                this.type = StaticParser.ParseEnumFromString<MudLogParameterType>(StaticHelper.ReadString(xmlReader), MudLogParameterType.unknown);
                                break;
                            case "dTime":
                                this.dTime = StaticParser.ParseDateTimeFromString(StaticHelper.ReadString(xmlReader));
                                this.dTimeSpecified = true;
                                break;
                            case "mdTop":
                                if (this.mdTop == null)
                                    this.mdTop = new measuredDepthCoord();
                                this.mdTop.HandleXML(xmlReader.ReadSubtree(), isNew);
                                break;
                            case "mdBottom":
                                if (this.mdBottom == null)
                                    this.mdBottom = new measuredDepthCoord();
                                this.mdBottom.HandleXML(xmlReader.ReadSubtree(), isNew);
                                break;
                            case "concentration":
                                if (!hasChoice)
                                {
                                    volumePerVolumeMeasure measureItem = new volumePerVolumeMeasure();
                                    measureItem.HandleXML(xmlReader.ReadSubtree(), isNew);
                                    this.Item = measureItem;
                                    hasChoice = true;
                                }
                                break;
                            case "equivalentMudWeight":
                                if (!hasChoice)
                                {
                                    densityMeasure measureItem = new densityMeasure();
                                    measureItem.HandleXML(xmlReader.ReadSubtree(), isNew);
                                    this.Item = measureItem;
                                    hasChoice = true;
                                }
                                break;
                            case "force":
                                if (!hasChoice)
                                {
                                    forceMeasure measureItem = new forceMeasure();
                                    measureItem.HandleXML(xmlReader.ReadSubtree(), isNew);
                                    this.Item = measureItem;
                                    hasChoice = true;
                                }
                                break;
                            case "pressureGradient":
                                if (!hasChoice)
                                {
                                    forcePerVolumeMeasure measureItem = new forcePerVolumeMeasure();
                                    measureItem.HandleXML(xmlReader.ReadSubtree(), isNew);
                                    this.Item = measureItem;
                                    hasChoice = true;
                                }
                                break;
                            case "text":
                                this.text = StaticHelper.ReadString(xmlReader);
                                break;
                            default:
                                break;
                        }
                    }
                }
            }
        }
    }
}
