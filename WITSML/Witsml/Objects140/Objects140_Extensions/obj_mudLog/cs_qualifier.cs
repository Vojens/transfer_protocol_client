﻿ 

using System;
using System.Collections.Generic;
using System.Text;
using System.Xml;

namespace Witsml.Objects140
{
    public partial class cs_qualifier : IHandleXML, IHasID
    {
        //this will need to be ignored
        [System.Xml.Serialization.XmlIgnore]
        public string UID
        {
            get { return this.uid; }
        }

        /// <summary>
        /// Convert XML string data into qualifier fields value
        /// </summary>
        /// <param name="xmlReader">reader that has qualifier node</param>
        /// <param name="IsNew">Is the object from stratch(true) or from db/serialization(false)</param>
        public void HandleXML(XmlReader xmlReader, bool isNew)
        {
            using (xmlReader)
            {
                xmlReader.MoveToContent();
                if (xmlReader.HasAttributes)
                {
                    if (xmlReader.MoveToAttribute("uid"))
                        StaticHelper.HandleUID(xmlReader.Value, "qualifier uid", out this.uid);
                    xmlReader.MoveToElement();
                }
                while (xmlReader.Read())
                {
                    if (xmlReader.Depth == 1 && xmlReader.IsStartElement() && !xmlReader.IsEmptyElement)
                    {
                        switch (xmlReader.Name)
                        {
                            case "type":
                                this.type = StaticHelper.ReadString(xmlReader);
                                break;
                            case "mdTop":
                                if (this.mdTop == null)
                                    this.mdTop = new measuredDepthCoord();
                                this.mdTop.HandleXML(xmlReader.ReadSubtree(), isNew);
                                break;
                            case "mdBottom":
                                if (this.mdBottom == null)
                                    this.mdBottom = new measuredDepthCoord();
                                this.mdBottom.HandleXML(xmlReader.ReadSubtree(), isNew);
                                break;
                            case "abundance":
                                if (this.abundance == null)
                                    this.abundance = new volumePerVolumeMeasurePercent();
                                this.abundance.HandleXML(xmlReader.ReadSubtree(), isNew);
                                break;
                            case "abundanceCode":
                                this.abundanceCode = StaticHelper.ReadString(xmlReader);
                                break;
                            case "description":
                                this.description = StaticHelper.ReadString(xmlReader);
                                break;
                            default:
                                break;
                        }
                    }
                }
            }
        }
    }
}
