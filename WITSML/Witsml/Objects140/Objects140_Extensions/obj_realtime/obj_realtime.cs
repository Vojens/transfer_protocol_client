 

using System;
using System.Collections.Generic;
using System.IO;
using System.Text;
using System.Xml;
using Witsml.Common.Exceptions;
using Witsml.Exceptions;

namespace Witsml.Objects140
{
    //no datagroup,dTimHeaderLastChanged added
    [System.Xml.Serialization.XmlRootAttribute("realtime", Namespace = "http://www.witsml.org/schemas/140", IsNullable = false)]
    public partial class obj_realtime : WITSMLObject, IHandleXML
    {
        public obj_realtime()
        {
            this.version = WITSMLVersion.V140;
        }
        /// <summary>
        /// Decode XML string into Realtime
        /// </summary>
        /// <param name="xmlIn">XML string</param>
        public override void Decode(ref string xmlIn)
        {
            try
            {
                //Consume XML string and convert node value to variable based on node name
                using (StringReader stringReader = new StringReader(xmlIn))
                {
                    XmlReaderSettings setting = new XmlReaderSettings();
                    setting.ConformanceLevel = ConformanceLevel.Document;
                    setting.IgnoreComments = true;
                    setting.IgnoreWhitespace = true;
                    setting.IgnoreProcessingInstructions = true;
                    using (XmlReader xmlReader = XmlReader.Create(stringReader, setting))
                    {
                        //only add operation allowed
                        bool isNew = true;

                        this.HandleXML(xmlReader, isNew);
                    }
                }
            }
            catch (XmlException ex)
            {
                throw new UnableToDecodeException(ex.Message
                    , WITSMLReturnCode.WITSMLDocumentError, ex.InnerException);
            }
            catch (NullReferenceException ex)
            {
                throw new Exceptions.UnableToDecodeException(ex.Message
                    , WITSMLReturnCode.WITSMLEmptyDocument, ex.InnerException);
            }
            catch (UnableToDecodeException ex)
            {
                throw ex;
            }
            catch (Exception ex)
            {
                throw new UnableToDecodeException(ex.Message, WITSMLReturnCode.WITSMLUnkownCause
                    , ex.InnerException);
            }
        }

        /// <summary>
        /// Convert XML string data into Realtime fields value
        /// </summary>
        /// <param name="xmlReader">reader that has Rig node</param>
        /// <param name="IsNew">Is the object from stratch(true) or from db/serialization(false)</param>
        public void HandleXML(XmlReader xmlReader, bool isNew)
        {
            List<cs_channel> listChannel = this.channel == null ? new List<cs_channel>() : new List<cs_channel>(this.channel);
            List<cs_record> listRecord = this.record == null ? new List<cs_record>() : new List<cs_record>(this.record);

            using (xmlReader)
            {
                //assign uid for the new well
                xmlReader.MoveToContent();
                if (xmlReader.HasAttributes)
                {
                    if (xmlReader.MoveToAttribute("uidWell"))
                        StaticHelper.HandleUID(xmlReader.Value, "realtime uidWell", out this.uidWell);
                    if (xmlReader.MoveToAttribute("uidWellbore"))
                        StaticHelper.HandleUID(xmlReader.Value, "realtime uidWellbore", out this.uidWellbore);
                    if (xmlReader.MoveToAttribute("idSub"))
                        StaticHelper.HandleUID(xmlReader.Value, "realtime idSub", out this.idSub);
                    xmlReader.MoveToElement();
                }
                while (xmlReader.Read())
                {
                    //only take child node
                    if (xmlReader.Depth == 1 && xmlReader.IsStartElement() && !xmlReader.IsEmptyElement)
                    {
                        switch (xmlReader.Name)
                        {
                            case "dTim":
                                this.dTim = StaticParser.ParseDateTimeFromString(StaticHelper.ReadString(xmlReader));
                                this.dTimSpecified = true;
                                break;
                            case "md":
                                if (this.md == null)
                                    this.md = new measuredDepthCoord();
                                this.md.HandleXML(xmlReader.ReadSubtree(), isNew);
                                break;
                            case "sequence":
                                this.sequence = StaticParser.ParseIntFromString(StaticHelper.ReadString(xmlReader));
                                break;
                            case "dTimHeaderLastChanged":
                                this.dTimHeaderLastChanged = StaticParser.ParseDateTimeFromString(StaticHelper.ReadString(xmlReader));
                                this.dTimHeaderLastChangedSpecified = true;
                                break;
                            case "activityCode":
                                this.activityCode = StaticHelper.ReadString(xmlReader);
                                break;
                            case "detailActivity":
                                this.detailActivity = StaticHelper.ReadString(xmlReader);
                                break;
                            case "realtimeHeader":
                                if (this.realtimeHeader == null)
                                    this.realtimeHeader = new cs_realtimeHeader();
                                this.realtimeHeader.HandleXML(xmlReader.ReadSubtree(), isNew);
                                break;
                            case "channel":
                                //collection
                                cs_channel channel = new cs_channel();
                                channel.HandleXML(xmlReader.ReadSubtree(), isNew);
                                listChannel.Add(channel);
                                break;
                            case "record":
                                //collection
                                cs_record record = new cs_record();
                                record.HandleXML(xmlReader.ReadSubtree(), isNew);
                                listRecord.Add(record);
                                break;
                            case "commonData":
                                if (this.commonData == null)
                                    this.commonData = new cs_commonData();
                                this.commonData.HandleXML(xmlReader.ReadSubtree(), isNew);
                                break;
                            case "customData":
                                if (this.customData == null)
                                    this.customData = new cs_customData();
                                this.customData.HandleXML(xmlReader.ReadSubtree(), isNew);
                                break;
                            default:
                                break;
                        }
                    }
                }
                //assign list to collection
                this.channel = listChannel.ToArray();
                this.record = listRecord.ToArray();
            }
        }

        public override string ValidateGetPath(string newUid)
        {
            // Fix uid if the Object dosn't have one.
            if (!IsValidUid(this.idSub))
            {
                this.idSub = newUid;
                // TODO: Add some loging that we created a new UID for the object maybe.
            }

            StringBuilder path = new StringBuilder(this.GetParentPath());
            path.Append(@"\");
            path.Append(this.idSub);
            return path.ToString();
        }

        public override string GetParentPath()
        {
            if (!IsValidUid(this.uidWell) || !IsValidUid(this.uidWellbore))
            {
                throw new UnableToDecodeException("One or more Parent UID's are invalid", WITSMLReturnCode.WITSMLInvalidParentUID);
                //TODO: Log the issue
            }

            StringBuilder path = new StringBuilder(@"\");
            path.Append(this.uidWell);
            path.Append(@"\");
            path.Append(this.uidWellbore);
            return path.ToString();
        }

        // NOTE: these are done with functions, instead of property access, so as not to mess around with the auto-generated / serialized classes.
        public override string Get_uid()
        {
            return this.idSub;
        }

        public override string Get_uidWell()
        {
            return this.uidWell;
        }

        public override string Get_uidWellbore()
        {
            return this.uidWellbore;
        }

        public override string Get_name()
        {
            return "";
        }

        public override string Get_nameWell()
        {
            return "";
        }

        public override string Get_nameWellbore()
        {
            return "";
        }

        public override ICommonData Get_commonData()
        {
            if (this.commonData == null)
            {
                this.commonData = new cs_commonData();
            }
            return (ICommonData)this.commonData;
        }
        public override ICustomData Get_customData()
        {
            if (this.customData == null)
            {
                this.customData = new cs_customData();
            }
            return (ICustomData)this.customData;
        }
    }
}
