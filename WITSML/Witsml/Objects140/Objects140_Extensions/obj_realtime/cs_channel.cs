 

using System;
using System.Collections.Generic;
using System.Text;
using System.Xml;

namespace Witsml.Objects140
{
    public partial class cs_channel : IHandleXML
    {
         /// <summary>
        /// Convert XML string data into channel fields value
        /// </summary>
        /// <param name="xmlReader">reader that has timeMeasure node</param>
        /// <param name="IsNew">Is the object from stratch(true) or from db/serialization(false)</param>
        public void HandleXML(XmlReader xmlReader, bool isNew)
        {
            using (xmlReader)
            {
                while (xmlReader.Read())
                {
                    if (xmlReader.Depth == 1 && xmlReader.IsStartElement() && !xmlReader.IsEmptyElement)
                    {
                        switch (xmlReader.Name)
                        {
                            case "id":
                                this.id = StaticHelper.ReadString(xmlReader);
                                break;
                            case "mnemonic":
                                this.mnemonic = StaticHelper.ReadString(xmlReader);
                                break;
                            case "dTim":
                                this.dTim = StaticParser.ParseDateTimeFromString(StaticHelper.ReadString(xmlReader));
                                this.dTimSpecified = true;
                                break;
                            case "md":
                                if (this.md == null)
                                    this.md = new measuredDepthCoord();
                                this.md.HandleXML(xmlReader.ReadSubtree(), isNew);
                                break;
                            case "value":
                                if (this.value == null)
                                    this.value = new encodedArrayString();
                                this.value.HandleXML(xmlReader.ReadSubtree(), isNew);
                                break;
                            case "densData":
                                if (this.densData == null)
                                    this.densData = new perLengthMeasure();
                                this.densData.HandleXML(xmlReader.ReadSubtree(), isNew);
                                break;
                            case "qualData":
                                this.qualData = StaticHelper.ReadString(xmlReader);
                                break;
                            case "fet":
                                if (this.fet == null)
                                    this.fet = new timeMeasure();
                                this.fet.HandleXML(xmlReader.ReadSubtree(), isNew);
                                break;
                            default:
                                break;
                        }
                    }
                }
            }
        }
    }
}
