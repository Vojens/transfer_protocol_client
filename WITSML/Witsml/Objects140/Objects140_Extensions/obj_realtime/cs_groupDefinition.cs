 

using System;
using System.Collections.Generic;
using System.Text;
using System.Xml;

namespace Witsml.Objects140
{
    //dataGroup added
    public partial class cs_groupDefinition : IHandleXML
    {
        /// <summary>
        /// Convert XML string data into group definition fields value
        /// </summary>
        /// <param name="xmlReader">reader that has timeMeasure node</param>
        /// <param name="IsNew">Is the object from stratch(true) or from db/serialization(false)</param>
        public void HandleXML(XmlReader xmlReader, bool isNew)
        {
            //allow only one choice between persistedAsLog or persistedAsWellLog
            bool hasChoice = false;
            //temporary list to handle data manipulation
            List<cs_channelDefinition> listChannelDef = this.channelDefinition == null ? new List<cs_channelDefinition>() : new List<cs_channelDefinition>(this.channelDefinition);
            using (xmlReader)
            {
                while (xmlReader.Read())
                {
                    if (xmlReader.Depth == 1 && xmlReader.IsStartElement() && !xmlReader.IsEmptyElement)
                    {
                        switch (xmlReader.Name)
                        {
                            case "id":
                                this.id = StaticHelper.ReadString(xmlReader);
                                break;
                            case "multiplexed":
                                this.multiplexed = StaticParser.ParseBooleanFromString(StaticHelper.ReadString(xmlReader));
                                break;
                            case "dataGroup":
                                this.dataGroup = StaticHelper.ReadString(xmlReader);
                                break;
                            case "interval":
                                if (this.interval == null)
                                    this.interval = new cs_interval();
                                this.interval.HandleXML(xmlReader.ReadSubtree(), isNew);
                                break;
                            case "persistedAsLog":
                                if (!hasChoice)
                                {
                                    this.Item = new refNameString();
                                    this.Item.ObjectName = "persistedAsLog";
                                    this.Item.HandleXML(xmlReader.ReadSubtree(), isNew);
                                    this.ItemElementName = ItemChoiceType1.persistedAsLog;
                                    hasChoice = true;
                                }
                                break;
                            case "persistedAsWellLog":
                                if (!hasChoice)
                                {
                                    this.Item = new refNameString();
                                    this.Item.ObjectName = "persistedAsWellLog";
                                    this.Item.HandleXML(xmlReader.ReadSubtree(), isNew);
                                    this.ItemElementName = ItemChoiceType1.persistedAsWellLog;
                                    hasChoice = true;
                                }
                                break;
                            case "comment":
                                this.comment = StaticHelper.ReadString(xmlReader);
                                break;
                            case "channelDefinition":
                                //collection
                                cs_channelDefinition channelDef = new cs_channelDefinition();
                                channelDef.HandleXML(xmlReader.ReadSubtree(), isNew);
                                listChannelDef.Add(channelDef);
                                break;
                            default:
                                break;
                        }
                    }
                }
                //assign list to collection
                this.channelDefinition = listChannelDef.ToArray();
            }
        }
    }
}
