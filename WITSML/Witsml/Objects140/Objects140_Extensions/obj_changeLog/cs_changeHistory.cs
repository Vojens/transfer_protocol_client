﻿ 

using System;
using System.Collections.Generic;
using System.Text;
using System.Xml;

namespace Witsml.Objects140
{
    public partial class cs_changeHistory : IHandleXML, IHasID
    {
        //this will need to be ignored
        [System.Xml.Serialization.XmlIgnore]
        public string UID
        {
            get { return this.uid; }
        }

        public cs_changeHistory()
        {
            this.changeType = ChangeInfoType.unknown;
        }
        public void HandleXML(XmlReader xmlReader, bool isNew)
        {
            using (xmlReader)
            {
                xmlReader.MoveToContent();
                if (xmlReader.HasAttributes)
                {
                    if (xmlReader.MoveToAttribute("uid"))
                        StaticHelper.HandleUID(xmlReader.Value, "changeHistory uid", out this.uid);
                    xmlReader.MoveToElement();
                }
                while (xmlReader.Read())
                {
                    //only take child node
                    if (xmlReader.Depth == 1 && xmlReader.IsStartElement() && !xmlReader.IsEmptyElement)
                    {
                        switch (xmlReader.Name)
                        {
                            case "dTimChange":
                                this.dTimChange = StaticParser.ParseDateTimeFromString(StaticHelper.ReadString(xmlReader));
                                break;
                            case "changeType":
                                this.changeType = StaticParser.ParseEnumFromString<ChangeInfoType>(StaticHelper.ReadString(xmlReader), ChangeInfoType.unknown);
                                break;
                            case "changeInfo":
                                this.changeInfo = StaticHelper.ReadString(xmlReader);
                                break;
                            case "startIndex":
                                this.startIndex = StaticHelper.ReadString(xmlReader);
                                break;
                            case "endIndex":
                                this.endIndex = StaticHelper.ReadString(xmlReader);
                                break;
                            case "startDateTimeIndex":
                                this.startDateTimeIndex = StaticParser.ParseDateTimeFromString(StaticHelper.ReadString(xmlReader));
                                this.startDateTimeIndexSpecified = true;
                                break;
                            case "endDateTimeIndex":
                                this.endDateTimeIndex = StaticParser.ParseDateTimeFromString(StaticHelper.ReadString(xmlReader));
                                this.endDateTimeIndexSpecified = true;
                                break;
                            case "mnemonics":
                                this.mnemonics = StaticHelper.ReadString(xmlReader);
                                break;
                            default:
                                break;
                        }
                    }
                }
            }
        }

    }
}
