﻿ 

using System;
using System.Collections.Generic;
using System.IO;
using System.Text;
using System.Xml;
using System.Xml.Serialization;
using System.Linq;
using Witsml.Common.Exceptions;


namespace Witsml.Objects140
{
    //Extension class for wells document that inherit from WITSMLDocument and Implement Decode Function
    public partial class obj_changeLogs : WITSMLDocument
    {

        public override WITSMLObject GetSingleObject(int Item)
        {
            return this.changeLog[Item];
        }

        /// <summary>
        /// Deserialize XML string into WitsmlDocument object
        /// </summary>
        /// <param name="XMLin">XML string to be serialized</param>
        /// <returns>Number of singular changeLog inside</returns>
        public override int Decode(ref string xmlIn)
        {
            //return value
            //number of changeLog inside xmlIn
            int countOfChangeLog = 0;
            try
            {
                //check whether collection is from stratch(isNew = true) or from deserialization(isNew=false)
                bool isNew = this.changeLog == null;

                //create temporary list to help with data manipulation
                List<obj_changeLog> listChangeLog = isNew ? new List<obj_changeLog>() : new List<obj_changeLog>(this.changeLog);

                //Consume XML string and convert node value to variable based on node name
                using (StringReader stringReader = new StringReader(xmlIn))
                {
                    XmlReaderSettings setting = new XmlReaderSettings();
                    setting.ConformanceLevel = ConformanceLevel.Document;
                    setting.IgnoreComments = true;
                    setting.IgnoreWhitespace = true;
                    setting.IgnoreProcessingInstructions = true;
                    using (XmlReader xmlReader = XmlReader.Create(stringReader, setting))
                    {
                        while (xmlReader.Read())
                        {
                            //assign version
                            xmlReader.MoveToContent();
                            if (xmlReader.MoveToAttribute("version"))
                                this.version = xmlReader.Value;
                            else
                                this.version = "1.4.0";//no version specified,set it to 1.4.0

                            //pre-checking on whether node is the correct child based on depth,start tag and not empty
                            if (xmlReader.Depth == 1 && xmlReader.IsStartElement() && !xmlReader.IsEmptyElement)
                            {
                                switch (xmlReader.Name)
                                {
                                    case "documentInfo":
                                        if (this.documentInfo == null)
                                            this.documentInfo = new cs_documentInfo();
                                        this.documentInfo.HandleXML(xmlReader.ReadSubtree(), isNew);
                                        break;
                                    case "changeLog":
                                        obj_changeLog changeLog;
                                        //foreign key(well id,wellbore id) and primary key(wellbore id) to identify existing changeLog
                                        string currentUIDWell = string.Empty;
                                        string currentUIDWellbore = string.Empty;
                                        string currentUID = string.Empty;

                                        //Update changeLog
                                        if (!isNew)
                                        {
                                            //check for uid attribute and assign it to temporary string for searching in collection
                                            if (xmlReader.HasAttributes)
                                            {
                                                if (xmlReader.MoveToAttribute("uidWell"))
                                                {
                                                    currentUIDWell = xmlReader.Value;
                                                }
                                                if (xmlReader.MoveToAttribute("uidWellbore"))
                                                {
                                                    currentUIDWellbore = xmlReader.Value;
                                                }
                                                if (xmlReader.MoveToAttribute("uid"))
                                                {
                                                    currentUID = xmlReader.Value;
                                                }
                                                xmlReader.MoveToElement();
                                            }
                                            //look for changeLog based on its UID and update the changeLog with xml string
                                            if (currentUID != string.Empty)
                                            {
                                                changeLog = listChangeLog.FirstOrDefault(
                                                    delegate(obj_changeLog objchangeLog)
                                                    { if (objchangeLog.uidWell == currentUIDWell && objchangeLog.uidWellbore == currentUIDWellbore && objchangeLog.uid == currentUID)return true; else return false; });
                                                if (changeLog != null)
                                                {
                                                    changeLog.HandleXML(xmlReader.ReadSubtree(), isNew);
                                                    countOfChangeLog++;
                                                    continue;
                                                }
                                                else
                                                {
                                                    //Update can't process changeLog without match
                                                    continue;
                                                }
                                            }
                                            else
                                            {
                                                //Update can't process changeLog without uid
                                                continue;
                                            }
                                        }

                                        //Add New Well 
                                        changeLog = new obj_changeLog();
                                        changeLog.HandleXML(xmlReader.ReadSubtree(), true);
                                        listChangeLog.Add(changeLog);
                                        countOfChangeLog++;
                                        break;
                                    default:
                                        break;
                                }
                            }
                        }
                        //assign list to wellbore collection
                        this.changeLog = listChangeLog.ToArray();
                    }
                }
            }
            catch (XmlException ex)
            {
                throw new Exceptions.UnableToDecodeException(ex.Message, WITSMLReturnCode.WITSMLDocumentError, ex.InnerException);
            }
            catch (NullReferenceException ex)
            {
                throw new Exceptions.UnableToDecodeException(ex.Message, WITSMLReturnCode.WITSMLEmptyDocument, ex.InnerException);
            }
            catch (Exceptions.UnableToDecodeException ex)
            {
                throw ex;
            }
            catch (Exception ex)
            {
                throw new Exceptions.UnableToDecodeException(ex.Message, WITSMLReturnCode.WITSMLUnkownCause, ex.InnerException);
            }
            return countOfChangeLog;

        }

    }
}
