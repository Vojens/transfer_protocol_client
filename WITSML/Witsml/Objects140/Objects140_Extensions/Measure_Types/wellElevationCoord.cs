 

using System;
using System.Xml;

namespace Witsml.Objects140
{
    public partial class wellElevationCoord : IHandleXML
    {
        public wellElevationCoord()
        {
            this.uom = WellVerticalCoordinateUom.unknown;
        }
        /// <summary>
        /// Convert XML string data into wellElevationCoord fields value
        /// </summary>
        /// <param name="xmlReader">reader that has wellElevationCoord node</param>
        /// <param name="IsNew">Is the object from stratch(true) or from db/serialization(false)</param>
        public void HandleXML(XmlReader xmlReader, bool isNew)
        {
            using (xmlReader)
            {
                xmlReader.Read();
                if (xmlReader.HasAttributes)
                {
                    if (xmlReader.MoveToAttribute("uom"))
                    {
                        string value = xmlReader.Value;
                        this.uom = StaticParser.ParseEnumUomFromString<WellVerticalCoordinateUom>(value);
                    }
                    if (xmlReader.MoveToAttribute("datum"))
                        this.datum = xmlReader.Value;
                    xmlReader.MoveToElement();
                }
                this.Value = StaticParser.ParseDoubleFromString(StaticHelper.ReadString(xmlReader));
            }
        }

    }
}
