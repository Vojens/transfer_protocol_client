﻿ 

using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Xml;

namespace Witsml.Objects140
{
    public partial class footageNorthSouth
    {
        public footageNorthSouth()
        {
            this.uom = LengthUom.unknown;
            this.@ref = NorthOrSouth.unknown;
        }
        /// <summary>
        /// Convert XML string data into footageNorthSouth fields value
        /// </summary>
        /// <param name="xmlReader">reader that has equivalentPerMassMeasure node</param>
        /// <param name="IsNew">Is the object from stratch(true) or from db/serialization(false)</param>
        public void HandleXML(XmlReader xmlReader, bool isNew)
        {
            using (xmlReader)
            {
                xmlReader.Read();
                if (xmlReader.HasAttributes)
                {
                    if (xmlReader.MoveToAttribute("uom"))
                    {
                        string value = xmlReader.Value;
                        this.uom = StaticParser.ParseEnumUomFromString<LengthUom>(value);
                    }
                    if (xmlReader.MoveToAttribute("ref"))
                    {
                        string value2 = xmlReader.Value;
                        this.@ref = StaticParser.ParseEnumUomFromString<NorthOrSouth>(value2);
                    }
                    xmlReader.MoveToElement();
                }
                this.Value = StaticParser.ParseDoubleFromString(StaticHelper.ReadString(xmlReader));
            }
        }
    }
}
