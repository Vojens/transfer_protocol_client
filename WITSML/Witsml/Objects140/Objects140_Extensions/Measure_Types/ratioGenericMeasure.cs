 

using System;
using System.Collections.Generic;
using System.Text;
using System.Xml;

namespace Witsml.Objects140
{
    public partial class ratioGenericMeasure : IHandleXML
    {
        /// <summary>
        /// Convert XML string data into ratioGenericMeasure fields value
        /// </summary>
        /// <param name="xmlReader">reader that has ratioGenericMeasure node</param>
        /// <param name="IsNew">Is the object from stratch(true) or from db/serialization(false)</param>
        public void HandleXML(XmlReader xmlReader, bool isNew)
        {
            using (xmlReader)
            {
                xmlReader.Read();
                if (xmlReader.HasAttributes)
                {
                    if (xmlReader.MoveToAttribute("uom"))
                        this.uom = xmlReader.Value;
                    if (xmlReader.MoveToAttribute("numerator"))
                    {
                        this.numerator = StaticParser.ParseDoubleFromString(xmlReader.Value);
                        this.numeratorSpecified = true;
                    }
                    else
                    {
                        this.numeratorSpecified = false;
                    }
                    if (xmlReader.MoveToAttribute("denominator"))
                    {
                        this.denominator = StaticParser.ParseDoubleFromString(xmlReader.Value);
                        this.denominatorSpecified = true;
                    }
                    else
                    {
                        this.denominatorSpecified = false;  
                    }
                    xmlReader.MoveToElement();
                }
                this.Value = StaticParser.ParseDoubleFromString(StaticHelper.ReadString(xmlReader));
            }
        }
    }
}
