 

using System;
using System.Xml;

namespace Witsml.Objects140
{
    public partial class massPerLengthMeasure : IHandleXML
    {
        public massPerLengthMeasure()
        {
            this.uom = MassPerLengthUom.unknown;
        }
        /// <summary>
        /// Convert XML string data into massPerLengthMeasure fields value
        /// </summary>
        /// <param name="xmlReader">reader that has massPerLengthMeasure node</param>
        /// <param name="IsNew">Is the object from stratch(true) or from db/serialization(false)</param>
        public void HandleXML(XmlReader xmlReader, bool isNew)
        {
            using (xmlReader)
            {
                xmlReader.Read();
                if (xmlReader.HasAttributes)
                {
                    if (xmlReader.MoveToAttribute("uom"))
                    {
                        //special conversion of enum string
                        string value = xmlReader.Value;
                        this.uom = StaticParser.ParseEnumUomFromString<MassPerLengthUom>(value);
                    }

                    xmlReader.MoveToElement();
                }
                this.Value = StaticParser.ParseDoubleFromString(StaticHelper.ReadString(xmlReader));
            }
        }
    }
}
