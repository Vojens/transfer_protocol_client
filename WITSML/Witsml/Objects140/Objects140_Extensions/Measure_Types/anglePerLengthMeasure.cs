 

using System;
using System.Collections.Generic;
using System.Text;
using System.Xml;
using Support.Common;

namespace Witsml.Objects140
{
    public partial class anglePerLengthMeasure : IHandleXML
    {
        public anglePerLengthMeasure()
        {
            this.uom = AnglePerLengthUom.unknown;
        }
        /// <summary>
        /// Convert XML string data into anglePerLengthMeasure fields value
        /// </summary>
        /// <param name="xmlReader">reader that has anglePerLengthMeasure node</param>
        /// <param name="IsNew">Is the object from stratch(true) or from db/serialization(false)</param>
        public void HandleXML(XmlReader xmlReader, bool isNew)
        {
            using (xmlReader)
            {
                xmlReader.Read();
                if (xmlReader.HasAttributes)
                {
                    if (xmlReader.MoveToAttribute("uom"))
                    {
                        string value = xmlReader.Value;
                        this.uom = StaticParser.ParseEnumUomFromString<AnglePerLengthUom>(value);
                    }
                    xmlReader.MoveToElement();
                }
                this.Value = StaticParser.ParseDoubleFromString(StaticHelper.ReadString(xmlReader));
            }
        }
    }
}

