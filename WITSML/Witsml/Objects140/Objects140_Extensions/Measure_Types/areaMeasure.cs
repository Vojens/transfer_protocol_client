 

using System;
using System.Collections.Generic;
using System.Text;
using System.Xml;

namespace Witsml.Objects140
{
    public partial class areaMeasure : IHandleXML
    {
        public areaMeasure()
        {
            this.uom = AreaUom.unknown;
        }
        /// <summary>
        /// Convert XML string data into areaMeasure fields value
        /// </summary>
        /// <param name="xmlReader">reader that has areaMeasure node</param>
        /// <param name="IsNew">Is the object from stratch(true) or from db/serialization(false)</param>
        public void HandleXML(XmlReader xmlReader, bool isNew)
        {
            using (xmlReader)
            {
                xmlReader.Read();
                if (xmlReader.HasAttributes)
                {
                    if (xmlReader.MoveToAttribute("uom"))
                        this.uom = StaticParser.ParseEnumUomFromString<AreaUom>(xmlReader.Value);
                    xmlReader.MoveToElement();
                }
                this.Value = StaticParser.ParseDoubleFromString(StaticHelper.ReadString(xmlReader));
            }
        }
    }
}
