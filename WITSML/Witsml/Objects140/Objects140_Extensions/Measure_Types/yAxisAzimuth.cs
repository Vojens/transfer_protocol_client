 

using System;
using System.Collections.Generic;
using System.Xml;

namespace Witsml.Objects140
{
    public partial class yAxisAzimuth : IHandleXML
    {
        public yAxisAzimuth()
        {
            this.uom = PlaneAngleUom.unknown;
            this.northDirection = AziRef.unknown;
        }
        /// <summary>
        /// Convert XML string data into yAxisAzimuth fields value
        /// </summary>
        /// <param name="xmlReader">reader that has yAxisAzimuth node</param>
        /// <param name="IsNew">Is the object from stratch(true) or from db/serialization(false)</param>
        public void HandleXML(XmlReader xmlReader, bool isNew)
        {
            using (xmlReader)
            {
                xmlReader.Read();
                if (xmlReader.HasAttributes)
                {
                    if (xmlReader.MoveToAttribute("uom"))
                        this.uom = StaticParser.ParseEnumUomFromString<PlaneAngleUom>(xmlReader.Value);
                    if (xmlReader.MoveToAttribute("northDirection"))
                    {
                        string value = xmlReader.Value;
                        this.northDirection = StaticParser.ParseEnumUomFromString<AziRef>(value);
                        this.northDirectionSpecified = true;
                    }
                    else
                    {
                        this.northDirection = AziRef.unknown;
                        this.northDirectionSpecified = false;
                    }
                    xmlReader.MoveToElement();
                }
                this.Value = StaticParser.ParseDoubleFromString(StaticHelper.ReadString(xmlReader));
            }
        }
    }
}
