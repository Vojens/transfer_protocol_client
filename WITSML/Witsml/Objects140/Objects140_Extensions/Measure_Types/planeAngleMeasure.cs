 

using System;
using System.Xml;
using Support.Common;

namespace Witsml.Objects140
{
    public partial class planeAngleMeasure : IHandleXML
    {
        public planeAngleMeasure()
        {
            this.uom = PlaneAngleUom.unknown;
        }
        /// <summary>
        /// Convert XML string data into planeAngleMeasure fields value
        /// </summary>
        /// <param name="xmlReader">reader that has planeAngleMeasure node</param>
        /// <param name="IsNew">Is the object from stratch(true) or from db/serialization(false)</param>
        public void HandleXML(XmlReader xmlReader, bool isNew)
        {
            using (xmlReader)
            {
                xmlReader.Read();
                if (xmlReader.HasAttributes)
                {
                    if (xmlReader.MoveToAttribute("uom"))
                        this.uom = StaticParser.ParseEnumUomFromString<PlaneAngleUom>(xmlReader.Value);
                    xmlReader.MoveToElement();
                }
                this.Value = StaticParser.ParseDoubleFromString(StaticHelper.ReadString(xmlReader));
            }
        }
    }
}
