 

using System;
using System.Collections.Generic;
using System.Xml;

namespace Witsml.Objects140
{
    public partial class timeMeasure : IHandleXML
    {
        public timeMeasure()
        {
            this.uom = TimeUom.unknown;
        }
        /// <summary>
        /// Convert XML string data into timeMeasure fields value
        /// </summary>
        /// <param name="xmlReader">reader that has timeMeasure node</param>
        /// <param name="IsNew">Is the object from stratch(true) or from db/serialization(false)</param>
        public void HandleXML(XmlReader xmlReader, bool isNew)
        {
            using (xmlReader)
            {
                xmlReader.Read();
                if (xmlReader.HasAttributes)
                {
                    if (xmlReader.MoveToAttribute("uom"))
                    {
                        string value = xmlReader.Value;
                        this.uom = StaticParser.ParseEnumUomFromString<TimeUom>(value);
                    }
                    xmlReader.MoveToElement();
                }
                this.Value = StaticParser.ParseDoubleFromString(StaticHelper.ReadString(xmlReader));
            }
        }
    }
}
