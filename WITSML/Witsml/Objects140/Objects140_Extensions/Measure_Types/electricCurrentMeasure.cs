 

using System;
using System.Collections.Generic;
using System.Text;
using System.Xml;

namespace Witsml.Objects140
{
    public partial class electricCurrentMeasure : IHandleXML
    {
        public electricCurrentMeasure()
        {
            this.uom = ElectricCurrentUom.unknown;
        }
        /// <summary>
        /// Convert XML string data into electricCurrentMeasure fields value
        /// </summary>
        /// <param name="xmlReader">reader that has electricCurrentMeasure node</param>
        /// <param name="IsNew">Is the object from stratch(true) or from db/serialization(false)</param>
        public void HandleXML(XmlReader xmlReader, bool isNew)
        {
            using (xmlReader)
            {
                xmlReader.Read();
                if (xmlReader.HasAttributes)
                {
                    if (xmlReader.MoveToAttribute("uom"))
                        this.uom = StaticParser.ParseEnumUomFromString<ElectricCurrentUom>(xmlReader.Value);
                    xmlReader.MoveToElement();
                }
                this.Value = StaticParser.ParseDoubleFromString(StaticHelper.ReadString(xmlReader));
            }
        }
    }
}
