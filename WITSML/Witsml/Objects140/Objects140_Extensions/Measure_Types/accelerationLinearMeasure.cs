 

using System;
using System.Collections.Generic;
using System.Text;
using System.Xml;
using Support.Common;

namespace Witsml.Objects140
{
    public partial class accelerationLinearMeasure : IHandleXML
    {
        public accelerationLinearMeasure()
        {
            this.uom = AccelerationLinearUom.unknown;
        }
        /// <summary>
        /// Convert XML string data into accelerationLinearMeasure fields value
        /// </summary>
        /// <param name="xmlReader">reader that has accelerationLinearMeasure node</param>
        /// <param name="IsNew">Is the object from stratch(true) or from db/serialization(false)</param>
        public void HandleXML(XmlReader xmlReader, bool isNew)
        {
            using (xmlReader)
            {
                xmlReader.Read();
                if (xmlReader.HasAttributes)
                {
                    if (xmlReader.MoveToAttribute("uom"))
                    {
                        string value = xmlReader.Value;
                        this.uom = StaticParser.ParseEnumUomFromString<AccelerationLinearUom>(value);
                    }
                    xmlReader.MoveToElement();
                }
                this.Value = StaticParser.ParseDoubleFromString(StaticHelper.ReadString(xmlReader));
            }
        }
    }
}
