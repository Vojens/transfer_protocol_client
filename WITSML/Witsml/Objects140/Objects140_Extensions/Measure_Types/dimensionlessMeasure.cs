 

using System;
using System.Xml;

namespace Witsml.Objects140
{
    public partial class dimensionlessMeasure : IHandleXML
    {
        public dimensionlessMeasure()
        {
            this.uom = DimensionlessUom.unknown;
        }
        /// <summary>
        /// Convert XML string data into dimensionlessMeasure fields value
        /// </summary>
        /// <param name="xmlReader">reader that has dimensionlessMeasure node</param>
        /// <param name="IsNew">Is the object from stratch(true) or from db/serialization(false)</param>
        public void HandleXML(XmlReader xmlReader, bool isNew)
        {
            using (xmlReader)
            {
                xmlReader.Read();
                if (xmlReader.HasAttributes)
                {
                    if (xmlReader.MoveToAttribute("uom"))
                    {
                        //special conversion of enum string
                        string value = xmlReader.Value;
                        this.uom = StaticParser.ParseEnumUomFromString<DimensionlessUom>(value);
                    }
                        
                    xmlReader.MoveToElement();
                }
                this.Value = StaticParser.ParseDoubleFromString(StaticHelper.ReadString(xmlReader));
            }
        }
    }
}
