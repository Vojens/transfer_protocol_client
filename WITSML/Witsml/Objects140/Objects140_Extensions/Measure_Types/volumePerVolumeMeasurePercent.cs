 

using System;
using System.Collections.Generic;
using System.Text;
using System.Xml;

namespace Witsml.Objects140
{
    public partial class volumePerVolumeMeasurePercent : IHandleXML
    {
        public volumePerVolumeMeasurePercent()
        {
            this.uom = PercentUom.unknown;
        }
        /// <summary>
        /// Convert XML string data into volumePerVolumeMeasurePercent fields value
        /// </summary>
        /// <param name="xmlReader">reader that has volumePerVolumeMeasurePercent node</param>
        /// <param name="IsNew">Is the object from stratch(true) or from db/serialization(false)</param>
        public void HandleXML(XmlReader xmlReader, bool isNew)
        {
            using (xmlReader)
            {
                xmlReader.Read();
                if (xmlReader.HasAttributes)
                {
                    if (xmlReader.MoveToAttribute("uom"))
                    {
                        string value = xmlReader.Value;
                        this.uom = StaticParser.ParseEnumUomFromString<PercentUom>(value);
                    }
                    xmlReader.MoveToElement();
                }
                this.Value = StaticParser.ParseDoubleFromString(StaticHelper.ReadString(xmlReader));
            }
        }
    }
}
