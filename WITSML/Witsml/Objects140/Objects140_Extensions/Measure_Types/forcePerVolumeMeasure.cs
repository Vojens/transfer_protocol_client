 

using System;
using System.Collections.Generic;
using System.Text;
using System.Xml;

namespace Witsml.Objects140
{
    public partial class forcePerVolumeMeasure : IHandleXML
    {
        public forcePerVolumeMeasure()
        {
            this.uom = ForcePerVolumeUom.unknown;
        }
        /// <summary>
        /// Convert XML string data into forcePerVolumeMeasure fields value
        /// </summary>
        /// <param name="xmlReader">reader that has forcePerVolumeMeasure node</param>
        /// <param name="IsNew">Is the object from stratch(true) or from db/serialization(false)</param>
        public void HandleXML(XmlReader xmlReader, bool isNew)
        {
            using (xmlReader)
            {
                xmlReader.Read();
                if (xmlReader.HasAttributes)
                {
                    if (xmlReader.MoveToAttribute("uom"))
                    {
                        string value = xmlReader.Value;
                        this.uom = StaticParser.ParseEnumUomFromString<ForcePerVolumeUom>(value);
                    }
                    xmlReader.MoveToElement();
                }
                this.Value = StaticParser.ParseDoubleFromString(StaticHelper.ReadString(xmlReader));
            }
        }
    }
}
