 

using System;
using System.Collections.Generic;
using System.Text;
using System.Xml;

namespace Witsml.Objects140
{
    public partial class magneticInductionMeasure : IHandleXML
    {
        public magneticInductionMeasure()
        {
            this.uom = MagneticInductionUom.unknown;
        }
        /// <summary>
        /// Convert XML string data into magneticInductionMeasure fields value
        /// </summary>
        /// <param name="xmlReader">reader that has magneticInductionMeasure node</param>
        /// <param name="IsNew">Is the object from stratch(true) or from db/serialization(false)</param>
        public void HandleXML(XmlReader xmlReader, bool isNew)
        {
            using (xmlReader)
            {
                xmlReader.Read();
                if (xmlReader.HasAttributes)
                {
                    if (xmlReader.MoveToAttribute("uom"))
                        this.uom = StaticParser.ParseEnumUomFromString<MagneticInductionUom>(xmlReader.Value);
                    xmlReader.MoveToElement();
                }
                this.Value = StaticParser.ParseDoubleFromString(StaticHelper.ReadString(xmlReader));
            }
        }
    }
}
