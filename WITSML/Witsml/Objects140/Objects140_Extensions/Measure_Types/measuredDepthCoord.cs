 

using System;
using System.Xml;

namespace Witsml.Objects140
{
    public partial class measuredDepthCoord : IHandleXML
    {
        public measuredDepthCoord()
        {
            this.uom = MeasuredDepthUom.unknown;
        }
        /// <summary>
        /// Convert XML string data into measuredDepthCoord fields value
        /// </summary>
        /// <param name="xmlReader">reader that has measuredDepthCoord node</param>
        /// <param name="IsNew">Is the object from stratch(true) or from db/serialization(false)</param>
        public void HandleXML(XmlReader xmlReader, bool isNew)
        {
            using (xmlReader)
            {
                xmlReader.Read();
                if (xmlReader.HasAttributes)
                {
                    if (xmlReader.MoveToAttribute("uom"))
                        this.uom = StaticParser.ParseEnumUomFromString<MeasuredDepthUom>(xmlReader.Value);
                    if (xmlReader.MoveToAttribute("datum"))
                        this.datum = xmlReader.Value;
                    xmlReader.MoveToElement();
                }
                this.Value = StaticParser.ParseDoubleFromString(StaticHelper.ReadString(xmlReader));
            }
        }

    }
}
