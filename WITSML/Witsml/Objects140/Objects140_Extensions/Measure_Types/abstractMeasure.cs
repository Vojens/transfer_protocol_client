 

using System;
using System.Collections.Generic;
using System.Text;

namespace Witsml.Objects140
{
    public partial class abstractMeasure
    {
        public override string ToString()
        {
            return this.Value.ToString();
        }
    }
}
