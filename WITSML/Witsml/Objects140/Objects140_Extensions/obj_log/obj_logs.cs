﻿ 

using System;
using System.Collections.Generic;
using System.IO;
using System.Text;
using System.Xml;
using System.Xml.Serialization;
using System.Linq;
using Witsml.Common.Exceptions;


namespace Witsml.Objects140
{
    //Extension class for wells document that inherit from WITSMLDocument and Implement Decode Function
    public partial class obj_logs : WITSMLDocument
    {

        public override WITSMLObject GetSingleObject(int Item)
        {
            return this.log[Item];
        }

        /// <summary>
        /// Deserialize XML string into WitsmlDocument object
        /// </summary>
        /// <param name="XMLin">XML string to be serialized</param>
        /// <returns>Number of singular log inside</returns>
        public override int Decode(ref string xmlIn)
        {
            //return value
            //number of log inside xmlIn
            int countOfLog = 0;
            try
            {
                //check whether collection is from stratch(isNew = true) or from deserialization(isNew=false)
                bool isNew = this.log == null;

                //create temporary list to help with data manipulation
                List<obj_log> listLog = isNew ? new List<obj_log>() : new List<obj_log>(this.log);

                //Consume XML string and convert node value to variable based on node name
                using (StringReader stringReader = new StringReader(xmlIn))
                {
                    XmlReaderSettings setting = new XmlReaderSettings();
                    setting.ConformanceLevel = ConformanceLevel.Document;
                    setting.IgnoreComments = true;
                    setting.IgnoreWhitespace = true;
                    setting.IgnoreProcessingInstructions = true;
                    using (XmlReader xmlReader = XmlReader.Create(stringReader, setting))
                    {
                        while (xmlReader.Read())
                        {
                            //assign version
                            xmlReader.MoveToContent();
                            if (xmlReader.MoveToAttribute("version"))
                                this.version = xmlReader.Value;
                            else
                                this.version = "1.4.0";//no version specified,set it to 1.4.0

                            //pre-checking on whether node is the correct child based on depth,start tag and not empty
                            if (xmlReader.Depth == 1 && xmlReader.IsStartElement() && !xmlReader.IsEmptyElement)
                            {
                                switch (xmlReader.Name)
                                {
                                    case "documentInfo":
                                        if (this.documentInfo == null)
                                            this.documentInfo = new cs_documentInfo();
                                        this.documentInfo.HandleXML(xmlReader.ReadSubtree(), isNew);
                                        break;
                                    case "log":
                                        obj_log log;
                                        //foreign key(well id,wellbore id) and primary key(wellbore id) to identify existing log
                                        string currentUIDWell = string.Empty;
                                        string currentUIDWellbore = string.Empty;
                                        string currentUID = string.Empty;

                                        //Update log
                                        if (!isNew)
                                        {
                                            //check for uid attribute and assign it to temporary string for searching in collection
                                            if (xmlReader.HasAttributes)
                                            {
                                                if (xmlReader.MoveToAttribute("uidWell"))
                                                {
                                                    currentUIDWell = xmlReader.Value;
                                                }
                                                if (xmlReader.MoveToAttribute("uidWellbore"))
                                                {
                                                    currentUIDWellbore = xmlReader.Value;
                                                }
                                                if (xmlReader.MoveToAttribute("uid"))
                                                {
                                                    currentUID = xmlReader.Value;
                                                }
                                                xmlReader.MoveToElement();
                                            }
                                            //look for log based on its UID and update the log with xml string
                                            if (currentUID != string.Empty)
                                            {
                                                log = listLog.FirstOrDefault(
                                                    delegate(obj_log objLog)
                                                    { if (objLog.uidWell == currentUIDWell && objLog.uidWellbore == currentUIDWellbore && objLog.uid == currentUID)return true; else return false; });
                                                if (log != null)
                                                {
                                                    log.HandleXML(xmlReader.ReadSubtree(), isNew);
                                                    countOfLog++;
                                                    continue;
                                                }
                                                else
                                                {
                                                    //Update can't process log without match
                                                    continue;
                                                }
                                            }
                                            else
                                            {
                                                //Update can't process log without uid
                                                continue;
                                            }
                                        }

                                        //Add New Well 
                                        log = new obj_log();
                                        log.HandleXML(xmlReader.ReadSubtree(), true);
                                        listLog.Add(log);
                                        countOfLog++;
                                        break;
                                    default:
                                        break;
                                }
                            }
                        }
                        //assign list to wellbore collection
                        this.log = listLog.ToArray();
                    }
                }
            }
            catch (XmlException ex)
            {
                throw new Exceptions.UnableToDecodeException(ex.Message, WITSMLReturnCode.WITSMLDocumentError, ex.InnerException);
            }
            catch (NullReferenceException ex)
            {
                throw new Exceptions.UnableToDecodeException(ex.Message, WITSMLReturnCode.WITSMLEmptyDocument, ex.InnerException);
            }
            catch (Exceptions.UnableToDecodeException ex)
            {
                throw ex;
            }
            catch (Exception ex)
            {
                throw new Exceptions.UnableToDecodeException(ex.Message, WITSMLReturnCode.WITSMLUnkownCause, ex.InnerException);
            }
            return countOfLog;

        }

        /// <summary>
        /// Serialize without LogData
        /// </summary>
        /// <returns>xml string representation of obj_logs</returns>
        public override string Serialize()
        {
            //we want to ignore logData on serialize,so we create xmlAttribute that will be ignored
            XmlAttributes xmlAtts = new XmlAttributes();
            xmlAtts.XmlIgnore = true;

            //specify override for logData which accept ignore attribute
            XmlAttributeOverrides xmlAttsOverrides = new XmlAttributeOverrides();
            xmlAttsOverrides.Add(typeof(obj_log), "logData", xmlAtts);

            //do serialize with override parameter
            return this.Serialize(xmlAttsOverrides);
        }

        /// <summary>
        /// Serialize With Log Data
        /// </summary>
        /// <returns>xml string representation of obj_logs</returns>
        public string SerializeWithData()
        {
            //call method in log to convert custom log data container to serialization log data container
            if (this.log != null)
            {
                for (int i = 0; i < this.log.Length; i++)
                {
                    this.log[i].PrepareLogDataForSerialize();
                }
            }

            return base.Serialize();
        }
    }
}
