﻿ 

using System;
using System.Collections.Generic;
using System.Text;
using System.Xml;

namespace Witsml.Objects140
{
    public partial class cs_depthRegPoint : IHandleXML
    {

        public void HandleXML(XmlReader xmlReader, bool isNew)
        {
            using (xmlReader)
            {
                xmlReader.MoveToContent();
                while (xmlReader.Read())
                {
                    switch (xmlReader.Name)
                    {
                        case "x":
                            this.x = StaticParser.ParseIntFromString(StaticHelper.ReadString(xmlReader));
                            break;
                        case "y":
                            this.y = StaticParser.ParseIntFromString(StaticHelper.ReadString(xmlReader));
                            break;
                        default:
                            break;
                    }
                }
            }
        }

    }
}
