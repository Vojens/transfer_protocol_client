﻿ 

using System;
using System.Collections.Generic;
using System.Text;
using System.Xml;

namespace Witsml.Objects140
{
    public partial class cs_depthRegTrack : IHandleXML, IHasID
    {
        //this will need to be ignored
        [System.Xml.Serialization.XmlIgnore]
        public string UID
        {
            get { return this.uid; }
        }

        public cs_depthRegTrack()
        {
            this.type = LogTrackType.unknown;
        }
        public void HandleXML(XmlReader xmlReader, bool isNew)
        {
            using (xmlReader)
            {
                xmlReader.MoveToContent();
                if (xmlReader.HasAttributes)
                {
                    if (xmlReader.MoveToAttribute("uid"))
                        StaticHelper.HandleUID(xmlReader.Value, "depthRegTrack uid", out this.uid);
                    xmlReader.MoveToElement();
                }
                while (xmlReader.Read())
                {
                    //only take child node
                    if (xmlReader.Depth == 1 && xmlReader.IsStartElement() && !xmlReader.IsEmptyElement)
                    {
                        switch (xmlReader.Name)
                        {
                            case "name":
                                this.name = StaticHelper.ReadString(xmlReader);
                                break;
                            case "type":
                                this.type = StaticParser.ParseEnumFromString<LogTrackType>(StaticHelper.ReadString(xmlReader), LogTrackType.unknown);
                                break;
                            case "leftEdge":
                                if (this.leftEdge == null)
                                    this.leftEdge = new lengthMeasure();
                                this.leftEdge.HandleXML(xmlReader.ReadSubtree(), isNew);
                                break;
                            case "rightEdge":
                                if (this.rightEdge == null)
                                    this.rightEdge = new lengthMeasure();
                                this.rightEdge.HandleXML(xmlReader.ReadSubtree(), isNew);
                                break;
                            default:
                                break;
                        }
                    }
                }
            }
        }

    }
}
