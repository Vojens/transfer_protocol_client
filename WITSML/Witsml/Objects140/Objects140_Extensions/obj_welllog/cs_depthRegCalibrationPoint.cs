﻿ 

using System;
using System.Collections.Generic;
using System.Text;
using System.Xml;

namespace Witsml.Objects140
{
    public partial class cs_depthRegCalibrationPoint : IHandleXML, IHasID
    {
        //this will need to be ignored
        [System.Xml.Serialization.XmlIgnore]
        public string UID
        {
            get { return this.uid; }
        }

        public cs_depthRegCalibrationPoint()
        {
            this.role = CalibrationPointRole.unknown;
        }
        public void HandleXML(XmlReader xmlReader, bool isNew)
        {
            bool isParameterNew = this.parameter == null;
            //temporary list for data manipulation
            List<cs_depthRegParameter> listParameter = isParameterNew ? new List<cs_depthRegParameter>() : new List<cs_depthRegParameter>(this.parameter);
            List<string> listComment = this.comment == null ? new List<string>() : new List<string>(this.comment);

            using (xmlReader)
            {
                xmlReader.MoveToContent();
                if (xmlReader.HasAttributes)
                {
                    if (xmlReader.MoveToAttribute("uid"))
                        StaticHelper.HandleUID(xmlReader.Value, "depthRegCalibrationPoint uid", out this.uid);
                    xmlReader.MoveToElement();
                }
                while (xmlReader.Read())
                {
                    //only take child node
                    if (xmlReader.Depth == 1 && xmlReader.IsStartElement() && !xmlReader.IsEmptyElement)
                    {
                        switch (xmlReader.Name)
                        {
                            case "index":
                                if (this.index == null)
                                    this.index = new genericMeasure();
                                this.index.HandleXML(xmlReader.ReadSubtree(), isNew);
                                break;
                            case "point":
                                if (this.point == null)
                                    this.point = new cs_depthRegPoint();
                                this.point.HandleXML(xmlReader.ReadSubtree(), isNew);
                                break;
                            case "track":
                                if (this.track == null)
                                    this.track = new refNameString();
                                this.track.HandleXML(xmlReader.ReadSubtree(), isNew);
                                break;
                            case "role":
                                this.role = StaticParser.ParseEnumFromString<CalibrationPointRole>(StaticHelper.ReadString(xmlReader), CalibrationPointRole.unknown);
                                break;
                            case "fraction":
                                this.fraction = StaticParser.ParseDoubleFromString(StaticHelper.ReadString(xmlReader));
                                this.fractionSpecified = true;
                                break;
                            case "parameter":
                                //collection
                                StaticHelper.AddUpdateWithUid<cs_depthRegParameter>(xmlReader, isParameterNew, ref listParameter);
                                break;
                            case "comment":
                                //collection
                                listComment.Add(StaticHelper.ReadString(xmlReader));
                                break;
                            default:
                                break;
                        }
                    }
                }
                //assign list to collection
                this.parameter = listParameter.ToArray();
                this.comment = listComment.ToArray();
            }
        }
    }
}
