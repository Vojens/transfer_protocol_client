﻿ 

using System;
using System.Collections.Generic;
using System.Text;
using System.Xml;

namespace Witsml.Objects140
{
    public partial class cs_depthRegLogSection : IHandleXML, IHasID
    {
        //this will need to be ignored
        [System.Xml.Serialization.XmlIgnore]
        public string UID
        {
            get { return this.uid; }
        }

        public cs_depthRegLogSection()
        {
            this.logSectionType = LogSectionType.unknown;
            this.indexType = LogIndexType.unknown;
            this.indexReference = ElevCodeEnum.unknown;
        }

        public void HandleXML(XmlReader xmlReader, bool isNew)
        {
            bool isTrackNew = this.track == null;
            bool isUpperCurveScaleRectNew = this.upperCurveScaleRect == null;
            bool isLowerCurveScaleRectNew = this.lowerCurveScaleRect == null;
            bool isWhitespaceNew = this.whitespace == null;
            bool isCalibrationPointNew = this.calibrationPoint == null;
            bool isParameterNew = this.parameter == null;
            //temporary list
            List<refNameString> listCurveInfo = this.curveInfo == null ? new List<refNameString>() : new List<refNameString>(this.curveInfo);
            List<cs_depthRegTrack> listTrack = isTrackNew ? new List<cs_depthRegTrack>() : new List<cs_depthRegTrack>(this.track);
            List<cs_depthRegRectangular> listUpperCurveScaleRect = isUpperCurveScaleRectNew ? new List<cs_depthRegRectangular>() : new List<cs_depthRegRectangular>(this.upperCurveScaleRect);
            List<cs_depthRegRectangular> listLowerCurveScaleRect = isLowerCurveScaleRectNew ? new List<cs_depthRegRectangular>() : new List<cs_depthRegRectangular>(this.lowerCurveScaleRect);
            List<cs_depthRegRectangular> listWhitespace = isWhitespaceNew ? new List<cs_depthRegRectangular>() : new List<cs_depthRegRectangular>(this.whitespace);
            List<cs_depthRegCalibrationPoint> listCalibrationPoint = isCalibrationPointNew ? new List<cs_depthRegCalibrationPoint>() : new List<cs_depthRegCalibrationPoint>(this.calibrationPoint);
            List<cs_depthRegParameter> listParameter = isParameterNew ? new List<cs_depthRegParameter>() : new List<cs_depthRegParameter>(this.parameter);
            List<string> listComment = this.comment == null ? new List<string>() : new List<string>(this.comment);

            using (xmlReader)
            {
                xmlReader.MoveToContent();
                if (xmlReader.HasAttributes)
                {
                    if (xmlReader.MoveToAttribute("uid"))
                        StaticHelper.HandleUID(xmlReader.Value, "depthRegLogSection uid", out this.uid);
                    xmlReader.MoveToElement();
                }
                while (xmlReader.Read())
                {
                    //only take child node
                    if (xmlReader.Depth == 1 && xmlReader.IsStartElement() && !xmlReader.IsEmptyElement)
                    {
                        switch (xmlReader.Name)
                        {
                            case "logSectionSequenceNumber":
                                this.logSectionSequenceNumber = StaticParser.ParseShortFromString(StaticHelper.ReadString(xmlReader));
                                break;
                            case "logSectionType":
                                StaticParser.SetEnumFromString<LogSectionType>(StaticHelper.ReadString(xmlReader), out this.logSectionType, out this.logSectionTypeSpecified);
                                break;
                            case "logSectionName":
                                this.logSectionName = StaticHelper.ReadString(xmlReader);
                                break;
                            case "curveInfo":
                                //collection
                                refNameString curveInfo = new refNameString();
                                curveInfo.HandleXML(xmlReader.ReadSubtree(), isNew);
                                listCurveInfo.Add(curveInfo);
                                break;
                            case "track":
                                //collection
                                StaticHelper.AddUpdateWithUid<cs_depthRegTrack>(xmlReader, isTrackNew, ref listTrack);
                                break;
                            case "logMatrix":
                                this.logMatrix = StaticHelper.ReadString(xmlReader);
                                break;
                            case "scaleNumerator":
                                if (this.scaleNumerator == null)
                                    this.scaleNumerator = new lengthMeasure();
                                this.scaleNumerator.HandleXML(xmlReader.ReadSubtree(), isNew);
                                break;
                            case "scaleDenominator":
                                if (this.scaleDenominator == null)
                                    this.scaleDenominator = new genericMeasure();
                                this.scaleDenominator.HandleXML(xmlReader.ReadSubtree(), isNew);
                                break;
                            case "indexType":
                                this.indexType = StaticParser.ParseEnumFromString<LogIndexType>(StaticHelper.ReadString(xmlReader), LogIndexType.unknown);
                                break;
                            case "indexUom":
                                this.indexUom = StaticHelper.ReadString(xmlReader);
                                break;
                            case "indexReference":
                                StaticParser.SetEnumFromString<ElevCodeEnum>(StaticHelper.ReadString(xmlReader), out this.indexReference, out this.indexReferenceSpecified);
                                break;
                            case "minInterval":
                                if (this.minInterval == null)
                                    this.minInterval = new genericMeasure();
                                this.minInterval.HandleXML(xmlReader.ReadSubtree(), isNew);
                                break;
                            case "maxInterval":
                                if (this.maxInterval == null)
                                    this.maxInterval = new genericMeasure();
                                this.maxInterval.HandleXML(xmlReader.ReadSubtree(), isNew);
                                break;
                            case "upperCurveScaleRect":
                                //collection
                                StaticHelper.AddUpdateWithUid<cs_depthRegRectangular>(xmlReader, isUpperCurveScaleRectNew, ref listUpperCurveScaleRect);
                                break;
                            case "lowerCurveScaleRect":
                                //collection
                                StaticHelper.AddUpdateWithUid<cs_depthRegRectangular>(xmlReader, isLowerCurveScaleRectNew, ref listLowerCurveScaleRect);
                                break;
                            case "whitespace":
                                //collection
                                StaticHelper.AddUpdateWithUid<cs_depthRegRectangular>(xmlReader, isWhitespaceNew, ref listWhitespace);
                                break;
                            case "calibrationPoint":
                                //collection
                                StaticHelper.AddUpdateWithUid<cs_depthRegCalibrationPoint>(xmlReader, isCalibrationPointNew, ref listCalibrationPoint);
                                break;
                            case "parameter":
                                //collection
                                StaticHelper.AddUpdateWithUid<cs_depthRegParameter>(xmlReader, isParameterNew, ref listParameter);
                                break;
                            case "comment":
                                //collection
                                listComment.Add(StaticHelper.ReadString(xmlReader));
                                break;
                            default:
                                break;
                        }
                    }
                }
                //assign list back to collection
                this.curveInfo = listCurveInfo.ToArray();
                this.track = listTrack.ToArray();
                this.upperCurveScaleRect = listUpperCurveScaleRect.ToArray();
                this.lowerCurveScaleRect = listLowerCurveScaleRect.ToArray();
                this.whitespace = listWhitespace.ToArray();
                this.calibrationPoint = listCalibrationPoint.ToArray();
                this.parameter = listParameter.ToArray();
                this.comment = listComment.ToArray();

            }
        }

    }
}
