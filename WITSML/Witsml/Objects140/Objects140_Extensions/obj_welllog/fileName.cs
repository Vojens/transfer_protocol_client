﻿ 

using System;
using System.Collections.Generic;
using System.Text;
using System.Xml;

namespace Witsml.Objects140
{
    public partial class fileName : IHandleXML
    {
        public fileName()
        {
            this.nameType = FileNameType.unknown;
        }

        public void HandleXML(XmlReader xmlReader, bool isNew)
        {
            using (xmlReader)
            {
                xmlReader.Read();
                if (xmlReader.HasAttributes)
                {
                    if (xmlReader.MoveToAttribute("nameType"))
                        this.nameType = StaticParser.ParseEnumFromString<FileNameType>(xmlReader.Value, FileNameType.unknown);
                    xmlReader.MoveToElement();
                }
                this.Value = StaticHelper.ReadString(xmlReader);
            }
        }

    }
}
