﻿ 

using System;
using System.Collections.Generic;
using System.Text;
using System.IO;
using System.Xml;
using System.Linq;
using Witsml.Common.Exceptions;


namespace Witsml.Objects140
{
    public partial class obj_wellLogs : WITSMLDocument
    {
        public override WITSMLObject GetSingleObject(int Item)
        {
            return this.wellLog[Item];
        }
        /// <summary>
        /// Deserialize XML string into WitsmlDocument object
        /// </summary>
        /// <param name="XMLin">XML string to be serialized</param>
        /// <returns>Number of singular mudLog inside</returns>
        public override int Decode(ref string xmlIn)
        {
            //return value
            //number of rig inside xmlIn
            int countOfWellLog = 0;
            try
            {
                //check whether collection is from stratch(isNew = true) or from deserialization(isNew=false)
                bool isNew = this.wellLog == null;

                //create temporary list to help with data manipulation
                List<obj_wellLog> listWellLog = isNew ? new List<obj_wellLog>() : new List<obj_wellLog>(this.wellLog);

                //Consume XML string and convert node value to variable based on node name
                using (StringReader stringReader = new StringReader(xmlIn))
                {
                    XmlReaderSettings setting = new XmlReaderSettings();
                    setting.ConformanceLevel = ConformanceLevel.Document;
                    setting.IgnoreComments = true;
                    setting.IgnoreWhitespace = true;
                    setting.IgnoreProcessingInstructions = true;
                    using (XmlReader xmlReader = XmlReader.Create(stringReader, setting))
                    {
                        while (xmlReader.Read())
                        {
                            //assign version
                            xmlReader.MoveToContent();
                            if (xmlReader.MoveToAttribute("version"))
                                this.version = xmlReader.Value;
                            else
                                this.version = "1.4.0";//no version specified,set it to 1.4.0

                            //pre-checking on whether node is the correct child based on depth,start tag and not empty
                            if (xmlReader.Depth == 1 && xmlReader.IsStartElement() && !xmlReader.IsEmptyElement)
                            {
                                switch (xmlReader.Name)
                                {
                                    case "documentInfo":
                                        if (this.documentInfo == null)
                                            this.documentInfo = new cs_documentInfo();
                                        this.documentInfo.HandleXML(xmlReader.ReadSubtree(), isNew);
                                        break;
                                    case "wellLog":
                                        obj_wellLog wellLogObj;
                                        //foreign key(well id,wellbore id) and primary key(wellbore id) to identify existing mudLog
                                        string currentUIDWell = string.Empty;
                                        string currentUIDWellbore = string.Empty;
                                        string currentUID = string.Empty;

                                        //Update WellLog
                                        if (!isNew)
                                        {
                                            //check for uid attribute and assign it to temporary string for searching in collection
                                            if (xmlReader.HasAttributes)
                                            {
                                                if (xmlReader.MoveToAttribute("uidWell"))
                                                {
                                                    currentUIDWell = xmlReader.Value;
                                                }
                                                if (xmlReader.MoveToAttribute("uidWellbore"))
                                                {
                                                    currentUIDWellbore = xmlReader.Value;
                                                }
                                                if (xmlReader.MoveToAttribute("uid"))
                                                {
                                                    currentUID = xmlReader.Value;
                                                }
                                                xmlReader.MoveToElement();
                                            }
                                            //look for well log based on its UID and update the well log with xml string
                                            if (currentUID != string.Empty)
                                            {
                                                wellLogObj = listWellLog.FirstOrDefault(
                                                    delegate(obj_wellLog objRig)
                                                    { if (objRig.uidWell == currentUIDWell && objRig.uidWellbore == currentUIDWellbore && objRig.uid == currentUID)return true; else return false; });
                                                if (wellLogObj != null)
                                                {
                                                    wellLogObj.HandleXML(xmlReader.ReadSubtree(), isNew);
                                                    countOfWellLog++;
                                                    continue;
                                                }
                                                else
                                                {
                                                    //Update can't process rig without match
                                                    continue;
                                                }
                                            }
                                            else
                                            {
                                                //Update can't process rig without uid
                                                continue;
                                            }
                                        }

                                        //Add New WellLog
                                        wellLogObj = new obj_wellLog();
                                        wellLogObj.HandleXML(xmlReader.ReadSubtree(), true);
                                        listWellLog.Add(wellLogObj);
                                        countOfWellLog++;
                                        break;
                                    default:
                                        break;
                                }
                            }
                        }
                        //assign list to wellbore collection
                        this.wellLog = listWellLog.ToArray();
                    }
                }
            }
            catch (XmlException ex)
            {
                throw new Exceptions.UnableToDecodeException(ex.Message, WITSMLReturnCode.WITSMLDocumentError, ex.InnerException);
            }
            catch (NullReferenceException ex)
            {
                throw new Exceptions.UnableToDecodeException(ex.Message, WITSMLReturnCode.WITSMLEmptyDocument, ex.InnerException);
            }
            catch (Exceptions.UnableToDecodeException ex)
            {
                throw ex;
            }
            catch (Exception ex)
            {
                throw new Exceptions.UnableToDecodeException(ex.Message, WITSMLReturnCode.WITSMLUnkownCause, ex.InnerException);
            }
            return countOfWellLog;
        }
    }
}
