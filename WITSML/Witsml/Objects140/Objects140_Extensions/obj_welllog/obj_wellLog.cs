﻿ 

using System;
using System.Collections.Generic;
using System.IO;
using System.Text;
using System.Xml;
using System.Xml.Linq;
using System.Xml.Serialization;
using System.Linq;
using Witsml.Common.Exceptions;
using Witsml.Exceptions;


namespace Witsml.Objects140
{
    // objectGrowing changed to enum,datagroup added,dataDelimiter added,depthRegistrationData added
    [XmlRoot("wellLog", Namespace = "http://www.witsml.org/schemas/140", IsNullable = false)]
    public partial class obj_wellLog : WITSMLObject, IHandleXML
    {
        public obj_wellLog()
        {
            this.indexType = LogIndexType.unknown;
            this.version = WITSMLVersion.V140;
        }

        /// <summary>
        /// Decode XML string into rig
        /// </summary>
        /// <param name="xmlIn">XML string</param>
        public override void Decode(ref string xmlIn)
        {
            try
            {
                //Consume XML string and convert node value to variable based on node name
                using (StringReader stringReader = new StringReader(xmlIn))
                {
                    XmlReaderSettings setting = new XmlReaderSettings();
                    setting.ConformanceLevel = ConformanceLevel.Document;
                    setting.IgnoreComments = true;
                    setting.IgnoreWhitespace = true;
                    setting.IgnoreProcessingInstructions = true;
                    using (XmlReader xmlReader = XmlReader.Create(stringReader, setting))
                    {
                        //determine add or update operation
                        bool isNew;
                        if (this.uid == string.Empty)
                            isNew = true;
                        else isNew = false;

                        this.HandleXML(xmlReader, isNew);
                    }
                }
            }
            catch (XmlException ex)
            {
                throw new Exceptions.UnableToDecodeException(ex.Message, WITSMLReturnCode.WITSMLDocumentError, ex.InnerException);
            }
            catch (NullReferenceException ex)
            {
                throw new Exceptions.UnableToDecodeException(ex.Message, WITSMLReturnCode.WITSMLEmptyDocument, ex.InnerException);
            }
            catch (Exceptions.UnableToDecodeException ex)
            {
                throw ex;
            }
            catch (Exception ex)
            {
                throw new Exceptions.UnableToDecodeException(ex.Message, WITSMLReturnCode.WITSMLUnkownCause, ex.InnerException);
            }
        }

        /// <summary>
        /// Convert XML string data into rig fields value
        /// </summary>
        /// <param name="xmlReader">reader that has Rig node</param>
        /// <param name="IsNew">Is the object from stratch(true) or from db/serialization(false)</param>
        public void HandleXML(XmlReader xmlReader, bool isNew)
        {
            //used to skip read on logCurveInfo handling
            bool hasRead = false;
            //condition for add/update
            bool isLogParamNew = this.logParam == null;
            bool isWellLogCurveInfoNew = this.logCurveInfo == null;
            bool isBlockInfoNew = this.blockInfo == null;

            //temporary list
            List<indexedObject> listLogParam = isLogParamNew ? new List<indexedObject>() : new List<indexedObject>(this.logParam);
            List<cs_wellLogCurveInfo> listWellLogCurveInfo = isWellLogCurveInfoNew ? new List<cs_wellLogCurveInfo>() : new List<cs_wellLogCurveInfo>(this.logCurveInfo);
            List<cs_blockInfo> listBlockInfo = isBlockInfoNew ? new List<cs_blockInfo>() : new List<cs_blockInfo>(this.blockInfo);

            using (xmlReader)
            {
                //assign uid for the new well
                xmlReader.MoveToContent();
                if (xmlReader.HasAttributes)
                {
                    if (xmlReader.MoveToAttribute("uidWell"))
                        StaticHelper.HandleUID(xmlReader.Value, "wellLog uidWell", out this.uidWell);
                    if (xmlReader.MoveToAttribute("uidWellbore"))
                        StaticHelper.HandleUID(xmlReader.Value, "wellLog uidWellbore", out this.uidWellbore);
                    if (xmlReader.MoveToAttribute("uid"))
                        StaticHelper.HandleUID(xmlReader.Value, "wellLog uid", out this.uid);
                    xmlReader.MoveToElement();
                }
                while (!xmlReader.EOF)
                {
                    if (!hasRead)
                    {
                        xmlReader.Read();
                    }
                    else
                    {
                        //skip reading on logcurveInfo node
                        hasRead = false;
                    }
                    //only take child node
                    if (xmlReader.Depth == 1 && xmlReader.IsStartElement() && !xmlReader.IsEmptyElement)
                    {
                        switch (xmlReader.Name)
                        {
                            case "nameWell":
                                this.nameWell = StaticHelper.ReadString(xmlReader);
                                break;
                            case "nameWellbore":
                                this.nameWellbore = StaticHelper.ReadString(xmlReader);
                                break;
                            case "name":
                                this.name = StaticHelper.ReadString(xmlReader);
                                break;
                            case "objectGrowing":
                                StaticParser.SetEnumFromString<ObjectGrowingState>(StaticHelper.ReadString(xmlReader), out this.objectGrowing, out this.objectGrowingSpecified);
                                break;
                            case "dataGroup":
                                this.dataGroup = StaticHelper.ReadString(xmlReader);
                                break;
                            case "serviceCompany":
                                this.serviceCompany = StaticHelper.ReadString(xmlReader);
                                break;
                            case "runNumber":
                                this.runNumber = StaticHelper.ReadString(xmlReader);
                                break;
                            case "bhaRunNumber":
                                this.bhaRunNumber = StaticParser.ParseShortFromString(StaticHelper.ReadString(xmlReader));
                                this.bhaRunNumberSpecified = true;
                                break;
                            case "creationDate":
                                this.creationDate = StaticParser.ParseDateTimeFromString(StaticHelper.ReadString(xmlReader));
                                this.creationDateSpecified = true;
                                break;
                            case "description":
                                this.description = StaticHelper.ReadString(xmlReader);
                                break;
                            case "dataDelimiter":
                                this.dataDelimiter = StaticHelper.ReadString(xmlReader);
                                break;
                            case "indexType":
                                this.indexType = StaticParser.ParseEnumFromString<LogIndexType>(StaticHelper.ReadString(xmlReader), LogIndexType.unknown);
                                break;
                            case "minIndex":
                                if (this.minIndex == null)
                                    this.minIndex = new genericMeasure();
                                this.minIndex.HandleXML(xmlReader.ReadSubtree(), isNew);
                                break;
                            case "maxIndex":
                                if (this.maxIndex == null)
                                    this.maxIndex = new genericMeasure();
                                this.maxIndex.HandleXML(xmlReader.ReadSubtree(), isNew);
                                break;
                            case "minDateTimeIndex":
                                this.minDateTimeIndex = StaticParser.ParseDateTimeFromString(StaticHelper.ReadString(xmlReader));
                                this.minDateTimeIndexSpecified = true;
                                break;
                            case "maxDateTimeIndex":
                                this.maxDateTimeIndex = StaticParser.ParseDateTimeFromString(StaticHelper.ReadString(xmlReader));
                                this.maxDateTimeIndexSpecified = true;
                                break;
                            case "nullValue":
                                this.nullValue = StaticHelper.ReadString(xmlReader);
                                break;
                            case "logParam":
                                //collection
                                //StaticHelper.AddUpdateIndexedObj(xmlReader, isLogParamNew, ref listLogParam);
                                break;
                            case "logCurveInfo":
                                //collection
                                cs_wellLogCurveInfo wellLogCurveInfoObj;
                                XElement wellLogCurveElement = XElement.ReadFrom(xmlReader) as XElement;
                                hasRead = true;

                                //Only care about Mnemonic and columnIndex,which handling is on object level
                                string mnemonicValue = string.Empty;
                                XNamespace wits = xmlReader.NamespaceURI;
                                if (wellLogCurveElement.HasElements && wellLogCurveElement.Element(wits + "mnemonic") != null)
                                {
                                    mnemonicValue = wellLogCurveElement.Element(wits + "mnemonic").Value;
                                }

                                //look for existing object
                                wellLogCurveInfoObj = listWellLogCurveInfo.FirstOrDefault(
                                        delegate(cs_wellLogCurveInfo objWellLogCurve)
                                        { if (objWellLogCurve.mnemonic == mnemonicValue)return true; else return false; });

                                //collection exists
                                if (!isWellLogCurveInfoNew)
                                {
                                    //UpdateToStore

                                    //update if there is existing object with the same mnemonic
                                    if (wellLogCurveInfoObj != null)
                                    {
                                        //update
                                        wellLogCurveInfoObj.objectState = ObjectState.Update;
                                        wellLogCurveInfoObj.HandleXML(wellLogCurveElement.CreateReader(), isWellLogCurveInfoNew);
                                    }
                                    else
                                    {
                                        //new
                                        wellLogCurveInfoObj = new cs_wellLogCurveInfo();
                                        wellLogCurveInfoObj.HandleXML(wellLogCurveElement.CreateReader(), isWellLogCurveInfoNew);
                                        wellLogCurveInfoObj.objectState = ObjectState.New;
                                        listWellLogCurveInfo.Add(wellLogCurveInfoObj);
                                    }
                                }
                                else
                                {
                                    //AddToStore and GetFromStore

                                    //check whether mnemonic is duplicate first,and then only process xml handling
                                    if (wellLogCurveInfoObj != null)
                                    {
                                        throw new Exceptions.UnableToDecodeException("Duplicate Mnemonic on Mnemonic =" + mnemonicValue, WITSMLReturnCode.WITSMLMnemonicExists, true);
                                    }

                                    //Add
                                    wellLogCurveInfoObj = new cs_wellLogCurveInfo();
                                    wellLogCurveInfoObj.HandleXML(wellLogCurveElement.CreateReader(), isWellLogCurveInfoNew);
                                    wellLogCurveInfoObj.objectState = ObjectState.Normal;
                                    listWellLogCurveInfo.Add(wellLogCurveInfoObj);

                                }
                                break;
                            case "blockInfo":
                                //collection
                                cs_blockInfo blokInfoObj;
                                string currentBlockInfoID = string.Empty;

                                if (xmlReader.HasAttributes)
                                {
                                    //check for uid attribute and assign it to temporary string for searching in collection
                                    if (xmlReader.MoveToAttribute("uid"))
                                    {
                                        currentBlockInfoID = xmlReader.Value;
                                        xmlReader.MoveToElement();
                                    }
                                }

                                //check whether uid is empty
                                if (!string.IsNullOrEmpty(currentBlockInfoID))
                                {
                                    //update
                                    if (!isBlockInfoNew)
                                    {
                                        blokInfoObj = listBlockInfo.FirstOrDefault(
                                            delegate(cs_blockInfo objBlockInfo)
                                            { if (objBlockInfo.UID == currentBlockInfoID)return true; else return false; });
                                        if (blokInfoObj != null)
                                        {
                                            blokInfoObj.HandleXML(xmlReader.ReadSubtree(), isBlockInfoNew);
                                            blokInfoObj.objectState = ObjectState.Update;
                                            continue;
                                        }
                                    }
                                }

                                //add
                                blokInfoObj = new cs_blockInfo();
                                blokInfoObj.HandleXML(xmlReader.ReadSubtree(), isBlockInfoNew);
                                blokInfoObj.objectState = ObjectState.New;
                                listBlockInfo.Add(blokInfoObj);
                                break;
                            case "logData":
                                //collection
                                this.HandleLogData(xmlReader.ReadSubtree(), isNew);
                                break;
                            case "depthRegistrationData":
                                if (this.depthRegistrationData == null)
                                    this.depthRegistrationData = new cs_depthRegImage();
                                this.depthRegistrationData.HandleXML(xmlReader.ReadSubtree(), isNew);
                                break;
                            case "commonData":
                                if (this.commonData == null)
                                    this.commonData = new cs_commonData();
                                this.commonData.HandleXML(xmlReader.ReadSubtree(), isNew);
                                break;
                            case "customData":
                                if (this.customData == null)
                                    this.customData = new cs_customData();
                                this.customData.HandleXML(xmlReader.ReadSubtree(), isNew);
                                break;
                            default:
                                break;
                        }
                    }
                }
                //assign list to collection
                this.logParam = listLogParam.ToArray();
                this.logCurveInfo = listWellLogCurveInfo.ToArray();
                this.blockInfo = listBlockInfo.ToArray();
            }
        }
        private void HandleLogData(XmlReader xmlReader, bool isNew)
        {
            using (xmlReader)
            {
                //temporary list for data manipulation
                List<data> listLogData = new List<data>();

                while (xmlReader.Read())
                {
                    //only take child node
                    if (xmlReader.Depth == 1 && xmlReader.IsStartElement() && !xmlReader.IsEmptyElement)
                    {
                        if (xmlReader.Name == "data")
                        {
                            data dataObj = new data();
                            dataObj.HandleXML(xmlReader.ReadSubtree(), isNew);
                            listLogData.Add(dataObj);
                        }
                    }
                }
                //assign list to collection
                this.logData = listLogData.ToArray();
            }
        }

        public override string ValidateGetPath(string newUid)
        {
            // Fix uid if the Object dosn't have one.
            if (!IsValidUid(this.uid))
            {
                this.uid = newUid;
                // TODO: Add some loging that we created a new UID for the object maybe.
            }

            StringBuilder path = new StringBuilder(this.GetParentPath());
            path.Append(@"\");
            path.Append(this.uid);
            return path.ToString();
        }

        public override string GetParentPath()
        {
            if (!IsValidUid(this.uidWell) || !IsValidUid(this.uidWellbore))
            {
                throw new UnableToDecodeException("One or more Parent UID's are invalid", WITSMLReturnCode.WITSMLInvalidParentUID);
                //TODO: Log the issue
            }

            StringBuilder path = new StringBuilder(@"\");
            path.Append(this.uidWell);
            path.Append(@"\");
            path.Append(this.uidWellbore);
            return path.ToString();
        }

        // NOTE: these are done with functions, instead of property access, so as not to mess around with the auto-generated / serialized classes.
        public override string Get_uid()
        {
            return this.uid;
        }

        public override string Get_uidWell()
        {
            return this.uidWell;
        }

        public override string Get_uidWellbore()
        {
            return this.uidWellbore;
        }

        public override string Get_name()
        {
            return this.name;
        }

        public override string Get_nameWell()
        {
            return this.nameWell;
        }

        public override string Get_nameWellbore()
        {
            return this.nameWellbore;
        }

        public override ICommonData Get_commonData()
        {
            if (this.commonData == null)
            {
                this.commonData = new cs_commonData();
            }
            return (ICommonData)this.commonData;
        }
        public override ICustomData Get_customData()
        {
            if (this.customData == null)
            {
                this.customData = new cs_customData();
            }
            return (ICustomData)this.customData;
        }

        /// <summary>
        /// Serialize without LogData
        /// </summary>
        /// <returns>xml string representation of obj_log</returns>
        public override string Serialize()
        {
            //we want to ignore logData on serialize,so we create xmlAttribute that will be ignored
            XmlAttributes xmlAtts = new XmlAttributes();
            xmlAtts.XmlIgnore = true;

            //specify override for logData which accept ignore attribute
            XmlAttributeOverrides xmlAttsOverrides = new XmlAttributeOverrides();
            xmlAttsOverrides.Add(typeof(obj_wellLog), "logData", xmlAtts);

            //do serialize with override parameter
            return this.Serialize(xmlAttsOverrides,SerializerType.WelllogWithoutData);
        }
        /// <summary>
        /// Serialize With Log Data
        /// </summary>
        /// <returns>xml string representation of obj_log</returns>
        public string SerializeWithData()
        {
            return base.Serialize();
        }
        /// <summary>
        /// Serialize without LogData and namespace or declaration
        /// </summary>
        /// <returns>xml string representation of obj_log</returns>
        public override string SerializeClean()
        {
            //we want to ignore logData on serialize,so we create xmlAttribute that will be ignored
            XmlAttributes xmlAtts = new XmlAttributes();
            xmlAtts.XmlIgnore = true;

            //specify override for logData which accept ignore attribute
            XmlAttributeOverrides xmlAttsOverrides = new XmlAttributeOverrides();
            xmlAttsOverrides.Add(typeof(obj_wellLog), "logData", xmlAtts);

            //do serialize with override parameter
            return this.SerializeClean(xmlAttsOverrides,SerializerType.WelllogWithoutData);
        }
    }
}
