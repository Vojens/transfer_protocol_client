﻿ 

using System;
using System.Collections.Generic;
using System.Text;
using System.Xml;

namespace Witsml.Objects140
{
    public partial class checksum : IHandleXML
    {
        public checksum()
        {
            this.type = MessageDigestType.unknown;
        }

        public void HandleXML(XmlReader xmlReader, bool isNew)
        {
            using (xmlReader)
            {
                xmlReader.Read();
                if (xmlReader.HasAttributes)
                {
                    if (xmlReader.MoveToAttribute("type"))
                        this.type = StaticParser.ParseEnumFromString<MessageDigestType>(xmlReader.Value, MessageDigestType.unknown);
                    xmlReader.MoveToElement();
                }
                this.Value = StaticHelper.ReadString(xmlReader);
            }
        }

    }
}
