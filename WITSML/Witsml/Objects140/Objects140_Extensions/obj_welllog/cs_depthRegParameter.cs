﻿ 

using System;
using System.Collections.Generic;
using System.Text;
using System.Xml;

namespace Witsml.Objects140
{
    public partial class cs_depthRegParameter : IHandleXML, IHasID
    {
        //this will need to be ignored
        [System.Xml.Serialization.XmlIgnore]
        public string UID
        {
            get { return this.uid; }
        }

        public void HandleXML(XmlReader xmlReader, bool isNew)
        {
            using (xmlReader)
            {
                xmlReader.MoveToContent();
                if (xmlReader.HasAttributes)
                {
                    if (xmlReader.MoveToAttribute("uid"))
                        StaticHelper.HandleUID(xmlReader.Value, "depthRegCalibrationPoint uid", out this.uid);
                    xmlReader.MoveToElement();
                }
                while (xmlReader.Read())
                {
                    //only take child node
                    if (xmlReader.Depth == 1 && xmlReader.IsStartElement() && !xmlReader.IsEmptyElement)
                    {
                        switch (xmlReader.Name)
                        {
                            case "mnemonic":
                                this.mnemonic = StaticHelper.ReadString(xmlReader);
                                break;
                            case "dictionary":
                                this.dictionary = StaticHelper.ReadString(xmlReader);
                                break;
                            case "topIndex":
                                if (this.topIndex == null)
                                    this.topIndex = new genericMeasure();
                                this.topIndex.HandleXML(xmlReader.ReadSubtree(), isNew);
                                break;
                            case "bottomIndex":
                                if (this.bottomIndex == null)
                                    this.bottomIndex = new genericMeasure();
                                this.bottomIndex.HandleXML(xmlReader.ReadSubtree(), isNew);
                                break;
                            case "value":
                                if (this.value == null)
                                    this.value = new genericMeasure();
                                this.value.HandleXML(xmlReader.ReadSubtree(), isNew);
                                break;
                            case "description":
                                this.description = StaticHelper.ReadString(xmlReader);
                                break;
                            default:
                                break;
                        }
                    }
                }
            }
        }

    }
}
