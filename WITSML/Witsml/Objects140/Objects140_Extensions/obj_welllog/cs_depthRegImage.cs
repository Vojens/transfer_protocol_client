﻿ 

using System;
using System.Collections.Generic;
using System.Text;
using System.Xml;

namespace Witsml.Objects140
{
    public partial class cs_depthRegImage : IHandleXML
    {
        public void HandleXML(XmlReader xmlReader, bool isNew)
        {
            bool isHeaderSectionNew = this.headerSection == null;
            bool isAlternateSectionNew = this.alternateSection == null;
            bool isLogSectionNew = this.logSection == null;

            List<cs_depthRegLogRect> listHeaderSection = isHeaderSectionNew ? new List<cs_depthRegLogRect>():new List<cs_depthRegLogRect>(this.headerSection);
            List<cs_depthRegLogRect> listAlternateSection = isAlternateSectionNew ? new List<cs_depthRegLogRect>() : new List<cs_depthRegLogRect>(this.alternateSection);
            List<cs_depthRegLogSection> listLogSection = isLogSectionNew ? new List<cs_depthRegLogSection>() : new List<cs_depthRegLogSection>(this.logSection);
            using (xmlReader)
            {
                xmlReader.MoveToContent();
                while (xmlReader.Read())
                {
                    //only take child node
                    if (xmlReader.Depth == 1 && xmlReader.IsStartElement() && !xmlReader.IsEmptyElement)
                    {
                        switch (xmlReader.Name)
                        {
                            case "fileName":
                                if (this.fileName == null)
                                    this.fileName = new fileName();
                                this.fileName.HandleXML(xmlReader.ReadSubtree(), isNew);
                                break;
                            case "mimetype":
                                this.mimetype = StaticParser.ParseEnumFromString<MimeType>(StaticHelper.ReadString(xmlReader), MimeType.unknown);
                                break;
                            case "fileSize":
                                this.fileSize = StaticParser.ParseShortFromString(StaticHelper.ReadString(xmlReader));
                                break;
                            case "checksum":
                                if (this.checksum == null)
                                    this.checksum = new checksum();
                                this.checksum.HandleXML(xmlReader.ReadSubtree(), isNew);
                                break;
                            case "imagePixelWidth":
                                this.imagePixelWidth = StaticParser.ParseIntFromString(StaticHelper.ReadString(xmlReader));
                                break;
                            case "imagePixelHeight":
                                this.imagePixelHeight = StaticParser.ParseIntFromString(StaticHelper.ReadString(xmlReader));
                                break;
                            case "imageMargins":
                                if (this.imageMargins == null)
                                    this.imageMargins = new cs_depthRegRectangular();
                                this.imageMargins.HandleXML(xmlReader.ReadSubtree(), isNew);
                                break;
                            case "headerSection":
                                //collection
                                StaticHelper.AddUpdateWithUid<cs_depthRegLogRect>(xmlReader, isHeaderSectionNew, ref listHeaderSection);
                                break;
                            case "alternateSection":
                                //collection
                                StaticHelper.AddUpdateWithUid<cs_depthRegLogRect>(xmlReader, isAlternateSectionNew, ref listAlternateSection);
                                break;
                            case "logSection":
                                //collection
                                StaticHelper.AddUpdateWithUid<cs_depthRegLogSection>(xmlReader, isLogSectionNew, ref listLogSection);
                                break;
                            default:
                                break;
                        }
                    }
                }
                //assign temporary list to collection
                this.headerSection = listHeaderSection.ToArray();
                this.alternateSection = listAlternateSection.ToArray();
                this.logSection = listLogSection.ToArray();


            }
        }

    }
}
