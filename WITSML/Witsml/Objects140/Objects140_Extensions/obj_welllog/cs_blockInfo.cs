﻿ 

using System;
using System.Collections.Generic;
using System.Text;
using System.Xml;
using System.Linq;

namespace Witsml.Objects140
{
    public partial class cs_blockInfo : IHandleXML, IHasID
    {
        //this will need to be ignored
        [System.Xml.Serialization.XmlIgnore]
        public string UID
        {
            get { return this.uid; }
        }
        [System.Xml.Serialization.XmlIgnore]
        public ObjectState objectState;

        public cs_blockInfo()
        {
            this.indexType = LogIndexType.unknown;
            this.direction = LogIndexDirection.unknown;
        }
        public void HandleXML(XmlReader xmlReader, bool isNew)
        {
            //to be used for determining add or update
            bool isLogParamNew = this.logParam == null;
            bool isBlockCurveInfoNew = this.blockCurveInfo == null;

            //create temporary list for data manipulation
            List<indexedObject> listLogParam = isLogParamNew ? new List<indexedObject>() : new List<indexedObject>(this.logParam);
            List<cs_blockCurveInfo> listBlockCurveInfo = isBlockCurveInfoNew ? new List<cs_blockCurveInfo>() : new List<cs_blockCurveInfo>(this.blockCurveInfo);
            using (xmlReader)
            {
                xmlReader.MoveToContent();
                if (xmlReader.HasAttributes)
                {
                    if (xmlReader.MoveToAttribute("uid"))
                        StaticHelper.HandleUID(xmlReader.Value, "blockInfo uid", out this.uid);
                    xmlReader.MoveToElement();
                }
                while (xmlReader.Read())
                {
                    //only take child node
                    if (xmlReader.Depth == 1 && xmlReader.IsStartElement() && !xmlReader.IsEmptyElement)
                    {
                        switch (xmlReader.Name)
                        {
                            case "dataRowCount":
                                this.dataRowCount = StaticParser.ParseIntFromString(StaticHelper.ReadString(xmlReader));
                                this.dataRowCountSpecified = true;
                                break;
                            case "pass":
                                this.pass = StaticHelper.ReadString(xmlReader);
                                break;
                            case "description":
                                this.description = StaticHelper.ReadString(xmlReader);
                                break;
                            case "indexType":
                                this.indexType = StaticParser.ParseEnumFromString<LogIndexType>(StaticHelper.ReadString(xmlReader), LogIndexType.unknown);
                                break;
                            case "startIndex":
                                if (this.startIndex == null)
                                    this.startIndex = new genericMeasure();
                                this.startIndex.HandleXML(xmlReader.ReadSubtree(), isNew);
                                break;
                            case "endIndex":
                                if (this.endIndex == null)
                                    this.endIndex = new genericMeasure();
                                this.endIndex.HandleXML(xmlReader.ReadSubtree(), isNew);
                                break;
                            case "stepIncrement":
                                if (this.stepIncrement == null)
                                    this.stepIncrement = new ratioGenericMeasure();
                                this.stepIncrement.HandleXML(xmlReader.ReadSubtree(), isNew);
                                break;
                            case "startDateTimeIndex":
                                this.startDateTimeIndex = StaticParser.ParseDateTimeFromString(StaticHelper.ReadString(xmlReader));
                                this.startDateTimeIndexSpecified = true;
                                break;
                            case "endDateTimeIndex":
                                this.endDateTimeIndex = StaticParser.ParseDateTimeFromString(StaticHelper.ReadString(xmlReader));
                                this.endDateTimeIndexSpecified = true;
                                break;
                            case "direction":
                                StaticParser.SetEnumFromString<LogIndexDirection>(StaticHelper.ReadString(xmlReader), out this.direction, out this.directionSpecified);
                                break;
                            case "indexCurve":
                                if (this.indexCurve == null)
                                    this.indexCurve = new indexCurve();
                                this.indexCurve.HandleXML(xmlReader.ReadSubtree(), isNew);
                                break;
                            case "nullValue":
                                this.nullValue = StaticHelper.ReadString(xmlReader);
                                break;
                            case "logParam":
                                //collection
                                //StaticHelper.AddUpdateIndexedObj(xmlReader, isLogParamNew, ref listLogParam);
                                break;
                            case "blockCurveInfo":
                                //collection
                                //collection
                                cs_blockCurveInfo blockCurveInfoObj;
                                string currentBlockCurveInfoID = string.Empty;

                                if (xmlReader.HasAttributes)
                                {
                                    //check for uid attribute and assign it to temporary string for searching in collection
                                    if (xmlReader.MoveToAttribute("uid"))
                                    {
                                        currentBlockCurveInfoID = xmlReader.Value;
                                        xmlReader.MoveToElement();
                                    }
                                }

                                //check whether uid is empty
                                if (!string.IsNullOrEmpty(currentBlockCurveInfoID))
                                {
                                    //update
                                    if (!isBlockCurveInfoNew)
                                    {
                                        blockCurveInfoObj = listBlockCurveInfo.FirstOrDefault(
                                            delegate(cs_blockCurveInfo objBlockCurveInfo)
                                            { if (objBlockCurveInfo.UID == currentBlockCurveInfoID)return true; else return false; });
                                        if (blockCurveInfoObj != null)
                                        {
                                            blockCurveInfoObj.HandleXML(xmlReader.ReadSubtree(), isBlockCurveInfoNew);
                                            blockCurveInfoObj.objectState = ObjectState.Update;
                                            continue;
                                        }
                                    }
                                }

                                //add
                                blockCurveInfoObj = new cs_blockCurveInfo();
                                blockCurveInfoObj.HandleXML(xmlReader.ReadSubtree(), isBlockCurveInfoNew);
                                blockCurveInfoObj.objectState = ObjectState.New;
                                listBlockCurveInfo.Add(blockCurveInfoObj);
                                break;
                            default:
                                break;
                        }
                    }
                }
                //assign to list
                this.logParam = listLogParam.ToArray();
                this.blockCurveInfo = listBlockCurveInfo.ToArray();
            }
        }

    }
}
