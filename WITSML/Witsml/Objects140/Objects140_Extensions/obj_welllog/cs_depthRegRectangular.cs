﻿ 

using System;
using System.Collections.Generic;
using System.Text;
using System.Xml;

namespace Witsml.Objects140
{
    public partial class cs_depthRegRectangular : IHandleXML, IHasID
    {
        //this will need to be ignored
        [System.Xml.Serialization.XmlIgnore]
        public string UID
        {
            get { return this.uid; }
        }

        public void HandleXML(XmlReader xmlReader, bool isNew)
        {
            using (xmlReader)
            {
                xmlReader.MoveToContent();
                if (xmlReader.HasAttributes)
                {
                    if (xmlReader.MoveToAttribute("uid"))
                        StaticHelper.HandleUID(xmlReader.Value, "depthRegRectangular uid", out this.uid);
                    xmlReader.MoveToElement();
                }
                while (xmlReader.Read())
                {
                    //only take child node
                    if (xmlReader.Depth == 1 && xmlReader.IsStartElement() && !xmlReader.IsEmptyElement)
                    {
                        switch (xmlReader.Name)
                        {
                            case "ul":
                                if (this.ul == null)
                                    this.ul = new cs_depthRegPoint();
                                this.ul.HandleXML(xmlReader.ReadSubtree(), isNew);
                                break;
                            case "ur":
                                if (this.ur == null)
                                    this.ur = new cs_depthRegPoint();
                                this.ur.HandleXML(xmlReader.ReadSubtree(), isNew);
                                break;
                            case "ll":
                                if (this.ll == null)
                                    this.ll = new cs_depthRegPoint();
                                this.ll.HandleXML(xmlReader.ReadSubtree(), isNew);
                                break;
                            case "lr":
                                if (this.lr == null)
                                    this.lr = new cs_depthRegPoint();
                                this.lr.HandleXML(xmlReader.ReadSubtree(), isNew);
                                break;
                            default:
                                break;
                        }
                    }
                }
            }
        }

    }
}
