﻿ 

using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Xml;

namespace Witsml.Objects140
{
    public partial class timestampedTimeZone : IHandleXML
    {
        /// <summary>
        /// Convert XML string data into datetime with Timezone fields value
        /// </summary>
        /// <param name="xmlReader">reader that has CommonData node</param>
        /// <param name="IsNew">Is the object from stratch(true) or from db/serialization(false)</param>
        public void HandleXML(XmlReader xmlReader, bool isNew)
        {
            using (xmlReader)
            {
                xmlReader.Read();
                if (xmlReader.HasAttributes)
                {
                    if (xmlReader.MoveToAttribute("dTim"))
                    {
                        this.dTim = StaticParser.ParseDateTimeFromString(xmlReader.Value);
                        this.dTimSpecified = true;
                    }
                    xmlReader.MoveToElement();
                }
                this.Value = StaticHelper.ReadString(xmlReader);
            }
        }

    }
}
