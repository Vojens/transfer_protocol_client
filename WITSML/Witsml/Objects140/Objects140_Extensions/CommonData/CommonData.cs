﻿ 

using System;
using System.Collections.Generic;
using System.Text;
using System.Xml;

namespace Witsml.Objects140
{
    public partial class cs_commonData : IHandleXML, ICommonData
    {
        //interface members
        [System.Xml.Serialization.XmlIgnore]
        public DateTime DTimCreation
        {
            set { this.dTimCreation = value; }
        }

        [System.Xml.Serialization.XmlIgnore]
        public bool DTimCreationSpecified
        {
            set { this.dTimCreationSpecified = value; }
        }

        [System.Xml.Serialization.XmlIgnore]
        public DateTime DTimLastChange
        {
            set { this.dTimLastChange = value; }
        }

        [System.Xml.Serialization.XmlIgnore]
        public bool DTimLastChangeSpecified
        {
            set { this.dTimLastChangeSpecified = value; }
        }

        //constructor
        public cs_commonData()
        {
            this.itemState = ItemState.unknown;
            this.serviceCategory = ServiceCategory.unknown;
        }

        /// <summary>
        /// Convert XML string data into CommonData fields value
        /// </summary>
        /// <param name="xmlReader">reader that has CommonData node</param>
        /// <param name="IsNew">Is the object from stratch(true) or from db/serialization(false)</param>
        public void HandleXML(XmlReader xmlReader, bool isNew)
        {
            //check whether collection is new
            bool isAcquisitionTimeZoneNew = this.acquisitionTimeZone == null;
            //used for temporary processing
            List<timestampedTimeZone> listTime = isAcquisitionTimeZoneNew ? new List<timestampedTimeZone>() : new List<timestampedTimeZone>(this.acquisitionTimeZone);
            using (xmlReader)
            {
                while (xmlReader.Read())
                {
                    //start on depth 1 which are the childs that we want to check against
                    //check for starting tag and convert node value to its expected variable
                    if (xmlReader.Depth == 1 && xmlReader.IsStartElement() && !xmlReader.IsEmptyElement)
                    {
                        switch (xmlReader.Name)
                        {
                            case "sourceName":
                                this.sourceName = StaticHelper.ReadString(xmlReader);
                                break;
                            case "dTimCreation":
                                this.dTimCreation = StaticParser.ParseDateTimeFromString(StaticHelper.ReadString(xmlReader));
                                this.dTimCreationSpecified = true;
                                break;
                            case "dTimLastChange":
                                this.dTimLastChange = StaticParser.ParseDateTimeFromString(StaticHelper.ReadString(xmlReader));
                                this.dTimLastChangeSpecified = true;
                                break;
                            case "itemState":
                                StaticParser.SetEnumFromString<ItemState>(StaticHelper.ReadString(xmlReader), out this.itemState, out this.itemStateSpecified);
                                break;
                            case "serviceCategory":
                                StaticParser.SetEnumFromString<ServiceCategory>(StaticHelper.ReadString(xmlReader), out this.serviceCategory, out this.serviceCategorySpecified);
                                break;
                            case "comments":
                                this.comments = StaticHelper.ReadString(xmlReader);
                                break;
                            case "acquisitionTimeZone":
                                //collection
                                timestampedTimeZone timeStampedObj = new timestampedTimeZone();
                                timeStampedObj.HandleXML(xmlReader.ReadSubtree(), isAcquisitionTimeZoneNew);
                                listTime.Add(timeStampedObj);
                                break;
                            default:
                                break;
                        }
                    }
                }
                //assign list back to collection
                this.acquisitionTimeZone = listTime.ToArray();
            }
        }

    }
}
