﻿ 

using System;
using System.Collections.Generic;
using System.Text;
using System.Xml;

namespace Witsml.Objects140
{
    public partial class cs_stnTrajCorUsed : IHandleXML
    {
        /// <summary>
        /// Convert XML string data into cs_stnTrajCorUsed fields value
        /// </summary>
        /// <param name="xmlReader">reader that has cs_stnTrajCorUsed node</param>
        /// <param name="IsNew">Is the object from stratch(true) or from db/serialization(false)</param>
        public void HandleXML(XmlReader xmlReader, bool isNew)
        {
            using (xmlReader)
            {
                while (xmlReader.Read())
                {
                    //start on depth 1 which are the childs that we want to check against
                    //check for starting tag and convert node value to its expected variable
                    if (xmlReader.Depth == 1 && xmlReader.IsStartElement() && !xmlReader.IsEmptyElement)
                    {
                        switch (xmlReader.Name)
                        {
                            case "gravAxialAccelCor":
                                if (this.gravAxialAccelCor == null)
                                    this.gravAxialAccelCor = new accelerationLinearMeasure();
                                this.gravAxialAccelCor.HandleXML(xmlReader.ReadSubtree(), isNew);
                                break;
                            case "gravTran1AccelCor":
                                if (this.gravTran1AccelCor == null)
                                    this.gravTran1AccelCor = new accelerationLinearMeasure();
                                this.gravTran1AccelCor.HandleXML(xmlReader.ReadSubtree(), isNew);
                                break;
                            case "gravTran2AccelCor":
                                if (this.gravTran2AccelCor == null)
                                    this.gravTran2AccelCor = new accelerationLinearMeasure();
                                this.gravTran2AccelCor.HandleXML(xmlReader.ReadSubtree(), isNew);
                                break;
                            case "magAxialDrlstrCor":
                                if (this.magAxialDrlstrCor == null)
                                    this.magAxialDrlstrCor = new magneticInductionMeasure();
                                this.magAxialDrlstrCor.HandleXML(xmlReader.ReadSubtree(), isNew);
                                break;
                            case "magTran1DrlstrCor":
                                if (this.magTran1DrlstrCor == null)
                                    this.magTran1DrlstrCor = new magneticInductionMeasure();
                                this.magTran1DrlstrCor.HandleXML(xmlReader.ReadSubtree(), isNew);
                                break;
                            case "magTran2DrlstrCor":
                                if (this.magTran2DrlstrCor == null)
                                    this.magTran2DrlstrCor = new magneticInductionMeasure();
                                this.magTran2DrlstrCor.HandleXML(xmlReader.ReadSubtree(), isNew);
                                break;
                            case "sagIncCor":
                                if (this.sagIncCor == null)
                                    this.sagIncCor = new planeAngleMeasure();
                                this.sagIncCor.HandleXML(xmlReader.ReadSubtree(), isNew);
                                break;
                            case "sagAziCor":
                                if (this.sagAziCor == null)
                                    this.sagAziCor = new planeAngleMeasure();
                                this.sagAziCor.HandleXML(xmlReader.ReadSubtree(), isNew);
                                break;
                            case "stnMagDeclUsed":
                                if (this.stnMagDeclUsed == null)
                                    this.stnMagDeclUsed = new planeAngleMeasure();
                                this.stnMagDeclUsed.HandleXML(xmlReader.ReadSubtree(), isNew);
                                break;
                            case "stnGridCorUsed":
                                if (this.stnGridCorUsed == null)
                                    this.stnGridCorUsed = new planeAngleMeasure();
                                this.stnGridCorUsed.HandleXML(xmlReader.ReadSubtree(), isNew);
                                break;
                            case "dirSensorOffset":
                                if (this.dirSensorOffset == null)
                                    this.dirSensorOffset = new lengthMeasure();
                                this.dirSensorOffset.HandleXML(xmlReader.ReadSubtree(), isNew);
                                break;
                            default:
                                break;
                        }
                    }
                }
            }
        }
    }
}
