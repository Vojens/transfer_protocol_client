﻿ 

using System;
using System.Collections.Generic;
using System.IO;
using System.Text;
using System.Xml;
using System.Xml.Serialization;
using System.Linq;
using Witsml.Common.Exceptions;
using Witsml.Exceptions;


namespace Witsml.Objects140
{
    //objectGroing changed to enum
    [XmlRoot("trajectory", Namespace = "http://www.witsml.org/schemas/140", IsNullable = false)]
    public partial class obj_trajectory : WITSMLObject, IHandleXML
    {
        public obj_trajectory()
        {
            this.objectGrowing = ObjectGrowingState.unknown;
            this.aziRef = AziRef.unknown;
            this.version = WITSMLVersion.V140;

        }

        //Decode string to fill this object
        public override void Decode(ref string xmlIn)
        {
            //used to determine add or update
            try
            {
                //Consume XML string and convert node value to variable based on node name
                using (StringReader stringReader = new StringReader(xmlIn))
                {
                    XmlReaderSettings setting = new XmlReaderSettings();
                    setting.ConformanceLevel = ConformanceLevel.Document;
                    setting.IgnoreComments = true;
                    setting.IgnoreWhitespace = true;
                    setting.IgnoreProcessingInstructions = true;
                    using (XmlReader xmlReader = XmlReader.Create(stringReader, setting))
                    {
                        //determine add or update operation
                        bool isNew;
                        if (this.uid == string.Empty)
                            isNew = true;
                        else isNew = false;

                        this.HandleXML(xmlReader, isNew);
                    }
                }
            }
            catch (XmlException ex)
            {
                throw new Exceptions.UnableToDecodeException(ex.Message, WITSMLReturnCode.WITSMLDocumentError, ex.InnerException);
            }
            catch (NullReferenceException ex)
            {
                throw new Exceptions.UnableToDecodeException(ex.Message, WITSMLReturnCode.WITSMLEmptyDocument, ex.InnerException);
            }
            catch (Exceptions.UnableToDecodeException ex)
            {
                throw ex;
            }
            catch (Exception ex)
            {
                throw new Exceptions.UnableToDecodeException(ex.Message, WITSMLReturnCode.WITSMLUnkownCause, ex.InnerException);
            }
        }

        /// <summary>
        /// Convert XML string data into log fields value
        /// </summary>
        /// <param name="xmlReader">reader that has log node</param>
        /// <param name="IsNew">Is the object from stratch(true) or from db/serialization(false)</param>
        public void HandleXML(XmlReader xmlReader, bool isNew)
        {
            //temporary list
            List<cs_trajectoryStation> listTrajectoryStation = this.trajectoryStation == null ? new List<cs_trajectoryStation>() : new List<cs_trajectoryStation>(this.trajectoryStation);
            using (xmlReader)
            {
                //assign uid for the new well
                xmlReader.MoveToContent();
                if (xmlReader.HasAttributes)
                {
                    if (xmlReader.MoveToAttribute("uidWell"))
                        StaticHelper.HandleUID(xmlReader.Value, "trajectory uidWell", out this.uidWell);
                    if (xmlReader.MoveToAttribute("uidWellbore"))
                        StaticHelper.HandleUID(xmlReader.Value, "trajectory uidWellbore", out this.uidWellbore);
                    if (xmlReader.MoveToAttribute("uid"))
                        StaticHelper.HandleUID(xmlReader.Value, "trajectory uid", out this.uid);
                    xmlReader.MoveToElement();
                }
                while (xmlReader.Read())
                {
                    //only take child node
                    if (xmlReader.Depth == 1 && xmlReader.IsStartElement() && !xmlReader.IsEmptyElement)
                    {
                        switch (xmlReader.Name)
                        {
                            case "nameWell":
                                this.nameWell = StaticHelper.ReadString(xmlReader);
                                break;
                            case "nameWellbore":
                                this.nameWellbore = StaticHelper.ReadString(xmlReader);
                                break;
                            case "name":
                                this.name = StaticHelper.ReadString(xmlReader);
                                break;
                            case "objectGrowing":
                                StaticParser.SetEnumFromString<ObjectGrowingState>(StaticHelper.ReadString(xmlReader), out this.objectGrowing, out this.objectGrowingSpecified);
                                break;
                            case "parentTrajectory":
                                if (this.parentTrajectory == null)
                                    this.parentTrajectory = new cs_refWellboreTrajectory();
                                this.parentTrajectory.HandleXML(xmlReader.ReadSubtree(), isNew);
                                break;
                            case "dTimTrajStart":
                                this.dTimTrajStart = StaticParser.ParseDateTimeFromString(StaticHelper.ReadString(xmlReader));
                                this.dTimTrajStartSpecified = true;
                                break;
                            case "dTimTrajEnd":
                                this.dTimTrajEnd = StaticParser.ParseDateTimeFromString(StaticHelper.ReadString(xmlReader));
                                this.dTimTrajEndSpecified = true;
                                break;
                            case "mdMn":
                                if (this.mdMn == null)
                                    this.mdMn = new measuredDepthCoord();
                                this.mdMn.HandleXML(xmlReader.ReadSubtree(), isNew);
                                break;
                            case "mdMx":
                                if (this.mdMx == null)
                                    this.mdMx = new measuredDepthCoord();
                                this.mdMx.HandleXML(xmlReader.ReadSubtree(), isNew);
                                break;
                            case "serviceCompany":
                                this.serviceCompany = StaticHelper.ReadString(xmlReader);
                                break;
                            case "magDeclUsed":
                                if (this.magDeclUsed == null)
                                    this.magDeclUsed = new planeAngleMeasure();
                                this.magDeclUsed.HandleXML(xmlReader.ReadSubtree(), isNew);
                                break;
                            case "gridCorUsed":
                                if (this.gridCorUsed == null)
                                    this.gridCorUsed = new planeAngleMeasure();
                                this.gridCorUsed.HandleXML(xmlReader.ReadSubtree(), isNew);
                                break;
                            case "aziVertSect":
                                if (this.aziVertSect == null)
                                    this.aziVertSect = new planeAngleMeasure();
                                this.aziVertSect.HandleXML(xmlReader.ReadSubtree(), isNew);
                                break;
                            case "dispNsVertSectOrig":
                                if (this.dispNsVertSectOrig == null)
                                    this.dispNsVertSectOrig = new lengthMeasure();
                                this.dispNsVertSectOrig.HandleXML(xmlReader.ReadSubtree(), isNew);
                                break;
                            case "dispEwVertSectOrig":
                                if (this.dispEwVertSectOrig == null)
                                    this.dispEwVertSectOrig = new lengthMeasure();
                                this.dispEwVertSectOrig.HandleXML(xmlReader.ReadSubtree(), isNew);
                                break;
                            case "definitive":
                                this.definitive = StaticParser.ParseBooleanFromString(StaticHelper.ReadString(xmlReader));
                                this.definitiveSpecified = true;
                                break;
                            case "memory":
                                this.memory = StaticParser.ParseBooleanFromString(StaticHelper.ReadString(xmlReader));
                                this.memorySpecified = true;
                                break;
                            case "finalTraj":
                                this.finalTraj = StaticParser.ParseBooleanFromString(StaticHelper.ReadString(xmlReader));
                                this.finalTrajSpecified = true;
                                break;
                            case "aziRef":
                                string value = StaticHelper.ReadString(xmlReader);
                                this.aziRef = StaticParser.ParseEnumUomFromString<AziRef>(value);
                                this.aziRefSpecified = true;
                                break;
                            case "trajectoryStation":
                                bool isUidEmpty = false;
                                //multiple recurring object
                                //do checking to database
                                cs_trajectoryStation trajectoryStationObj = null;
                                string currentTrajectoryStationUID = string.Empty;

                                if (xmlReader.HasAttributes)
                                {
                                    //check for uid attribute and assign it to temporary string for searching in collection
                                    if (xmlReader.MoveToAttribute("uid"))
                                    {
                                        currentTrajectoryStationUID = xmlReader.Value;
                                        xmlReader.MoveToElement();
                                    }
                                }

                                //handling for recursive object with no uid
                                if (String.IsNullOrEmpty(currentTrajectoryStationUID))
                                {
                                    //generate uid for it
                                    currentTrajectoryStationUID = StaticHelper.GenerateObjectUID();
                                    isUidEmpty = true;
                                }
                                else
                                {
                                    //check whether the object exist on database and get it from db(this method is overriden on store)
                                    trajectoryStationObj = this.CheckForExistence<cs_trajectoryStation>(currentTrajectoryStationUID, null);
                                }

                                bool isTrajectoryStationNew;
                                //assign operationtype to cs_trajectory_station
                                if (trajectoryStationObj == null)
                                {
                                    //new
                                    isTrajectoryStationNew = true;

                                    //check whether object with same uid already exists on collection,if exist,throw exception
                                    cs_trajectoryStation trajStationObjCheck = listTrajectoryStation.FirstOrDefault(
                                       delegate(cs_trajectoryStation objTrajStation)
                                       { if (objTrajStation.uid == currentTrajectoryStationUID)return true; else return false; });
                                    if (trajStationObjCheck != null)
                                    {
                                        throw new Exceptions.UnableToDecodeException("Trajectory Station with same uid value of " + currentTrajectoryStationUID + " already exists on the xml file", WITSMLReturnCode.WITSMLObjectExistOnXMLFile,true);
                                    }

                                    trajectoryStationObj = new cs_trajectoryStation();
                                    trajectoryStationObj.operationType = OperationType.Add;
                                }
                                else
                                {
                                    //update
                                    isTrajectoryStationNew = false;
                                    trajectoryStationObj.operationType = OperationType.Update;
                                }
                                //Fill/Update the object on memory
                                trajectoryStationObj.HandleXML(xmlReader.ReadSubtree(), isTrajectoryStationNew);
                                //fill uid if it is empty
                                if (isUidEmpty)
                                    trajectoryStationObj.uid = currentTrajectoryStationUID;
                                //Add to list for common handle
                                listTrajectoryStation.Add(trajectoryStationObj);
                                break;
                            case "commonData":
                                if (this.commonData == null)
                                    this.commonData = new cs_commonData();
                                this.commonData.HandleXML(xmlReader.ReadSubtree(), isNew);
                                break;
                            case "customData":
                                if (this.customData == null)
                                    this.customData = new cs_customData();
                                this.customData.HandleXML(xmlReader.ReadSubtree(), isNew);
                                break;
                            default:
                                break;
                        }
                    }
                }
            }
            //assign list to collection
            this.trajectoryStation = listTrajectoryStation.ToArray();
        }

        public override string Serialize()
        {
            //we want to ignore trajectoryStation on serialize,so we create xmlAttribute that will be ignored
            XmlAttributes xmlAtts = new XmlAttributes();
            xmlAtts.XmlIgnore = true;

            //specify override for trajectoryStation which accept ignore attribute
            XmlAttributeOverrides xmlAttsOverrides = new XmlAttributeOverrides();
            xmlAttsOverrides.Add(typeof(obj_trajectory), "trajectoryStation", xmlAtts);

            //do serialize with override parameter
            return this.Serialize(xmlAttsOverrides,SerializerType.TrajectoryWithoutStation);
        }

        /// <summary>
        /// Serialize Trajectory include with Trajectory Station.XML Declaration and prefix not included
        /// </summary>
        /// <returns>serialize result without xml declaration</returns>
        public string SerializeWithTrajectoryStation()
        {
            //we want to ignore trajectoryStation on serialize,so we create xmlAttribute that will be ignored
            XmlAttributes xmlAtts = new XmlAttributes();
            xmlAtts.XmlIgnore = false;

            //specify override for trajectoryStation which accept ignore attribute
            XmlAttributeOverrides xmlAttsOverrides = new XmlAttributeOverrides();
            xmlAttsOverrides.Add(typeof(obj_trajectory), "trajectoryStation", xmlAtts);

            //do serialize with override parameter
            return this.Serialize(xmlAttsOverrides,SerializerType.TrajectoryWithoutStation);
        }

        public override string ValidateGetPath(string newUid)
        {
            // Fix uid if the Object dosn't have one.
            if (!IsValidUid(this.uid))
            {
                this.uid = newUid;
                // TODO: Add some loging that we created a new UID for the object maybe.
            }

            StringBuilder path = new StringBuilder(this.GetParentPath());
            path.Append(@"\");
            path.Append(this.uid);
            return path.ToString();
        }

        public override string GetParentPath()
        {
            if (!IsValidUid(this.uidWell) || !IsValidUid(this.uidWellbore))
            {
                throw new UnableToDecodeException("One or more Parent UID's are invalid", WITSMLReturnCode.WITSMLInvalidParentUID);
                //TODO: Log the issue
            }

            StringBuilder path = new StringBuilder(@"\");
            path.Append(this.uidWell);
            path.Append(@"\");
            path.Append(this.uidWellbore);
            return path.ToString();
        }

        // NOTE: these are done with functions, instead of property access, so as not to mess around with the auto-generated / serialized classes.
        public override string Get_uid()
        {
            return this.uid;
        }

        public override string Get_uidWell()
        {
            return this.uidWell;
        }

        public override string Get_uidWellbore()
        {
            return this.uidWellbore;
        }

        public override string Get_name()
        {
            return this.name;
        }

        public override string Get_nameWell()
        {
            return this.nameWell;
        }

        public override string Get_nameWellbore()
        {
            return this.nameWellbore;
        }

        public override ICommonData Get_commonData()
        {
            if (this.commonData == null)
            {
                this.commonData = new cs_commonData();
            }
            return (ICommonData)this.commonData;
        }
        public override ICustomData Get_customData()
        {
            if (this.customData == null)
            {
                this.customData = new cs_customData();
            }
            return (ICustomData)this.customData;
        }
    }
}
