﻿ 

using System;
using System.Collections.Generic;
using System.Text;
using System.Xml;

namespace Witsml.Objects140
{
    public partial class cs_stnTrajRawData : IHandleXML
    {
        /// <summary>
        /// Convert XML string data into cs_stnTrajRawData fields value
        /// </summary>
        /// <param name="xmlReader">reader that has cs_stnTrajRawData node</param>
        /// <param name="IsNew">Is the object from stratch(true) or from db/serialization(false)</param>
        public void HandleXML(XmlReader xmlReader, bool isNew)
        {
            using (xmlReader)
            {
                while (xmlReader.Read())
                {
                    //start on depth 1 which are the childs that we want to check against
                    //check for starting tag and convert node value to its expected variable
                    if (xmlReader.Depth == 1 && xmlReader.IsStartElement() && !xmlReader.IsEmptyElement)
                    {
                        switch (xmlReader.Name)
                        {
                            case "gravAxialRaw":
                                if (this.gravAxialRaw == null)
                                    this.gravAxialRaw = new accelerationLinearMeasure();
                                this.gravAxialRaw.HandleXML(xmlReader.ReadSubtree(), isNew);
                                break;
                            case "gravTran1Raw":
                                if (this.gravTran1Raw == null)
                                    this.gravTran1Raw = new accelerationLinearMeasure();
                                this.gravTran1Raw.HandleXML(xmlReader.ReadSubtree(), isNew);
                                break;
                            case "gravTran2Raw":
                                if (this.gravTran2Raw == null)
                                    this.gravTran2Raw = new accelerationLinearMeasure();
                                this.gravTran2Raw.HandleXML(xmlReader.ReadSubtree(), isNew);
                                break;
                            case "magAxialRaw":
                                if (this.magAxialRaw == null)
                                    this.magAxialRaw = new magneticInductionMeasure();
                                this.magAxialRaw.HandleXML(xmlReader.ReadSubtree(), isNew);
                                break;
                            case "magTran1Raw":
                                if (this.magTran1Raw == null)
                                    this.magTran1Raw = new magneticInductionMeasure();
                                this.magTran1Raw.HandleXML(xmlReader.ReadSubtree(), isNew);
                                break;
                            case "magTran2Raw":
                                if (this.magTran2Raw == null)
                                    this.magTran2Raw = new magneticInductionMeasure();
                                this.magTran2Raw.HandleXML(xmlReader.ReadSubtree(), isNew);
                                break;
                            default:
                                break;
                        }
                    }
                }
            }
        }
    }
}
