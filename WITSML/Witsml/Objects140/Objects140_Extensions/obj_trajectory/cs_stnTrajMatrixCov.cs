﻿ 

using System;
using System.Collections.Generic;
using System.Text;
using System.Xml;

namespace Witsml.Objects140
{
    public partial class cs_stnTrajMatrixCov : IHandleXML
    {
        /// <summary>
        /// Convert XML string data into cs_stnTrajCorUsed fields value
        /// </summary>
        /// <param name="xmlReader">reader that has cs_stnTrajCorUsed node</param>
        /// <param name="IsNew">Is the object from stratch(true) or from db/serialization(false)</param>
        public void HandleXML(XmlReader xmlReader, bool isNew)
        {
            using (xmlReader)
            {
                while (xmlReader.Read())
                {
                    //start on depth 1 which are the childs that we want to check against
                    //check for starting tag and convert node value to its expected variable
                    if (xmlReader.Depth == 1 && xmlReader.IsStartElement() && !xmlReader.IsEmptyElement)
                    {
                        switch (xmlReader.Name)
                        {
                            case "varianceNN":
                                if (this.varianceNN == null)
                                    this.varianceNN = new areaMeasure();
                                this.varianceNN.HandleXML(xmlReader.ReadSubtree(), isNew);
                                break;
                            case "varianceNE":
                                if (this.varianceNE == null)
                                    this.varianceNE = new areaMeasure();
                                this.varianceNE.HandleXML(xmlReader.ReadSubtree(), isNew);
                                break;
                            case "varianceNVert":
                                if (this.varianceNVert == null)
                                    this.varianceNVert = new areaMeasure();
                                this.varianceNVert.HandleXML(xmlReader.ReadSubtree(), isNew);
                                break;
                            case "varianceEE":
                                if (this.varianceEE == null)
                                    this.varianceEE = new areaMeasure();
                                this.varianceEE.HandleXML(xmlReader.ReadSubtree(), isNew);
                                break;
                            case "varianceEVert":
                                if (this.varianceEVert == null)
                                    this.varianceEVert = new areaMeasure();
                                this.varianceEVert.HandleXML(xmlReader.ReadSubtree(), isNew);
                                break;
                            case "varianceVertVert":
                                if (this.varianceVertVert == null)
                                    this.varianceVertVert = new areaMeasure();
                                this.varianceVertVert.HandleXML(xmlReader.ReadSubtree(), isNew);
                                break;
                            case "biasN":
                                if (this.biasN == null)
                                    this.biasN = new lengthMeasure();
                                this.biasN.HandleXML(xmlReader.ReadSubtree(), isNew);
                                break;
                            case "biasE":
                                if (this.biasE == null)
                                    this.biasE = new lengthMeasure();
                                this.biasE.HandleXML(xmlReader.ReadSubtree(), isNew);
                                break;
                            case "biasVert":
                                if (this.biasVert == null)
                                    this.biasVert = new lengthMeasure();
                                this.biasVert.HandleXML(xmlReader.ReadSubtree(), isNew);
                                break;
                            default:
                                break;
                        }
                    }
                }
            }
        }
    }
}
