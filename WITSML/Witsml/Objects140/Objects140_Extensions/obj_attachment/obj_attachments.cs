﻿ 

using System;
using System.Collections.Generic;
using System.IO;
using System.Text;
using System.Xml;
using System.Xml.Serialization;
using System.Linq;
using Witsml.Common.Exceptions;


namespace Witsml.Objects140
{
    //Extension class for wells document that inherit from WITSMLDocument and Implement Decode Function
    public partial class obj_attachments : WITSMLDocument
    {

        public override WITSMLObject GetSingleObject(int Item)
        {
            return this.attachment[Item];
        }

        /// <summary>
        /// Deserialize XML string into WitsmlDocument object
        /// </summary>
        /// <param name="XMLin">XML string to be serialized</param>
        /// <returns>Number of singular attachment inside</returns>
        public override int Decode(ref string xmlIn)
        {
            //return value
            //number of attachment inside xmlIn
            int countOfAttachment = 0;
            try
            {
                //check whether collection is from stratch(isNew = true) or from deserialization(isNew=false)
                bool isNew = this.attachment == null;

                //create temporary list to help with data manipulation
                List<obj_attachment> listAttachment = isNew ? new List<obj_attachment>() : new List<obj_attachment>(this.attachment);

                //Consume XML string and convert node value to variable based on node name
                using (StringReader stringReader = new StringReader(xmlIn))
                {
                    XmlReaderSettings setting = new XmlReaderSettings();
                    setting.ConformanceLevel = ConformanceLevel.Document;
                    setting.IgnoreComments = true;
                    setting.IgnoreWhitespace = true;
                    setting.IgnoreProcessingInstructions = true;
                    using (XmlReader xmlReader = XmlReader.Create(stringReader, setting))
                    {
                        while (xmlReader.Read())
                        {
                            //assign version
                            xmlReader.MoveToContent();
                            if (xmlReader.MoveToAttribute("version"))
                                this.version = xmlReader.Value;
                            else
                                this.version = "1.4.0";//no version specified,set it to 1.4.0

                            //pre-checking on whether node is the correct child based on depth,start tag and not empty
                            if (xmlReader.Depth == 1 && xmlReader.IsStartElement() && !xmlReader.IsEmptyElement)
                            {
                                switch (xmlReader.Name)
                                {
                                    case "documentInfo":
                                        if (this.documentInfo == null)
                                            this.documentInfo = new cs_documentInfo();
                                        this.documentInfo.HandleXML(xmlReader.ReadSubtree(), isNew);
                                        break;
                                    case "attachment":
                                        obj_attachment attachment;
                                        //foreign key(well id,wellbore id) and primary key(wellbore id) to identify existing attachment
                                        string currentUIDWell = string.Empty;
                                        string currentUIDWellbore = string.Empty;
                                        string currentUID = string.Empty;

                                        //Update attachment
                                        if (!isNew)
                                        {
                                            //check for uid attribute and assign it to temporary string for searching in collection
                                            if (xmlReader.HasAttributes)
                                            {
                                                if (xmlReader.MoveToAttribute("uidWell"))
                                                {
                                                    currentUIDWell = xmlReader.Value;
                                                }
                                                if (xmlReader.MoveToAttribute("uidWellbore"))
                                                {
                                                    currentUIDWellbore = xmlReader.Value;
                                                }
                                                if (xmlReader.MoveToAttribute("uid"))
                                                {
                                                    currentUID = xmlReader.Value;
                                                }
                                                xmlReader.MoveToElement();
                                            }
                                            //look for attachment based on its UID and update the attachment with xml string
                                            if (currentUID != string.Empty)
                                            {
                                                attachment = listAttachment.FirstOrDefault(
                                                    delegate(obj_attachment objattachment)
                                                    { if (objattachment.uidWell == currentUIDWell && objattachment.uidWellbore == currentUIDWellbore && objattachment.uid == currentUID)return true; else return false; });
                                                if (attachment != null)
                                                {
                                                    attachment.HandleXML(xmlReader.ReadSubtree(), isNew);
                                                    countOfAttachment++;
                                                    continue;
                                                }
                                                else
                                                {
                                                    //Update can't process attachment without match
                                                    continue;
                                                }
                                            }
                                            else
                                            {
                                                //Update can't process attachment without uid
                                                continue;
                                            }
                                        }

                                        //Add New Well 
                                        attachment = new obj_attachment();
                                        attachment.HandleXML(xmlReader.ReadSubtree(), true);
                                        listAttachment.Add(attachment);
                                        countOfAttachment++;
                                        break;
                                    default:
                                        break;
                                }
                            }
                        }
                        //assign list to wellbore collection
                        this.attachment = listAttachment.ToArray();
                    }
                }
            }
            catch (XmlException ex)
            {
                throw new Exceptions.UnableToDecodeException(ex.Message, WITSMLReturnCode.WITSMLDocumentError, ex.InnerException);
            }
            catch (NullReferenceException ex)
            {
                throw new Exceptions.UnableToDecodeException(ex.Message, WITSMLReturnCode.WITSMLEmptyDocument, ex.InnerException);
            }
            catch (Exceptions.UnableToDecodeException ex)
            {
                throw ex;
            }
            catch (Exception ex)
            {
                throw new Exceptions.UnableToDecodeException(ex.Message, WITSMLReturnCode.WITSMLUnkownCause, ex.InnerException);
            }
            return countOfAttachment;

        }

    }
}
