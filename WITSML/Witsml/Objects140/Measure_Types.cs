﻿ 

namespace Witsml.Objects140
{

    using System.Xml.Serialization;

    /// <remarks/>
    [XmlInclude(typeof(footageEastWest))]
    [XmlInclude(typeof(footageNorthSouth))]
    [XmlInclude(typeof(wellElevationCoord))]
    [XmlInclude(typeof(wellVerticalDepthCoord))]
    [XmlInclude(typeof(measuredDepthCoord))]
    [XmlInclude(typeof(ratioGenericMeasure))]
    [XmlInclude(typeof(genericMeasure))]
    [XmlInclude(typeof(volumePerVolumeMeasurePercent))]
    [XmlInclude(typeof(yAxisAzimuth))]
    [XmlInclude(typeof(volumePerVolumeMeasure))]
    [XmlInclude(typeof(volumeFlowRateMeasure))]
    [XmlInclude(typeof(volumeMeasure))]
    [XmlInclude(typeof(velocityMeasure))]
    [XmlInclude(typeof(timeMeasure))]
    [XmlInclude(typeof(thermodynamicTemperatureMeasure))]
    [XmlInclude(typeof(specificVolumeMeasure))]
    [XmlInclude(typeof(relativePowerMeasure))]
    [XmlInclude(typeof(pressureMeasure))]
    [XmlInclude(typeof(powerMeasure))]
    [XmlInclude(typeof(planeAngleMeasure))]
    [XmlInclude(typeof(perLengthMeasure))]
    [XmlInclude(typeof(momentOfForceMeasure))]
    [XmlInclude(typeof(massPerLengthMeasure))]
    [XmlInclude(typeof(massMeasure))]
    [XmlInclude(typeof(massConcentrationMeasure))]
    [XmlInclude(typeof(magneticInductionMeasure))]
    [XmlInclude(typeof(lengthPerLengthMeasure))]
    [XmlInclude(typeof(lengthMeasure))]
    [XmlInclude(typeof(illuminanceMeasure))]
    [XmlInclude(typeof(forcePerVolumeMeasure))]
    [XmlInclude(typeof(forcePerLengthMeasure))]
    [XmlInclude(typeof(forceMeasure))]
    [XmlInclude(typeof(equivalentPerMassMeasure))]
    [XmlInclude(typeof(electricPotentialMeasure))]
    [XmlInclude(typeof(electricCurrentMeasure))]
    [XmlInclude(typeof(dynamicViscosityMeasure))]
    [XmlInclude(typeof(dimensionlessMeasure))]
    [XmlInclude(typeof(densityMeasure))]
    [XmlInclude(typeof(areaPerAreaMeasure))]
    [XmlInclude(typeof(areaMeasure))]
    [XmlInclude(typeof(anglePerTimeMeasure))]
    [XmlInclude(typeof(anglePerLengthMeasure))]
    [XmlInclude(typeof(accelerationLinearMeasure))]
    [System.CodeDom.Compiler.GeneratedCodeAttribute("xsd", "2.0.50727.42")]
    

    
    [XmlType(Namespace = "http://www.witsml.org/schemas/140")]
    public abstract partial class abstractMeasure
    {

        /// <remarks/>
        [XmlText()]
        public double Value;
    }

    /// <remarks/>
    [System.CodeDom.Compiler.GeneratedCodeAttribute("xsd", "2.0.50727.42")]
    
    
    [XmlType(Namespace = "http://www.witsml.org/schemas/140")]
    public partial class yAxisAzimuth : abstractMeasure
    {

        /// <remarks/>
        [XmlAttribute()]
        public PlaneAngleUom uom;

        /// <remarks/>
        [XmlAttribute()]
        public AziRef northDirection;

        /// <remarks/>
        [XmlIgnore()]
        public bool northDirectionSpecified;
    }

    /// <remarks/>
    [System.CodeDom.Compiler.GeneratedCodeAttribute("xsd", "2.0.50727.42")]
    
    
    [XmlType(Namespace = "http://www.witsml.org/schemas/140")]
    public partial class footageEastWest : abstractMeasure
    {

        /// <remarks/>
        [XmlAttribute()]
        public LengthUom uom;

        /// <remarks/>
        [XmlAttribute()]
        public EastOrWest @ref;
    }

    /// <remarks/>
    [System.CodeDom.Compiler.GeneratedCodeAttribute("xsd", "2.0.50727.42")]
    
    
    [XmlType(Namespace = "http://www.witsml.org/schemas/140")]
    public partial class footageNorthSouth : abstractMeasure
    {

        /// <remarks/>
        [XmlAttribute()]
        public LengthUom uom;

        /// <remarks/>
        [XmlAttribute()]
        public NorthOrSouth @ref;
    }

    /// <remarks/>
    [System.CodeDom.Compiler.GeneratedCodeAttribute("xsd", "2.0.50727.42")]
    
    
    [XmlType(Namespace = "http://www.witsml.org/schemas/140")]
    public partial class wellElevationCoord : abstractMeasure
    {

        /// <remarks/>
        [XmlAttribute()]
        public WellVerticalCoordinateUom uom;

        /// <remarks/>
        [XmlAttribute()]
        public string datum;
    }

    /// <remarks/>
    [System.CodeDom.Compiler.GeneratedCodeAttribute("xsd", "2.0.50727.42")]
    
    
    [XmlType(Namespace = "http://www.witsml.org/schemas/140")]
    public partial class wellVerticalDepthCoord : abstractMeasure
    {

        /// <remarks/>
        [XmlAttribute()]
        public WellVerticalCoordinateUom uom;

        /// <remarks/>
        [XmlAttribute()]
        public string datum;
    }

    /// <remarks/>
    [System.CodeDom.Compiler.GeneratedCodeAttribute("xsd", "2.0.50727.42")]
    
    
    [XmlType(Namespace = "http://www.witsml.org/schemas/140")]
    public partial class measuredDepthCoord : abstractMeasure
    {

        /// <remarks/>
        [XmlAttribute()]
        public MeasuredDepthUom uom;

        /// <remarks/>
        [XmlAttribute()]
        public string datum;
    }

    /// <remarks/>
    [System.CodeDom.Compiler.GeneratedCodeAttribute("xsd", "2.0.50727.42")]
    
    
    [XmlType(Namespace = "http://www.witsml.org/schemas/140")]
    public partial class ratioGenericMeasure : abstractMeasure
    {

        /// <remarks/>
        [XmlAttribute()]
        public string uom;

        /// <remarks/>
        [XmlAttribute()]
        public double numerator;

        /// <remarks/>
        [XmlIgnore()]
        public bool numeratorSpecified;

        /// <remarks/>
        [XmlAttribute()]
        public double denominator;

        /// <remarks/>
        [XmlIgnore()]
        public bool denominatorSpecified;
    }

    /// <remarks/>
    [System.CodeDom.Compiler.GeneratedCodeAttribute("xsd", "2.0.50727.42")]
    
    
    [XmlType(Namespace = "http://www.witsml.org/schemas/140")]
    public partial class genericMeasure : abstractMeasure
    {

        /// <remarks/>
        [XmlAttribute()]
        public string uom;
    }

    /// <remarks/>
    [System.CodeDom.Compiler.GeneratedCodeAttribute("xsd", "2.0.50727.42")]
    
    
    [XmlType(Namespace = "http://www.witsml.org/schemas/140")]
    public partial class volumePerVolumeMeasurePercent : abstractMeasure
    {

        /// <remarks/>
        [XmlAttribute()]
        public PercentUom uom;
    }

    /// <remarks/>
    [System.CodeDom.Compiler.GeneratedCodeAttribute("xsd", "2.0.50727.42")]
    
    
    [XmlType(Namespace = "http://www.witsml.org/schemas/140")]
    public partial class volumePerVolumeMeasure : abstractMeasure
    {

        /// <remarks/>
        [XmlAttribute()]
        public VolumePerVolumeUom uom;
    }



    /// <remarks/>
    [System.CodeDom.Compiler.GeneratedCodeAttribute("xsd", "2.0.50727.42")]
    
    
    [XmlType(Namespace = "http://www.witsml.org/schemas/140")]
    public partial class volumeFlowRateMeasure : abstractMeasure
    {

        /// <remarks/>
        [XmlAttribute()]
        public VolumeFlowRateUom uom;
    }

    /// <remarks/>
    [System.CodeDom.Compiler.GeneratedCodeAttribute("xsd", "2.0.50727.42")]
    
    
    [XmlType(Namespace = "http://www.witsml.org/schemas/140")]
    public partial class volumeMeasure : abstractMeasure
    {

        /// <remarks/>
        [XmlAttribute()]
        public VolumeUom uom;
    }

    /// <remarks/>
    [System.CodeDom.Compiler.GeneratedCodeAttribute("xsd", "2.0.50727.42")]
    
    
    [XmlType(Namespace = "http://www.witsml.org/schemas/140")]
    public partial class velocityMeasure : abstractMeasure
    {

        /// <remarks/>
        [XmlAttribute()]
        public VelocityUom uom;
    }

    /// <remarks/>
    [System.CodeDom.Compiler.GeneratedCodeAttribute("xsd", "2.0.50727.42")]
    
    
    [XmlType(Namespace = "http://www.witsml.org/schemas/140")]
    public partial class timeMeasure : abstractMeasure
    {

        /// <remarks/>
        [XmlAttribute()]
        public TimeUom uom;
    }

    /// <remarks/>
    [System.CodeDom.Compiler.GeneratedCodeAttribute("xsd", "2.0.50727.42")]
    
    
    [XmlType(Namespace = "http://www.witsml.org/schemas/140")]
    public partial class thermodynamicTemperatureMeasure : abstractMeasure
    {

        /// <remarks/>
        [XmlAttribute()]
        public ThermodynamicTemperatureUom uom;
    }

    /// <remarks/>
    [System.CodeDom.Compiler.GeneratedCodeAttribute("xsd", "2.0.50727.42")]
    
    
    [XmlType(Namespace = "http://www.witsml.org/schemas/140")]
    public partial class specificVolumeMeasure : abstractMeasure
    {

        /// <remarks/>
        [XmlAttribute()]
        public SpecificVolumeUom uom;
    }


    /// <remarks/>
    [System.CodeDom.Compiler.GeneratedCodeAttribute("xsd", "2.0.50727.42")]
    
    
    [XmlType(Namespace = "http://www.witsml.org/schemas/140")]
    public partial class relativePowerMeasure : abstractMeasure
    {

        /// <remarks/>
        [XmlAttribute()]
        public RelativePowerUom uom;
    }


    /// <remarks/>
    [System.CodeDom.Compiler.GeneratedCodeAttribute("xsd", "2.0.50727.42")]
    
    
    [XmlType(Namespace = "http://www.witsml.org/schemas/140")]
    public partial class pressureMeasure : abstractMeasure
    {

        /// <remarks/>
        [XmlAttribute()]
        public PressureUom uom;
    }



    /// <remarks/>
    [System.CodeDom.Compiler.GeneratedCodeAttribute("xsd", "2.0.50727.42")]
    
    
    [XmlType(Namespace = "http://www.witsml.org/schemas/140")]
    public partial class powerMeasure : abstractMeasure
    {

        /// <remarks/>
        [XmlAttribute()]
        public PowerUom uom;
    }

    /// <remarks/>
    [System.CodeDom.Compiler.GeneratedCodeAttribute("xsd", "2.0.50727.42")]
    
    
    [XmlType(Namespace = "http://www.witsml.org/schemas/140")]
    public partial class planeAngleMeasure : abstractMeasure
    {

        /// <remarks/>
        [XmlAttribute()]
        public PlaneAngleUom uom;
    }

    /// <remarks/>
    [System.CodeDom.Compiler.GeneratedCodeAttribute("xsd", "2.0.50727.42")]
    
    
    [XmlType(Namespace = "http://www.witsml.org/schemas/140")]
    public partial class perLengthMeasure : abstractMeasure
    {

        /// <remarks/>
        [XmlAttribute()]
        public PerLengthUom uom;
    }



    /// <remarks/>
    [System.CodeDom.Compiler.GeneratedCodeAttribute("xsd", "2.0.50727.42")]
    
    
    [XmlType(Namespace = "http://www.witsml.org/schemas/140")]
    public partial class momentOfForceMeasure : abstractMeasure
    {

        /// <remarks/>
        [XmlAttribute()]
        public MomentOfForceUom uom;
    }

    /// <remarks/>
    [System.CodeDom.Compiler.GeneratedCodeAttribute("xsd", "2.0.50727.42")]
    
    
    [XmlType(Namespace = "http://www.witsml.org/schemas/140")]
    public partial class massPerLengthMeasure : abstractMeasure
    {

        /// <remarks/>
        [XmlAttribute()]
        public MassPerLengthUom uom;
    }

    /// <remarks/>
    [System.CodeDom.Compiler.GeneratedCodeAttribute("xsd", "2.0.50727.42")]
    
    
    [XmlType(Namespace = "http://www.witsml.org/schemas/140")]
    public partial class massMeasure : abstractMeasure
    {

        /// <remarks/>
        [XmlAttribute()]
        public MassUom uom;
    }



    /// <remarks/>
    [System.CodeDom.Compiler.GeneratedCodeAttribute("xsd", "2.0.50727.42")]
    
    
    [XmlType(Namespace = "http://www.witsml.org/schemas/140")]
    public partial class massConcentrationMeasure : abstractMeasure
    {

        /// <remarks/>
        [XmlAttribute()]
        public MassConcentrationUom uom;
    }



    /// <remarks/>
    [System.CodeDom.Compiler.GeneratedCodeAttribute("xsd", "2.0.50727.42")]
    
    
    [XmlType(Namespace = "http://www.witsml.org/schemas/140")]
    public partial class magneticInductionMeasure : abstractMeasure
    {

        /// <remarks/>
        [XmlAttribute()]
        public MagneticInductionUom uom;
    }



    /// <remarks/>
    [System.CodeDom.Compiler.GeneratedCodeAttribute("xsd", "2.0.50727.42")]
    
    
    [XmlType(Namespace = "http://www.witsml.org/schemas/140")]
    public partial class lengthPerLengthMeasure : abstractMeasure
    {

        /// <remarks/>
        [XmlAttribute()]
        public LengthPerLengthUom uom;
    }



    /// <remarks/>
    [System.CodeDom.Compiler.GeneratedCodeAttribute("xsd", "2.0.50727.42")]
    
    
    [XmlType(Namespace = "http://www.witsml.org/schemas/140")]
    public partial class lengthMeasure : abstractMeasure
    {

        /// <remarks/>
        [XmlAttribute()]
        public LengthUom uom;
    }

    /// <remarks/>
    [System.CodeDom.Compiler.GeneratedCodeAttribute("xsd", "2.0.50727.42")]
    
    
    [XmlType(Namespace = "http://www.witsml.org/schemas/140")]
    public partial class illuminanceMeasure : abstractMeasure
    {

        /// <remarks/>
        [XmlAttribute()]
        public IlluminanceUom uom;
    }



    /// <remarks/>
    [System.CodeDom.Compiler.GeneratedCodeAttribute("xsd", "2.0.50727.42")]
    
    
    [XmlType(Namespace = "http://www.witsml.org/schemas/140")]
    public partial class forcePerVolumeMeasure : abstractMeasure
    {

        /// <remarks/>
        [XmlAttribute()]
        public ForcePerVolumeUom uom;
    }



    /// <remarks/>
    [System.CodeDom.Compiler.GeneratedCodeAttribute("xsd", "2.0.50727.42")]
    
    
    [XmlType(Namespace = "http://www.witsml.org/schemas/140")]
    public partial class forcePerLengthMeasure : abstractMeasure
    {

        /// <remarks/>
        [XmlAttribute()]
        public ForcePerLengthUom uom;
    }



    /// <remarks/>
    [System.CodeDom.Compiler.GeneratedCodeAttribute("xsd", "2.0.50727.42")]
    
    
    [XmlType(Namespace = "http://www.witsml.org/schemas/140")]
    public partial class forceMeasure : abstractMeasure
    {

        /// <remarks/>
        [XmlAttribute()]
        public ForceUom uom;
    }


    /// <remarks/>
    [System.CodeDom.Compiler.GeneratedCodeAttribute("xsd", "2.0.50727.42")]
    
    
    [XmlType(Namespace = "http://www.witsml.org/schemas/140")]
    public partial class equivalentPerMassMeasure : abstractMeasure
    {

        /// <remarks/>
        [XmlAttribute()]
        public EquivalentPerMassUom uom;
    }



    /// <remarks/>
    [System.CodeDom.Compiler.GeneratedCodeAttribute("xsd", "2.0.50727.42")]
    
    
    [XmlType(Namespace = "http://www.witsml.org/schemas/140")]
    public partial class electricPotentialMeasure : abstractMeasure
    {

        /// <remarks/>
        [XmlAttribute()]
        public ElectricPotentialUom uom;
    }



    /// <remarks/>
    [System.CodeDom.Compiler.GeneratedCodeAttribute("xsd", "2.0.50727.42")]
    
    
    [XmlType(Namespace = "http://www.witsml.org/schemas/140")]
    public partial class electricCurrentMeasure : abstractMeasure
    {

        /// <remarks/>
        [XmlAttribute()]
        public ElectricCurrentUom uom;
    }



    /// <remarks/>
    [System.CodeDom.Compiler.GeneratedCodeAttribute("xsd", "2.0.50727.42")]
    
    
    [XmlType(Namespace = "http://www.witsml.org/schemas/140")]
    public partial class dynamicViscosityMeasure : abstractMeasure
    {

        /// <remarks/>
        [XmlAttribute()]
        public DynamicViscosityUom uom;
    }



    /// <remarks/>
    [System.CodeDom.Compiler.GeneratedCodeAttribute("xsd", "2.0.50727.42")]
    
    
    [XmlType(Namespace = "http://www.witsml.org/schemas/140")]
    public partial class dimensionlessMeasure : abstractMeasure
    {

        /// <remarks/>
        [XmlAttribute()]
        public DimensionlessUom uom;
    }



    /// <remarks/>
    [System.CodeDom.Compiler.GeneratedCodeAttribute("xsd", "2.0.50727.42")]
    
    
    [XmlType(Namespace = "http://www.witsml.org/schemas/140")]
    public partial class densityMeasure : abstractMeasure
    {

        /// <remarks/>
        [XmlAttribute()]
        public DensityUom uom;
    }



    /// <remarks/>
    [System.CodeDom.Compiler.GeneratedCodeAttribute("xsd", "2.0.50727.42")]
    
    
    [XmlType(Namespace = "http://www.witsml.org/schemas/140")]
    public partial class areaPerAreaMeasure : abstractMeasure
    {

        /// <remarks/>
        [XmlAttribute()]
        public AreaPerAreaUom uom;
    }



    /// <remarks/>
    [System.CodeDom.Compiler.GeneratedCodeAttribute("xsd", "2.0.50727.42")]
    
    
    [XmlType(Namespace = "http://www.witsml.org/schemas/140")]
    public partial class areaMeasure : abstractMeasure
    {

        /// <remarks/>
        [XmlAttribute()]
        public AreaUom uom;
    }



    /// <remarks/>
    [System.CodeDom.Compiler.GeneratedCodeAttribute("xsd", "2.0.50727.42")]
    
    
    [XmlType(Namespace = "http://www.witsml.org/schemas/140")]
    public partial class anglePerTimeMeasure : abstractMeasure
    {

        /// <remarks/>
        [XmlAttribute()]
        public AnglePerTimeUom uom;
    }



    /// <remarks/>
    [System.CodeDom.Compiler.GeneratedCodeAttribute("xsd", "2.0.50727.42")]
    
    
    [XmlType(Namespace = "http://www.witsml.org/schemas/140")]
    public partial class anglePerLengthMeasure : abstractMeasure
    {

        /// <remarks/>
        [XmlAttribute()]
        public AnglePerLengthUom uom;
    }



    /// <remarks/>
    [System.CodeDom.Compiler.GeneratedCodeAttribute("xsd", "2.0.50727.42")]
    
    
    [XmlType(Namespace = "http://www.witsml.org/schemas/140")]
    public partial class accelerationLinearMeasure : abstractMeasure
    {

        /// <remarks/>
        [XmlAttribute()]
        public AccelerationLinearUom uom;
    }

}
