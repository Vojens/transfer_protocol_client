﻿ 

using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;

namespace Witsml.Objects140
{
    /// <remarks/>
    [System.CodeDom.Compiler.GeneratedCodeAttribute("xsd", "2.0.50727.42")]
    
    
    [System.Xml.Serialization.XmlTypeAttribute(Namespace = "http://www.witsml.org/schemas/140")]
    public partial class cs_commonData
    {

        /// <remarks/>
        public string sourceName;

        /// <remarks/>
        public System.DateTime dTimCreation;

        /// <remarks/>
        [System.Xml.Serialization.XmlIgnoreAttribute()]
        public bool dTimCreationSpecified;

        /// <remarks/>
        public System.DateTime dTimLastChange;

        /// <remarks/>
        [System.Xml.Serialization.XmlIgnoreAttribute()]
        public bool dTimLastChangeSpecified;

        /// <remarks/>
        public ItemState itemState;

        /// <remarks/>
        [System.Xml.Serialization.XmlIgnoreAttribute()]
        public bool itemStateSpecified;

        /// <remarks/>
        public ServiceCategory serviceCategory;

        /// <remarks/>
        [System.Xml.Serialization.XmlIgnoreAttribute()]
        public bool serviceCategorySpecified;

        /// <remarks/>
        public string comments;

        /// <remarks/>
        [System.Xml.Serialization.XmlElementAttribute("acquisitionTimeZone")]
        public timestampedTimeZone[] acquisitionTimeZone;
    }

    


    /// <remarks/>
    [System.CodeDom.Compiler.GeneratedCodeAttribute("xsd", "2.0.50727.42")]
    
    
    [System.Xml.Serialization.XmlTypeAttribute(Namespace = "http://www.witsml.org/schemas/140")]
    public partial class timestampedTimeZone
    {

        /// <remarks/>
        [System.Xml.Serialization.XmlAttributeAttribute()]
        public System.DateTime dTim;

        /// <remarks/>
        [System.Xml.Serialization.XmlIgnoreAttribute()]
        public bool dTimSpecified;

        /// <remarks/>
        [System.Xml.Serialization.XmlTextAttribute()]
        public string Value;
    }
}
