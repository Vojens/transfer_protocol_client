﻿ 

namespace Witsml.Objects140
{
    using System.Xml.Serialization;

    /// <remarks/>
    [System.CodeDom.Compiler.GeneratedCodeAttribute("xsd", "2.0.50727.42")]
    
    [XmlType(Namespace = "http://www.witsml.org/schemas/140")]
    public enum PlaneAngleUom
    {

        /// <remarks/>
        rad,

        /// <remarks/>
        c,

        /// <remarks/>
        ccgr,

        /// <remarks/>
        cgr,

        /// <remarks/>
        dega,

        /// <remarks/>
        gon,

        /// <remarks/>
        gr,

        /// <remarks/>
        Grad,

        /// <remarks/>
        krad,

        /// <remarks/>
        mila,

        /// <remarks/>
        mina,

        /// <remarks/>
        mrad,

        /// <remarks/>
        Mrad,

        /// <remarks/>
        mseca,

        /// <remarks/>
        seca,

        /// <remarks/>
        urad,

        /// <remarks/>
        unknown,
    }

    /// <remarks/>
    [System.CodeDom.Compiler.GeneratedCodeAttribute("xsd", "2.0.50727.42")]
    
    [XmlType(Namespace = "http://www.witsml.org/schemas/140")]
    public enum AziRef
    {

        /// <remarks/>
        [XmlEnum("magnetic north")]
        magneticnorth,

        /// <remarks/>
        [XmlEnum("grid north")]
        gridnorth,

        /// <remarks/>
        [XmlEnum("true north")]
        truenorth,

        /// <remarks/>
        unknown,
    }

    /// <remarks/>
    [System.CodeDom.Compiler.GeneratedCodeAttribute("xsd", "2.0.50727.42")]
    
    [XmlType(Namespace = "http://www.witsml.org/schemas/140")]
    public enum LengthUom
    {

        /// <remarks/>
        m,

        /// <remarks/>
        angstrom,

        /// <remarks/>
        chBnA,

        /// <remarks/>
        chBnB,

        /// <remarks/>
        chCla,

        /// <remarks/>
        chSe,

        /// <remarks/>
        chUS,

        /// <remarks/>
        cm,

        /// <remarks/>
        dm,

        /// <remarks/>
        fathom,

        /// <remarks/>
        fm,

        /// <remarks/>
        ft,

        /// <remarks/>
        ftBnA,

        /// <remarks/>
        ftBnB,

        /// <remarks/>
        [XmlEnum("ftBr(65)")]
        ftBr65,

        /// <remarks/>
        ftCla,

        /// <remarks/>
        ftGC,

        /// <remarks/>
        ftInd,

        /// <remarks/>
        [XmlEnum("ftInd(37)")]
        ftInd37,

        /// <remarks/>
        [XmlEnum("ftInd(62)")]
        ftInd62,

        /// <remarks/>
        [XmlEnum("ftInd(75)")]
        ftInd75,

        /// <remarks/>
        ftMA,

        /// <remarks/>
        ftSe,

        /// <remarks/>
        ftUS,

        /// <remarks/>
        @in,

        /// <remarks/>
        [XmlEnum("in/10")]
        in10,

        /// <remarks/>
        [XmlEnum("in/16")]
        in16,

        /// <remarks/>
        [XmlEnum("in/32")]
        in32,

        /// <remarks/>
        [XmlEnum("in/64")]
        in64,

        /// <remarks/>
        inUS,

        /// <remarks/>
        km,

        /// <remarks/>
        lkBnA,

        /// <remarks/>
        lkBnB,

        /// <remarks/>
        lkCla,

        /// <remarks/>
        lkSe,

        /// <remarks/>
        lkUS,

        /// <remarks/>
        mGer,

        /// <remarks/>
        mi,

        /// <remarks/>
        mil,

        /// <remarks/>
        miUS,

        /// <remarks/>
        mm,

        /// <remarks/>
        Mm,

        /// <remarks/>
        nautmi,

        /// <remarks/>
        nm,

        /// <remarks/>
        pm,

        /// <remarks/>
        um,

        /// <remarks/>
        yd,

        /// <remarks/>
        ydBnA,

        /// <remarks/>
        ydBnB,

        /// <remarks/>
        ydCla,

        /// <remarks/>
        ydIm,

        /// <remarks/>
        ydInd,

        /// <remarks/>
        [XmlEnum("ydInd(37)")]
        ydInd37,

        /// <remarks/>
        [XmlEnum("ydInd(62)")]
        ydInd62,

        /// <remarks/>
        [XmlEnum("ydInd(75)")]
        ydInd75,

        /// <remarks/>
        ydSe,

        /// <remarks/>
        unknown,
    }

    /// <remarks/>
    [System.CodeDom.Compiler.GeneratedCodeAttribute("xsd", "2.0.50727.42")]
    
    [XmlType(Namespace = "http://www.witsml.org/schemas/140")]
    public enum EastOrWest
    {

        /// <remarks/>
        east,

        /// <remarks/>
        west,

        /// <remarks/>
        unknown,
    }

    /// <remarks/>
    [System.CodeDom.Compiler.GeneratedCodeAttribute("xsd", "2.0.50727.42")]
    
    [XmlType(Namespace = "http://www.witsml.org/schemas/140")]
    public enum NorthOrSouth
    {

        /// <remarks/>
        north,

        /// <remarks/>
        south,

        /// <remarks/>
        unknown,
    }

    /// <remarks/>
    [System.CodeDom.Compiler.GeneratedCodeAttribute("xsd", "2.0.50727.42")]
    
    [XmlType(Namespace = "http://www.witsml.org/schemas/140")]
    public enum WellVerticalCoordinateUom
    {

        /// <remarks/>
        m,

        /// <remarks/>
        ft,

        /// <remarks/>
        ftUS,

        /// <remarks/>
        [XmlEnum("ftBr(65)")]
        ftBr65,

        /// <remarks/>
        unknown,
    }

    /// <remarks/>
    [System.CodeDom.Compiler.GeneratedCodeAttribute("xsd", "2.0.50727.42")]
    
    [XmlType(Namespace = "http://www.witsml.org/schemas/140")]
    public enum MeasuredDepthUom
    {

        /// <remarks/>
        m,

        /// <remarks/>
        ft,

        /// <remarks/>
        ftUS,

        /// <remarks/>
        unknown,
    }

    /// <remarks/>
    [System.CodeDom.Compiler.GeneratedCodeAttribute("xsd", "2.0.50727.42")]
    
    [XmlType(Namespace = "http://www.witsml.org/schemas/140")]
    public enum PercentUom
    {

        /// <remarks/>
        [XmlEnum("%")]
        Item,

        /// <remarks/>
        unknown,
    }

    /// <remarks/>
    [System.CodeDom.Compiler.GeneratedCodeAttribute("xsd", "2.0.50727.42")]
    
    [XmlType(Namespace = "http://www.witsml.org/schemas/140")]
    public enum VolumePerVolumeUom
    {

        /// <remarks/>
        Euc,

        /// <remarks/>
        [XmlEnum("%")]
        Item,

        /// <remarks/>
        permil,

        /// <remarks/>
        ppdk,

        /// <remarks/>
        ppk,

        /// <remarks/>
        ppm,

        /// <remarks/>
        [XmlEnum("bbl/acre.ft")]
        bblacreft,

        /// <remarks/>
        [XmlEnum("bbl/bbl")]
        bblbbl,

        /// <remarks/>
        [XmlEnum("bbl/ft3")]
        bblft3,

        /// <remarks/>
        [XmlEnum("bbl/100bbl")]
        bbl100bbl,

        /// <remarks/>
        [XmlEnum("bbl/k(ft3)")]
        bblkft3,

        /// <remarks/>
        [XmlEnum("bbl/M(ft3)")]
        bblMft3,

        /// <remarks/>
        [XmlEnum("cm3/cm3")]
        cm3cm3,

        /// <remarks/>
        [XmlEnum("cm3/m3")]
        cm3m3,

        /// <remarks/>
        [XmlEnum("dm3/m3")]
        dm3m3,

        /// <remarks/>
        [XmlEnum("ft3/bbl")]
        ft3bbl,

        /// <remarks/>
        [XmlEnum("ft3/ft3")]
        ft3ft3,

        /// <remarks/>
        [XmlEnum("galUS/kgalUS")]
        galUSkgalUS,

        /// <remarks/>
        [XmlEnum("galUK/kgalUK")]
        galUKkgalUK,

        /// <remarks/>
        [XmlEnum("galUK/ft3")]
        galUKft3,

        /// <remarks/>
        [XmlEnum("galUK/Mbbl")]
        galUKMbbl,

        /// <remarks/>
        [XmlEnum("galUS/bbl")]
        galUSbbl,

        /// <remarks/>
        [XmlEnum("galUS/10bbl")]
        galUS10bbl,

        /// <remarks/>
        [XmlEnum("galUS/ft3")]
        galUSft3,

        /// <remarks/>
        [XmlEnum("galUS/Mbbl")]
        galUSMbbl,

        /// <remarks/>
        [XmlEnum("1000ft3/bbl")]
        Item1000ft3bbl,

        /// <remarks/>
        [XmlEnum("ksm3/sm3")]
        ksm3sm3,

        /// <remarks/>
        [XmlEnum("L/10bbl")]
        L10bbl,

        /// <remarks/>
        [XmlEnum("L/m3")]
        Lm3,

        /// <remarks/>
        [XmlEnum("m3/ha.m")]
        m3ham,

        /// <remarks/>
        [XmlEnum("m3/m3")]
        m3m3,

        /// <remarks/>
        [XmlEnum("M(ft3)/acre.ft")]
        Mft3acreft,

        /// <remarks/>
        [XmlEnum("mL/galUK")]
        mLgalUK,

        /// <remarks/>
        [XmlEnum("mL/galUS")]
        mLgalUS,

        /// <remarks/>
        [XmlEnum("mL/mL")]
        mLmL,

        /// <remarks/>
        [XmlEnum("MMbbl/acre.ft")]
        MMbblacreft,

        /// <remarks/>
        [XmlEnum("MMscf60/stb60")]
        MMscf60stb60,

        /// <remarks/>
        [XmlEnum("Mscf60/stb60")]
        Mscf60stb60,

        /// <remarks/>
        [XmlEnum("ptUK/Mbbl")]
        ptUKMbbl,

        /// <remarks/>
        [XmlEnum("ptUS/10bbl")]
        ptUS10bbl,

        /// <remarks/>
        pu,

        /// <remarks/>
        [XmlEnum("scm15/stb60")]
        scm15stb60,

        /// <remarks/>
        [XmlEnum("sm3/ksm3")]
        sm3ksm3,

        /// <remarks/>
        [XmlEnum("sm3/sm3")]
        sm3sm3,

        /// <remarks/>
        [XmlEnum("stb60/MMscf60")]
        stb60MMscf60,

        /// <remarks/>
        [XmlEnum("stb60/MMscm15")]
        stb60MMscm15,

        /// <remarks/>
        [XmlEnum("stb60/Mscf60")]
        stb60Mscf60,

        /// <remarks/>
        [XmlEnum("stb60/Mscm15")]
        stb60Mscm15,

        /// <remarks/>
        [XmlEnum("stb60/scm15")]
        stb60scm15,

        /// <remarks/>
        unknown,
    }

    /// <remarks/>
    [System.CodeDom.Compiler.GeneratedCodeAttribute("xsd", "2.0.50727.42")]
    
    [XmlType(Namespace = "http://www.witsml.org/schemas/140")]
    public enum VolumeFlowRateUom
    {

        /// <remarks/>
        [XmlEnum("m3/s")]
        m3s,

        /// <remarks/>
        [XmlEnum("bbl/d")]
        bbld,

        /// <remarks/>
        [XmlEnum("bbl/hr")]
        bblhr,

        /// <remarks/>
        [XmlEnum("bbl/min")]
        bblmin,

        /// <remarks/>
        [XmlEnum("cm3/30min")]
        cm330min,

        /// <remarks/>
        [XmlEnum("cm3/h")]
        cm3h,

        /// <remarks/>
        [XmlEnum("cm3/min")]
        cm3min,

        /// <remarks/>
        [XmlEnum("cm3/s")]
        cm3s,

        /// <remarks/>
        [XmlEnum("dm3/s")]
        dm3s,

        /// <remarks/>
        [XmlEnum("ft3/d")]
        ft3d,

        /// <remarks/>
        [XmlEnum("ft3/h")]
        ft3h,

        /// <remarks/>
        [XmlEnum("ft3/min")]
        ft3min,

        /// <remarks/>
        [XmlEnum("ft3/s")]
        ft3s,

        /// <remarks/>
        [XmlEnum("galUK/d")]
        galUKd,

        /// <remarks/>
        [XmlEnum("galUK/hr")]
        galUKhr,

        /// <remarks/>
        [XmlEnum("galUK/min")]
        galUKmin,

        /// <remarks/>
        [XmlEnum("galUS/d")]
        galUSd,

        /// <remarks/>
        [XmlEnum("galUS/hr")]
        galUShr,

        /// <remarks/>
        [XmlEnum("galUS/min")]
        galUSmin,

        /// <remarks/>
        [XmlEnum("kbbl/d")]
        kbbld,

        /// <remarks/>
        [XmlEnum("1000ft3/d")]
        Item1000ft3d,

        /// <remarks/>
        [XmlEnum("1000m3/d")]
        Item1000m3d,

        /// <remarks/>
        [XmlEnum("1000m3/h")]
        Item1000m3h,

        /// <remarks/>
        [XmlEnum("L/h")]
        Lh,

        /// <remarks/>
        [XmlEnum("L/min")]
        Lmin,

        /// <remarks/>
        [XmlEnum("L/s")]
        Ls,

        /// <remarks/>
        [XmlEnum("m3/d")]
        m3d,

        /// <remarks/>
        [XmlEnum("m3/h")]
        m3h,

        /// <remarks/>
        [XmlEnum("m3/min")]
        m3min,

        /// <remarks/>
        [XmlEnum("Mbbl/d")]
        Mbbld,

        /// <remarks/>
        [XmlEnum("M(ft3)/d")]
        Mft3d,

        /// <remarks/>
        [XmlEnum("M(m3)/d")]
        Mm3d,

        /// <remarks/>
        unknown,
    }

    /// <remarks/>
    [System.CodeDom.Compiler.GeneratedCodeAttribute("xsd", "2.0.50727.42")]
    
    [XmlType(Namespace = "http://www.witsml.org/schemas/140")]
    public enum VolumeUom
    {

        /// <remarks/>
        m3,

        /// <remarks/>
        [XmlEnum("acre.ft")]
        acreft,

        /// <remarks/>
        bbl,

        /// <remarks/>
        bcf,

        /// <remarks/>
        cm3,

        /// <remarks/>
        dm3,

        /// <remarks/>
        flozUK,

        /// <remarks/>
        flozUS,

        /// <remarks/>
        ft3,

        /// <remarks/>
        galUK,

        /// <remarks/>
        galUS,

        /// <remarks/>
        [XmlEnum("ha.m")]
        ham,

        /// <remarks/>
        hL,

        /// <remarks/>
        in3,

        /// <remarks/>
        [XmlEnum("1000ft3")]
        Item1000ft3,

        /// <remarks/>
        km3,

        /// <remarks/>
        L,

        /// <remarks/>
        Mbbl,

        /// <remarks/>
        Mcf,

        /// <remarks/>
        [XmlEnum("M(ft3)")]
        Mft3,

        /// <remarks/>
        mi3,

        /// <remarks/>
        mL,

        /// <remarks/>
        [XmlEnum("M(m3)")]
        Mm3,

        /// <remarks/>
        mm3,

        /// <remarks/>
        MMbbl,

        /// <remarks/>
        ptUK,

        /// <remarks/>
        ptUS,

        /// <remarks/>
        qtUK,

        /// <remarks/>
        qtUS,

        /// <remarks/>
        tcf,

        /// <remarks/>
        [XmlEnum("um2.m")]
        um2m,

        /// <remarks/>
        yd3,

        /// <remarks/>
        unknown,
    }

    /// <remarks/>
    [System.CodeDom.Compiler.GeneratedCodeAttribute("xsd", "2.0.50727.42")]
    
    [XmlType(Namespace = "http://www.witsml.org/schemas/140")]
    public enum VelocityUom
    {

        /// <remarks/>
        [XmlEnum("m/s")]
        ms,

        /// <remarks/>
        [XmlEnum("cm/a")]
        cma,

        /// <remarks/>
        [XmlEnum("cm/s")]
        cms,

        /// <remarks/>
        [XmlEnum("dm/s")]
        dms,

        /// <remarks/>
        [XmlEnum("ft/d")]
        ftd,

        /// <remarks/>
        [XmlEnum("ft/h")]
        fth,

        /// <remarks/>
        [XmlEnum("ft/min")]
        ftmin,

        /// <remarks/>
        [XmlEnum("ft/ms")]
        ftms,

        /// <remarks/>
        [XmlEnum("ft/s")]
        fts,

        /// <remarks/>
        [XmlEnum("ft/us")]
        ftus,

        /// <remarks/>
        [XmlEnum("in/a")]
        ina,

        /// <remarks/>
        [XmlEnum("in/min")]
        inmin,

        /// <remarks/>
        [XmlEnum("in/s")]
        ins,

        /// <remarks/>
        [XmlEnum("kft/h")]
        kfth,

        /// <remarks/>
        [XmlEnum("kft/s")]
        kfts,

        /// <remarks/>
        [XmlEnum("km/h")]
        kmh,

        /// <remarks/>
        [XmlEnum("km/s")]
        kms,

        /// <remarks/>
        knot,

        /// <remarks/>
        [XmlEnum("m/d")]
        md,

        /// <remarks/>
        [XmlEnum("m/h")]
        mh,

        /// <remarks/>
        [XmlEnum("m/min")]
        mmin,

        /// <remarks/>
        [XmlEnum("m/ms")]
        mms,

        /// <remarks/>
        [XmlEnum("mi/h")]
        mih,

        /// <remarks/>
        [XmlEnum("mil/yr")]
        milyr,

        /// <remarks/>
        [XmlEnum("mm/a")]
        mma,

        /// <remarks/>
        [XmlEnum("mm/s")]
        mms1,

        /// <remarks/>
        [XmlEnum("nm/s")]
        nms,

        /// <remarks/>
        [XmlEnum("um/s")]
        ums,

        /// <remarks/>
        unknown,
    }

    /// <remarks/>
    [System.CodeDom.Compiler.GeneratedCodeAttribute("xsd", "2.0.50727.42")]
    
    [XmlType(Namespace = "http://www.witsml.org/schemas/140")]
    public enum TimeUom
    {

        /// <remarks/>
        s,

        /// <remarks/>
        a,

        /// <remarks/>
        cs,

        /// <remarks/>
        d,

        /// <remarks/>
        Ga,

        /// <remarks/>
        h,

        /// <remarks/>
        [XmlEnum("100s")]
        Item100s,

        /// <remarks/>
        Ma,

        /// <remarks/>
        min,

        /// <remarks/>
        ms,

        /// <remarks/>
        [XmlEnum("ms/2")]
        ms2,

        /// <remarks/>
        ns,

        /// <remarks/>
        ps,

        /// <remarks/>
        us,

        /// <remarks/>
        wk,

        /// <remarks/>
        [XmlEnum("100ka")]
        Item100ka,

        /// <remarks/>
        unknown,
    }

    /// <remarks/>
    [System.CodeDom.Compiler.GeneratedCodeAttribute("xsd", "2.0.50727.42")]
    
    [XmlType(Namespace = "http://www.witsml.org/schemas/140")]
    public enum ThermodynamicTemperatureUom
    {

        /// <remarks/>
        K,

        /// <remarks/>
        degC,

        /// <remarks/>
        degF,

        /// <remarks/>
        degR,

        /// <remarks/>
        unknown,
    }

    /// <remarks/>
    [System.CodeDom.Compiler.GeneratedCodeAttribute("xsd", "2.0.50727.42")]
    
    [XmlType(Namespace = "http://www.witsml.org/schemas/140")]
    public enum SpecificVolumeUom
    {

        /// <remarks/>
        [XmlEnum("m3/kg")]
        m3kg,

        /// <remarks/>
        [XmlEnum("bbl/tonUK")]
        bbltonUK,

        /// <remarks/>
        [XmlEnum("bbl/tonUS")]
        bbltonUS,

        /// <remarks/>
        [XmlEnum("cm3/g")]
        cm3g,

        /// <remarks/>
        [XmlEnum("dm3/kg")]
        dm3kg,

        /// <remarks/>
        [XmlEnum("dm3/t")]
        dm3t,

        /// <remarks/>
        [XmlEnum("ft3/kg")]
        ft3kg,

        /// <remarks/>
        [XmlEnum("ft3/lbm")]
        ft3lbm,

        /// <remarks/>
        [XmlEnum("ft3/sack94")]
        ft3sack94,

        /// <remarks/>
        [XmlEnum("galUS/sack94")]
        galUSsack94,

        /// <remarks/>
        [XmlEnum("galUK/lbm")]
        galUKlbm,

        /// <remarks/>
        [XmlEnum("galUS/lbm")]
        galUSlbm,

        /// <remarks/>
        [XmlEnum("galUS/tonUK")]
        galUStonUK,

        /// <remarks/>
        [XmlEnum("galUS/tonUS")]
        galUStonUS,

        /// <remarks/>
        [XmlEnum("L/100kg")]
        L100kg,

        /// <remarks/>
        [XmlEnum("L/kg")]
        Lkg,

        /// <remarks/>
        [XmlEnum("L/t")]
        Lt,

        /// <remarks/>
        [XmlEnum("L/tonUK")]
        LtonUK,

        /// <remarks/>
        [XmlEnum("m3/g")]
        m3g,

        /// <remarks/>
        [XmlEnum("m3/t")]
        m3t,

        /// <remarks/>
        [XmlEnum("m3/tonUK")]
        m3tonUK,

        /// <remarks/>
        [XmlEnum("m3/tonUS")]
        m3tonUS,

        /// <remarks/>
        unknown,
    }


    /// <remarks/>
    [System.CodeDom.Compiler.GeneratedCodeAttribute("xsd", "2.0.50727.42")]
    
    [XmlType(Namespace = "http://www.witsml.org/schemas/140")]
    public enum RelativePowerUom
    {

        /// <remarks/>
        [XmlEnum("%")]
        Item,

        /// <remarks/>
        [XmlEnum("Btu/bhp.hr")]
        Btubhphr,

        /// <remarks/>
        [XmlEnum("W/kW")]
        WkW,

        /// <remarks/>
        [XmlEnum("W/W")]
        WW,

        /// <remarks/>
        unknown,
    }

    /// <remarks/>
    [System.CodeDom.Compiler.GeneratedCodeAttribute("xsd", "2.0.50727.42")]
    
    [XmlType(Namespace = "http://www.witsml.org/schemas/140")]
    public enum PressureUom
    {

        /// <remarks/>
        Pa,

        /// <remarks/>
        at,

        /// <remarks/>
        atm,

        /// <remarks/>
        bar,

        /// <remarks/>
        [XmlEnum("cmH2O(4degC)")]
        cmH2O4degC,

        /// <remarks/>
        [XmlEnum("dyne/cm2")]
        dynecm2,

        /// <remarks/>
        GPa,

        /// <remarks/>
        hbar,

        /// <remarks/>
        [XmlEnum("inH2O(39.2F)")]
        inH2O392F,

        /// <remarks/>
        [XmlEnum("inH2O(60F)")]
        inH2O60F,

        /// <remarks/>
        [XmlEnum("inHg(32F)")]
        inHg32F,

        /// <remarks/>
        [XmlEnum("inHg(60F)")]
        inHg60F,

        /// <remarks/>
        [XmlEnum("kgf/cm2")]
        kgfcm2,

        /// <remarks/>
        [XmlEnum("kgf/mm2")]
        kgfmm2,

        /// <remarks/>
        [XmlEnum("kN/m2")]
        kNm2,

        /// <remarks/>
        kPa,

        /// <remarks/>
        kpsi,

        /// <remarks/>
        [XmlEnum("lbf/ft2")]
        lbfft2,

        /// <remarks/>
        [XmlEnum("lbf/100ft2")]
        lbf100ft2,

        /// <remarks/>
        [XmlEnum("lbf/in2")]
        lbfin2,

        /// <remarks/>
        mbar,

        /// <remarks/>
        [XmlEnum("mmHg(0C)")]
        mmHg0C,

        /// <remarks/>
        mPa,

        /// <remarks/>
        MPa,

        /// <remarks/>
        Mpsi,

        /// <remarks/>
        [XmlEnum("N/m2")]
        Nm2,

        /// <remarks/>
        [XmlEnum("N/mm2")]
        Nmm2,

        /// <remarks/>
        [XmlEnum("Pa(g)")]
        Pag,

        /// <remarks/>
        pPa,

        /// <remarks/>
        psi,

        /// <remarks/>
        psia,

        /// <remarks/>
        psig,

        /// <remarks/>
        [XmlEnum("tonfUS/ft2")]
        tonfUSft2,

        /// <remarks/>
        [XmlEnum("tonfUS/in2")]
        tonfUSin2,

        /// <remarks/>
        torr,

        /// <remarks/>
        ubar,

        /// <remarks/>
        [XmlEnum("umHg(0C)")]
        umHg0C,

        /// <remarks/>
        uPa,

        /// <remarks/>
        upsi,

        /// <remarks/>
        unknown,
    }

    /// <remarks/>
    [System.CodeDom.Compiler.GeneratedCodeAttribute("xsd", "2.0.50727.42")]
    
    [XmlType(Namespace = "http://www.witsml.org/schemas/140")]
    public enum PowerUom
    {

        /// <remarks/>
        W,

        /// <remarks/>
        ch,

        /// <remarks/>
        CV,

        /// <remarks/>
        ehp,

        /// <remarks/>
        GW,

        /// <remarks/>
        hhp,

        /// <remarks/>
        hp,

        /// <remarks/>
        [XmlEnum("kcal/h")]
        kcalh,

        /// <remarks/>
        kW,

        /// <remarks/>
        [XmlEnum("MJ/a")]
        MJa,

        /// <remarks/>
        MW,

        /// <remarks/>
        mW,

        /// <remarks/>
        nW,

        /// <remarks/>
        [XmlEnum("ton of refrig")]
        tonofrefrig,

        /// <remarks/>
        TW,

        /// <remarks/>
        uW,

        /// <remarks/>
        unknown,
    }

    /// <remarks/>
    [System.CodeDom.Compiler.GeneratedCodeAttribute("xsd", "2.0.50727.42")]
    
    [XmlType(Namespace = "http://www.witsml.org/schemas/140")]
    public enum PerLengthUom
    {

        /// <remarks/>
        [XmlEnum("1/m")]
        Item1m,

        /// <remarks/>
        [XmlEnum("1/angstrom")]
        Item1angstrom,

        /// <remarks/>
        [XmlEnum("1/cm")]
        Item1cm,

        /// <remarks/>
        [XmlEnum("1/ft")]
        Item1ft,

        /// <remarks/>
        [XmlEnum("1/in")]
        Item1in,

        /// <remarks/>
        [XmlEnum("1/mi")]
        Item1mi,

        /// <remarks/>
        [XmlEnum("1/mm")]
        Item1mm,

        /// <remarks/>
        [XmlEnum("1/nm")]
        Item1nm,

        /// <remarks/>
        [XmlEnum("1/yd")]
        Item1yd,

        /// <remarks/>
        unknown,
    }


    /// <remarks/>
    [System.CodeDom.Compiler.GeneratedCodeAttribute("xsd", "2.0.50727.42")]
    
    [XmlType(Namespace = "http://www.witsml.org/schemas/140")]
    public enum MomentOfForceUom
    {

        /// <remarks/>
        J,

        /// <remarks/>
        [XmlEnum("dN.m")]
        dNm,

        /// <remarks/>
        [XmlEnum("daN.m")]
        daNm,

        /// <remarks/>
        [XmlEnum("ft.lbf")]
        ftlbf,

        /// <remarks/>
        [XmlEnum("kft.lbf")]
        kftlbf,

        /// <remarks/>
        [XmlEnum("kgf.m")]
        kgfm,

        /// <remarks/>
        [XmlEnum("kN.m")]
        kNm,

        /// <remarks/>
        [XmlEnum("lbf.ft")]
        lbfft,

        /// <remarks/>
        [XmlEnum("lbf.in")]
        lbfin,

        /// <remarks/>
        [XmlEnum("lbm.ft2/s2")]
        lbmft2s2,

        /// <remarks/>
        [XmlEnum("N.m")]
        Nm,

        /// <remarks/>
        [XmlEnum("pdl.ft")]
        pdlft,

        /// <remarks/>
        [XmlEnum("tonfUS.ft")]
        tonfUSft,

        /// <remarks/>
        [XmlEnum("tonfUS.mi")]
        tonfUSmi,

        /// <remarks/>
        unknown,
    }

    /// <remarks/>
    [System.CodeDom.Compiler.GeneratedCodeAttribute("xsd", "2.0.50727.42")]
    
    [XmlType(Namespace = "http://www.witsml.org/schemas/140")]
    public enum MassPerLengthUom
    {

        /// <remarks/>
        [XmlEnum("kg/m")]
        kgm,

        /// <remarks/>
        [XmlEnum("klbm/in")]
        klbmin,

        /// <remarks/>
        [XmlEnum("lbm/ft")]
        lbmft,

        /// <remarks/>
        [XmlEnum("Mg/in")]
        Mgin,

        /// <remarks/>
        [XmlEnum("kg.m/cm2")]
        kgmcm2,

        /// <remarks/>
        unknown,
    }

    /// <remarks/>
    [System.CodeDom.Compiler.GeneratedCodeAttribute("xsd", "2.0.50727.42")]
    
    [XmlType(Namespace = "http://www.witsml.org/schemas/140")]
    public enum MassUom
    {

        /// <remarks/>
        kg,

        /// <remarks/>
        ag,

        /// <remarks/>
        ct,

        /// <remarks/>
        cwtUK,

        /// <remarks/>
        cwtUS,

        /// <remarks/>
        g,

        /// <remarks/>
        grain,

        /// <remarks/>
        klbm,

        /// <remarks/>
        lbm,

        /// <remarks/>
        Mg,

        /// <remarks/>
        mg,

        /// <remarks/>
        [XmlEnum("oz(av)")]
        ozav,

        /// <remarks/>
        [XmlEnum("oz(troy)")]
        oztroy,

        /// <remarks/>
        ozm,

        /// <remarks/>
        sack94,

        /// <remarks/>
        t,

        /// <remarks/>
        tonUK,

        /// <remarks/>
        tonUS,

        /// <remarks/>
        ug,

        /// <remarks/>
        unknown,
    }

    /// <remarks/>
    [System.CodeDom.Compiler.GeneratedCodeAttribute("xsd", "2.0.50727.42")]
    
    [XmlType(Namespace = "http://www.witsml.org/schemas/140")]
    public enum MassConcentrationUom
    {

        /// <remarks/>
        Euc,

        /// <remarks/>
        [XmlEnum("%")]
        Item,

        /// <remarks/>
        [XmlEnum("g/kg")]
        gkg,

        /// <remarks/>
        [XmlEnum("kg/kg")]
        kgkg,

        /// <remarks/>
        [XmlEnum("kg/sack94")]
        kgsack94,

        /// <remarks/>
        [XmlEnum("mg/kg")]
        mgkg,

        /// <remarks/>
        permil,

        /// <remarks/>
        ppdk,

        /// <remarks/>
        ppk,

        /// <remarks/>
        ppm,

        /// <remarks/>
        unknown,
    }

    /// <remarks/>
    [System.CodeDom.Compiler.GeneratedCodeAttribute("xsd", "2.0.50727.42")]
    
    [XmlType(Namespace = "http://www.witsml.org/schemas/140")]
    public enum MagneticInductionUom
    {

        /// <remarks/>
        T,

        /// <remarks/>
        gauss,

        /// <remarks/>
        mT,

        /// <remarks/>
        mgauss,

        /// <remarks/>
        nT,

        /// <remarks/>
        uT,

        /// <remarks/>
        unknown,
    }

    /// <remarks/>
    [System.CodeDom.Compiler.GeneratedCodeAttribute("xsd", "2.0.50727.42")]
    
    [XmlType(Namespace = "http://www.witsml.org/schemas/140")]
    public enum LengthPerLengthUom
    {

        /// <remarks/>
        [XmlEnum("%")]
        Item,

        /// <remarks/>
        [XmlEnum("ft/100ft")]
        ft100ft,

        /// <remarks/>
        [XmlEnum("ft/ft")]
        ftft,

        /// <remarks/>
        [XmlEnum("ft/in")]
        ftin,

        /// <remarks/>
        [XmlEnum("ft/m")]
        ftm,

        /// <remarks/>
        [XmlEnum("ft/mi")]
        ftmi,

        /// <remarks/>
        [XmlEnum("km/cm")]
        kmcm,

        /// <remarks/>
        [XmlEnum("m/30m")]
        m30m,

        /// <remarks/>
        [XmlEnum("m/cm")]
        mcm,

        /// <remarks/>
        [XmlEnum("m/km")]
        mkm,

        /// <remarks/>
        [XmlEnum("m/m")]
        mm,

        /// <remarks/>
        [XmlEnum("mi/in")]
        miin,

        /// <remarks/>
        unknown,
    }

    /// <remarks/>
    [System.CodeDom.Compiler.GeneratedCodeAttribute("xsd", "2.0.50727.42")]
    
    [XmlType(Namespace = "http://www.witsml.org/schemas/140")]
    public enum IlluminanceUom
    {

        /// <remarks/>
        lx,

        /// <remarks/>
        [XmlEnum("lm/m2")]
        lmm2,

        /// <remarks/>
        footcandle,

        /// <remarks/>
        klx,

        /// <remarks/>
        unknown,
    }

    /// <remarks/>
    [System.CodeDom.Compiler.GeneratedCodeAttribute("xsd", "2.0.50727.42")]
    
    [XmlType(Namespace = "http://www.witsml.org/schemas/140")]
    public enum ForcePerVolumeUom
    {

        /// <remarks/>
        [XmlEnum("N/m3")]
        Nm3,

        /// <remarks/>
        [XmlEnum("atm/100m")]
        atm100m,

        /// <remarks/>
        [XmlEnum("atm/m")]
        atmm,

        /// <remarks/>
        [XmlEnum("bar/km")]
        barkm,

        /// <remarks/>
        [XmlEnum("bar/m")]
        barm,

        /// <remarks/>
        [XmlEnum("GPa/cm")]
        GPacm,

        /// <remarks/>
        [XmlEnum("kPa/100m")]
        kPa100m,

        /// <remarks/>
        [XmlEnum("kPa/m")]
        kPam,

        /// <remarks/>
        [XmlEnum("lbf/ft3")]
        lbfft3,

        /// <remarks/>
        [XmlEnum("lbf/galUS")]
        lbfgalUS,

        /// <remarks/>
        [XmlEnum("MPa/m")]
        MPam,

        /// <remarks/>
        [XmlEnum("psi/ft")]
        psift,

        /// <remarks/>
        [XmlEnum("psi/100ft")]
        psi100ft,

        /// <remarks/>
        [XmlEnum("psi/kft")]
        psikft,

        /// <remarks/>
        [XmlEnum("psi/m")]
        psim,

        /// <remarks/>
        [XmlEnum("Pa/m")]
        Pam,

        /// <remarks/>
        [XmlEnum("atm/ft")]
        atmft,

        /// <remarks/>
        unknown,
    }

    /// <remarks/>
    [System.CodeDom.Compiler.GeneratedCodeAttribute("xsd", "2.0.50727.42")]
    
    [XmlType(Namespace = "http://www.witsml.org/schemas/140")]
    public enum ForcePerLengthUom
    {

        /// <remarks/>
        [XmlEnum("N/30m")]
        N30m,

        /// <remarks/>
        [XmlEnum("N/m")]
        Nm,

        /// <remarks/>
        [XmlEnum("dyne/cm")]
        dynecm,

        /// <remarks/>
        [XmlEnum("kN/m")]
        kNm,

        /// <remarks/>
        [XmlEnum("kgf/cm")]
        kgfcm,

        /// <remarks/>
        [XmlEnum("lbf/100ft")]
        lbf100ft,

        /// <remarks/>
        [XmlEnum("lbf/30m")]
        lbf30m,

        /// <remarks/>
        [XmlEnum("lbf/ft")]
        lbfft,

        /// <remarks/>
        [XmlEnum("lbf/in")]
        lbfin,

        /// <remarks/>
        [XmlEnum("mN/km")]
        mNkm,

        /// <remarks/>
        [XmlEnum("mN/m")]
        mNm,

        /// <remarks/>
        [XmlEnum("pdl/cm")]
        pdlcm,

        /// <remarks/>
        [XmlEnum("tonfUK/ft")]
        tonfUKft,

        /// <remarks/>
        [XmlEnum("tonfUS/ft")]
        tonfUSft,

        /// <remarks/>
        unknown,
    }

    /// <remarks/>
    [System.CodeDom.Compiler.GeneratedCodeAttribute("xsd", "2.0.50727.42")]
    
    [XmlType(Namespace = "http://www.witsml.org/schemas/140")]
    public enum ForceUom
    {

        /// <remarks/>
        N,

        /// <remarks/>
        daN,

        /// <remarks/>
        dyne,

        /// <remarks/>
        gf,

        /// <remarks/>
        kdyne,

        /// <remarks/>
        kgf,

        /// <remarks/>
        klbf,

        /// <remarks/>
        kN,

        /// <remarks/>
        lbf,

        /// <remarks/>
        Mgf,

        /// <remarks/>
        mN,

        /// <remarks/>
        MN,

        /// <remarks/>
        ozf,

        /// <remarks/>
        pdl,

        /// <remarks/>
        tonfUK,

        /// <remarks/>
        tonfUS,

        /// <remarks/>
        uN,

        /// <remarks/>
        unknown,
    }


    /// <remarks/>
    [System.CodeDom.Compiler.GeneratedCodeAttribute("xsd", "2.0.50727.42")]
    
    [XmlType(Namespace = "http://www.witsml.org/schemas/140")]
    public enum EquivalentPerMassUom
    {

        /// <remarks/>
        [XmlEnum("eq/kg")]
        eqkg,

        /// <remarks/>
        [XmlEnum("meq/g")]
        meqg,

        /// <remarks/>
        [XmlEnum("meq/100g")]
        meq100g,

        /// <remarks/>
        unknown,
    }

    /// <remarks/>
    [System.CodeDom.Compiler.GeneratedCodeAttribute("xsd", "2.0.50727.42")]
    
    [XmlType(Namespace = "http://www.witsml.org/schemas/140")]
    public enum ElectricPotentialUom
    {

        /// <remarks/>
        V,

        /// <remarks/>
        kV,

        /// <remarks/>
        mV,

        /// <remarks/>
        MV,

        /// <remarks/>
        uV,

        /// <remarks/>
        unknown,
    }

    /// <remarks/>
    [System.CodeDom.Compiler.GeneratedCodeAttribute("xsd", "2.0.50727.42")]
    
    [XmlType(Namespace = "http://www.witsml.org/schemas/140")]
    public enum ElectricCurrentUom
    {

        /// <remarks/>
        A,

        /// <remarks/>
        MA,

        /// <remarks/>
        kA,

        /// <remarks/>
        mA,

        /// <remarks/>
        nA,

        /// <remarks/>
        pA,

        /// <remarks/>
        uA,

        /// <remarks/>
        unknown,
    }

    /// <remarks/>
    [System.CodeDom.Compiler.GeneratedCodeAttribute("xsd", "2.0.50727.42")]
    
    [XmlType(Namespace = "http://www.witsml.org/schemas/140")]
    public enum DynamicViscosityUom
    {

        /// <remarks/>
        [XmlEnum("Pa.s")]
        Pas,

        /// <remarks/>
        cP,

        /// <remarks/>
        P,

        /// <remarks/>
        [XmlEnum("psi.s")]
        psis,

        /// <remarks/>
        [XmlEnum("dyne.s/cm2")]
        dynescm2,

        /// <remarks/>
        [XmlEnum("kgf.s/m2")]
        kgfsm2,

        /// <remarks/>
        [XmlEnum("lbf.s/ft2")]
        lbfsft2,

        /// <remarks/>
        [XmlEnum("lbf.s/in2")]
        lbfsin2,

        /// <remarks/>
        [XmlEnum("mPa.s")]
        mPas,

        /// <remarks/>
        [XmlEnum("N.s/m2")]
        Nsm2,

        /// <remarks/>
        unknown,
    }

    /// <remarks/>
    [System.CodeDom.Compiler.GeneratedCodeAttribute("xsd", "2.0.50727.42")]
    
    [XmlType(Namespace = "http://www.witsml.org/schemas/140")]
    public enum DimensionlessUom
    {

        /// <remarks/>
        Euc,

        /// <remarks/>
        [XmlEnum("%")]
        Item,

        /// <remarks/>
        cEuc,

        /// <remarks/>
        mEuc,

        /// <remarks/>
        nEuc,

        /// <remarks/>
        uEuc,

        /// <remarks/>
        unknown,
    }

    /// <remarks/>
    [System.CodeDom.Compiler.GeneratedCodeAttribute("xsd", "2.0.50727.42")]
    
    [XmlType(Namespace = "http://www.witsml.org/schemas/140")]
    public enum DensityUom
    {

        /// <remarks/>
        [XmlEnum("kg/m3")]
        kgm3,

        /// <remarks/>
        [XmlEnum("10Mg/m3")]
        Item10Mgm3,

        /// <remarks/>
        dAPI,

        /// <remarks/>
        [XmlEnum("g/cm3")]
        gcm3,

        /// <remarks/>
        [XmlEnum("g/dm3")]
        gdm3,

        /// <remarks/>
        [XmlEnum("g/galUK")]
        ggalUK,

        /// <remarks/>
        [XmlEnum("g/galUS")]
        ggalUS,

        /// <remarks/>
        [XmlEnum("g/L")]
        gL,

        /// <remarks/>
        [XmlEnum("g/m3")]
        gm3,

        /// <remarks/>
        [XmlEnum("grain/ft3")]
        grainft3,

        /// <remarks/>
        [XmlEnum("grain/galUS")]
        graingalUS,

        /// <remarks/>
        [XmlEnum("grain/100ft3")]
        grain100ft3,

        /// <remarks/>
        [XmlEnum("kg/dm3")]
        kgdm3,

        /// <remarks/>
        [XmlEnum("kg/L")]
        kgL,

        /// <remarks/>
        [XmlEnum("Mg/m3")]
        Mgm3,

        /// <remarks/>
        [XmlEnum("lbm/10bbl")]
        lbm10bbl,

        /// <remarks/>
        [XmlEnum("lbm/bbl")]
        lbmbbl,

        /// <remarks/>
        [XmlEnum("lbm/ft3")]
        lbmft3,

        /// <remarks/>
        [XmlEnum("lbm/galUK")]
        lbmgalUK,

        /// <remarks/>
        [XmlEnum("lbm/1000galUK")]
        lbm1000galUK,

        /// <remarks/>
        [XmlEnum("lbm/galUS")]
        lbmgalUS,

        /// <remarks/>
        [XmlEnum("lbm/1000galUS")]
        lbm1000galUS,

        /// <remarks/>
        [XmlEnum("lbm/in3")]
        lbmin3,

        /// <remarks/>
        [XmlEnum("lbm/Mbbl")]
        lbmMbbl,

        /// <remarks/>
        [XmlEnum("mg/dm3")]
        mgdm3,

        /// <remarks/>
        [XmlEnum("mg/galUS")]
        mggalUS,

        /// <remarks/>
        [XmlEnum("mg/L")]
        mgL,

        /// <remarks/>
        [XmlEnum("mg/m3")]
        mgm3,

        /// <remarks/>
        [XmlEnum("ug/cm3")]
        ugcm3,

        /// <remarks/>
        unknown,
    }

    /// <remarks/>
    [System.CodeDom.Compiler.GeneratedCodeAttribute("xsd", "2.0.50727.42")]
    
    [XmlType(Namespace = "http://www.witsml.org/schemas/140")]
    public enum AreaPerAreaUom
    {

        /// <remarks/>
        Euc,

        /// <remarks/>
        [XmlEnum("%")]
        Item,

        /// <remarks/>
        [XmlEnum("in2/ft2")]
        in2ft2,

        /// <remarks/>
        [XmlEnum("in2/in2")]
        in2in2,

        /// <remarks/>
        [XmlEnum("m2/m2")]
        m2m2,

        /// <remarks/>
        [XmlEnum("mm2/mm2")]
        mm2mm2,

        /// <remarks/>
        unknown,
    }

    /// <remarks/>
    [System.CodeDom.Compiler.GeneratedCodeAttribute("xsd", "2.0.50727.42")]
    
    [XmlType(Namespace = "http://www.witsml.org/schemas/140")]
    public enum AreaUom
    {

        /// <remarks/>
        m2,

        /// <remarks/>
        acre,

        /// <remarks/>
        b,

        /// <remarks/>
        cm2,

        /// <remarks/>
        ft2,

        /// <remarks/>
        ha,

        /// <remarks/>
        in2,

        /// <remarks/>
        km2,

        /// <remarks/>
        mi2,

        /// <remarks/>
        miUS2,

        /// <remarks/>
        mm2,

        /// <remarks/>
        um2,

        /// <remarks/>
        yd2,

        /// <remarks/>
        unknown,
    }

    /// <remarks/>
    [System.CodeDom.Compiler.GeneratedCodeAttribute("xsd", "2.0.50727.42")]
    
    [XmlType(Namespace = "http://www.witsml.org/schemas/140")]
    public enum AnglePerTimeUom
    {

        /// <remarks/>
        [XmlEnum("rad/s")]
        rads,

        /// <remarks/>
        [XmlEnum("c/s")]
        cs,

        /// <remarks/>
        [XmlEnum("dega/h")]
        degah,

        /// <remarks/>
        [XmlEnum("dega/min")]
        degamin,

        /// <remarks/>
        [XmlEnum("dega/s")]
        degas,

        /// <remarks/>
        [XmlEnum("rev/s")]
        revs,

        /// <remarks/>
        rpm,

        /// <remarks/>
        unknown,
    }

    /// <remarks/>
    [System.CodeDom.Compiler.GeneratedCodeAttribute("xsd", "2.0.50727.42")]
    
    [XmlType(Namespace = "http://www.witsml.org/schemas/140")]
    public enum AnglePerLengthUom
    {

        /// <remarks/>
        [XmlEnum("rad/m")]
        radm,

        /// <remarks/>
        [XmlEnum("dega/30ft")]
        dega30ft,

        /// <remarks/>
        [XmlEnum("dega/ft")]
        degaft,

        /// <remarks/>
        [XmlEnum("dega/100ft")]
        dega100ft,

        /// <remarks/>
        [XmlEnum("dega/m")]
        degam,

        /// <remarks/>
        [XmlEnum("dega/30m")]
        dega30m,

        /// <remarks/>
        [XmlEnum("rad/ft")]
        radft,

        /// <remarks/>
        unknown,
    }
    /// <remarks/>
    [System.CodeDom.Compiler.GeneratedCodeAttribute("xsd", "2.0.50727.42")]
    
    [XmlType(Namespace = "http://www.witsml.org/schemas/140")]
    public enum AccelerationLinearUom
    {

        /// <remarks/>
        [XmlEnum("m/s2")]
        ms2,

        /// <remarks/>
        [XmlEnum("cm/s2")]
        cms2,

        /// <remarks/>
        [XmlEnum("ft/s2")]
        fts2,

        /// <remarks/>
        Gal,

        /// <remarks/>
        mgn,

        /// <remarks/>
        gn,

        /// <remarks/>
        mGal,

        /// <remarks/>
        unknown,
    }
}
