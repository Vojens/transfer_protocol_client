﻿ 

namespace Witsml.Objects140
{
    using System.Xml.Serialization;


    /// <remarks/>
    [System.CodeDom.Compiler.GeneratedCodeAttribute("xsd", "2.0.50727.42")]
    
    [XmlType(Namespace = "http://www.witsml.org/schemas/140", IncludeInSchema = false)]
    public enum ItemsChoiceType
    {

        /// <remarks/>
        easting,

        /// <remarks/>
        latitude,

        /// <remarks/>
        localX,

        /// <remarks/>
        localY,

        /// <remarks/>
        longitude,

        /// <remarks/>
        northing,

        /// <remarks/>
        projectedX,

        /// <remarks/>
        projectedY,

        /// <remarks/>
        southing,

        /// <remarks/>
        westing,
    }

    /// <remarks/>
    [System.CodeDom.Compiler.GeneratedCodeAttribute("xsd", "2.0.50727.42")]
    
    [XmlType(Namespace = "http://www.witsml.org/schemas/140")]
    public enum ItemState
    {

        /// <remarks/>
        actual,

        /// <remarks/>
        model,

        /// <remarks/>
        plan,

        /// <remarks/>
        unknown,
    }

    /// <remarks/>
    [System.CodeDom.Compiler.GeneratedCodeAttribute("xsd", "2.0.50727.42")]
    
    [XmlType(Namespace = "http://www.witsml.org/schemas/140")]
    public enum ServiceCategory
    {

        /// <remarks/>
        [XmlEnum("mud log service")]
        mudlogservice,

        /// <remarks/>
        [XmlEnum("cement service")]
        cementservice,

        /// <remarks/>
        [XmlEnum("LWD service")]
        LWDservice,

        /// <remarks/>
        [XmlEnum("rig service")]
        rigservice,

        /// <remarks/>
        [XmlEnum("drilling service")]
        drillingservice,

        /// <remarks/>
        unknown,
    }


    /// <remarks/>
    [System.CodeDom.Compiler.GeneratedCodeAttribute("xsd", "2.0.50727.42")]
    
    [XmlType(Namespace = "http://www.witsml.org/schemas/140")]
    public enum GeodeticDatum
    {

        /// <remarks/>
        ADND,

        /// <remarks/>
        ARC50,

        /// <remarks/>
        AUSG,

        /// <remarks/>
        CAA,

        /// <remarks/>
        CHAS,

        /// <remarks/>
        CORAL,

        /// <remarks/>
        ED50,

        /// <remarks/>
        ED87,

        /// <remarks/>
        ERIN65,

        /// <remarks/>
        GD49,

        /// <remarks/>
        GHANA,

        /// <remarks/>
        GUAM63,

        /// <remarks/>
        HJRS55,

        /// <remarks/>
        HTS,

        /// <remarks/>
        INCH,

        /// <remarks/>
        INDIA1,

        /// <remarks/>
        INDIA2,

        /// <remarks/>
        INDNS74,

        /// <remarks/>
        LIB64,

        /// <remarks/>
        LUZON,

        /// <remarks/>
        MRCH,

        /// <remarks/>
        NAD27,

        /// <remarks/>
        NAD83,

        /// <remarks/>
        NGRA,

        /// <remarks/>
        None,

        /// <remarks/>
        NPRM,

        /// <remarks/>
        OSGB36,

        /// <remarks/>
        POTS1,

        /// <remarks/>
        PULK1,

        /// <remarks/>
        PULK2,

        /// <remarks/>
        QRNQ,

        /// <remarks/>
        SA56,

        /// <remarks/>
        SRL60,

        /// <remarks/>
        TNRV25,

        /// <remarks/>
        TOKYO,

        /// <remarks/>
        UserDefined,

        /// <remarks/>
        VROL,

        /// <remarks/>
        WGS72,

        /// <remarks/>
        WGS84,

        /// <remarks/>
        YACR,

        /// <remarks/>
        unknown,
    }

    /// <remarks/>
    [System.CodeDom.Compiler.GeneratedCodeAttribute("xsd", "2.0.50727.42")]
    
    [XmlType(Namespace = "http://www.witsml.org/schemas/140")]
    public enum Ellipsoid
    {

        /// <remarks/>
        AGD66,

        /// <remarks/>
        AIRY_MOD,

        /// <remarks/>
        AIRY30,

        /// <remarks/>
        AIRY49,

        /// <remarks/>
        AUST_NAT,

        /// <remarks/>
        [XmlEnum("BESL-DHD")]
        BESLDHD,

        /// <remarks/>
        [XmlEnum("BESL-NGL")]
        BESLNGL,

        /// <remarks/>
        [XmlEnum("BESL-RT9")]
        BESLRT9,

        /// <remarks/>
        BESS41,

        /// <remarks/>
        BESSNAM,

        /// <remarks/>
        BOGOTA,

        /// <remarks/>
        CL58,

        /// <remarks/>
        [XmlEnum("CL58-1")]
        CL581,

        /// <remarks/>
        CL66,

        /// <remarks/>
        [XmlEnum("CL66-M")]
        CL66M,

        /// <remarks/>
        CL80,

        /// <remarks/>
        [XmlEnum("CL80-A")]
        CL80A,

        /// <remarks/>
        [XmlEnum("CL80-B")]
        CL80B,

        /// <remarks/>
        [XmlEnum("CL80-I")]
        CL80I,

        /// <remarks/>
        [XmlEnum("CL80-J")]
        CL80J,

        /// <remarks/>
        [XmlEnum("CL80-M")]
        CL80M,

        /// <remarks/>
        [XmlEnum("CL80-P")]
        CL80P,

        /// <remarks/>
        CMPOINCH,

        /// <remarks/>
        DAN,

        /// <remarks/>
        DELA,

        /// <remarks/>
        ED50,

        /// <remarks/>
        EGYPT07,

        /// <remarks/>
        EVER,

        /// <remarks/>
        EVER48,

        /// <remarks/>
        EVER56,

        /// <remarks/>
        EVER69,

        /// <remarks/>
        [XmlEnum("EVER-BR")]
        EVERBR,

        /// <remarks/>
        EVERMOD,

        /// <remarks/>
        [XmlEnum("EVER-P")]
        EVERP,

        /// <remarks/>
        [XmlEnum("EVER-TM")]
        EVERTM,

        /// <remarks/>
        EVTM,

        /// <remarks/>
        FISC60,

        /// <remarks/>
        FISC60MOD,

        /// <remarks/>
        FISC68,

        /// <remarks/>
        FISCMOD,

        /// <remarks/>
        GDA94,

        /// <remarks/>
        GRS67,

        /// <remarks/>
        GRS80,

        /// <remarks/>
        HAY09,

        /// <remarks/>
        HEIS,

        /// <remarks/>
        HEL06,

        /// <remarks/>
        HEL07,

        /// <remarks/>
        HOUG,

        /// <remarks/>
        [XmlEnum("IAG-75")]
        IAG75,

        /// <remarks/>
        INDIAN75,

        /// <remarks/>
        [XmlEnum("INDO-74")]
        INDO74,

        /// <remarks/>
        INT24,

        /// <remarks/>
        IUGG67,

        /// <remarks/>
        IUGG75,

        /// <remarks/>
        JEFF48,

        /// <remarks/>
        KAU63,

        /// <remarks/>
        KRSV,

        /// <remarks/>
        MERIT83,

        /// <remarks/>
        NAD27,

        /// <remarks/>
        NAHRAN,

        /// <remarks/>
        NEWINT67,

        /// <remarks/>
        [XmlEnum("NWL-10D")]
        NWL10D,

        /// <remarks/>
        [XmlEnum("NWL-9D")]
        NWL9D,

        /// <remarks/>
        OSGB36,

        /// <remarks/>
        OSU86F,

        /// <remarks/>
        OSU91A,

        /// <remarks/>
        [XmlEnum("PLESSIS-1817")]
        PLESSIS1817,

        /// <remarks/>
        PSAD56,

        /// <remarks/>
        PTNOIRE,

        /// <remarks/>
        SA69,

        /// <remarks/>
        SPHR,

        /// <remarks/>
        STRU,

        /// <remarks/>
        WALB,

        /// <remarks/>
        WAR24,

        /// <remarks/>
        WGS60,

        /// <remarks/>
        WGS66,

        /// <remarks/>
        WGS72,

        /// <remarks/>
        WGS84,

        /// <remarks/>
        unknown,
    }

    /// <remarks/>
    [System.CodeDom.Compiler.GeneratedCodeAttribute("xsd", "2.0.50727.42")]
    
    [XmlType(Namespace = "http://www.witsml.org/schemas/140")]
    public enum Projection
    {

        /// <remarks/>
        [XmlEnum("Albers equal area")]
        Albersequalarea,

        /// <remarks/>
        [XmlEnum("azimuthal equidistant")]
        azimuthalequidistant,

        /// <remarks/>
        Cassini,

        /// <remarks/>
        [XmlEnum("equidistant conic")]
        equidistantconic,

        /// <remarks/>
        equirectangular,

        /// <remarks/>
        gnomonic,

        /// <remarks/>
        [XmlEnum("Lambert azimuthal")]
        Lambertazimuthal,

        /// <remarks/>
        [XmlEnum("Lambert conformal conic")]
        Lambertconformalconic,

        /// <remarks/>
        Mercator,

        /// <remarks/>
        Miller,

        /// <remarks/>
        [XmlEnum("oblique Mercator")]
        obliqueMercator,

        /// <remarks/>
        orthographic,

        /// <remarks/>
        perspective,

        /// <remarks/>
        [XmlEnum("polar stereographic")]
        polarstereographic,

        /// <remarks/>
        polyconic,

        /// <remarks/>
        sinusoidal,

        /// <remarks/>
        [XmlEnum("state plane")]
        stateplane,

        /// <remarks/>
        stereographic,

        /// <remarks/>
        [XmlEnum("transverse Mercator")]
        transverseMercator,

        /// <remarks/>
        [XmlEnum("universal transverse Mercator")]
        universaltransverseMercator,

        /// <remarks/>
        [XmlEnum("user defined")]
        userdefined,

        /// <remarks/>
        [XmlEnum("Van der Grinten")]
        VanderGrinten,

        /// <remarks/>
        unknown,
    }

    /// <remarks/>
    [System.CodeDom.Compiler.GeneratedCodeAttribute("xsd", "2.0.50727.42")]
    
    [XmlType(Namespace = "http://www.witsml.org/schemas/140")]
    public enum ProjectionVariantsObliqueMercator
    {

        /// <remarks/>
        @default,

        /// <remarks/>
        rectified,

        /// <remarks/>
        [XmlEnum("rectified skew")]
        rectifiedskew,

        /// <remarks/>
        [XmlEnum("rectified skew center origin")]
        rectifiedskewcenterorigin,

        /// <remarks/>
        unknown,
    }

    /// <remarks/>
    [System.CodeDom.Compiler.GeneratedCodeAttribute("xsd", "2.0.50727.42")]
    
    [XmlType(Namespace = "http://www.witsml.org/schemas/140")]
    public enum NADTypes
    {

        /// <remarks/>
        NAD27,

        /// <remarks/>
        NAD83,

        /// <remarks/>
        unknown,
    }

    /// <remarks/>
    [System.CodeDom.Compiler.GeneratedCodeAttribute("xsd", "2.0.50727.42")]
    
    [XmlType(Namespace = "http://www.witsml.org/schemas/140")]
    public enum Hemispheres
    {

        /// <remarks/>
        northern,

        /// <remarks/>
        southern,

        /// <remarks/>
        unknown,
    }

    /// <remarks/>
    [System.CodeDom.Compiler.GeneratedCodeAttribute("xsd", "2.0.50727.42")]
    
    [XmlType(Namespace = "http://www.witsml.org/schemas/140")]
    public enum PrincipalMeridian
    {

        /// <remarks/>
        [XmlEnum("1st Principal Meridian")]
        Item1stPrincipalMeridian,

        /// <remarks/>
        [XmlEnum("2nd Principal Meridian")]
        Item2ndPrincipalMeridian,

        /// <remarks/>
        [XmlEnum("3rd Principal Meridian")]
        Item3rdPrincipalMeridian,

        /// <remarks/>
        [XmlEnum("4th Principal Meridian")]
        Item4thPrincipalMeridian,

        /// <remarks/>
        [XmlEnum("5th Principal Meridian")]
        Item5thPrincipalMeridian,

        /// <remarks/>
        [XmlEnum("6th Principal Meridian")]
        Item6thPrincipalMeridian,

        /// <remarks/>
        [XmlEnum("Black Hills Meridian")]
        BlackHillsMeridian,

        /// <remarks/>
        [XmlEnum("Boise Meridian")]
        BoiseMeridian,

        /// <remarks/>
        [XmlEnum("Choctaw Meridian")]
        ChoctawMeridian,

        /// <remarks/>
        [XmlEnum("Chickasaw Meridian")]
        ChickasawMeridian,

        /// <remarks/>
        [XmlEnum("Cimarron Meridian")]
        CimarronMeridian,

        /// <remarks/>
        [XmlEnum("Copper River Meridian")]
        CopperRiverMeridian,

        /// <remarks/>
        [XmlEnum("Fairbanks Meridian")]
        FairbanksMeridian,

        /// <remarks/>
        [XmlEnum("Gila and Salt River Meridian")]
        GilaandSaltRiverMeridian,

        /// <remarks/>
        [XmlEnum("Humboldt Meridian")]
        HumboldtMeridian,

        /// <remarks/>
        [XmlEnum("Huntsville Meridian")]
        HuntsvilleMeridian,

        /// <remarks/>
        [XmlEnum("Indian Meridian")]
        IndianMeridian,

        /// <remarks/>
        [XmlEnum("Kateel River Meridian")]
        KateelRiverMeridian,

        /// <remarks/>
        [XmlEnum("Lousiana Meridian")]
        LousianaMeridian,

        /// <remarks/>
        [XmlEnum("Michigan Meridian")]
        MichiganMeridian,

        /// <remarks/>
        [XmlEnum("Mount Diablo Meridian")]
        MountDiabloMeridian,

        /// <remarks/>
        [XmlEnum("New Mexico Meridian")]
        NewMexicoMeridian,

        /// <remarks/>
        [XmlEnum("Saint Stephens Meridian")]
        SaintStephensMeridian,

        /// <remarks/>
        [XmlEnum("Saint Helena Meridian")]
        SaintHelenaMeridian,

        /// <remarks/>
        [XmlEnum("Salt Lake Meridian")]
        SaltLakeMeridian,

        /// <remarks/>
        [XmlEnum("San Bernardo Meridian")]
        SanBernardoMeridian,

        /// <remarks/>
        [XmlEnum("Seward Meridian")]
        SewardMeridian,

        /// <remarks/>
        [XmlEnum("Tallahassee Meridian")]
        TallahasseeMeridian,

        /// <remarks/>
        [XmlEnum("Uintah Meridian")]
        UintahMeridian,

        /// <remarks/>
        [XmlEnum("Umiat Meridian")]
        UmiatMeridian,

        /// <remarks/>
        [XmlEnum("Ute Meridian")]
        UteMeridian,

        /// <remarks/>
        [XmlEnum("Washington Meridian")]
        WashingtonMeridian,

        /// <remarks/>
        [XmlEnum("Williamette Meridian")]
        WilliametteMeridian,

        /// <remarks/>
        [XmlEnum("Wind River Meridian")]
        WindRiverMeridian,

        /// <remarks/>
        unknown,
    }

    /// <remarks/>
    [System.CodeDom.Compiler.GeneratedCodeAttribute("xsd", "2.0.50727.42")]
    
    [XmlType(Namespace = "http://www.witsml.org/schemas/140")]
    public enum ElevCodeEnum
    {

        /// <remarks/>
        CF,

        /// <remarks/>
        CV,

        /// <remarks/>
        DF,

        /// <remarks/>
        GL,

        /// <remarks/>
        KB,

        /// <remarks/>
        RB,

        /// <remarks/>
        RT,

        /// <remarks/>
        SF,

        /// <remarks/>
        LAT,

        /// <remarks/>
        SL,

        /// <remarks/>
        MHHW,

        /// <remarks/>
        MHW,

        /// <remarks/>
        MLLW,

        /// <remarks/>
        MLW,

        /// <remarks/>
        MTL,

        /// <remarks/>
        KO,

        /// <remarks/>
        unknown,
    }

    /// <remarks/>
    [System.CodeDom.Compiler.GeneratedCodeAttribute("xsd", "2.0.50727.42")]
    
    [XmlType(Namespace = "http://www.witsml.org/schemas/140")]
    public enum WellStatus
    {

        /// <remarks/>
        abandoned,

        /// <remarks/>
        active,

        /// <remarks/>
        [XmlEnum("active -- injecting")]
        activeinjecting,

        /// <remarks/>
        [XmlEnum("active -- producing")]
        activeproducing,

        /// <remarks/>
        completed,

        /// <remarks/>
        drilling,

        /// <remarks/>
        [XmlEnum("partially plugged")]
        partiallyplugged,

        /// <remarks/>
        permitted,

        /// <remarks/>
        [XmlEnum("plugged and abandoned")]
        pluggedandabandoned,

        /// <remarks/>
        proposed,

        /// <remarks/>
        sold,

        /// <remarks/>
        suspended,

        /// <remarks/>
        [XmlEnum("temporarily abandoned")]
        temporarilyabandoned,

        /// <remarks/>
        testing,

        /// <remarks/>
        tight,

        /// <remarks/>
        [XmlEnum("working over")]
        workingover,

        /// <remarks/>
        unknown,
    }

    /// <remarks/>
    [System.CodeDom.Compiler.GeneratedCodeAttribute("xsd", "2.0.50727.42")]
    
    [XmlType(Namespace = "http://www.witsml.org/schemas/140")]
    public enum WellPurpose
    {

        /// <remarks/>
        appraisal,

        /// <remarks/>
        [XmlEnum("appraisal -- confirmation appraisal")]
        appraisalconfirmationappraisal,

        /// <remarks/>
        [XmlEnum("appraisal -- exploratory appraisal")]
        appraisalexploratoryappraisal,

        /// <remarks/>
        exploration,

        /// <remarks/>
        [XmlEnum("exploration -- deeper-pool wildcat")]
        explorationdeeperpoolwildcat,

        /// <remarks/>
        [XmlEnum("exploration -- new-field wildcat")]
        explorationnewfieldwildcat,

        /// <remarks/>
        [XmlEnum("exploration -- new-pool wildcat")]
        explorationnewpoolwildcat,

        /// <remarks/>
        [XmlEnum("exploration -- outpost wildcat")]
        explorationoutpostwildcat,

        /// <remarks/>
        [XmlEnum("exploration -- shallower-pool wildcat")]
        explorationshallowerpoolwildcat,

        /// <remarks/>
        development,

        /// <remarks/>
        [XmlEnum("development -- infill development")]
        developmentinfilldevelopment,

        /// <remarks/>
        [XmlEnum("development -- injector")]
        developmentinjector,

        /// <remarks/>
        [XmlEnum("development -- producer")]
        developmentproducer,

        /// <remarks/>
        [XmlEnum("fluid storage")]
        fluidstorage,

        /// <remarks/>
        [XmlEnum("fluid storage -- gas storage")]
        fluidstoragegasstorage,

        /// <remarks/>
        [XmlEnum("general srvc")]
        generalsrvc,

        /// <remarks/>
        [XmlEnum("general srvc -- borehole re-acquisition")]
        generalsrvcboreholereacquisition,

        /// <remarks/>
        [XmlEnum("general srvc -- observation")]
        generalsrvcobservation,

        /// <remarks/>
        [XmlEnum("general srvc -- relief")]
        generalsrvcrelief,

        /// <remarks/>
        [XmlEnum("general srvc -- research")]
        generalsrvcresearch,

        /// <remarks/>
        [XmlEnum("general srvc -- research -- drill test")]
        generalsrvcresearchdrilltest,

        /// <remarks/>
        [XmlEnum("general srvc -- research -- strat test")]
        generalsrvcresearchstrattest,

        /// <remarks/>
        [XmlEnum("general srvc -- waste disposal")]
        generalsrvcwastedisposal,

        /// <remarks/>
        mineral,

        /// <remarks/>
        unknown,
    }

    /// <remarks/>
    [System.CodeDom.Compiler.GeneratedCodeAttribute("xsd", "2.0.50727.42")]
    
    [XmlType(Namespace = "http://www.witsml.org/schemas/140")]
    public enum WellFluid
    {

        /// <remarks/>
        air,

        /// <remarks/>
        condensate,

        /// <remarks/>
        dry,

        /// <remarks/>
        gas,

        /// <remarks/>
        [XmlEnum("gas-water")]
        gaswater,

        /// <remarks/>
        [XmlEnum("non HC gas")]
        nonHCgas,

        /// <remarks/>
        [XmlEnum("non HC gas -- CO2")]
        nonHCgasCO2,

        /// <remarks/>
        oil,

        /// <remarks/>
        [XmlEnum("oil-gas")]
        oilgas,

        /// <remarks/>
        [XmlEnum("oil-water")]
        oilwater,

        /// <remarks/>
        steam,

        /// <remarks/>
        water,

        /// <remarks/>
        [XmlEnum("water -- brine")]
        waterbrine,

        /// <remarks/>
        [XmlEnum("water -- fresh water")]
        waterfreshwater,

        /// <remarks/>
        unknown,
    }

    /// <remarks/>
    [System.CodeDom.Compiler.GeneratedCodeAttribute("xsd", "2.0.50727.42")]
    
    [XmlType(Namespace = "http://www.witsml.org/schemas/140")]
    public enum WellDirection
    {

        /// <remarks/>
        [XmlEnum("huff-n-puff")]
        huffnpuff,

        /// <remarks/>
        injector,

        /// <remarks/>
        producer,

        /// <remarks/>
        uncertain,

        /// <remarks/>
        unknown,
    }

    /// <remarks/>
    [System.CodeDom.Compiler.GeneratedCodeAttribute("xsd", "2.0.50727.42")]
    
    [XmlType(Namespace = "http://www.witsml.org/schemas/140")]
    public enum WellboreType
    {

        /// <remarks/>
        bypass,

        /// <remarks/>
        initial,

        /// <remarks/>
        redrill,

        /// <remarks/>
        reentry,

        /// <remarks/>
        respud,

        /// <remarks/>
        sidetrack,

        /// <remarks/>
        unknown,
    }

    /// <remarks/>
    [System.CodeDom.Compiler.GeneratedCodeAttribute("xsd", "2.0.50727.42")]
    
    [XmlType(Namespace = "http://www.witsml.org/schemas/140")]
    public enum WellboreShape
    {

        /// <remarks/>
        [XmlEnum("build and hold")]
        buildandhold,

        /// <remarks/>
        deviated,

        /// <remarks/>
        [XmlEnum("double kickoff")]
        doublekickoff,

        /// <remarks/>
        horizontal,

        /// <remarks/>
        [XmlEnum("S-shaped")]
        Sshaped,

        /// <remarks/>
        vertical,

        /// <remarks/>
        unknown,
    }

    /// <remarks/>
    [System.CodeDom.Compiler.GeneratedCodeAttribute("xsd", "2.0.50727.42")]
    
    [XmlType(Namespace = "http://www.witsml.org/schemas/140")]
    public enum LogTraceState
    {

        /// <remarks/>
        [XmlEnum("depth adjusted")]
        depthadjusted,

        /// <remarks/>
        edited,

        /// <remarks/>
        joined,

        /// <remarks/>
        processed,

        /// <remarks/>
        raw,

        /// <remarks/>
        unknown,
    }

    /// <remarks/>
    [System.CodeDom.Compiler.GeneratedCodeAttribute("xsd", "2.0.50727.42")]
    
    [XmlType(Namespace = "http://www.witsml.org/schemas/140")]
    public enum LogTraceOrigin
    {

        /// <remarks/>
        realtime,

        /// <remarks/>
        modeled,

        /// <remarks/>
        unknown,
    }

    /// <remarks/>
    [System.CodeDom.Compiler.GeneratedCodeAttribute("xsd", "2.0.50727.42")]
    
    [XmlType(Namespace = "http://www.witsml.org/schemas/140")]
    public enum LogDataType
    {

        /// <remarks/>
        @byte,

        /// <remarks/>
        [XmlEnum("date time")]
        datetime,

        /// <remarks/>
        @double,

        /// <remarks/>
        @float,

        /// <remarks/>
        @int,

        /// <remarks/>
        @long,

        /// <remarks/>
        @short,

        /// <remarks/>
        @string,

        /// <remarks/>
        string40,

        /// <remarks/>
        string16,

        /// <remarks/>
        unknown,
    }

    /// <remarks/>
    [System.CodeDom.Compiler.GeneratedCodeAttribute("xsd", "2.0.50727.42")]
    
    [XmlType(Namespace = "http://www.witsml.org/schemas/140")]
    public enum ObjectGrowingState
    {

        /// <remarks/>
        growing,

        /// <remarks/>
        idle,

        /// <remarks/>
        closed,

        /// <remarks/>
        unknown,
    }

    /// <remarks/>
    [System.CodeDom.Compiler.GeneratedCodeAttribute("xsd", "2.0.50727.42")]
    
    [XmlType(Namespace = "http://www.witsml.org/schemas/140")]
    public enum LogIndexType
    {

        /// <remarks/>
        [XmlEnum("date time")]
        datetime,

        /// <remarks/>
        [XmlEnum("elapsed time")]
        elapsedtime,

        /// <remarks/>
        length,

        /// <remarks/>
        [XmlEnum("measured depth")]
        measureddepth,

        /// <remarks/>
        [XmlEnum("vertical depth")]
        verticaldepth,

        /// <remarks/>
        other,

        /// <remarks/>
        unknown,
    }

    /// <remarks/>
    [System.CodeDom.Compiler.GeneratedCodeAttribute("xsd", "2.0.50727.42")]
    
    [XmlType(Namespace = "http://www.witsml.org/schemas/140")]
    public enum LogIndexDirection
    {

        /// <remarks/>
        decreasing,

        /// <remarks/>
        increasing,

        /// <remarks/>
        unknown,
    }

    /// <remarks/>
    [System.CodeDom.Compiler.GeneratedCodeAttribute("xsd", "2.0.50727.42")]
    
    [XmlType(Namespace = "http://www.witsml.org/schemas/140")]
    public enum GasPeakType
    {

        /// <remarks/>
        [XmlEnum("circulating background gas")]
        circulatingbackgroundgas,

        /// <remarks/>
        [XmlEnum("connection gas")]
        connectiongas,

        /// <remarks/>
        [XmlEnum("drilling background gas")]
        drillingbackgroundgas,

        /// <remarks/>
        [XmlEnum("drilling gas peak")]
        drillinggaspeak,

        /// <remarks/>
        [XmlEnum("flow check gas")]
        flowcheckgas,

        /// <remarks/>
        [XmlEnum("no readings")]
        noreadings,

        /// <remarks/>
        other,

        /// <remarks/>
        [XmlEnum("shut down gas")]
        shutdowngas,

        /// <remarks/>
        [XmlEnum("trip gas")]
        tripgas,

        /// <remarks/>
        unknown,
    }

    /// <remarks/>
    [System.CodeDom.Compiler.GeneratedCodeAttribute("xsd", "2.0.50727.42")]
    
    [XmlType(Namespace = "http://www.witsml.org/schemas/140")]
    public enum ShowRating
    {

        /// <remarks/>
        none,

        /// <remarks/>
        [XmlEnum("very poor")]
        verypoor,

        /// <remarks/>
        poor,

        /// <remarks/>
        fair,

        /// <remarks/>
        good,

        /// <remarks/>
        [XmlEnum("very good")]
        verygood,

        /// <remarks/>
        unknown,
    }

    /// <remarks/>
    [System.CodeDom.Compiler.GeneratedCodeAttribute("xsd", "2.0.50727.42")]
    
    [XmlType(Namespace = "http://www.witsml.org/schemas/140")]
    public enum ShowFluorescence
    {

        /// <remarks/>
        faint,

        /// <remarks/>
        bright,

        /// <remarks/>
        none,

        /// <remarks/>
        unknown,
    }

    /// <remarks/>
    [System.CodeDom.Compiler.GeneratedCodeAttribute("xsd", "2.0.50727.42")]
    
    [XmlType(Namespace = "http://www.witsml.org/schemas/140")]
    public enum ShowSpeed
    {

        /// <remarks/>
        slow,

        /// <remarks/>
        [XmlEnum("moderately fast")]
        moderatelyfast,

        /// <remarks/>
        fast,

        /// <remarks/>
        instantaneous,

        /// <remarks/>
        none,

        /// <remarks/>
        unknown,
    }

    /// <remarks/>
    [System.CodeDom.Compiler.GeneratedCodeAttribute("xsd", "2.0.50727.42")]
    
    [XmlType(Namespace = "http://www.witsml.org/schemas/140")]
    public enum ShowLevel
    {

        /// <remarks/>
        blooming,

        /// <remarks/>
        streaming,

        /// <remarks/>
        unknown,
    }

    /// <remarks/>
    [System.CodeDom.Compiler.GeneratedCodeAttribute("xsd", "2.0.50727.42")]
    
    [XmlType(Namespace = "http://www.witsml.org/schemas/140")]
    public enum LithologySource
    {

        /// <remarks/>
        interpreted,

        /// <remarks/>
        core,

        /// <remarks/>
        cuttings,

        /// <remarks/>
        unknown,
    }

    /// <remarks/>
    [System.CodeDom.Compiler.GeneratedCodeAttribute("xsd", "2.0.50727.42")]
    
    [XmlType(Namespace = "http://www.witsml.org/schemas/140")]
    public enum MudLogParameterType
    {

        /// <remarks/>
        [XmlEnum("bit parameters")]
        bitparameters,

        /// <remarks/>
        [XmlEnum("bit type comment")]
        bittypecomment,

        /// <remarks/>
        [XmlEnum("casing point comment")]
        casingpointcomment,

        /// <remarks/>
        [XmlEnum("chromatograph comment")]
        chromatographcomment,

        /// <remarks/>
        [XmlEnum("circulation system comment")]
        circulationsystemcomment,

        /// <remarks/>
        [XmlEnum("core interval comment")]
        coreintervalcomment,

        /// <remarks/>
        [XmlEnum("cuttings gas")]
        cuttingsgas,

        /// <remarks/>
        [XmlEnum("direct fracture pressure")]
        directfracturepressure,

        /// <remarks/>
        [XmlEnum("direct pore pressure measurements")]
        directporepressuremeasurements,

        /// <remarks/>
        [XmlEnum("drilling data comment")]
        drillingdatacomment,

        /// <remarks/>
        [XmlEnum("fracture PG estimate most likely")]
        fracturePGestimatemostlikely,

        /// <remarks/>
        [XmlEnum("gas peaks comment")]
        gaspeakscomment,

        /// <remarks/>
        [XmlEnum("gas ratio comment")]
        gasratiocomment,

        /// <remarks/>
        [XmlEnum("general engineering comment")]
        generalengineeringcomment,

        /// <remarks/>
        [XmlEnum("kicks and flows")]
        kicksandflows,

        /// <remarks/>
        [XmlEnum("lithlog comment")]
        lithlogcomment,

        /// <remarks/>
        [XmlEnum("lost returns")]
        lostreturns,

        /// <remarks/>
        [XmlEnum("LWD comment")]
        LWDcomment,

        /// <remarks/>
        [XmlEnum("marker or formation top comment")]
        markerorformationtopcomment,

        /// <remarks/>
        [XmlEnum("midnight depth date")]
        midnightdepthdate,

        /// <remarks/>
        [XmlEnum("mud check comment")]
        mudcheckcomment,

        /// <remarks/>
        [XmlEnum("mud data comment")]
        muddatacomment,

        /// <remarks/>
        [XmlEnum("mudlog comment")]
        mudlogcomment,

        /// <remarks/>
        [XmlEnum("overburden gradient")]
        overburdengradient,

        /// <remarks/>
        [XmlEnum("overpull on connection")]
        overpullonconnection,

        /// <remarks/>
        [XmlEnum("overpull on trip")]
        overpullontrip,

        /// <remarks/>
        [XmlEnum("pore PG estimate most likely")]
        porePGestimatemostlikely,

        /// <remarks/>
        [XmlEnum("pore pressure estimate while drilling")]
        porepressureestimatewhiledrilling,

        /// <remarks/>
        [XmlEnum("pressure data comment")]
        pressuredatacomment,

        /// <remarks/>
        [XmlEnum("shale density comment")]
        shaledensitycomment,

        /// <remarks/>
        [XmlEnum("short trip comment")]
        shorttripcomment,

        /// <remarks/>
        [XmlEnum("show report comment")]
        showreportcomment,

        /// <remarks/>
        [XmlEnum("sidewall core comment")]
        sidewallcorecomment,

        /// <remarks/>
        [XmlEnum("sliding Interval")]
        slidingInterval,

        /// <remarks/>
        [XmlEnum("steam still results comment")]
        steamstillresultscomment,

        /// <remarks/>
        [XmlEnum("survey comment")]
        surveycomment,

        /// <remarks/>
        [XmlEnum("temperature data comment")]
        temperaturedatacomment,

        /// <remarks/>
        [XmlEnum("temperature trend comment")]
        temperaturetrendcomment,

        /// <remarks/>
        [XmlEnum("wireline log comment")]
        wirelinelogcomment,

        /// <remarks/>
        unknown,
    }

    /// <remarks/>
    [System.CodeDom.Compiler.GeneratedCodeAttribute("xsd", "2.0.50727.42")]
    
    [XmlType(Namespace = "http://www.witsml.org/schemas/140")]
    public enum TrajStationType
    {

        /// <remarks/>
        [XmlEnum("azimuth on plane")]
        azimuthonplane,

        /// <remarks/>
        [XmlEnum("buildrate to delta-MD")]
        buildratetodeltaMD,

        /// <remarks/>
        [XmlEnum("buildrate to INCL")]
        buildratetoINCL,

        /// <remarks/>
        [XmlEnum("buildrate to MD")]
        buildratetoMD,

        /// <remarks/>
        [XmlEnum("buildrate and turnrate to AZI")]
        buildrateandturnratetoAZI,

        /// <remarks/>
        [XmlEnum("buildrate and turnrate to delta-MD")]
        buildrateandturnratetodeltaMD,

        /// <remarks/>
        [XmlEnum("buildrate and turnrate to INCL")]
        buildrateandturnratetoINCL,

        /// <remarks/>
        [XmlEnum("buildrate and turnrate to INCL and AZI")]
        buildrateandturnratetoINCLandAZI,

        /// <remarks/>
        [XmlEnum("buildrate and turnrate to MD")]
        buildrateandturnratetoMD,

        /// <remarks/>
        [XmlEnum("buildrate and turnrate to TVD")]
        buildrateandturnratetoTVD,

        /// <remarks/>
        [XmlEnum("buildrate TVD")]
        buildrateTVD,

        /// <remarks/>
        [XmlEnum("casing MD")]
        casingMD,

        /// <remarks/>
        [XmlEnum("casing TVD")]
        casingTVD,

        /// <remarks/>
        DLS,

        /// <remarks/>
        [XmlEnum("DLS to AZI and MD")]
        DLStoAZIandMD,

        /// <remarks/>
        [XmlEnum("DLS to AZI-TVD")]
        DLStoAZITVD,

        /// <remarks/>
        [XmlEnum("DLS to INCL")]
        DLStoINCL,

        /// <remarks/>
        [XmlEnum("DLS to INCL and AZI")]
        DLStoINCLandAZI,

        /// <remarks/>
        [XmlEnum("DLS to INCL and MD")]
        DLStoINCLandMD,

        /// <remarks/>
        [XmlEnum("DLS to INCL and TVD")]
        DLStoINCLandTVD,

        /// <remarks/>
        [XmlEnum("DLS to NS, EW and TVD")]
        DLStoNSEWandTVD,

        /// <remarks/>
        [XmlEnum("DLS and toolface to AZI")]
        DLSandtoolfacetoAZI,

        /// <remarks/>
        [XmlEnum("DLS and toolface to delta-MD")]
        DLSandtoolfacetodeltaMD,

        /// <remarks/>
        [XmlEnum("DLS and toolface to INCL")]
        DLSandtoolfacetoINCL,

        /// <remarks/>
        [XmlEnum("DLS and toolface to INCL-AZI")]
        DLSandtoolfacetoINCLAZI,

        /// <remarks/>
        [XmlEnum("DLS and toolface to MD")]
        DLSandtoolfacetoMD,

        /// <remarks/>
        [XmlEnum("DLS and toolface to TVD")]
        DLSandtoolfacetoTVD,

        /// <remarks/>
        [XmlEnum("formation MD")]
        formationMD,

        /// <remarks/>
        [XmlEnum("formation TVD")]
        formationTVD,

        /// <remarks/>
        [XmlEnum("gyro inertial")]
        gyroinertial,

        /// <remarks/>
        [XmlEnum("gyro MWD")]
        gyroMWD,

        /// <remarks/>
        [XmlEnum("gyro north seeking")]
        gyronorthseeking,

        /// <remarks/>
        [XmlEnum("hold to delta-MD")]
        holdtodeltaMD,

        /// <remarks/>
        [XmlEnum("hold to MD")]
        holdtoMD,

        /// <remarks/>
        [XmlEnum("hold to TVD")]
        holdtoTVD,

        /// <remarks/>
        [XmlEnum("INCL, AZI and TVD")]
        INCLAZIandTVD,

        /// <remarks/>
        [XmlEnum("magnetic multi-shot")]
        magneticmultishot,

        /// <remarks/>
        [XmlEnum("magnetic MWD")]
        magneticMWD,

        /// <remarks/>
        [XmlEnum("magnetic single shot")]
        magneticsingleshot,

        /// <remarks/>
        [XmlEnum("marker MD")]
        markerMD,

        /// <remarks/>
        [XmlEnum("marker TVD")]
        markerTVD,

        /// <remarks/>
        [XmlEnum("NS, EW and TVD")]
        NSEWandTVD,

        /// <remarks/>
        [XmlEnum("target center")]
        targetcenter,

        /// <remarks/>
        [XmlEnum("target offset")]
        targetoffset,

        /// <remarks/>
        [XmlEnum("tie in point")]
        tieinpoint,

        /// <remarks/>
        [XmlEnum("turnrate to AZI")]
        turnratetoAZI,

        /// <remarks/>
        [XmlEnum("turnrate to delta-MD")]
        turnratetodeltaMD,

        /// <remarks/>
        [XmlEnum("turnrate to MD")]
        turnratetoMD,

        /// <remarks/>
        [XmlEnum("turnrate to TVD")]
        turnratetoTVD,

        /// <remarks/>
        unknown,
    }

    /// <remarks/>
    [System.CodeDom.Compiler.GeneratedCodeAttribute("xsd", "2.0.50727.42")]
    
    [XmlType(Namespace = "http://www.witsml.org/schemas/140")]
    public enum TypeSurveyTool
    {

        /// <remarks/>
        [XmlEnum("magnetic MWD")]
        magneticMWD,

        /// <remarks/>
        [XmlEnum("gyroscopic  MWD")]
        gyroscopicMWD,

        /// <remarks/>
        [XmlEnum("gyroscopic north seeking")]
        gyroscopicnorthseeking,

        /// <remarks/>
        [XmlEnum("gyroscopic inertial")]
        gyroscopicinertial,

        /// <remarks/>
        [XmlEnum("magnetic single-shot")]
        magneticsingleshot,

        /// <remarks/>
        [XmlEnum("magnetic multiple-shot")]
        magneticmultipleshot,

        /// <remarks/>
        unknown,
    }

    /// <remarks/>
    [System.CodeDom.Compiler.GeneratedCodeAttribute("xsd", "2.0.50727.42")]
    
    [XmlType(Namespace = "http://www.witsml.org/schemas/140")]
    public enum TrajStationStatus
    {

        /// <remarks/>
        open,

        /// <remarks/>
        rejected,

        /// <remarks/>
        position,

        /// <remarks/>
        unknown,
    }
}
