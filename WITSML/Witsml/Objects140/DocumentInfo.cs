﻿ 

using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;

namespace Witsml.Objects140
{
    /// <remarks/>
    [System.CodeDom.Compiler.GeneratedCodeAttribute("xsd", "2.0.50727.42")]
    
    
    [System.Xml.Serialization.XmlTypeAttribute(Namespace = "http://www.witsml.org/schemas/140")]
    public partial class cs_documentInfo
    {

        /// <remarks/>
        public nameStruct documentName;

        /// <remarks/>
        [System.Xml.Serialization.XmlElementAttribute("documentAlias")]
        public nameStruct[] documentAlias;

        /// <remarks/>
        public System.DateTime documentDate;

        /// <remarks/>
        [System.Xml.Serialization.XmlIgnoreAttribute()]
        public bool documentDateSpecified;

        /// <remarks/>
        [System.Xml.Serialization.XmlElementAttribute("documentClass")]
        public nameStruct[] documentClass;

        /// <remarks/>
        public cs_documentFileCreation fileCreationInformation;

        /// <remarks/>
        [System.Xml.Serialization.XmlElementAttribute("securityInformation")]
        public cs_documentSecurityInfo[] securityInformation;

        /// <remarks/>
        public string disclaimer;

        /// <remarks/>
        [System.Xml.Serialization.XmlArrayItemAttribute("event", IsNullable = false)]
        public cs_documentEvent[] auditTrail;

        /// <remarks/>
        public string owner;

        /// <remarks/>
        public string comment;
    }

    /// <remarks/>
    [System.CodeDom.Compiler.GeneratedCodeAttribute("xsd", "2.0.50727.42")]
    
    
    [System.Xml.Serialization.XmlTypeAttribute(Namespace = "http://www.witsml.org/schemas/140")]
    public partial class nameStruct
    {

        /// <remarks/>
        [System.Xml.Serialization.XmlAttributeAttribute()]
        public string namingSystem;

        /// <remarks/>
        [System.Xml.Serialization.XmlTextAttribute()]
        public string Value;
    }

    /// <remarks/>
    [System.CodeDom.Compiler.GeneratedCodeAttribute("xsd", "2.0.50727.42")]
    
    
    [System.Xml.Serialization.XmlTypeAttribute(Namespace = "http://www.witsml.org/schemas/140")]
    public partial class cs_documentFileCreation
    {

        /// <remarks/>
        public System.DateTime fileCreationDate;

        /// <remarks/>
        public string softwareName;

        /// <remarks/>
        public string fileCreator;

        /// <remarks/>
        public string comment;
    }

    /// <remarks/>
    [System.CodeDom.Compiler.GeneratedCodeAttribute("xsd", "2.0.50727.42")]
    
    
    [System.Xml.Serialization.XmlTypeAttribute(Namespace = "http://www.witsml.org/schemas/140")]
    public partial class cs_documentSecurityInfo
    {

        /// <remarks/>
        public string @class;

        /// <remarks/>
        public string securitySystem;

        /// <remarks/>
        public System.DateTime endDate;

        /// <remarks/>
        [System.Xml.Serialization.XmlIgnoreAttribute()]
        public bool endDateSpecified;

        /// <remarks/>
        public string comment;
    }

    /// <remarks/>
    [System.CodeDom.Compiler.GeneratedCodeAttribute("xsd", "2.0.50727.42")]
    
    
    [System.Xml.Serialization.XmlTypeAttribute(Namespace = "http://www.witsml.org/schemas/140")]
    public partial class cs_documentEvent
    {

        /// <remarks/>
        public System.DateTime eventDate;

        /// <remarks/>
        public string eventType;

        /// <remarks/>
        public string responsibleParty;

        /// <remarks/>
        public string comment;
    }
}
