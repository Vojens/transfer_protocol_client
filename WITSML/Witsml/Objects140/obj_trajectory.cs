﻿ 

//------------------------------------------------------------------------------
// <auto-generated>
//     This code was generated by a tool.
//     Runtime Version:2.0.50727.1433
//
//     Changes to this file may cause incorrect behavior and will be lost if
//     the code is regenerated.
// </auto-generated>
//------------------------------------------------------------------------------

// 
// This source code was auto-generated by xsd, Version=2.0.50727.42.
// 
namespace Witsml.Objects140 {
    using System.Xml.Serialization;
    
    
    /// <remarks/>
    [System.CodeDom.Compiler.GeneratedCodeAttribute("xsd", "2.0.50727.42")]
    
    [System.Diagnostics.DebuggerStepThroughAttribute()]
    
    [XmlType(Namespace="http://www.witsml.org/schemas/140")]
    [XmlRoot("trajectorys", Namespace="http://www.witsml.org/schemas/140", IsNullable=false)]
    public partial class obj_trajectorys {
        
        /// <remarks/>
        public cs_documentInfo documentInfo;
        
        /// <remarks/>
        [XmlElement("trajectory")]
        public obj_trajectory[] trajectory;
        
        /// <remarks/>
        [XmlAttribute()]
        public string version;
    }
    
    /// <remarks/>
    [System.CodeDom.Compiler.GeneratedCodeAttribute("xsd", "2.0.50727.42")]
    
    [System.Diagnostics.DebuggerStepThroughAttribute()]
    
    [XmlType(Namespace="http://www.witsml.org/schemas/140")]
    public partial class cs_refWellboreTrajectoryStation {
        
        /// <remarks/>
        public string stationReference;
        
        /// <remarks/>
        public refNameString trajectoryParent;
        
        /// <remarks/>
        public refNameString wellboreParent;
    }
    
    
    /// <remarks/>
    [System.CodeDom.Compiler.GeneratedCodeAttribute("xsd", "2.0.50727.42")]
    
    [System.Diagnostics.DebuggerStepThroughAttribute()]
    
    [XmlType(Namespace="http://www.witsml.org/schemas/140")]
    public partial class cs_stnTrajMatrixCov {
        
        /// <remarks/>
        public areaMeasure varianceNN;
        
        /// <remarks/>
        public areaMeasure varianceNE;
        
        /// <remarks/>
        public areaMeasure varianceNVert;
        
        /// <remarks/>
        public areaMeasure varianceEE;
        
        /// <remarks/>
        public areaMeasure varianceEVert;
        
        /// <remarks/>
        public areaMeasure varianceVertVert;
        
        /// <remarks/>
        public lengthMeasure biasN;
        
        /// <remarks/>
        public lengthMeasure biasE;
        
        /// <remarks/>
        public lengthMeasure biasVert;
    }
    
    /// <remarks/>
    [System.CodeDom.Compiler.GeneratedCodeAttribute("xsd", "2.0.50727.42")]
    
    [System.Diagnostics.DebuggerStepThroughAttribute()]
    
    [XmlType(Namespace="http://www.witsml.org/schemas/140")]
    public partial class cs_stnTrajValid {
        
        /// <remarks/>
        public magneticInductionMeasure magTotalFieldCalc;
        
        /// <remarks/>
        public planeAngleMeasure magDipAngleCalc;
        
        /// <remarks/>
        public accelerationLinearMeasure gravTotalFieldCalc;
    }
    
    /// <remarks/>
    [System.CodeDom.Compiler.GeneratedCodeAttribute("xsd", "2.0.50727.42")]
    
    [System.Diagnostics.DebuggerStepThroughAttribute()]
    
    [XmlType(Namespace="http://www.witsml.org/schemas/140")]
    public partial class cs_stnTrajCorUsed {
        
        /// <remarks/>
        public accelerationLinearMeasure gravAxialAccelCor;
        
        /// <remarks/>
        public accelerationLinearMeasure gravTran1AccelCor;
        
        /// <remarks/>
        public accelerationLinearMeasure gravTran2AccelCor;
        
        /// <remarks/>
        public magneticInductionMeasure magAxialDrlstrCor;
        
        /// <remarks/>
        public magneticInductionMeasure magTran1DrlstrCor;
        
        /// <remarks/>
        public magneticInductionMeasure magTran2DrlstrCor;
        
        /// <remarks/>
        public planeAngleMeasure sagIncCor;
        
        /// <remarks/>
        public planeAngleMeasure sagAziCor;
        
        /// <remarks/>
        public planeAngleMeasure stnMagDeclUsed;
        
        /// <remarks/>
        public planeAngleMeasure stnGridCorUsed;
        
        /// <remarks/>
        public lengthMeasure dirSensorOffset;
    }
    
    /// <remarks/>
    [System.CodeDom.Compiler.GeneratedCodeAttribute("xsd", "2.0.50727.42")]
    
    [System.Diagnostics.DebuggerStepThroughAttribute()]
    
    [XmlType(Namespace="http://www.witsml.org/schemas/140")]
    public partial class cs_stnTrajRawData {
        
        /// <remarks/>
        public accelerationLinearMeasure gravAxialRaw;
        
        /// <remarks/>
        public accelerationLinearMeasure gravTran1Raw;
        
        /// <remarks/>
        public accelerationLinearMeasure gravTran2Raw;
        
        /// <remarks/>
        public magneticInductionMeasure magAxialRaw;
        
        /// <remarks/>
        public magneticInductionMeasure magTran1Raw;
        
        /// <remarks/>
        public magneticInductionMeasure magTran2Raw;
    }
    
    /// <remarks/>
    [System.CodeDom.Compiler.GeneratedCodeAttribute("xsd", "2.0.50727.42")]
    
    [System.Diagnostics.DebuggerStepThroughAttribute()]
    
    [XmlType(Namespace="http://www.witsml.org/schemas/140")]
    public partial class cs_trajectoryStation {
        
        /// <remarks/>
        public refNameString target;
        
        /// <remarks/>
        public System.DateTime dTimStn;
        
        /// <remarks/>
        [XmlIgnore()]
        public bool dTimStnSpecified;
        
        /// <remarks/>
        public TrajStationType typeTrajStation;
        
        /// <remarks/>
        public TypeSurveyTool typeSurveyTool;
        
        /// <remarks/>
        [XmlIgnore()]
        public bool typeSurveyToolSpecified;
        
        /// <remarks/>
        public measuredDepthCoord md;
        
        /// <remarks/>
        public wellVerticalDepthCoord tvd;
        
        /// <remarks/>
        public planeAngleMeasure incl;
        
        /// <remarks/>
        public planeAngleMeasure azi;
        
        /// <remarks/>
        public planeAngleMeasure mtf;
        
        /// <remarks/>
        public planeAngleMeasure gtf;
        
        /// <remarks/>
        public lengthMeasure dispNs;
        
        /// <remarks/>
        public lengthMeasure dispEw;
        
        /// <remarks/>
        public lengthMeasure vertSect;
        
        /// <remarks/>
        public anglePerLengthMeasure dls;
        
        /// <remarks/>
        public anglePerLengthMeasure rateTurn;
        
        /// <remarks/>
        public anglePerLengthMeasure rateBuild;
        
        /// <remarks/>
        public lengthMeasure mdDelta;
        
        /// <remarks/>
        public lengthMeasure tvdDelta;
        
        /// <remarks/>
        public string modelToolError;
        
        /// <remarks/>
        public accelerationLinearMeasure gravTotalUncert;
        
        /// <remarks/>
        public planeAngleMeasure dipAngleUncert;
        
        /// <remarks/>
        public magneticInductionMeasure magTotalUncert;
        
        /// <remarks/>
        public bool gravAccelCorUsed;
        
        /// <remarks/>
        [XmlIgnore()]
        public bool gravAccelCorUsedSpecified;
        
        /// <remarks/>
        public bool magXAxialCorUsed;
        
        /// <remarks/>
        [XmlIgnore()]
        public bool magXAxialCorUsedSpecified;
        
        /// <remarks/>
        public bool sagCorUsed;
        
        /// <remarks/>
        [XmlIgnore()]
        public bool sagCorUsedSpecified;
        
        /// <remarks/>
        public bool magDrlstrCorUsed;
        
        /// <remarks/>
        [XmlIgnore()]
        public bool magDrlstrCorUsedSpecified;
        
        /// <remarks/>
        public accelerationLinearMeasure gravTotalFieldReference;
        
        /// <remarks/>
        public magneticInductionMeasure magTotalFieldReference;
        
        /// <remarks/>
        public planeAngleMeasure magDipAngleReference;
        
        /// <remarks/>
        public string magModelUsed;
        
        /// <remarks/>
        public string magModelValid;
        
        /// <remarks/>
        public string geoModelUsed;
        
        /// <remarks/>
        public TrajStationStatus statusTrajStation;
        
        /// <remarks/>
        [XmlIgnore()]
        public bool statusTrajStationSpecified;
        
        /// <remarks/>
        public cs_stnTrajRawData rawData;
        
        /// <remarks/>
        public cs_stnTrajCorUsed corUsed;
        
        /// <remarks/>
        public cs_stnTrajValid valid;
        
        /// <remarks/>
        public cs_stnTrajMatrixCov matrixCov;
        
        /// <remarks/>
        [XmlElement("location")]
        public cs_location[] location;
        
        /// <remarks/>
        public cs_refWellboreTrajectoryStation sourceStation;
        
        /// <remarks/>
        public cs_commonData commonData;
        
        /// <remarks/>
        [XmlAttribute()]
        public string uid;
    }
    
    /// <remarks/>
    [System.CodeDom.Compiler.GeneratedCodeAttribute("xsd", "2.0.50727.42")]
    
    [System.Diagnostics.DebuggerStepThroughAttribute()]
    
    [XmlType(Namespace="http://www.witsml.org/schemas/140")]
    public partial class cs_refWellboreTrajectory {
        
        /// <remarks/>
        public refNameString trajectoryReference;
        
        /// <remarks/>
        public refNameString wellboreParent;
    }
    
    /// <remarks/>
    [System.CodeDom.Compiler.GeneratedCodeAttribute("xsd", "2.0.50727.42")]
    
    [System.Diagnostics.DebuggerStepThroughAttribute()]
    
    [XmlType(Namespace="http://www.witsml.org/schemas/140")]
    public partial class obj_trajectory {
        
        /// <remarks/>
        public string nameWell;
        
        /// <remarks/>
        public string nameWellbore;
        
        /// <remarks/>
        public string name;
        
        /// <remarks/>
        public ObjectGrowingState objectGrowing;
        
        /// <remarks/>
        [XmlIgnore()]
        public bool objectGrowingSpecified;
        
        /// <remarks/>
        public cs_refWellboreTrajectory parentTrajectory;
        
        /// <remarks/>
        public System.DateTime dTimTrajStart;
        
        /// <remarks/>
        [XmlIgnore()]
        public bool dTimTrajStartSpecified;
        
        /// <remarks/>
        public System.DateTime dTimTrajEnd;
        
        /// <remarks/>
        [XmlIgnore()]
        public bool dTimTrajEndSpecified;
        
        /// <remarks/>
        public measuredDepthCoord mdMn;
        
        /// <remarks/>
        public measuredDepthCoord mdMx;
        
        /// <remarks/>
        public string serviceCompany;
        
        /// <remarks/>
        public planeAngleMeasure magDeclUsed;
        
        /// <remarks/>
        public planeAngleMeasure gridCorUsed;
        
        /// <remarks/>
        public planeAngleMeasure aziVertSect;
        
        /// <remarks/>
        public lengthMeasure dispNsVertSectOrig;
        
        /// <remarks/>
        public lengthMeasure dispEwVertSectOrig;
        
        /// <remarks/>
        public bool definitive;
        
        /// <remarks/>
        [XmlIgnore()]
        public bool definitiveSpecified;
        
        /// <remarks/>
        public bool memory;
        
        /// <remarks/>
        [XmlIgnore()]
        public bool memorySpecified;
        
        /// <remarks/>
        public bool finalTraj;
        
        /// <remarks/>
        [XmlIgnore()]
        public bool finalTrajSpecified;
        
        /// <remarks/>
        public AziRef aziRef;
        
        /// <remarks/>
        [XmlIgnore()]
        public bool aziRefSpecified;
        
        /// <remarks/>
        [XmlElement("trajectoryStation")]
        public cs_trajectoryStation[] trajectoryStation;
        
        /// <remarks/>
        public cs_commonData commonData;
        
        /// <remarks/>
        public cs_customData customData;
        
        /// <remarks/>
        [XmlAttribute()]
        public string uidWell;
        
        /// <remarks/>
        [XmlAttribute()]
        public string uidWellbore;
        
        /// <remarks/>
        [XmlAttribute()]
        public string uid;
    }
    
    
}
