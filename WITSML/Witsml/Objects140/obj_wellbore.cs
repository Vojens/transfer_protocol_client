﻿ 

//------------------------------------------------------------------------------
// <auto-generated>
//     This code was generated by a tool.
//     Runtime Version:2.0.50727.1433
//
//     Changes to this file may cause incorrect behavior and will be lost if
//     the code is regenerated.
// </auto-generated>
//------------------------------------------------------------------------------

// 
// This source code was auto-generated by xsd, Version=2.0.50727.42.
// 
namespace Witsml.Objects140
{
    using System.Xml.Serialization;


    /// <remarks/>
    [System.CodeDom.Compiler.GeneratedCodeAttribute("xsd", "2.0.50727.42")]
    
    
    [XmlType(Namespace = "http://www.witsml.org/schemas/140")]
    [XmlRoot("wellbores", Namespace = "http://www.witsml.org/schemas/140", IsNullable = false)]
    public partial class obj_wellbores
    {

        /// <remarks/>
        public cs_documentInfo documentInfo;

        /// <remarks/>
        [XmlElement("wellbore")]
        public obj_wellbore[] wellbore;

        /// <remarks/>
        [XmlAttribute()]
        public string version;
    }

    /// <remarks/>
    [System.CodeDom.Compiler.GeneratedCodeAttribute("xsd", "2.0.50727.42")]
    
    
    [XmlType(Namespace = "http://www.witsml.org/schemas/140")]
    public partial class obj_wellbore
    {

        /// <remarks/>
        public string nameWell;

        /// <remarks/>
        public string name;

        /// <remarks/>
        public refNameString parentWellbore;

        /// <remarks/>
        public string number;

        /// <remarks/>
        public string suffixAPI;

        /// <remarks/>
        public string numGovt;

        /// <remarks/>
        public WellStatus statusWellbore;

        /// <remarks/>
        [XmlIgnore()]
        public bool statusWellboreSpecified;

        /// <remarks/>
        public bool isActive;

        /// <remarks/>
        [XmlIgnore()]
        public bool isActiveSpecified;

        /// <remarks/>
        public WellPurpose purposeWellbore;

        /// <remarks/>
        [XmlIgnore()]
        public bool purposeWellboreSpecified;

        /// <remarks/>
        public WellboreType typeWellbore;

        /// <remarks/>
        [XmlIgnore()]
        public bool typeWellboreSpecified;

        /// <remarks/>
        public WellboreShape shape;

        /// <remarks/>
        [XmlIgnore()]
        public bool shapeSpecified;

        /// <remarks/>
        public System.DateTime dTimKickoff;

        /// <remarks/>
        [XmlIgnore()]
        public bool dTimKickoffSpecified;

        /// <remarks/>
        public bool achievedTD;

        /// <remarks/>
        [XmlIgnore()]
        public bool achievedTDSpecified;

        /// <remarks/>
        public measuredDepthCoord mdCurrent;

        /// <remarks/>
        public wellVerticalDepthCoord tvdCurrent;

        /// <remarks/>
        public measuredDepthCoord mdBitCurrent;

        /// <remarks/>
        public wellVerticalDepthCoord tvdBitCurrent;

        /// <remarks/>
        public measuredDepthCoord mdKickoff;

        /// <remarks/>
        public wellVerticalDepthCoord tvdKickoff;

        /// <remarks/>
        public measuredDepthCoord mdPlanned;

        /// <remarks/>
        public wellVerticalDepthCoord tvdPlanned;

        /// <remarks/>
        public measuredDepthCoord mdSubSeaPlanned;

        /// <remarks/>
        public wellVerticalDepthCoord tvdSubSeaPlanned;

        /// <remarks/>
        public timeMeasure dayTarget;

        /// <remarks/>
        public cs_commonData commonData;

        /// <remarks/>
        public cs_customData customData;

        /// <remarks/>
        [XmlAttribute()]
        public string uidWell;

        /// <remarks/>
        [XmlAttribute()]
        public string uid;
    }
    

}
