 

using System;
using System.Reflection;
using System.Text;
using System.Xml.Serialization;

using Support.Common;
using Witsml.Common.Exceptions;

namespace Witsml
{
    /// <summary>
    /// Helper Class To handle parsing of data type from string(Datetime,int,bool,short,double and enum)
    /// </summary>
    public static class StaticParser
    {
        /// <summary>
        /// Return result of parsing datetime from string value
        /// </summary>
        /// <param name="value">value to be parsed</param>
        /// <returns>parsed datetime value</returns>
        public static DateTime ParseDateTimeFromString(string value)
        {
            DateTime dt = new DateTime();
            try
            {
                if (value == string.Empty)
                    return dt.ToUniversalTime();
                else
                    return Parser.ParseDateTime(value);
            }
            catch (FormatException)
            {
                throw new Exceptions.UnableToDecodeException(string.Format("Unable to convert from string to datetime on {0}", value), WITSMLReturnCode.WITSMLParseError, true);
            }
        }

        /// <summary>
        /// Return result of parsing datetime from string value
        /// </summary>
        /// <param name="value">value to be parsed</param>
        /// <param name="defaultValue">default value if it is empty</param>
        /// <returns>parsed datetime value</returns>
        public static DateTime ParseDateTimeFromString(string value, DateTime defaultValue)
        {
            try
            {
                if (value == string.Empty)
                    return defaultValue.ToUniversalTime();
                else
                    return Parser.ParseDateTime(value);
            }
            catch (FormatException)
            {
                return defaultValue.ToUniversalTime();
            }
        }

        /// <summary>
        /// Return result of parsing integer from string value
        /// </summary>
        /// <param name="value">value to be parsed</param>
        /// <returns>parsed integer value</returns>
        public static int ParseIntFromString(string value)
        {
            int defaultInt = 0;
            try
            {
                if (value == string.Empty)
                    return defaultInt;
                else
                    return Parser.ParseInt(value);
            }
            catch (FormatException)
            {
                throw new Exceptions.UnableToDecodeException(string.Format("Unable to convert from string to int on {0}", value), WITSMLReturnCode.WITSMLParseError, true);
            }
        }

        /// <summary>
        /// Return result of parsing integer from string value
        /// </summary>
        /// <param name="value">value to be parsed</param>
        /// <param name="defaultValue">default value if it is empty</param>
        /// <returns>parsed integer value</returns>
        public static int ParseIntFromString(string value, int defaultValue)
        {
            try
            {
                if (value == string.Empty)
                    return defaultValue;
                else
                    return Parser.ParseInt(value);
            }
            catch (FormatException)
            {
                return defaultValue;
            }
        }

        /// <summary>
        /// Return result of parsing short from string value
        /// </summary>
        /// <param name="value">value to be parsed</param>
        /// <returns>parsed short value</returns>
        public static Int16 ParseShortFromString(string value)
        {
            short defaultShort = new short();
            try
            {
                if (value == string.Empty)
                    return defaultShort;
                else
                    return Parser.ParseShort(value);
            }
            catch (FormatException)
            {
                throw new Exceptions.UnableToDecodeException(string.Format("Unable to convert from string to short on {0}", value), WITSMLReturnCode.WITSMLParseError, true);
            }
        }

        /// <summary>
        /// Return result of parsing short from string value
        /// </summary>
        /// <param name="value">value to be parsed</param>
        /// <param name="defaultValue">default value if it is empty</param>
        /// <returns>parsed short value</returns>
        public static Int16 ParseShortFromString(string value, short defaultValue)
        {
            try
            {
                if (value == string.Empty)
                    return defaultValue;
                else
                    return Parser.ParseShort(value);
            }
            catch (FormatException)
            {
                return defaultValue;
            }
        }

        /// <summary>
        /// Return result of parsing double from string value
        /// </summary>
        /// <param name="value">value to be parsed</param>
        /// <returns>parsed double value</returns>
        public static double ParseDoubleFromString(string value)
        {
            double defaultDouble = new double();
            try
            {
                if (value == string.Empty)
                    return defaultDouble;
                else if (value == "-INF")
                    return double.NegativeInfinity;
                else
                    return Parser.ParseDouble(value);
            }
            catch (FormatException)
            {
                throw new Exceptions.UnableToDecodeException(string.Format("Unable to convert from string to double on {0}", value), WITSMLReturnCode.WITSMLParseError, true);
            }
        }

        /// <summary>
        /// Return result of parsing double from string value
        /// </summary>
        /// <param name="value">value to be parsed</param>
        /// <param name="defaultValue">default value if it is empty</param>
        /// <returns>parsed double value</returns>
        public static double ParseDoubleFromString(string value, double defaultValue)
        {
            try
            {
                if (value == string.Empty)
                    return defaultValue;
                else if (value == "-INF")
                    return double.NegativeInfinity;
                else
                    return Parser.ParseDouble(value);
            }
            catch (FormatException)
            {
                return defaultValue;
            }
        }

        /// <summary>
        /// Return result of parsing decimal from string value
        /// </summary>
        /// <param name="value">value to be parsed</param>
        /// <returns>parsed decimal value</returns>
        public static decimal ParseDecimalFromString(string value)
        {
            decimal defaultDecimal = new decimal();
            try
            {
                if (value == string.Empty)
                    return defaultDecimal;
                else
                    return Parser.ParseDecimal(value);
            }
            catch (FormatException)
            {
                throw new Exceptions.UnableToDecodeException(string.Format("Unable to convert from string to double on {0}", value), WITSMLReturnCode.WITSMLParseError, true);
            }
        }

        /// <summary>
        /// Return result of parsing decimal from string value
        /// </summary>
        /// <param name="value">value to be parsed</param>
        /// <param name="defaultValue">default value if it is empty</param>
        /// <returns>parsed decimal value</returns>
        public static decimal ParseDecimalFromString(string value, decimal defaultValue)
        {
            try
            {
                if (value == string.Empty)
                    return defaultValue;
                else
                    return Parser.ParseDecimal(value);
            }
            catch (FormatException)
            {
                return defaultValue;
            }
        }

        /// <summary>
        /// Return result of parsing boolean from string value
        /// </summary>
        /// <param name="value">value to be parsed</param>
        /// <returns>parsed boolean value</returns>
        public static bool ParseBooleanFromString(string value)
        {
            bool defaultBool = new bool();
            try
            {
                try
                {
                    //try int to bool conversion(0,1)
                    int valueInt = int.Parse(value);
                    return Parser.ParseBoolean(valueInt);
                }
                catch
                {
                    if (value == string.Empty)
                        return defaultBool;
                    else
                        return Parser.ParseBoolean(value);
                }
            }
            catch (FormatException)
            {
                throw new Exceptions.UnableToDecodeException(string.Format("Unable to convert from string to boolean on {0}", value), WITSMLReturnCode.WITSMLParseError, true);
            }
        }

        /// <summary>
        /// Return result of parsing boolean from string value
        /// </summary>
        /// <param name="value">value to be parsed</param>
        /// <param name="defaultValue">default value if it is empty</param>
        /// <returns>parsed boolean value</returns>
        public static bool ParseBooleanFromString(string value, bool defaultValue)
        {
            try
            {
                try
                {
                    //try int to bool conversion(0,1)
                    int valueInt = int.Parse(value);
                    return Parser.ParseBoolean(valueInt);
                }
                catch
                {
                    if (value == string.Empty)
                        return defaultValue;
                    else
                        return Parser.ParseBoolean(value);
                }
            }
            catch (FormatException)
            {
                return defaultValue;
            }
        }

        /// <summary>
        /// special enum parser method,for uom(which does not have unknown/default).parse string to enum,if enum not exist,try parse "unknown",if it still not exist,throw UnableToDecode Exception
        /// </summary>
        /// <typeparam name="T">type of enum</typeparam>
        /// <param name="value">string to be parsed to enum</param>
        /// <returns>enum</returns>
        public static T ParseEnumUomFromString<T>(string value)
        {
            try
            {
                //unknown is just been added to enum value.
                if (!string.IsNullOrEmpty(value))
                {
                    foreach (FieldInfo fi in typeof(T).GetFields())
                    {
                        object[] attrs = fi.GetCustomAttributes(typeof(XmlEnumAttribute), false);
                        if ((attrs.Length > 0 && ((XmlEnumAttribute)attrs[0]).Name.Equals(value, StringComparison.OrdinalIgnoreCase)) || fi.Name.Equals(value, StringComparison.OrdinalIgnoreCase))
                        {
                            return (T)Enum.Parse(typeof(T), fi.Name, true);
                        }
                    }
                }

                //match not found
                return (T)Enum.Parse(typeof(T), "unknown", false);
            }
            catch (Exception)
            {
                //this kind of enum does not have unknown
                throw new Exceptions.UnableToDecodeException(string.Format("Unable to convert from string to enum on {0}", value), WITSMLReturnCode.WITSMLParseError, true);
            }
        }

        /// <summary>
        /// parse string to enum,if enum not exist,throw exception
        /// </summary>
        /// <typeparam name="T">type of enum</typeparam>
        /// <param name="value">string to be parsed to enum</param>
        /// <param name="defaultEnum">default enum,which is to be returned if no matching enum found</param>
        /// <returns>enum</returns>
        public static T ParseEnumFromString<T>(string value)
        {
            try
            {
                if (!string.IsNullOrEmpty(value))
                {
                    foreach (FieldInfo fi in typeof(T).GetFields())
                    {
                        object[] attrs = fi.GetCustomAttributes(typeof(XmlEnumAttribute), false);
                        if ((attrs.Length > 0 && ((XmlEnumAttribute)attrs[0]).Name == value) || fi.Name == value)
                        {
                            return (T)Enum.Parse(typeof(T), fi.Name, true);
                        }
                    }
                }

                //match not found
                throw new Exceptions.UnableToDecodeException(string.Format("Unable to convert from string to enum on {0}", value), WITSMLReturnCode.WITSMLParseError, true);
            }
            catch (Exception)
            {
                //this kind of enum does not have unknown
                throw new Exceptions.UnableToDecodeException(string.Format("Unable to convert from string to enum on {0}", value), WITSMLReturnCode.WITSMLParseError, true);
            }

        }

        /// <summary>
        /// parse string to enum,if enum not exist,return defaultEnum
        /// </summary>
        /// <typeparam name="T">type of enum</typeparam>
        /// <param name="value">string to be parsed to enum</param>
        /// <param name="defaultEnum">default enum,which is to be returned if no matching enum found</param>
        /// <returns>enum</returns>
        public static T ParseEnumFromString<T>(string value, T defaultEnum)
        {
            if (!string.IsNullOrEmpty(value))
            {
                foreach (FieldInfo fi in typeof(T).GetFields())
                {
                    object[] attrs = fi.GetCustomAttributes(typeof(XmlEnumAttribute), false);
                    if ((attrs.Length > 0 && ((XmlEnumAttribute)attrs[0]).Name == value) || fi.Name == value)
                    {
                        return (T)Enum.Parse(typeof(T), fi.Name, true);
                    }
                }
            }

            //match not found
            return defaultEnum;

        }

        /// <summary>
        /// Parse A Datetime into A String which conform Witsml Format (Combined ISO Format)
        /// </summary>
        /// <param name="inputDateTime">A Date Time</param>
        /// <returns>A String Representing Datetime in ISO Format for usage in Witsml</returns>
        public static string ParseStringFromDateTime(DateTime inputDateTime)
        {
            if (inputDateTime == null) return null;

            string result = string.Empty;
            result = inputDateTime.ToString("o");
            return result;
        }

        private static System.Globalization.CultureInfo usCultureInfo = new System.Globalization.CultureInfo("en-US", false);
        /// <summary>
        /// Parse A Double into A String which conform Witsml Format (en US Format)
        /// </summary>
        /// <param name="inputDouble">A Double</param>
        /// <returns>A String Representing Datetime in ISO Format for usage in Witsml</returns>
        public static string ParseStringFromDouble(double inputDouble)
        {
            string result = string.Empty;
            result = inputDouble.ToString(usCultureInfo);
            return result;
        }

        /// <summary>
        /// Accept string value and return related enum and boolean value
        /// </summary>
        /// <typeparam name="T">Enum name</typeparam>
        /// <param name="value">string value for the enum object</param>
        /// <param name="result">Out Enum object</param>
        /// <param name="isSpecified">Out Boolean to be assgned,which determine to show Enum or not</param>
        public static void SetEnumFromString<T>(string value, out T result, out bool isSpecified)
        {
            try
            {
                result = (T)Enum.Parse(typeof(T), "unknown", false);

                if (!string.IsNullOrEmpty(value))
                {
                    foreach (FieldInfo fi in typeof(T).GetFields())
                    {
                        object[] attrs = fi.GetCustomAttributes(typeof(XmlEnumAttribute), false);
                        if ((attrs.Length > 0 && ((XmlEnumAttribute)attrs[0]).Name == value) || fi.Name == value)
                        {
                            result = (T)Enum.Parse(typeof(T), fi.Name, true);
                            break;
                        }
                    }
                }

                isSpecified = true;
            }
            catch
            {
                result = (T)Enum.Parse(typeof(T), "unknown", false);
                isSpecified = false;
            }
        }

        /// <summary>
        /// Accept string value and return related enum and boolean value
        /// </summary>
        /// <typeparam name="T">Enum name</typeparam>
        /// <param name="value">string value for the enum object</param>
        /// <param name="result">Out Enum object</param>
        /// <param name="isSpecified">Out Boolean to be assgned,which determine to show Enum or not</param>
        public static void SetEnumFromString<T>(string value, out T result, out bool isSpecified, T defaultValue)
        {
            try
            {
                result = defaultValue;

                if (!string.IsNullOrEmpty(value))
                {
                    foreach (FieldInfo fi in typeof(T).GetFields())
                    {
                        object[] attrs = fi.GetCustomAttributes(typeof(XmlEnumAttribute), false);
                        if ((attrs.Length > 0 && ((XmlEnumAttribute)attrs[0]).Name == value) || fi.Name == value)
                        {
                            result = (T)Enum.Parse(typeof(T), fi.Name, true);
                            break;
                        }
                    }
                }

                isSpecified = true;
            }
            catch
            {
                result = defaultValue;
                isSpecified = false;
            }
        }

        /// <summary>
        /// Return result of parsing GUID from string value
        /// </summary>
        /// <param name="value">value to be parsed</param>
        /// <returns>parsed GUID value</returns>
        public static Guid ParseGuidFromString(string value)
        {
            try
            {
                if (value == string.Empty)
                    return Guid.Empty;
                else
                    return Parser.ParseGuid(value);
            }
            catch (FormatException)
            {
                throw new Exceptions.UnableToDecodeException(string.Format("Unable to convert from string to GUID on {0}", value), WITSMLReturnCode.WITSMLParseError, true);
            }
        }

        public static byte[] ParseByteArrayFromString(string value)
        {
            try
            {
                UnicodeEncoding encoding = new UnicodeEncoding();
                return encoding.GetBytes(value);
                /*
                System.Runtime.Remoting.Metadata.W3cXsd2001.SoapBase64Binary soap = System.Runtime.Remoting.Metadata.W3cXsd2001.SoapBase64Binary.Parse(value);
                return soap.Value;
                 * */
            }
            /*
        catch (System.Runtime.Remoting.RemotingException ex)
        {
            throw new Exceptions.UnableToDecodeException(string.Format("Unable to convert from string to binary64 on {0} ,Parsing failed", value), Codes.WITSMLParseError, true);
        }
             * */
            catch (Exception)
            {
                throw new Exceptions.UnableToDecodeException(string.Format("Unable to convert from string to binary64 on {0}", value), WITSMLReturnCode.WITSMLParseError, true);
            }
        }

    }
}
