 

using System;
using System.Collections.Generic;
using System.Text;
using System.Xml;
using System.Xml.Serialization;
using System.IO;
using Witsml.Utility;
using Witsml.Common.Exceptions;
using Witsml.Exceptions;
using Witsml.Objects1311;


namespace Witsml
{
    /// <summary>
    /// Stores ANY WITSML Document, decoded and available as an ObjectGraph
    /// </summary>
    public abstract class WITSMLDocument
    {
        //TODO: These may not be needed.
        //private string version = "";
        //private string namespaceURI = "";
        //protected WITSMLObjType witsmlType;

        protected WITSMLDocument()
        {

        }

        // Abstract method to return a single Object from the collection, of type WITSMLObject (Regardless of the actual type).
        public abstract WITSMLObject GetSingleObject(int Item);

        //Abstract method to serialize xml string into witsmldocument object
        public abstract int Decode(ref string xmlIn);

        /// <summary>
        /// Serialize object into xml string,can be overriden
        /// </summary>
        /// <returns>result of serialization</returns>
        public virtual string Serialize()
        {
            //generic serialize
            return this.Serialize(null);
        }

        /// <summary>
        /// Serialize object with override
        /// </summary>
        /// <param name="xmlAttributeOverrides"></param>
        /// <returns></returns>
        protected string Serialize(XmlAttributeOverrides xmlAttributeOverrides)
        {
            XmlSerializer serializer;
            if (xmlAttributeOverrides == null)
                //generic serializationB
                serializer = new XmlSerializer(this.GetType());
            else
                //custom serialization with override
                serializer = new XmlSerializer(this.GetType(), xmlAttributeOverrides);
            //string returned need to be in utf-8
            XmlWriterSettings settings = new XmlWriterSettings();
            settings.Encoding = System.Text.Encoding.UTF8;
            using (MemoryStream ms = new MemoryStream())
            {
                using (XmlWriter writer = XmlWriter.Create(ms, settings))
                {
                    serializer.Serialize(writer, this);
                }
                ms.Seek(0, System.IO.SeekOrigin.Begin);
                using (StreamReader sr = new StreamReader(ms))
                {
                    return sr.ReadToEnd();
                }

            }
        }

        /// <summary>
        /// Return WitsmlDocument Object from xml string using XML deserialization
        /// </summary>
        /// <typeparam name="T">Type of WITSMLDocument</typeparam>
        /// <param name="xmlIn">xml string</param>
        /// <returns>new witsmlDocument</returns>
        public static T Deserialize<T>(ref string xmlIn) where T : WITSMLDocument
        {
            XmlSerializer serializer = new XmlSerializer(typeof(T));
            using (StringReader reader = new StringReader(xmlIn))
            {
                T tObj = (T)serializer.Deserialize(reader);
                return tObj;
            }
        }

        public static WITSMLDocument WITSMLDocumentFactory(WITSMLObjType objType)
        {
            try
            {
                switch (objType.ObjectTypeName)
                {
                    //generic
                    case "well":
                        return new obj_wells();
                    case "wellBore":
                        return new obj_wellbores();
                    case "rig":
                        return new obj_rigs();
                    case "opsReport":
                        return new obj_opsReports();
                    case "formationMarker":
                        return new obj_formationMarkers();
                    case "bhaRun":
                        return new obj_bhaRuns();
                    case "tubular":
                        return new obj_tubulars();
                    case "wbGeometry":
                        return new obj_wbGeometrys();
                    case "target":
                        return new obj_targets();
                    case "surveyProgram":
                        return new obj_surveyPrograms();
                    case "sidewallCore":
                        return new obj_sidewallCores();
                    case "risk":
                        return new obj_risks();
                    case "message":
                        return new obj_messages();
                    case "fluidsReport":
                        return new obj_fluidsReports();
                    case "convCore":
                        return new obj_convCores();
                    case "cementJob":
                        return new obj_cementJobs();
                    case "dtsInstalledSystem":
                        return new obj_dtsInstalledSystems();
                    case "dtsMeasurement":
                        return new obj_dtsMeasurements();
                    //special
                    case "trajectory":
                        return new obj_trajectorys();
                    case "log":
                        return new obj_logs();
                    case "mudLog":
                        return new obj_mudLogs();
                    case "wellLog":
                        return new obj_wellLogs();
                    case "realtime":
                        return new obj_realtimes();
                    default:
                        throw new UnsupportedObjectType("Type Unknown:" + objType.ObjectTypeName,WITSMLReturnCode.WITSMLObjectTypeNotSupported);
                }
            }

            catch (UnsupportedObjectType ex)
            {
                throw (ex);
            }

            catch (Exception ex)
            {
                //TODO: log the error
                throw new UnableToDecodeException("Unable to create WITSML Document from objectType, EX:" + ex.Message,WITSMLReturnCode.WITSMLUnkownCause);
            }
        }        
    }
}
