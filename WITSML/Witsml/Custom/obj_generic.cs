﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Xml.Serialization;
using Witsml.Objects1311;

namespace Witsml.Custom
{
    [XmlType(Namespace = Namespace.Default)]
    [XmlRoot("generics", Namespace = Namespace.Default, IsNullable = false)]
    public partial class obj_generics
    {
        /// <remarks/>
        public cs_documentInfo documentInfo;

        /// <remarks/>
        [XmlElement("generic")]
        public obj_generic[] generic;

        /// <remarks/>
        [XmlAttribute()]
        public string version;
    }

    [XmlType(Namespace = Namespace.Default)]
    [XmlRoot("generic", Namespace = Namespace.Default, IsNullable = false)]
    public partial class obj_generic
    {
        /// <remarks/>
        [XmlAttribute()]
        public string uidWell;

        /// <remarks/>
        [XmlAttribute()]
        public string uidWellbore;

        /// <remarks/>
        [XmlAttribute()]
        public string uid;

        public string nameWell;
        public string nameWellbore;
        public string name;

        /// <remarks/>
        [XmlAnyElement()]
        public System.Xml.Linq.XElement[] Any;

        /// <remarks/>
        public cs_commonData commonData;

        /// <remarks/>
        public cs_customData customData;
    }
}
