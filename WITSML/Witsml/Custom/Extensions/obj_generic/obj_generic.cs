﻿ 

using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Xml.Serialization;
using System.Xml;
using System.IO;
using System.Xml.Linq;
using Witsml.Common.Exceptions;
using Witsml.Exceptions;
using Witsml.Objects1311;

namespace Witsml.Custom
{
    public partial class obj_generic : WITSMLObject, IHandleXML
    {
        public obj_generic()
        {

        }
        public obj_generic(string genericObjectName)
        {
            this.genericObjectName = genericObjectName;
        }
        private string genericObjectName;

        public override void Decode(ref string xmlIn)
        {
            //Consume XML string and convert node value to variable based on node name
            using (StringReader stringReader = new StringReader(xmlIn))
            {
                XmlReaderSettings setting = new XmlReaderSettings();
                setting.ConformanceLevel = ConformanceLevel.Document;
                setting.IgnoreComments = true;
                setting.IgnoreWhitespace = true;
                setting.IgnoreProcessingInstructions = true;
                using (XmlReader xmlReader = XmlReader.Create(stringReader, setting))
                {
                    //determine add or update operation
                    bool isNew;
                    if (this.uid == string.Empty)
                        isNew = true;
                    else isNew = false;

                    this.HandleXML(xmlReader, isNew);
                }
            }
        }

        public override string ValidateGetPath(string newUid)
        {
            // Fix uid if the Object dosn't have one.
            if (!IsValidUid(this.uid))
            {
                this.uid = newUid;
                // TODO: Add some loging that we created a new UID for the object maybe.
            }

            StringBuilder path = new StringBuilder(this.GetParentPath());
            path.Append(@"\");
            path.Append(this.uid);
            return path.ToString();
        }

        public override string GetParentPath()
        {
            if (!IsValidUid(this.uidWell) || !IsValidUid(this.uidWellbore))
            {
                throw new UnableToDecodeException("One or more Parent UID's are invalid", WITSMLReturnCode.WITSMLInvalidParentUID);
                //TODO: Log the issue
            }

            StringBuilder path = new StringBuilder(@"\");
            path.Append(this.uidWell);
            path.Append(@"\");
            path.Append(this.uidWellbore);
            return path.ToString();
        }

        public override string Get_uid()
        {
            return this.uid;
        }

        public override string Get_uidWell()
        {
            return this.uidWell;
        }

        public override string Get_uidWellbore()
        {
            return this.uidWellbore;
        }

        public override string Get_name()
        {
            return this.name;
        }

        public override string Get_nameWell()
        {
            return this.nameWell;
        }

        public override string Get_nameWellbore()
        {
            return this.nameWellbore;
        }

        public override ICommonData Get_commonData()
        {
            if (this.commonData == null)
            {
                this.commonData = new cs_commonData();
            }
            return (ICommonData)this.commonData;
        }

        public override ICustomData Get_customData()
        {
            if (this.customData == null)
            {
                this.customData = new cs_customData();
            }
            return (ICustomData)this.customData;
        }

        public void HandleXML(XmlReader xmlReader, bool isNew)
        {
            bool hasRead = false;
            using (xmlReader)
            {
                //assign uid for the new well
                xmlReader.MoveToContent();
                if (xmlReader.HasAttributes)
                {
                    if (xmlReader.MoveToAttribute("uidWell"))
                        StaticHelper.HandleUID(xmlReader.Value, this.genericObjectName + " uidWell", out this.uidWell);
                    if (xmlReader.MoveToAttribute("uidWellbore"))
                        StaticHelper.HandleUID(xmlReader.Value, this.genericObjectName + " uidWellbore", out this.uidWellbore);
                    if (xmlReader.MoveToAttribute("uid"))
                        StaticHelper.HandleUID(xmlReader.Value, this.genericObjectName + " uid", out this.uid);

                    xmlReader.MoveToElement();
                }

                //temporary list
                List<XElement> listAny = this.Any == null ? new List<XElement>() : new List<XElement>(this.Any);

                while (!xmlReader.EOF)
                {
                    if (!hasRead)
                    {
                        xmlReader.Read();
                    }
                    else
                    {
                        //skip reading on logcurveInfo node
                        hasRead = false;
                    }
                    //only take child node
                    if (xmlReader.Depth == 1 && xmlReader.IsStartElement() && !xmlReader.IsEmptyElement)
                    {
                        switch (xmlReader.Name)
                        {
                            case "nameWell":
                                this.nameWell = StaticHelper.ReadString(xmlReader);
                                break;
                            case "nameWellbore":
                                this.nameWellbore = StaticHelper.ReadString(xmlReader);
                                break;
                            case "name":
                                this.name = StaticHelper.ReadString(xmlReader);
                                break;
                            case "commonData":
                                //extension object
                                if (this.commonData == null)
                                    this.commonData = new cs_commonData();
                                this.commonData.HandleXML(xmlReader.ReadSubtree(), isNew); break;
                            case "customData":
                                //extension object
                                if (this.customData == null)
                                    this.customData = new cs_customData();
                                this.customData.HandleXML(xmlReader.ReadSubtree(), isNew);
                                break;
                            default:
                                XElement element = XElement.ReadFrom(xmlReader) as XElement;
                                var query = from XElement x in listAny
                                            where x.Name == element.Name
                                            select x;

                                if (query.Any())
                                {
                                    this.ReplaceElement(listAny, element);
                                }
                                else
                                {
                                    listAny.Add(element);
                                }

                                hasRead = true;
                                break;
                        }
                    }
                }

                this.Any = listAny.ToArray();
            }
        }

        private void ReplaceElement(List<XElement> list, XElement updateElm)
        {
            for (int i = 0; i < list.Count; i++)
            {
                XElement elm = list[i];
                if (elm.Name == updateElm.Name)
                {
                    list.RemoveAt(i);
                    list.Insert(i, updateElm);
                    break;
                }
            }
        }

        public override string Serialize()
        {
            XmlSerializer xmlSerializer = this.GetSerializer();
            return base.Serialize(xmlSerializer);
        }

        public override string SerializeClean()
        {
            XmlSerializer xmlSerializer = this.GetSerializer();
            return base.SerializeClean(xmlSerializer);
        }

        private XmlSerializer GetSerializer()
        {
            XmlAttributeOverrides xmlAttributeOverrides = this.getRootElementNameOverride();
            if (xmlAttributeOverrides == null)
                //generic serialization
                return new XmlSerializer(this.GetType());
            else
                //custom serialization with override
                return XmlSerializerFactory.Create(this.GetType(), xmlAttributeOverrides, this.genericObjectName);
        }

        private XmlAttributeOverrides getRootElementNameOverride()
        {
            //Change Root Attribute Element Name
            XmlAttributes xmlAtts = new XmlAttributes();
            xmlAtts.XmlRoot = new XmlRootAttribute();
            xmlAtts.XmlRoot.ElementName = genericObjectName;

            //specify override for logData which accept ignore attribute
            XmlAttributeOverrides xmlAttsOverrides = new XmlAttributeOverrides();
            xmlAttsOverrides.Add(typeof(obj_generic), xmlAtts);

            return xmlAttsOverrides;
        }
    }
}
