﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Xml.Serialization;
using System.Xml;
using System.IO;
using Witsml.Common.Exceptions;
using Witsml.Objects1311;

namespace Witsml.Custom
{
    public partial class obj_generics : WITSMLDocument
    {
        public obj_generics()
        {
        }
        public obj_generics(string objectName)
        {
            this.genericObjectName = objectName;
        }
        /// <summary>
        /// The generic object name
        /// </summary>
        protected string genericObjectName;

        public override WITSMLObject GetSingleObject(int Item)
        {
            return this.generic[Item];
        }

        public override int Decode(ref string xmlIn)
        {
            //return value
            //number of report inside xmlIn
            int countOfReport = 0;
            try
            {
                //check whether collection is from stratch(isNew = true) or from deserialization(isNew=false)
                bool isNew = this.generic == null;

                //create temporary list to help with data manipulation
                List<obj_generic> listObject = isNew ? new List<obj_generic>() : new List<obj_generic>(this.generic);

                //Consume XML string and convert node value to variable based on node name
                using (StringReader stringReader = new StringReader(xmlIn))
                {
                    XmlReaderSettings setting = new XmlReaderSettings();
                    setting.ConformanceLevel = ConformanceLevel.Document;
                    setting.IgnoreComments = true;
                    setting.IgnoreWhitespace = true;
                    setting.IgnoreProcessingInstructions = true;
                    using (XmlReader xmlReader = XmlReader.Create(stringReader, setting))
                    {
                        while (xmlReader.Read())
                        {
                            //assign version
                            xmlReader.MoveToContent();
                            if (xmlReader.MoveToAttribute("version"))
                                this.version = xmlReader.Value;
                            else
                                this.version = "1.3.1.1";//no version specified,set it to 1.3.1.1

                            //pre-checking on whether node is the correct child based on depth,start tag and not empty
                            if (xmlReader.Depth == 1 && xmlReader.IsStartElement() && !xmlReader.IsEmptyElement)
                            {
                                if (xmlReader.Name == genericObjectName)
                                {
                                    obj_generic reportObj;
                                    //foreign key(well id,wellbore id) and primary key(wellbore id) to identify existing mudLog
                                    string currentUIDWell = string.Empty;
                                    string currentUIDWellbore = string.Empty;
                                    string currentUID = string.Empty;

                                    //Update mudLog
                                    if (!isNew)
                                    {
                                        //check for uid attribute and assign it to temporary string for searching in collection
                                        if (xmlReader.HasAttributes)
                                        {
                                            if (xmlReader.MoveToAttribute("uidWell"))
                                            {
                                                currentUIDWell = xmlReader.Value;
                                            }
                                            if (xmlReader.MoveToAttribute("uidWellbore"))
                                            {
                                                currentUIDWellbore = xmlReader.Value;
                                            }
                                            if (xmlReader.MoveToAttribute("uid"))
                                            {
                                                currentUID = xmlReader.Value;
                                            }
                                            xmlReader.MoveToElement();
                                        }
                                        //look for log based on its UID and update the log with xml string
                                        if (currentUID != string.Empty)
                                        {
                                            reportObj = listObject.FirstOrDefault(
                                                delegate(obj_generic objReport)
                                                { if (objReport.uidWell == currentUIDWell && objReport.uidWellbore == currentUIDWellbore && objReport.uid == currentUID)return true; else return false; });
                                            if (reportObj != null)
                                            {
                                                reportObj.HandleXML(xmlReader.ReadSubtree(), isNew);
                                                countOfReport++;
                                                continue;
                                            }
                                            else
                                            {
                                                //Update can't process rig without match
                                                continue;
                                            }
                                        }
                                        else
                                        {
                                            //Update can't process rig without uid
                                            continue;
                                        }
                                    }

                                    //Add New Generic Object
                                    reportObj = new obj_generic(genericObjectName);
                                    reportObj.HandleXML(xmlReader.ReadSubtree(), true);
                                    listObject.Add(reportObj);
                                    countOfReport++;
                                }
                                else
                                {
                                    switch (xmlReader.Name)
                                    {
                                        case "documentInfo":
                                            if (this.documentInfo == null)
                                                this.documentInfo = new cs_documentInfo();
                                            this.documentInfo.HandleXML(xmlReader.ReadSubtree(), isNew);
                                            break;
                                        default:
                                            break;
                                    }
                                }                                
                            }
                        }
                        //assign list to report collection
                        this.generic = listObject.ToArray();
                    }
                }
            }
            catch (XmlException ex)
            {
                throw new Exceptions.UnableToDecodeException(ex.Message, WITSMLReturnCode.WITSMLDocumentError, ex.InnerException);
            }
            catch (NullReferenceException ex)
            {
                throw new Exceptions.UnableToDecodeException(ex.Message, WITSMLReturnCode.WITSMLEmptyDocument, ex.InnerException);
            }
            catch (Exceptions.UnableToDecodeException ex)
            {
                throw ex;
            }
            catch (Exception ex)
            {
                throw new Exceptions.UnableToDecodeException(ex.Message, WITSMLReturnCode.WITSMLUnkownCause, ex.InnerException);
            }
            return countOfReport;
        }
    }
}
