 
//Comment added by feroz
using System;
using System.Collections.Generic;
using System.Text;
using System.IO;
using System.Xml;
using System.Net;


namespace Witsml.Client
{
    // Structure containing responses from the Remote Call.
    // This structure is used for the result of all calls for simplicity.  For some calls, not all values will be returned.

    public class WMLSCallResult
    {
        public double duration;
        public string suppMsgOut;
        /// <summary>
        /// 1	Function completed successfully
        ///
        /// -1nn: 	Parameter errors
        ///
        ///-101	Parameter error: Invalid/missing WITSML object type
        ///-102	Parameter error: Invalid/missing XML
        ///-103	Parameter error: Invalid/missing selection criteria
        ///-104	Parameter error: Invalid/missing server name
        ///-105	Parameter error: Invalid/missing publisher name
        ///-106	Parameter error: Invalid/missing subscriber name
        ///-107	Parameter error: Invalid/missing real-time data type name
        ///-108	Parameter error: Invalid/missing real-time data
        ///-109	Parameter error: Invalid/missing XML Schema (XSD) path/filename
        ///-110	Parameter error: Invalid option
        ///-111	Parameter error: Invalid/missing subscriber process location
        ///-112	Parameter error: Invalid/missing subscriber secure indicator
        ///-199	Parameter error: unknown cause
        ///
        ///-2nn: 	Persistent Store errors
        ///
        ///-201	Persistent Store error: Duplicate key
        ///-202	Persistent Store error: Could not parse XML
        ///-203	Persistent Store error: Could not map XML
        ///-204	Persistent Store error: No data matched selection criteria
        ///-205	Persistent Store error: The query results are too large
        ///-299	Persistent Store error: unknown cause
        ///
        ///
        ///-3nn:	XML Schema errors
        ///
        ///-301	XML Schema error: Error while parsing the specified XML Schema (XSD) file
        ///-302	XML Schema error: specified XML Schema (XSD) has invalid content
        ///-303	XML Schema error: no base attribute present below data type definition element
        ///-304	XML Schema error: nesting of data types exceeds maximum - check for circular
        ///reference in definitions
        ///-305	XML Schema error: unrecognized XML Schema data type
        ///-306	XML Schema error: circular reference (loop) in included schemas
        ///-399	XML Schema error: unknown cause
        ///
        ///
        ///-9nn:	program logic/exception errors
        ///
        ///-901	Program Error: problem loading internal program or component
        ///-902	Program Error: XML Schema data type is not presently supported by the  WITSML API
        ///-999	Program Error: unknown cause
        ///
        /// -10001: connection time out
        /// -10002: connection failure
        /// </summary>
        public short returnCode;
        public string errorMessage;
        public string XMLOut;
        public string BaseMessage;

        // Provides formatted version of the returned XML
        // Not it ONLY spends time formating the string if, and when its needed (i.e. accessed, not for all calls);
        public string XMLOut_Formated
        {
            get 
            {
                try // If formating fails for any reason, return the base XMLOut String.
                {
                    return this.FormatXmlString(XMLOut);
                }
                catch
                {
                    return XMLOut;
                }

            }
        }

        // Constructor
        public WMLSCallResult()
        {
            this.clear();
        }

        // Clear all params and set to null/empty values.
        public void clear()
        {
            this.duration = 0;
            this.suppMsgOut = "";
            this.returnCode = 0;
            this.errorMessage = "";
            this.XMLOut = "";
            this.BaseMessage = "";
        }

        // Generates a 'summary' of a result as a human readable string.
        // Commonly used for logging a result.
        // Only generated when needed, so as not to impact 'normal' operations.

        public string Sumary()
        {
            try
            {
                StringBuilder sumary = new StringBuilder();
                sumary.AppendFormat("  CallDuration: {0:#.###} sec", this.duration);
                sumary.AppendLine();
                sumary.AppendFormat("  Return Code : {0:#}", this.returnCode);
                sumary.AppendLine();
                sumary.AppendFormat("  SuppMsgOut  : {0:s}", this.suppMsgOut);
                sumary.AppendLine();
                if (this.BaseMessage != "")
                {
                    sumary.AppendFormat("  Base Message: {0:s} /n", this.BaseMessage);
                    sumary.AppendLine();
                }

                if (this.XMLOut != "")
                {
                    sumary.AppendLine("  XML OUT     : ");
                    sumary.AppendLine(this.XMLOut_Formated);
                }

                return sumary.ToString();
            }
            catch 
            {
                return "";
            }
        }


        // Formats XML in a nice tabbed manner for display.
        private string FormatXmlString(string stringToFormat)
        {
            string retStr = string.Empty;

            try
            {
                if (!string.IsNullOrEmpty(stringToFormat))
                {
                    stringToFormat = stringToFormat.Trim();

                    MemoryStream myMS = new MemoryStream();
                    XmlTextWriter myXmlTextWriter = null;
                    StreamReader myStreamReader = null;

                    try
                    {
                        XmlDocument xmlDOM = new XmlDocument();
                        xmlDOM.LoadXml(stringToFormat);

                        //create a XmlTextWriter and tell it which stream to write too
                        myXmlTextWriter = new XmlTextWriter(myMS, System.Text.Encoding.UTF8);
                        //Enable indentation
                        myXmlTextWriter.Formatting = Formatting.Indented;
                        myXmlTextWriter.Indentation = 3;

                        //write the content of the XmlDocument object to the XmlTextWriter
                        xmlDOM.WriteTo(myXmlTextWriter);
                        myXmlTextWriter.Flush();

                        //Read it back out of the MemoryStream to a String var.
                        myMS.Seek(0, SeekOrigin.Begin);
                        myStreamReader = new StreamReader(myMS);
                        retStr = myStreamReader.ReadToEnd();
                    }
                    finally
                    {
                        if (myMS != null)
                        {
                            myMS.Close();
                        }
                        if (myStreamReader != null)
                        {
                            myStreamReader.Close();
                        }
                        if (myXmlTextWriter != null)
                        {
                            myXmlTextWriter.Close();
                        }
                    }
                }

                return retStr;
            }
            catch
            {
                return stringToFormat;
            }
        }
    }
}
