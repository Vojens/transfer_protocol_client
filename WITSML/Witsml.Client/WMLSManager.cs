using System;
using System.Collections.Generic;
using System.Text;

namespace Witsml.Client
{
    public class WMLSManager
    {
        private string m_url;

        public string Url
        {
            get { return m_url; }
            set { m_url = value; }
        }

        private string m_user;

        public string User
        {
            get { return m_user; }
            set { m_user = value; }
        }

        private string m_password;

        public string Password
        {
            get { return m_password; }
            set { m_password = value; }
        }
	
        public WMLSManager(string url, string user, string password)
        {
            m_url = url;
            m_user = user;
            m_password = password;
        }

        public void Process(string methodName, string wmlsType, string xmlQueryIn, string optionsIn, out string xmlResponse
            , out WMLSCallResult wmlsCallResult, out bool isExpectXmlResponseReturn)
        {
            xmlResponse = string.Empty;
            wmlsCallResult = new WMLSCallResult();
            isExpectXmlResponseReturn = false;

            WMLSClient wmlsClient = new WMLSClient(m_url, m_user, m_password);

            switch (methodName)
            {
                case MethodConstant.METHOD_GETFROMSTORE:
                    xmlResponse = wmlsClient.GetFromStore(wmlsType, xmlQueryIn, optionsIn, out wmlsCallResult);
                    isExpectXmlResponseReturn = true;
                    break;

                case MethodConstant.METHOD_ADDTOSTORE:
                    wmlsClient.AddToStore(wmlsType, xmlQueryIn, optionsIn, out wmlsCallResult);
                    break;

                case MethodConstant.METHOD_DELETEFROMSTORE:
                    wmlsClient.DeleteFromStore(wmlsType, xmlQueryIn, optionsIn, out wmlsCallResult);
                    break;

                case MethodConstant.METHOD_UPDATEINSTORE:
                    wmlsClient.UpdateInStore(wmlsType, xmlQueryIn, optionsIn, out wmlsCallResult);
                    break;
            }
        }

        public void Process(string methodName, string wmlsType, string xmlQueryIn, string optionsIn
            , out WMLSCallResult wmlsCallResult)
        {
            string xmlResponse = string.Empty;
            wmlsCallResult = new WMLSCallResult();
            bool isExpectXmlResponseReturn = false;

            Process(methodName, wmlsType, xmlQueryIn, optionsIn, out xmlResponse, out wmlsCallResult
                , out isExpectXmlResponseReturn);
        }
    }
}
