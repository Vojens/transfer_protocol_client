 

using System;
using System.Collections.Generic;
using System.Text;

namespace Witsml.Client
{
    /// <summary>
    /// Constant contains VALID method for step testing.
    /// </summary>
    public struct MethodConstant
    {
        public const string METHOD_GETFROMSTORE = "GetFromStore";
        public const string METHOD_ADDTOSTORE = "AddToStore";
        public const string METHOD_DELETEFROMSTORE = "DeleteFromStore";
        public const string METHOD_UPDATEINSTORE = "UpdateInStore";

        //  Has passed UnitTest.
        /// <summary>
        /// Check if constant is in the list of VALID constant.
        /// </summary>
        /// <param name="constant">Constant that will be checked.</param>
        /// <returns>Boolean status of constant is in this class or not.</returns>
        public static bool IsValidConstant(string constant)
        {
            return (constant == METHOD_ADDTOSTORE || constant == METHOD_DELETEFROMSTORE || constant == METHOD_GETFROMSTORE
                || constant == METHOD_UPDATEINSTORE);
        }
    }
}
