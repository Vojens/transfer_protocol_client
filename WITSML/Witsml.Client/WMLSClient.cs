using System;
using System.Collections.Generic;
using System.Text;
using System.IO;
using System.Xml;
using System.Net;
using System.Threading;
using System.Configuration;

namespace Witsml.Client
{

    /// <summary>
    /// WITSML Client Access Library.
    /// This library provides all the basic functions for calling a remote WITSML Webserverice, and returning the results
    /// It also deals with most 'normal' error conditions when calling a remote service.
    /// </summary>
    public class WMLSClient
    {

        private WMLSProxy WMLSProxy;
        public string capabilitiesIn;

        // Mutex for Logfile Synchronisation.  This will be used for all 'logging' in this threadspace.
        // Expect this to be replaced in the future by an external logging class, that handles write queueing etc. (TODO:)

        // NOTE: This protects the log contained within THIS class, to ensure this class is threadsafe.  it does NOT protect the log file itself in the case where
        //        Another instance uses the same Logfile (which it shouldn't).  
        private Mutex logMutex;

        // Constructor
        public WMLSClient()
        {
            logMutex = new Mutex();
            WMLSProxy = new WMLSProxy();

            //  Add timeout.
            try
            {
                int clientTimeOut;
                if (Int32.TryParse(ConfigurationManager.AppSettings["ClientTimeOut"], out clientTimeOut))
                {
                    if (clientTimeOut > 0)
                    {
                        WMLSProxy.Timeout = clientTimeOut;
                    }
                }
            }
            catch { }

            m_LogEnabled = false;
            m_LogFileName = AppDomain.CurrentDomain.BaseDirectory + "\\" + "DefaultCallLog.txt";  // Default log in Current directory
            System.Net.ServicePointManager.CertificatePolicy = new CertificatePolicyOverride();
        }

        // Constructor override to provide URL, but no credentials
        public WMLSClient(string url)
            : this()
        {
            this.URL = url;
            this.SetCredentials("", "");
        }

        // Constructor override to provide credentials.
        public WMLSClient(string url, string user, string password)
            : this()
        {
            this.URL = url;
            this.SetCredentials(user, password);
        }

        public WMLSClient(string url, string token)
            : this()
        {
            this.URL = url;
            this.SetCredentials(token);
        }

        /// <summary>
        /// Constructor override to provide credentials and time out
        /// </summary>
        /// <param name="url">the base URL of the XML Web service the client is requesting</param>
        /// <param name="user"></param>
        /// <param name="password"></param>
        /// <param name="clientTimeOut">Indicates the time an XML Web service client waits for a synchronous XML Web service request to complete (in milliseconds)</param>
        /// <remarks>21 July 2010: Yohan Prasetia Siswanto</remarks>
        public WMLSClient(string url, string user, string password, int clientTimeOut)
            : this()
        {
            this.URL = url;
            this.SetCredentials(user, password);
            if (clientTimeOut > 0)
            {
                WMLSProxy.Timeout = clientTimeOut;
            }
        }

        // Constructor override to provide credentials and useragent.
        public WMLSClient(string url, string user, string password, string UserAgent)
            : this()
        {
            this.URL = url;
            this.SetCredentials(user, password);
            if (!string.IsNullOrEmpty(UserAgent))
            {
                WMLSProxy.UserAgent = UserAgent;
            }
        }

        // Public Property for the URL, which controls the URL on the Proxy.
        // TODO: may add code here to check syntax.
        public string URL
        {
            get
            {
                return WMLSProxy.Url;

            }
            set
            {
                WMLSProxy.Url = value;
            }
        }

        //Public Property to control logging, either enabled or not
        private bool m_LogEnabled;

        public bool LogEnabled
        {
            get { return m_LogEnabled; }
            set { m_LogEnabled = value; }
        }

        //Public Property for Logfilename - When set, checks to see if the directory exists, and if it dosn't, creates it. 
        // Maybe changed at anytime for Rolling Logfiles
        // TODO:  The log handling section may be replaced by a Logging Library. If so, this code will prob move to the library.
        private string m_LogFileName;

        public string LogFileName
        {
            get
            {
                return m_LogFileName;
            }

            set // Makes sure the directory exists to write too, and creates it if not.
            {
                try
                {
                    string dirName = Path.GetDirectoryName(value);
                    if (Directory.Exists(dirName))  // Exists ? if not, create it
                    {
                        Directory.CreateDirectory(dirName);
                    }

                    // Seem all ok, assign it.
                    m_LogFileName = value;
                }
                catch
                {
                    // Do nothing, but leave the name unchanged.
                    // TODO: Generate a Custom exception 'unable to create log directory'
                }

            }
        }


        /// <summary>
        /// // Function to allow setting/changing of the url and user/password for the server
        /// </summary>
        /// <param name="url"></param>
        /// <param name="user"></param>
        /// <param name="password"></param>
        public void SetCredentials(string user, string password)
        {
            try
            {
                if (user != "")
                {

                    NetworkCredential netCredential = new NetworkCredential(user, password);
                    Uri uri = new Uri(this.URL);
                    ICredentials credentials = netCredential.GetCredential(uri, "Basic");

                    WMLSProxy.Credentials = credentials;
                    WMLSProxy.PreAuthenticate = true;  //Required for Riglink Servers or anyother using JAVA SOAP Webserives

                }
                capabilitiesIn = "";
            }
            catch
            {
            }
        }

        /// <summary>
        /// Enables Proxy credentials, and turns on proxy authentication.
        /// </summary>
        /// <param name="proxyURL"></param>
        /// <param name="user"></param>
        /// <param name="password"></param>
        public void SetProxyCredentials(string proxyURL, string user, string password)
        {
            try
            {
                IWebProxy proxyObject = new WebProxy(proxyURL, false);
                WMLSProxy.Proxy = proxyObject;
            }
            catch
            {
            }
        }

        public void SetCredentials(string samlToken)
        {
            try
            {
                if (!string.IsNullOrEmpty(samlToken))
                {
                    //HttpWebRequest webRequest = (HttpWebRequest)WebRequest.Create(this.URL);
                    //byte[] credentialBuffer = new System.Text.UTF8Encoding().GetBytes(samlToken);
                    //if (webRequest.Headers["SamlToken"] == null)
                    //    webRequest.Headers.Add("SamlToken", Convert.ToBase64String(credentialBuffer));

                    byte[] credentialBuffer = new System.Text.UTF8Encoding().GetBytes(samlToken);
                    WMLSProxy.Token = Convert.ToBase64String(credentialBuffer);
                    WMLSProxy.PreAuthenticate = false;
                }
                capabilitiesIn = "";
            }
            catch
            {
            }
        }


        /// <summary>
        /// Clears Proxy credentials, and turns off
        /// </summary>
        public void ClearProxyCredentials()
        {
            try
            {
                WMLSProxy.Proxy = null;
            }
            catch
            {
            }
        }



        /// <summary>
        /// Allows clients to set a useragent text, based on Format as per API 141F.
        /// </summary>
        /// <param name="ProductNameAndVersion">
        /// <para>ProductName/ProductVersion</para>
        /// <para>Can provide multiple ProductName/ProductVersion, PROVIDED the ProductName/ProductVersion are seprated by a white space</para>
        /// <para>Eg. for single product name and version</para>
        /// <para>string value = "WITSML-Workbench/1.2.3.4"</para>
        /// <para>Eg. for mutiple product name and version</para>
        /// <para>string value = "WITSML-Workbench/1.2.3.4 WITSML-Client/0.1.2.3"</para>
        /// <para>NOTE: In the mutiple prod name and version example there is a space between the values</para></param>
        public void SetUserAgent(string ProductNameAndVersion)
        {
            const string UserAgentPatternStart = "User-Agent: ";
            this.WMLSProxy.UserAgent = UserAgentPatternStart + ProductNameAndVersion;
        }


        // Internal Helper function for consistent Exception Handling
        // Handle the Exception's, and deal with various return Codes.
        // TODO: Add returned custom exception to potentially throw back.
        private string HandleWebException(WebException wex, ref WMLSCallResult result)
        {

            HttpWebResponse response = wex.Response as HttpWebResponse;
            if ((response != null) && (response.StatusCode == HttpStatusCode.Unauthorized))
            {
                //TODO:  Need to do something more intelligent with errors and status.
                result.errorMessage = "Unauthorised: " + wex.Message;
                result.returnCode = -1;  //TODO: need to put in actual WITSML Return code for UNAUTHORISED here
            }
            else
            {
                //21 July 2010: Yohan Prasetia Siswanto
                //more detailed return code for web exception
                switch (wex.Status)
                {
                    case WebExceptionStatus.Timeout:
                        result.returnCode = -10001;
                        break;
                    case WebExceptionStatus.ConnectFailure:
                    case WebExceptionStatus.NameResolutionFailure:
                        result.returnCode = -10002;
                        break;
                    default:
                        result.returnCode = -1;
                        break;
                }

                result.errorMessage = "Web Exception:" + wex.Message;

            }
            return "";
        }

        private string HandleGeneralException(Exception ex, ref WMLSCallResult result)
        {
            //TODO: Need to add logging here and other handling
            result.clear();
            result.errorMessage = ex.Message;
            result.returnCode = -1;
            return "";
        }


        /// <summary>
        /// Call WMLS Store function GETBASEMESSAGE
        /// Returns the base message, and provides additional call information in the result class
        /// </summary>
        /// <param name="ReturnValue"></param>
        /// <param name="result"></param>
        /// <returns></returns>
        public string GetBaseMsg(short ReturnValue, out WMLSCallResult result)
        {
            string baseMessage = null;
            result = new WMLSCallResult();
            try
            {
                HiPerfTimer hpt = new HiPerfTimer();

                // Carry out call, wrapped in HighPerfTimer code for most accurate timing.
                hpt.Start();
                baseMessage = WMLSProxy.WMLS_GetBaseMsg(ReturnValue);
                hpt.Stop();

                // Deal with results
                result.duration = hpt.FinalDuration;
                result.returnCode = 1;
                result.BaseMessage = baseMessage;
                if (this.m_LogEnabled)
                    WriteLog("GETBASEMESSAGE", result.Sumary());
                return baseMessage;
            }

            // Handle misc communications issues.  These are expected issues, and need to be handled by the class
            catch (WebException wex)
            {
                return HandleWebException(wex, ref result);
            }
            // Runtime issues. 
            catch (Exception ex)
            {
                return HandleGeneralException(ex, ref result);
            }
            finally
            {
            }
        }

        /// <summary>
        /// Call GetCap (Get Capabilities) and return the result.
        /// </summary>
        /// <param name="optionsIN"></param>
        /// <param name="result"></param>
        /// <returns></returns>
        public string GetCap(string optionsIN, out WMLSCallResult result)
        {
            result = new WMLSCallResult();
            try
            {
                HiPerfTimer hpt = new HiPerfTimer();

                // Carry out call, wrapped in HighPerfTimer code for most accurate timing.
                hpt.Start();
                result.returnCode = WMLSProxy.WMLS_GetCap(optionsIN, out result.XMLOut, out result.suppMsgOut);
                hpt.Stop();

                // Deal with results
                result.duration = hpt.FinalDuration;
                result.returnCode = 1;
                if (this.m_LogEnabled)
                    WriteLog("GETCAP", result.Sumary());
                return result.XMLOut;
            }
            // Handle misc communications issues.  These are expected issues, and need to be handled by the class
            catch (WebException wex)
            {
                return HandleWebException(wex, ref result);
            }
            // Runtime issues. 
            catch (Exception ex)
            {
                return HandleGeneralException(ex, ref result);
            }
            finally
            {
            }
        }

        /// <summary>
        /// Call GetVersion on remote store, to return the version of API and objects that it supports.
        /// </summary>
        /// <param name="result"></param>
        /// <returns></returns>
        public string GetVersion(out WMLSCallResult result)
        {
            result = new WMLSCallResult();
            try
            {
                HiPerfTimer hpt = new HiPerfTimer();

                // Carry out call, wrapped in HighPerfTimer code for most accurate timing.
                hpt.Start();
                result.XMLOut = WMLSProxy.WMLS_GetVersion();
                hpt.Stop();

                // Deal with results
                result.duration = hpt.FinalDuration;
                result.returnCode = 1;
                if (this.m_LogEnabled)
                    WriteLog("GETVERSION", result.Sumary());
                return result.XMLOut;
            }
            // Handle misc communications issues.  These are expected issues, and need to be handled by the class
            catch (WebException wex)
            {
                return HandleWebException(wex, ref result);
            }
            // Runtime issues. 
            catch (Exception ex)
            {
                return HandleGeneralException(ex, ref result);
            }
            finally
            {
            }
        }

        /// <summary>
        /// Call GETFROMSTORE on remote server.
        /// </summary>
        /// <param name="WMLTypeIn"></param>
        /// <param name="QueryIn"></param>
        /// <param name="optionsIN"></param>
        /// <param name="result"></param>
        /// <returns></returns>
        public string GetFromStore(string WMLTypeIn, string QueryIn, string optionsIN, out WMLSCallResult result)
        {
            result = new WMLSCallResult();
            //this.SetProxyCredentials("http://localhost:8080", "", "");
            try
            {
                HiPerfTimer hpt = new HiPerfTimer();
                // Carry out call, wrapped in HighPerfTimer code for most accurate timing.
                hpt.Start();
                result.returnCode = WMLSProxy.WMLS_GetFromStore(WMLTypeIn, QueryIn, optionsIN, this.capabilitiesIn, out result.XMLOut, out result.suppMsgOut);
                hpt.Stop();

                // Deal with results
                result.duration = hpt.FinalDuration;
                if (this.m_LogEnabled)
                    WriteLog("GETFROMSTORE", result.Sumary());
                return result.XMLOut;
            }
            // Handle misc communications issues.  These are expected issues, and need to be handled by the class
            catch (WebException wex)
            {
                return HandleWebException(wex, ref result);
            }
            // Runtime issues. 
            catch (Exception ex)
            {
                return HandleGeneralException(ex, ref result);
            }
            finally
            {
            }
        }

        /// <summary>
        /// Call AddtoStore on remote server.
        /// </summary>
        /// <param name="WMLTypeIn"></param>
        /// <param name="XMLIn"></param>
        /// <param name="optionsIN"></param>
        /// <param name="result"></param>
        public void AddToStore(string WMLTypeIn, string XMLIn, string optionsIN, out WMLSCallResult result)
        {
            string baseMessage = null;
            result = new WMLSCallResult();
            try
            {
                HiPerfTimer hpt = new HiPerfTimer();

                // Carry out call, wrapped in HighPerfTimer code for most accurate timing.
                hpt.Start();
                result.returnCode = WMLSProxy.WMLS_AddToStore(WMLTypeIn, XMLIn, optionsIN, this.capabilitiesIn, out result.suppMsgOut);
                hpt.Stop();

                // Deal with results
                result.duration = hpt.FinalDuration;
                if (this.m_LogEnabled)
                    WriteLog("ADDTOSTORE", result.Sumary());
                result.XMLOut = baseMessage;
            }
            // Handle misc communications issues.  These are expected issues, and need to be handled by the class
            catch (WebException wex)
            {
                HandleWebException(wex, ref result);
            }
            // Runtime issues. 
            catch (Exception ex)
            {
                HandleGeneralException(ex, ref result);
            }
            finally
            {
            }
        }

        /// <summary>
        /// Call UpdateinStore on remote server
        /// </summary>
        /// <param name="WMLTypeIn"></param>
        /// <param name="XMLIn"></param>
        /// <param name="optionsIN"></param>
        /// <param name="result"></param>
        public void UpdateInStore(string WMLTypeIn, string XMLIn, string optionsIN, out WMLSCallResult result)
        {
            string baseMessage = null;
            result = new WMLSCallResult();
            try
            {
                HiPerfTimer hpt = new HiPerfTimer();

                // Carry out call, wrapped in HighPerfTimer code for most accurate timing.
                hpt.Start();
                result.returnCode = WMLSProxy.WMLS_UpdateInStore(WMLTypeIn, XMLIn, optionsIN, this.capabilitiesIn, out result.suppMsgOut);
                hpt.Stop();

                // Deal with results
                result.duration = hpt.FinalDuration;
                result.XMLOut = baseMessage;
                if (this.m_LogEnabled)
                    WriteLog("UPDATEINSTORE", result.Sumary());
            }
            // Handle misc communications issues.  These are expected issues, and need to be handled by the class
            catch (WebException wex)
            {
                HandleWebException(wex, ref result);
            }
            // Runtime issues. 
            catch (Exception ex)
            {
                HandleGeneralException(ex, ref result);
            }
            finally
            {
            }
        }

        /// <summary>
        /// Call DeletefromStore on remote server
        /// </summary>
        /// <param name="WMLTypeIn"></param>
        /// <param name="XMLIn"></param>
        /// <param name="optionsIN"></param>
        /// <param name="result"></param>
        public void DeleteFromStore(string WMLTypeIn, string XMLIn, string optionsIN, out WMLSCallResult result)
        {
            string baseMessage = null;
            result = new WMLSCallResult();
            try
            {
                HiPerfTimer hpt = new HiPerfTimer();

                // Carry out call, wrapped in HighPerfTimer code for most accurate timing.
                hpt.Start();
                result.returnCode = WMLSProxy.WMLS_DeleteFromStore(WMLTypeIn, XMLIn, optionsIN, this.capabilitiesIn, out result.suppMsgOut);
                hpt.Stop();

                // Deal with results
                result.duration = hpt.FinalDuration;
                result.XMLOut = baseMessage;
                if (this.m_LogEnabled)
                    WriteLog("DELETEFROMSTORE", result.Sumary());
            }
            // Handle misc communications issues.  These are expected issues, and need to be handled by the class
            catch (WebException wex)
            {
                HandleWebException(wex, ref result);
            }
            // Runtime issues. 
            catch (Exception ex)
            {
                HandleGeneralException(ex, ref result);
            }
            finally
            {
            }
        }


        // Base Logging function.  Expect these to be replaced in the future.
        private void WriteLog(string call, string message)
        {
            if (this.m_LogEnabled)
            {
                try
                {
                    logMutex.WaitOne();  // Thread Sync - Capture Mutex

                    // Open the file, write the log entry and close it
                    // Not efficient, but is threadsafe, wrapped in the Mutex.  
                    // Logging in this manner should be on for debugging only.

                    StringBuilder logEntry = new StringBuilder();
                    logEntry.AppendFormat("{0:yyyy-MM-dd HH:ss} {1:s} to host: {2:s}", System.DateTime.Now, call, this.URL);
                    logEntry.AppendLine();
                    logEntry.AppendLine(message);

                    StreamWriter sw = new StreamWriter(this.m_LogFileName, true);
                    sw.WriteLine(logEntry.ToString());
                    sw.Close();
                }
                catch
                {
                }

                finally  // release the Mutex in the Finally block, to ensure its done.
                {
                    logMutex.ReleaseMutex();
                }

            }
        }

    }
}
