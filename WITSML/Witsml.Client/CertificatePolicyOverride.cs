using System;
using System.Collections.Generic;
using System.Text;
using System.Net;
using System.Security.Cryptography.X509Certificates;

namespace Witsml.Client
{
    // Call back class which overrides the certificate check, and forces it to be 'true', regardless of weather the remote
    // names match or not.
    public class CertificatePolicyOverride : ICertificatePolicy
    {
        public bool CheckValidationResult(
              ServicePoint srvPoint
            , X509Certificate certificate
            , WebRequest request
            , int certificateProblem)
        {
            //Return True to force the certificate to be accepted.
            return true;

        }
    }
}