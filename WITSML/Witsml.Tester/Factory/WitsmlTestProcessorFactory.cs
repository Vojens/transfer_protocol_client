using System;
using System.Collections.Generic;
using System.Text;
using Support.Common;
using Witsml.Tester;
using Witsml.Tester.Parameter;

namespace Witsml.Tester
{
    public class WitsmlTestProcessorFactory
    {
        //  Has passed UnitTest.
        /// <summary>
        /// Gets processor based on information from parameter.
        /// </summary>
        /// <param name="parameter">Parameter contains information to create processor.</param>
        /// <param name="isEnableLog">Boolean that determine whether the processor will log the error or not.</param>
        /// <returns>AbstractWitsmlTestProcessor</returns>
        public static AbstractWitsmlTestProcessor<ReportDetail> GetProcessor(WitsmlTestParameterString parameter
            , bool isEnableLog)
        {
            if (parameter == null)
            {
                throw new WitsmlTestException("Witsml test parameter is null.");
            }

            try
            {
                //  Make sure all parameters needed are valid, ie: Fixtre, Suite, Url.
                parameter.Validate();
            }
            catch (WitsmlTestParameterException wmlTstParamEx)
            {
                //  Rewrite exception so it will be known as WitsmlTestException.
                throw new WitsmlTestException(wmlTstParamEx.Message);
            }

            if (!string.IsNullOrEmpty(parameter.FixtureFilePath))
            {
                return new FixtureProcessor(parameter.Url, parameter.User, parameter.Password, parameter.FixtureFilePath
                    , isEnableLog, false, parameter.IgnoreResponseTime);
            }
            else
            {
                return new SuiteProcessor(parameter.Url, parameter.User, parameter.Password, parameter.SuiteFilePath
                    , isEnableLog, parameter.IgnoreResponseTime);
            }
        }
    }
}
