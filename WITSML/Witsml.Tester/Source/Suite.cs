﻿//------------------------------------------------------------------------------
// <auto-generated>
//     This code was generated by a tool.
//     Runtime Version:2.0.50727.832
//
//     Changes to this file may cause incorrect behavior and will be lost if
//     the code is regenerated.
// </auto-generated>
//------------------------------------------------------------------------------

// 
// This source code was auto-generated by xsd, Version=2.0.50727.42.
// 
namespace Witsml.Tester {
    using System.Xml.Serialization;
    
    
    /// <remarks/>
    [System.CodeDom.Compiler.GeneratedCodeAttribute("xsd", "2.0.50727.42")]
    [System.SerializableAttribute()]
    [System.Diagnostics.DebuggerStepThroughAttribute()]
    [System.ComponentModel.DesignerCategoryAttribute("code")]
    [XmlType(AnonymousType=true, Namespace="urn:WitsmlTests")]
    [XmlRoot(Namespace="urn:WitsmlTests", IsNullable=false)]
    public partial class WitsmlTestSuite {
        
        private string descriptionField;
        
        private Comparator[] comparatorField;
        
        private FixtureInfo[] testFixturesField;
        
        /// <remarks/>
        public string description {
            get {
                return this.descriptionField;
            }
            set {
                this.descriptionField = value;
            }
        }
        
        /// <remarks/>
        [XmlElement("comparator")]
        public Comparator[] comparator {
            get {
                return this.comparatorField;
            }
            set {
                this.comparatorField = value;
            }
        }
        
        /// <remarks/>
        [XmlArrayItem("TestFixture", typeof(FixtureInfo), IsNullable=false)]
        public FixtureInfo[] TestFixtures {
            get {
                return this.testFixturesField;
            }
            set {
                this.testFixturesField = value;
            }
        }
    }
    
    /// <remarks/>
    [System.CodeDom.Compiler.GeneratedCodeAttribute("xsd", "2.0.50727.42")]
    [System.SerializableAttribute()]
    [System.Diagnostics.DebuggerStepThroughAttribute()]
    [System.ComponentModel.DesignerCategoryAttribute("code")]
    [XmlType(AnonymousType=true, Namespace="urn:WitsmlTests")]
    public partial class Comparator {
        
        private string pathField;
        
        private string argumentsField;
        
        private string saveDifferencesField;
        
        /// <remarks/>
        public string path {
            get {
                return this.pathField;
            }
            set {
                this.pathField = value;
            }
        }
        
        /// <remarks/>
        public string arguments {
            get {
                return this.argumentsField;
            }
            set {
                this.argumentsField = value;
            }
        }
        
        /// <remarks/>
        [XmlAttribute()]
        public string saveDifferences {
            get {
                return this.saveDifferencesField;
            }
            set {
                this.saveDifferencesField = value;
            }
        }
    }
    
    /// <remarks/>
    [System.CodeDom.Compiler.GeneratedCodeAttribute("xsd", "2.0.50727.42")]
    [System.SerializableAttribute()]
    [System.Diagnostics.DebuggerStepThroughAttribute()]
    [System.ComponentModel.DesignerCategoryAttribute("code")]
    [XmlType(AnonymousType=true, Namespace="urn:WitsmlTests")]
    public partial class FixtureInfo {
        
        private string filenameField;
        
        private string enabledField;
        
        /// <remarks/>
        [XmlAttribute()]
        public string filename {
            get {
                return this.filenameField;
            }
            set {
                this.filenameField = value;
            }
        }
        
        /// <remarks/>
        [XmlAttribute()]
        public string enabled {
            get {
                return this.enabledField;
            }
            set {
                this.enabledField = value;
            }
        }
    }
    
    /// <remarks/>
    [System.CodeDom.Compiler.GeneratedCodeAttribute("xsd", "2.0.50727.42")]
    [System.SerializableAttribute()]
    [System.Diagnostics.DebuggerStepThroughAttribute()]
    [System.ComponentModel.DesignerCategoryAttribute("code")]
    [XmlType(AnonymousType=true, Namespace="urn:WitsmlTests")]
    [XmlRoot(Namespace="urn:WitsmlTests", IsNullable=false)]
    public partial class NewDataSet {
        
        private WitsmlTestSuite[] itemsField;
        
        /// <remarks/>
        [XmlElement("WitsmlTestSuite")]
        public WitsmlTestSuite[] Items {
            get {
                return this.itemsField;
            }
            set {
                this.itemsField = value;
            }
        }
    }
}
