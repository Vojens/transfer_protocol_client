﻿//------------------------------------------------------------------------------
// <auto-generated>
//     This code was generated by a tool.
//     Runtime Version:2.0.50727.832
//
//     Changes to this file may cause incorrect behavior and will be lost if
//     the code is regenerated.
// </auto-generated>
//------------------------------------------------------------------------------

// 
// This source code was auto-generated by xsd, Version=2.0.50727.42.
// 
namespace Witsml.Tester {
    using System.Xml.Serialization;
    
    
    /// <remarks/>
    [System.CodeDom.Compiler.GeneratedCodeAttribute("xsd", "2.0.50727.42")]
    [System.SerializableAttribute()]
    [System.Diagnostics.DebuggerStepThroughAttribute()]
    [System.ComponentModel.DesignerCategoryAttribute("code")]
    [XmlType(AnonymousType=true, Namespace="urn:WitsmlTests")]
    [XmlRoot(Namespace="urn:WitsmlTests", IsNullable=true)]
    public partial class Macro {
        
        private string idField;
        
        private string valueField;
        
        /// <remarks/>
        [XmlAttribute()]
        public string id {
            get {
                return this.idField;
            }
            set {
                this.idField = value;
            }
        }
        
        /// <remarks/>
        [XmlText()]
        public string Value {
            get {
                return this.valueField;
            }
            set {
                this.valueField = value;
            }
        }
    }
    
    /// <remarks/>
    [System.CodeDom.Compiler.GeneratedCodeAttribute("xsd", "2.0.50727.42")]
    [System.SerializableAttribute()]
    [System.Diagnostics.DebuggerStepThroughAttribute()]
    [System.ComponentModel.DesignerCategoryAttribute("code")]
    [XmlType(AnonymousType=true, Namespace="urn:WitsmlTests")]
    [XmlRoot(Namespace="urn:WitsmlTests", IsNullable=false)]
    public partial class TestFixture {
        
        private object[] itemsField;
        
        /// <remarks/>
        [XmlElement("Macro", typeof(Macro), IsNullable=true)]
        [XmlElement("test", typeof(Test))]
        public object[] Items {
            get {
                return this.itemsField;
            }
            set {
                this.itemsField = value;
            }
        }
    }
    
    /// <remarks/>
    [System.CodeDom.Compiler.GeneratedCodeAttribute("xsd", "2.0.50727.42")]
    [System.SerializableAttribute()]
    [System.Diagnostics.DebuggerStepThroughAttribute()]
    [System.ComponentModel.DesignerCategoryAttribute("code")]
    [XmlType(AnonymousType=true, Namespace="urn:WitsmlTests")]
    public partial class Test {
        
        private string descriptionField;
        
        private Step[] stepField;
        
        private string repeatField;
        
        private string delayField;
        
        private string delayJitterField;

        private string m_id;

        [XmlAttribute("id")]
        public string Id
        {
            get { return m_id; }
            set { m_id = value; }
        }
        
        /// <remarks/>
        public string description {
            get {
                return this.descriptionField;
            }
            set {
                this.descriptionField = value;
            }
        }
        
        /// <remarks/>
        [XmlElement("step")]
        public Step[] step {
            get {
                return this.stepField;
            }
            set {
                this.stepField = value;
            }
        }
        
        /// <remarks/>
        [XmlAttribute()]
        public string repeat {
            get {
                return this.repeatField;
            }
            set {
                this.repeatField = value;
            }
        }
        
        /// <remarks/>
        [XmlAttribute()]
        public string delay {
            get {
                return this.delayField;
            }
            set {
                this.delayField = value;
            }
        }
        
        /// <remarks/>
        [XmlAttribute()]
        public string delayJitter {
            get {
                return this.delayJitterField;
            }
            set {
                this.delayJitterField = value;
            }
        }
    }
    
    /// <remarks/>
    [System.CodeDom.Compiler.GeneratedCodeAttribute("xsd", "2.0.50727.42")]
    [System.SerializableAttribute()]
    [System.Diagnostics.DebuggerStepThroughAttribute()]
    [System.ComponentModel.DesignerCategoryAttribute("code")]
    //[System.Xml.Serialization.XmlTypeAttribute(AnonymousType=true, Namespace="urn:WitsmlTests")]
    public partial class Step {
        
        private string descriptionField;
        
        private string capabilitiesInFileField;
        
        private string requestFileField;
        
        private string expectedResponseFileField;
        
        private string expectedStatusField;
        
        private string expectedMinResponseTimeField;
        
        private string expectedMaxResponseTimeField;
        
        private Method methodField;
        
        private Macro[] requestFileMacrosField;
        
        private Macro[] expectedResponseMacrosField;
        
        private string repeatField;
        
        private string delayField;
        
        private string delayJitterField;
        
        private string concurrentField;

        private string m_id;

        [XmlAttribute("id")]
        public string Id
        {
            get { return m_id; }
            set { m_id = value; }
        }
        
        /// <remarks/>
        public string description {
            get {
                return this.descriptionField;
            }
            set {
                this.descriptionField = value;
            }
        }
        
        /// <remarks/>
        public string capabilitiesInFile {
            get {
                return this.capabilitiesInFileField;
            }
            set {
                this.capabilitiesInFileField = value;
            }
        }
        
        /// <remarks/>
        public string requestFile {
            get {
                return this.requestFileField;
            }
            set {
                this.requestFileField = value;
            }
        }
        
        /// <remarks/>
        public string expectedResponseFile {
            get {
                return this.expectedResponseFileField;
            }
            set {
                this.expectedResponseFileField = value;
            }
        }
        
        /// <remarks/>
        public string expectedStatus {
            get {
                return this.expectedStatusField;
            }
            set {
                this.expectedStatusField = value;
            }
        }
        
        /// <remarks/>
        public string expectedMinResponseTime {
            get {
                return this.expectedMinResponseTimeField;
            }
            set {
                this.expectedMinResponseTimeField = value;
            }
        }
        
        /// <remarks/>
        public string expectedMaxResponseTime {
            get {
                return this.expectedMaxResponseTimeField;
            }
            set {
                this.expectedMaxResponseTimeField = value;
            }
        }
        
        /// <remarks/>
        [XmlElement("method")]
        public Method method {
            get {
                return this.methodField;
            }
            set {
                this.methodField = value;
            }
        }
        
        /// <remarks/>
        [XmlArrayItem("Macro", typeof(Macro))]
        public Macro[] requestFileMacros {
            get {
                return this.requestFileMacrosField;
            }
            set {
                this.requestFileMacrosField = value;
            }
        }
        
        /// <remarks/>
        [XmlArrayItem("Macro", typeof(Macro))]
        public Macro[] expectedResponseMacros {
            get {
                return this.expectedResponseMacrosField;
            }
            set {
                this.expectedResponseMacrosField = value;
            }
        }
        
        /// <remarks/>
        [XmlAttribute()]
        public string repeat {
            get {
                return this.repeatField;
            }
            set {
                this.repeatField = value;
            }
        }
        
        /// <remarks/>
        [XmlAttribute()]
        public string delay {
            get {
                return this.delayField;
            }
            set {
                this.delayField = value;
            }
        }
        
        /// <remarks/>
        [XmlAttribute()]
        public string delayJitter {
            get {
                return this.delayJitterField;
            }
            set {
                this.delayJitterField = value;
            }
        }
        
        /// <remarks/>
        [XmlAttribute()]
        public string concurrent {
            get {
                return this.concurrentField;
            }
            set {
                this.concurrentField = value;
            }
        }
    }
    
    /// <remarks/>
    [System.CodeDom.Compiler.GeneratedCodeAttribute("xsd", "2.0.50727.42")]
    [System.SerializableAttribute()]
    [System.Diagnostics.DebuggerStepThroughAttribute()]
    [System.ComponentModel.DesignerCategoryAttribute("code")]
    [XmlType(AnonymousType=true, Namespace="urn:WitsmlTests")]
    public partial class Method {
        
        private string nameField;
        
        private string witsmlTypeField;
        
        private string optionsInField;
        
        /// <remarks/>
        [XmlAttribute()]
        public string name {
            get {
                return this.nameField;
            }
            set {
                this.nameField = value;
            }
        }
        
        /// <remarks/>
        [XmlAttribute()]
        public string witsmlType {
            get {
                return this.witsmlTypeField;
            }
            set {
                this.witsmlTypeField = value;
            }
        }
        
        /// <remarks/>
        [XmlAttribute()]
        public string optionsIn {
            get {
                return this.optionsInField;
            }
            set {
                this.optionsInField = value;
            }
        }
    }
}
