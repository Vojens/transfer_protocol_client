using System;
using System.Collections.Generic;
using System.Text;

namespace Witsml.Tester
{
    public class WitsmlTestException:Exception
    {
        public WitsmlTestException(string message)
            : base(message)
        {
        }
    }
}
