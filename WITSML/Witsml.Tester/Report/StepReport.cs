using System;
using System.Collections.Generic;
using System.Text;
using System.Xml.Serialization;

namespace Witsml.Tester
{
    /// <summary>
    /// Detail of for each repeating step.
    /// </summary>
    public class StepReportDetail:ReportDetail
    {
        private double m_responseTime = 0D;

        [XmlIgnore]
        public double ResponseTime
        {
            get { return m_responseTime; }
            set { m_responseTime = value; }
        }

        [XmlElement("ResponseTime")]
        public string ResponseTimeText
        {
            get { return string.Format("{0:00.000}", m_responseTime / 1000); }
            set {  }
        }

        private short m_returnCode;

        public short ReturnCode
        {
            get { return m_returnCode; }
            set { m_returnCode = value; }
        }

        private string m_response = null;

        [XmlIgnore]
        public string Response
        {
            get { return m_response; }
            set { m_response = value; }
        }
	
    }

    /// <summary>
    /// Report of for each step in test.
    /// </summary>
    public class StepReport : AbstractReport<StepReportDetail>
    {
        private double m_totalResponseTime = 0D;

        [XmlIgnore]
        public double TotalResponseTime
        {
            get { return m_totalResponseTime; }
            set {
                    m_totalResponseTime = value;
            }
        }

        [XmlElement("TotalResponseTime")]
        public string TotalResponseTimeText
        {
            get { return string.Format("{0:00.000}", m_totalResponseTime / 1000); }
            set { }
        }

        private double m_minResponseTime = double.MaxValue;

        [XmlIgnore]
        public double MinResponseTime
        {
            get { return m_minResponseTime; }
            set {
                    m_minResponseTime = value;
            }
        }

        [XmlElement("MinResponseTime")]
        public string MinResponseTimeText
        {
            get {
                if (m_minResponseTime == double.MaxValue)
                {
                    m_minResponseTime = 0;
                }

                return string.Format("{0:00.000}", m_minResponseTime / 1000); 
            }
            set { }
        }

        private double m_maxResponseTime = double.MinValue;

        /// <summary>
        /// Maximum response time from a set of responses time of n iterations process of this step.
        /// </summary>
        [XmlIgnore]
        public double MaxResponseTime
        {
            get { return m_maxResponseTime; }
            set {
                    m_maxResponseTime = value;
            }
        }

        [XmlElement("MaxResponseTime")]
        public string MaxResponseTimeText
        {
            get {
                if (m_maxResponseTime == double.MinValue)
                {
                    m_maxResponseTime = 0;
                }

                return string.Format("{0:00.000}", m_maxResponseTime / 1000); 
            }
            set { }
        }

        private double m_avgResponseTime;

        /// <summary>
        /// Average response time of method called.
        /// </summary>
        [XmlIgnore]
        public double AvgResponseTime
        {
            get { return m_avgResponseTime; }
            set { m_avgResponseTime = value; }
        }

        /// <summary>
        /// Average response time in formatted text.
        /// </summary>
        [XmlElement("AvgResponseTime")]
        public string AvgResponseTimeText
        {
            get { return string.Format("{0:00.000}", m_avgResponseTime / 1000); }
            set { }
        }

        private short m_returnCode = 1;

        public short ReturnCode
        {
            get { return m_returnCode; }
            set { m_returnCode = value; }
        }

        public StepReport():base(string.Empty, "StepReport")
        {
        }

        public StepReport(string id): base(id, "StepReport")
        {
        }

        protected override Status getStatus()
        {
            if (m_failureTimes > 0)
            {
                return Status.Fail;
            }
            else
            {
                return Status.Pass;
            }
        }
    }
}
