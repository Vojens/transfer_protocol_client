using System;
using System.Collections.Generic;
using System.Text;
using System.Xml.Serialization;
using Support.Common;

namespace Witsml.Tester
{
    public class FailingStep
    {
        private string m_testId;

        [XmlAttribute("testID")]
        public string TestId
        {
            get { return m_testId; }
            set { m_testId = value; }
        }

        private string m_step;

        [XmlAttribute("step")]
        public string Step
        {
            get { return m_step; }
            set { m_step = value; }
        }

        private string m_fixtureFileName;

        public string FixtureFileName
        {
            get { return m_fixtureFileName; }
            set { m_fixtureFileName = value; }
        }

        private short m_returnCode;

        public short ReturnCode
        {
            get { return m_returnCode; }
            set { m_returnCode = value; }
        }

        private List<string> m_messages;

        [XmlIgnore]
        public List<string> Messages
        {
            get { return m_messages ; }
            set { m_messages = value; }
        }

        [XmlArrayItem("Message")]
        public List<string> ErrorMessages
        {
            get
            {
                if (m_messages.Count > 0)
                {
                    return m_messages;
                }
                else
                {
                    return null;
                }
            }

            set { }
        }
    }
}
