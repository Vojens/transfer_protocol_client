using System;
using System.Collections.Generic;
using System.Text;
using System.Xml.Serialization;

namespace Witsml.Tester
{
    public class ReportDetail
    {
        protected int m_iteration = 0;

        private double m_runningTime = 0D;

        private Status m_status = Status.Pass;

        /// <summary>
        /// There will be numbers of failed messages possibility regarding to the schema:
        /// 1. Unexpected return code.
        /// 2. Unexpected min/max response time.
        /// 3. Unexpected response compared to expected response file.
        /// </summary>
        private List<string> m_messages = new List<string>();
        [XmlIgnore]
        public int Iteration
        {
            get { return m_iteration; }
            set { m_iteration = value; }
        }

        [XmlAttribute("Iteration")]
        public string IterationText
        {
            get
            {
                if (m_iteration > 0)
                {
                    return m_iteration.ToString();
                }
                else
                {
                    return null;
                }
            }

            set { }
        }

        [XmlIgnore]
        public double RunningTime
        {
            get { return m_runningTime; }
            set
            {
                m_runningTime = value;
            }
        }

        [XmlElement("RunningTime")]
        public string RunningTimeText
        {
            //  In seconds.
            get { return string.Format("{0:00.000}", m_runningTime / 1000); }
            set { }
        }

        public Status Status
        {
            get { return m_status; }
            set { m_status = value; }
        }

        [XmlIgnore]
        public List<string> Messages
        {
            get {   return m_messages;
            }
            set { m_messages = value; }
        }

        [XmlArrayItem("Message")]
        public List<string> ErrorMessages
        {
            get
            {
                if (m_messages.Count > 0)
                {
                    return m_messages;
                }
                else
                {
                    return null;
                }
            }

            set { }
        }
    }
}
