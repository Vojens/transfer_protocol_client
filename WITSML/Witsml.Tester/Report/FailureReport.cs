using System;
using System.Collections.Generic;
using System.Text;
using System.Xml.Serialization;

namespace Witsml.Tester
{
    public class FailureReport
    {
        [XmlElement("FailingStep")]
        public FailingStep[] FailingSteps
        {
            get { return m_failingStepList.ToArray(); }
            set {  }
        }

        private List<FailingStep> m_failingStepList = new List<FailingStep>();

        [XmlIgnore]
        public List<FailingStep> FailingStepList
        {
            get { return m_failingStepList; }
            set { m_failingStepList = value; }
        }

        public static string Name = "FailureReport";
    }
}
