using System;
using System.Collections.Generic;
using System.Text;
using System.Xml.Serialization;

namespace Witsml.Tester
{
    [XmlRoot(Namespace = "urn:WitsmlTests")]
    public class SuiteReport : AbstractReport<ReportDetail>
    {
        private FixtureReport[] m_childs;

        [XmlArray("Fixtures")]
        public FixtureReport[] Childs
        {
            get { return m_childs; }
            set { m_childs = value; }
        }

        public SuiteReport(): base(string.Empty, "SuiteReport")
        {
        }

        private long m_numberOfTestFail;

        [XmlIgnore]
        public long NumberOfTestFail
        {
            get { return m_numberOfTestFail; }
            set { m_numberOfTestFail = value; }
        }

        private long m_numberOfStepFail = 0;

        [XmlIgnore]
        public long NumberOfStepFail
        {
            get { return m_numberOfStepFail; }
            set { m_numberOfStepFail = value; }
        }

        private long m_numberOfStep = 0;

        [XmlIgnore]
        public long NumberOfStep
        {
            get { return m_numberOfStep; }
            set { m_numberOfStep = value; }
        }

        private long m_numberOfTest = 0;
        
        [XmlIgnore]
        public long NumberOfTest
        {
            get { return m_numberOfTest; }
            set { m_numberOfTest = value; }
        }

        protected override Status getStatus()
        {
            if (m_childs != null)
            {
                foreach (FixtureReport fixture in m_childs)
                {
                    if (fixture.Status == Status.Fail)
                    {
                        return fixture.Status;
                    }
                }
            }

            return Status.Pass;
        }
    }
}
