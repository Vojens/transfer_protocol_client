﻿using System;
namespace Witsml.Tester
{
    interface IWitsmlTestReport
    {
        double RunningTime { get; set; }
        string Name { get;}
    }
}
