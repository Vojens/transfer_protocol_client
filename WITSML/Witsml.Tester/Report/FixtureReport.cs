using System;
using System.Collections.Generic;
using System.Text;
using System.Xml.Serialization;

namespace Witsml.Tester
{
    [XmlRoot(Namespace="urn:WitsmlTests")]
    public class FixtureReport : AbstractReport<ReportDetail>
    {
        private string m_capResult;

        [XmlIgnore]
        public string Capabilities
        {
            get { return m_capResult; }
            set { m_capResult = value; }
        }

        private TestReport[] m_childs = null;

        [XmlArray("Tests")]
        public TestReport[] Childs
        {
            get { return m_childs; }
            set { m_childs = value; }
        }

        public FixtureReport():base(null, "FixtureReport")
        {
        }

        private string m_versionResult;

        [XmlAttribute("version")]
        public string Version
        {
            get { return m_versionResult; }
            set { m_versionResult = value; }
        }

        private long m_numberOfTestFail;

        [XmlIgnore]
        public long NumberOfTestFail
        {
            get { return m_numberOfTestFail; }
            set { m_numberOfTestFail = value; }
        }

        private long m_numberOfStepFail = 0;

        [XmlIgnore]
        public long NumberOfStepFail
        {
            get { return m_numberOfStepFail; }
            set { m_numberOfStepFail = value; }
        }

        private long m_numberOfStep = 0;

        [XmlIgnore]
        public long NumberOfStep
        {
            get { return m_numberOfStep; }
            set { m_numberOfStep = value; }
        }

        protected override Status getStatus()
        {
            if (m_childs != null)
            {
                foreach (TestReport test in m_childs)
                {
                    if (test.Status == Status.Fail)
                    {
                        return test.Status;
                    }
                }
            }

                return Status.Pass;
            }
    }
}
