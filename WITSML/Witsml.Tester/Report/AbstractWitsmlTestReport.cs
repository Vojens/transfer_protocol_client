using System;
using System.Collections.Generic;
using System.Text;
using System.Threading;
using System.Xml.Serialization;
using System.Xml;
using Support.Common;

namespace Witsml.Tester
{
    public abstract class AbstractReport<TReportDetail>: IWitsmlTestReport
    {
        private double m_runningTime = 0D;

        [XmlIgnore]
        public double RunningTime
        {
            get { return m_runningTime; }
            set {
                    m_runningTime = value;
            }
        }

        [XmlElement("RunningTime")]
        public string RunningTimeText
        {
            //  In seconds.
            get { return string.Format("{0:00.000}", m_runningTime / 1000); }
            set {  }
        }

        [XmlElement("FailureTimes")]
        public string FailureTimeText
        {
            get
            {
                if (m_failureTimes > 0)
                {
                    return m_failureTimes.ToString();
                }
                else
                {
                    return null;
                }
            }
            set { }
        }

        protected string m_name;

        public string Name
        {
            get { return m_name; }
        }

        protected string m_id = null;

        [XmlAttribute("Id")]
        public string Id
        {
            get { return m_id; }
            set { m_id = value; }
        }

        public AbstractReport(string id, string name)
        {
            m_id = id;
            m_name = name;
        }

        public AbstractReport():this(string.Empty, string.Empty)
        {
        }

        protected double m_failureTimes = 0D;

        /// <summary>
        /// Number of failures of iterations for this step processing.
        /// </summary>
        [XmlIgnore]
        public double FailureTimes
        {
            get { return m_failureTimes; }
            set
            {
                m_failureTimes = value;
            }
        }

        private string m_message = null;

        public string Message
        {
            get {
                if (string.IsNullOrEmpty(m_message))
                {
                    return null;
                }
                else
                {
                    return m_message;
                }
            }
            set { m_message = value; }
        }

        //private Status m_status = Status.Pass;

        public Status Status
        {
            get { return getStatus(); }
            set {  }
        }

        /// <summary>
        /// Get status based on child status (if any child has a fail status, then this report is fail).
        /// </summary>
        /// <returns>Status of report.</returns>
        protected abstract Status getStatus();

        private List<TReportDetail> m_details = null;

        public List<TReportDetail> Details
        {
            get { return m_details; }
            set { m_details = value; }
        }
    }
}
