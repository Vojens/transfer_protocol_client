using System;
using System.Collections.Generic;
using System.Text;
using System.Xml.Serialization;
using System.Xml.Schema;
using System.Xml;

namespace Witsml.Tester
{
    public class TestReport : AbstractReport<ReportDetail>
    {
	
        public TestReport(string id, string name, int iteration): base(id, name)
        {
        }

        public TestReport(Test test):base(test.Id, "TestReport")
        {
        }

        public TestReport()
            : base(string.Empty, string.Empty)
        {
        }

        private long m_numberOfStepFail = 0;

        [XmlIgnore]
        public long NumberOfStepFail
        {
            get { return m_numberOfStepFail; }
            set { m_numberOfStepFail = value; }
        }

        private StepReport[] m_childs = null;

        [XmlArray("Steps")]
        public StepReport[] Childs
        {
            get { return m_childs ; }
            set { m_childs = value; }
        }

        protected override Status getStatus()
        {
            if (m_numberOfStepFail > 0 || m_failureTimes > 0)
            {
                return Status.Fail;
            }
            else
            {
                return Status.Pass;
            }
        }
    }
}
