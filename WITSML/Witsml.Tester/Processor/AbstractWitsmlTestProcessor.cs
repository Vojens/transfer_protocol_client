using System;
using System.Collections.Generic;
using System.Text;
using Support.Common;
using Support.Common.Logging;

namespace Witsml.Tester
{
    public abstract class AbstractWitsmlTestProcessor<TReportDetail>:IWitsmlTestProcessor<TReportDetail>, IDisposable
    {
        protected FailureReport m_failingReport = new FailureReport();

        public FailureReport FailingReport
        {
            get { return m_failingReport ; }
            set { m_failingReport = value; }
        }

        protected string m_password = string.Empty;

        public string  Password
        {
            get { return m_password; }
            set { m_password = value; }
        }

        protected string m_url = string.Empty;

        public string Url
        {
            get { return m_url; }
            set { m_url = value; }
        }

        protected string m_user = string.Empty;

        public string User
        {
            get { return m_user; }
            set { m_user = value; }
        }

        public abstract void Process();

        protected bool m_isEnableLog = false;

        public bool IsEnableLog
        {
            get { return m_isEnableLog; }
            set { m_isEnableLog = value; }
        }

        //  Has passed UnitTest.
        /// <summary>
        /// Abstract constructor for Witsmltest processor.
        /// </summary>
        /// <param name="url"></param>
        /// <param name="user"></param>
        /// <param name="password"></param>
        /// <param name="fullFilePath">Full file path to xml file, usually used for Fixture and Suite.</param>
        /// <param name="isEnableLog"></param>
        /// <param name="isChild">Determines if this processor is a child from other processor.</param>
        /// <param name="iteration"></param>
        /// <param name="isIgnoreResponseTime"></param>
        public AbstractWitsmlTestProcessor(string url, string user, string password, string fullFilePath, bool isEnableLog
            , bool isChild, int iteration, bool isIgnoreResponseTime):this(url, user, password, isEnableLog, isChild, iteration
            , isIgnoreResponseTime)
        {
            //  Construct file path from full file path. ie: C:\Folder\File.ext becomes C:\Folder and File.ext.
            //  Or ../File.ext becomes The relative path from "../" and File.ext.
            try
            {
                m_filePath = IOHelper.GetFilePath(fullFilePath);
                m_fileName = IOHelper.GetFileName(fullFilePath);

                //  Generate directory name, everytime this processor created, if only this processor is the root processor.
                if (!m_isChild)
                {
                    m_directoryFullName = IOHelper.GetSequencedDirectory(m_filePath, Report.Name, true);
                }
            }
            catch (IOHelperException)
            {
                throw new WitsmlTestException("File path is not valid.");
            }
        }

        //  Has passed UnitTest.
        /// <summary>
        /// Abstract Witsmltest processor constructor.
        /// </summary>
        /// <param name="url">Url is required parameter.</param>
        /// <param name="user"></param>
        /// <param name="password"></param>
        /// <param name="isEnableLog"></param>
        /// <param name="isChild"></param>
        /// <param name="iteration"></param>
        /// <param name="isIgnoreResponseTime"></param>
        public AbstractWitsmlTestProcessor(string url, string user, string password, bool isEnableLog, bool isChild
            , int iteration, bool isIgnoreResponseTime)
        {
            if (string.IsNullOrEmpty(url))
            {
                throw new WitsmlTestException("Url is null.");
            }

            m_url = url;
            m_user = user;
            m_password = password;
            m_isEnableLog = isEnableLog;
            m_isChild = isChild;
            m_iteration = iteration;
            m_isIgnoreResponseTime = isIgnoreResponseTime;
        }

        public abstract void Dispose();

        public abstract void GenerateReport();

        private void generateReport<T>(T report, string xmlFileName)
        {
            string xmlFullFilePath = xmlFileName;

            if (m_directoryFullName != string.Empty && m_directoryFullName != null)
            {
                xmlFullFilePath = string.Format(@"{0}\{1}", m_directoryFullName, xmlFileName);
            }

            XmlHelper.Serialize<T>(report, xmlFullFilePath);

            try
            {
                XmlHelper.Transform(xmlFullFilePath, xmlFullFilePath.Replace(".xml", ".xslt")
                    , xmlFullFilePath.Replace(".xml", ".html"));
            }
            catch (XmlHelperException ex)
            {
                ExceptionHandlerLogger.Write(ex.Message);
            }
            catch (IOHelperException ex)
            {
                ExceptionHandlerLogger.Write(ex.Message);
            }
        }

        protected void GenerateReport<T>(T report, string name)
        {
            string xmlFileName = string.Format("{0}.xml", name);
            generateReport<T>(report, xmlFileName);
        }

        protected void GenerateReport<T>(T report, string name, bool isUnique)
        {
            if (isUnique)
            {
                GenerateReport<T>(report, name);
            }
            else
            {
                string xmlFileName = string.Format("{0}.xml", name);
                generateReport<T>(report, xmlFileName);
            }
        }

        protected void Log(string message)
        {
            if (m_isEnableLog)
            {
                //  Change to some specific format if necessary.
                CommonLogger.Write(message);
            }
        }

        protected void Log(string format, params object[] args)
        {
            Log(string.Format(format, args)); 
        }

        public abstract AbstractReport<TReportDetail> Report{get;}

        protected bool m_isChild = true;

        public bool IsChild
        {
            get { return m_isChild; }
            set { m_isChild = value; }
        }

        protected string m_filePath = string.Empty;

        public string FilePath
        {
            get { return m_filePath; }
            set { m_filePath = value; }
        }

        protected int m_iteration;

        public int Iteration
        {
            get { return m_iteration; }
            set { m_iteration = value; }
        }

        protected bool m_isIgnoreResponseTime = false;

        public bool IsIgnoreResponseTime
        {
            get
            {
                return m_isIgnoreResponseTime;
            }
            set
            {
                m_isIgnoreResponseTime = value;
            }
        }

        protected string m_fileName = string.Empty;

        public string FileName
        {
            get
            {
                return m_fileName;
            }
            set
            {
                m_fileName = value;
            }
        }

        public string FullFilePath
        {
            get
            {
                if (m_filePath != string.Empty && m_filePath != null)
                {
                    return string.Format(@"{0}\{1}", m_filePath, m_fileName);
                }
                else
                {
                    return m_fileName;
                }
            }
        }

        protected void addMessage(List<string> target, string message)
        {
            if (target == null)
            {
                target = new List<string>();
            }

            if (!string.IsNullOrEmpty(message))
            {
                target.Add(message);
            }
        }

        protected void addMessage(List<string> target, List<string> source)
        {
            if (target == null)
            {
                target = new List<string>();
            }

            if (source != null)
            {
                target.AddRange(source);
            }
        }

        protected string m_directoryFullName = string.Empty;

        public string DirectoryFullName
        {
            get
            {
                return m_directoryFullName;
            }
            set
            {
                m_directoryFullName = value;
            }
        }
    }
}
