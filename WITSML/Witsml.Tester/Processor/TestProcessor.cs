using System;
using System.Collections.Generic;
using System.Text;
using Support.Common;

namespace Witsml.Tester
{
    public class TestProcessor: AbstractWitsmlTestProcessor<ReportDetail>
    {
        private TestReport m_testReport;

        public TestReport TestReport
        {
            get { return m_testReport; }
            set { m_testReport = value; }
        }

        private Test m_test;

        //public TestProcessor(Test test, string url, string user, string password, string filePath, string fileName
        //    , bool isEnableLog, int iteration, bool isIgnoreResponseTime)
        //    : base(url, user, password, filePath, fileName, isEnableLog, true, iteration, isIgnoreResponseTime)
        public TestProcessor(Test test, string url, string user, string password, string filePath, bool isEnableLog
            , int iteration, bool isIgnoreResponseTime)
            : base(url, user, password, isEnableLog, true, iteration, isIgnoreResponseTime)
        {
            m_test = test;
            m_filePath = filePath;
        }

        public Test Test
        {
            get { return m_test; }
            set { m_test = value; }
        }

        public override void Process()
        {
            if (m_test == null)
            {
                Log("Test is null, no test will be processed");
            }
            else
            {
                Log("Test {0}, {1}", m_test.Id, m_test.description);

                m_testReport = new TestReport(m_test.Id, string.Empty, m_iteration);
                List<StepReport> stepReports = new List<StepReport>();

                HiPerfTimer timer = new HiPerfTimer();
                timer.Start();

                if (string.IsNullOrEmpty(m_test.Id))
                {
                    m_testReport.Message = "Test Id is required.";
                    m_testReport.FailureTimes++;
                }
                else
                {
                    foreach (Step step in m_test.step)
                    {
                        //using (StepProcessor stepProcessor = new StepProcessor(step, m_url, m_user, m_password, m_filePath
                        //    , string.Empty, m_isEnableLog, m_isIgnoreResponseTime))
                        using (StepProcessor stepProcessor = new StepProcessor(step, m_url, m_user, m_password, m_filePath
                            , m_isEnableLog, m_isIgnoreResponseTime))
                        {
                            stepProcessor.DirectoryFullName = m_directoryFullName;
                            //stepProcessor.LogHandler = m_logHandler;
                            stepProcessor.Process();
                            stepReports.Add(stepProcessor.StepReport);

                            if (stepProcessor.StepReport.Status == Status.Fail)
                            {
                                m_testReport.NumberOfStepFail++;

                                FailingStep failingStep = new FailingStep();
                                failingStep.FixtureFileName = FullFilePath;

                                if (stepProcessor.StepReport.Details != null)
                                {
                                    foreach (StepReportDetail stepReportDetail in stepProcessor.StepReport.Details)
                                    {
                                        //if (stepReportDetail.Messages.Count > 0)
                                        if (stepReportDetail.Status == Status.Fail)
                                        {
                                            //  Write the response file if not null.
                                            if (stepReportDetail.Response != null)
                                            {
                                                string iteration = (stepReportDetail.Iteration <= 0) ? string.Empty
                                                    : string.Format("_Iteration{0}", stepReportDetail.Iteration);

                                                try
                                                {
                                                    IOHelper.WriteToFile(stepReportDetail.Response, m_directoryFullName
                                                        , string.Format("Response_Test{0}_Step{1}{2}.xml", m_test.Id
                                                        , stepProcessor.StepReport.Id, iteration));
                                                }
                                                catch (IOHelperException)
                                                {
                                                }
                                            }

                                            if (stepReportDetail.Messages.Count > 0)
                                            {
                                                if (failingStep.Messages == null)
                                                {
                                                    failingStep.Messages = new List<string>();
                                                }

                                                addMessage(failingStep.Messages, stepReportDetail.Messages);
                                            }
                                        }
                                    }
                                }
                                else
                                {
                                    if (failingStep.Messages == null)
                                    {
                                        failingStep.Messages = new List<string>();
                                    }

                                    addMessage(failingStep.Messages, stepProcessor.StepReport.Message);
                                }

                                failingStep.ReturnCode = stepProcessor.StepReport.ReturnCode;
                                failingStep.Step = stepProcessor.StepReport.Id;
                                failingStep.TestId = m_test.Id;

                                m_failingReport.FailingStepList.Add(failingStep);
                            }
                        }
                    }
                }

                timer.Stop();
                m_testReport.RunningTime = timer.Duration * 1000;
                m_testReport.Childs = stepReports.ToArray();
            }
        }

        public override void Dispose()
        {
        }

        public override AbstractReport<ReportDetail> Report
        {
            get { return m_testReport; }
        }

        public override void GenerateReport()
        {
            GenerateReport<TestReport>(m_testReport, m_testReport.Name);
        }

    }
}
