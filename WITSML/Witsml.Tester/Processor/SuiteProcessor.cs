using System;
using System.Collections.Generic;
using System.Text;
using Support.Common;

namespace Witsml.Tester
{
    public class SuiteProcessor:AbstractWitsmlTestProcessor<ReportDetail>
    {
        private WitsmlTestSuite m_suite;

        public WitsmlTestSuite Suite
        {
            get { return m_suite; }
            set { m_suite = value; }
        }

        private SuiteReport m_suiteReport = new SuiteReport();

        public SuiteReport SuiteReport
        {
            get { return m_suiteReport; }
            set { m_suiteReport = value; }
        }

        public override void Process()
        {
            if (m_suite == null)
            {
                Log("Suite is null, no suite will be processed");
            }
            else
            {
                Log("Processing Suite {0}, {1}.", FullFilePath, m_suite.description);

                List<FixtureReport> fixtureReports = new List<FixtureReport>();

                HiPerfTimer timer = new HiPerfTimer();
                timer.Start();

                foreach (FixtureInfo fixtureInfo in m_suite.TestFixtures)
                {
                    //  TODO. Convertion must be validated.
                    bool isEnabled = Convert.ToBoolean(fixtureInfo.enabled);

                    //  Only load enabled fixture.
                    if (isEnabled)
                    {
                        string fullFilePath = string.Format(@"{0}\{1}", m_filePath, fixtureInfo.filename);
                        using (FixtureProcessor fixtureProcessor = new FixtureProcessor(m_url, m_user, m_password, fullFilePath
                            , m_isEnableLog, true, m_isIgnoreResponseTime))
                        {
                            fixtureProcessor.DirectoryFullName = m_directoryFullName;
                            //fixtureProcessor.LogHandler = m_logHandler;
                            fixtureProcessor.Process();
                            fixtureReports.Add(fixtureProcessor.FixtureReport);

                            m_suiteReport.NumberOfStep += fixtureProcessor.FixtureReport.NumberOfStep;
                            m_suiteReport.NumberOfStepFail += fixtureProcessor.FixtureReport.NumberOfStepFail;
                            m_suiteReport.NumberOfTestFail += fixtureProcessor.FixtureReport.NumberOfTestFail;
                            m_suiteReport.NumberOfTest += fixtureProcessor.FixtureReport.Childs.Length;

                            foreach (FailingStep failingStep in fixtureProcessor.FailingReport.FailingStepList)
                            {
                                m_failingReport.FailingStepList.Add(failingStep);
                            }
                        }
                    }
                }

                timer.Stop();
                m_suiteReport.RunningTime = timer.Duration * 1000;
                m_suiteReport.Childs = fixtureReports.ToArray();

                Log("");
                Log("Report:");
                Log("-------");
                Log(string.Format("Tested server({0}), with testSuite({1})", m_url, m_suite.description));
                Log(string.Format("{0} test(s) executed, {1} passed, {2} failed", m_suiteReport.NumberOfTest
                    , m_suiteReport.NumberOfTest - m_suiteReport.NumberOfTestFail, m_suiteReport.NumberOfTestFail));
                Log(string.Format("{0} step(s) executed {1}", m_suiteReport.NumberOfStep
                    , ((m_suiteReport.NumberOfStep > 0) ? (string.Format(", {0} passed, {1} failed"
                    , m_suiteReport.NumberOfStep - m_suiteReport.NumberOfStepFail, m_suiteReport.NumberOfStepFail))
                    : string.Empty)));
                Log(string.Format("{0:00.000} seconds total run time", timer.Duration));
            }
        }

        //  Has passed UnitTest.
        /// <summary>
        /// Constructs suite processor.
        /// </summary>
        /// <param name="url"></param>
        /// <param name="user"></param>
        /// <param name="password"></param>
        /// <param name="fullFilePath"></param>
        /// <param name="isEnableLog"></param>
        /// <param name="isIgnoreResponseTime"></param>
        public SuiteProcessor(string url, string user, string password, string fullFilePath, bool isEnableLog
            , bool isIgnoreResponseTime)
            : base(url, user, password, fullFilePath, isEnableLog, false, 0, isIgnoreResponseTime)
        {
            try
            {
                m_suite = XmlHelper.Deserialize<WitsmlTestSuite>(fullFilePath);
            }
            catch (XmlHelperException)
            {
                throw new WitsmlTestException("Unable to deserialize suite.");
            }
        }

        public override void Dispose()
        {
            throw new Exception("The method or operation is not implemented.");
        }

        public override AbstractReport<ReportDetail> Report
        {
            get { return m_suiteReport; }
        }

        public override void GenerateReport()
        {
            GenerateReport<SuiteReport>(m_suiteReport, m_suiteReport.Name);
            GenerateReport<FailureReport>(m_failingReport, FailureReport.Name);
        }
    }
}
