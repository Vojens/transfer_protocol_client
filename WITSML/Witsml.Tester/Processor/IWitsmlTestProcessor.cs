﻿using System;
using System.Collections.Generic;

namespace Witsml.Tester
{
    interface IWitsmlTestProcessor<TReportDetail>
    {
        string Password { get; set; }
        void Process();
        string Url { get; set; }
        string User { get; set; }
        bool IsIgnoreResponseTime { get; set;}
        bool IsChild { get;}
        AbstractReport<TReportDetail> Report { get;}
        string FilePath { get; set;}
        string FileName { get; set;}
        string FullFilePath { get; }
        string DirectoryFullName { get; set;}

        void GenerateReport();
    }
}
