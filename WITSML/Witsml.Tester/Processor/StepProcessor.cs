using System;
using System.Collections.Generic;
using System.Text;
using CommonLib = Support.Common;
using System.Threading;
using System.Xml.Linq;
using Support.Common.Logging;
using System.Configuration;
using Witsml.Client;

namespace Witsml.Tester
{
    /// <summary>
    /// Processor for each step in test.
    /// </summary>
    public class StepProcessor: AbstractWitsmlTestProcessor<StepReportDetail>
    {
        private static Mutex m_processMutex = new Mutex();

        private Step m_step;

        public Step Step
        {
            get { return m_step; }
            set { m_step = value; }
        }

        private StepReport m_stepReport;

        public StepReport StepReport
        {
            get { return m_stepReport; }
            set { m_stepReport = value; }
        }

        //public StepProcessor(Step step, string url, string user, string password, string filePath, string fileName
        //    , bool isEnableLog, bool isIgnoreResponseTime)
        //    : base(url, user, password, filePath, fileName, isEnableLog, true, 0, isIgnoreResponseTime)
        public StepProcessor(Step step, string url, string user, string password, string filePath, bool isEnableLog
            , bool isIgnoreResponseTime)
            : base(url, user, password, isEnableLog, true, 0, isIgnoreResponseTime)
        {
            m_step = step;
            m_filePath = filePath;
        }

        //  Compare return code with expected status.
        private bool isValidReturnCode(short returnCode, StepReportDetail repeat)
        {
            if (returnCode != Convert.ToInt16(m_step.expectedStatus))
            {
                addMessage(repeat.Messages, "Unexpected return code.");
                return false;
            }
            else
            {
                return true;
            }
        }

        /// <summary>
        /// Compares duration with expected minimum response time.
        /// If duration is LOWER THAN expected, than this function SHOULD RETURN FALSE.
        /// </summary>
        /// <param name="duration"></param>
        /// <param name="repeat"></param>
        /// <returns></returns>
        private bool isValidMinResponseTime(double duration, StepReportDetail repeat)
        {
            if (!m_isIgnoreResponseTime)
            {
                double expectedMinResponseTime = Convert.ToDouble(m_step.expectedMinResponseTime) / 1000;

                //  Duration should not be LOWER THAN expected minimum response time (means: response is FASTER THAN expected).
                if (duration < expectedMinResponseTime)
                {
                    addMessage(repeat.Messages, "Unexpected min response time.");

                    //  Log duration and expected time.
                    ExceptionHandlerLogger.Write(string.Format("Duration {0}, Expected {1}", duration, expectedMinResponseTime));

                    return false;
                }
                else
                {
                    return true;
                }
            }
            else
            {
                return true;
            }
        }

        //  Compare duration with expected max response time.
        private bool isValidMaxResponseTime(double duration, StepReportDetail repeat)
        {
            if (!m_isIgnoreResponseTime)
            {
                if (duration > (Convert.ToDouble(m_step.expectedMaxResponseTime) / 1000))
                {
                    addMessage(repeat.Messages, "Unexpected max response time.");
                    return false;
                }
                else
                {
                    return true;
                }
            }
            else
            {
                return true;
            }
        }

        private List<string> m_ignoredTags = null;

        private List<string> getIgonerdTags()
        {
            if (m_ignoredTags == null)
            {
                m_ignoredTags = new List<string>(ConfigurationManager.AppSettings["IgnoredTags"].Split(new char[] { ',' }));
            }

            return m_ignoredTags;
        }

        private bool isValidResponseFile(bool isExpectXmlResponseReturn, string xmlResponse, StepReportDetail repeat)
        {
            bool isResponseComparationValid = true;

            //  Only open expected response file when xml response is returned from called method and expected.
            if (isExpectXmlResponseReturn)
            {
                //  Retrieve expected response file.
                string expectedResponseFile = string.Format(@"{0}\{1}", m_filePath, m_step.expectedResponseFile);

                try
                {
                    string expectedResponseFileContent = CommonLib.IOHelper.GetFileContent(expectedResponseFile);
                    string diffString = "Differences between XML and expected response: {0}";

                    //  XML Helper will throw an error if one of string to be compared is null or an empty string.
                    //  Check first before send it to the helper whether one of the parameter is null or an empty string.
                    if (!string.IsNullOrEmpty(expectedResponseFileContent) && !string.IsNullOrEmpty(xmlResponse))
                    {
                        try
                        {
                            string differenceMessage = CommonLib.XmlHelper.GetDiff(xmlResponse, expectedResponseFileContent, getIgonerdTags());
                            if (differenceMessage != string.Empty)
                            {
                                addMessage(repeat.Messages, string.Format(diffString, differenceMessage));
                                isResponseComparationValid = false;
                            }
                        }
                        catch (CommonLib.XmlHelperException xmlEx)
                        {
                            addMessage(repeat.Messages, xmlEx.Message);
                            isResponseComparationValid = false;
                        }
                    }
                    else
                    {
                        if (xmlResponse != expectedResponseFileContent)
                        {
                            if (string.IsNullOrEmpty(xmlResponse))
                            {
                                addMessage(repeat.Messages, string.Format(diffString, "response is null or empty, expected is not null nor empty."));
                            }

                            if (string.IsNullOrEmpty(expectedResponseFileContent))
                            {
                                addMessage(repeat.Messages, string.Format(diffString, "response is not null nor empty, expected is null or empty."));
                            }

                            isResponseComparationValid = false;
                        }
                    }
                }
                catch (CommonLib.IOHelperException ioEx)
                {
                    addMessage(repeat.Messages, ioEx.Message);
                    isResponseComparationValid = false;
                }
            }

            return isResponseComparationValid;
        }

        private void process(string xmlQueryIn, bool isConcurrent)
        {
            HiPerfTimer timer = new HiPerfTimer();
            timer.Start();

            Method method = m_step.method;

            string xmlResponse = string.Empty;
            bool isExpectXmlResponseReturn = false;

            WMLSCallResult wmlsCallResult = null;

            WMLSManager wmlsManager = new WMLSManager(m_url, m_user, m_password);
            wmlsManager.Process(method.name, method.witsmlType, xmlQueryIn, method.optionsIn, out xmlResponse
                , out wmlsCallResult, out isExpectXmlResponseReturn);

            m_processMutex.WaitOne();

            StepReportDetail repeat = new StepReportDetail();

            //  Convert duration(second) into millisecond.
            repeat.ReturnCode = wmlsCallResult.returnCode;
            repeat.ResponseTime = wmlsCallResult.duration * 1000;

            if (wmlsCallResult.duration < m_stepReport.MinResponseTime)
            {
                m_stepReport.MinResponseTime = (wmlsCallResult.duration * 1000);
            }

            if (wmlsCallResult.duration > m_stepReport.MaxResponseTime)
            {
                m_stepReport.MaxResponseTime = (wmlsCallResult.duration * 1000);
            }

            m_stepReport.TotalResponseTime += (wmlsCallResult.duration * 1000);

            if (!isValidResponseFile(isExpectXmlResponseReturn, xmlResponse, repeat)
                ||!isValidReturnCode(wmlsCallResult.returnCode, repeat) 
                || !isValidMinResponseTime(wmlsCallResult.duration, repeat)
                || !isValidMaxResponseTime(wmlsCallResult.duration, repeat))
            {
                repeat.Status = Status.Fail;
                m_stepReport.FailureTimes++;

                //  Write to file the xml response.
                if (isExpectXmlResponseReturn)
                {
                    //  Parse the xml response first.
                    if (!string.IsNullOrEmpty(xmlResponse))
                    {
                        xmlResponse = XElement.Parse(xmlResponse).ToString();
                    }
                    repeat.Response = xmlResponse;
                }
            }

            if (wmlsCallResult.returnCode != 1){
                if (wmlsCallResult.errorMessage != string.Empty && wmlsCallResult.errorMessage != null)
                {
                    addMessage(repeat.Messages, wmlsCallResult.errorMessage);
                }

                addMessage(repeat.Messages, wmlsCallResult.suppMsgOut);
            }

            m_stepReport.Details.Add(repeat);

            m_processMutex.ReleaseMutex();

            timer.Stop();
            repeat.RunningTime = timer.Duration * 1000;
        }

        private void delayStep()
        {
            int delay = Convert.ToInt32(m_step.delay);
            Random randomGenerator = new Random(DateTime.Now.Millisecond);
            delay += randomGenerator.Next(0, Convert.ToInt32(m_step.delayJitter));
            Thread.Sleep(delay);
        }

        private void failureReport(string message)
        {
            m_stepReport.ReturnCode = -1;
            m_stepReport.Message = message;
            m_stepReport.FailureTimes++;
        }

        private void failureReport()
        {
            failureReport(string.Empty);
        }

        /// <summary>
        /// Process step and generate report.
        /// </summary>
        public override void Process()
        {
            m_stepReport = new StepReport(m_step.Id);
            long repeat = 0;

            HiPerfTimer timer = new HiPerfTimer();
            timer.Start();

            if (string.IsNullOrEmpty(m_step.Id))
            {
                failureReport("Step Id is required.");
            }
            else
            {
                if (m_step.method == null)
                {
                    failureReport("Method is null.");
                }
                else
                {
                    //  Validate method name in step.
                    if (!MethodConstant.IsValidConstant(m_step.method.name))
                    {
                        failureReport("Unknown method.");
                    }
                    else
                    {
                        //  Only open request file if method name is valid.
                        string requestFile = string.Format(@"{0}\{1}", m_filePath, m_step.requestFile);

                        try
                        {
                            string xmlQueryIn = CommonLib.IOHelper.GetFileContent(requestFile);

                        //  TODO. Use a helper to validate convertion from one to other datatype.
                        repeat = Convert.ToInt64(m_step.repeat);

                        //  Inisiate details.
                        m_stepReport.Details = new List<StepReportDetail>();


                        bool isConcurrentProcess = Convert.ToBoolean(m_step.concurrent);

                        if (isConcurrentProcess)
                        {
                            for (int i = 0; i < repeat; i++)
                            {
                                ThreadStart methodToExecute = delegate { process(xmlQueryIn, isConcurrentProcess); };
                                Thread methodThread = new Thread(methodToExecute);
                                methodThread.Start();
                                methodThread.Join();
                                delayStep();
                            }
                        }
                        else
                        {
                            //  Process this step sequentially in repeat times.
                            for (int i = 0; i < repeat; i++)
                            {
                                //  Process step in here.
                                process(xmlQueryIn, isConcurrentProcess);
                                delayStep();
                            }
                        }

                        m_stepReport.AvgResponseTime = m_stepReport.TotalResponseTime / repeat;

                        //  If there is one or more fails and the expected status is '1'; then this Step is failed.
                        if (m_stepReport.FailureTimes > 0)
                        {
                            failureReport();
                        }
                    }
                    catch (CommonLib.IOHelperException)
                        {
                            failureReport("Request file is not valid / not found.");
                        }
                    }
                }
            }

            timer.Stop();
            m_stepReport.RunningTime += timer.Duration * 1000;

            Log("\tStep {0} {1} in {2}s{3}", m_step.Id
                , (m_stepReport.Status == Status.Pass) ? "passed" : "failed"
                , string.Format("{0:00.000}", timer.Duration)
                , (repeat > 1) ? string.Format(" {0} Itterations.", repeat) : ".");
        }

        public override void Dispose()
        {
        }

        public override AbstractReport<StepReportDetail> Report
        {
            get { return m_stepReport; }
        }

        public override void GenerateReport()
        {
            GenerateReport<StepReport>(m_stepReport, m_stepReport.Name);
        }
    }
}
