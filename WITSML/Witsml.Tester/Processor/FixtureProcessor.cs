using System;
using System.Collections.Generic;
using System.Text;
using System.Threading;
using CommonLib = Support.Common;
using Support.Common.Logging;
using Witsml.Client;

namespace Witsml.Tester
{
    public class FixtureProcessor:AbstractWitsmlTestProcessor<ReportDetail>
    {
        private FixtureReport m_fixtureReport = new FixtureReport();

        public FixtureReport FixtureReport
        {
            get { return m_fixtureReport; }
            set { m_fixtureReport = value; }
        }

        private TestFixture m_fixture;

        public TestFixture Fixture
        {
            get { return m_fixture; }
            set { m_fixture = value; }
        }

        private string getVersion()
        {
            WMLSClient wmlsClient = new WMLSClient(m_url, m_user, m_password);
            WMLSCallResult wmlsCallResult = null;
            return wmlsClient.GetVersion(out wmlsCallResult);
        }

        private string getCapabilities()
        {
            WMLSClient wmlsClient = new WMLSClient(m_url, m_user, m_password);
            WMLSCallResult wmlsCallResult = null;
            return wmlsClient.GetCap(string.Empty, out wmlsCallResult);
        }

        private void process(Test test, int iteration, List<TestReport> testReports)
        {
            using (TestProcessor testProcessor = new TestProcessor(test, m_url, m_user, m_password, m_filePath, m_isEnableLog
                , iteration, m_isIgnoreResponseTime))
            {
                testProcessor.DirectoryFullName = m_directoryFullName;
                //testProcessor.LogHandler = m_logHandler;
                testProcessor.Process();
                testReports.Add(testProcessor.TestReport);

                if (testProcessor.TestReport.Status == Status.Fail)
                {
                    m_fixtureReport.NumberOfTestFail++;

                    FailingStep failingStep = new FailingStep();
                    failingStep.FixtureFileName = FullFilePath;

                    if (failingStep.Messages == null)
                    {
                        failingStep.Messages = new List<string>();
                    }

                    addMessage(failingStep.Messages, testProcessor.TestReport.Message);
                    failingStep.ReturnCode = -1;
                    failingStep.TestId = test.Id;

                    m_failingReport.FailingStepList.Add(failingStep);
                }

                m_fixtureReport.NumberOfStepFail += testProcessor.TestReport.NumberOfStepFail;
                m_fixtureReport.NumberOfStep += testProcessor.TestReport.Childs.Length;

                foreach (FailingStep failingStep in testProcessor.FailingReport.FailingStepList)
                {
                    m_failingReport.FailingStepList.Add(failingStep);
                }
            }
        }

        public override void Process()
        {
            if (m_fixture == null)
            {
                OpenLogger.Write("Fixture is null, no fixture will be processed");
                //Log("Fixture is null, no fixture will be processed");
            }
            else
            {
                OpenLogger.Write("Processing Fixture {0}.", FullFilePath);
                //Log("Processing Fixture {0}.", FullFilePath);

                List<TestReport> testReports = new List<TestReport>();

                HiPerfTimer timer = new HiPerfTimer();
                timer.Start();

                m_fixtureReport.Version = getVersion();
                m_fixtureReport.Capabilities = getCapabilities();

                foreach (Test test in m_fixture.Items)
                {
                    int repeat = Convert.ToInt32(test.repeat);
                    if (repeat > 1)
                    {
                        for (int i = 0; i < repeat ; i++)
                        {
                            process(test, i+1, testReports);
                        }
                    }
                    else
                    {
                        process(test, 0, testReports);
                    }

                    Thread.Sleep(500);
                }

                timer.Stop();
                m_fixtureReport.RunningTime = timer.Duration * 1000;
                m_fixtureReport.Childs = testReports.ToArray();
                if (!m_isChild)
                {
                    Log("");
                    Log("Report:");
                    Log("-------");
                    Log(string.Format("Tested server({0})", m_url));
                    Log(string.Format("{0} test(s) executed, {1} passed, {2} failed", m_fixtureReport.Childs.Length
                        , m_fixtureReport.Childs.Length - m_fixtureReport.NumberOfTestFail
                        , m_fixtureReport.NumberOfTestFail));
                    Log(string.Format("{0} step(s) executed {1}", m_fixtureReport.NumberOfStep
                        , ((m_fixtureReport.NumberOfStep > 0) ? (string.Format(", {0} passed, {1} failed"
                        , m_fixtureReport.NumberOfStep - m_fixtureReport.NumberOfStepFail, m_fixtureReport.NumberOfStepFail)) 
                        : string.Empty)));
                    Log(string.Format("{0:00.000} seconds total run time", timer.Duration));
                }
            }
        }

        public override void Dispose()
        {
        }

        public override AbstractReport<ReportDetail> Report
        {
            get { return m_fixtureReport; }
        }

        public override void GenerateReport()
        {
            GenerateReport<FixtureReport>(m_fixtureReport, m_fixtureReport.Name);
            GenerateReport<FailureReport>(m_failingReport, FailureReport.Name);
        }

        //  Has passed UnitTest.
        /// <summary>
        /// Constructs fixture processor.
        /// </summary>
        /// <param name="url">Target web service url.</param>
        /// <param name="user">User id for web service.</param>
        /// <param name="password">Password for web service.</param>
        /// <param name="fullFilePath">Full file path to the source of fixture.</param>
        /// <param name="isEnableLog">Enable log, whether to file or console, or other target.</param>
        /// <param name="isChild">Is it a child processer, ie. if a suite has a fixture
        /// , then fixture processor is a child of Suite processor.</param>
        /// <param name="isIgnoreResponseTime">Ignore response time from the web service.</param>
        public FixtureProcessor(string url, string user, string password, string fullFilePath, bool isEnableLog, bool isChild
            , bool isIgnoreResponseTime)
            : base(url, user, password, fullFilePath, isEnableLog, isChild, 0, isIgnoreResponseTime)
        {
            try
            {
                m_fixture = CommonLib.XmlHelper.Deserialize<TestFixture>(fullFilePath);
            }
            catch (CommonLib.XmlHelperException)
            {
                throw new WitsmlTestException("Unable to deserialize fixture.");
            }
        }
    }
}
