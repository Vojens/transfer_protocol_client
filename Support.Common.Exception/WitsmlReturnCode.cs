﻿ 

using System.Collections.Generic;

namespace Support.Common.Exceptions
{
    public enum WitsmlReturnCode
    {
        // Success !
        Success = 1,

        unknown = 0,

        //900 Group - program logic/exception errors
        InternalLoadError = -901, 
        InternalError = -999,

        //1000 Config related error.
        NullOrEmptyConfigValue  = -1001,
        InvalidConnectionStringFormat = -1002
    }

    public enum PVSecurityReturnCode
    {
        NULL_DataSource = -7000,
        EMPTY_DataSourceProvider = -7001,
        EMPTY_DataSourceConnStr = -7002,
        NULL_SamlToken = -7004,
        DENIEND_GetFolder = -7006,
        DENIEND_DeleteFolder = -7007,
        DENIEND_UpdateFolder = -7008,
        DENIEND_CreateFolder = -7009,
        EMPTY_FolderParent = -7010,
        DENIEND_CreatePrincipal = -7011,
        DENIEND_UpdatePrincipal = -7012,
        DENIEND_GetPrincipal = -7013,
        DENIEND_DeletePrincipal = -7014,
        DATA_CHANGED_BY_SOMEONEELSE = -7017,
        DATA_DELETED_BY_SOMEONEELSE = -7018,
        FOLDER_ALREADY_EXIST = -7020,
        CONVERSION_FAILED = -7021,
        STORE_OPERATION_FAILED = -7022,
        CONNECTION_ERROR = -7024,
        DATABASE_COMMAND_TIMEOUT = -7025,
        DENIED_CreateCompany = -7026,
        PARENT_SET_HAS_BEEN_DELETED = -7027,
        NO_ACCESS_OR_ALLOWED_TYPE = -7028,
        PASSWORD_NOT_MEET_REQUIREMENT = -7029
    }

    public class WitsmlReturnCodeMessage : IReturnCodeMessage
    {
        class Nested
        {
            // Explicit static constructor to tell C# compiler
            // not to mark type as beforefieldinit
            static Nested()
            {
            }

            internal static readonly WitsmlReturnCodeMessage instance = new WitsmlReturnCodeMessage();
        }

        public static IReturnCodeMessage Instance
        {
            get
            {
                return Nested.instance;
            }

        }

        protected static Dictionary<short, string> m_messageList;

        WitsmlReturnCodeMessage()
        {
            m_messageList = new Dictionary<short, string>();

            m_messageList.Add(1, "Operation completed Successfully");
            m_messageList.Add(0, "Program Error: unknown cause");

            // -9nn Program errors
            m_messageList.Add(-901, "Program Error: problem loading internal program or component");
            m_messageList.Add(-999, "Program Error: unknown cause");

            // -10nn Config errors.
            m_messageList.Add(-1001, "Program Error: Null or empty config value.");
            m_messageList.Add(-1002, "Connection Error: Invalid Connection String Format");
        }

        public Dictionary<short, string> MessageList
        {
            get { return m_messageList; }
        }
    }
}
