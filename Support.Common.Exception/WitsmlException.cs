﻿ 

using System;

namespace Support.Common.Exceptions
{
    /// <summary>
    /// Base Exception for all calls related to square WITSML
    /// </summary>
    public class WitsmlException: ApplicationException
    {
        protected IReturnCodeMessage m_retCodeMessage;
        protected string returnMessage;

        private bool _mayShowErrorDetailToClient = false;
        public bool MayShowErrorDetailToClient
        {
            get { return _mayShowErrorDetailToClient; }
            set { _mayShowErrorDetailToClient = value; }
        }

        /// <summary>
        /// This ReturnCode is returned as WebMethod return value.
        /// This code will be converted to short dataType.
        /// </summary>
        public short ReturnCode { get; set; }
        /// <summary>
        /// The Message to be returned back to the caller via SuppMsgOut output parameter.
        /// This is message is intended for General WebMethod call via PowerStore.
        /// If this Message is empty should look up in the Dictionary based on ReturnCode
        /// </summary>
        public string ReturnMessage 
        {
            get
            {
                if (String.IsNullOrEmpty(returnMessage))
                {
                    this.returnMessage = m_retCodeMessage.MessageList[ReturnCode]; //ReturnCodes.ResolveCodetoString(this.ReturnCode);
                }
                return this.returnMessage;
            }
            set
            {
                this.returnMessage = value;
            }
        }

        public WitsmlException(string message, short code)
            : base(message)
        {
            m_retCodeMessage = WitsmlReturnCodeMessage.Instance;
            ReturnCode = code;
        }

        public WitsmlException(string message, WitsmlReturnCode code)
            : this(message, (short)code)
        {
        }

        public WitsmlException(string message, short code, bool isReturnMsg)
            : this(message, code)
        {
            if (isReturnMsg)
            {
                this.returnMessage = message;
            }
        }

        public WitsmlException(string message, WitsmlReturnCode code, bool isReturnMsg)
            : this(message, (short)code, isReturnMsg)
        {
        }

        public WitsmlException(string message, short code, IReturnCodeMessage retCodeMesage)
            : this(message, code)
        {
            m_retCodeMessage = retCodeMesage;
        }

        public WitsmlException(string message, short code, IReturnCodeMessage retCodeMesage, bool returnMsg)
            : this(message, code, returnMsg)
        {
            m_retCodeMessage = retCodeMesage;
        }

        public WitsmlException(string message, short code, Exception innerException)
            : base(message, innerException)
        {
            m_retCodeMessage = WitsmlReturnCodeMessage.Instance;
            ReturnCode = code;
        }

        /// <summary>
        /// Construct a PowerStore Exception inside the Catch block
        /// </summary>
        /// <param name="message">Exception Message</param>
        /// <param name="code">ReturnCode for the caller</param>
        /// <param name="innerException">The innerException catched in the catch(Exception ex){} block</param>
        public WitsmlException(string message, WitsmlReturnCode code, Exception innerException)
            : this(message, (short)code, innerException)
        {
        }

        public WitsmlException(string message, short code, Exception innerException, IReturnCodeMessage retCodeMesage)
            : this(message, code, innerException)
        {
            m_retCodeMessage = retCodeMesage;
        }

        public WitsmlException(string message, short code, Exception innerException, IReturnCodeMessage retCodeMesage
            , bool returnMsg)
            : this(message, code, innerException, retCodeMesage)
        {
            if (returnMsg)
            {
                this.ReturnMessage = message;
            }
        }

        public WitsmlException()
            : base()
        {
            m_retCodeMessage = WitsmlReturnCodeMessage.Instance;
        }

        public WitsmlException(string message, WitsmlReturnCode code, Exception innerException, bool returnMsg)
            : this(message, code, innerException)
        {
            if (returnMsg)
            {
                this.ReturnMessage = message;
            }
        }
    }
}
