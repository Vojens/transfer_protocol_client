﻿ 

using System.Collections.Generic;
using System;
namespace Support.Common.Exceptions
{
    public interface IReturnCodeMessage
    {
        Dictionary<short, string> MessageList { get; }
    }

    public interface IReturnCodeMap
    {
        Dictionary<short, Dictionary<short, Tuple<short, string>>> MapList { get; }
    }
}
