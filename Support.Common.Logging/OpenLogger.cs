 

using System;
using System.Collections.Generic;
using System.Text;
using Microsoft.Practices.EnterpriseLibrary.Logging;
using Microsoft.Practices.EnterpriseLibrary.Logging.Filters;
using System.Diagnostics;
using Microsoft.Practices.EnterpriseLibrary.Logging.Formatters;
using System.Configuration;
using Microsoft.Practices.EnterpriseLibrary.Logging.Configuration;
using Microsoft.Practices.EnterpriseLibrary.Common.Configuration;
using Microsoft.Practices.EnterpriseLibrary.Logging.TraceListeners;

namespace Support.Common.Logging
{
    /// <summary>
    /// OpenLogger means that the logger is open for adding new things such as listener.
    /// </summary>
    public static class OpenLogger
    {
        /// <summary>
        /// Default formatter.
        /// </summary>
        private static ILogFormatter m_defaultFormatter = new TextFormatter("{message}");

        /// <summary>
        /// Public accessor for default text formatter.
        /// </summary>
        public static ILogFormatter DefaultFormatter
        {
            get
            {
                return m_defaultFormatter;
            }
        }

        /// <summary>
        /// Default formatter name, if this name exist in app.config, it will override the default formatter.
        /// </summary>
        private  const string DEFAULT_FORMATTER_NAME = "OpenLoggerFormatter";

        /// <summary>
        /// Default log source name, if this name exist in app.config, it will override the default formatter.
        /// </summary>
        private const string DEFAULT_LOGSOURCE_NAME = "OpenLoggerLogSource";

        /// <summary>
        /// Existing logging settings.
        /// </summary>
        private static LoggingSettings m_settings = LoggingSettings.GetLoggingSettings(new SystemConfigurationSource());

        /// <summary>
        /// Log source dictionary, to stores any related trace listener.
        /// </summary>
        private static IDictionary<string, LogSource> m_logSources = new Dictionary<string, LogSource>();

        /// <summary>
        /// Constructs Open Logger.
        /// </summary>
        static OpenLogger()
        {
            //  Create a default log source.
            m_logSources.Add(DEFAULT_LOGSOURCE_NAME, new LogSource(DEFAULT_LOGSOURCE_NAME));

            //  Override default values with values got from configuration.
            if (m_settings != null)
            {
                //  Override default FORMATTER with value from the config, if exist.
                if (m_settings.Formatters.Contains(DEFAULT_FORMATTER_NAME))
                {
                    //  Check if the formatter is a TEXT FORMATTER DATA.
                    if (m_settings.Formatters.Get(DEFAULT_FORMATTER_NAME) is TextFormatterData)
                    {
                        //  Get formmater setting.
                        TextFormatterData formatterData = (TextFormatterData)m_settings.Formatters.Get(DEFAULT_FORMATTER_NAME);

                        //  Override the current default formatter.
                        m_defaultFormatter = new TextFormatter(formatterData.Template);
                    }
                }

                //  Populate log source from configuration and add it to the log source dictionary.
                if (m_settings.TraceSources.Count > 0)
                {
                    foreach (TraceSourceData traceSourceData in m_settings.TraceSources)
                    {
                        LogSource logSource = new LogSource(traceSourceData.Name, traceSourceData.DefaultLevel);
                        m_logSources.Add(logSource.Name, logSource);
                    }
                }
            }
        }

        /// <summary>
        /// Add trace listener.
        /// </summary>
        /// <param name="traceListener"></param>
        public static void AddTraceListener(TraceListener traceListener)
        {
            foreach (LogSource logSource in m_logSources.Values)
            {
                logSource.Listeners.Add(traceListener);
            }
        }

        /// <summary>
        /// Add trace listener to a specific log source name.
        /// </summary>
        /// <param name="traceListener"></param>
        /// <param name="logSourceName"></param>
        public static void AddTraceListener(TraceListener traceListener, string logSourceName)
        {
            if (!m_logSources.ContainsKey(logSourceName))
            {
                throw new OpenLoggerException("There is no Log Source with specific name exist.");
            }

            m_logSources[logSourceName].Listeners.Add(traceListener);
        }

        /// <summary>
        /// Adds trace listener to a specific log source name, and if force to create, automatically create source if not exist.
        /// </summary>
        /// <param name="traceListener"></param>
        /// <param name="logSourceName"></param>
        public static void AddTraceListener(TraceListener traceListener, string logSourceName, bool isForceCreateLogSource)
        {
            //  Adds log source name if not exist.
            if (!m_logSources.ContainsKey(logSourceName) && isForceCreateLogSource)
            {
                m_logSources.Add(logSourceName, new LogSource(logSourceName));
            }

            AddTraceListener(traceListener, logSourceName);
        }

        /// <summary>
        /// Adds trace listener to all log source, exclude the specified log source in params.
        /// </summary>
        /// <param name="traceListener"></param>
        /// <param name="excludeFromTheseLogs"></param>
        public static void AddTraceListener(TraceListener traceListener, string[] excludeFromTheseLogs)
        {
            List<string> excludeList = new List<string>(excludeFromTheseLogs);

            foreach (LogSource logSource in m_logSources.Values)
            {
                if (!excludeList.Contains(logSource.Name))
                {
                    logSource.Listeners.Add(traceListener);
                }
            }
        }

        /// <summary>
        /// Log source for error log source.
        /// </summary>
        private static LogSource m_emptySource = new LogSource("Empty");

        /// <summary>
        /// Writer to write the log.
        /// Instantiate everytime additional trace listener is added.
        /// </summary>
        private static LogWriter m_writer = null;

        private static object m_sync = new object();

        /// <summary>
        /// Writer to write log to all defined source.
        /// </summary>
        public static LogWriter Writer
        {
            get
            {
                //  Constructs writer if it is null or force to init writer again.
                if (m_writer == null)
                {
                    lock (m_sync)
                    {
                        //  Create writer with default log source specified in DEFAULT_LOGSOURCE_NAME.
                        m_writer = new LogWriter(new ILogFilter[0], m_logSources, m_emptySource, DEFAULT_LOGSOURCE_NAME);
                    }
                }

                return m_writer;
            }
        }

        /// <summary>
        /// Determines that this logger should write to configured logger (LAB) or not.
        /// </summary>
        private static bool m_isWriteToConfiguredLogger = true;

        public static bool IsWriteToConfiguredLogger
        {
            get { return m_isWriteToConfiguredLogger; }
            set { m_isWriteToConfiguredLogger = value; }
        }

        /// <summary>
        /// Get categories from log sources EXCLUDES the specified excluded categories.
        /// </summary>
        /// <param name="excludedCategories"></param>
        /// <returns></returns>
        private static List<string> getCategories(params string[] excludedCategories)
        {
            //  Convert excluded categories string array into list.
            List<string> excludeCategoryList = new List<string>(excludedCategories);

            //  Prepare place to store category.
            List<string> categoryList = new List<string>();

            //  Filter log source target with excluded category/
            foreach (string logSourceTarget in m_logSources.Keys)
            {
                if (!excludeCategoryList.Contains(logSourceTarget))
                {
                    categoryList.Add(logSourceTarget);
                }
            }

            return categoryList;
        }

        /// <summary>
        /// Writes message to all category.
        /// </summary>
        /// <param name="message"></param>
        public static void Write(string message)
        {
            //  Gets all categories.
            List<string> categories = getCategories();
            write(message, categories);
        }

        /// <summary>
        /// Write message with specific format.
        /// </summary>
        /// <param name="format"></param>
        /// <param name="args"></param>
        public static void Write(string category, string format, params object[] args)
        {
            Write(string.Format(format, args), category);
        }

        /// <summary>
        /// Writes message to all category except the excluded categories.
        /// </summary>
        /// <param name="message"></param>
        /// <param name="excludedCategories"></param>
        public static void Write(string message, string[] excludedCategories)
        {
            //  Gets filtered categories.
            List<string> categories = getCategories(excludedCategories);
            write(message, categories);
        }

        /// <summary>
        /// Writes message to specific category.
        /// </summary>
        /// <param name="message"></param>
        /// <param name="category"></param>
        public static void Write(string message, string category)
        {
            //  Gets single category;
            List<string> categories = new List<string>();
            categories.Add(category);

            write(message, categories);
        }

        /// <summary>
        /// Gets log entry for specified categories.
        /// </summary>
        /// <param name="message"></param>
        /// <param name="categories"></param>
        /// <returns></returns>
        private static LogEntry getLogEntry(string message, List<string> categories)
        {
            LogEntry logEntry = new LogEntry();
            logEntry.Message = message;
            logEntry.Categories = categories;

            return logEntry;
        }

        /// <summary>
        /// Writes message to specified categories.
        /// </summary>
        /// <param name="message"></param>
        /// <param name="categories"></param>
        private static void write(string message, List<string> categories)
        {
            LogEntry logEntry = getLogEntry(message, categories);
            Writer.Write(logEntry);

            //  Log to the configured logger if necessary.
            if (m_isWriteToConfiguredLogger)
            {
                try
                {
                    Logger.Write(logEntry);
                }
                catch (ConfigurationException cEx)
                {
                    //  If no configuration exist, write to OpenLogger source, and set to not write to the configured logger.
                    Writer.Write(getLogEntry(cEx.Message, categories));
                    m_isWriteToConfiguredLogger = false;
                }
            }
        }

    }
}
