 

using System;
using System.Collections.Generic;
using System.Text;
using System.Diagnostics;

namespace Support.Common.Logging
{
    /// <summary>
    /// Writes message directly to the exception source.
    /// </summary>
    public static class ExceptionLogger
    {
        public static void Write(string message)
        {
            OpenLogger.Write(message, OpenLoggerCategory.Exception);
        }

        public static void Write(string format, params object[] args)
        {
            OpenLogger.Write(OpenLoggerCategory.Exception, format, args);
        }
    }

    /// <summary>
    /// Writes message directly to the exception handler source.
    /// </summary>
    public static class ExceptionHandlerLogger
    {
        public static void Write(string message)
        {
            OpenLogger.Write(message, OpenLoggerCategory.ExceptionHandler);
        }

        public static void Write(string format, params object[] args)
        {
            OpenLogger.Write(OpenLoggerCategory.ExceptionHandler, format, args);
        }
    }

    /// <summary>
    /// Writes message directly to the activity source.
    /// </summary>
    public static class ActivityLogger
    {
        public static void Write(string message)
        {
            OpenLogger.Write(message, OpenLoggerCategory.Activity);
        }

        public static void Write(string format, params object[] args)
        {
            OpenLogger.Write(OpenLoggerCategory.Activity, format, args);
        }
    }

    /// <summary>
    /// Writes message directly to all source, excludes exceptions.
    /// </summary>
    public static class CommonLogger
    {
        private static string[] m_excludedLogSource = new string[] { OpenLoggerCategory.Exception
            , OpenLoggerCategory.ExceptionHandler };

        public static void Write(string message)
        {
            OpenLogger.Write(message, m_excludedLogSource);
        }

        public static void AddListener(TraceListener traceListener)
        {
            OpenLogger.AddTraceListener(traceListener, m_excludedLogSource);
        }
    }
}
