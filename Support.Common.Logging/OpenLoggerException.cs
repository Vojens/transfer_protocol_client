 

using System;
using System.Collections.Generic;
using System.Text;

namespace Support.Common.Logging
{
    public class OpenLoggerException : ApplicationException
    {
        public OpenLoggerException(string message)
            : base(message)
        {
        }
    }
}
