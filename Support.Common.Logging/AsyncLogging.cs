﻿ 

using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.IO;

namespace Support.Common.Logging
{
    public delegate void WriteLogDelegate(string fileName, ref string message);
    public class AsyncLogging
    {
        public static void WriteToFile(string fileName, ref string message)
        {
            try
            {
                //Check if directory Exists
                string dirName = Path.GetDirectoryName(fileName);
                if (!Directory.Exists(dirName))
                {
                    Directory.CreateDirectory(dirName);
                }

                //Wait till resource is released
                using (TextWriter w = StreamWriter.Synchronized(File.AppendText(fileName)))
                {
                    w.WriteLine(message);
                }
            }
            catch (Exception ex)
            {
                string logFileName = Path.Combine(Path.GetDirectoryName(fileName), "LoggingError.log");
                WriteLoggingError(logFileName, ex);
            }
        }

        private static void WriteLoggingError(string fileName, Exception ex)
        {
            try
            {
                using (TextWriter w = StreamWriter.Synchronized(File.AppendText(fileName)))
                {
                    w.WriteLine(ex.Message + System.Environment.NewLine);
                    w.WriteLine(ex.StackTrace);
                }
            }
            catch (Exception)
            {

            }
        }
    }
}
