 

using System;
using System.Collections.Generic;
using System.Text;

namespace Support.Common.Logging
{
    /// <summary>
    /// Default logger category for Bric.
    /// </summary>
    public struct OpenLoggerCategory
    {
        /// <summary>
        /// Expected exception with handler.
        /// </summary>
        public const string ExceptionHandler = "ExceptionHandler";

        /// <summary>
        /// Unexpected exception.
        /// </summary>
        public const string Exception = "Exception";

        /// <summary>
        /// Common activity.
        /// </summary>
        public const string Activity = "Activity";
    }
}
