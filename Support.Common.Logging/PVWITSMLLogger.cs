﻿ 

using System;
using System.Collections.Generic;
using System.Text;
using Microsoft.Practices.EnterpriseLibrary.Logging;
using Microsoft.Practices.EnterpriseLibrary.Logging.TraceListeners;

namespace Support.Common.Logging
{
    /// <summary>
    /// Deals with Logging square WITSML related Logging
    /// </summary>
    public class PVWITSMLLogger
    {

        /// <summary>
        /// Writes an UnHandled Exception which is used is Catch(Exception ex) -> ALL
        /// </summary>
        /// <param name="ex"></param>
        public static void LogUnHandledException(Exception ex)
        {
            LogEntry logEntry = new LogEntry();
            logEntry.Title = "WebMethod Call Fails.";
            logEntry.Message = GetCompleteExceptionMessage(ex);
            logEntry.Categories = new List<string>() { OpenLoggerCategory.Exception };

            Logger.Write(logEntry);
        }

        /// <summary>
        /// Gets a Complete Stack Trace by traversing an Exception all d way down to it's inner Exception
        /// </summary>
        /// <param name="ex"></param>
        /// <returns></returns>
        private static string GetCompleteExceptionMessage(Exception ex)
        {
            StringBuilder exceptionMsg = new StringBuilder();
            do
            {
                exceptionMsg.Append(String.Format("[{0}]{1}{2}", new string[] { ex.Message, System.Environment.NewLine, ex.StackTrace }));
                ex = ex.InnerException;
            }
            while (ex != null);

            return exceptionMsg.ToString();
        }
    }
}
