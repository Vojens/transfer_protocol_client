 

using System;
using System.Collections.Generic;
using System.Text;
using Microsoft.Practices.EnterpriseLibrary.Common.Configuration;
using Microsoft.Practices.EnterpriseLibrary.Logging;
using Microsoft.Practices.EnterpriseLibrary.Logging.Configuration;
using Microsoft.Practices.EnterpriseLibrary.Logging.TraceListeners;
using System.Diagnostics;
using Microsoft.Practices.EnterpriseLibrary.Logging.Formatters;

namespace Support.Common.Logging
{
    /// <summary>
    /// Logging into Console.
    /// </summary>
    [ConfigurationElementType(typeof(CustomTraceListenerData))]
    public class CustomListener : CustomTraceListener
    {
        /// <summary>
        /// Format the data using selected Formatter type.
        /// </summary>
        /// <param name="eventCache"></param>
        /// <param name="source"></param>
        /// <param name="eventType"></param>
        /// <param name="id"></param>
        /// <param name="data">Data to write to the console.</param>
        public override void TraceData(TraceEventCache eventCache, string source, TraceEventType eventType, int id, object data)
        {
            //  Formatting data based on it's type.
            if (data is LogEntry && this.Formatter != null)
            {
                this.WriteLine(this.Formatter.Format(data as LogEntry));
            }
            else if (data is string)
            {
                this.WriteLine(data);
            }
            else
            {
                this.WriteLine(data.ToString());
            }
        }

        /// <summary>
        /// Override Write method so the application writes data to the console.
        /// </summary>
        /// <param name="message"></param>
        public override void Write(string message)
        {
            //Console.Write(message);
            m_delegateWrite(message);
        }

        public  DelegateWrite m_delegateWrite;
        public delegate void DelegateWrite(string message);

        /// <summary>
        /// Override Write method so the application writes data to the console, followed by current line terminator.
        /// </summary>
        /// <param name="message"></param>
        public override void WriteLine(string message)
        {
            //Console.WriteLine(message);
            m_delegateWriteLine(message);
        }

        public  DelegateWriteLine m_delegateWriteLine;
        public delegate void DelegateWriteLine(string message);


        /// <summary>
        /// Constructs console listener with name and formatter.
        /// </summary>
        /// <param name="name"></param>
        /// <param name="logFormatter"></param>
        public CustomListener(string name, ILogFormatter logFormatter)
        {
            this.Name = name;
            this.Formatter = logFormatter;
        }

        /// <summary>
        /// Constructs console listener with formatter.
        /// </summary>
        /// <param name="logFormatter"></param>
        public CustomListener(ILogFormatter logFormatter)
        {
            this.Formatter = logFormatter;
        }

        /// <summary>
        /// Default non parameters constructor.
        /// </summary>
        public CustomListener()
        {
        }
    }
}
